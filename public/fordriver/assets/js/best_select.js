// ajax 配合JQ3 引入設置
// select
let select = document.querySelectorAll('.best-select select');
let p = document.querySelectorAll('.best-select p');
let selectLength = select.length;
for (let i = 0; i < selectLength; i++) {
  let optionList = select[i].children;
  select[i].value = optionList[0].value;
  p[i].textContent = optionList[0].textContent;
  select[i].addEventListener('change', function (event) {
      let optionListLength = optionList.length;
      for (let index = 0; index < optionListLength; index++) {
          if (optionList[index].value === select[i].value) {
              p[i].textContent = optionList[index].textContent;
          }
      }
  });
}