function openSignPic(sysOrdNo) {
    console.log("openSignPic");
    if(picsignsysno !=0){
        sysnonow = picsignsysno;
    }
    var signid ="";
    for(var i = 0 ; i<document.getElementsByName("signbtn").length ; i++){
        signid= document.getElementsByName("signbtn")[i].id;
        document.getElementById(signid).style.backgroundColor ="white";
    }
    picsignsysno = sysOrdNo;
    setTimeout(function(){ document.getElementById(sysOrdNo+"sign").style.backgroundColor ='rgb(' + 204 + ',' + 204 + ',' + 255 + ')';},500);
    window.open(BASE_URL + "/show/" + sysOrdNo + '/FINISH', '簽收照片', config='height=500,width=600');
}
function openPicture(path) {

    document.getElementById('light-box').style.display = 'flex';
    document.getElementById('light-box-lg-img').src = BASE_URL + "/storage/img/" + path.replace('public/img/', '');
    return false;
}
function openappNo(appno) {
    window.open(BASE_URL +"/"+lang+ "/openappno/" + appno , config='height=500,width=600');
}
function openerror(error_id) {
    window.open(BASE_URL +"/"+lang+ "/openerror/" + error_id , config='height=500,width=600');
}

function openorder(ord_no) {
    window.open(BASE_URL +"/"+lang+ "/openorder/" + ord_no , config='height=500,width=600');
}
function opensign(ord_no) {
    window.open(BASE_URL +"/"+lang+ "/opensign/" + ord_no , config='height=500,width=600');
}
function openFile(file_id) {

    $.ajax({
        url: BASE_URL + '/' + lang + '/' + 'file/download/'+file_id,
        type: 'POST',
        async: false,
        beforeSend: function () {
            
        },
        error: function () {
            swal("發生錯誤", "", "error");
            return false;
        },
        success: function (data) {
            if(data.message !='success'){
                    swal("無檔案提供下載", "", "warning");
                    return ; 
                }
                window.open(data.donwloadLink);
                // var link = document.createElement('a');
                // link.id = 'downurl6';
                // link.href = data.donwloadLink;
                // document.body.appendChild(link);
                // document.getElementById('downurl6').click();
                // document.getElementById('downurl6').remove();
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
}

function openErrorPic(sysOrdNo) { 
    // if(picerrsysno !=0){
    //     document.getElementById(picerrsysno+"error").style.background ="white"
    // }
    var tempid ="";
    for(var i = 0 ; i<document.getElementsByName("errorbtn").length ; i++){
        tempid= document.getElementsByName("errorbtn")[i].id;
        document.getElementById(tempid).style.backgroundColor ="white";
    }
    picerrsysno = sysOrdNo;
    setTimeout(function(){ document.getElementById(sysOrdNo+"error").style.backgroundColor ='rgb(' + 204 + ',' + 204 + ',' + 255 + ')';},500);
    window.open(BASE_URL + "/show/" + sysOrdNo + '/ERROR', '異常照片', config='height=500,width=600');
}

var initGrid = function (gridId,fieldData,getDataUrl,gridOpt, fieldObj) {
    var statusCode = {};
    $.grep(fieldData[1], function(e){
        if(e.datafield == "status") {
            statusCode = e.statusCode;
            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "status") {
                    e['values'] = { source: statusAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "trs_mode") {
            trsMode = e.trsMode;
            var trsModeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: trsMode
            };

            var trsModeAdapter = new $.jqx.dataAdapter(trsModeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "trs_mode") {
                    e['values'] = { source: trsModeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "car_type") {
            carType = e.carType;
            var carTypeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: carType
            };

            var carTypeAdapter = new $.jqx.dataAdapter(carTypeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "car_type") {
                    e['values'] = { source: carTypeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "dlv_type") {
            dlvType = e.dlvType;
            var dlvTypeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: dlvType
            };

            var dlvTypeAdapter = new $.jqx.dataAdapter(dlvTypeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "dlv_type") {
                    e['values'] = { source: dlvTypeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "sign_pic") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                var signPic = dataAdapter.records[row].sign_pic;
                var btn = "";

                if(signPic > 0) {
                    btn = `<button class="btn btn-link" onclick="openSignPic('{sysOrdNo}')">簽收照片連結</button>`;
                    btn = btn.replace("{sysOrdNo}", sysOrdNo);
                }
                
                return btn;
            };

            e.cellsrenderer = cellsrenderer;

        }
        if(e.datafield == "application_no" && tableName == "barcode_history") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var appno = dataAdapter.records[row].application_no;
                var btn = "";
                if(appno != '' && appno != null ) {
                    btn = `<button class="btn btn-link" onclick="openappNo('{appno}')">`+appno+`</button>`;
                    btn = btn.replace("{appno}", appno);
                }
                return btn;
            };
            e.cellsrenderer = cellsrenderer;
        }

        if(e.datafield == "error_pic") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var sysOrdNo = dataAdapter.records[row].sys_ord_no;
                var errorPic = dataAdapter.records[row].error_pic;
                var btn = "";

                if(errorPic > 0) {
                    btn = `<button class="btn btn-link" onclick="openErrorPic('{sysOrdNo}')">異常照片連結</button>`;
                    btn = btn.replace("{sysOrdNo}", sysOrdNo);
                }
                
                return btn;
            };

            e.cellsrenderer = cellsrenderer;

        }

        if(e.datafield == "image") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var image = dataAdapter.records[row].image;
                var btn = "";

                if(image != '' ) {
                    btn = `<button class="btn btn-link" onclick="openPicture('{image}')">點我打開照片</button>`;
                    btn = btn.replace("{image}", image);
                }
                
                return btn;
            };

            e.cellsrenderer = cellsrenderer;

        }

        // if(e.datafield == "jdn") {
        //     // 物料建檔內頁 物料欄位
        //     var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
        //         var imgUrl = dataAdapter.records[row].imgUrl || 'https://www.loverabbit.org/upload/20220516143155jeqe21.jpg';
        //         var jdnName = dataAdapter.records[row].jdnName || '物料名稱';
        //         var jpn = dataAdapter.records[row].jpn || 'JPN-NUMBER';
        //         var jpnPro = dataAdapter.records[row].jpnPro || '專案';
        //         var content = "";

        //         if(imgUrl) {
        //             content = `<div style="display:flex;">
        //                             <div style="width: 48px; height: 48px; margin-right: 8px; overflow: hidden;">
        //                                 <img src="{imgUrl}" alt="" style="max-width: 100%;">
        //                             </div>
        //                             <div>
        //                                 <div style="font-size: 12px; color: #333333;">{jdnName}</div>
        //                                 <div style="font-size: 12px; color: #666666;">{jpn}</div>
        //                                 <div style="font-size: 12px; color: #999999;">{jpnPro}</div>
        //                             </div>
        //                         </div>`
        //             btn = `<button class="btn btn-link" onclick="openSignPic('{sysOrdNo}')">簽收照片連結</button>`;
        //             content = content.replace("{jdn}", jdn);
        //         }
                
        //         return content;
        //     };

        //     e.cellsrenderer = cellsrenderer;

        // }
    });

    
    var source =
    {
        datatype: "json",
        datafields: fieldData[0],
        root:"Rows",
        pagenum: 0,
        beforeprocessing: function (data) {
            source.totalrecords = data[0].TotalRows;
            if(data[0].StatusCount.length > 0) {
                data[0].StatusCount.push({'count': data[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
            }

            if($("#statusList").find('a').length == 0) {
                genStatus(data, gridId);
            }
            /*else{

                $.each($("#statusList a span.badge"),function(k,v){
                    v.innerHTML=0
                    $.each(data[0].StatusCount, function(i, item) {
                        if($($("#statusList a span.text")[k]).attr("id") == item.status){
                            v.innerHTML=item.count;
                        }
                    });
                });
              
            }*/
            
        },
        filter: function () {
            // update the grid and send a request to the server.
            $("#"+gridId).jqxGrid('updatebounddata', 'filter');
        },
        sort: function () {
            // update the grid and send a request to the server.
            $("#"+gridId).jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 50,
        url: getDataUrl
    }

    if(typeof gridOpt.selectionmode === "undefined"){
        gridOpt.selectionmode = "checkbox";
    }
    var dataAdapter = new $.jqx.dataAdapter(source, { 
        async: false, 
        loadError: function (xhr, status, error) { 
            alert('Error loading "' + source.url + '" : ' + error); 
        },
        loadComplete: function() {
            console.log('123');
            $("#jqxLoader").jqxLoader('close');
            $.get(gridOpt.getStatusCount, {}, function(res){
                res[0].StatusCount.push({'count': res[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
                genStatus(res, gridId);
            }, 'JSON');
        }
    });

    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }

    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }
    $("#"+gridId).jqxGrid(
    {
        width: '100%',
        height: winHeigt,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
        //autoheight: true,
        source: dataAdapter,
        sortable: true,
        filterable: (typeof gridOpt.searchOpt !== "undefined")? gridOpt.searchOpt: false,
        altrows: true,
        showfilterrow: (typeof gridOpt.searchOpt !== "undefined")? gridOpt.searchOpt: false,
        pageable: true,
        virtualmode: true,
        autoshowfiltericon: true,
        columnsreorder: true,
        columnsresize: true,
        columnsautoresize: true,
        //autoloadstate: true,
        clipboard: true,
        selectionmode: gridOpt.selectionmode,
        enablebrowserselection: (typeof gridOpt.enablebrowserselection !== "undefined")? gridOpt.enablebrowserselection: true,
        pagesizeoptions:[50, 100, 500, 2000],
        rendergridrows: function (params) {
            //alert("rendergridrows");
            return params.data;
        },
        ready: function () {
            // if(gridOpt.who != 'lookup'){
            //     $("#jqxLoader").jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
            //     // $("#jqxLoader").jqxLoader('open');
            // }
            // $("#jqxLoader").jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
            // $("#jqxLoader").jqxLoader('open');
            $('#' + gridId).jqxGrid('autoresizecolumns');
        },
        updatefilterconditions: function (type, defaultconditions) {
            var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
            var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
            var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
            var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
            switch (type) {
                case 'stringfilter':
                    return stringcomparisonoperators;
                case 'numericfilter':
                    return numericcomparisonoperators;
                case 'datefilter':
                    return datecomparisonoperators;
                case 'booleanfilter':
                    return booleancomparisonoperators;
            }
        },
        columns: fieldData[1]
    });

    if(gridOpt.who == 'lookup')
    {
        $('#'+gridOpt.gridId).on('rowdoubleclick', function (event) 
        { 
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;


            var rowindexes = $('#'+gridOpt.gridId).jqxGrid('getselectedrowindexes');
            if(gridOpt.selectionmode === "checkbox" && rowindexes.length > 0){
                
            }else{
                var datarow = $('#'+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
                console.log(datarow);
                var fieldMapping = fieldObj.info4.split(";");
            
                $.each(fieldMapping,function(k,v){
                k = v.split("=");
                $("#"+k[1]).val(datarow[k[0]]);
                if(datarow['area_nm']!=undefined){
                    if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
                    $("#"+k[1]).val(datarow['city_nm']+datarow['area_nm']);
                    }
                }else{
                    if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
                    $("#"+k[1]).val(datarow['city_nm']+datarow['dist_nm']);
                    }
                }
                });
            }
            $("#lookupEvent").trigger(fieldObj.triggerfunc,[rowindexes]);
            if(typeof callBackFunc !== 'undefined') {
                callBackFunc(datarow);
            }
            
            $('#lookupModal').modal('hide');
            $('#jqxLoader').jqxLoader('close');
        });
    }
    

    $("#"+gridId).on('bindingcomplete', function (event) {
        var statusHeight = 0;
        $('#jqxLoader').jqxLoader('close');
        if(typeof gridOpt.enabledStatus !== "undefined") {
            if(gridOpt.enabledStatus == false) {
                statusHeight = 50;
            }
        }
        var winHeight = $( window ).height() - 350 + statusHeight;
        $("#"+gridId).jqxGrid('height', winHeight+'px');
        //$("#"+gridId).jqxGrid("hideloadelement"); 
    });
    
    if(typeof fieldData[2] !== "undefined") {
        $("#"+gridId).jqxGrid('loadstate', fieldData[2]);
    }

    if(typeof gridOpt.getState != "undefined" && gridOpt.getState != null){
        gridOpt.getState();
    }

    var localizationobj = {};
    localizationobj.pagergotopagestring              = "到   : ";
    localizationobj.pagershowrowsstring              = "顯示筆數: ";
    localizationobj.pagerrangestring                 = " 的 ";
    // localizationobj.pagernextbuttonstring         = "voriger";
    // localizationobj.pagerpreviousbuttonstring     = "nächster";
    localizationobj.sortascendingstring              = "升冪";
    localizationobj.sortdescendingstring             = "降冪";
    localizationobj.sortremovestring                 = "移除排序";
    localizationobj.filterstringcomparisonoperators  = ['包含','不包含'];
    localizationobj.filternumericcomparisonoperators = ['小於','大於'];
    localizationobj.filterdatecomparisonoperators    = ['小於','大於'];
    localizationobj.filterbooleancomparisonoperators = ['等於','不等於'];
    localizationobj.filterclearstring                = "清除";
    localizationobj.filterstring                     = "篩選";
    localizationobj.filterchoosestring               = "請選擇：";
    localizationobj.filtershowrowstring              = "顯示資料於：";
    localizationobj.filterandconditionstring         = "且";
    localizationobj.filterorconditionstring          = "或";
    // apply localization.
    try{
        $("#" + gridId).jqxGrid('localizestrings', localizationobj);
    }catch{

    }

    //$("#"+gridId).jqxGrid('selectionmode', 'multiplecellsextended');
}

function genStatus(data, gridId) {
    console.log('genStatus');
    $("#statusList").html("");
    $.each(data[0].StatusCount, function(i, item) {
        $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
    });

    $('.statusBtn').off().on('click', function(){
        $('#statusList').find('a').removeClass('active');
        $(this).addClass('active');

        var statusGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = $(this).find('span.text').attr("id");
        if(filtervalue == "ALL") {
            $("#" + gridId).jqxGrid('clearfilters');
        }
        else {
            var filtercondition = 'contains';
            var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
            statusGroup.addfilter(filter_or_operator, statusFilter1);
            $("#" + gridId).jqxGrid('addfilter', 'status', statusGroup);
            $("#" + gridId).jqxGrid('applyfilters');
        }
        
    });
}

// START
var gridOpt          = {};
var filtervalue      = "";
var filterfield      = "";
var filtertype       = "";
var enabledheader    = [];
var filtercolumndata = [];
try {
    gridOpt.pageId                 = fileName;
    gridOpt.enabledStatus          = false;
    gridOpt.fieldsUrl              = BASE_URL + "/admin/baseApi/getGridJson/" + tableName;
    gridOpt.dataUrl                = BASE_URL + "/admin/baseApi/getGridJson/" + tableName;
    gridOpt.fieldsUrl              = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl              = BASE_URL + '/' + lang + '/' + fileName + '/create';
    gridOpt.editUrl                = BASE_URL + '/' + lang + '/' + fileName + '/{id}/edit';
    gridOpt.height                 = 800;
    gridOpt.selectionmode          = "checkbox";
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick         = true;
    gridOpt.searchOpt              = true;
    gridOpt.getStatusCount         = BASE_URL + "/admin/baseApi/getStatusCount/" + tableName +"/"+languagename;
} catch (error) {
    
}


var btnGroup                       = []
var btnGroupSimple                       = [
    {
        btnId: "btnSearchWindow",
        btnIcon: "fa fa-search",
        btnText: "搜尋",
        btnFunc: function () {
            $('#searchWindow').jqxWindow('open');
        }
    },
    {
        btnId: "btnExportExcel",
        btnIcon: "fa fa-cloud-download",
        btnText: 'Excel匯出',
        btnFunc: function () {
            var url  =BASE_URL;
            url = url.replace("admin","");
            var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
            var pageNum = 1;
            var pageSize = 50;
            filtercolumndata = [];
            var filtercolumn = [];

            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
            }
            var anotherCondition = typeof sqlcondition !== 'undefined' ? sqlcondition : [];
            if(ids.length == 0) {
                var pageNum = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                var pageSize = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagesize;
                for (var i = 0; i < filterGroups.length; i++) {
                    var filterGroup = filterGroups[i];
                    var filters = filterGroup.filter.getfilters();
                    for (var k = 0; k < filters.length; k++) {
                        if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                            var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                            // new Date(unix_timestamp * 1000)
                            filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                        } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                            var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                            filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                        } else if(filters[k].condition == "contains" ) {
                            filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                        } else {
                            filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                        }
                        // filtercolumndata.push(filtercolumn);
                    }
                }
            } else {
                filtercolumn[m] = ['id', `in`, ids];
            }
            var sortby = [];
            var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
            if(sortinformation.sortcolumn != null ) {
                var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                var obj = {};
                obj["sortField"] = sortinformation.sortcolumn;
                obj["sortValue"] = sortvalue;
                sortby.push(obj);
                // sortby.push([ sortinformation.sortcolumn,  tpye]);
            }
            $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                'table'        : tableName,
                'fileName'     : fileName,
                'baseCondition': filtercolumn,
                'anotherCondition': anotherCondition,
                'pageNum':pageNum,
                'pageSize':pageSize,
                'sort': sortby,
                'header'       : enabledheader,
            }, function(data){
                if(data.message == "success") {
                    window.open(data.donwloadLink);
                }

            });
        }
    },
    {
        btnId: "btnOpenGridOpt",
        btnIcon: "fa fa-table",
        btnText: "欄位調整",
        btnFunc: function () {
            $('#gridOptModal').modal('show');
        }
    },
    {
        btnId: "btnAdd",
        btnIcon: "fa fa-edit",
        btnText: btnAdd,
        btnFunc: function() {
            location.href = gridOpt.createUrl;
        }
    },
    {
        btnId: "btnDelete",
        btnIcon: "fa fa-trash-o",
        btnText: btnDelete,
        btnFunc: function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if (typeof row != "undefined") {
                    ids.push(row.id);
                }
            }
            if (ids.length == 0) {
                swal(msg1, "", "warning");
                return;
            }

            swal({
                title: msgnew2,
                text: msgnew3,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: confirmButtonText,
                cancelButtonText: cancelButtonText
            }).then((result) => {
                if (result.value) {
                    $.post(BASE_URL + '/' + lang + '/' + fileName + '/batchDelete', {
                        'ids': ids
                    }, function(data) {
                        if (data.success) {
                            swal(msgnew4, "", "success");
                            $("#jqxGrid").jqxGrid('updatebounddata');
                            $("#jqxGrid").jqxGrid('clearselection');
                        } else {
                            swal(msgnew5, '', "error");
                        }
                    });

                } else {

                }
            });

        }
    },
    {
        btnId:"btnUploadExcel",
        btnIcon:"fa fa-cloud-upload",
        btnText:btnUploadExcel,
        btnFunc:function(){
            $("#excelFile").click();
        }
    },
    {
        btnId:"btnReport9",
        btnIcon:"fa fa-file",
        btnText:btnReport9,
        btnFunc:function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                swal(msg1, "", "warning");
                return;
            }
            var now = new Date();
            $.post(BASE_URL + '/' + lang + '/' + fileName + '/report/down', {'ids': ids}, function(data){
                if(data.message =='error'){
                    swal("報表異常請重新列印", "", "warning");
                    return ; 
                }
                var date_now = now.format("yyyyMMddhhmmss");
                var link = document.createElement('a');
                    link.id = 'downurl6';
                    link.href = data.donwloadLink;
                    link.download = "報表"+date_now+".pdf";
                    console.log(link);
                    document.body.appendChild(link);
                    document.getElementById('downurl6').click();
                    document.getElementById('downurl6').remove();
            });
            $('#jqxGrid').jqxGrid('clearselection');
        }
    },
    {
        btnId: "btnSendmail",
        btnIcon: "fa fa-cloud-download",
        btnText: '發送mail',
        btnFunc: function() {
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if (typeof row != "undefined") {
                    ids.push(row.id);
                }
            }
            if (ids.length == 0) {
                swal(msg1, "", "warning");
                return;
            }

            swal({
                title: msgnew2,
                text: '確認要發送嗎',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: confirmButtonText,
                cancelButtonText: cancelButtonText
            }).then((result) => {
                if (result.value) {
                    $.post(BASE_URL + '/' + lang + '/' + fileName + '/send/mail', {
                        'ids': ids
                    }, function(data) {
                        if (data.success) {
                            swal("發送成功", "", "success");
                            $("#jqxGrid").jqxGrid('updatebounddata');
                            $("#jqxGrid").jqxGrid('clearselection');
                        } else {
                            swal(msgnew5, '', "error");
                        }
                    });

                } else {

                }
            });

        }
    },
];

var btnGroupSimpleLength = btnGroupSimple.length;
var btnGroupListLength = typeof btnGroupList == 'undefined' ? 0 : btnGroupList.length;
for (let i = 0; i < btnGroupListLength; i++) {
    for (let index = 0; index < btnGroupSimpleLength; index++) {
        if(btnGroupSimple[index].btnId == btnGroupList[i]) {
            btnGroup.push(btnGroupSimple[index]);
        }
    }
}

$.each(btnGroup, function(i, item) {
    var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
    btnHtml = btnHtml.replace("{btnId}", item.btnId);
    btnHtml = btnHtml.replace("{btnIcon}", item.btnIcon);
    btnHtml = btnHtml.replace("{btnText}", item.btnText);
    $("#btnArea").append(btnHtml);
    $("#" + item.btnId).on("click", function() {
        item.btnFunc();
    });
});

$('#updategrid').on('click', function(){
    swal.close();
    $("#jqxGrid").jqxGrid('updatebounddata');
    $("#jqxGrid").jqxGrid('clearselection');
});