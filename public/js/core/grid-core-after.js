var theme = 'bootstrap';
$.jqx.theme = theme;
gridOpt.gridId = "jqxGrid";
loadState();
gridOpt.getState = function() {
    loadState();
};
console.log(JSON.parse($("#datafields").val()));
var columnsdata = JSON.parse($("#columns").val());

$.grep(columnsdata, function(e){
    if(e.datafield == "image") {
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            var image = dataAdapter.records[row].image;
            var btn = "";

            if(image != undefined ) {
                btn = `<button class="btn btn-link" onclick="openPicture('{image}')">點我打開照片</button>`;
                btn = btn.replace("{image}", image);
            }
            
            return btn;
        };

        e.cellsrenderer = cellsrenderer;
    }

    if(e.datafield == "sys_ord_no" && tableName == "mod_message") {
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            var error_id = dataAdapter.records[row].error_id;
            var sys_ord_no = dataAdapter.records[row].sys_ord_no;
            var btn = "";
            if(error_id != '' && error_id != null ) {
                btn = `<button class="btn btn-link" onclick="openerror('{error_id}')">`+sys_ord_no+`</button>`;
                btn = btn.replace("{error_id}", error_id);
                return btn;
            } else {
                return defaulthtml;
            }
            
        };
        e.cellsrenderer = cellsrenderer;
    }

    if(e.datafield == "ord_no" && tableName == "mod_error_report") {
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            var ord_no = dataAdapter.records[row].ord_no;
            var btn = "";
            if(ord_no != '' && ord_no != null ) {
                btn = `<button class="btn btn-link" onclick="openorder('{ord_no}')">`+ord_no+`</button>`;
                btn = btn.replace("{ord_no}", ord_no);
            }
            return btn;
        };
        e.cellsrenderer = cellsrenderer;
    }
    if(e.datafield == "sign_pic") {

        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            var ord_no = dataAdapter.records[row].ord_no;
            var sign_pic = dataAdapter.records[row].sign_pic;
            var btn = "";
            if(ord_no != '' && ord_no != null && sign_pic != 0 && sign_pic !=null ) {
                btn = `<button class="btn btn-link" onclick="openSignPic('{ord_no}')">簽收照片連結</button>`;
                btn = btn.replace("{ord_no}", ord_no);
            } else {
                return defaulthtml;
            }
            return btn;
        };
        e.cellsrenderer = cellsrenderer;
    }
});
var fieldObj = null;
var source = {
    datatype: "json",
    // condition
    datafields: JSON.parse($("#datafields").val()),
    root: "Rows",
    pagenum: 0,
    beforeprocessing: function(data) {
        source.totalrecords = data[0].TotalRows;
        if (data[0].StatusCount != null && data[0].StatusCount.length > 0) {
            data[0].StatusCount.push({
                'count': data[0].TotalRows,
                'status': 'ALL',
                'statustext': 'ALL'
            });
        }

        if ($("#statusList").find('a').length == 0) {

            $("#statusList").html("");
            $.each(data[0].StatusCount, function(i, item) {
                $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> ' + item.count + '</span><i class="fa fa-bullhorn"></i><span class="text" id="' + item.status + '">' + item.statustext + '</span></a>');
            });

            $('.statusBtn').off().on('click', function() {
                $('#statusList').find('a').removeClass('active');
                $(this).addClass('active');

                var statusGroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = $(this).find('span.text').attr("id");
                if (filtervalue == "ALL") {
                    $("#jqxGrid").jqxGrid('clearfilters');
                } else {
                    var filtercondition = 'contains';
                    var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
                    statusGroup.addfilter(filter_or_operator, statusFilter1);
                    $("#jqxGrid").jqxGrid('addfilter', 'status', statusGroup);
                    $("#jqxGrid").jqxGrid('applyfilters');
                }

            });
        }
    },
    filter: function() {
        // update the grid and send a request to the server.
        $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
    },
    sort: function() {
        // update the grid and send a request to the server.
        $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
    },
    cache: false,
    pagesize: 50,
    url: BASE_URL + "/admin/baseApi/getGridJson/" + tableName + ((typeof condition  !== 'undefined') ? condition : '') 
}
var dataAdapter = new $.jqx.dataAdapter(source, {
    async: false,
    loadError: function(xhr, status, error) {
        alert('Error loading "' + source.url + '" : ' + error);
    },
    loadComplete: function() {
        $.get(gridOpt.getStatusCount, {}, function(res){
            console.log(res);
            res[0].StatusCount.push({'count': res[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
            genStatus(res, 'jqxGrid');
        }, 'JSON');
    }
});

function genStatus(data, gridId) {
    $("#statusList").html("");

    $.each(data[0].StatusCount, function(i, item) {
        $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> ' + item.count + '</span><i class="fa fa-bullhorn"></i><span class="text" id="' + item.status + '">' + item.statustext + '</span></a>');
    });

    $('.statusBtn').off().on('click', function() {
        $('#statusList').find('a').removeClass('active');
        $(this).addClass('active');

        var statusGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = $(this).find('span.text').attr("id");
        if (filtervalue == "ALL") {
            $("#" + gridId).jqxGrid('clearfilters');
        } else {
            var filtercondition = 'contains';
            var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
            statusGroup.addfilter(filter_or_operator, statusFilter1);
            $("#" + gridId).jqxGrid('addfilter', 'status', statusGroup);
            $("#" + gridId).jqxGrid('applyfilters');
        }

    });
}
var h = 300;

if (typeof enabledStatus !== "undefined") {
    if (enabledStatus == "Y") {
        h = 450;
    }
}
if(fileName == "dlvCarnew") {
    h = 700;
}
var winHeigt = $(window).height() - h;
if (typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
    winHeigt = 500;
}
console.log( JSON.parse($("#columns").val()));
$("#jqxGrid").jqxGrid({
    width: '100%',
    height: winHeigt,
    source: dataAdapter,
    sortable: true,
    filterable: true,
    altrows: true,
    showfilterrow: true,
    pageable: true,
    virtualmode: true,
    autoshowfiltericon: true,
    columnsreorder: true,
    columnsresize: true,
    columnsautoresize: true,
    clipboard: true,
    selectionmode: 'checkbox',
    enablebrowserselection: true,
    pagesizeoptions: [50, 100, 500, 2000, 9999],
    rendergridrows: function(params) {
        //alert("rendergridrows");
        return params.data;
    },
    ready: function() {
        $("#jqxLoader").jqxLoader({
            width: 100,
            height: 60,
            imagePosition: 'top'
        });
        // $("#jqxLoader").jqxLoader('open');
        //$('#' + gridId).jqxGrid('autoresizecolumns');
        // loadState();
    },
    updatefilterconditions: function(type, defaultconditions) {
        var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
        var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
        var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
        var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
        switch (type) {
            case 'stringfilter':
                return stringcomparisonoperators;
            case 'numericfilter':
                return numericcomparisonoperators;
            case 'datefilter':
                return datecomparisonoperators;
            case 'booleanfilter':
                return booleancomparisonoperators;
        }
    },
    columns: columnsdata
});
gridOpt.gridId = "jqxGrid";
$("#jqxGrid").on('bindingcomplete', function(event) {
    var statusHeight = 0;
    $('#jqxLoader').jqxLoader('close');
    if (typeof enabledStatus !== "undefined") {
        if (enabledStatus == "Y") {
            statusHeight = -100;
        }
    }
    if(fileName == "dlvCarnew") {
        statusHeight = -100;
    }
    var winHeight = $(window).height() - 300 + statusHeight;
    $("#jqxGrid").jqxGrid('height', winHeight + 'px');
});
$("#jqxGrid").on("rowselect", function(event) {
    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
    $("#selectcount").text(rows.length);
});
$('#jqxGrid').on('rowunselect', function(event) {
    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
    $("#selectcount").text(rows.length);
});

function loadState() {
    var loadURL = BASE_URL + '/admin/baseApi/getLayoutJson';
    $.ajax({
        type: "GET", //  OR POST WHATEVER...
        url: loadURL,
        data: {
            key: gridOpt.pageId,
            lang: lang
        },
        success: function(response) {
            gridOpt.gridId = "jqxGrid";
            if (response != "" && response != 'false') {
                response = JSON.parse(response);
                $("#" + gridOpt.gridId).jqxGrid('loadstate', response);
            }

            var listSource = [];

            state = $("#" + gridOpt.gridId).jqxGrid('getstate');
            console.log(state);
            $('#jqxLoader').jqxLoader('close');
            $.get(BASE_URL + "/searchList/get/" + gridOpt.pageId, {}, function(data) {
                if (data.msg == "success") {
                    var opt = "<option value=''>請選擇</option>";

                    for (i in data.data) {
                        if (data.data[i]["layout_default"] == "Y") {
                            opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                            $("input[name='searchName']").val(data.data[i]["title"]);
                        } else {
                            opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                        }
                    }

                    $("select[name='selSearchName']").html(opt);
                    // searchHtml

                    $.get(BASE_URL + "/searchHtml/get/" + gridOpt.pageId +"/"+ $("select[name='selSearchName']").val(), {}, function(data) {
                        if (data.msg == "success") {
                            if (data.data != null) {
                                $("#searchContent").html(data.data["data"]);
                            } else {
                                var seasrchTpl = getSearchTpl(state);
                                initSearchWindow(seasrchTpl);
                            }

                            var offset = $(".content-wrapper").offset();
                            $('#searchWindow').jqxWindow({
                                position: {
                                    x: offset.left + 50,
                                    y: offset.top + 50
                                },
                                showCollapseButton: true,
                                maxHeight: 400,
                                maxWidth: 700,
                                minHeight: 200,
                                minWidth: 200,
                                height: 300,
                                width: 700,
                                autoOpen: false,
                                initContent: function() {
                                    $('#searchWindow').jqxWindow('focus');
                                }
                            });


                            $("button[name='winSearchAdd']").on("click", function() {
                                var seasrchTpl = getSearchTpl(state);
                                $("#searchContent").append(searchTpl);
                            });

                            $(document).on("click", "button[name='winBtnRemove']", function() {
                                $(this).parents(".row").remove();
                            });

                            $(document).on("change", "select[name='winField[]']", function() {
                                $('option:selected', this).siblings().removeAttr('selected');
                                $('option:selected', this).attr('selected', 'selected');
                                var val = $(this).val();
                                var info = searchObj(val, fieldObj[0]);
                                var str = [{
                                        val: 'CONTAINS',
                                        label: '包含'
                                    },
                                    {
                                        val: 'EQUAL',
                                        label: '等於'
                                    },
                                    {
                                        val: 'NOT_EQUAL',
                                        label: '不等於'
                                    },
                                    {
                                        val: 'NULL',
                                        label: 'NULL'
                                    },
                                    {
                                        val: 'NOT_NULL',
                                        label: 'NOT NULL'
                                    },
                                ];

                                var num = [{
                                        val: 'EQUAL',
                                        label: '等於'
                                    },
                                    {
                                        val: 'NOT_EQUAL',
                                        label: '不等於'
                                    },
                                    {
                                        val: 'LESS_THAN',
                                        label: '小於'
                                    },
                                    {
                                        val: 'LESS_THAN_OR_EQUAL',
                                        label: '小於等於'
                                    },
                                    {
                                        val: 'GREATER_THAN',
                                        label: '大於'
                                    },
                                    {
                                        val: 'GREATER_THAN_OR_EQUAL',
                                        label: '大於等於'
                                    },
                                    {
                                        val: 'NULL',
                                        label: 'NULL'
                                    },
                                    {
                                        val: 'NOT_NULL',
                                        label: 'NOT NULL'
                                    },
                                ];

                                var opt = "";
                                if (info.type == "string") {
                                    for (i in str) {
                                        opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                    }
                                } else {
                                    for (i in num) {
                                        opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                    }
                                }
                                $($(this).parent().siblings()[0]).find("select").html(opt);
                            });

                            $(document).on("change", "select[name='winOp[]']", function() {
                                $('option:selected', this).siblings().removeAttr('selected');
                                $('option:selected', this).attr('selected', 'selected');
                            });

                            $(document).on("change", "input[name='winContent[]']", function() {
                                $(this).attr('value', $(this).val());
                            });

                            $("button[name='winSearchBtn']").on("click", function() {
                                var winField = [];
                                var winContent = [];
                                var winOp = [];
                                $("select[name='winField[]']").each(function() {
                                    winField.push($(this).val());
                                });
                                $("input[name='winContent[]']").each(function() {
                                    winContent.push($(this).val());
                                });
                                $("select[name='winOp[]']").each(function() {
                                    winOp.push($(this).val());
                                });

                                addfilter(winField, winContent, winOp);
                            });
                        }
                    });

                }
            });

            $.each(state.columns, function(i, item) {
                if (item.text != "" && item.text != "undefined") {
                    listSource.push({
                        label: item.text,
                        value: i,
                        checked: !item.hidden
                    });
                    if (!item.hidden) {
                        var headerdata = {
                            'filed_text': item.text,
                            'filed_name': i,
                            'show': item.hidden
                        };
                        enabledheader.push(headerdata);
                    }
                }
            });

            $("#jqxlistbox").jqxListBox({
                allowDrop: true,
                allowDrag: true,
                source: listSource,
                width: "99%",
                height: 500,
                checkboxes: true,
                filterable: true,
                searchMode: 'contains'
            });
        }
    });
}
$("#saveGrid").on("click", function() {
    var items = $("#jqxlistbox").jqxListBox('getItems');
    //$('#jqxGrid').jqxGrid('clear');
    // enabledheader
    enabledheader    = [];
    $("#" + gridOpt.gridId).jqxGrid('beginupdate');
    // var key = 0;
    $.each(items, function(i, item) {
        console.log($('#' + gridOpt.gridId).jqxGrid('getcolumnindex', item.value));
        var thisIndex = $('#' + gridOpt.gridId).jqxGrid('getcolumnindex', item.value) - 1;
        if (thisIndex != item.index) {
            //console.log(item.value+":"+thisIndex+"="+item.index);
            $('#' + gridOpt.gridId).jqxGrid('setcolumnindex', item.value, item.index+1 );
        }
        // $('#' + gridOpt.gridId).jqxGrid('setcolumnindex', item.value, key);
        // key++;
        if (item.checked) {
            $("#" + gridOpt.gridId).jqxGrid('showcolumn', item.value);
            var headerdata = {
                'filed_text': item.label,
                'filed_name': item.value,
                'show': item.hidden
            };
            enabledheader.push(headerdata);

        } else {
            $("#" + gridOpt.gridId).jqxGrid('hidecolumn', item.value);
        }
    })

    $("#" + gridOpt.gridId).jqxGrid('endupdate');
    state = $("#" + gridOpt.gridId).jqxGrid('getstate');

    var saveUrl = BASE_URL + '/admin/baseApi/saveLayoutJson';
    var stateToSave = JSON.stringify(state);
    var savekey = gridOpt.pageId;
    if(typeof savelayoutkey !== "undefined" ) {
        savekey = savelayoutkey
    }
    $.ajax({
        type: "POST",
        url: saveUrl,
        data: {
            data: stateToSave,
            key: savekey,
            table_name: tableName,
            lang: lang
        },
        success: function(response) {
            if (response == "true") {
                alert("save successful");
                $('#gridOptModal').modal('hide');
            } else {
                alert("save failded");
            }

        }
    });
});

$("#clearGrid").on("click", function() {
    $.ajax({
        type: "POST",
        url: BASE_URL + '/admin/baseApi/clearLayout',
        data: {
            key: gridOpt.pageId
        },
        success: function(response) {
            if (response == "true") {
                alert("clear successful");
                location.reload();
                $('#gridOptModal').modal('hide');
            } else {
                //alert("save failded");
            }

        }
    });
});
// function myFunctionchange() {
//     var x = document.getElementById("swal-input1").value;
//     // console.log(x);
//     $("#swal-input2").text(x);
//     // document.getElementById("demo").innerHTML = "You selected: " + x;
// }
$("select[name='selSearchName']").on("change", function() {
    var text = $('option:selected', this).text();
    if ($(this).val() == '') {
        text = "";
    }
    $("input[name='searchName']").val(text);

    $.get(BASE_URL + "/searchHtml/get/" + $(this).val(), {}, function(data) {
        //console.log(data.data["data"]);
        $("#searchContent").html(data.data["data"]);
    });
})

function initSearchWindow(searchTpl) {
    $("#searchContent").append(searchTpl);
}

function getSearchTpl(state) {
    var fields = [];
    $.each(state.columns, function(i, item) {
        if (item.hidden == false && item.text != "") {
            var a = {
                label: item.text,
                value: i
            }
            fields.push(a);
        }
    });

    var fieldStr = "";
    for (i in fields) {
        fieldStr += '<option value="' + fields[i].value + '">' + fields[i].label + '</option>';
    }




    searchTpl = '<div class="row">\
                        <div class="col-xs-3">\
                            <select class="form-control input-sm" name="winField[]">' + fieldStr + '</select>\
                        </div>\
                        <div class="col-xs-3">\
                            <select class="form-control input-sm" name="winOp[]">\
                            <option value="CONTAINS">包含</option>\
                            <option value="CONTAINSLIKE">包含(多值)</option>\
                            <option value="EQUAL">等於</option>\
                            <option value="NOT_EQUAL">不等於</option>\
                            <option value="NULL">NULL</option>\
                            <option value="NOT_NULL">NOT NULL</option>\
                            <option value="LESS_THAN">小於</option>\
                            <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                            <option value="GREATER_THAN">大於</option>\
                            <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                            </select>\
                        </div>\
                        <div class="col-xs-4">\
                            <input type="text" class="form-control input-sm" name="winContent[]">\
                        </div>\
                        <div class="col-xs-2">\
                            <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove">-</button>\
                        </div>\
                    </div>';
    return searchTpl;
}

$("#jqxGrid").on("rowdoubleclick", function(event) {
    // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
    var args = event.args;
    // row's bound index.
    var boundIndex = args.rowindex;
    // row's visible index.
    var visibleIndex = args.visibleindex;
    // right click.
    var rightclick = args.rightclick;
    // original event.
    var ev = args.originalEvent;
    if(typeof toedit !== "undefined" && toedit == "false") {
        return;
    }
    
    var datarow = $("#" + gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
    console.log(datarow);

    // status
    if(typeof customdirect !== "undefined") {
        // if(tableName == "mod_application_form_bymy" ) {
        //     if(datarow.type_descp == "轉移作業" && datarow.status == "SUCCESS") {
        //         location.href = BASE_URL  + "/" +lang +"/"+'pickingindex'+"/" + datarow.application_no ;
        //         return;
        //     } else {
        //         var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
        //         window.open(editUrl);
        //         return;
        //     }
        // }
        var editUrl = BASE_URL  + "/" +lang +"/"+customdirect+"/" + datarow.id ;
        window.open(editUrl);
        return;
    }

    var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
    //location.href = editUrl;
    window.open(editUrl);
})

var addfilter = function(datafield, filterval, filtercondition) {
    $("#jqxGrid").jqxGrid('clearfilters');
    var old_filed = "";
    for (i in datafield) {
        var filtergroup = new $.jqx.filter();
        if (old_filed != datafield[i]) {
            old_filed = datafield[i];

            var filter_or_operator = 1;
            //var filtervalue = filterval;
            var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
            if (datafield[i] == "updated_at" || datafield[i] == "created_at" || datafield[i] == "confirm_dt" || datafield[i] == "finish_date" || datafield[i] == "etd") {
                filter = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                filtergroup.addfilter(filter_or_operator, filter);
            } else {
                filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                filtergroup.addfilter(filter_or_operator, filter);
            }

            // add the filters.
            $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);
        } else {
            var filter3 = filtergroup.createfilter('datefilter', filterval[i - 1], filtercondition[i - 1]);
            filtergroup.addfilter(0, filter3);
            $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);

            var filter2 = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
            filtergroup.addfilter(0, filter2);
            $("#jqxGrid").jqxGrid('addfilter', datafield[i], filtergroup);
        }
    }
    // apply the filters.
    $("#jqxGrid").jqxGrid('applyfilters');
}


function searchObj(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].name === nameKey) {
            return myArray[i];
        }
    }
}

$('#searchWindow').on('open', function(event) {
    if ($("body").width() < 400) {
        var offset = $("body").offset();
        $('#searchWindow').jqxWindow({
            height: 500,
            width: $("body").width(),
            position: {
                x: 0,
                y: offset.top + 150
            },
        });
    } else {

    }

});
$('#updategrid').on('click', function() {
    $("#jqxGrid").jqxGrid('updatebounddata');
    $("#jqxGrid").jqxGrid('clearselection');
});
$("button[name='setDefault']").on("click", function() {
    $.ajax({
        url: BASE_URL + "/setSearchDefault/" + gridOpt.pageId + "/" + $("select[name='selSearchName']").val(),
        dataType: "JSON",
        method: "PUT",
        success: function(result) {
            if (result.msg == "success") {
                swal("儲存成功", "", "success");
            } else {
                swal("操作失敗", "", "error");
            }
        },
        error: function() {
            swal("網路似乎出了問題，請稍後再試", "", "error");
        }
    });
});

$("button[name='delSearch']").on("click", function() {
    $.ajax({
        url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName']").val(),
        dataType: "JSON",
        method: "post",
        success: function(result) {
            if (result.msg == "success") {
                swal("刪除成功", "", "success");
                $("select[name='selSearchName'] option:selected").remove();
                $("input[name='searchName']").val("");
            } else {
                if (typeof data.errorMsg !== "undefined") {
                    swal("操作失敗", data.errorMsg, "error");
                } else {
                    swal("操作失敗", "", "error");
                }

            }
        },
        error: function() {
            swal("網路似乎出了問題，請稍後再試", "", "error");
        }
    });
});

$("button[name='saveSearch']").on("click", function() {
    var htmlData = $("#searchContent").html();
    var postData = {
        "key": gridOpt.pageId,
        "data": htmlData,
        "title": $("input[name='searchName']").val(),
        "id": ($("select[name='selSearchName'] option:selected").text() == $("input[name='searchName']").val()) ? $("select[name='selSearchName']").val() : null
    };
    $.ajax({
        url: BASE_URL + '/' + lang  + "/saveSearchLayout",
        data: postData,
        dataType: "JSON",
        method: "POST",
        success: function(result) {
            if (result.msg == "success") {
                swal("儲存成功", "", "success");
                $("select[name='selSearchName'] option").removeAttr("selected");
                $("select[name='selSearchName']").append("<option value='" + result.id + "' selected>" + $("input[name='searchName']").val() + "</option>")
            } else {
                swal("操作失敗", "", "error");
            }
        },
        error: function() {
            swal("請輸入查詢條件名稱", "", "error");
        }
    });
});
/* START */
var picsignsysno = 0;
var picerrsysno = 0;
var sysnonow = 0;
$('#updategridsign').on('click', function(imgtype) {
    console.log("success close window");
    document.getElementById(picsignsysno + "sign").style.background = "white";
});
$('#updategriderror').on('click', function(imgtype) {
    document.getElementById(picerrsysno + "error").style.background = "white";
    console.log("success close window");
});
Date.prototype.format = function(format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
/* END */


/* 匯入 START */
$(function() {
    $("#myForm").submit(function () {
        var postData = new FormData($(this)[0]);

        $.ajax({
        url: BASE_URL + '/' + lang + '/' + fileName + '/excel/import',
        type: 'POST',
        data: postData,
        async: false,
        beforeSend: function () {
            
        },
        error: function () {
            swal("excel 匯入發生錯誤", "", "error");
            $('#myForm')[0].reset();
            return false;
        },
        success: function (data) {
            if(!data.success) {
                swal("excel 匯入發生錯誤", data.message, "error");
                $('#myForm')[0].reset();
                return;
            }

            swal("匯入成功", "", "success");
            $('#myForm')[0].reset();
            $("#jqxGrid").jqxGrid('updatebounddata');
            $("#jqxGrid").jqxGrid('clearselection');
        },
        cache: false,
        contentType: false,
        processData: false
        });
            return false;
        });
        
        $("#excelFile").on("change", function(){
            $("#myForm").submit();
        });
})
/* 匯入 END */