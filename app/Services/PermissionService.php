<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\PermissionRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class PermissionService {

    protected $permissionRepo;
    protected $baseModel;
    
    public function __construct (PermissionRepository $permissionRepo, BaseModel $baseModel) {
        $this->permissionRepo = $permissionRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $permission = $this->permissionRepo->get($id);
        if ($permission) {
            return $permission;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        if (isset($data['name'])) {
            $permission = $this->permissionRepo->getByName($data['name']);

            if ($permission) {
                throw new Exception(trans('common.msgnew36') );
            }
        }
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $data['created_by'] = $user->email;
        $data['updated_by'] = $user->email;
        $data['g_key'] = $user->g_key;
        $data['c_key'] = $user->c_key;
        $data['s_key'] = $user->s_key;
        $data['d_key'] = $user->d_key;
        $permission = $this->permissionRepo->create($data);
        if (!$permission) {
            throw new Exception('新增失敗');
        } 
        return $permission;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_at']      = date("Y-m-d H:i:s");
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $permission = $this->permissionRepo->update($id, $data);
        if (!$permission) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $permission = $this->permissionRepo->delete($id);

        if (!$permission) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $permissionIds = $request->ids;

        foreach($permissionIds as $permissionId) {
            $this->permissionRepo->delete($permissionId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('permissions', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('permissions', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'permission_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('permission_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

}