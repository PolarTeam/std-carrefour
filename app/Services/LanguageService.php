<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\LanguageRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Log;
class LanguageService {

    protected $languageRepo;
    protected $baseModel;

    public function __construct (LanguageRepository $languageRepo, BaseModel $baseModel) {
        $this->languageRepo = $languageRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $language = $this->languageRepo->get($id);
        if ($language) {
            return $language;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getdetail ($id) {
        $language = $this->languageRepo->get($id);

        $detail = $this->languageRepo->getdetail($language->func_name);

        if ($detail) {
            return $detail;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $user = Auth::user();
        $data = $request->all();
        
        $language = $this->languageRepo->create($data);
        if (!$language) {
            throw new Exception('新增失敗');
        } 
        return $language;
    }

    public function update (Request $request, $id) {
        $data     = $request->all();
        $detail   = array();
        $enData   = array();
        $twData   = array();
        $langName = '';
        foreach ($data as $key => $row) {
            $checkdata = explode('-', $key);
            if(is_array($checkdata) && count($checkdata) == 4 ) {
                $detailId = $checkdata[0];
                if($checkdata[3] == "en") {
                    $enData[$checkdata[2]] = $row;
                    $detail['en_name']     = $row;
                } else if($checkdata[3] == "tw") {
                    $twData[$checkdata[2]] = $row;
                    $detail['tw_name']     = $row;
                }

                $this->languageRepo->updatedetail($detailId, $detail);
                $langName = $checkdata[1];
            }
        }
        $encontent = '<?php return [';
        foreach ($enData as $key => $row) {
            // $resultStr =  $this->baseModel->tolanguageStr($key);
            $encontent .= '"'.$key.'"=>"'. $row.'",';
        }
        $encontent .= '];'; 
        Storage::disk('languagepath')->put('en/'.$langName.".php", $encontent);


        $twcontent = '<?php return [';
        foreach ($twData as $key => $row) {
            // $resultStr =  $this->baseModel->tolanguageStr($key);
            $twcontent .= '"'.$key.'"=>"'. $row.'",';
        }

        $twcontent .= '];';
        Storage::disk('languagepath')->put('zh-TW/'.$langName.".php", $twcontent);

    }

    public function delete ($id) {
        $language = $this->languageRepo->delete($id);

        if (!$language) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $languageIds = $request->ids;

        foreach($languageIds as $languageId) {
            $this->languageRepo->delete($languageId);
        }

        return true;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('function_schema_lang', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'schema_lang/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('schema_lang/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

}