<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\UserMgmtRepository;
use App\Repositories\PermissionUsersRepository;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserService {

    protected $userRepo;
    protected $permissionUserRepo;
    protected $baseModel;

    public function __construct (
        UserMgmtRepository $userRepo,
        BaseModel $baseModel, PermissionUsersRepository $permissionUserRepo) {
        $this->userRepo           = $userRepo;
        $this->permissionUserRepo = $permissionUserRepo;
        $this->baseModel          = $baseModel;
    }

    public function get ($id) {
        
        $users = $this->userRepo->get($id);
        if ($users) {
            return $users;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $createuser         = Auth::user();
        $data = $request->all();
        $password =  bcrypt($request->password) ;
        $data['password'] = $password;

        if (isset($data['email'])) {
            $user = $this->userRepo->getByName($data['email']);

            if ($user) {
                throw new Exception(trans('common.msgnew49'));
            }
        }
        // if(isset($data['check_wh']) && isset($data['manage_warehouse'])  ) {
        //     $data['check_wh'][] = $data['manage_warehouse'];
        // }
        $chooseroel = $data['role'];
        $data['expire_date'] = '2023-12-31';

        if(in_array('*',$data['check_owner'])) {
            $data['check_owner_name']      = 'ALL,'.implode(',',$this->baseModel->getCustomerGroupName($data['check_owner']));
        }
        else {
            $data['check_owner_name']      =  implode(',',$this->baseModel->getCustomerGroupName($data['check_owner']));
        }

        $data['delivery_wh_name'] = implode(',',$this->baseModel->getWarehouseName($data['delivery_wh']));
        $data['delivery_wh']      = implode(',', $data['delivery_wh']);
        $data['check_wh_name']    = implode(',',$this->baseModel->getWarehouseName($data['check_wh']));
        $data['role_name']        = implode(',',$this->baseModel->getRoleNames($data['role']));
        $data['role']             = implode(',', $data['role']);
        $data['check_wh']         = implode(',', $data['check_wh']);
        $data['check_owner']      = implode(',', $data['check_owner']);
        $data['username']         = $data['email'];
        $data['g_key']            = 'DSS';
        $data['c_key']            = 'DSS';
        $data['s_key']            = 'DSS';
        $data['d_key']            = 'DSS';
        $data['web_user']         = 'Y';
        $data['created_by']       = $createuser->email;
        $data['updated_by']       = $createuser->email;
        $users              = $this->userRepo->create($data);

        $roles = $this->userRepo->getRoles();
        $user = $this->userRepo->get($users['id']);
        foreach($roles as $role) {
            $user->removeRole($role['name']);
        }

        $roleData =  $chooseroel;
        foreach($roleData as $roleId) {
            $user->assignRole($roleId);;
        }

        if (!$users) {
            throw new Exception('新增失敗');
        } 
        return $users;
    }
    
    public function checkexpire (Request $request) {
        $data    = $request->all();
        $user    = Auth::user();
        $checkuser = $this->userRepo->getBymail($data['email']);
        $now = strtotime(date("Y-m-d 00:00:00"));
        $expireDate = strtotime($checkuser->expire_date);
        if($now >= $expireDate) {
            return true;
        } else {
            return false;
        }
    }
    public function expireupdate (Request $request) {
        $data    = $request->all();
        $user    = Auth::user();
        $ogusers = $this->userRepo->getBymail($data['email']);
        unset($data['email']);

        if (Hash::check($data['newpasswordconfirm'], $ogusers->password)) { 
            throw new Exception('密碼不可重複');
        }
        if (!Hash::check($data['password'], $ogusers->password)) { 
            throw new Exception('密碼錯誤');
        }
        if($request->newpassword != null ) {
            $password   = bcrypt($request->newpassword);
            $data['password'] = $password;
        } else {
            unset($data['password']);
            unset($data['password_confirmation']);
        }
        $encryptpassword = base64_encode($data['newpasswordconfirm']);
        $check = $this->userRepo->checkPasswordHistory($ogusers['id'], $encryptpassword);
        foreach ($check as $checkkey => $checkrow) {
            if($checkrow->password == $encryptpassword) {
                throw new Exception('密碼10次內不可重複');
            }
        }
        $passwordHistory = array();
        $data['expire_date'] = '2023-12-31';
        $passwordHistory['user_id']  = $ogusers['id'];
        $passwordHistory['password'] = $encryptpassword;
        $this->userRepo->createPasswordHistory($passwordHistory);

        $nextupdate = date('Y-m-d h:i:s', strtotime('+90 days'));
        $data['expire_date'] =  $nextupdate;
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        
        // 1012test@gamil.com

        $this->userRepo->update($ogusers['id'], $data);

    }
    public function update (Request $request, $id) {
        $data         = $request->all();
        $user         = Auth::user();
        $ogusers = $this->userRepo->get($id);
        if($request->password != null ) {
            $password   = bcrypt($request->password);
            $data['password'] = $password;
        } else {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $chooseroel = $data['role'];
        if(isset($data['check_owner'])){
            if(in_array('*',$data['check_owner'])) {
                $data['check_owner_name'] = 'ALL,'.implode(',',$this->baseModel->getCustomerGroupName($data['check_owner']));
            }
            else {
                $data['check_owner_name'] =  implode(',',$this->baseModel->getCustomerGroupName($data['check_owner']));
            }
        }
        if(isset($data['check_wh'])){
            $data['check_wh_name'] =  implode(',',$this->baseModel->getWarehouseName($data['check_wh']));
        }
        if(isset($data['delivery_wh'])){
            $data['delivery_wh_name']  =  implode(',',$this->baseModel->getWarehouseName($data['delivery_wh']));
            $data['delivery_wh']      = implode(',', $data['delivery_wh']);
        } else {
            $data['delivery_wh_name'] = '';
            $data['delivery_wh']      = '';
        }
        $data['web_user']    = 'Y';
        $data['role_name']   = implode(',',$this->baseModel->getRoleNames($data['role']));
        $data['role']        = implode(',', $data['role']);
        $data['check_owner'] = implode(',', $data['check_owner']);
        $data['check_wh']    = implode(',', $data['check_wh']);
        if(isset($data['email'])) {
            $data['username'] = $data['email'];
        }

        $data['updated_by']  = $user->email;
        foreach ($data as $key => $value) {
            if($key != 'delivery_wh_name' && $key != 'delivery_wh') {
                if($value == "false" || $value == null) {
                    unset($data[$key]);
                }
            }
        }
        $data['g_key']    = "DSS";
        $data['c_key']    = "DSS";
        $data['s_key']    = "DSS";
        $data['d_key']    = "DSS";
        $userMgmt        = $this->userRepo->update($id, $data);

        $roles = $this->userRepo->getRoles();
        $user = $this->userRepo->get($id);
        foreach($roles as $role) {
            $user->removeRole($role['name']);
        }

        $roleData =  $chooseroel;
        foreach($roleData as $roleId) {
            $user->assignRole($roleId);;
        }

        if (!$userMgmt) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $users = $this->userRepo->delete($id);

        if (!$users) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $usersId = $request->ids;

        foreach($usersId as $userId) {
            $this->userRepo->delete($userId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('users', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function queryuser() {

        $result  =  $this->userRepo->getByuser();

        return $result;
    }
    public function getUserPermission($userId) {
        // $user = Auth::user();
        $result  =  $this->permissionUserRepo->getByuser($userId);
        return $result;
    }

    public function setDashboardReadList (Request $request) {
        $data         = $request->all();
        $user         = Auth::user();
        $id           = $user->id;
        $data['updated_by'] = $id;

        $userMgmt        = $this->userRepo->update($id, $data);
        if (!$userMgmt) {
            throw new Exception('更新失敗');
        }
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        $baseCondition[] = ['web_user', '=', 'Y'];
        $result        = $this->baseModel->baseQuery('users', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'users/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('users/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

}