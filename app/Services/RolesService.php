<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\RolesRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Log;
class RolesService {

    protected $rolesRepo;
    protected $permissionRepo;
    protected $baseModel;

    public function __construct (RolesRepository $rolesRepo, PermissionRepository $permissionRepo, BaseModel $baseModel) {
        $this->rolesRepo = $rolesRepo;
        $this->permissionRepo = $permissionRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $role = $this->rolesRepo->get($id);
        if ($role) {
            return $role;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function getwebPermission () {
        
        $filed = $this->permissionRepo->getwebPermission();
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }
    public function getRolePermission ($id) {
        $permissionid = $this->permissionRepo->getRolePermission($id);
        $permissionName = $this->permissionRepo->getPermissionNameByid($permissionid);

        if ($permissionName) {
            return $permissionName;
        } else {
            return null;
        }
    }
    public function getappPermission () {
        
        $filed = $this->permissionRepo->getappPermission();
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        if (isset($data['display_name'])) {
            $role = $this->rolesRepo->getByName($data['display_name']);

            if ($role) {
                throw new Exception(trans('common.msgnew36'));
            }
        }
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $data['created_by'] = $user->email;
        $data['updated_by'] = $user->email;

        $role = $this->rolesRepo->create($data);
        if (!$role) {
            throw new Exception('新增失敗');
        } 
        return $role;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_at']      = date("Y-m-d H:i:s");
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $role = $this->rolesRepo->update($id, $data);
        if (!$role) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $role = $this->rolesRepo->delete($id);

        if (!$role) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $roleIds = $request->ids;

        foreach($roleIds as $roleId) {
            $this->rolesRepo->delete($roleId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('roles', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('roles', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'role_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('role_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

}