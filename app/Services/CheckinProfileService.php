<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\CheckinRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class CheckinProfileService {

    protected $checkinRepo;
    protected $baseModel;
    
    public function __construct (CheckinRepository $checkinRepo, BaseModel $baseModel) {
        $this->checkinRepo = $checkinRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $checkin = $this->checkinRepo->get($id);
        if ($checkin) {
            return $checkin;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }
    public function getfinalcheckin (Request $request) {
        Log::info('getfinalcheckin');
        Log::info($request->all());
        $data = $request->all();
        $today = date("Y-m-d 00:00:00");
        //2023-08-08 只有當天有報到 判斷進貨 拉空櫃
        $finalcheckin = DB::table('mod_driver_checkin')
        ->whereIn('job_type_desc', ['進貨','拉空櫃'])
        ->where('phone', $data['phone'])
        ->where('created_at', '>=', $today)
        ->orderBy('id','desc')
        ->first();

        return $finalcheckin;
    }
    public function create (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        $today = date("Y-m-d");
        $now = date("Y-m-d H:i:s");
        $prepdata = DB::table('mod_dss_dlv')
        ->where('driver_phone', $data['phone'])
        ->orderBy('created_at','desc')
        ->first();
        
        Log::info($request->all());

        $graceTime = DB::table('bscode')
        ->where('cd_type', 'GRACETIME')
        ->where('cd', 'gracetime')
        ->first();

        $truck = DB::table('mod_car_trader')
        ->where('cust_name',$data['truck_cmp_name'])
        ->first();

        $container = DB::table('bscode')
        ->where("cd_type","container")
        ->where('cd',$data['con_type'])
        ->first();

        $carType = DB::table('bscode')
        ->where("cd_type","IMPORTCARTYPE")
        ->where('cd', $data['car_type'])
        ->first();

        $whdata = DB::table('mod_warehouse')
        ->where('cust_no', $data['dc_id'])
        ->first();


        if(!empty($data['con_id'])) {
            $checkCon = $this->baseModel->checkCon($data['con_id']);
            if(!$checkCon) {
                throw new Exception('櫃號檢查異常');
            }
        }
        $data['entry_time'] = date("Y-m-d H:i:s");
        if(isset( $prepdata)) {
            //出貨
            $data['mark_no'] = $prepdata->mark_no;
        } else {


            $nexttime = strtotime(date("Y-m-d H:i:00").' + 15 minute');
            $nexttime = date('Y-m-d H:i:s', $nexttime);

            if (empty($data['con_id'])) {
                $reserveMark = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date',$today)
                ->where('cust_import_no',$data['cust_ord_no'])
                ->where('con_no',$data['con_id'])
                ->count();
            } else {
                $reserveMark = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date',$today)
                ->where('cust_import_no',$data['cust_ord_no'])
                ->count();
            }
            
            $reservefinal = DB::table('mod_mark_reserve')
            ->where('dc_id',$data['dc_id'])
            ->whereNull('mark_no')
            ->where('reserve_date',$today)
            ->where('cust_import_no',$data['cust_ord_no'])
            ->orderBy('id','desc')
            ->first();
            $finalentry = $now;
            //
            if(isset($reservefinal)) {
                $finalentry =  strtotime($reservefinal->full_date.' + '.$graceTime->value1.' minute');
                $canentry = strtotime($now);
                $result = $finalentry - $canentry;
            } else {
                $result = 0;
            }
            if ($reserveMark > 0  && $result >= 0) {


                // $carType->cd_descp = empty($data['con_type']) ? $carType->cd_descp : '貨櫃';
                // Log::info('預約 貨櫃');
                // Log::info($carType->cd_descp);
                
                $canuseMark = DB::table('mod_mark')
                ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                ->where('is_enabled','Y')
                ->where('type','!=','code4')
                ->where('in_use','N')
                ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                ->where('owner_cd', 'like', '%'.$data['owner_cd'].'%')
                ->first();

                if(!isset($canuseMark)) {
                    $canuseMark = DB::table('mod_mark')
                    ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                    ->where('is_enabled','Y')
                    ->where('type','!=','code4')
                    ->where('in_use','N')
                    ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                    ->whereNull('owner_cd')
                    ->first();
                }
                
                $processTime = !empty($reservefinal->process_time) ? $reservefinal->process_time : 0;

                if (($processTime == 0 || $processTime == null) && $data['job_type'] != '拉空櫃' ) {
                    $marksetting = DB::table('mod_mark_setting')
                    ->where('box_num_end', '>=', $data['box_num'])
                    ->where('items_num_end', '>=', $data['items_num'])
                    ->where('warehouse_id', 'like', '%'.$data['dc_id'].'%')
                    ->where('car_type',  $data['car_type'])
                    ->orderBy('box_num','asc')
                    ->orderBy('items_num','asc')
                    ->orderBy('id','asc')
                    ->first();
        
                    if(isset($marksetting->process_time)) {
                        $processTime = $marksetting->process_time + $marksetting->trans_time;
                        $data['process_time'] = $processTime;
                    }
                }
                
                $newtimestamp = strtotime(date("Y-m-d H:i:00").' + '. $processTime.' minute');
                $data['process_time']   = $processTime;
                $endat = date('Y-m-d H:i:s', $newtimestamp);
            } else {

                //
                // Log::info('沒預約 貨櫃');
                // Log::info($carType->cd_descp);
                // Log::info('test 1');
                // $carType->cd_descp = empty($data['con_type']) ? $carType->cd_descp : '貨櫃';
                // if($carType->cd_descp == "小車" || $carType->cd_descp =="大車") {
                //     $carType->cd_descp = mb_substr($carType->cd_descp ,0,1);
                // }
                Log::info('test 2');
                Log::info('判斷mod_mark');

                // 假設未預約司機 9:10到場 扣到預留時間10分鐘為 9:00 這時候去查有沒有 9:10 >= x >=9:00 的預約司機 表示這個未預約司機需等這個預約司機
                $newtimestamp = strtotime(date("Y-m-d H:i:00").' - '. $graceTime->value1.' minute');
                $reserveM = date('i', $newtimestamp);

                $countReserveByOwner = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date', date("Y-m-d"))
                ->where('reserve_h', date("H"))
                ->where('reserve_m', '<=', date("i"))
                ->where('reserve_m', '>=', $reserveM)
                ->where('onsite','N')
                ->where('owner_no', $data['owner_cd'])
                ->count();

                $countReserve = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date', date("Y-m-d"))
                ->where('reserve_h', date("H"))
                ->where('reserve_m', '<=', date("i"))
                ->where('reserve_m', '>=', $reserveM)
                ->where('onsite','N')
                ->count();

                $canuseMark = DB::table('mod_mark')
                ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                ->where('is_enabled','Y')
                ->where('type','!=','code4')
                ->where('in_use','N')
                ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                ->where('owner_cd', 'like', '%'.$data['owner_cd'].'%')
                ->get();

                $markCount = count($canuseMark);
                
                Log::info('判斷 canuseMark');

                if(!isset($canuseMark) || $markCount <= $countReserveByOwner) {
                    // 如果沒有指定貨主的碼頭 || 指定貨主的碼頭空位數量 <= 已預約該時段指定貨主數量 
                    Log::info('判斷 canuseMark double');
                    $canuseMark = DB::table('mod_mark')
                    ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                    ->where('is_enabled','Y')
                    ->where('type','!=','code4')
                    ->where('in_use','N')
                    ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                    ->whereNull('owner_cd')
                    ->get();
                    if (isset($canuseMark)) {
                        $markCount += count($canuseMark);
                    }

                    if ($markCount <= $countReserve) {
                        // 檢查可使用碼頭數(包含指定貨主跟無指定貨主) <= 已預約該時段數量 ， 如果是表示碼頭不夠用
                        $canuseMark = null;
                        $data['entry_time'] = null;
                    }
                }

                if (isset($canuseMark)) {
                    $canuseMark = $canuseMark[0];
                }

                Log::info('判斷box item num');
                $processTime = 0; 
                if(!empty($data['box_num']) && !empty($data['items_num'])) {
                    $marksetting = DB::table('mod_mark_setting')
                    ->where('box_num_end', '>=', $data['box_num'])
                    ->where('items_num_end', '>=', $data['items_num'])
                    ->where('warehouse_id', 'like', '%'.$data['dc_id'].'%')
                    ->where('car_type',  $data['car_type'])
                    ->orderBy('box_num','asc')
                    ->orderBy('items_num','asc')
                    ->orderBy('id','asc')
                    ->first();
                    if (isset($marksetting->process_time)) {
                        $processTime = $marksetting->process_time + $marksetting->trans_time;
                        $data['process_time'] = $processTime;
                    }
                }

                $newtimestamp = strtotime(date("Y-m-d H:i:00").' + '. $processTime.' minute');
                $data['process_time']   = $processTime;
                $endat = date('Y-m-d H:i:s', $newtimestamp);


                //
                $min = date("i");
                $newmin  = date("i");

                $closestMin = (int)($min / 15);
                $closestMin = $closestMin * 15 ;
                $resultMin  = abs($closestMin - (int)$min);
                
                if($resultMin <= 7) {
                    $newmin =  date("i",strtotime($now.' - '.$resultMin.' minute'));
                } else {
                    $resultMin = (60 - $resultMin) % 15;
                    $newmin =  date("i",strtotime($now.' + '.$resultMin.' minute'));
                }
                //
               
                DB::table('mod_mark_reserve')->insert([
                    'dc_id'          => $data['dc_id'],
                    'cust_import_no' => $data['cust_ord_no'],
                    'dc_name'        => $whdata->cname,
                    'reserve_date'   => date("Y-m-d"),
                    'reserve_h'      => date("H"),
                    'reserve_m'      => $newmin,
                    'full_date'      => date("Y-m-d H:'.$newmin.':00"),
                    'mark_no'        => isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_no : NULL,
                    'mark_name'      => isset($canuseMark->cust_name) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_name : NULL,
                    'driver_phone'   => $data['phone'],
                    'driver_name'    => $data['driver_name'],
                    'car_no'         => $data['car_no'],
                    'owner_no'       => isset($data['owner_cd']) ? $data['owner_cd'] : NULL,
                    'owner_name'     => isset($data['owner_name']) ? $data['owner_name']: NULL,
                    'car_type'       => $carType->cd,
                    'car_type_descp' => $carType->cd_descp,
                    'con_type'       => isset($container) ? $container->cd : NULL,
                    'con_type_desc'  => isset($container) ? $container->cd_descp : NULL,
                    'truck_cmp_no'   => isset($truck) ? $truck->cust_no : NULL,
                    'truck_cmp_name' => isset($truck) ? $truck->cust_name : NULL,
                    'box_num'        => $data['box_num'],
                    'items_num'      => $data['items_num'],
                    'process_time'   => $processTime,
                    'end_at'         => $endat,
                    'g_key'          => 'DSS',
                    'c_key'          => 'DSS',
                    's_key'          => 'DSS',
                    'd_key'          => 'DSS',
                    'onsite'         => 'Y',
                    'created_by'     => 'SYSTEM',
                    'updated_by'     => 'SYSTEM',
                    'created_at'     => $now,
                    'updated_at'     => $now,
                ]);
            }
            $data['created_by']    = $data['driver_name'];
            $data['updated_by']    = $data['driver_name'];
            $data['process_time']  = $processTime;
            $data['car_type']      = $data['car_type'];
            $data['car_type_desc'] = $data['car_type_desc'];
            $data['wh_no']         = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->wh_no : NULL;
            $data['mark_no']       = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_no : NULL;
            $data['mark_name']     = isset($canuseMark->cust_name) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_name : NULL;
            Log::info('資料新增確認');
            Log::info($data['wh_no']);
        }
        $data['con_type']      = $data['con_type_desc'];
        $data['con_type_desc'] = $data['con_type_desc'];

        $attninfo = DB::table('mod_cust_wh')
        ->where('dc_id',$data['dc_id'])
        ->where('cust_no', $data['owner_cd'])
        ->first();
        
        $whforremark = DB::table('mod_warehouse')
        ->where('cust_no',  $attninfo->wh_no)
        ->first();
       
        $data['created_at']  = $now;
        $data['updated_at']  = $now;


        $data['process_time']  = $processTime;
        $data['dc_id']         = $data['dc_id'];
        $data['warehouse']     = $whdata->cname;
        $data['button_status'] = '確認下貨';
        $data['status']        = 'import';
        $data['status_desc']   = '進場';
        $data['wh_name']       = isset($canuseMark->wh_name) ? $canuseMark->wh_name : null;
        if( isset($whforremark) && empty($data['mark_no'])  ) {
            $data['contactinfo'] = $whforremark->contactinfo;
        } else {
            $data['contactinfo'] = '';
        }
        $checkin = $this->checkinRepo->create($data);
        Log::info('新增完成');
        // contactinfo
        $markforstr = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->wh_name : NULL;
        if($markforstr != null ) {
            $checkin['mark_name'] = $markforstr.'-'.$checkin['mark_name'];
        }
        if($reserveMark > 0){
            $reserveMark = DB::table('mod_mark_reserve')
            ->where('dc_id',$data['dc_id'])
            ->whereNull('mark_no')
            ->where('reserve_date',$today)
            ->where('cust_import_no',$data['cust_ord_no'])
            ->where('con_no',$data['con_id'])
            ->update([
                'arrive_flag' => $checkin['id']
            ]);
        }

        if(isset($canuseMark) && $data['job_type_desc'] != "拉空櫃" ) {
            DB::table('mod_mark')
            ->where('id',$canuseMark->id)
            ->update([
                'in_use' => 'Y'
            ]);
        }

        // mod_mark_reserve
        if (!$checkin) {
            throw new Exception('新增失敗');
        } 
        return $checkin;
    }

    /*
        public function create (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        $today = date("Y-m-d");
        $now = date("Y-m-d H:i:s");
        $prepdata = DB::table('mod_dss_dlv')
        ->where('driver_phone', $data['phone'])
        ->orderBy('created_at','desc')
        ->first();
        
        Log::info($request->all());

        $graceTime = DB::table('bscode')
        ->where('cd_type', 'GRACETIME')
        ->where('cd', 'gracetime')
        ->first();

        $truck = DB::table('mod_car_trader')
        ->where('cust_name',$data['truck_cmp_name'])
        ->first();

        $container = DB::table('bscode')
        ->where("cd_type","container")
        ->where('cd',$data['con_type'])
        ->first();

        $carType = DB::table('bscode')
        ->where("cd_type","IMPORTCARTYPE")
        ->where('cd', $data['car_type'])
        ->first();

        $whdata = DB::table('mod_warehouse')
        ->where('cust_no', $data['dc_id'])
        ->first();


        if(!empty($data['con_id'])) {
            $checkCon = $this->baseModel->checkCon($data['con_id']);
            if(!$checkCon) {
                throw new Exception('櫃號檢查異常');
            }
        }

        if(isset( $prepdata)) {
            //出貨
            $data['mark_no'] = $prepdata->mark_no;
        } else {


            $nexttime = strtotime(date("Y-m-d H:i:00").' + 15 minute');
            $nexttime = date('Y-m-d H:i:s', $nexttime);

            $reserveMark = array();
            if (empty($data['con_id'])) {
                $reserveMark = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date',$today)
                ->where('cust_import_no',$data['cust_ord_no'])
                ->where('con_no',$data['con_id'])
                ->count();
            } else {
                $reserveMark = DB::table('mod_mark_reserve')
                ->where('dc_id',$data['dc_id'])
                ->whereNull('mark_no')
                ->where('reserve_date',$today)
                ->where('cust_import_no',$data['cust_ord_no'])
                ->count();
            }
            




            $reservefinal = DB::table('mod_mark_reserve')
            ->where('dc_id',$data['dc_id'])
            ->whereNull('mark_no')
            ->where('reserve_date',$today)
            ->where('cust_import_no',$data['cust_ord_no'])
            ->orderBy('id','desc')
            ->first();
            $finalentry = $now;
            //
            if(isset($reservefinal)) {
                $finalentry =  strtotime($reservefinal->full_date.' + '.$graceTime->value1.' minute');
                $canentry = strtotime($now);
                $result = $finalentry - $canentry;
            } else {
                $result = 0;
            }
            if($reserveMark > 0  && $result >= 0) {


                // $carType->cd_descp = empty($data['con_type']) ? $carType->cd_descp : '貨櫃';
                // Log::info('預約 貨櫃');
                // Log::info($carType->cd_descp);
                
                $canuseMark = DB::table('mod_mark')
                ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                ->where('is_enabled','Y')
                ->where('type','!=','code4')
                ->where('in_use','N')
                ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                ->where('owner_cd', 'like', '%'.$data['owner_cd'].'%')
                ->first();

                if(!isset($canuseMark)) {
                    $canuseMark = DB::table('mod_mark')
                    ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                    ->where('is_enabled','Y')
                    ->where('type','!=','code4')
                    ->where('in_use','N')
                    ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                    ->whereNull('owner_cd')
                    ->first();
                }
                
                $processTime = !empty($reservefinal->process_time) ? $reservefinal->process_time : 0;

                if (($processTime == 0 || $processTime == null) && $data['job_type'] != '拉空櫃' ) {
                    $marksetting = DB::table('mod_mark_setting')
                    ->where('box_num_end', '>=', $data['box_num'])
                    ->where('items_num_end', '>=', $data['items_num'])
                    ->where('warehouse_id', 'like', '%'.$data['dc_id'].'%')
                    ->where('car_type',  $data['car_type'])
                    ->orderBy('box_num','asc')
                    ->orderBy('items_num','asc')
                    ->orderBy('id','asc')
                    ->first();
        
                    if(isset($marksetting->process_time)) {
                        $processTime = $marksetting->process_time + $marksetting->trans_time;
                        $data['process_time'] = $processTime;
                    }
                }
                
                $newtimestamp = strtotime(date("Y-m-d H:i:00").' + '. $processTime.' minute');
                $data['process_time']   = $processTime;
                $endat = date('Y-m-d H:i:s', $newtimestamp);
            } else {

                //
                // Log::info('沒預約 貨櫃');
                // Log::info($carType->cd_descp);
                // Log::info('test 1');
                // $carType->cd_descp = empty($data['con_type']) ? $carType->cd_descp : '貨櫃';
                // if($carType->cd_descp == "小車" || $carType->cd_descp =="大車") {
                //     $carType->cd_descp = mb_substr($carType->cd_descp ,0,1);
                // }
                Log::info('test 2');
                Log::info('判斷mod_mark');
                $canuseMark = DB::table('mod_mark')
                ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                ->where('is_enabled','Y')
                ->where('type','!=','code4')
                ->where('in_use','N')
                ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                ->where('owner_cd', 'like', '%'.$data['owner_cd'].'%')
                ->first();
                Log::info('判斷 canuseMark');
                if(!isset($canuseMark)) {
                    Log::info('判斷 canuseMark double');
                    $canuseMark = DB::table('mod_mark')
                    ->where('wh_no', 'like', '%'.$data['dc_id'].'%')
                    ->where('is_enabled','Y')
                    ->where('type','!=','code4')
                    ->where('in_use','N')
                    ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
                    ->whereNull('owner_cd')
                    ->first();
                }

                Log::info('判斷box item num');
                $processTime = 0; 
                if(!empty($data['box_num']) && !empty($data['items_num'])) {
                    $marksetting = DB::table('mod_mark_setting')
                    ->where('box_num_end', '>=', $data['box_num'])
                    ->where('items_num_end', '>=', $data['items_num'])
                    ->where('warehouse_id', 'like', '%'.$data['dc_id'].'%')
                    ->where('car_type',  $data['car_type'])
                    ->orderBy('box_num','asc')
                    ->orderBy('items_num','asc')
                    ->orderBy('id','asc')
                    ->first();
                    if (isset($marksetting->process_time)) {
                        $processTime = $marksetting->process_time + $marksetting->trans_time;
                        $data['process_time'] = $processTime;
                    }
                }

                $newtimestamp = strtotime(date("Y-m-d H:i:00").' + '. $processTime.' minute');
                $data['process_time']   = $processTime;
                $endat = date('Y-m-d H:i:s', $newtimestamp);


                //
                $min = date("i");
                $newmin  = date("i");

                $closestMin = (int)($min / 15);
                $closestMin = $closestMin * 15 ;
                $resultMin  = abs($closestMin - (int)$min);
                
                if($resultMin <= 7) {
                    $newmin =  date("i",strtotime($now.' - '.$resultMin.' minute'));
                } else {
                    $resultMin = (60 - $resultMin) % 15;
                    $newmin =  date("i",strtotime($now.' + '.$resultMin.' minute'));
                }
                //
               
                DB::table('mod_mark_reserve')->insert([
                    'dc_id'          => $data['dc_id'],
                    'cust_import_no' => $data['cust_ord_no'],
                    'dc_name'        => $whdata->cname,
                    'reserve_date'   => date("Y-m-d"),
                    'reserve_h'      => date("H"),
                    'reserve_m'      => $newmin,
                    'full_date'      => date("Y-m-d H:'.$newmin.':00"),
                    'mark_no'        => isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_no : NULL,
                    'mark_name'      => isset($canuseMark->cust_name) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_name : NULL,
                    'driver_phone'   => $data['phone'],
                    'driver_name'    => $data['driver_name'],
                    'car_no'         => $data['car_no'],
                    'owner_no'       => isset($data['owner_cd']) ? $data['owner_cd'] : NULL,
                    'owner_name'     => isset($data['owner_name']) ? $data['owner_name']: NULL,
                    'car_type'       => $carType->cd,
                    'car_type_descp' => $carType->cd_descp,
                    'con_type'       => isset($container) ? $container->cd : NULL,
                    'con_type_desc'  => isset($container) ? $container->cd_descp : NULL,
                    'truck_cmp_no'   => isset($truck) ? $truck->cust_no : NULL,
                    'truck_cmp_name' => isset($truck) ? $truck->cust_name : NULL,
                    'box_num'        => $data['box_num'],
                    'items_num'      => $data['items_num'],
                    'process_time'   => $processTime,
                    'end_at'         => $endat,
                    'g_key'          => 'DSS',
                    'c_key'          => 'DSS',
                    's_key'          => 'DSS',
                    'd_key'          => 'DSS',
                    'onsite'         => 'Y',
                    'created_by'     => 'SYSTEM',
                    'updated_by'     => 'SYSTEM',
                    'created_at'     => $now,
                    'updated_at'     => $now,
                ]);
            }
            $data['created_by']    = $data['driver_name'];
            $data['updated_by']    = $data['driver_name'];
            $data['process_time']  = $processTime;
            $data['car_type']      = $data['car_type'];
            $data['car_type_desc'] = $data['car_type_desc'];
            $data['wh_no']         = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->wh_no : NULL;
            $data['mark_no']       = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_no : NULL;
            $data['mark_name']     = isset($canuseMark->cust_name) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->cust_name : NULL;
            Log::info('資料新增確認');
            Log::info($data['wh_no']);
        }
        $data['con_type']      = $data['con_type_desc'];
        $data['con_type_desc'] = $data['con_type_desc'];
        //同一時段資料
        $checkreserve = DB::table('mod_mark_reserve')
        ->where('dc_id',$data['dc_id'])
        ->whereNull('mark_no')
        ->where('reserve_date', date("Y-m-d"))
        ->where('reserve_h', date("H"))
        ->where('onsite','N')
        ->first();

        $data['updated_at'] = date("Y-m-d H:i:s");
        if(!isset($checkreserve)) {
            //如果沒有同一時段 進場時間 = 現在
            $data['entry_time'] = date("Y-m-d H:i:s");
        } else {
            //此客戶訂單號今天是否有預約
            $isreserve = DB::table('mod_mark_reserve')
            ->where('dc_id',$data['dc_id'])
            ->whereNull('mark_no')
            ->where('reserve_date', date("Y-m-d"))
            ->where('reserve_h', date("H"))
            ->where('cust_import_no',$data['cust_ord_no'])
            ->count();

            $leavecount =  DB::table('mod_mark')
            ->where('wh_no', $canuseMark->wh_no)
            ->count();
            //有空的時間直接 讓他進去
            if(isset($isreserve)) {
                $data['entry_time'] = date("Y-m-d H:i:s");
            }
    
            $entry_time = '';
            $graceTime = DB::table('bscode')
            ->where('cd_type', 'GRACETIME')
            ->where('cd', 'gracetime')
            ->first();
            //
            if($leavecount == $isreserve) {
                $entry_time = strtotime(date("Y-m-d H:i:00").' + '.$graceTime->value1.' minute');
                $entry_time = date('Y-m-d H:i:s', $entry_time);
                $data['entry_time'] =  $entry_time;
            }
        }

        $attninfo = DB::table('mod_cust_wh')
        ->where('dc_id',$data['dc_id'])
        ->where('cust_no', $data['owner_cd'])
        ->first();
        
        $whforremark = DB::table('mod_warehouse')
        ->where('cust_no',  $attninfo->wh_no)
        ->first();
       
        $data['created_at']  = $now;
        $data['updated_at']  = $now;


        $data['process_time']  = $processTime;
        $data['dc_id']         = $data['dc_id'];
        $data['warehouse']     = $whdata->cname;
        $data['button_status'] = '確認下貨';
        $data['status']        = 'import';
        $data['status_desc']   = '進場';
        $data['wh_name']       = isset($canuseMark->wh_name) ? $canuseMark->wh_name : null;
        if( isset($whforremark) && empty($data['mark_no'])  ) {
            $data['contactinfo'] = $whforremark->contactinfo;
        } else {
            $data['contactinfo'] = '';
        }
        $checkin = $this->checkinRepo->create($data);
        Log::info('新增完成');
        // contactinfo
        $markforstr = isset($canuseMark->cust_no) && $data['job_type_desc'] != "拉空櫃" ? $canuseMark->wh_name : NULL;
        if($markforstr != null ) {
            $checkin['mark_name'] = $markforstr.'-'.$checkin['mark_name'];
        }
        if($reserveMark > 0){
            $reserveMark = DB::table('mod_mark_reserve')
            ->where('dc_id',$data['dc_id'])
            ->whereNull('mark_no')
            ->where('reserve_date',$today)
            ->where('cust_import_no',$data['cust_ord_no'])
            ->where('con_no',$data['con_id'])
            ->update([
                'arrive_flag' => $checkin['id']
            ]);
        }

        if(isset($canuseMark) && $data['job_type_desc'] != "拉空櫃" ) {
            DB::table('mod_mark')
            ->where('id',$canuseMark->id)
            ->update([
                'in_use' => 'Y'
            ]);
        }

        // mod_mark_reserve
        if (!$checkin) {
            throw new Exception('新增失敗');
        } 
        return $checkin;
    }
    */
    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_at']      = date("Y-m-d H:i:s");
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $checkin = $this->checkinRepo->update($id, $data);
        if (!$checkin) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $checkin = $this->checkinRepo->delete($id);

        if (!$checkin) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $checkinIds = $request->ids;

        foreach($checkinIds as $checkinId) {
            $this->checkinRepo->delete($checkinId);
        }

        return true;
    }
    public function manualleave($request) {
        $user = Auth::user();
        $id = $request->ids;
        DB::table('mod_driver_checkin')
        ->whereIn('id',$id)
        ->update([
            'status'            => 'export',
            'status_desc'       => '離場',
            'leave_time'        => date("Y-m-d H:i:s"),
            'discharged_status' => '(手動離場)',
            'updated_at'        => date("Y-m-d H:i:s"),
            'updated_by'        => $user->email,
        ]);

        return;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        $result = $this->baseModel->baseQuery('mod_driver_checkin', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('mod_driver_checkin', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'checkin_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('checkin_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }
    public function deliveryexport(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('mod_delivery_log', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'checkin_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('checkin_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }
}