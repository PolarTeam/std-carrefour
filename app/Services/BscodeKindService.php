<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\BscodeKindRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
class BscodeKindService {

    protected $bscodeKindRepo;
    protected $baseModel;

    public function __construct (BscodeKindRepository $bscodeKindRepo, BaseModel $baseModel) {
        $this->bscodeKindRepo = $bscodeKindRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $bscodeKind = $this->bscodeKindRepo->get($id);
        if ($bscodeKind) {
            return $bscodeKind;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $user = Auth::user();
        $data = $request->all();

        if (isset($data['cd'])) {
            $bscodeKindRepo = $this->bscodeKindRepo->getByCode($data['cd']);

            if ($bscodeKindRepo) {
                throw new Exception(trans('common.msgnew25') );
            }
        }
    
        $data['created_by']      = $user->id;
        $data['created_by_name'] = $user->name;
        $data['updated_by']      = $user->id;
        $data['updated_by_name'] = $user->name;
        $data['g_key'] = $user->g_key;
        $data['c_key'] = $user->c_key;
        $data['s_key'] = $user->s_key;
        $data['d_key'] = $user->d_key;
        
        $bscodeKind = $this->bscodeKindRepo->create($data);
        if (!$bscodeKind) {
            throw new Exception('新增失敗');
        } 
        return $bscodeKind;
    }

    public function createdetail (Request $request) {
        $user = Auth::user();
        $data = $request->all();

        if (isset($data['cd'])) {
            $detail = $this->bscodeKindRepo->getDetailByCode($data['cd_type'], $data['cd']);

            if ($detail) {
                throw new Exception(trans('common.msgnew25') );
            }
        }
    
        $data['created_by']      = $user->email;
        $data['created_by_name'] = $user->name;
        $data['updated_by']      = $user->email;
        $data['updated_by_name'] = $user->name;
        $data['g_key'] = $user->g_key;
        $data['c_key'] = $user->c_key;
        $data['s_key'] = $user->s_key;
        $data['d_key'] = $user->d_key;
        $bscode = $this->bscodeKindRepo->createdetail($data);
        if (!$bscode) {
            throw new Exception('新增失敗');
        } 
        return $bscode;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_by_name'] = $user->name;
        $data['updated_at']      = date("Y-m-d H:i:s");
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $bscodeKind = $this->bscodeKindRepo->update($id, $data);
        if (!$bscodeKind) {
            throw new Exception('更新失敗');
        }
    }

    public function detailUpdate (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_by_name'] = $user->name;

        $bscodeKind = $this->bscodeKindRepo->updatedetail($data['id'], $data);
        if (!$bscodeKind) {
            throw new Exception('更新失敗');
        }
    }


    public function delete ($id) {
        $maindata = $this->bscodeKindRepo->get($id);
        $details = $this->bscodeKindRepo->getbscode($maindata['cd_type']);
        $bscodeKind = $this->bscodeKindRepo->delete($id);

        foreach ($details as $detail) {
            $this->bscodeKindRepo->deletedetail($detail->id);
        }

        if (!$bscodeKind) {
            throw new Exception('刪除失敗');
        }
    }

    public function deletedetail ($id) {
        $bscodeKind = $this->bscodeKindRepo->deletedetail($id);

        if (!$bscodeKind) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $bscodeKindIds = $request->ids;

        foreach($bscodeKindIds as $bscodeKindId) {
            $maindata = $this->bscodeKindRepo->get($bscodeKindId);
            $details = $this->bscodeKindRepo->getbscode($maindata['cd_type']);
            $this->bscodeKindRepo->delete($bscodeKindId);
            foreach ($details as $detail) {
                $this->bscodeKindRepo->deletedetail($detail->id);
            }
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('bscode_kind', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }
    
    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('bscode_kind', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'bscode_kind_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('bscode_kind_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

    public function getbscode($cdType) {

        return $this->bscodeKindRepo->getbscode($cdType);

    }

}