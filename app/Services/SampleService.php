<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Repositories\SampleRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SampleService {

    protected $sampleRepo;
    protected $baseModel;

    public function __construct (SampleRepository $sampleRepo, BaseModel $baseModel) {
        $this->sampleRepo = $sampleRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $sample = $this->sampleRepo->get($id);
        if ($sample) {
            return $sample;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $data = $request->all();
        $sample = $this->sampleRepo->create($data);
        if (!$sample) {
            throw new Exception('新增失敗');
        } 
        return $sample;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $sample = $this->sampleRepo->update($id, $data);
        if (!$sample) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $sample = $this->sampleRepo->delete($id);

        if (!$sample) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $sampleIds = $request->sampleIds;

        foreach($sampleIds as $sampleId) {
            $this->sampleRepo->delete($sampleId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        $baseCondition[] = ['company_id', '=', $user->company_id];
        $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('sample', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

}