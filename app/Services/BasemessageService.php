<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\BasemessageRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Log;
use App\Helpers\FcmHelper;
class BasemessageService {

    protected $basemessageRepo;
    protected $baseModel;
    
    public function __construct (BasemessageRepository $basemessageRepo, BaseModel $baseModel) {
        $this->basemessageRepo = $basemessageRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $basemessage = $this->basemessageRepo->get($id);
        if ($basemessage) {
            return $basemessage;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $data = $request->all();
        $user = Auth::user();
        if (isset($data['send_user']) ) {
            $userNames =  implode(',',$this->baseModel->getUserNameByids($data['send_user']));
            $data['send_user'] =  implode(',',$data['send_user']);
            $data['send_name'] =  $userNames;
        }
        if (isset($data['send_role']) ) {
            $roleNames =  implode(',',$this->baseModel->getRoleNames($data['send_role']));
            $data['send_role'] =  implode(',',$data['send_role']);
            $data['send_role_name'] = $roleNames;
        }

        $data['created_by'] = $user->email;
        $data['updated_by'] = $user->email;
        $data['g_key'] = $user->g_key;
        $data['c_key'] = $user->c_key;
        $data['s_key'] = $user->s_key;
        $data['d_key'] = $user->d_key;
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }
        $basemessage = $this->basemessageRepo->create($data);
        if (!$basemessage) {
            throw new Exception('新增失敗');
        } 
        return $basemessage;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->email;
        $data['updated_at']      = date("Y-m-d H:i:s");
        foreach ($data as $key => $value) {
            if($value == "false" || $value == null) {
                unset($data[$key]);
            }
        }

        if (isset($data['send_user']) ) {
            $userNames =  implode(',',$this->baseModel->getUserNameByids($data['send_user']));
            $data['send_user'] =  implode(',',$data['send_user']);
            $data['send_name'] =  $userNames;
        } else {
            $data['send_user'] =  '';
            $data['send_name'] =  '';
        }

        if (isset($data['send_role']) ) {
            $roleNames =  implode(',',$this->baseModel->getRoleNames($data['send_role']));
            $data['send_role'] =  implode(',',$data['send_role']);
            $data['send_role_name'] = $roleNames;
        } else {
            $data['send_role'] =  '';
            $data['send_role_name'] =  '';
        }
        
        $basemessage = $this->basemessageRepo->update($id, $data);
        if (!$basemessage) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $basemessage = $this->basemessageRepo->delete($id);

        if (!$basemessage) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $basemessageIds = $request->ids;

        foreach($basemessageIds as $basemessageId) {
            $this->basemessageRepo->delete($basemessageId);
        }

        return true;
    }
    public function batchSendMessage($request) {
        $user = Auth::user();
        $basemessageIds = $request->ids;
        $sendArray = array();

        foreach($basemessageIds as $basemessageId) {
            $data = $this->basemessageRepo->get($basemessageId);

            $phonefordriver = explode(',', $data['send_user']);
            $alldriver =  $this->baseModel->getAlldriverByid($phonefordriver);
            foreach ( $alldriver as $userkey => $userid) {
                $sendArray[] = (int)$userid;
            }
            if(count($sendArray)==0) {
                $rolsUsers = $this->baseModel->getRole( explode(',',$data['send_role']));
                foreach ($rolsUsers as $rolekey => $roleuser) {
                    $sendArray[] = $roleuser;
                }
            }


            $content    = $data['content'];
            $title      = $data['title'];
            $sendtoData = $this->baseModel->getFcmuserById($sendArray);

            foreach ($sendtoData as $key => $row) {
                $this->baseModel->updateUnReadCount($row->id);
                $this->sendDsvTmsUnReadMessageByWebsocket($data, $row->id);
                # code...
                $messagedata  = array();
                $messagedata['title']      = $title;
                $messagedata['to_by']      = $row->email;
                $messagedata['to_by_name'] = $row->name;
                $messagedata['content']    = $content;
                $messagedata['created_by'] = $user->name;
                $messagedata['updated_by'] = $user->name;
                $messagedata['created_at'] = date("Y-m-d H:i:s");
                $messagedata['updated_at'] = date("Y-m-d H:i:s");
                $this->basemessageRepo->createFcm($messagedata);
                FcmHelper::sendToFcm( $title , $content, $row->d_token);
            }
            $updatedata  = array();
            $updatedata['is_send'] = '是';
            $updatedata['final_send'] = date("Y-m-d H:i:s");
            $this->basemessageRepo->update($basemessageId, $updatedata);
        }

        
        return true;
    }

    public function sendDsvTmsUnReadMessageByWebsocket ($newsData, $sendUser) {

        $allUnRead     = $this->basemessageRepo->getUnread($sendUser);

        $unReadData    = array();

        $unReadData['newData']     = $newsData;
        $unReadData['unReadCount'] = $allUnRead;
        $postData = array(
            "groupId" => "groupId_DsvdevTmsUnReadMessageByWebsocket{$sendUser}",
            "msg" => $unReadData
        );
        $postData = array(
            "groupId" => "groupId_DsvdevTmsUnReadMessageByWebsocket{$sendUser}",
            "msg" => $unReadData
        );
        // Log::info($sendUser);
        // Log::info('$postData在這裡在這裡在這裡在這裡在這裡在這裡');
        // Log::info($postData);
        try {
            Log::info("enter DsvTmsUnReadMessageByWebsocket");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://dss-ws.target-ai.com:9599/websocket/notifyByGroupId',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($postData, JSON_UNESCAPED_UNICODE),
                CURLOPT_HTTPHEADER => array(
                    'cache-control: no-cache',
                    'content-type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            Log::info($response);
        }
        catch(\Exception $e) {
            Log::info("GpsDataByWebsocket error");
            Log::info($e->getMessage());
            Log::info($e->getLine());
        }
        Log::info("DsvTmsUnReadMessageByWebsocket finish");
        return;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('mod_base_message', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition    = $request->baseCondition;
        $anotherCondition = $request->anotherCondition;
        if (is_array($anotherCondition)) {
            $baseCondition[] = $anotherCondition;
        }
        $pageNum       = $request->pageNum;
        $pageSize      = $request->pageSize;
        $subSelect        = array();
        $excelHeaders     = array();
        $dbCols           = array();
        $sort             = $request->sort;
        
        $result        = $this->baseModel->baseQuery('mod_base_message', $pageNum, $pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'basemessage_export/'.$fileName, 'local');

        $s3 = \Storage::disk('s3');
        $filePath = env('EXCELATH').$fileName;
        $fileContents = \Storage::get('basemessage_export/'.$fileName);
        $s3->put($filePath, $fileContents,'public');

        return env('S3EXCEL_URL').$fileName;
    }

}