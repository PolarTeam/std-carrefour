<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        // $user = Auth::user();
        // if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
        //     return redirect('/board');
        //     // $ueserrole = explode(',', $user->role);
        //     // $redirectto = DB::table('bscode')->where('cd_type', 'LOGINPAGE')->whereIn('cd', $ueserrole)->value('value1');
        //     // if(isset($redirectto)){
        //     //     return redirect('/'.$redirectto);
        //     // }else{
        //     //     return redirect('/board');
        //     // }
        // }

        return parent::render($request, $exception);
    }
}
