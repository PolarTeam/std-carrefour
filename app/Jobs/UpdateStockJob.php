<?php

namespace App\Jobs;

use App\Helpers\StockUpdate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class UpdateStockJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $productIds;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productIds) {
        $this->productIds = $productIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        // DB::beginTransaction();

        try {
            ini_set('memory_limit', '3072M');
            Log::info('start update stock');
            StockUpdate::stockByProductUpdate($this->productIds); //更新商品庫存
            StockUpdate::stockByStorageUpdate($this->productIds); //更新儲位庫存
            StockUpdate::updateStockByInventory($this->productIds); //更新儲位庫存
        }
        catch (Exception $e) {
            // DB::rollback();
            Log::error($e);
            return false;
        }

        // DB::commit();
        return true;
    }
}
