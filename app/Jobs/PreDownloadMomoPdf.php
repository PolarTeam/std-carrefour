<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Exception;

class PreDownloadMomoPdf implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $spkNo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($spkNo)
    {
        $this->spkNo = $spkNo;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::info('get momo ecan pdf');
            $recOutbounds = DB::table('rec_outbound')->where('pickinglist_no_byproduct', $this->spkNo)->get();
            $momotoken    = DB::table('api_config')
                                ->where('platform_name', $recOutbounds[0]->ecstore_code)
                                ->first();
            $shippingCompany = 1;
            if($recOutbounds[0]->shipping_company_name != '宅配通momo') {
                $shippingCompany = 2;
            }
            foreach($recOutbounds as $recOutbound) {
                $loginInfo = $momotoken->login_info;
                $orderNo   = array();
                $fileName  = '';

                array_push($orderNo, array(
                    'completeOrderNo' => $recOutbound->order_no
                ));
    
                $fileName = 'momo_'.$recOutbound->order_no;

                $curl = curl_init();

                if($shippingCompany === 1) {
                    curl_setopt_array($curl, array(
                        CURLOPT_URL            => 'https://scmapi.momoshop.com.tw/OrderServlet.do',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => '',
                        CURLOPT_MAXREDIRS      => 10,
                        CURLOPT_TIMEOUT        => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST  => 'POST',
                        CURLOPT_POSTFIELDS     => '{
                            "doAction": "unsendThirdPrintPdf",
                            "printType": "label",
                            "third_delyGb": 61,
                            "loginInfo":'.$loginInfo.',
                            "sendInfoList":'.json_encode($orderNo).'
                        }',
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                        ),
                    ));
                }
                else {
                    curl_setopt_array($curl, array(
                        CURLOPT_URL            => 'https://scmapi.momoshop.com.tw/OrderServlet.do',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => '',
                        CURLOPT_MAXREDIRS      => 10,
                        CURLOPT_TIMEOUT        => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST  => 'POST',
                        CURLOPT_POSTFIELDS     => '{
                            "doAction": "unsendStoresPrintPdf",
                            "printType": "label",
                            "loginInfo":'.$loginInfo.',
                            "sendInfoList":'.json_encode($orderNo).'
                        }',
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                        ),
                    ));
                }

                $response = curl_exec($curl);
                curl_close($curl);
                
                $response = json_decode($response);
                if(!isset($response->pdfData)) {
                    // throw new Exception('還無法列印面單');
                    Log::error($recOutbound->order_no.'還無法列印面單');
                    continue;
                }

                $pdfDecode = base64_decode ($response->pdfData);

                Storage::disk('local')->put('Momo_PDF/'.$fileName.'.pdf', $pdfDecode);
            }
        }
        catch (Exception $e) {
            Log::error($e);
            // return false;
        }


        return true;
    }
}
