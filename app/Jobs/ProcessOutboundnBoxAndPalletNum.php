<?php

namespace App\Jobs;

use App\Jobs\Middleware\RateLimited;
use App\Services\RecOutboundService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class ProcessOutboundnBoxAndPalletNum implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recOutboundService;
    protected $outboundIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(RecOutboundService $recOutboundService, $outboundIds)
    {
        $this->recOutboundService = $recOutboundService;
        $this->outboundIds = $outboundIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::info('start calculate box num and pallet num');
            $this->recOutboundService->calBoxNumAndPalletNum($this->outboundIds);
        }
        catch (Exception $e) {
            Log::error($e);
            return false;
        }

        return true;
    }
}
