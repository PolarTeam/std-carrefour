<?php

namespace App\Jobs;

use App\Services\RecOutboundService;
use App\Helpers\LogActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class OutboundJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recOutboundService;
    protected $mainData;
    protected $detailData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mainData, $detailData, RecOutboundService $recOutboundService)
    {
        $this->mainData           = $mainData;
        $this->detailData         = $detailData;
        $this->recOutboundService = $recOutboundService;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::beginTransaction();

        try {
            Log::info('OPEN_OUTBOUND_API');
            $this->recOutboundService->createOutbound($this->mainData, $this->detailData);
        }
        catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            $userName = null;
            if(isset($this->mainData['created_by_name'])) {
                $userName = $this->mainData['created_by_name'];
            }
            LogActivity::addApiErrorLog('API出庫', $e->getMessage(), $this->mainData['created_by'], $userName);
            return false;
        }

        DB::commit();
        return true;
    }
}
