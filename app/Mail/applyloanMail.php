<?php

namespace App\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class applyloanMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailData;
    protected $appno;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData,$appno,$formdata)
    {
        $this->mailData = $mailData;
        $this->appno = $appno;
        $this->formdata = $formdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = str_replace('{applyno}', $this->appno, $this->mailData['title']);
        return $this->subject($title)
        ->markdown('emails.applyloanMail')
        ->with(['mailData'=>$this->mailData, 'appno'=>$this->appno]);
    }
}
