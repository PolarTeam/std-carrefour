<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class AutoNo extends Model
{
    use AutoNumberTrait;

    protected $attributes = ['num_format' => "", 'num_length' => 3];
    protected $fillable = ['auto_no', 'num_format', 'num_length', 'created_at', 'updated_at'];
    protected $table = 'auto_no_modes';
    /**
     * Return the autonumber configuration array for this model.
     *
     * @return array
     */
    public function getAutoNumberOptions()
    {
        return [
            'auto_no' => [
                'format' => function () {
                    return $this->attributes['num_format']; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => $this->attributes['num_length']// The number of digits in an autonumber
            ]
        ];
    }

}