<?php

namespace App\Helpers;
use App\ReportList;
use App\StdAutoNo;
use App\Helpers\StockUpdate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use DateTime;

class CommonHelper 
{
    public static function sendToPrinter($msg, $lineNo) 
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => env('PRINTER_SERVER').'/'.$lineNo.'?type=queue',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $msg,
        CURLOPT_HTTPHEADER => array(
            'Authorization: Basic YWRtaW46YWRtaW4=',
            'Content-Type: text/plain',
            'Cookie: JSESSIONID=node05c140b8i4ljh1lov1vlxjpdq210.node0'
        ),
        ));

        $response = curl_exec($curl);
        Log::info(env('PRINTER_SERVER').'/'.$lineNo.'?type=queue');
        Log::info('送到印表機server');
        Log::info($response);
        curl_close($curl);

        return true;
    }

    public static function validateDate($date)
    {
        if(date('Y-m-d', strtotime($date)) == $date){
            return date('Y-m-d', strtotime($date));
        }
        else {
            if(date('Y/m/d', strtotime($date)) == $date){
                return date('Y-m-d', strtotime($date));
            }
            else {
                if(date('Y/n/j', strtotime($date)) == $date){
                    return date('Y-m-d', strtotime($date));
                }
                else {
                    if(date('Ymd', strtotime($date)) == $date){
                        return date('Y-m-d', strtotime($date));
                    }
                    else {
                        return false;
                    }
                }
            }
        }

        return false;
    }

    public static function getReportFileName($reportCode) {
        return ReportList::where('report_code', $reportCode)->value('file_name');
    }

    public static function checkStock() {
        // 如果inventory有跟商品資料不同步時 可以使用這個同步
        $productIds = DB::table('stock_by_product')->whereNotNull('product_code')->groupBy('product_code', 'product_quality')->having(DB::raw('count(*)'), '>', 1)->pluck('product_id')->toArray();

        if(count($productIds) > 0) {
            DB::table("inventory")->whereIn('product_id', $productIds)->update([ "product_name" => DB::raw('(select product_name from product where product.product_id=inventory.product_id)')]);
            DB::table("inventory")->whereIn('product_id', $productIds)->update([ "owner_no" => DB::raw('(select owner_no from product where product.product_id=inventory.product_id)')]);
            DB::table("inventory")->whereIn('product_id', $productIds)->update([ "owner_id" => DB::raw('(select owner_id from product where product.product_id=inventory.product_id)')]);
            DB::table("inventory")->whereIn('product_id', $productIds)->update([ "owner_name" => DB::raw('(select owner_name from product where product.product_id=inventory.product_id)')]);
            DB::table("inventory")->whereIn('product_id', $productIds)->update([ "product_barcode" => DB::raw('(select product_barcode from product where product.product_id=inventory.product_id)')]);

            StockUpdate::stockByProductUpdate($productIds);
            StockUpdate::stockByStorageUpdate($productIds);
        }

        return true;
    }

    public function getAutoNo($autoNo, $length) {
        $today = date('Y-m-d H:i:s');
        $autoNoData = StdAutoNo::onWriteConnection()->lockForUpdate()->where('auto_no', $autoNo)->where('num_length', $length)->first();
        $number = 0;
        if($autoNoData) {
            $number = $autoNoData->last_num + 1;

            StdAutoNo::where('id', $autoNoData->id)->update(array(
                'last_num' => $number
            ));
        }
        else {
            StdAutoNo::create(array(
                'auto_no' => $autoNo,
                'num_length' => $length,
                'last_num' => 1
            ));

            $number = 1;
        }

        $result = $autoNo.str_pad($number, $length, '0', STR_PAD_LEFT);

        DB::table('std_autono_record')->insert(array(
            'auto_no'    => $result,
            'created_at' => $today,
            'updated_at' => $today,
        ));

        return $result;
    }

    public static function validateDateTime($dateStr, $format) {
        $date = DateTime::createFromFormat($format, $dateStr);
        return $date && ($date->format($format) === $dateStr);
    }

    public static function exMomoOrderNo() {
        $outbounds = DB::select("SELECT outbound_id, order_no FROM rec_outbound WHERE outbound_status=9 and shipping_company=201");

        foreach($outbounds as $outbound) {
            $exOrderNo = explode('-', $outbound->order_no);
            $momoOrderNo = $exOrderNo[0];

            DB::update("UPDATE rec_outbound SET momo_order_no='{$momoOrderNo}' WHERE outbound_id={$outbound->outbound_id}");
        }

        return true;
    }

    public static function getMappingDetail($mappingCode, $whId) {
        return DB::select("SELECT * FROM mapping_detail WHERE mapping_code='{$mappingCode}' AND wh_id={$whId} ORDER BY seq ASC");
    }

    public static function getExcelNameByColumnName($mappingCode, $ColName, $whId) {
        $mappingDetail = DB::select("SELECT excel_col_name FROM mapping_detail WHERE mapping_code='{$mappingCode}' AND wh_id={$whId} AND col_name='{$ColName}' ORDER BY seq ASC");
        return isset($mappingDetail[0]) ? $mappingDetail[0]->excel_col_name : null;
    }
}