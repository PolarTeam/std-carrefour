<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FcmHelper 
{
    public static function sendToFcm($title, $content, $token) 
    {
        // Log::info($title);
        // Log::info($content);
        // Log::info('token');
        // Log::info($token);
        // Log::info('token end');
        $key = 'AAAAeyjK4Ok:APA91bFDEh7hiA0yEDZMjqmaNlVYx7iZw9Emd-xFOGEAdkLV3AO-OqMoUdkZJKIf47hnySpKThSkVR2kRXGY-0ZF0pnEuRBQ15hOamt2hjuMl34SsCAzyDtdyCR0lCTyXGaCqmX8_at4';
        $registrationIds = $token;

        $msg = array(
            'body'  =>  $content,
            'title' =>  $title,
                'icon'  => 'myicon',/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
        );

        $fields = array(
            'to' => $registrationIds,
            'notification' => $msg);

        $headers = array(
                'Authorization: key=' . $key,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        // Log::info('fcmresult');
        // Log::info($result);
        // Log::info('fcmresult end');
        return true;
    }

    public static function batchSendToFcm($title, $content, $tokens) 
    {
        // Log::info($title);
        // Log::info($content);
        // Log::info('token');
        // Log::info($token);
        // Log::info('token end');
        $key = 'AAAAeyjK4Ok:APA91bFDEh7hiA0yEDZMjqmaNlVYx7iZw9Emd-xFOGEAdkLV3AO-OqMoUdkZJKIf47hnySpKThSkVR2kRXGY-0ZF0pnEuRBQ15hOamt2hjuMl34SsCAzyDtdyCR0lCTyXGaCqmX8_at4';

        $msg = array(
            'body'  =>  $content,
            'title' =>  $title,
                'icon'  => 'myicon',/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
        );

        $fields = array(
            'registration_ids' => $tokens,
            'notification' => $msg);

        $headers = array(
                'Authorization: key=' . $key,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        // Log::info('fcmresult');
        // Log::info($result);
        // Log::info('fcmresult end');
        return true;
    }
}