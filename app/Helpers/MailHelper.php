<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\MailFormat;
use Carbon\Carbon;
use App\Mail\applyloanMail;
class MailHelper 
{
    public static function sendMail($type , $cc = []) {
          Log::info("before mail");
          $mailFormat =  array();
          $mailFormat['title'] = 'DSS測試機獲取資料異常'; 
          $mailFormat['content'] = $type; 
          Mail::to('d1024242031@gmail.com')
          ->bcc(["maya@standard-info.com", "anderson@standard-info.com", "tim@standard-info.com"])
          ->send(new applyloanMail($mailFormat,'DSS手動獲取資料異常', null));
          Log::info(" mail end");
          return true;
    }

}