<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Request;
use App\LogActivity as LogActivityModel;
use App\ApiErrorLog;
use App\TmpProductRelation;
use App\ProductRelation;
use App\Product;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;



class LogActivity
{
    protected $recOutboundService;

    public function __construct($outboundIds,RecOutboundService $recOutboundService)
    {
        $this->recOutboundService = $recOutboundService;
    }


    public static function addToLog($subject, $message='')
    {
        $user = Auth::user();

        $log               = [];
        $log['subject']    = $subject;
        $log['url']        = Request::fullUrl();
        $log['method']     = Request::method();
        $log['ip']         = Request::ip();
        $log['agent']      = Request::header('user-agent');
        $log['post_data']  = empty(Request::all()) ? null : json_encode(Request::all());
        $log['created_by'] = isset($user->id) ? $user->id : null ;
        $log['updated_by'] = isset($user->id) ? $user->id : null ;
        LogActivityModel::create($log);
    }

    public static function addApiErrorLog($subject, $message, $userId, $userName)
    {
        $log = [];
        $log['subject']         = $subject;
        $log['created_by']      = $userId;
        $log['created_by_name'] = $userName;
        ApiErrorLog::create($log);
    }


    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }

    public static function processProduct() {
        $tmp = TmpProductRelation::orderBy('seq', 'asc')->get();

        $data = array();
        $motherCode = null;
        $motherId   = 0;
        $motherName = null;
        foreach($tmp as $idx=>$row) {
            if($row->prod_type == '*') {
                $motherCode = $row->product_code;
                $product = Product::where('product_code', $motherCode)->first();

                if($product) {
                    $motherCode = $product->product_code;
                    $motherId   = $product->product_id;
                    $motherName = $product->product_name;

                    Product::where('product_id', $motherId)->update(array(
                        'product_relation' => 'Y'
                    ));
                }
                else {
                    $motherCode = null;
                    $motherId   = null;
                    $motherName = null;
                    continue;
                }

                continue;
            }
            else {
                if(!empty($motherCode)) {
                    if(str_contains($row->product_code, '##')) {
                        continue;
                    }
                    
                    $product = Product::where('product_code', $row->product_code)->first();
                    if($product) {
                        array_push($data, array(
                            'company_id'                  => 1,
                            'wh_id'                       => 1,
                            'parent_product_id'           => $motherId,
                            'parent_product_code'         => $motherCode,
                            'parent_product_name'         => $motherName,
                            'child_product_id'            => $product->product_id,
                            'child_product_code'          => $product->product_code,
                            'child_product_name'          => $product->product_name,
                            'child_product_outbound_nums' => $row->nums
                        ));
                    }
                    else {
                        throw new Exception( $row->product_code.' 找不到');  
                    }
                    
                }
            }
        }

        Log::info('data num: '. count($data));
        $productCollect = collect($data);
        $chunks = $productCollect->chunk(500);

        foreach($chunks as $chunk) {
            ProductRelation::insert($chunk->toArray());
        }

    }

    public static function processRelationProduct() {
        $motnerProducts = ProductRelation::groupBy('parent_product_id')->pluck('parent_product_id')->toArray();

        foreach($motnerProducts as $productId) {
            Product::where('product_id', $productId)->update(array(
                'product_relation' => 'Y'
            ));
        }

        return true;
    }


    public static function checkProduct() {
        $chkProduct = array();

        $products = Product::whereIn('product_code', $chkProduct)->pluck('product_code')->toArray();
        $noProduct = array();
        foreach($chkProduct as $product) {
            if(!in_array($product, $products)) {
                array_push($noProduct, $product);
            }
        }

        print_r($noProduct);
        return true;
    }

    public function momoToShipped() {
        $outboundIds = DB::table('rec_outbound')->select('order_no')
                        ->where('outbound_status', 20)
                        ->whereIn('shipping_company', [178, 195])
                        ->where('ecstore_code', '藍mo')
                        ->pluck('outbound_id')->toArray();
        
        $this->recOutboundService->momoToShipped($outboundIds);
    }

}