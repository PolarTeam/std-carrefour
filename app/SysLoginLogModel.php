<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysLoginLogModel extends Model {



	protected $table = 'sys_login_log';

	protected $fillable = [
		'name',
		'car_no',
		'phone',
		'login_time'
	];

}