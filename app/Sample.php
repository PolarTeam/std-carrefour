<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    protected $table = 'sample_table';
    protected $fillable = [
        'id',
        'company_id',
        'wh_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
