<?php

namespace App\Console\Commands;

use App\Helpers\StockUpdate;
use App\Services\ApplicationFormService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Helpers\FcmHelper;
class systemfcm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:systemfcm';
    protected $mailhelper;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '系統推播';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(FcmHelper $fcmHelper) {

        parent::__construct();

        $this->fcmHelper = $fcmHelper;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info('start systemfcm');
            $sysmessageId = DB::table('mod_sys_message')
            ->where('id',4)
            ->first();

            $phone = DB::table('mod_dlv_plan')
            ->where('button_status_desc','下貨完成')
            ->groupBy('driver_phone')
            ->pluck('driver_phone')
            ->toArray();

            $driverdata = DB::table('users')
            ->whereIn('email',$phone)
            ->get();
            foreach ($driverdata as $key => $driver) {
                Log::info('fcm user');
                Log::info($driver->name);
                FcmHelper::sendToFcm( $sysmessageId->title ,  $sysmessageId->content, $driver->d_token);
                Log::info('fcm user end');
            }

            
        }
        catch(\Exception $e) {
            Log::error($e);
        }
        Log::info('start end');
        return;
    }
}
