<?php

namespace App\Console;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\OrderMgmtModel;
use App\ScheduleModel;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(env('APP_ENV') == 'production') {
            // try {
            //     //取得dss api 資料 每四小時一次
            //     $schedule->call(function () {
            //         $hour = date("H");
            //         if($hour % 4 == 0) {
            //             \Log::info("dssapi start");
            //             $ordermgmtmodel = new OrderMgmtModel;
            //             $ordermgmtmodel->getorderdata();
            //             \Log::info("dssapi end");
            //         }
            //     })->hourly();
            // }
            // catch(\Exception $e) {
            //     \Log::info("dssapi error");
            //     \Log::error($e->getMessage());
            // }
    
            // try {
            //      //取得dss 明細 資料 每小時一次
            //     $schedule->call(function () {
            //         \Log::info("wms start");
            //         $ordermgmtmodel = new OrderMgmtModel;
            //         $ordermgmtmodel->getdetailtest();
            //         \Log::info("wms end");
            //     })->hourly();
            // }
            // catch(\Exception $e) {
            //     \Log::info("wms error");
            //     \Log::error($e->getMessage());
            // }

            try {
                $schedule->call(function () {
                    $hour = date("H");
                    if($hour % 2 ==0) {
                        \Log::info("send api status");
                        $ordermgmtmodel = new OrderMgmtModel;
                        $ordermgmtmodel->sendstatus();
                        \Log::info("send api end");
                    }
                })->hourly();
            }
            catch(\Exception $e) {
                \Log::info("send api status");
                \Log::error($e->getMessage());
            }

            try {
                $schedule->call(function () {
                    \Log::info("check expire status");
                    // $ordermgmtmodel = new OrderMgmtModel;
                    // $ordermgmtmodel->checkexpire();
                    \Log::info("check expire end");
                })->everyThirtyMinutes();
            }
            catch(\Exception $e) {
                \Log::info("check expire");
                \Log::error($e->getMessage());
            }
    
        }
        try {
            //離場超時
           $schedule->call(function () {
               \Log::info("離場超時 start");
               $schedulemodel = new ScheduleModel;
               $schedulemodel->checkschedule();
               \Log::info("離場超時 end");
           })->everyFiveMinutes();
       }
       catch(\Exception $e) {
           \Log::info("離場超時 error");
           \Log::info($e->getLine());
           \Log::error($e->getMessage());
       }
        // $schedule->command('command:systemfcm')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
