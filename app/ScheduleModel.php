<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Fcm;
use App\Helpers\FcmHelper;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleModel extends Model
{
    public function checkschedule () {
        $start = date("Y-m-d");
        $now   = date("Y-m-d H:i:00");

        $data  = DB::table('mod_driver_checkin')
        ->select('car_no' , 'phone')
        ->whereNotNull('discharged_status')
        ->whereNull('leave_time')
        ->where('created_at', '>=', $start.' 00:00:00')
        ->where('created_at', '<=', $start.' 23:59:59')
        ->where('updated_at', '<=', $now)
        ->where('send_late_flag' , 'N')
        ->groupBy('car_no','phone')
        ->get();

        $basemessage = DB::table('mod_sys_message')
        ->where('title','離場超時')
        ->first();


        foreach ($data as $key => $row) {
            # code...
            // 
            $finaldelivery = DB::table('mod_delivery_log')
            ->where('driver_phone', $row->phone)
            ->orderBy('id','desc')
            ->first();

            if(isset($finaldelivery->wh_no))  {
                $senddata = DB::table('users')
                ->where('role', 'like', '%STORKEEPER%')
                ->where('check_wh', 'like', '%'.$finaldelivery->wh_no.'%')
                ->get();
            } else {
                $senddata = array();
            }


            foreach ($senddata as $senduserkey => $senduserrow) {
               $message = $basemessage->content;
               $message = str_replace('{car_no}',$row->car_no ,$message);
               $message = str_replace('{wh_name}',$finaldelivery->wh_name ,$message);
               $message = str_replace('{delivery_time}',$finaldelivery->delivery_time ,$message);

               $messagedata  = array();
               $messagedata['to_by']      = $senduserrow->email;
               $messagedata['to_name']    = $senduserrow->name;
               $messagedata['type']       = 'deliveryovertime';
               $messagedata['title']      = '離場超時';
               $messagedata['content']    = $message;
               $messagedata['created_by'] = 'SYSTEM';
               $messagedata['updated_by'] = 'SYSTEM';
               $messagedata['created_at'] = date("Y-m-d H:i:s");
               $messagedata['updated_at'] = date("Y-m-d H:i:s");
               $this->createFcm($messagedata);
               FcmHelper::sendToFcm( '離場超時' , '確認是否離場', $senduserrow->d_token);
            }
            if(isset($finaldelivery->wh_no))  {
                DB::table('mod_driver_checkin')
                ->where('car_no' , $row->car_no)
                ->where('phone' , $row->phone)
                ->where('created_at', '>=', $start.' 00:00:00')
                ->where('created_at', '<=', $start.' 23:59:59')
                ->where('updated_at', '<=', $now)
                ->update([
                    'send_late_flag'=>'Y'
                ]);
            }

            //
        }


        return ;
    }
    public function createFcm(array $data) {
        return Fcm::create($data);
    }

    public function checkexpire () {
        $today = date("Y-m-d");
        $now = date("Y-m-d H:i:00");
        Log::info('checkexpire enter');
        $currentTime = time();  // 取得目前的 UNIX 時間戳
        // 加上 30 分鐘
        $modifiedTime = strtotime('+30 minutes', $currentTime);
        // 將修改後的時間格式化為 "H:i" 格式
        $time = date("H:i", $modifiedTime);
        // $time = '12:00';
        $data = DB::table('mod_dlv_plan')
        ->where('status','!=', 'FINISHED')
        ->whereNotNull('upload_finish_time')
        ->where('dlv_date', $today)
        ->where('upload_finish_time', $time)
        ->get();
        Log::info( $today);
        Log::info($time);
        $dTokenList = array();
        $messagedataList = array();
        foreach ($data as $key => $row) {
            Log::info('data');
            Log::info($row->id);
            $maindata = DB::table('mod_dss_dlv')
            ->where('dlv_no',$row->dlv_no)
            ->first();
            $checkusers = DB::table('users')
            ->where('email', $row->driver_phone)
            ->where('truck_cmp_no', $maindata->truck_cmp_no)
            ->first();

            $messagedata  = array();
            $messagedata['dlv_no']     = $row->dlv_no;
            $messagedata['wms_ord_no'] = $row->wms_ord_no;
            $messagedata['sys_ord_no'] = $row->ord_no;
            $messagedata['to_by']      = $checkusers->email;
            $messagedata['flag']       = 'N';
            $messagedata['type']       = 'ontime';
            $messagedata['title']      = '準時到達';
            $messagedata['content']    = $row->cust_name.'-客戶訂單編號:'.$row->ord_no.'-'.$row->upload_finish_time.'是否在預定時間內到達';
            $messagedata['created_by'] = 'SYSTEM';
            $messagedata['updated_by'] = 'SYSTEM';
            $messagedata['created_at'] = date("Y-m-d H:i:s");
            $messagedata['updated_at'] = date("Y-m-d H:i:s");
            array_push($dTokenList, $checkusers->d_token);
            array_push($messagedataList, $messagedata);
            FcmHelper::batchSendToFcm( '準時到達' , '單號:'.$row->ord_no.'是否在預定時間內到達', $checkusers->d_token);
            
        }
        $messagedataArray = collect($messagedataList);
        $chunks = $messagedataArray->chunk(500);
        foreach($chunks as $chunk) {
            Fcm::insert($chunk->toArray());
        }
        Log::info("finish checkexpire");
       return;
    }
    public function palletprocess () {
        $data = DB::table('mod_pallet_manage')
        ->where('type_cd', 'driver')
        ->where('pallet_num', '<', 0)
        ->where('owner_by','0931451636')
        ->get();
        
        foreach ($data as $key => $row) {
            $detail = DB::table('mod_pallet_manage_log')
            ->select(
                DB::raw("sum(pallet_num)as total_pallet_num")
            )
            ->where('type_cd', 'driver')
            ->where('owner_by', $row->owner_by)
            ->where('pallet_type', $row->pallet_type)
            ->first();

            DB::table('mod_pallet_manage_log')
            ->insert([
                'ready_go'       => 'Y',
                'move_type'      => '系統調整',
                'control_from'   => 'app',
                'owner_by'       => $row->owner_by,
                'owner_name'     => $row->owner_name,
                'cust_no'        => $row->cust_no,
                'cust_name'      => $row->cust_name,
                'pallet_num'     => abs($detail->total_pallet_num),
                'pallet_code'    => $row->pallet_code,
                'pallet_type'    => $row->pallet_type.'(系統處理)',
                'type_cd'        => $row->type_cd,
                'type_name'      => $row->type_name,
                'from_wh_cd'     => $row->from_wh_cd,
                'from_wh_name'   => $row->from_wh_name,
                'wh_no'          => $row->wh_no,
                'wh_name'        => $row->wh_name,
                'dlv_no'         => $row->dlv_no,
                'is_update'      => 'Y',
                'wms_ord_no'     => '',
                'truck_cmp_name' => $row->truck_cmp_name,
                'updated_by'     => $row->updated_by,
                'created_by'     => $row->created_by,
                'created_at'     => date("Y-m-d H:i:s"),
                'updated_at'     => date("Y-m-d H:i:s"),
            ]);

            DB::table('mod_pallet_manage')
            ->where('id',  $row->id)
            ->update([
                'pallet_num' => 0
            ]);

        }
        return;
    }


    public function checkdriverpallet () {
        //假設從30開始
        try {
            $drvierpallet = DB::table('mod_pallet_manage')
            ->select(
                'owner_by','from_wh_cd', 'pallet_code',
                DB::raw("sum(pallet_num) as pallet_num")
            )
            ->where('type_cd','driver')
            ->where('ready_go','Y')
            ->where('created_at', '>=', '2023-06-30 00:00:00')
            ->groupBy('pallet_code','owner_by','from_wh_cd')
            ->get();
    
            foreach ($drvierpallet as $key => $row) {
                # code...
                $palletlog = DB::table('mod_pallet_manage_log')
                ->select(
                    DB::raw("sum(pallet_num)as pallet_num")
                )
                ->where('type_cd','driver')
                ->where('ready_go','Y')
                ->where('owner_by', $row->owner_by)
                ->where('from_wh_cd', $row->from_wh_cd)
                ->where('pallet_code', $row->pallet_code)
                ->where('created_at', '>=', '2023-06-30 00:00:00')
                ->first();
                
                if($row->pallet_num != $palletlog->pallet_num) {
                    DB::table('mod_pallet_manage')
                    ->where('type_cd','driver')
                    ->where('ready_go','Y')
                    ->where('owner_by', $row->owner_by)
                    ->where('from_wh_cd', $row->from_wh_cd)
                    ->where('pallet_code', $row->pallet_code)
                    ->where('created_at', '>=', '2023-06-30 00:00:00')
                    ->update([
                        'pallet_num'  => 0,
                        'num_from_wh' => 0,
                    ]);
                    if($palletlog->pallet_num > 0 ) {
                        $final =  DB::table('mod_pallet_manage')
                        ->where('type_cd','driver')
                        ->where('ready_go','Y')
                        ->where('owner_by', $row->owner_by)
                        ->where('from_wh_cd', $row->from_wh_cd)
                        ->where('pallet_code', $row->pallet_code)
                        ->where('created_at', '>=', '2023-06-30 00:00:00')
                        ->first();
    
                        DB::table('mod_pallet_manage')
                        ->where('id', $final->id)
                        ->update([
                            'pallet_num'  => $palletlog->pallet_num,
                            'num_from_wh' => $palletlog->pallet_num,
                        ]);
                    }
    
    
                }
                
            }
        } catch (\Throwable $e) {
            Log::info('checkdriverpallet error');
            Log::info( $e->getMessage());
            return "fail";
        }



        return "success";

    }

    public function carboncal () {

        $now = date("Y-m-d 00:00:00");
        $dssdlvs =  DB::table('mod_dss_dlv')
        ->whereIn('dlv_no', ['C023071000079'])
        ->get();
        // $dssdlvs =  DB::table('mod_dss_dlv')
        // ->where('dlv_no', 'A023062900106')
        // ->get();
        
        foreach ($dssdlvs as $key => $dssdlv) {
            # code...
            //逆物流
            $deliveryN =  DB::table('mod_dlv_plan')
            ->where('dlv_no',$dssdlv->dlv_no)
            ->where('backdelivery','Y')
            ->where('trs_mode','dlv')
            ->whereNotNull('arrive_lat')
            ->pluck('id')
            ->toArray();

            //純逆物流
            $onlydeliveryN =  DB::table('mod_dlv_plan')
            ->where('dlv_no',$dssdlv->dlv_no)
            ->where('backdelivery','Y')
            ->where('trs_mode','pick')
            ->whereNotNull('arrive_lat')
            ->pluck('id')
            ->toArray();

            $deliveryY =  DB::table('mod_dlv_plan')
            ->where('dlv_no',$dssdlv->dlv_no)
            ->where('backdelivery','!=' ,'Y')
            ->where('trs_mode','dlv')
            ->whereNotNull('arrive_lat')
            ->orderBy('finish_time','asc')
            ->get();

            $warehouse =  DB::table('mod_warehouse')
            ->where('cust_no', $dssdlv->dc_id)
            ->first();

            $carbon = DB::table('mod_carbon_cal')
            ->where('car_type', $dssdlv->car_type)
            ->first();

            $gpsarray = array();
            foreach ($deliveryY as $index => $row) {
                $distance = 0 ;
                $spellcarbon = 0;
                Log::info('到點經緯度');
                Log::info($row->arrive_lat);
                Log::info($row->arrive_lng);
                if( $index == 0 ) {
                    $distance = $this->getDistanceBetweenPointsNew($row->arrive_lat, $row->arrive_lng, $warehouse->lat, $warehouse->lng );
                } else {
                    $distance = $this->getDistanceBetweenPointsNew($row->arrive_lat, $row->arrive_lng, $deliveryY[$index-1]->arrive_lat, $deliveryY[$index-1]->arrive_lng );
                }
                // 同地址只算一次
                if(!in_array(  $row->cust_address, $gpsarray) ) {
                    $gpsarray[] = $row->cust_address;
                } else {
                    continue;
                }

                $distance = abs($distance);
                Log::info('dlv_plan id');
                Log::info($row->id);
                Log::info('碳排放量');
                Log::info($distance);
                Log::info($carbon->carbon_value);
                $spellcarbon = $distance *  $carbon->carbon_value;

                $samedata = DB::table('mod_dlv_plan')
                ->where('status','FINISHED')
                ->where('dlv_no',$dssdlv->dlv_no)
                ->where('cust_address', $row->cust_address)
                ->where('backdelivery','=' ,'N')
                ->whereNotNull('arrive_lat')
                ->get();
                Log::info('samedata count');
                Log::info(count($samedata));
                $sameid     = array();
                $samewmsord = array();

                foreach ($samedata as $key => $samedetail) {
                    $sameid[] =  $samedetail->id;
                    $samewmsord[] =  $samedetail->wms_ord_no;
                }
                $samecount =  count($samedata) ;
                Log::info('samewmsord data');
                Log::info(json_encode($samewmsord));
                Log::info($samecount);
                Log::info('count');

                $distance    = $distance / $samecount;
                $spellcarbon = $spellcarbon / $samecount;
                
                DB::table('mod_dlv_plan')
                ->whereIn('id', $sameid)
                ->update([
                    'carbon_distance' => $distance / $samecount,
                    'carbon'          => $spellcarbon / $samecount,
                ]);

                $ordervalue =  DB::table('mod_order')
                ->whereIn('ord_no', $samewmsord)
                ->get();
                Log::info('ordervalue');
                Log::info(count($ordervalue));

                foreach ($ordervalue as $key => $orderrow) {
                    $orddistince = (float)$orderrow->carbon_distance;
                    $ordcarbone = (float)$orderrow->carbon;
                    Log::info('fororder value');
                    Log::info( $orddistince + $distance);
                    Log::info( $ordcarbone  + $spellcarbon);

                    Log::info($orderrow->id);
                    DB::table('mod_order')
                    ->where('id', $orderrow->id)
                    ->update([
                        'carbon_distance' => $orddistince + $distance,
                        'carbon'          => $ordcarbone  + $spellcarbon,
                    ]);
                }

            }

        }

        if(count($deliveryN)>0 || count($onlydeliveryN)>0) {

            $distance = 0 ;
            $spellcarbon = 0;
            $uncount =  count($deliveryN) ;

            if(count($deliveryY)>0) {
                Log::info('逆物流到點經緯度');
                Log::info($deliveryY[$index]->arrive_lat);
                Log::info($deliveryY[$index]->arrive_lng);
                Log::info('逆物流到點經緯度end');
                $distance = $this->getDistanceBetweenPointsNew($deliveryY[$index]->arrive_lat, $deliveryY[$index]->arrive_lng, $warehouse->lat, $warehouse->lng );
            } else {
                $uncount =  count($onlydeliveryN) ;
                $deliveryN = $onlydeliveryN;
                $backdeliverydata =  DB::table('mod_dlv_plan')
                ->where('dlv_no',$dssdlv->dlv_no)
                ->where('backdelivery','Y')
                ->where('trs_mode','pick')
                ->whereNotNull('arrive_lat')
                ->orderBy('finish_time','desc')
                ->first();
                Log::info('純逆物流到點經緯度');
                Log::info($backdeliverydata->arrive_lat);
                Log::info($backdeliverydata->arrive_lng);
                Log::info('逆物流到點經緯度end');
                $distance = $this->getDistanceBetweenPointsNew($backdeliverydata->arrive_lat, $backdeliverydata->arrive_lng, $warehouse->lat, $warehouse->lng );
            }
            $distance = abs($distance);

            Log::info('back dlv_plan id');
            Log::info(json_encode($deliveryN));
            Log::info('逆物流碳排放量');
            Log::info($uncount);
            Log::info($distance);
            Log::info(($distance *  $carbon->carbon_value) / $uncount );
            $distance = $distance  / $uncount;
            $spellcarbon = ($distance * $carbon->carbon_value) ;

            DB::table('mod_dlv_plan')
            ->whereIn('id',$deliveryN)
            ->update([
                'carbon_distance' => $distance,
                'carbon'          => $spellcarbon,
            ]);

            $wmsords =  DB::table('mod_dlv_plan')
            ->whereIn('id',$deliveryN)
            ->pluck('wms_ord_no')
            ->toArray();

            $ordervalue =  DB::table('mod_order')
            ->whereIn('ord_no', $wmsords)
            ->get();
            foreach ($ordervalue as $key => $orderrow) {
                $orddistince = (float)$orderrow->carbon_distance;
                $ordcarbone = (float)$orderrow->carbon;
                Log::info('fororder value');
                Log::info( $orddistince + $distance);
                Log::info( $ordcarbone  + $spellcarbon);

                Log::info($orderrow->id);
                DB::table('mod_order')
                ->where('id', $orderrow->id)
                ->update([
                    'carbon_distance' => $orddistince + $distance,
                    'carbon'          => $ordcarbone  + $spellcarbon,
                ]);
            }
        }

        return true;
        

    }

    function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'kilometers') {
        Log::info('對比經緯度');
        Log::info($latitude2);
        Log::info($longitude2);

        $theta = $longitude1 - $longitude2; 
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 
        $distance = acos($distance); 
        $distance = rad2deg($distance); 
        $distance = $distance * 60 * 1.1515; 
        switch($unit) { 
          case 'miles': 
            break; 
          case 'kilometers' : 
            $distance = $distance * 1.609344; 
        } 
        return (round($distance,2)); 
    }

}
