<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Illuminate\Support\Facades\Log;


class BaseExport extends DefaultValueBinder implements FromView, WithCustomValueBinder {

    use Exportable;

    protected $data;
    protected $headers;
    protected $showCols;
    protected $stringCol = array();
    
    public function __construct($data, $headers, $showCols)
    {
        $this->data      = $data;
        $this->headers   = $headers;
        $this->showCols   = $showCols;
        // $this->stringCol = array();

    }

    /**
     * @return array
     */
    public function view(): View
    {
        return view('exports.baseExport', [
            'data' => collect($this->data)->map(function($x){ return (array) $x; })->toArray(),
            'headers' => $this->headers,
            'showCols' => $this->showCols
        ]);
    }

    public function bindValue(Cell $cell, $value)
    {
        $firstStr = substr($value, 0, 1);
        $secondStr = substr($value, 1, 1);
        
        if(strpos($value, '料號') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '名稱') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '色碼') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '條碼') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '託運單號') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }
        
        // Log::info($value);
        if (!empty($value) && is_numeric($value) && $secondStr != 'E' && strlen((string) $value) < 12 && !in_array($cell->getColumn(), $this->stringCol)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }
        else if (is_numeric($value) && strlen((string) $value) == 1 && $firstStr == 0 && !in_array($cell->getColumn(), $this->stringCol)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }
        else {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

}