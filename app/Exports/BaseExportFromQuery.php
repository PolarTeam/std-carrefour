<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Illuminate\Support\Facades\Log;
use App\StockByProduct;


class BaseExportFromQuery extends DefaultValueBinder implements FromCollection, WithHeadings, WithCustomValueBinder {

    use Exportable;

    protected $data;
    protected $headers;
    protected $showCols;
    protected $stringCol = array();
    
    public function __construct($data, $headers, $showCols)
    {
        $this->data      = $data;
        $this->headers   = $headers;
        $this->showCols   = $showCols;
        // $this->stringCol = array();

    }

    /**
     * @return array
     */
    public function collection()
    {
        ini_set('memory_limit', '2056M');
        $oData = $this->data;
        $data = array();
        foreach($oData as $row) {
            $detail = array();

            foreach($this->showCols as $showCol) {
                $dData = (array)$row;
                
                $detail[$showCol] = isset($dData[$showCol]) ? $dData[$showCol]:"";
                
            }

            array_push($data, $detail);
        }
        
        return collect($data);
    }

    public function headings(): array
    {
        return $this->headers;
    }

    public function bindValue(Cell $cell, $value)
    {
        $firstStr = substr($value, 0, 1);
        $secondStr = substr($value, 1, 1);
        
        if(strpos($value, '料號') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '名稱') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '色碼') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '條碼') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if(strpos($value, '託運單號') !== false) {
            array_push($this->stringCol, $cell->getColumn());
        }

        if (!empty($value) && is_numeric($value) && $secondStr != 'E' && strlen((string) $value) < 12 && !in_array($cell->getColumn(), $this->stringCol)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }
        else if (is_numeric($value) && strlen((string) $value) == 1 && $firstStr == 0 && !in_array($cell->getColumn(), $this->stringCol)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }
        else {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

}