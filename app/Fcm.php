<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Fcm extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'mod_fcm';
    protected $fillable = [
        'id',
        'flag',
        'dlv_no',
        'wms_ord_no',
        'title',
        'sys_ord_no',
        'type',
        'to_by',
        'to_by_name',
        'content',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

}
