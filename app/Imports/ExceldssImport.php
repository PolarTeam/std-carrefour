<?php
namespace App\Imports;

use App\Repositories\ExcelImportRepository;
use Exception;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ExceldssImport implements OnEachRow, WithHeadingRow
{
    protected $importTime;
    protected $excelimportRepo;

    public function __construct() {
        $this->excelimportRepo    = new ExcelImportRepository;
    }

    public function onRow(Row $row) {
        $user = Auth::user();

        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
        $mappingDetails = $this->getMappingDetail('DSSORDER_IMPORT');
        $insertData     = array();

        foreach($mappingDetails as $mappingDetail) {

            if(isset($row[$mappingDetail->excel_col_name]) && !empty($row[$mappingDetail->excel_col_name])  ) {
                $insertData[$mappingDetail->col_name] = $row[$mappingDetail->excel_col_name];
            } 
        }

        $insertData['created_by']      = $user->email;  //
        $insertData['updated_by']      = $user->email;  //
        $insertData['g_key']      = $user->g_key;  //
        $insertData['c_key']      = isset($insertData['s_key']) ? $insertData['s_key']:$user->s_key;
        $insertData['s_key']      = $user->s_key;  //
        $insertData['d_key']      = $user->d_key;  //
        $this->excelimportRepo->createdss($insertData);
    }

    public function headingRow(): int {
        return 1; //指定第n列為表頭
    }


    public function getMappingDetail($mappingCode) {
        return DB::table('mapping_detail')
                ->where('mapping_code', $mappingCode)
                ->orderBy('seq', 'asc')
                ->get();

    }
}
