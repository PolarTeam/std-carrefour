<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class AutoNoModel extends Model
{
    use AutoNumberTrait;

    protected $attributes = ['num_format' => ""];
    protected $fillable = ['auto_no', 'num_format', 'created_at', 'updated_at'];
    protected $table = 'auto_no_models';
    /**
     * Return the autonumber configuration array for this model.
     *
     * @return array
     */
    public function getAutoNumberOptions()
    {
        return [
            'auto_no' => [
                'format' => function () {
                    return $this->attributes['num_format']; // autonumber format. '?' will be replaced with the generated number.
                },
                'length' => 5 // The number of digits in an autonumber
            ]
        ];
    }

    


}