<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = [
        'id',
        'name',
        'display_name',
        'type',
        'sorted',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'color',
        'level',
        'guard_name',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
