<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\APP;
use App\Helpers\LogActivity;

class UserMgmtController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function index() {
        $user = Auth::user();
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){

        } else {
            return back();
        }
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='users' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));
            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('users.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        => trans('users.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }
        $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='users' AND user_id={$user->id} LIMIT 1";
        $gridLayout = DB::select($sql);

        $columns = array();
        if(!$gridLayout) {
            //預設layout
            $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='users' AND user_id = '29' LIMIT 1";
            $gridLayout = DB::select($sql);
            //連預設都沒有的時候用產生的
            if(!$gridLayout) {
                $columns = json_encode($cmptycolumns);
            } else {
                $columns = $gridLayout[0]->layout_content;
            }

        } else {
            $columns = $gridLayout[0]->layout_content;
        }
        $lang = app()->getLocale();
        return view('user.index', array(
            'title'      => trans('users.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));
    }

    public function edit($lang,$id) {
        // if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        // } else {
        //     return back();
        // }
        $this->data['title']       = trans('users.titleName');
        $this->data['entry']       = array();
        $this->data['id']          = $id;
        $this->data['lang']        = $lang;
        $this->data['role']        = $this->getRole();
        $this->data['check_wh']    = $this->getWh();
        $this->data['check_owner'] = $this->getOwner();

        return view('user.edit', $this->data);
    }

    public function create() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title']       = trans('users.titleName');
        $this->data['entry']       = array();
        $this->data['id']          = null;
        $this->data['role']        = $this->getRole();
        $this->data['check_wh']    = $this->getWh();
        $this->data['check_owner'] = $this->getOwner();
        $this->data['lang']        = app()->getLocale();
        return view('user.edit', $this->data);
    }

    public function show($lang,$id) {
        try {
            $userdata = $this->userService->get($id);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success'     => true,
            'message'     => "get user success",
            'data'        => $userdata,
            'filed'       => $this->userService->getfiled('users'),
            'role'        => $this->getRole(),
            'check_wh'    => $this->getWh(),
            'delivery_wh' => $this->getWh(),
            'check_owner' => $this->getOwner(),
        ]);
    }
    public function usereditshow($id) {
        try {
            $userdata = $this->userService->get($id);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success'     => true,
            'message'     => "get user success",
            'data'        => $userdata,
            'filed'       => $this->userService->getfiled('users'),
            'role'        => $this->getRole(),
            'check_wh'    => $this->getWh(),
            'delivery_wh'    => $this->getWh(),
            'check_owner' => $this->getOwner(),
        ]);
    }

    public function getRole () {
        return DB::table('roles')
        ->select('id', 'name', 'display_name')
        ->get();

    }

    public function getWh () {
        return DB::table('mod_warehouse')
        ->select('cust_no', 'cname')
        ->whereNotNull('dc_id')
        ->where('status', 'A')
        ->get();

    }

    public function getOwner () {
        return DB::table('mod_customer_group')
        ->select(
        'group_cd',
        'group_name')
        ->get();

    }

    public function store (Request $request) {

        $result = array();
        try {
            $result = $this->userService->create($request);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('新增user');
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'lastId' => $result['id']
        ]);
    }

    public function update (Request $request, $lang, $id) {
        $result = array();
        try {
            $result = $this->userService->update($request, $id);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('修改user');
        return response()->json([
            'success' => true,
            'message' => '修改成功',
            'data' => $this->userService->get($id),
            'filed'   => $this->userService->getfiled('users')
        ]);
    }
    public function checkexpire (Request $request) {
        $result = array();
        try {
            // cpanel
            $result = $this->userService->checkexpire($request);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('修改user');
        return response()->json([
            'success' => true,
            'message' => '修改成功',
            'data' => $result,
            'filed'   => $this->userService->getfiled('users')
        ]);
    }
    public function expireupdate (Request $request) {
        $result = array();
        try {
            // cpanel
            $result = $this->userService->expireupdate($request);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('修改user');
        return response()->json([
            'success' => true,
            'message' => '修改成功',
            'data' => $result,
            'filed'   => $this->userService->getfiled('users')
        ]);
    }

    public function destroy (Request $request, $lang, $id) {
        try {
            $this->userService->delete($id);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除user');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function batchDelete (Request $request) {

        DB::beginTransaction();

        try {
            $this->userService->batchDelete($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除user');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function query(Request $request) {
        $result = array();

        try {
            $result = $this->userService->query($request);
        }
        catch(Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'data'       => $result['data'],
            'totalCount' => $result['totalCount'],
            'message'    => 'success'
        ));
    }
    public function custuser(){
        $user = Auth::user();
        if(Auth::user()->hasPermissionTo('CUSTUSER')){

        } else {
            return back();
        }
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='users' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));
            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('users.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        => trans('users.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }
        $columns = json_encode($cmptycolumns);

        $lang = app()->getLocale();
        return view('user.custuser', array(
            'title'      => trans('users.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));
    }
    public function getpermissionUser($userId) {
        // $orderDetail = [];
        // if($ord_id != null) {
        //     $this_query = DB::table('mod_order_detail');
        //     $this_query->where('ord_id', $ord_id);
        //     $orderDetail = $this_query->get();
        // }
        // $data[] = array(
        //     'Rows' => $orderDetail,
        // );

        try {
            $result = $this->userService->getUserPermission($userId);
        }
        catch(Exception $e) {
            return response()->json(array(
                'success' => false,
                'data'    => array(),
                'message' => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success' => true,
            'data'    => $result,
            'message' => 'success'
        ));
    }

    public function queryemail() {
        try {
            $user = Auth::user();
            $sql = "SELECT name FROM users WHERE id != {$user->id} ";
            $data = DB::select($sql);
        }
        catch(Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'data'       => $data,
            'totalCount' => count($data),
            'message'    => 'success'
        ));
    }
    public function queryemailall() {
        try {
            $user = Auth::user();
            $sql = "SELECT name FROM users ";
            $data = DB::select($sql);
        }
        catch(Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'data'       => $data,
            'totalCount' => count($data),
            'message'    => 'success'
        ));
    }

    public function setDashboardReadList(Request $request) {
        $result = array();
        try {
            $result = $this->userService->setDashboardReadList($request);
        }
        catch(Exception $e) {
            Log::error($e);
            return response()->json(array(
                'success'    => false,
                'data'       => null,
                'message'    => $e->getMessage()
            ));
        }

        LogActivity::addToLog('修改user dashboardReadList');
        return response()->json(array(
            'success'    => true,
            'data' => $result,
            'message'    => 'success'
        ));
    }

    public function exportdata(Request $request) {

        $downloadLink = '';

        try {
            $downloadLink = $this->userService->export($request);
        }
        catch(\Exception $e) {
            Log::error($e);
            return response()->json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }

        if(empty($downloadLink)) {
            return response()->json(array(
                'success' => false,
                'message' => '下載錯誤'
            ));
        }

        return response()->json(array(
            'success' => true,
            'donwloadLink' => $downloadLink,
            'message' => 'success'
        ));
    }

}
