<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\UserService;

class UserController extends Controller {
    const SUCCUSUS_STATUS_CODE = 200;
    const UNAUTHORISED_STATUS_CODE = 401;

    protected $userService;

    public function __construct(UserRepositoryInterface $userRepository, UserService $userService) {
        $this->userRepository = $userRepository;
        $this->userService    = $userService;
    }

    public function login(UserLoginRequest $request) {
        $response = $this->userRepository->login($request);
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function register(UserRegisterRequest $request) {
        $response = $this->userRepository->register($request);
        return response()->json($response["data"], $response["statusCode"]);
    }
    public function appregister(Request $request) {
        try {
            return $response = $this->userRepository->appregister($request);
        }
        catch(\Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }
    }
    public function updateUser(Request $request) {
        try {
            return $response = $this->userRepository->updateUser($request);
        }
        catch(\Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }
    }

    public function details() {
        $response = $this->userRepository->details();
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function logout(Request $request) {
        $response = $this->userRepository->logout($request);
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function refreshToken(Request $request) {
        $response = $this->userRepository->refreshToken($request);
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function superLogout(Request $request) {
        $response = $this->userRepository->superLogout($request);
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function getOnlineUser(Request $request) {
        $response = $this->userRepository->getOnlineUser($request);
        return response()->json($response["data"], $response["statusCode"]);
    }

    public function query (Request $request) {
        $result = array();

        try {
            $result = $this->userService->query($request);
        }
        catch(\Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'data'       => $result['data'],
            'totalCount' => $result['totalCount'],
            'message'    => 'success'
        ));
    }

    public function sendMsg(Request $request) {
        try {
            $response = $this->userRepository->sendMsg($request);
        }
        
        catch(\Exception $e) {
            return response()->json(array(
                'success'    => false,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'message'    => 'success'
        ));
    }

    public function validationCode(Request $request) {
        try {
            $response = $this->userRepository->validationCode($request);
        }
        
        catch(\Exception $e) {
            return response()->json(array(
                'success'    => false,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'message'    => 'success'
        ));
    }
}
