<?php

namespace App\Http\Controllers;

use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class DashboardController extends Controller
{
    
    public function dashboard() {
        $title = trans('dashboard.titleName');
        //echo "<title>$title</title>";
        $user = Auth::user();
        $now = date('Y-m-d');

        $ordSumQuery = DB::table('mod_order')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);

        if($user->identity == 'G') {
            $ordSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
            $ordSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordSumQuery->where('g_key', $user->g_key);
            $ordSumQuery->where('c_key', $user->c_key);
            $ordSumQuery->where('s_key', $user->s_key);
            $ordSumQuery->where('d_key', $user->d_key);
        }
        $ordSum = $ordSumQuery->count();

        $ordcloseSumQuery = DB::table('mod_order_close')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);

        if($user->identity == 'G') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
            $ordcloseSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseSumQuery->where('g_key', $user->g_key);
            $ordcloseSumQuery->where('c_key', $user->c_key);
            $ordcloseSumQuery->where('s_key', $user->s_key);
            $ordcloseSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseSum = $ordcloseSumQuery->count();

        $ordSum = $ordSum+$ordcloseSum;

        $ordErrorSumQuery = DB::table('mod_order')->where('status', 'ERROR')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
            $ordErrorSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordErrorSumQuery->where('g_key', $user->g_key);
            $ordErrorSumQuery->where('c_key', $user->c_key);
            $ordErrorSumQuery->where('s_key', $user->s_key);
            $ordErrorSumQuery->where('d_key', $user->d_key);
        }
        $ordErrorSum = $ordErrorSumQuery->count();

        $ordcloseErrorSumQuery = DB::table('mod_order_close')->where('status', 'ERROR')->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
            $ordcloseErrorSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseErrorSumQuery->where('g_key', $user->g_key);
            $ordcloseErrorSumQuery->where('c_key', $user->c_key);
            $ordcloseErrorSumQuery->where('s_key', $user->s_key);
            $ordcloseErrorSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseErrorSum = $ordcloseErrorSumQuery->count();
        $ordErrorSum = $ordErrorSum+$ordcloseErrorSum;

        $ordFinishSumQuery = DB::table('mod_order')
                                ->where('status', 'FINISHED')
                                ->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
            $ordFinishSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordFinishSumQuery->where('g_key', $user->g_key);
            $ordFinishSumQuery->where('c_key', $user->c_key);
            $ordFinishSumQuery->where('s_key', $user->s_key);
            $ordFinishSumQuery->where('d_key', $user->d_key);
        }
        $ordFinishSum = $ordFinishSumQuery->count();

        $ordcloseFinishSumQuery = DB::table('mod_order_close')
            ->where('status', 'FINISHED')
            ->whereBetween('etd', [$now.' 00:00:00', $now.' 23:59:59']);
        if($user->identity == 'G') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
            $ordcloseFinishSumQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $ordcloseFinishSumQuery->where('g_key', $user->g_key);
            $ordcloseFinishSumQuery->where('c_key', $user->c_key);
            $ordcloseFinishSumQuery->where('s_key', $user->s_key);
            $ordcloseFinishSumQuery->where('d_key', $user->d_key);
        }
        $ordcloseFinishSum = $ordcloseFinishSumQuery->count();
        $ordFinishSum = $ordFinishSum + $ordcloseFinishSum;
        
        $lastTimeQuery = DB::table('mod_car_real_data')
                            ->select('created_at')
                            ->where('status', 'SETOFF')
                            ->whereBetween('created_at', [$now.' 00:00:00', $now.' 23:59:59'])
                            ->orderBy('created_at', 'desc');
        if($user->identity == 'G') {
            $lastTimeQuery->where('g_key', $user->g_key);
        }
        else if($user->identity == 'C') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
        }
        else if($user->identity == 'S') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
            $lastTimeQuery->where('s_key', $user->s_key);
        }
        else if($user->identity == 'D') {
            $lastTimeQuery->where('g_key', $user->g_key);
            $lastTimeQuery->where('c_key', $user->c_key);
            $lastTimeQuery->where('s_key', $user->s_key);
            $lastTimeQuery->where('d_key', $user->d_key);
        }

        $lastTime = $lastTimeQuery->first();
        $nowTime = date('Y-m-d H:i:s', strtotime("-300 second"));
        $cardata = DB::table('users') 
        ->select(
            'email', 'final_refresh',DB::raw('IFNULL(gps_time, "") as gps_time'),
            DB::raw("case  when  final_refresh > '".$nowTime."' then 'Y' else 'N'end as status")
        )
        ->whereNotNull('final_refresh')
        ->where('c_key', $user->c_key)
        ->where('g_key', $user->g_key)
        ->orderBy('id')
        ->get();
    
        $ordCompleteNum = 0;
        if($ordSum > 0) {
            $ordCompleteNum = round(($ordFinishSum/$ordSum) * 100);
        }
        $file_size = 0;
		$foldersize = DB::table('bscode')
		->where("cd_type","FORDERSIZE")
        ->where("cd","fordersize")
        ->first();
        $file_size = (int)$foldersize->value1;
        $file_size = $file_size/1048576;
        $viewData = array(
            'ordSum'         => $ordSum,
            'ordErrorSum'    => $ordErrorSum,
            'ordCompleteNum' => $ordCompleteNum,
            'ordCompleteNum' => $ordCompleteNum,
            'cardata'        => $cardata,
            'foldersize'     => number_format((int)$file_size),
            'lastTime'       => (isset($lastTime))? $lastTime->created_at: ''
        );
        try{
            return view('dashboard')->with('viewData', $viewData);
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function DriverSigIn(Request $request) {
        $container = DB::table('bscode')
		->where("cd_type","container")
        ->get();
		$carType = DB::table('bscode')
        ->select('cd',
            DB::raw("cd as cd_descp")
        )
		->where("cd_type","IMPORTCARTYPE")
        ->get();

        $dcid = $request->dcid;
        
        $customerrelation = DB::table('mod_cust_wh')
        ->where("dc_id", $dcid)
        ->pluck('cust_no')
        ->toArray();
      
        //2023-07-03 問題單256 台北、觀音、台中、高雄分貨主 貨主欄位要加上貨主代碼
        $customer = DB::table('sys_customers')
        ->whereIn("cust_no",$customerrelation)
        ->where("type","other")
        ->get();


        $viewData = array(
            'carType'   => $carType,
            'container' => $container,
            'customer'  => $customer,
            'dc_id'     => $dcid,
        );

        return view('driversigin')->with('viewData',  $viewData);
    }

    public function finalcardata(Request $request) {
        $today = date("Y-m-d");

        $insidecar = DB::table('mod_mark_reserve')
        ->select(
            DB::raw('driver_name as driver_name'),
            DB::raw('owner_name as owner_name'),
            DB::raw('owner_no as owner_cd'),
            DB::raw('truck_cmp_no as truck_cmp_no'),
            DB::raw('truck_cmp_name as truck_cmp_name'),
            DB::raw('car_type as car_type'),
            DB::raw('car_type_descp as car_type_descp'),
            DB::raw('unloading_descp as unloading_type'),
            'con_no',
            'con_type',
            'con_type_desc',
            'car_no'
        )
        ->where('driver_phone', $request->phone)
        ->where('reserve_date', $today)
        ->where('onsite', 'N')
        ->orderBy('id','desc')
        ->first();
        if(isset($insidecar) ) {
            return response()->json($insidecar);
        } else {
            $insidecar = DB::table('mod_driver_checkin')
            ->select(
                DB::raw('driver_name as driver_name'),
                DB::raw('owner_nm as owner_name'),
                DB::raw('owner_cd as owner_cd'),
                DB::raw('truck_cmp_no as truck_cmp_no'),
                DB::raw('truck_cmp_name as truck_cmp_name'),
                DB::raw('car_type as car_type'),
                DB::raw('car_type_desc as car_type_descp'),
                DB::raw('con_id as con_no'),
                DB::raw('unloading_type_desc as unloading_type'),
                'con_type',
                'con_type_desc',
                'car_no'
            )
            ->where('phone', $request->phone)
            ->orderBy('id','desc')
            ->first();
        }



        return response()->json($insidecar);
    }
    public function carleave(Request $request) {
        $now = date("Y-m-d H:i:s");    

        $final = DB::table('mod_driver_checkin')
        ->where('car_no', $request->car_no)
        ->orderBy('id','desc')
        ->first();
        
        $insidecar = DB::table('mod_driver_checkin')
        ->where('id', $final->id)
        ->whereNull('leave_time')
        ->update([
            'status'      => 'export',
            'status_desc' => '離場',
            'leave_time'  => $now
        ]);
        $ogdata = DB::table('mod_driver_checkin')
        ->where('car_no', $request->car_no)
        ->orderBy('created_at', 'desc')
        ->first();
        // discharged_status
        if($ogdata->job_type == "拉空櫃" ||  $ogdata->job_type == "換櫃"  || ( $ogdata->job_type == "進貨" && $ogdata->discharged_status == "(空車頭放行)" ) ) {

        } else {
            DB::table('mod_mark')
            ->select('cust_no','cust_name')
            ->where('cust_no', $ogdata->mark_no)
            ->update([
                'in_use' => 'N'
            ]);
        }


        return response()->json($insidecar);
    }
    public function security() {
        $user = Auth::user();
        $now = date("Y-m-d 00:00:00");
        $checkWarehouse = explode(',',$user->check_wh);

        $newdcid = array();
        foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
            $dcid =  substr($fordetailrow,0,2);
            if($dcid != "A0" && $dcid != 'B0') {
                $newdcid[] = substr($fordetailrow,0,2);
            } else {
                $newdcid[] = $fordetailrow;
            }
        }
        $today = date("Y-m-d 00:00:00");
        // $whDataName = DB::table('mod_warehouse')
        // ->WhereIn('cust_no', $newdcid)
        // ->where('status','A')
        // ->pluck('cname');


        // $changocar = DB::table('mod_dlv_plan')
        // ->whereNotNull('discharged_status')
        // ->whereNull('leave_time')
        // ->groupBy('car_no')
        // ->get();
        $carnos = DB::table('mod_driver_checkin')
        ->whereNotNull('discharged_status')
        ->whereNull('leave_time')
        ->groupBy('car_no')
        ->pluck('car_no')
        ->toArray();

        // 'button_status'      => 'STARTUPLOADING',
        $dlvcarnos = DB::table('mod_dlv_plan')
        ->select('car_no')
        ->whereIn('car_no', $carnos)
        ->where('button_status_desc', '待放行')
        ->pluck('car_no')->toArray();


        $cango = DB::table('mod_driver_checkin')
        ->whereNotIn('car_no', $dlvcarnos)
        ->whereNotNull('discharged_status')
        ->whereNull('leave_time')
        ->where('created_at', '>=', $today)
        ->groupBy('car_no')
        ->get();

        $cangocars = array();
        foreach ($cango as $key => $cangorow) {
            $cangocars[] =  $cangorow->car_no;
        }

        $insidecar = DB::table('mod_driver_checkin')
        ->whereNotIn('car_no', $cangocars)
        ->whereNull('leave_time')
        ->groupBy('car_no')
        ->get();

        $checkWh = $user->check_wh;

        $viewData = array(
            'cango'      => $cango,
            'insidecar'  => $insidecar,
            'readyleave' => null,
            'checkWh'    => $checkWh,
        );

        return view('security')->with('viewData', $viewData);
    }

    public function wharfStatus() {
        $now = date("Y-m-d H:i:00");
        $user = Auth::user();
        ini_set('memory_limit', '256M');

        $checkWarehouse = explode(',',$user->check_wh);
        $today = date("Y-m-d");
        $min = date("i");
        $now = date("Y-m-d H:i:00");
        $whData = DB::table('mod_warehouse')
        ->select('cust_no','cname','dc_id')
        ->WhereIn('cust_no', $checkWarehouse)
        ->whereNotNull('dc_id')
        ->where('status','A')
        ->get();
        $timeArray = array();
        $timeArray[0]['time'] = '目前時間';
        try{
            foreach ($whData as $key => $whrow) {
                if($whrow->dc_id != 'A0' && $whrow->dc_id  != 'B0') {
                    $whrow->cust_no = $whrow->dc_id ;
                }

                $mark = DB::table('mod_mark')
                ->select('cust_no','cust_name','wh_no')
                ->where('wh_no', $whrow->cust_no)
                ->where('is_enabled', 'Y')
                ->get();
                
                foreach ($mark as $markkey => $markrow) {
                    $firstmarkreserve = DB::table('mod_driver_checkin')
                    ->select('car_no', DB::raw(" entry_time as time"),DB::raw(" process_time as end_at"))
                    ->where('mark_no', $markrow->cust_no)
                    ->where('dc_id', $whrow->dc_id)
                    //0420 需再補上對應的倉庫
                    ->where('wh_no', $markrow->wh_no)
                    ->whereNull('leave_time')
                    ->Where(function($query) {
                        $query->whereIn('job_type', ['拉空櫃','換櫃','進貨','出貨'])
                        ->where('mark_no','!=','空地')
                        ->orWhereNull('discharged_status');
                    })
                    ->orderBy('created_at','desc')
                    ->take(1)
                    ->get()
                    ->toArray();

                    if(count($firstmarkreserve) == 0) {
                        $firstmarkreserve[0]['time'] = '目前時間';
                        $firstmarkreserve[0]['car_no'] = '-';
                        $firstmarkreserve[0]['end_at'] = '處理時間';
                        $firstmarkreserve = json_decode(json_encode($firstmarkreserve), FALSE);
                    }
                    $markreserveArray = $firstmarkreserve;

                    foreach ($markreserveArray as $schedulekey => $schedulerow) {
                        $schedulerow->end_at = empty($schedulerow->end_at) ? '' : $schedulerow->end_at;
                        $schedulerow->time = date('H:i', strtotime($schedulerow->time));
                    }
                    
                    $markrow->schedule = $markreserveArray;
                }
                $whrow->markdata = $mark;
            }
            
    
        }
        catch(\Exception $e) {
            return $e->getmessage();
        }
        $viewData = array(
            'allwh'      => $whData,
            'readyleave' => null,
        );

        return view('wharfStatus')->with('viewData', $viewData);
    }
    public function message() {

        // if(Auth::user()->hasPermissionTo('BASEMESSAGE')){
        // } else {
        //     return back();
        // }
        $user = Auth::user();
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='mod_message' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));

            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('mod_message.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        => trans('mod_message.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }

        $columns = json_encode($cmptycolumns);

        $lang = app()->getLocale();
        return view('message', array(
            'title'      => trans('mod_message.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));;


    }
    public function map() {
        $title = trans('dashboard.map');
        echo "<title>$title</title>";
        try{
            if(Auth::user()->hasPermissionTo('MAP'))
            {
                return view('map');
            }
        }
        catch(\Exception $e) {
            return back();
        }
    }
    public function dashboardText2() {
        $user = Auth::user();
        $today = date("Y-m-d ");
        $days = date("Y-m-d ", strtotime("- 3 day"));
        $checkWarehouse = explode(',',$user->check_wh);

        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->whereNotNull('dc_id')
        ->pluck('cust_no');

        $userOwnerGroup = explode(',',$user->check_owner);
        $checkOwner = DB::table('mod_customer_relation')
        ->whereIn('group_cd', $userOwnerGroup)
        ->pluck('cust_no')->toArray();
        
        $userwh2 =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->whereNotNull('dc_id')
        ->pluck('dc_id');

        $userdcid =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->whereNotNull('dc_id')
        ->pluck('dc_id')->toArray();

        $userWhDc =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $userdcid)
        ->pluck('cname')->toArray();
        $checkWarehouse = explode(',',$user->check_wh);
        $fromwh =  DB::table('mod_warehouse')
        ->select('*', 
            DB::raw("(select cname from mod_warehouse as b where cust_no =mod_warehouse.dc_id and dc_id IS NULL limit 1 ) as dc_name")
        )
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('dc_name');

        $errordata = array();
        if($user->check_owner != "*" ) {
            $expiedata = DB::table('mod_order')
            ->select('status','cust_ord_no','from_wh','owner_nm',
            'dlv_cust_nm','driver','driver_phone','dlv_date',
            'status_desc','created_at','upload_finish_time','unupload_time','upload_start_time','finish_time')
            ->where('dlv_date', '>=', $days)
            ->whereIn("owner_cd", $checkOwner)
            ->where(function ($query) use ($checkOwner, $userwh, $fromwh) {
                $query->whereIn("dc_id", $userwh)
                    ->orWhereIn('from_wh', $fromwh);
            })
            ->where("status",'!=', 'FINISHED')
            ->get();

            $errordata = DB::table('mod_error_report')
            ->select('status','id','owner_name','cust_name','created_by','phone','error_name','created_at','ord_no',
            DB::raw("(select from_wh from mod_order where ord_no = mod_error_report.wms_ord_no limit 1) as from_wh"))
            ->whereIn("owner_cd", $checkOwner)
            ->whereIn("dc_id", $userwh2)
            ->where("status",'!=', 'FINISHED')
            ->where("created_at", '>=', date("Y-m-d 00:00:00"))
            ->orderBy('id','desc')
            ->get();

        } else {
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            $dlvdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $dcid =  substr($fordetailrow,0,2);
                if($dcid != "A0" && $dcid != 'B0') {
                    $newdcid[] = substr($fordetailrow,0,2);
                } else {
                    $newdcid[] = $fordetailrow;
                }
                $dlvdcid[] = $dcid;
            }
            $expiedata = DB::table('mod_order')
            ->select('status','cust_ord_no','from_wh','owner_nm',
            'dlv_cust_nm','driver','driver_phone','dlv_date',
            'status_desc','created_at','upload_finish_time','unupload_time','upload_start_time','finish_time')
            ->where('dlv_date', '>=', $days)
            ->where('dlv_date', '<', $today)
            ->where("status",'!=', 'FINISHED')
            ->get();

            $errordata = DB::table('mod_error_report')
            ->select('status','id','owner_name','cust_name','created_by','phone','error_name','created_at','ord_no',
            DB::raw("(select from_wh from mod_order where ord_no = mod_error_report.wms_ord_no limit 1) as from_wh"))
            ->where("created_at", '>=', date("Y-m-d 00:00:00"))
            ->where("status",'!=', 'FINISHED')
            ->orderBy('id','desc')
            ->get();
            
        }


        foreach ($expiedata as $key => $row) {
            # code...
            // FINISHED	配送完成
            // OFFSTAFF	貨物卸載中
            // PICKEDFINISH	揀貨完成
            // UPLOADFINISH	提貨完成
            switch($row->status) { 
                case 'PICKEDFINISH': 
                    $row->show_time = $row->created_at;
                    break; 
                case 'UPLOADFINISH' : 
                    $row->show_time = $row->upload_finish_time;
                    break; 
                case 'OFFSTAFF' : 
                    $row->show_time = $row->unupload_time;
                    break; 
                case 'LOADING' : 
                    $row->show_time = $row->upload_start_time;
                    break;
                case 'FINISHED' : 
                    $row->show_time = $row->finish_time;
                    break; 
              }           
        }

        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardText2')
        ->value('value1');
        $viewData = array(
            'refreshtime' => (string)($refreshtime * 1000),
            'errordata'   => $errordata,
            'expiedata'   => $expiedata,
        );
        return view('dashboardText2')->with('viewData', $viewData);
    }

    public function dashboardChartWhbydate(Request $request){
        $user = Auth::user();
        //total_count
        $today = $request->date;
        $wharray = $request->wharray;

        DB::table('users')
        ->where('id',$user->id)
        ->update([
            'dashboard_wh' => implode(',',$wharray )
        ]);

        ini_set('memory_limit', '512M');
        $checkWarehouse = explode(',',$user->check_wh);
        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->whereNotNull('dc_id')
        ->pluck('dc_id');
        $newdcid = array();
        $custNos = array();
        //totalwarehousedata
        foreach ($checkWarehouse as $checkkey => &$checkrow) {
            $custNos[] = $checkrow;
            $whno = substr($checkrow,0,2);
            $newdcid[] = $whno;
            if($whno != "A0" && $whno != "B0") {
                $checkrow = $whno;
                Log::info( $checkrow);
            }
        }
        $deliveryWh = DB::table('mod_warehouse')
        ->whereIn('cust_no', $custNos)
        ->pluck('cust_no')
        ->toArray();
        $deliveryWhuser = DB::table('mod_warehouse')
        ->whereIn('cust_no', $checkWarehouse)
        ->pluck('cust_no')
        ->toArray();
        $userOwnerGroup = explode(',',$user->check_owner);
        $checkOwner = DB::table('mod_customer_relation')
        ->whereIn('group_cd', $userOwnerGroup)
        ->pluck('cust_no')->toArray();

        $managewarehouse = DB::table('mod_warehouse')
        ->select('*', 
            DB::raw("(select group_concat(cust_no) from mod_warehouse w where w.dc_id = mod_warehouse.cust_no) as underdcid")
        )
        ->whereIn("cust_no", $userwh)
        ->get();

        $totalwarehousedata = array();
        Log::info('貨主前判斷');
        Log::info($deliveryWh);
        if($user->check_owner != "*" ) {
            Log::info('各別貨主');
            Log::info($checkWarehouse);
            $totalwarehousedata = DB::table('mod_order')
            ->whereIn("owner_cd", $checkOwner)
            ->whereIn("dc_id", $deliveryWh)
            ->where("dlv_date", $today)
            ->get();
            // $this_query->whereIn("owner_cd", $checkOwner);
        } else {
            Log::info('all 貨主');
            $totalwarehousedata = DB::table('mod_order')
            ->whereIn("dc_id", $deliveryWh)
            ->where("dlv_date", $today)
            ->get();
        }


        $test = array();
        $totalarray = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0);
        foreach ($totalwarehousedata as $orderkey => $orderrow) {
            if($orderrow->status == "PICKEDFINISH") {
                $totalarray['PICKEDFINISH'] ++;
            } else if ($orderrow->status == "LOADING") {
                $totalarray['LOADING'] ++;
            } else if ($orderrow->status == "UPLOADFINISH") {
                $totalarray['PICKED'] ++;
            } else if ($orderrow->status == "OFFSTAFF") {
                $totalarray['OFFSTAFF'] ++;
            } else if ($orderrow->status == "FINISHED") {
                $totalarray['FINISHED'] ++;
            }
            if(!isset( $test[$orderrow->status])) {
                $test[$orderrow->status] = 0;
            } else {
                $test[$orderrow->status] ++;
            }
        }
        $totalwarehouse = count($totalwarehousedata);

        $north = DB::table('sys_area')
        ->where('station_nm', '北')
        ->pluck('dist_cd')->toArray();

        $middle = DB::table('sys_area')
        ->where('station_nm', '中')
        ->pluck('dist_cd')->toArray();

        $south = DB::table('sys_area')
        ->where('station_nm', '南')
        ->pluck('dist_cd')->toArray();

        $east = DB::table('sys_area')
        ->where('station_nm', '東')
        ->pluck('dist_cd')->toArray();

        if($user->check_owner != "*" ) {
            $orderbynorth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $north)
            ->where("from_wh", '台北倉')
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbymiddle = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $middle)
            ->where("from_wh", '觀音一倉')
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbysouth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $south)
            ->where("from_wh", '台中倉')
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbyeast = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $east)
            ->where("from_wh", '高雄倉')
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
        } else {
            $orderbynorth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $north)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbymiddle = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $middle)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbysouth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $south)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbyeast = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $east)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
        }


        foreach ($managewarehouse as $managekey => $managerow) {
            $dcid = [];

            if($managerow->cust_no == "A0" || $managerow->cust_no == "B0") {
                $userdetail = explode(',',$user->check_wh);
                foreach ($userdetail as $key => $userdetail) {
                    $whno = substr($userdetail,0,2); 
                    if(($whno == "A0" || $whno == "B0") && $whno == $managerow->cust_no) {
                        Log::info('紀錄dc_id');
                        Log::info($whno);
                        Log::info('紀錄明細');
                        Log::info( $userdetail);
                        $dcid[] = $userdetail;
                    }
                }
            } else {
                $dcid[] = $managerow->cust_no.'-1';
            }
            Log::info('紀錄like 用cust_no');
            Log::info($dcid);
            if($user->check_owner != "*" ) {
                $totalorder = DB::table('mod_order')
                ->whereIn("dc_id", $dcid)
                ->whereIn("owner_cd", $checkOwner)
                ->where("dlv_date", $today)
                ->get();
            } else {
                $totalorder = DB::table('mod_order')
                ->whereIn("dc_id", $dcid)
                ->where("dlv_date", $today)
                ->get();
            }



            $statuspickedfinish = 0;
            $statusloading = 0;
            $statuspicked = 0;
            $statusoffloading = 0;
            $statusfinished = 0;
            foreach ($totalorder as $orderkey => $orderrow) {
                if($orderrow->status == "PICKEDFINISH") {
                    $statuspickedfinish ++;
                } else if ($orderrow->status == "LOADING") {
                    $statusloading ++;
                } else if ($orderrow->status == "UPLOADFINISH") {
                    $statuspicked ++;
                } else if ($orderrow->status == "OFFSTAFF") {
                    $statusoffloading ++;
                } else if ($orderrow->status == "FINISHED") {
                    $statusfinished ++;
                }
            }

            $managerow->statuspickedfinish = $statuspickedfinish;
            $managerow->statusloading = $statusloading;
            $managerow->statuspicked = $statuspicked;
            $managerow->statusoffloading = $statusoffloading;
            $managerow->statusfinished = $statusfinished;

            $managerow->total_count = $statuspickedfinish+$statusloading+ $statuspicked+$statusoffloading+$statusfinished;
        }

        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardChartWh')
        ->value('value1');

        $viewData = array(
            'refreshtime'        => (string)($refreshtime * 1000),
            'totalwarehousedata' => $totalarray,
            'managewarehouse'    => $managewarehouse,
            'totalwarehouse'     => $totalwarehouse,
            'north'              => $orderbynorth,
            'middle'             => $orderbymiddle,
            'south'              => $orderbysouth,
            'east'               => $orderbyeast,
            
        );
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $viewData]);
    }

    public function dashboardChartOwnerbydate(Request $request) {
        $user = Auth::user();
        $userOwnerGroup = explode(',',$user->check_owner);

        $today = $request->date;
        // $orderowner = DB::table('mod_order')
        // ->where("dlv_date", $today)
        // ->groupBy('owner_cd')
        // ->pluck('owner_cd')
        // ->toArray();

        $postowner = $request->wharray;

        DB::table('users')
        ->where('id',$user->id)
        ->update([
            'dashboard_owner' => implode(',',$postowner )
        ]);

        $owner = DB::table('sys_customers')
        ->select('cust_no',DB::raw("cname as cust_name"))
        // ->whereIn('cust_no', $orderowner)
        ->where('type', 'other')
        ->get();

        if($user->check_owner != "*" ) {
            $checkOwner = DB::table('mod_customer_relation')
            ->whereIn('group_cd', $userOwnerGroup)
            ->pluck('cust_no')->toArray();
            $owner = DB::table('sys_customers')
            ->select('cust_no',DB::raw("cname as cust_name"))
            // ->whereIn('cust_no', $orderowner)
            ->whereIn('cust_no', $checkOwner)
            ->where('type', 'other')
            ->get();
        }


        $allzip = DB::table('sys_area')
        ->get();

        $ziparea = array();
        foreach ($allzip as $zipkey => $ziprow) {
            $ziparea[$ziprow->dist_cd] = $ziprow->station_nm;
            # code...
        }
        $checkWarehouse = explode(',',$user->check_wh);
        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('cust_no');
        $newdcid = array();
        foreach ($userwh as $whkey => $value) {
            $whno = substr($value,0,2);
            Log::info($whno);
            if($whno == "A0" || $whno == "B0") {
                Log::info('類別 a0 b0');
                $newdcid[] = $value;
            } else {
                Log::info('類別 != a0 b0');
                $newdcid[] = $whno;
            }
        }
        $fromwh =  DB::table('mod_warehouse')
        ->select('*', 
            DB::raw("(select cname from mod_warehouse as b where cust_no =mod_warehouse.dc_id and dc_id IS NULL limit 1 ) as dc_name")
        )
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('dc_name');
        Log::info( $newdcid);
        foreach($owner as $ownerkey=>$ownerrow) {
            $totalstatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0,'UPLOADFINISH'=>0);
            $orderbyowner = DB::table('mod_order')
            ->where("dlv_date", $today)
            ->select('status', 'dlv_zip','id')
            ->whereIn('s_wh_no', $checkWarehouse)
            ->where('owner_cd', $ownerrow->cust_no)
            ->get();

            $north        = 0;
            $middle       = 0;
            $south        = 0;
            $east         = 0;
            $northStatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $middleStatus = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $southStatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $eastStatus   = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $tempids = array();
            foreach($orderbyowner as $orderkey=>$orderrow) {
                $status = $orderrow->status;
                $zip = $orderrow->dlv_zip;
                if(isset($ziparea[$zip])) {
                    if($ziparea[$zip] == "北") {
                        $north  ++;
                        $northStatus[$status]++;
                    } else if ($ziparea[$zip] == "中") {
                        $middle ++;
                        $middleStatus[$status]++;
                    } else if ($ziparea[$zip] == "南") {
                        $south ++;
                        $southStatus[$status]++;
                    } else if ($ziparea[$zip] == "東") {
                        $east  ++;
                        $eastStatus[$status]++;
                    }
                }
                $tempids[] = $orderrow->id;
                $totalstatus[$status] ++;
            }
            $ownerrow->ids = $tempids;
            $ownerrow->north         = $north;
            $ownerrow->north_status  = $northStatus;

            $ownerrow->middle        = $middle;
            $ownerrow->middle_status = $middleStatus;

            $ownerrow->south         = $south;
            $ownerrow->south_status  = $southStatus;

            $ownerrow->east          = $east;
            $ownerrow->east_status   = $eastStatus;

            $ownerrow->total_count    = count($orderbyowner);
            $ownerrow->total_status   = $totalstatus;
        }
        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardChartOwner')
        ->value('value1');
        
        $viewData = array(
            'refreshtime' => (string)($refreshtime * 1000),
            'owner'       => $owner,
            'readyleave'  => null,
        );
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $viewData]);
    }

    public function dashboardChartWh() {
        $user = Auth::user();
        //total_count
        $today = date("Y-m-d");
        ini_set('memory_limit', '512M');
        $checkWarehouse = explode(',',$user->check_wh);
        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->whereNotNull('dc_id')
        ->pluck('dc_id');
        $newdcid = array();
        foreach ($checkWarehouse as $checkkey => &$checkrow) {
            $whno = substr($checkrow,0,2);
            $newdcid[] = $whno;
            if($whno != "A0" && $whno != "B0") {
                $checkrow = $whno;
                Log::info( $checkrow);
            }
        }
        

        $userOwnerGroup = explode(',',$user->check_owner);
        $checkOwner = DB::table('mod_customer_relation')
        ->whereIn('group_cd', $userOwnerGroup)
        ->pluck('cust_no')->toArray();

        $managewarehouse = DB::table('mod_warehouse')
        ->select('*', 
            DB::raw("(select group_concat(cust_no) from mod_warehouse w where w.dc_id = mod_warehouse.cust_no) as underdcid")
        )
        ->whereIn("cust_no", $userwh)
        ->get();
        $custNos = array();
        //totalwarehousedata
        foreach ($checkWarehouse as $checkkey => &$checkrow) {
            $whno = substr($checkrow,0,2);
            $newdcid[] = $whno;
            if($whno != "A0" && $whno != "B0") {
                $custNos[] = $checkrow.'-1';
            } else {
                $custNos[] = $checkrow;
            }
        }
        $totalwarehousedata = array();
        if($user->check_owner != "*" ) {
            $totalwarehousedata = DB::table('mod_order')
            ->whereIn("owner_cd", $checkOwner)
            ->whereIn("dc_id", $custNos)
            ->where("dlv_date", $today)
            ->get();
            // $this_query->whereIn("owner_cd", $checkOwner);
        } else {
            $totalwarehousedata = DB::table('mod_order')
            ->whereIn("dc_id", $custNos)
            ->where("dlv_date", $today)
            ->get();
        }



        $totalarray = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0);
        foreach ($totalwarehousedata as $orderkey => $orderrow) {
            if($orderrow->status == "PICKEDFINISH") {
                $totalarray['PICKEDFINISH'] ++;
            } else if ($orderrow->status == "LOADING") {
                $totalarray['LOADING'] ++;
            } else if ($orderrow->status == "UPLOADFINISH") {
                $totalarray['PICKED'] ++;
            } else if ($orderrow->status == "OFFSTAFF") {
                $totalarray['OFFSTAFF'] ++;
            } else if ($orderrow->status == "FINISHED") {
                $totalarray['FINISHED'] ++;
            }
        }

        $totalwarehouse = count($totalwarehousedata);

        $north = DB::table('sys_area')
        ->where('station_nm', '北')
        ->pluck('dist_cd')->toArray();

        $middle = DB::table('sys_area')
        ->where('station_nm', '中')
        ->pluck('dist_cd')->toArray();

        $south = DB::table('sys_area')
        ->where('station_nm', '南')
        ->pluck('dist_cd')->toArray();

        $east = DB::table('sys_area')
        ->where('station_nm', '東')
        ->pluck('dist_cd')->toArray();

        if($user->check_owner != "*" ) {
            $orderbynorth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $north)
            ->whereIn("dc_id", $checkWarehouse)
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbymiddle = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $middle)
            ->whereIn("dc_id", $checkWarehouse)
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbysouth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $south)
            ->whereIn("dc_id", $checkWarehouse)
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbyeast = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $east)
            ->whereIn("dc_id", $checkWarehouse)
            ->whereIn("owner_cd", $checkOwner)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
        } else {
            $orderbynorth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $north)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbymiddle = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $middle)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbysouth = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $south)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
    
            $orderbyeast = DB::table('mod_order')
            ->select('status', DB::raw("count(status) as status_count"))
            ->whereIn('pick_zip', $east)
            ->whereIn("dc_id", $checkWarehouse)
            ->where("dlv_date", $today)
            ->groupBy('status')
            ->get();
        }


        foreach ($managewarehouse as $managekey => $managerow) {
            $dcid = [];
            if($managerow->cust_no == "A0" || $managerow->cust_no == "B0") {
                $dcid =  explode(',',$managerow->underdcid);
            } else {
                $dcid[] = $managerow->cust_no.'-1';

            }
            if($user->check_owner != "*" ) {
                $totalorder = DB::table('mod_order')
                ->where("dc_id", 'like', '%'.$managerow->cust_no.'%')
                ->whereIn("owner_cd", $checkOwner)
                ->where("dlv_date", $today)
                ->get();
            } else {
                $totalorder = DB::table('mod_order')
                ->where("dc_id", 'like', '%'.$managerow->cust_no.'%')
                ->where("dlv_date", $today)
                ->get();
            }



            $statuspickedfinish = 0;
            $statusloading = 0;
            $statuspicked = 0;
            $statusoffloading = 0;
            $statusfinished = 0;
            foreach ($totalorder as $orderkey => $orderrow) {
                if($orderrow->status == "PICKEDFINISH") {
                    $statuspickedfinish ++;
                } else if ($orderrow->status == "LOADING") {
                    $statusloading ++;
                } else if ($orderrow->status == "UPLOADFINISH") {
                    $statuspicked ++;
                } else if ($orderrow->status == "OFFSTAFF") {
                    $statusoffloading ++;
                } else if ($orderrow->status == "FINISHED") {
                    $statusfinished ++;
                }
            }
            $managerow->statuspickedfinish = $statuspickedfinish;
            $managerow->statusloading = $statusloading;
            $managerow->statuspicked = $statuspicked;
            $managerow->statusoffloading = $statusoffloading;
            $managerow->statusfinished = $statusfinished;

            $managerow->total_count = $statuspickedfinish+$statusloading+ $statuspicked+$statusoffloading+$statusfinished;
        }

        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardChartWh')
        ->value('value1');

        $checkWarehouse = explode(',',$user->check_wh);

        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('dc_id');

        $seewh =  DB::table('mod_warehouse')
        ->select('cust_no', 'cname')
        ->whereIn("cust_no", $userwh)
        ->get();
        // dd($totalwarehouse);
        $viewData = array(
            'refreshtime'        => (string)($refreshtime * 1000),
            'totalwarehousedata' => $totalarray,
            'managewarehouse'    => $managewarehouse,
            'totalwarehouse'     => $totalwarehouse,
            'north'              => $orderbynorth,
            'middle'             => $orderbymiddle,
            'south'              => $orderbysouth,
            'east'               => $orderbyeast,
            'wharray'            => $user->dashboard_wh,
            'see_wh'             => $seewh,
        );
    
        return view('dashboardChartWh')->with('viewData', $viewData);
                //code...

    }

    public function dashboardChartOwner() {
        $user = Auth::user();
        $userOwnerGroup = explode(',',$user->check_owner);

        $today = date("Y-m-d");
        $orderowner = DB::table('mod_order')
        ->groupBy('owner_cd')
        ->pluck('owner_cd')
        ->toArray();

        $owner = DB::table('sys_customers')
        ->select('cust_no',DB::raw("cname as cust_name"))
        ->whereIn('cust_no', $orderowner)
        ->where('type', 'other')
        ->get();

        if($user->check_owner != "*" ) {
            $checkOwner = DB::table('mod_customer_relation')
            ->whereIn('group_cd', $userOwnerGroup)
            ->pluck('cust_no')->toArray();
            $owner = DB::table('sys_customers')
            ->select('cust_no',DB::raw("cname as cust_name"))
            ->whereIn('cust_no', $orderowner)
            ->whereIn('cust_no', $checkOwner)
            ->where('type', 'other')
            ->get();
        }


        $allzip = DB::table('sys_area')
        ->get();

        $ziparea = array();
        foreach ($allzip as $zipkey => $ziprow) {
            $ziparea[$ziprow->dist_cd] = $ziprow->station_nm;
            # code...
        }
        $checkWarehouse = explode(',',$user->check_wh);
        $userwh =  DB::table('mod_warehouse')
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('cust_no');
        $fromwh =  DB::table('mod_warehouse')
        ->select('*', 
            DB::raw("(select cname from mod_warehouse as b where cust_no =mod_warehouse.dc_id and dc_id IS NULL limit 1 ) as dc_name")
        )
        ->whereIn("cust_no", $checkWarehouse)
        ->pluck('dc_name');
        //
        foreach($owner as $ownerkey=>$ownerrow) {
            $totalstatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0,'UPLOADFINISH'=>0);
            $orderbyowner = DB::table('mod_order')
            ->select('status', 'dlv_zip')
            ->whereIn('s_wh_no', $checkWarehouse)
            ->where('owner_cd', $ownerrow->cust_no)
            ->where("dlv_date", $today)
            ->get();

            $north        = 0;
            $middle       = 0;
            $south        = 0;
            $east         = 0;
            $northStatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $middleStatus = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $southStatus  = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            $eastStatus   = array('PICKEDFINISH'=>0,'LOADING'=>0,'PICKED'=>0,'OFFSTAFF'=>0,'FINISHED'=>0, 'UPLOADFINISH'=>0);
            foreach($orderbyowner as $orderkey=>$orderrow) {
                $status = $orderrow->status;
                $zip = $orderrow->dlv_zip;
                if(isset($ziparea[$zip])) {
                    if($ziparea[$zip] == "北") {
                        $north  ++;
                        $northStatus[$status]++;
                    } else if ($ziparea[$zip] == "中") {
                        $middle ++;
                        $middleStatus[$status]++;
                    } else if ($ziparea[$zip] == "南") {
                        $south ++;
                        $southStatus[$status]++;
                    } else if ($ziparea[$zip] == "東") {
                        $east  ++;
                        $eastStatus[$status]++;
                    }
                }
                $totalstatus[$status] ++;
            }

            $ownerrow->north         = $north;
            $ownerrow->north_status  = $northStatus;

            $ownerrow->middle        = $middle;
            $ownerrow->middle_status = $middleStatus;

            $ownerrow->south         = $south;
            $ownerrow->south_status  = $southStatus;

            $ownerrow->east          = $east;
            $ownerrow->east_status   = $eastStatus;

            $ownerrow->total_count    = count($orderbyowner);
            $ownerrow->total_status   = $totalstatus;
        }
        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardChartOwner')
        ->value('value1');
        
        $viewData = array(
            'refreshtime' => (string)($refreshtime * 1000),
            'owner'       => $owner,
            'readyleave'  => null,
            'ownerarray'  => $user->dashboard_owner,
        );

        return view('dashboardChartOwner')->with('viewData', $viewData);
    }
    public function dashboardTextbydate(Request $request) {
        $user = Auth::user();
        $today = $request->date;

        $usetempowner = explode(',',$user->dashboard_owner_bytest);
        $wharraystr   = $request->wharray;
        $wharray      = array();
        $forcustno    = array();
        $custfilter    = array();
        foreach ($wharraystr as $whkey => $whrow) {
            $dcid =  substr($whrow['cust_no'],0,2);
            $wharray[] = $dcid;
            if($dcid == "A0" || $dcid == "B0") {
                $forcustno[] = $whrow['cust_no'];
            } else {
                $forcustno[] = $dcid;
            }
            $custfilter[] = $whrow['cust_no'];
        }
        $owenerstr = $request->owenerarray;
        $owenerarray = array();
        foreach ($owenerstr as $ownerkey => $ownerrow) {
            $owenerarray[] = $ownerrow['cust_no'];
        }
        Log::info('dashboard user id');
        Log::info($user->id);
        Log::info(count( $custfilter) > 0 ? implode(',', $custfilter) : $user->dashboard_wh_bytest);
        DB::table('users')
        ->where('id',$user->id)
        ->update([
            'dashboard_owner_bytest' => implode(',', $owenerarray),
            'dashboard_wh_bytest'    => count( $custfilter) > 0 ? implode(',', $custfilter) : $user->dashboard_wh_bytest,
        ]);

        $userOwnerGroup = explode(',',$user->check_owner);
        $allwhouse = DB::table('mod_warehouse')
        ->whereNotNull('dc_id')
        ->whereIn('cust_no', explode(',',$user->check_wh))
        ->get();

        if($forcustno) {
            // select * from `mod_warehouse1` where `cust_no` in (C0-1) and `cust_no` in (C0)
            Log::info('warehouse1');
            $warehouse = DB::table('mod_warehouse')
            ->whereIn('cust_no', $custfilter)
            ->get();
        } else {
            Log::info('warehouse2');
            $warehouse = DB::table('mod_warehouse')
            ->whereIn('dc_id', explode(',',$user->check_wh))
            ->get();
        }
        $userdcid = explode(',',$user->check_wh);
        $dcidforsql = '';
        foreach ($userdcid as $dcidkey => $dcidrow) {
            if(!empty($dcidrow)) {
                $dcid =  substr($dcidrow,0,2);
                if($dcid == "A0" || $dcid == "B0") {
                    $dcidforsql .= "'".$dcidrow."',";
                } else {
                    $dcidforsql .= "'".$dcid."',";
                }
            }
        }
        $dcidforsql = substr( $dcidforsql,1 ,-1);
        Log::info('dashboardTextbydate test start');
        Log::info($dcidforsql);
        Log::info('dashboardTextbydate test end');

        if($user->check_owner != "*" ) {
            $checkOwner = DB::table('mod_customer_relation')
            ->whereIn('group_cd', $userOwnerGroup)
            ->pluck('cust_no')->toArray();
            
            if(count($owenerarray) > 0) {
                $owner = DB::table('sys_customers')
                ->whereIn('cust_no', $owenerarray)
                ->whereIn('cust_no', $checkOwner)
                ->where('type', 'other')
                ->pluck('cust_no')->toArray();
            } else {
                $owner = DB::table('sys_customers')
                ->whereIn('cust_no', $checkOwner)
                ->where('type', 'other')
                ->pluck('cust_no')->toArray();
            }

        } else {
            $owner = DB::table('sys_customers')
            ->select('cust_no',DB::raw("cname as cust_name"))
            ->where('type', 'other')
            ->pluck('cust_no')->toArray();
        }

        $checkOwner = DB::table('mod_customer_relation')
        ->whereIn('group_cd', $userOwnerGroup)
        ->pluck('cust_no')->toArray();
        foreach($warehouse as $warehousekey=>$warehouserow) {
            // if($warehouserow->dc_id != 'A0' && $warehouserow->dc_id !='B0' ) {
            //     $warehouserow->cust_no = $warehouserow->dc_id;
            // }
    
            if($user->check_owner != "*" ) {
                $ordnos = DB::table('mod_order')
                ->whereIn("owner_cd", $checkOwner)
                ->where('dlv_date', $today)
                ->where('s_wh_no', $warehouserow->cust_no)
                ->get();
            } else {
                $ordnos = DB::table('mod_order')
                ->where('dlv_date', $today)
                ->where('s_wh_no', $warehouserow->cust_no)
                ->get();
            }
            $ordnosArray = array();
            $finishcount = 0;
            foreach ($ordnos as $ordkey => $ordno) {
                $ordnosArray[] = $ordno->ord_no;
                if($ordno->status =="FINISHED") {
                    $finishcount++;
                }
                // if(!empty($ordno->exp_reason)) {
                //     $wherrorcount ++ ;
                // }
            }
            $errorcount = DB::table('mod_error_report')
            ->whereIn('wms_ord_no', $ordnosArray)
            ->groupBy('wms_ord_no')
            ->get();
            $errorcount = count($errorcount);
            $warehouserow->error_count = $errorcount>0 ?$errorcount :0;
            
            $warehouserow->total_count = count($ordnos);
            $finishparsent = 0;
            if(count($ordnosArray) > 0 ){
                $finishparsent =  empty($finishcount) ? 0 : round(($finishcount / count($ordnosArray) *100),1) ;
            }

            $warehouserow->finish_parsent  = $finishparsent ;
            $warehouserow->dlv_parsent  = round(100 - $finishparsent,1) ;
        }

        $ownerreturn  = array();

        $allzip = DB::table('sys_area')
        ->get();

        $whaddress = DB::table('mod_warehouse')
        ->select('address')
        ->groupBy('address')
        ->get();

        $whaddressArray = array();
        foreach ($whaddress as $whaddresskey => $whaddressrow) {
            $whaddressArray[$whaddressrow->address] =  $whaddressrow->address;
        }

        $ziparea = array();
        foreach ($allzip as $zipkey => $ziprow) {
            $ziparea[$ziprow->dist_cd] = $ziprow->station_nm;
            # code...
        }
        $allowner = DB::table('sys_customers')
        ->select(
            DB::raw("cust_no as cust_no"),
            DB::raw("cname as cust_name"),
            DB::raw("cust_no as owner_cd"),
            DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
            DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql})  and owner_cd = sys_customers.cust_no )  as finishcount")
        )
        ->where('type', 'other')
        ->whereIn('cust_no', $owner)
        ->get();

        if($user->check_owner != "*" ) {
            if(count($owenerarray) > 0) {
                $ownerordnos = DB::table('sys_customers')
                ->select(
                    DB::raw("cust_no as cust_no"),
                    DB::raw("cname as cust_name"),
                    DB::raw("cust_no as owner_cd"),
                    DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
                    DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql})  and owner_cd = sys_customers.cust_no ) as finishcount")
                )
                ->where('type', 'other')
                ->whereIn('cust_no', $owenerarray)
                ->get();
            } else {
                $ownerordnos = DB::table('sys_customers')
                ->select(
                    DB::raw("cust_no as cust_no"),
                    DB::raw("cname as cust_name"),
                    DB::raw("cust_no as owner_cd"),
                    DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
                    DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql})  and owner_cd = sys_customers.cust_no ) as finishcount")
                )
                ->where('type', 'other')
                ->whereIn('cust_no', $owner)
                ->get();
            }

        } else {
            $ownerordnos = DB::table('sys_customers')
            ->select(
                DB::raw("cust_no as cust_no"),
                DB::raw("cname as cust_name"),
                DB::raw("cust_no as owner_cd"),
                DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
                DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql}) and owner_cd = sys_customers.cust_no ) as finishcount")
            )
            ->where('type', 'other')
            ->whereIn('cust_no', $owenerarray)
            ->get();
        }
        $checkWarehouse = explode(',',$user->check_wh);
        $newdcid = array();
        foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
            $dcid =  substr($fordetailrow,0,2);
            if($dcid != "A0" && $dcid != 'B0') {
                $newdcid[] = substr($fordetailrow,0,2);
            } else {
                $newdcid[] = $fordetailrow;
            }
        }
        if($forcustno) {
            $orderbyzip = DB::table('mod_order')
            ->where('dlv_date', $today)
            ->whereIn('owner_cd', $owner)
            ->whereIn('s_wh_no', $checkWarehouse)
            ->get();
        } else {
            $orderbyzip = DB::table('mod_order')
            ->where('dlv_date', $today)
            ->whereIn('owner_cd', $owner)
            ->whereIn('s_wh_no', $checkWarehouse)
            ->get();
        }


        $orderzip = array();
        //
        foreach ($orderbyzip as $orderbyzipkey => $orderbyzipvalue) {
            if(isset( $orderzip[$orderbyzipvalue->owner_cd] ) ) {
            } else {
                $orderzip[$orderbyzipvalue->owner_cd] = array();
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumN'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumC'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumS'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumE'] = 0;
            }
            $zip = $orderbyzipvalue->dlv_zip;
            if(isset($ziparea[$zip])) {
                if($ziparea[$zip] == "北") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumN']  ++;
                } else if ($ziparea[$zip] == "中") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumC']  ++;
                } else if ($ziparea[$zip] == "南") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumS']  ++;
                } else if ($ziparea[$zip] == "東") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumE']  ++;
                }
            }
        }
        //

        //異常
        $errororder = DB::table('mod_order')
        ->where('dlv_date', $today)
        ->pluck('ord_no')
        ->toArray();

        $errorByorder = DB::table('mod_error_report')
        ->select('ord_no','owner_cd')
        ->whereIn('wms_ord_no', $errororder)
        ->groupBy('ord_no')
        ->get();
        
        $ownererror = array();
        foreach ($errorByorder as $errororderkey => $errororderrow) {
            if(isset( $ownererror[$errororderrow->owner_cd] ) ) {
                $ownererror[$errororderrow->owner_cd]++;
            } else {
                $ownererror[$errororderrow->owner_cd] = 1;
            }
        }

        //異常結束
        foreach ($ownerordnos as $orderkey => $ownerorder) {
            $total_count = $ownerorder->total_count;
            $finishcount = $ownerorder->finishcount;
            $finishparsent = 0;
            $finishparsent =  $finishcount == 0 ? 0 : round(($finishcount / $total_count *100),1) ;
            $ownerorder->finish_parsent  = $finishparsent ;
            $ownerorder->dlv_parsent  = round(100 - $finishparsent,1) ;

            $ownerorder->errorNumN = isset($orderzip[$ownerorder->owner_cd]['errorNumN']) ? $orderzip[$ownerorder->owner_cd]['errorNumN'] :0;
            $ownerorder->errorNumC = isset($orderzip[$ownerorder->owner_cd]['errorNumC']) ? $orderzip[$ownerorder->owner_cd]['errorNumC'] :0;
            $ownerorder->errorNumS = isset($orderzip[$ownerorder->owner_cd]['errorNumS']) ? $orderzip[$ownerorder->owner_cd]['errorNumS'] :0;
            $ownerorder->errorNumE = isset($orderzip[$ownerorder->owner_cd]['errorNumE']) ? $orderzip[$ownerorder->owner_cd]['errorNumE'] :0;

            $ownerorder->total_count = $ownerorder->errorNumN + $ownerorder->errorNumC + $ownerorder->errorNumS +$ownerorder->errorNumE ;
            $ownerorder->error_count = isset($ownererror[$ownerorder->owner_cd]) ? $ownererror[$ownerorder->owner_cd] : 0;
        }
        $ownerreturn =  $ownerordnos;
        
        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardText')
        ->value('value1');

        $viewData = array(
            'refreshtime' => (string)($refreshtime * 1000),
            'allwh'       => $allwhouse,
            'warehouse'   => $warehouse,
            'allowner'    => $allowner,
            'owner'       => $ownerreturn,
            'ownerarray'  => implode(',', $owenerarray),
            'wharray'     => implode(',', $wharray),
            'readyleave'  => null,
        );

        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $viewData]);
    }
    
    public function dashboardText() {
        $user = Auth::user();
        $today = date("Y-m-d");
        $allwhouse = DB::table('mod_warehouse')
        ->whereNotNull('dc_id')
        ->whereIn('cust_no', explode(',',$user->check_wh))
        ->get();
        $warehouse = DB::table('mod_warehouse')
        ->whereNotNull('dc_id')
        ->whereIn('cust_no', explode(',',$user->check_wh))
        ->whereIn('cust_no', explode(',',$user->dashboard_wh_bytest))
        ->get();

        $userOwnerGroup = explode(',',$user->check_owner);


        if($user->check_owner != "*" ) {
            $checkOwner = DB::table('mod_customer_relation')
            ->whereIn('group_cd', $userOwnerGroup)
            ->pluck('cust_no')->toArray();
            $owner = DB::table('sys_customers')
            ->whereIn('cust_no', $checkOwner)
            ->where('type', 'other')
            ->pluck('cust_no')->toArray();
        } else {
            $owner = DB::table('sys_customers')
            ->select('cust_no',DB::raw("cname as cust_name"))
            ->where('type', 'other')
            ->pluck('cust_no')->toArray();
        }

        $checkOwner = DB::table('mod_customer_relation')
        ->whereIn('group_cd', $userOwnerGroup)
        ->pluck('cust_no')->toArray();

        foreach($warehouse as $warehousekey=>$warehouserow) {
            // if($warehouserow->dc_id != 'A0' && $warehouserow->dc_id !='B0' ) {
            //     $warehouserow->cust_no = $warehouserow->dc_id;
            // }
    
            if($user->check_owner != "*" ) {
                $ordnos = DB::table('mod_order')
                ->whereIn("owner_cd", $checkOwner)
                ->where('dlv_date', $today)
                ->where('s_wh_no', $warehouserow->cust_no)
                ->get();
            } else {
                $ordnos = DB::table('mod_order')
                ->where('dlv_date', $today)
                ->where('s_wh_no', $warehouserow->cust_no)
                ->get();
            }
            $ordnosArray = array();
            $finishcount = 0;

            foreach ($ordnos as $ordkey => $ordno) {
                $ordnosArray[] = $ordno->ord_no;
                if($ordno->status =="FINISHED") {
                    $finishcount++;
                }
            }
            $errorcount = DB::table('mod_error_report')
            ->whereIn('wms_ord_no', $ordnosArray)
            ->groupBy('wms_ord_no')
            ->get();
            
            $errorcount = count($errorcount);
            $warehouserow->error_count = $errorcount>0 ?$errorcount :0;
            
            $warehouserow->total_count = count($ordnos);
            $finishparsent = 0;
            if(count($ordnosArray) > 0 ){
                $finishparsent =  empty($finishcount) ? 0 : round(($finishcount / count($ordnosArray) *100),1) ;
            }

            $warehouserow->finish_parsent  = $finishparsent ;
            $warehouserow->dlv_parsent  = round(100 - $finishparsent,1) ;
        }

        $ownerreturn  = array();
        $allzip = DB::table('sys_area')
        ->get();

        $whaddress = DB::table('mod_warehouse')
        ->select('address')
        ->groupBy('address')
        ->get();

        $whaddressArray = array();
        foreach ($whaddress as $whaddresskey => $whaddressrow) {
            $whaddressArray[$whaddressrow->address] =  $whaddressrow->address;
        }

        $ziparea = array();
        foreach ($allzip as $zipkey => $ziprow) {
            $ziparea[$ziprow->dist_cd] = $ziprow->station_nm;
            # code...
        }

        $allowner = DB::table('sys_customers')
        ->select(
            DB::raw("cust_no as cust_no"),
            DB::raw("cname as cust_name"),
            DB::raw("cust_no as owner_cd"),
            DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no ) as total_count"),
            DB::raw("(select count(*) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no ) as total_count"),
            DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no ) as finishcount")
        )
        ->where('type', 'other')
        ->whereIn('cust_no', $owner)
        ->get();

        $userdcid = explode(',',$user->check_wh);
        $dcidforsql = '';
        foreach ($userdcid as $dcidkey => $dcidrow) {
            if(!empty($dcidrow)) {
                $dcid =  substr($dcidrow,0,2);
                if($dcid == "A0" || $dcid == "B0") {
                    $dcidforsql .= "'".$dcidrow."',";
                } else {
                    $dcidforsql .= "'".$dcid."',";
                }
            }
        }
        $dcidforsql = substr( $dcidforsql,1 ,-1);
        if($user->check_owner != "*" ) {
            $ownerordnos = DB::table('sys_customers')
            ->select(
                DB::raw("cust_no as cust_no"),
                DB::raw("cname as cust_name"),
                DB::raw("cust_no as owner_cd"),
                DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
                DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql}) and owner_cd = sys_customers.cust_no ) as finishcount")
            )
            ->where('type', 'other')
            ->whereIn('cust_no', explode(',', $user->dashboard_owner_bytest) )
            ->get();
        } else {
            $ownerordnos = DB::table('sys_customers')
            ->select(
                DB::raw("cust_no as cust_no"),
                DB::raw("cname as cust_name"),
                DB::raw("cust_no as owner_cd"),
                DB::raw("(select count(owner_cd) from mod_order where dlv_date ='{$today}' and owner_cd = sys_customers.cust_no and s_wh_no in('{$dcidforsql}) )  as total_count"),
                DB::raw("(select count(if(status='FINISHED',true,null)) from mod_order where dlv_date ='{$today}' and s_wh_no in('{$dcidforsql}) and owner_cd = sys_customers.cust_no ) as finishcount")
            )
            ->where('type', 'other')
            ->whereIn('cust_no', explode(',', $user->dashboard_owner_bytest) )
            ->get();
        }

        // $orderbyzip = DB::table('mod_order')
        // ->where('dlv_date', $today)
        // ->whereIn('owner_cd', $owner)
        // ->whereIn('dc_id', explode(',',$user->check_wh))
        // ->whereIn('dc_id', explode(',',$user->dashboard_wh_bytest))
        // ->get();
        $checkWarehouse = explode(',',$user->dashboard_wh_bytest);
        $newdcid = array();
        foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
            $dcid =  substr($fordetailrow,0,2);
            if($dcid != "A0" && $dcid != 'B0') {
                $newdcid[] = substr($fordetailrow,0,2);
            } else {
                $newdcid[] = $fordetailrow;
            }
        }
        $orderbyzip = DB::table('mod_order')
        ->where('dlv_date', $today)
        ->whereIn('owner_cd', $owner)
        ->whereIn('s_wh_no', $newdcid)
        ->get();

        $orderzip = array();
        //
        foreach ($orderbyzip as $orderbyzipkey => $orderbyzipvalue) {
            if(isset( $orderzip[$orderbyzipvalue->owner_cd] ) ) {
            } else {
                $orderzip[$orderbyzipvalue->owner_cd] = array();
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumN'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumC'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumS'] = 0;
                $orderzip[$orderbyzipvalue->owner_cd]['errorNumE'] = 0;
            }
            $zip = $orderbyzipvalue->dlv_zip;
            if(isset($ziparea[$zip])) {
                if($ziparea[$zip] == "北") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumN']  ++;
                } else if ($ziparea[$zip] == "中") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumC']  ++;
                } else if ($ziparea[$zip] == "南") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumS']  ++;
                } else if ($ziparea[$zip] == "東") {
                    $orderzip[$orderbyzipvalue->owner_cd]['errorNumE']  ++;
                }
            }
        }
        //

        //異常
        $errororder = DB::table('mod_order')
        ->where('dlv_date', $today)
        ->whereIn('owner_cd', $owner)
        ->whereIn('dc_id', $newdcid)
        ->pluck('ord_no')
        ->toArray();

        $errorByorder = DB::table('mod_error_report')
        ->select('ord_no','owner_cd')
        ->whereIn('wms_ord_no', $errororder)
        ->groupBy('ord_no')
        ->get();
        
        $ownererror = array();
        foreach ($errorByorder as $errororderkey => $errororderrow) {
            if(isset( $ownererror[$errororderrow->owner_cd] ) ) {
                $ownererror[$errororderrow->owner_cd]++;
            } else {
                $ownererror[$errororderrow->owner_cd] = 1;
            }
        }
        $initarray = array();
        $temparray = array();
        $userowner = explode(',', $user->dashboard_owner);
        foreach ($userowner as $testkey => $testrow) {
            # code...
            $initarray[] = $testrow;
        }
        
        //異常結束
        foreach ($ownerordnos as $orderkey => $ownerorder) {
            $total_count = $ownerorder->total_count;
            $finishcount = $ownerorder->finishcount;
            $finishparsent = 0;
            $finishparsent =  $finishcount == 0 || $total_count == 0 ? 0 : round(($finishcount / $total_count *100),1) ;
            $ownerorder->finish_parsent  = $finishparsent ;
            $ownerorder->dlv_parsent  = round(100 - $finishparsent,1) ;

            $ownerorder->errorNumN = isset($orderzip[$ownerorder->owner_cd]['errorNumN']) ? $orderzip[$ownerorder->owner_cd]['errorNumN'] :0;
            $ownerorder->errorNumC = isset($orderzip[$ownerorder->owner_cd]['errorNumC']) ? $orderzip[$ownerorder->owner_cd]['errorNumC'] :0;
            $ownerorder->errorNumS = isset($orderzip[$ownerorder->owner_cd]['errorNumS']) ? $orderzip[$ownerorder->owner_cd]['errorNumS'] :0;
            $ownerorder->errorNumE = isset($orderzip[$ownerorder->owner_cd]['errorNumE']) ? $orderzip[$ownerorder->owner_cd]['errorNumE'] :0;
            $ownerorder->error_count = isset($ownererror[$ownerorder->owner_cd]) ? $ownererror[$ownerorder->owner_cd] : 0;
        }
        $ownerreturn =  $ownerordnos;
        
        $refreshtime = DB::table('bscode')
        ->where('cd_type', 'DASHBOARDTIME')
        ->where("cd", 'dashboardText')
        ->value('value1');

        $viewData = array(
            'refreshtime' => (string)($refreshtime * 1000),
            'allwh'       => $allwhouse,
            'warehouse'   => $warehouse,
            'allowner'    => $allowner,
            'owner'       => $ownerreturn,
            'ownerarray'  => $user->dashboard_owner_bytest,
            'wharray'     => $user->dashboard_wh_bytest,
            'readyleave'  => null,
        );

        return view('dashboardText')->with('viewData', $viewData);
    }


    public function getAmtReportByMonth(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $label = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        $result = DB::table('mod_order')
                    ->selectRaw('MONTH(created_at) as `month`, SUM(CASE WHEN cust_amt is null THEN amt ELSE cust_amt END) amt')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();

        $total = 0;
        foreach($result as $key=>$row) {

            $data[$row->month - 1] = $row->amt;           
            $total += $row->amt;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;

        return response()->json($returnData);
    }

    public function getDriver(Request $request) {
        $user = Auth::user();
        $newresult = DB::table('users') 
                    ->select('email', 'final_refresh',DB::raw('if(final_refresh>gps_time,final_refresh,gps_time) as gps_time'),
                    DB::raw('case  when  if(final_refresh>gps_time,final_refresh,gps_time) > \''.Carbon::now()->subMinutes(5).'\' then "Y" else "N"end as status'))
                    ->whereNotNull('final_refresh')
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->orderBy('status','desc')
                    ->get();
        $result = DB::table('users')
                    ->whereBetween(DB::raw('if(final_refresh>gps_time,final_refresh,gps_time)'), array(Carbon::now()->subMinutes(5), Carbon::now()))
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->get();
        $offresult = DB::table('users')
                    ->whereNotBetween(DB::raw('if(final_refresh>gps_time,final_refresh,gps_time)'), array(Carbon::now()->subMinutes(5), Carbon::now()))
                    ->where('c_key', $user->c_key)
                    ->where('g_key', $user->g_key)
                    ->get();
        $returnData = array();
        $returnData['online'] = $result;
        $returnData['offline'] = $offresult;
        $returnData['test'] = $newresult;
        return response()->json($returnData);
    }

    public function getAmtReportByDay(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $month = $request->month;
        $label = array();
        $data = array();

        $result = DB::table('mod_order')
                    ->selectRaw('DAY(created_at) as `day`, SUM(CASE WHEN cust_amt is null THEN amt ELSE cust_amt END) amt')
                    ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("DAY(created_at)"))
                    ->get();

        $total = 0;
        foreach($result as $key=>$row) {
            array_push($label, $row->day);
            array_push($data, $row->amt);

            $total += $row->amt;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;


        return response()->json($returnData);
    }

    public function getOrderReportByMonth(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $label = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
        $data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        $closeresult = DB::table('mod_order_close')
                    ->selectRaw('MONTH(created_at) as `month`, COUNT(*) as order_number')
                    ->where('status', '!=', 'FAIL')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();

        $result = DB::table('mod_order')
                    ->selectRaw('MONTH(created_at) as `month`, COUNT(*) as order_number')
                    ->where('status', '!=', 'FAIL')
                    ->whereYear('created_at', '=', $year)
                    ->where('c_key', $user->c_key)
                    ->orderBy('created_at')
                    ->groupBy(DB::raw("MONTH(created_at)"))
                    ->get();
        $total = 0;
        foreach($result as $key=>$row) {

            $data[$row->month - 1] = $row->order_number;           
            $total += $row->order_number;
        }
        foreach($closeresult as $key=>$row) {

            $data[$row->month - 1] += $row->order_number;           
            $total += $row->order_number;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;

        return response()->json($returnData);
    }

    public function getOrderReportByDay(Request $request) {
        $user = Auth::user();
        $year = $request->year;
        $month = $request->month;
        $label = array();
        $data = array();

        // $closeresult = DB::table('mod_order_close')
        //             ->selectRaw('DAY(created_at) as `day`, COUNT(*) order_number')
        //             ->where('status', '!=', 'FAIL')
        //             ->where('c_key', $user->c_key)
        //             ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
        //             ->orderBy('created_at')
        //             ->groupBy(DB::raw("DAY(created_at)"))
        //             ->get();

        // $orgresult = DB::table('mod_order')
        //             ->selectRaw('DAY(created_at) as `day`, COUNT(*) order_number')
        //             ->where('status', '!=', 'FAIL')
        //             ->where('c_key', $user->c_key)
        //             ->whereRaw('YEAR(created_at) = ? and MONTH(created_at) = ?', [$year, $month])
        //             ->orderBy('created_at')
        //             ->groupBy(DB::raw("DAY(created_at)"))
        //             ->get();

        $result = DB::select("SELECT a.day, 
                    Sum(order_number) AS order_number 
            FROM   (SELECT Day(created_at) AS `day`, 
                            Count(*)        order_number 
                    FROM   mod_order 
                    WHERE  status != 'fail' 
                            AND c_key = 'syl' 
                            AND Year(created_at) = $year 
                            AND Month(created_at) = $month 
                    GROUP  BY Day(created_at) 
                    UNION
                    
            (SELECT Day(created_at) AS `day`, 
                    Count(*)        order_number 
            FROM   mod_order_close 
            WHERE  status != 'fail' 
                    AND c_key = 'syl' 
                    AND Year(created_at) = $year 
                    AND Month(created_at) = $month 
            GROUP  BY Day(created_at) 
            ORDER  BY created_at ASC)) a 
            
            GROUP  BY a.day ");


        $total = 0;
        foreach($result as $key=>$row) {
            array_push($label, $row->day);
            array_push($data, $row->order_number);

            $total += $row->order_number;
        }

        $returnData = array();
        $returnData['label'] = $label;
        $returnData['data'] = $data;
        $returnData['total'] = $total;


        return response()->json($returnData);
    }
}
