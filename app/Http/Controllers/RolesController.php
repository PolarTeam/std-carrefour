<?php

namespace App\Http\Controllers;

use Exception;
use App\User;
use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\RolesService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\LogActivity;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RolesController extends Controller
{
    protected $rolesService;

    public function __construct(RolesService $rolesService) {
        $this->rolesService = $rolesService;
    }

    public function index() {
        $user = Auth::user();
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='roles' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));

            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('roles.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        =>  trans('roles.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }

        $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='roles' AND user_id={$user->id} LIMIT 1";
        $gridLayout = DB::select($sql);

        $columns = array();
        if(!$gridLayout) {
            //預設layout
            $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='roles' AND user_id = '29' LIMIT 1";
            $gridLayout = DB::select($sql);

            //連預設都沒有的時候用產生的
            if(!$gridLayout) {
                $columns = json_encode($cmptycolumns);
            } else {
                $columns = $gridLayout[0]->layout_content;
            }

        }

        $lang = app()->getLocale();
        return view('roles.index', array(
            'title'      => trans('roles.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));
    }

    public function edit($lang,$id) {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('roles.titleName');
        $this->data['entry'] = array();
        $this->data['id']    = $id;
        $this->data['lang']  = $lang;

        $this->data['web']   =  $this->rolesService->getwebPermission();
        $this->data['app']   =  $this->rolesService->getappPermission();

        return view('roles.edit', $this->data);
    }

    public function create() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('roles.titleName');
        $this->data['entry'] = array();
        $this->data['id']    = null;
        $this->data['web']   =  $this->rolesService->getwebPermission();
        $this->data['app']   =  $this->rolesService->getappPermission();
        $this->data['lang']     = app()->getLocale();
        return view('roles.edit', $this->data);
    }

    public function show($lang,$id) {
        try {
            $rolesdata = $this->rolesService->get($id);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        $filed = $this->rolesService->getfiled('roles');
        $web = $this->rolesService->getwebPermission()->toArray();
        $app = $this->rolesService->getappPermission()->toArray();

        $permission = array_merge($web,$app);
        
        foreach ($permission as $key => $row) {
            $filed[] = array( 'field_type'=> 'checkbox', 'field_name'=>$row->name);
        }
        
        return response()->json([
            'success'           => true,
            'message'           => "get role success",
            'data'              => $rolesdata,
            'filed'             => $filed,
            'enabledPermission' => $this->rolesService->getRolePermission($id),
        ]);
    }


    public function store (Request $request) {

        $result = array();
        try {
            $result = $this->rolesService->create($request);

            $permissionData = array();
            $role = null;
    
            unset($request['created_at']);
            unset($request['updated_at']);
            unset($request['created_by']);
            unset($request['updated_by']);
            foreach ($request->all() as $key => $row) {
                if ($key != 'id' && $key !='display_name' && $key !='_token' && $key !='name' &&  $key != "_method" ) {
                    $permissionData[$key] = $row;
                }
            }
            $roledata = $this->rolesService->get($result['id']);
            $role = Role::findByName($roledata['name'], 'api' );
            if(count($permissionData) > 0) {
                foreach($permissionData as $key => $per) {
                    if($per == 'on') {
                        $role->givePermissionTo($key);
                    }
                    else {
                        $role->revokePermissionTo($key);
                    }
                }
            }
        } catch (Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('新增roles');
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'lastId' => $result['id']
        ]);
    }

    public function destroy (Request $request, $lang, $id) {
        try {
            $this->rolesService->delete($id);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除roles');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function batchDelete (Request $request) {

        DB::beginTransaction();

        try {
            $this->rolesService->batchDelete($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除roles');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function exportdata(Request $request) {

        $downloadLink = '';

        try {
            $downloadLink = $this->rolesService->export($request);
        }
        catch(\Exception $e) {
            Log::error($e);
            return response()->json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }

        if(empty($downloadLink)) {
            return response()->json(array(
                'success' => false,
                'message' => '下載錯誤'
            ));
        }

        return response()->json(array(
            'success' => true,
            'donwloadLink' => $downloadLink,
            'message' => 'success'
        ));
    }


    public function get($roleName) {
        $role = null;
        $permission = null;
        
        if ($roleName != 'null') {
            $role = Role::findByName($roleName);
            $permission = Permission::where('ps_type', 'menulist')->orderBy('seq', 'asc')->get();

            $hasPermission = array();

            foreach($role->permissions as $row) {
                array_push($hasPermission, $row['name']);
            }
            foreach($permission as $menurow) {
                $menupermission = Permission::where('ps_type', 'menu')->where('rootItem', $menurow->name)->orderBy('seq', 'asc')->get();

                foreach($menupermission as $row) {
                    $actionPermissions = Permission::select('name', 'rootItem','display_name')->where('ps_type', 'action')->where('rootItem', $row->name)->orderBy('seq', 'asc')->get();
                    foreach($actionPermissions as $actionPermission) {
                        if(in_array($actionPermission['name'], $hasPermission)) {
                            $actionPermission['checked'] = true;
                        }
                        else {
                            $actionPermission['checked'] = false;
                        }
                    }
                    $row['Permission'] = $actionPermissions;
                    if(in_array($row['name'], $hasPermission)) {
                        $row['checked'] = true;
                    }
                    else {
                        $row['checked'] = false;
                    }
                }
                $menurow['Permission'] = $menupermission;
            }
            
        }
        else {
            $permission = Permission::orderBy('name', 'asc')->get();
            foreach($permission as $row) {
                $row['checked'] = false;
            }
        }

        
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'data' => array(
                'roleData' => $role,
                'permissionData' => $permission
            )
        ]);
    }
    
    public function update (Request $request, $lang, $id) {
        $result     = array();
        $permissionData = array();
        $role = null;
        try {
            unset($request['created_at']);
            unset($request['updated_at']);
            unset($request['updated_by']);
            unset($request['created_by']);
            $result = $this->rolesService->update($request, $id);
            foreach ($request->all() as $key => $row) {
                # code...
                if ($key != 'id' && $key !='display_name' && $key !='_token' && $key !='name'  &&  $key != "_method" ) {
                    $permissionData[$key] = $row;
                }
            }
            $roledata = $this->rolesService->get($id);
            // dd($permissionData);
            if(count($permissionData) > 0) {
                foreach($permissionData as $key => $per) {
                    try {
                        $role = Role::findByName($roledata['name'], 'api' );
                        if($per == 'on') {
                            $role->givePermissionTo($key);
                        }
                        else {
                            $role->revokePermissionTo($key);
                        }
                    } catch (\Throwable $th) {
                        //throw $th;
                    }
                }
            }

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'line' => $e->getLine(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('修改role');
        return response()->json([
            'success'          => true,
            'message'          => '修改成功',
            'data'             => $this->rolesService->get($id),
            'filed'            => $this->rolesService->getfiled('roles'),
        ]);
    }
    // public function update(Request $request, $roleName) {
    //     $role = null;
    //     $permission = null;

    //     $requestAll = $request->all();

    //     $roleData = $requestAll['roleData'];
    //     $permissionData = $requestAll['permissionData'];

    //     DB::beginTransaction();
    //     try {
    //         $role = Role::where('name', $roleName)->update($roleData);

    //         if(count($permissionData) > 0) {
    //             $role = Role::findByName($roleName);
    //             foreach($permissionData as $per) {
    //                 //$permission = Permission::create($per);
    //                 if($per['checked'] == true) {
    //                     $role->givePermissionTo($per['name']);
    //                 }
    //                 else {
    //                     $role->revokePermissionTo($per['name']);
    //                 }
    //             }
    //         }

    //     }
    //     catch (\Exception $e) {
    //         DB::rollback();
    //         Log::error($e);
    //         return response()->json([
    //             'success' => false,
    //             'message' => $e->getMessage()
    //         ]);
    //     }

    //     DB::commit();
    //     return response()->json([
    //         'success' => true,
    //         'message' => '修改成功',
    //         'data' => array(
    //             'roleData' => $role
    //         )
    //     ]);
    // }


    public function assignPermissionByRole(Request $request) {
        $role = null;
        $permission = null;

        $requestAll = $request->all();

        $roleName = $requestAll['roleName'];
        $permissionData = $requestAll['permissionData'];

        DB::beginTransaction();
        try {

            $role = Role::findByName($roleName);

            if(count($permissionData) > 0) {
                foreach($permissionData as $per) {
                    $role->givePermissionTo($per['name']);
                }
            }

        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }

        DB::commit();
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'data' => array(
                'roleData' => $role
            )
        ]);
    }

    public function assignRoleByUser(Request $request) {
        $role = null;

        $requestAll = $request->all();

        $userData = $requestAll['userData'];
        $roleName = $requestAll['roleName'];

        DB::beginTransaction();
        try {

            foreach($userData as $userId) {
                $user = User::find($userId);

                $user->assignRole($roleName);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }

        DB::commit();
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'data' => User::role($roleName)->get()
        ]);
    }

    public function removeRoleByUser(Request $request) {
        $role = null;

        $requestAll = $request->all();

        $userData = $requestAll['userData'];
        $roleName = $requestAll['roleName'];

        DB::beginTransaction();
        try {

            foreach($userData as $userId) {
                $user = User::find($userId);

                $user->removeRole($roleName);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }

        DB::commit();
        return response()->json([
            'success' => true,
            'message' => '移除成功',
            'data' => User::role($roleName)->get()
        ]);
    }

    public function revokePermissionByRole(Request $request) {
        $role = null;

        $requestAll = $request->all();

        $permissionData = $requestAll['permissionData'];
        $roleName = $requestAll['roleName'];

        DB::beginTransaction();
        try {

            foreach($permissionData as $per) {
                $role = Role::findByName($roleName);

                $role->revokePermissionTo($per['name']);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }

        DB::commit();
        return response()->json([
            'success' => true,
            'message' => '移除成功',
            'data' => $role
        ]);
    }

    public function query (Request $request) {

        $baseModel = new BaseModel();

        $user = Auth::user();
        
        $baseCondition = $request->baseCondition;
        
        // $baseCondition[] = ['company_id', '=', $user->company_id];

        $result = $baseModel->baseQuery('roles', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $request->sort);

        return response()->json([
            'success' => $result['success'],
            'totalCount' => $result['totalCount'],
            'message' => $result['message'],
            'data' => $result['data'],
        ]);
    }

    public function destory($id) {
        Role::where('id', $id)->delete();
        return response()->json([
            'success' => true,
            'message' => '移除成功'
        ]);
    }

    public function getAllpermission() {
        $permission = Permission::where('ps_type', 'menulist')->orderBy('seq', 'asc')->get();

        foreach($permission as $menurow) {

            $menupermission = Permission::where('ps_type', 'menu')->where('rootItem', $menurow->name)->orderBy('seq', 'asc')->get();

            foreach($menupermission as $row) {
                $actionPermissions = Permission::select('name', 'rootItem','display_name')->where('ps_type', 'action')->where('rootItem', $row->name)->orderBy('seq', 'asc')->get();
                foreach($actionPermissions as $actionPermission) {
                    $actionPermission['checked'] = false;
                }
                $row['Permission'] = $actionPermissions;
            }
            $menurow['Permission'] = $menupermission;
        }
        
        foreach($permission as $row) {
            $row['checked'] = false;
        }

        return response()->json([
            'success' => true,
            'message' => 'success',
            'data'    => $permission
        ]);
    }

}
