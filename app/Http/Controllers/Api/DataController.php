<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Carbon\Carbon;
use App\User;
use App\BaseMessage;
use App\Models\OrderMgmtModel;
use App\Models\BaseModel;
use App\Models\SysNoticeModel;
use App\Models\Map\Shape;
use App\Models\Map\ShapePoint;
use App\Services\WharfReserveService;
use Validator;
use App\Message;
use App\Fcm;
use App\Models\DlvModel;
use App\Models\FcmModel;
use App\Models\TrackingModel;
use App\Helpers\FcmHelper;
// use App\Models\ModTrackingModel;
class DataController extends Controller
{    
    protected $wharfReserveService;
    public function __construct(WharfReserveService $wharfReserveService) {
        $this->wharfReserveService = $wharfReserveService;
    }


    public function guid(){
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
            .substr($charid, 8, 4)
            .substr($charid,12, 4)
            .substr($charid,16, 4)
            .substr($charid,20,12);
        return $uuid;
    }


    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        try{
            $size_in_bytes = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);
            $size_in_kb    = $size_in_bytes / 1024;
    
            return $size_in_bytes;
        }
        catch(\Exception $e){
            return $e;
        }
    }


    public function menupersion () {
        $user = Auth::user();
        $data = array();
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        $driverdeliveryapp = 'N';
        $markapp           = 'N';
        $palletmanageapp   = 'N';
        $announceapp       = 'N';
        $scanqrcode        = 'N';
        $showqrcode        = 'N';
        
        $markreserve       = 'N';
        $markreservesearch = 'N';
        $markrestatus      = 'N';
        $assigncheckincar  = 'N';
        $importdischarged  = "N";
        $handover  = "N";
        
        try{
            if($user->hasPermissionTo('DRIVERDELIVERYAPP'))
            {
                $driverdeliveryapp="Y";
            }else{
                $driverdeliveryapp="N";
            }
        }
        catch(\Exception $e) {
            $driverdeliveryapp="N";
        }
        array_push($data, [
            'function' => "司機取送貨",
            'code'     => "DRIVERDELIVERYAPP",
            'enabled'  => $driverdeliveryapp
        ]);

        try{
            if($user->hasPermissionTo('MARKAPP'))
            {
                $markapp="Y";
            }else{
                $markapp="N";
            }
        }
        catch(\Exception $e) {
            $markapp="N";
        }
        array_push($data, [
            'function' => "碼頭管理",
            'code'     => "MARKAPP",
            'enabled'  => $markapp
        ]);
        
        try{
            if($user->hasPermissionTo('PALLETMANAGEAPP'))
            {
                $palletmanageapp="Y";
            }else{
                $palletmanageapp="N";
            }
        }
        catch(\Exception $e) {
            $palletmanageapp="N";
        }
        array_push($data, [
            'function' => "棧板管理",
            'code'     => "PALLETMANAGEAPP",
            'enabled'  => $palletmanageapp
        ]);
       
        try{
            if($user->hasPermissionTo('ANNOUNCEAPP'))
            {
                $announceapp="Y";
            }else{
                $announceapp="N";
            }
        }
        catch(\Exception $e) {
            $announceapp="N";
        }
        array_push($data, [
            'function' => "公告",
            'code'     => "ANNOUNCEAPP",
            'enabled'  => $announceapp
        ]);
     
        try{
            if($user->hasPermissionTo('SCANQRCODE'))
            {
                $scanqrcode="Y";
            }else{
                $scanqrcode="N";
            }
        }
        catch(\Exception $e) {
            $scanqrcode="N";
        }
        array_push($data, [
            'function' => "掃描 QR Code",
            'code'     => "SCANQRCODE",
            'enabled'  => $scanqrcode
        ]);
        try{
            if($user->hasPermissionTo('SHOWQRCODE'))
            {
                $showqrcode="Y";
            }else{
                $showqrcode="N";
            }
        }
        catch(\Exception $e) {
            $showqrcode="N";
        }

        array_push($data, [
            'function' => "Qrcode顯示",
            'code'     => "SHOWQRCODE",
            'enabled'  => $showqrcode
        ]);

        try{
            if($user->hasPermissionTo('MARKRESERVE'))
            {
                $markreserve="Y";
            }else{
                $markreserve="N";
            }
        }
        catch(\Exception $e) {
            $markreserve="N";
        }

        array_push($data, [
            'function' => "進倉碼頭預約",
            'code'     => "MARKRESERVE",
            'enabled'  => $markreserve
        ]);

        try{
            if($user->hasPermissionTo('MARKRESERVESEARCH'))
            {
                $markreservesearch="Y";
            }else{
                $markreservesearch="N";
            }
        }
        catch(\Exception $e) {
            $markreservesearch="N";
        }

        array_push($data, [
            'function' => "碼頭預約查詢",
            'code'     => "MARKRESERVESEARCH",
            'enabled'  => $markreservesearch
        ]);
  

        try{
            if($user->hasPermissionTo('MARKRESTATUS'))
            {
                $markrestatus="Y";
            }else{
                $markrestatus="N";
            }
        }
        catch(\Exception $e) {
            $markrestatus="N";
        }

        array_push($data, [
            'function' => "碼頭使用狀態",
            'code'     => "MARKRESTATUS",
            'enabled'  => $markrestatus
        ]);

        try{
            if($user->hasPermissionTo('ASSIGNCHECKINCAR'))
            {
                $assigncheckincar="Y";
            }else{
                $assigncheckincar="N";
            }
        }
        catch(\Exception $e) {
            $assigncheckincar="N";
        }

        array_push($data, [
            'function' => "分派報到車輛",
            'code'     => "ASSIGNCHECKINCAR",
            'enabled'  => $assigncheckincar
        ]);

        try{
            if($user->hasPermissionTo('IMPORTDISCHARGED'))
            {
                $importdischarged="Y";
            }else{
                $importdischarged="N";
            }
        }
        catch(\Exception $e) {
            $importdischarged="N";
        }

        array_push($data, [
            'function' => "進貨確認/放行",
            'code'     => "IMPORTDISCHARGED",
            'enabled'  => $importdischarged
        ]);

        try{
            if($user->hasPermissionTo('HANDOVER'))
            {
                $handover="Y";
            }else{
                $handover="N";
            }
        }
        catch(\Exception $e) {
            $handover="N";
        }

        array_push($data, [
            'function' => "接單作業",
            'code'     => "HANDOVER",
            'enabled'  => $handover
        ]);
        
        return response()->json([
            'msg' => 'success',
            'code' => '00',
            'data' => $data,
        ]);
    }


    public function checkopenApiCoulumnType($maindata) {
        $errormsg = "";

         //YN 系列
        $ynType  = ['is_urgent', 'owner_send_mail', 'pick_send_mail', 'dlv_send_mail'];
        $intType = ['collectamt', 'pkg_num', 'realnum', 'total_amount'];

        $stringType70 = ['truck_cmp_nm', 'owner_nm', 'pick_cust_nm', 'dlv_cust_nm'];
        $stringType20 = ['truck_cmp_no', 'owner_cd', 'car_no', 'pick_cust_no', 'dlv_cust_no', 'cust_ord_no' ];
        $stringType30 = ['pick_attn', 'driver', 'pick_tel', 'pick_tel2', 'dlv_attn', 'dlv_tel', 'dlv_tel2',  ];
        $stringType10 = ['pick_zip', 'dlv_zip' ];

        $stringType100 = ['pick_email', 'dlv_email'];
        $stringType200 = ['wh_addr', 'pick_addr', 'dlv_addr'  ];
        $stringType500 = ['wms_order_no'];
        
        $format = 'Y-m-d';
        $dateresult =  date($format, strtotime($maindata['etd'])) == $maindata['etd'];
        if(!$dateresult) {
            $errormsg .= "訂單號".$maindata['ord_no'].': '."etd 有問題 請檢查";
        }

        // dd($maindata);
        foreach ($maindata as $key => $value) {

            //YN 系列
            if (in_array($key, $ynType)) {
                if($value != 'N' && $value !='Y') {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 有問題 請檢查";
                }
            }
            //YN 系列 end

            //int 系列
            if (in_array($key, $intType)) {
                if(!is_numeric($value)) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 有問題 請檢查";
                }
            }
            //int 系列 end

            //字串
            if (in_array($key, $stringType10)) {
                if(strlen($value) > 10) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType20)) {
                if(strlen($value) > 20) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType30)) {
                if(strlen($value) > 30) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType70)) {
                if(strlen($value) > 70) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType100)) {
                if(strlen($value) > 100) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType200)) {
                if(strlen($value) > 200) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }

            if (in_array($key, $stringType500)) {
                if(strlen($value) > 500) {
                    $errormsg .= "訂單號".$maindata['ord_no'].': '.$key." 超過長度 請檢查";
                }
            }
            //字串end
        }

        return $errormsg;
    }

    public function checkopenApiDetailCoulumnType($ordNo ,$detaildata) {

        $errormsg = "";

        $intType      = ['pkg_num', 'gw', 'length', 'weight', 'height'];
        $stringType1  = ['pkg_unit'];
        $stringType2  = ['goods_no', 'goods_no2'];
        $stringType3  = ['cbmu', 'gwu'];
        $stringType7  = ['goods_nm'];
        $stringTypeSn = ['sn_no',];


        for ($i=0; $i < count($detaildata); $i++) { 

            foreach ($detaildata[$i] as $key => $value) {

                //int 系列
                if (in_array($key, $intType)) {
                    if(!is_numeric($value)) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 有問題 請檢查";
                    }
                }
                //int 系列 end
                
                //字串
                if (in_array($key, $stringType1)) {
                    if( strlen($value) > 10) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType2)) {
                    if(strlen($value) > 20) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType3)) {
                    if(strlen($value) > 30) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                if (in_array($key, $stringType7)) {
                    if(strlen($value) > 70) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                
                
                if (in_array($key, $stringTypeSn)) {
                    if(strlen($value) > 10000) {
                        $errormsg .= "訂單號".$ordNo.': '.$key." 超過長度 請檢查";
                    }
                }
                //字串end
                
            }
        }
        

        return $errormsg;
    }

    
    public function delorderimg(Request $request) {
        Log::info("delorderimg");
        Log::info($request->all());
        try {    
            $imgdata = DB::connection('mysql::write')
            ->table('mod_file')
            ->where('id', $request->id)
            ->first(); 
    
            DB::table('mod_file')
            ->where('id', $request->id)
            ->delete();
            Storage::disk('local')->delete($imgdata->guid);
    
            return response()->json([
                'msg' => 'success',
                'code' => '00',
            ]);
            
        } catch (\Throwable $e) {
            Log::info("delorderimg error");
            Log::info($e->getMessage());
            return response()->json([
                'msg' => $e->getMessage(),
                'code' => '01',
            ]);
        }

    }

    public function sendGpsDataByWebsocket ($data) {
        try {
            Log::info("enter GpsDataByWebsocket");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://dss-ws.target-ai.com:9599/websocket/notifyByGroupId',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "groupId": "groupId_StdTmsGpsDataByWebsocketDev",
                    "msg": ' . json_encode($data) .'
                }',
                CURLOPT_HTTPHEADER => array(
                    'cache-control: no-cache',
                    'content-type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            // Log::info($response);
        }
        catch(\Exception $e) {
            Log::info("GpsDataByWebsocket error");
            Log::info($e->getMessage());
            Log::info($e->getLine());
        }
        Log::info("GpsDataByWebsocket finish");
        return;
    }



    public function stdcheckversion(Request $request) {
        Log::info("stdcheckversion");
        Log::info($request->all());
        $user       = Auth::user();
        if($request->phone_type!="ios") {
            $appver = DB::table('bscode')->where('cd_type','appversion')->where('cd','appversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "data"=>$appver->cd_descp,"code"=>"01"]);
            } else {
                return response()->json(['msg' => '當前已是最新版本',"code"=>"00"]);
            }
        } else {
            $appver = DB::table('bscode')->where('cd_type','appversion')->where('cd','iosversion')->first();
            if($request->appversion != $appver->value1 && $request->appversion != $appver->value2 ){
                return response()->json(['msg' => '版本不相符 請更新app版本', "url"=>$appver->cd_descp,"code"=>"01"]);
            }else{
                return response()->json(['msg' => '當前已是最新版本',"code"=>"00"]);
            }
        }
    }




    public function stdrefreshpass() {
        try {
            $user = Auth::user();      
            $userData = DB::table('users')
                            ->where('id', $user->id)
                            ->update(['final_refresh' => Carbon::now()->toDateTimeString()]);
        }
        catch(\Exception $e) {
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }

    public function beforecheckin(Request $request) {
        Log::info("beforecheckin");
        Log::info($request->all());
        try {
            $user = Auth::user();
            $now = date("Y-m-d H:i:s");
            $today = date("Y-m-d 00:00:00");
            Log::info('user id');
            Log::info($user->id);
            $allwarehouse =  DB::table('mod_warehouse')
            ->whereNull('dc_id')
            ->get();
            $warehouse = array();
            foreach ($allwarehouse as $key => $whrow) {
                $distince =  $this->getdistancestation($whrow->lat,$whrow->lng,$request->lat,$request->lng);
                // Log::info($whrow->cname);
                // Log::info($distince);
                // Log::info($whrow->gps_range);
                if($distince <= $whrow->gps_range) {
                    $warehouse = $whrow;
                }
            }
            if(!isset($warehouse->cust_no)) {
                return response()->json(['msg' => '需靠近倉庫才可進行報到', 'code' => '01', 'data'=> 'N']);
            }
            Log::info('紀錄報到 start');
            Log::info(!isset($checkexist) );
            //Log::info(isset($lastcheckin));
            Log::info('紀錄報到');
        }
        catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
        return response()->json(['msg' => '確認是否要報到?', 'code' => '00', 'data'=> '' ]);

    }
    public function checkin(Request $request) {
        Log::info("checkin");
        Log::info($request->all());
        $user = Auth::user();
        $now = date("Y-m-d H:i:s");
        $today = date("Y-m-d 00:00:00");
        //判斷到站gps 是否在範圍內
        $warehouse = array();
        $dlvmark = DB::table('mod_dss_dlv')
        ->where('driver_phone', $user->email)
        ->first();

        //2023-06-30 第二關卡控
        $warehouse =  DB::table('mod_warehouse')
        ->where('cname', $request->wh_no)
        ->whereNull('dc_id')
        ->first();

        if(isset($warehouse)) {
            $distince =  $this->getdistancestation($warehouse->lat,$warehouse->lng,$request->lat,$request->lng);
            if($distince >= $warehouse->gps_range) {
                return response()->json(['msg' => '需靠近倉庫才可進行報到', 'code' => '01', 'data'=> 'N']);
            }
        }

        //第二關也要擋住 2023-06-19
        $checkexist = DB::table('mod_driver_checkin')
        ->where('phone', $user->email)
        ->where('created_at', '>=', $today)
        ->where('dc_id', $warehouse->cust_no)
        ->whereNull('leave_time')
        ->first();
        if(isset($checkexist)) {
            return response()->json(['msg' => '必須先離場才可進行報到', 'code' => '01', 'data'=> 'N']);
        }

        $lastcheckinfortime = DB::table('mod_driver_checkin')
        ->where('phone', $user->email)
        ->where('dc_id', $warehouse->cust_no)
        ->where('discharged_status', '!=', '(手動離場)')
        ->whereNotNull('leave_time')
        ->orderBy('id','desc')
        ->first();
        Log::info( $user->email);
        Log::info(isset($lastcheckinfortime));
        if(isset($lastcheckinfortime)) {
            $checkinintervals = DB::table('bscode')
            ->where('cd_type', 'CHECKININTERVALS')
            ->where('cd', 'checkinintervals')
            ->value('value1');

            $lastleave = strtotime($lastcheckinfortime->leave_time);
            $nowtime   = strtotime( date("Y-m-d H:i:s"));
            $result = (int) (($nowtime  - $lastleave) / 60 );
            Log::info('判斷時間 第二關');
            Log::info($result);
            Log::info('判斷時間 第二關 end');
            if($result < (int)$checkinintervals && $lastcheckinfortime->delivery_type !='還板報到' ) {
                return response()->json(['msg' => '報到時間需間隔'.$checkinintervals.'分鐘', 'code' => '01', 'data'=> 'Y' ]);
            }
        }
        //第二關也要擋住
        $markdata =  DB::table('mod_mark')
        ->where('cust_no', $dlvmark->mark_no)
        ->first();
        $checkinstatus = "出貨";
        $checkindesc   = "出貨";
        if($request->delivery_type == "還板報到" ) {
            $checkinstatus = "還板";
            $checkindesc = "還板";
            $dlvmark->mark_no = "";
        }
        
        // 'discharged_status' => '(提貨放行)',
        $checkinId = DB::table('mod_driver_checkin')->insertGetId([
            'status'         => 'import',
            'status_desc'    => '進場',
            'job_type'       => $checkinstatus,
            'job_type_desc'  => $checkindesc,
            'button_status'  => '確認下貨',
            'mark_no'        => $dlvmark->mark_no,
            'mark_name'      => $dlvmark->mark_no,
            'driver_name'    => $user->name,
            'phone'          => $user->email,
            'warehouse'      => $request->wh_no,
            'dc_id'          => $warehouse->cust_no,
            'wh_no'          => isset($markdata->wh_no) ? $markdata->wh_no : null,
            'car_no'         => $request->car_no,
            'car_type'       => $request->car_type,
            'car_type_desc'  => $request->car_type,
            'truck_cmp_no'   => $request->truck_cmp_name,
            'truck_cmp_name' => $request->truck_cmp_name,
            'delivery_type'  => $request->delivery_type,
            'entry_time'     => $now,
            'created_by'     => $user->email,
            'updated_by'     => $user->email,
            'created_at'     => $now,
            'updated_at'     => $now,
        ]);

        //預先編輯
        DB::table('mod_pallet_manage_log')
        ->where('owner_by', $user->email)
        ->where('checkin_id', -999)
        ->update([
            'checkin_id' => $checkinId
        ]);

        DB::table('mod_order')
        ->where('driver_phone', $user->email)
        ->where('status','PICKEDFINISH')
        ->update([
            'mark_no'       => $dlvmark->mark_no,
            'mark_name'     => $dlvmark->mark_no,
            'check_in_time' => Carbon::now()->toDateTimeString(),
        ]);
        //

        $dlvdata = DB::table('mod_dss_dlv')
        ->where('driver_phone',$user->email)
        ->where('status' ,'!=' ,'FINISHED')
        ->pluck('dlv_no')
        ->toArray();
        Log::info("checkinId");
        Log::info($checkinId);
        DB::table('mod_dss_dlv')
        ->where('dc_id',$warehouse->cust_no)
        ->where('driver_phone',$user->email)
        ->where('status' ,'!=' ,'FINISHED')
        ->whereNull('checkin_id')
        ->update([
            'status'      => 'DLV',
            'status_desc' => '配送中',
            'checkin_id'  => $checkinId,
            'updated_at'  => Carbon::now()->toDateTimeString(),
        ]);



        DB::table('mod_dlv_plan')
        ->where('trs_mode','pick')
        ->whereIn('dlv_no', $dlvdata)
        ->where('backdelivery','N')
        ->where('status','UNTREATED')
        ->where('dc_id',$warehouse->cust_no)
        ->update([
            'button_status'      => 'STARTUPLOADING',
            'button_status_desc' => '開始上貨',
        ]);

        // date("Y-m-d H:i:s");
        $checkreserve = DB::table('mod_mark_reserve')
        ->where('dc_id',$warehouse->cust_no)
        ->where('driver_phone', $user->email)
        ->whereNull('mark_no')
        ->where('reserve_date', date("Y-m-d"))
        ->first();


        if(!isset($checkreserve) ) {
            Log::info("報到預約新增");
            Log::info("test mark");
            Log::info(substr($warehouse->cust_no,0,2));
            $canuse = DB::table('mod_mark')
            ->where('is_enabled','Y')
            ->where('type','!=','code4')
            ->where('wh_no', 'like', '%'.substr($warehouse->cust_no,0,2).'%')
            ->first();
            Log::info($canuse->id);

            $marksetting = DB::table('mod_mark_setting')
            ->where('warehouse_id',$request->dc_id)
            ->where('car_type', $canuse->parked_models.'車')
            ->first();
            $processTime = '15';
            if(isset($marksetting->process_time)) {
                $processTime = $marksetting->process_time;
            }
            $newtimestamp = strtotime(date("Y-m-d H:i:00").' + '. $processTime.' minute');
            $endat = date('Y-m-d H:i:s', $newtimestamp);

            DB::table('mod_mark_reserve')->insert([
                'dc_id'          => $warehouse->cust_no,
                'dc_name'        => $warehouse->cname,
                'reserve_date'   => date("Y-m-d"),
                'reserve_h'      => date("H"),
                'reserve_m'      => date("i"),
                'full_date'      => date("Y-m-d H:i:00"),
                'driver_phone'   => $user->email,
                'driver_name'    => $user->name,
                'car_no'         => $request->car_no,
                'car_type'       => $request->car_type,
                'car_type_descp' => $request->car_type,
                'truck_cmp_no'   => $request->truck_cmp_name,
                'truck_cmp_name' => $request->truck_cmp_name,
                'onsite'         => 'Y',
                'g_key'          => 'DSS',
                'c_key'          => 'DSS',
                's_key'          => 'DSS',
                'd_key'          => 'DSS',
                'end_at'         => $endat,
                'created_by'     => $user->email,
                'updated_by'     => $user->email,
                'created_at'     => $now,
                'updated_at'     => $now,
            ]);
        }
        //2023-04-24
        DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_to_wh','Y')
        ->where('backdelivery','N')
        ->where('lock_pallet','N')
        ->whereIn('dlv_no', $dlvdata)
        ->where('dc_id', $warehouse->cust_no)
        ->update([
            'button_status'      => 'CHECKIN',
            'button_status_desc' => '報到',
            'updated_at'         => $now,
        ]);

        DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_to_wh','Y')
        ->where('backdelivery','N')
        ->whereIn('dlv_no', $dlvdata)
        ->where('dc_id', '!=', $warehouse->cust_no)
        ->update([
            'button_status'      => 'STARTUNLOADING',
            'button_status_desc' => '開始下貨',
            'status'             => 'PICKED',
            'updated_at'         => $now,
        ]);
        DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_to_wh','N')
        ->where('backdelivery','N')
        ->where('lock_pallet','N')
        ->where('status','!=','FINISHED')
        ->where('status' ,'!=' ,'CLOSED')
        ->whereIn('dlv_no', $dlvdata)
        ->update([
            'button_status'      => 'ARRIVE',
            'button_status_desc' => '到點',
            'status'             => 'PICKED',
            'updated_at'     => $now,
        ]);
        // 拒收順收

        $orderarrive = DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_to_wh','N')
        ->where('backdelivery','N')
        ->where('lock_pallet','Y')
        ->whereIn('dlv_no', $dlvdata)
        ->pluck('wms_ord_no')->toArray();
        
        if(count($orderarrive) >0 ) {
            DB::table('mod_order')
            ->whereIn('ord_no', $orderarrive)
            ->update([
                'upload_start_time' => DB::raw("COALESCE(upload_start_time, '".Carbon::now()->toDateTimeString()."')"),
                'updated_at'        => Carbon::now()->toDateTimeString(),
            ]);
        }

        DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_to_wh','N')
        ->where('backdelivery','N')
        ->where('lock_pallet','Y')
        ->whereIn('dlv_no', $dlvdata)
        ->where('button_status', '!=', 'FINISHED')
        ->where('status', '!=', 'FINISHED')
        ->where('status' ,'!=' ,'CLOSED')
        ->update([
            'button_status'      => 'STARTUNLOADING',
            'button_status_desc' => '開始下貨',
            'status'             => 'PICKED',
            'updated_at'         => $now,
            'finish_time'        => NULL,
        ]);
        $pickfinish = DB::table('mod_dlv_plan')
        ->where('trs_mode','pick')
        ->where('dlv_to_wh','N')
        ->where('backdelivery','Y')
        ->where('button_status', 'FINISHED')
        ->whereIn('dlv_no', $dlvdata)
        ->get();
        //

        $orderbackarrive = DB::table('mod_dlv_plan')
        ->where('dlv_to_wh','Y')
        ->whereIn('dlv_no', $dlvdata)
        ->where('status', 'FINISHED')
        ->pluck('wms_ord_no')
        ->toArray();
        Log::info("FOR update order arrive");
        Log::info($orderbackarrive);
        if(count($orderbackarrive) > 0 ) {
            DB::table('mod_order')
            ->whereIn('ord_no', $orderbackarrive)
            ->update([
                'arrive_time' => Carbon::now()->toDateTimeString(),
                'updated_at'  => Carbon::now()->toDateTimeString(),
            ]);
        }

        foreach ($pickfinish as $pickfinishkey => $pickfinishrow) {
            //純逆物流
            Log::info("pickfinishrow");
            Log::info($pickfinishrow->id);
            // 逆物流提貨完成到點後
            DB::table('mod_dlv_plan')
            ->where('trs_mode','dlv')
            ->where('backdelivery','Y')
            ->where('lock_pallet','N')
            ->where('dlv_no', $pickfinishrow->dlv_no)
            ->where('dlv_ord_no', $pickfinishrow->dlv_ord_no)
            ->where('wms_ord_no', $pickfinishrow->wms_ord_no)
            ->where('button_status', '!=', 'FINISHED')
            ->where('status', '!=', 'FINISHED')
            ->where('status' ,'!=' ,'CLOSED')
            ->update([
                'button_status'      => 'ARRIVE',
                'button_status_desc' => '到點',
                'status'             => 'PICKED',
                'updated_at'         => $now,
            ]);
            # code...
        }

        $this->updatedashboardstatus();

        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }

    function getdistancestation($stationlat,$stationlng,$Lat,$Lng){
        //将角度转为狐度
        $radLat1 = deg2rad($stationlat);//deg2rad()函数将角度转换为弧度
    
        $radLat2 = deg2rad($Lat);
    
        $radLng1 = deg2rad($stationlng);
    
        $radLng2 = deg2rad($Lng);
    
        $a       = $radLat1-$radLat2;
    
        $b       = $radLng1-$radLng2;
    
        $s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;
    
        return $s;
    }

    public function uploading(Request $request) {
        Log::info("uploading");
        Log::info($request->all());
        $user = Auth::user();
        // 配送中
        //逆務流上貨完成

        //逆務流上貨完成end

        try {
            $forapidetail = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->whereIn('id', $request->ids)
            ->get();

            
            $wmsOrdNoArray = array();

            $detail = $forapidetail[0];

            $sysWmsApiInsertList = array();
            if($detail->button_status_desc == "開始上貨"){
                foreach ($forapidetail as $key => $apidetail) {
                    array_push($wmsOrdNoArray, $apidetail->wms_ord_no);
                    array_push($sysWmsApiInsertList, array(
                        'wms_ord_no'  => $apidetail->wms_ord_no,
                        'ord_no'      => $apidetail->ord_no,
                        'status'      => 'LOADING',
                        'status_desc' => '貨物裝載中',
                        'action_time' => Carbon::now()->toDateTimeString(),
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString(),
                    ));
                }
            }
            $sysWmsApiInsertArray = collect($sysWmsApiInsertList);
            $chunks = $sysWmsApiInsertArray->chunk(500);
            foreach($chunks as $chunk) {
                DB::table('sys_wms_api')->insert($chunk->toArray());
            }

            DB::table('mod_order')
            ->whereIn('ord_no', $wmsOrdNoArray)
            ->update([
                'status'            => 'LOADING',
                'status_desc'       => '貨物裝載中',
                'upload_start_time' => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString(),
            ]);
            Log::info("uploading detail");
            Log::info($detail->backdelivery);
            if($detail->backdelivery == "Y" ) {

                Log::info("backdelivery Y");
                Log::info($detail->button_status);
                Log::info($detail->button_status_desc);
                if($detail->button_status_desc  == "到點") {

                    DB::table('mod_order')
                    ->whereIn('ord_no', $wmsOrdNoArray)
                    ->update([
                        'status'            => 'PICKEDFINISH',
                        'status_desc'       => '揀貨完成',
                        'upload_start_time' => Carbon::now()->toDateTimeString(),
                        'updated_at'        => Carbon::now()->toDateTimeString(),
                    ]);

                    // 為a地b送作修改 單號 dlv_no ='A023032000112'
                    Log::info("123 Y");
                    DB::table('mod_dlv_plan')
                    ->whereIn('id', $request->ids)
                    ->update([
                        'button_status_desc' => '開始上貨',
                    ]);
                }else {
                    Log::info("456 Y");
                    DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->whereIn('id', $request->ids)
                    ->where('trs_mode', 'pick')
                    ->update([
                        'button_status'      => 'PICKFINISHED',
                        'button_status_desc' => '上貨完成',
                    ]);

                    

                }
                $deliverymsg =  DB::table('mod_sys_message')
                ->where('title', '放行推播訊息')
                ->value('content');
                $deliverymsg = str_replace('{dlv_no}', $detail->dlv_no, $deliverymsg );
                $deliverymsg = str_replace('{driver_name}',  $user->name, $deliverymsg);
                if($detail->trs_mode == 'dlv' && $detail->button_status == "STARTUPLOADING") {
                    // 2023-06-27 問題224 放行 
                    $postdata     = array();
                    $postdata['content']    = $deliverymsg;
                    $postdata['created_by'] = $user->name;
                    $postdata['updated_at'] = date("Y-m-d H:i:s");
                    $postdata['created_at'] = date("Y-m-d H:i:s");
                    $senddata = array();
                    $senddata = DB::table('users')
                    ->where('role', 'like', '%STORKEEPER%')
                    ->where('check_wh', 'like', '%'.$detail->wh_no.'%')
                    ->get();
                    $dTokenList = array();
                    $messagedataList = array();
                    foreach ($senddata as $key => $senduser) {
                        $messagedata    = array();
                        $messagedata['title']        = '請協助放行';
                        $messagedata['to_by']        = $senduser->email;
                        $messagedata['to_by_name']   = $senduser->name;
                        $messagedata['content']      = $deliverymsg;
                        $messagedata['created_by']   = $user->name;
                        $messagedata['updated_by']   = $user->name;
                        $messagedata['created_at']   = date("Y-m-d H:i:s");
                        $messagedata['updated_at']   = date("Y-m-d H:i:s");
                        array_push($dTokenList, $senduser->d_token);
                        array_push($messagedataList, $messagedata);
                    }
                    FcmHelper::batchSendToFcm( '協助放行' , $deliverymsg, $dTokenList);
                    $messagedataArray = collect($messagedataList);
                    $chunks = $messagedataArray->chunk(500);
                    foreach($chunks as $chunk) {
                        Fcm::insert($chunk->toArray());
                    }
                }


            } else {
                $deliverymsg =  DB::table('mod_sys_message')
                ->where('title', '放行推播訊息')
                ->value('content');

                $deliverymsg = str_replace('{dlv_no}', $detail->dlv_no, $deliverymsg );
                $deliverymsg = str_replace('{driver_name}',  $user->name, $deliverymsg);


                Log::info("backdelivery N");
                if($detail->button_status_desc == "上貨完成" || $detail->button_status_desc == "待放行") {
                    // 判斷是不是同大倉上貨 不同小倉
                    Log::info($detail->wms_ord_no);
                    DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->whereIn('id', $request->ids)
                    ->where('backdelivery', 'N')
                    ->update([
                        'button_status_desc' => '待放行',
                        'status_desc'        => '任務已分派',
                    ]);
                    $postdata     = array();
                    $postdata['content']    = $deliverymsg;
                    $postdata['created_by'] = $user->name;
                    $postdata['updated_at'] = date("Y-m-d H:i:s");
                    $postdata['created_at'] = date("Y-m-d H:i:s");
                    $senddata = array();
                    $senddata = DB::table('users')
                    ->where('role', 'like', '%STORKEEPER%')
                    ->where('check_wh', 'like', '%'.$detail->wh_no.'%')
                    ->get();
                    if($detail->trs_mode == "pick" ) {
                        Log::info('起運點放行');
                    } else {
                        //
                        Log::info('配送點放行');
                        // $dlvwarehouse = DB::table('mod_warehouse')
                        // ->where('address', $checkdlvdata->cust_address)
                        // ->whereNotNull('dc_id')
                        // ->first();

                        // $senddata = DB::table('users')
                        // ->where('role', 'like', '%STORKEEPER%')
                        // ->where('check_wh', 'like', '%'.$dlvwarehouse->cust_no.'%')
                        // ->get();
                    }
                    $dTokenList = array();
                    $messagedataList = array();
                    foreach ($senddata as $key => $senduser) {
                        // $this->updateUnReadCount($senduser->id);
                        // $this->sendDsvTmsUnReadMessageByWebsocket($postdata, $senduser->id);
                        # code...
                        $messagedata    = array();
                        // $messagedata['sys_ord_no']   = $detail->ord_no;
                        $messagedata['title']        = '請協助放行';
                        // $messagedata['from_by']      = $user->id;
                        // $messagedata['from_by_name'] = $user->name;
                        $messagedata['to_by']        = $senduser->email;
                        $messagedata['to_by_name']   = $senduser->name;
                        $messagedata['content']      = $deliverymsg;
                        $messagedata['created_by']   = $user->name;
                        $messagedata['updated_by']   = $user->name;
                        $messagedata['created_at']   = date("Y-m-d H:i:s");
                        $messagedata['updated_at']   = date("Y-m-d H:i:s");
                        // $this->createMessage($messagedata);
                        //$this->createFcm($messagedata);
                        array_push($dTokenList, $senduser->d_token);
                        array_push($messagedataList, $messagedata);
                        //FcmHelper::sendToFcm( '協助放行' , '車次編號'.$detail->dlv_no.'請協助放行-'.$user->name, $senduser->d_token);
                    }
                    FcmHelper::batchSendToFcm( '協助放行' , $deliverymsg, $dTokenList);
                    $messagedataArray = collect($messagedataList);
                    $chunks = $messagedataArray->chunk(500);
                    foreach($chunks as $chunk) {
                        Fcm::insert($chunk->toArray());
                    }
                } else {
                    DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->whereIn('id', $request->ids)
                    ->where('trs_mode', 'pick')
                    ->where('backdelivery', 'N')
                    ->update([
                        'button_status_desc' => '上貨完成',
                        'status'             => 'DLV',
                    ]);
                }

            }
            $finalid =  DB::table('mod_driver_checkin')
            ->where('phone', $user->email)
            ->orderBy('id','desc')
            ->first();
            //228
            if(isset($finalid)) {
                $warehouseinfo = DB::table('mod_warehouse')
                ->where('dc_id', $detail->dc_id)
                ->first();
                if($warehouseinfo->leave_decision == "guard") {
                    DB::table('mod_driver_checkin')
                    ->where('id', $finalid->id)
                    ->update([
                        'discharged_status' => NULL,
                        'updated_at'        => Carbon::now()->toDateTimeString(),
                    ]);
                }
            }
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

        $this->updatedashboardstatus();
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }
    


    public function arrivercheck(Request $request) {
        Log::info("arrivercheck");
        Log::info($request->all());
        try {
            $user = Auth::user();

            $checkinget = DB::table('mod_driver_checkin')
            ->where('created_by', $user->email)
            ->orderBy('id','desc')
            ->first();

            $dlvplan = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->get();
            
            if(!isset($checkinget)) {
                return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
            }

            $checkpick = null;
            foreach ($dlvplan as $item) {
                if ($item->trs_mode == 'pick' && $item->button_status == 'FINISHED') {
                    $checkpick = $item;
                }
            }

            $dlvplan = $dlvplan[0];
            
            if(!isset($checkpick) && $dlvplan->backdelivery == "N" ) {
                return response()->json(['msg' => '請先完成提貨任務 ', 'code' => '01', 'data'=> null]);
            }
            
            Log::info($dlvplan->backdelivery);
            Log::info($dlvplan->from_handover);
            Log::info($dlvplan->handover_flag);
            if ($checkinget->leave_time == null && $dlvplan->backdelivery == "N" && $dlvplan->from_handover == "N" ) {
                Log::info('123');
                Log::info($checkinget->leave_time);
                Log::info($dlvplan->backdelivery);
                Log::info($dlvplan->from_handover);
                return response()->json(['msg' => '尚未點選離場', 'code' => '01', 'data'=> null]);
            }

            
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
    }

    // 2023-07-10 by Tony 優化
    public function driverarriver(Request $request) {
        Log::info("driverarriver");
        Log::info($request->all());
        $user = Auth::user();

        $dssdetail = DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('id', $request->ids)
        ->get();

        $hasback = 0;

        $samedata = array();

        foreach ($dssdetail as $detail) {
            if ($detail->backdelivery == 'Y') {
                $hasback++;
            }
            if ($detail->trs_mode == 'dlv') {
                array_push($samedata, $detail->wms_ord_no);
            }
        }
        
        $arriverange = DB::table('bscode')
        ->where('cd_type', 'DLVGPSRANGE')
        ->where('cd', 'dlvgpsrange')
        ->first();
        
        if(!empty($dssdetail[0]->lat)  && !empty($dssdetail[0]->lng)) {
            $distance = $this->getDistanceBetweenPointsNew($request->lat, $request->lng, $dssdetail[0]->lat, $dssdetail[0]->lng );
            $distance = $distance *1000;//換算回公尺
            if($distance > $arriverange->value1 ) {
                return response()->json(['msg' => '尚未到達客戶範圍內', 'code' => '01', 'data'=> null]);
            }
        }

        Log::info('uuuuu');
        Log::info($hasback);

        if($hasback > 0 ) {
            Log::info('test');
            $samedata = array();
            foreach ($dssdetail as $detail) {
                array_push($samedata, $detail->wms_ord_no);
            }

            DB::table('mod_dlv_plan')
            ->where('backdelivery', 'Y')
            ->whereIn('id', $request->ids)
            ->update([
                'button_status'      => 'STARTUPLOADING',
                'button_status_desc' => '開始上貨',
            ]);
        }

        DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('id', $request->ids)
        ->where('trs_mode','dlv')
        ->update([
            'button_status'      => 'STARTUNLOADING',
            'button_status_desc' => '開始下貨',
            'arrive_lat'         => $request->lat,
            'arrive_lng'         => $request->lng,
        ]);

        $distance = 0 ;
        $spellcarbon = 0;

        // if(isset($carbon)) {
        //     $distance = $this->getDistanceBetweenPointsNew($request->lat, $request->lng, $warehouse->lat, $warehouse->lng );
        //     Log::info('碳排放量');
        //     Log::info($distance);
        //     Log::info($carbon->carbon_value);

        //     $spellcarbon = $distance *  $carbon->carbon_value;
        // }

        DB::table('mod_order')
        ->whereIn('ord_no', $samedata)
        ->where('dlv_addr', $dssdetail[0]->cust_address)
        ->update([
            'carbon_distance' => $distance,
            'carbon'          => $spellcarbon,
            'arrive_lat'      => $request->lat,
            'arrive_lng'      => $request->lng,
            'arrive_time'     => Carbon::now()->toDateTimeString(),
            'updated_at'      => Carbon::now()->toDateTimeString(),
        ]);
        
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }

    function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'kilometers') {
        $theta = $longitude1 - $longitude2; 
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 
        $distance = acos($distance); 
        $distance = rad2deg($distance); 
        $distance = $distance * 60 * 1.1515; 
        switch($unit) { 
          case 'miles': 
            break; 
          case 'kilometers' : 
            $distance = $distance * 1.609344; 
        } 
        return (round($distance,2)); 
    }

    public function palletprocess() {
        Log::info('palletprocess');
        try {

            $limitday = DB::table('bscode')
            ->where('cd_type', 'WHPROCESSPALLET')
            ->where('cd', 'whprocesspallet')
            ->value('value1');
            $days = date("Y-m-d ", strtotime("-".$limitday." day"));

            $user = Auth::user();
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();

            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $dcid =  substr($fordetailrow,0,2);
                $newdcid[] = $dcid;
            }

            $dssdlv = DB::table('mod_dss_dlv')
            ->select(
                'sys_dlv_no','dlv_no','dlv_date','truck_cmp_no','truck_cmp_name','driver_phone','driver_nm',
                DB::raw("pallet_num as total_pallet_num")
            )
            ->where('dlv_date', '>=', $days)
            ->whereIn('dc_id', $newdcid)
            ->get();
            foreach ($dssdlv as $key => $row) {
                if($row->total_pallet_num == null) {
                    $row->total_pallet_num = "";
                } else {
                    $row->total_pallet_num = $row->total_pallet_num.'';
                }
                
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $dssdlv]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
    }

    public function checkpalletbydlvno(Request $request) {
        $user = Auth::user();
        try {

            $dssdata = DB::table('mod_dss_dlv')
            ->where('dlv_no', $request->dlv_no)
            ->first();

            $mark = DB::table('mod_mark')
            ->where('is_enabled','Y')
            ->where('tyPe','!=','code4')
            ->where('wh_no',  $dssdata->dc_id)
            ->pluck('cust_no')->toArray();

            $detailCusts = DB::table('mod_dlv_plan')
            ->select('wh_no','owner_name', 'dlv_no')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode', 'dlv')
            ->get();

            foreach ($detailCusts as $key => $row) {
                $dlvPallet = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw("pallet_type as pallet_name"),
                    'pallet_code',
                    DB::raw("sum(pallet_num)as pallet_num")
                )
                ->where('type_cd', 'driver')
                ->where('dlv_no', $request->dlv_no)
                ->groupBy('pallet_code','pallet_type')
                ->get();

                $totalnum = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw("sum(pallet_num)as pallet_num")
                )
                ->where('type_cd', 'driver')
                ->where('dlv_no', $request->dlv_no)
                ->value('pallet_num');
                $row->mark_no = $dssdata->mark_no;
                $row->driver_phone = $dssdata->driver_phone;
                $row->total_pallet = $totalnum;
                $row->detail = $dlvPallet;
                $row->mark = $mark;
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $detailCusts]);

        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> $e->getMessage()]);
        }

    }

    public function starttakeoff(Request $request) {
        Log::info("starttakeoff");
        Log::info($request->all());
        $user = Auth::user();


        $dssdlv = DB::table('mod_dss_dlv')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->first();

        $checkpick =  DB::table('mod_dlv_plan')
        ->where('trs_mode','pick')
        ->where('button_status', 'FINISHED')
        ->where('dlv_ord_no', $request->dlv_ord_no)
        ->first();
        if(!isset($checkpick)) {
            return response()->json(['msg' => '請先完成提貨任務 ', 'code' => '01', 'data'=> null]);
        }

        $checkfornotfin = DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('id', $request->ids)
        ->where('trs_mode', 'DLV')
        ->pluck('wms_ord_no')
        ->toArray();

        $notfinishdata = DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('wms_ord_no', $checkfornotfin)
        ->where('trs_mode', 'pick')
        ->where('button_status', '!=', 'FINISHED')
        ->pluck('wms_ord_no')
        ->toArray();

        if(count($notfinishdata) > 0) {
            $wmsstr = implode(', '."\n", $notfinishdata);
            return response()->json(['msg' =>  '請先完成提貨任務'."\n"."客戶訂單編號："."\n".$wmsstr, 'code' => '01', 'data'=> null ]);
        }
        $dlvdetail = DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('id', $request->ids)
        ->pluck('wms_ord_no')->toArray();

        DB::table('mod_order')
        ->whereIn('ord_no', $dlvdetail)
        ->where('status', '!=', 'FINISHED')
        ->update([
            'status'        => 'OFFSTAFF',
            'status_desc'   => '貨物卸載中',
            'unupload_time' => Carbon::now()->toDateTimeString(),
            'updated_at'    => Carbon::now()->toDateTimeString(),
        ]);

        $detail =  DB::table('mod_dlv_plan')
        ->where('dlv_ord_no', $request->dlv_ord_no)
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->first();

        $forapidetail = DB::table('mod_dlv_plan')
        ->where('sys_dlv_no', $request->sys_dlv_no)
        ->whereIn('id', $request->ids)
        ->get();
        foreach ($forapidetail as $key => $apidetail) {
            DB::table('sys_wms_api')
            ->insert([
                'wms_ord_no'  => $apidetail->wms_ord_no,
                'ord_no'      => $apidetail->ord_no,
                'status'      => 'OFFSTAFF',
                'status_desc' => '貨物卸載中',
                'action_time' => Carbon::now()->toDateTimeString(),
                'created_by'  => $user->email,
                'updated_by'  => $user->email,
                'created_at'  => Carbon::now()->toDateTimeString(),
                'updated_at'  => Carbon::now()->toDateTimeString(),
            ]);
        }


        DB::table('mod_dlv_plan')
        ->where('trs_mode','dlv')
        ->where('dlv_no', $dssdlv->dlv_no)
        ->whereIn('id', $request->ids)
        ->update([
            'button_status'      => 'TOGO',
            'button_status_desc' => '下貨完成',
        ]);
        $this->updatedashboardstatus();
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }
    public function whscandriver(Request $request) {
        Log::info("whscandriver");
        Log::info($request->all());
        $user = Auth::user();
        try {

            $drivermission = DB::table('mod_dss_dlv')
            ->where('driver_phone', $request->driver_phone)
            ->pluck('dlv_no')->toArray();

            $mission = DB::table('mod_dlv_plan')
            ->whereIn('dlv_no', $drivermission)
            // ->where('cust_name', $user->manage_warehouse_name)
            ->where('trs_mode', 'dlv')
            ->get();

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $mission]);

        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> $e->getMessage()]);
        }

    }
    public function getannounce(Request $request) {
        Log::info("getannounce");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $today = date("Y-m-d");
            $announce = DB::table('mod_announce')
            ->where('announce_start_date', '<=', $today)
            ->where('announce_end_date', '>=', $today)
            ->where('is_enabled', 'Y')
            ->orderBy('announce_date','desc')
            ->orderBy('id','desc')
            ->get();
            $returndata = array();
            $userrole = explode(',', strtoupper($user->role));
            $forcheck = array();
            if(!in_array('ADMIN', $userrole) ) {
                if(!empty($user->truck_cmp_no) ) {
                    // 司機
                    $warehouse = DB::table('mod_order')
                    ->where('driver_phone', $user->email )
                    ->groupBy('dc_id')
                    ->pluck('dc_id')
                    ->toArray();

                    foreach ($warehouse as $testkey => $testrow) {
                       $dcid =substr($testrow,0,2);
                       if($dcid != 'A0' && $dcid !='B0') {
                            $forcheck[] = $testrow;
                       } else {
                        $forcheck[] = $testrow;
                       }
                    }
                }
                foreach ($announce as $key => $row) {
                    Log::info('mod_announce id ');
                    Log::info($row->id);
                    Log::info($row->wh_no);
                    $announcerole = explode(',', $row->announce_type);
                    $resultdata = array_intersect($userrole, $announcerole);
                    if(empty($user->truck_cmp_no) ) {
                        $announcewh = explode(',', $user->check_wh);
                        $rowwarehouse = explode(',', $row->wh_no);

                        $resultwh = array_intersect($rowwarehouse, $announcewh); 

                        if(count($resultdata) > 0 && (count($resultwh) > 0 || $row->wh_no == '*')) {
                            Log::info('非司機公告123');
                            $returndata[] = $row;
                        }
                    } else {

                        if(!empty($user->truck_cmp_no) ) {
                            $announcewh = explode(',', $row->wh_no);
                            $resultwh = array_intersect($forcheck, $announcewh); 
                        }
                        Log::info('司機公告');
                        Log::info($warehouse);
                        Log::info($forcheck);
                        if(count($resultdata) > 0 && (count($resultwh) > 0 || $row->wh_no == '*')) {
                            $returndata[] = $row;
                        }
                    }

                }
            } else {
                Log::info('admin');
                foreach ($announce as $key => $row) {
                    Log::info('admin announce'.$key);
                    $returndata[] = $row;
                }
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $returndata]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }
    }

    public function checklackpallet(Request $request) {
        Log::info("checklackpallet");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $cartrader = DB::table('mod_car_trader')
            ->where('cust_name',$request->cust_no)
            ->first();

            $cartraderNo = '';
            if(isset($cartrader)) {
                $cartraderNo = $cartrader->cust_no;
            }
            $driverPhone = DB::table('users')
            ->where('truck_cmp_no',  $cartraderNo )
            ->pluck('email')->toArray();
            
            $data = array();
            $data = DB::table('mod_dss_dlv')
            ->select(
                DB::raw("driver_nm as name"),
                DB::raw("driver_phone as phone"),
                'pallet_num','dlv_no'
            )
            ->whereIn('driver_phone', $driverPhone)
            ->groupBy('driver_phone')
            ->groupBy('dlv_no')
            ->get();
            foreach ($data as $key => $row) {
                $detail = DB::table('mod_pallet_manage_log')
                ->select(
                    'pallet_code',
                    DB::raw("CAST(SUM(pallet_num) AS CHAR) AS pallet_num"),
                    DB::raw(" pallet_type as pallet_name")
                )
                ->where('dlv_no','dlv_no')
                ->where('type_cd','driver')
                ->where('pallet_num', '>', 0)
                ->where('owner_by',$row->phone)
                ->groupBy('pallet_code')
                ->get();

                $row->detail = $detail;
            }


            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    

    public function markreserveinfo(Request $request) {
        Log::info("markreserveinfo");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $data          = array();
            $nowByhour     = date("Y-m-d H:i:00");
            $nowtime       = strtotime($nowByhour);
            $now           = $request->date;
            $date          = $request->date;
            $today = date("Y-m-d");
            $standardDate  = array("Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday" ,"Saturday");
            $dayNameOfWeek = date("l", strtotime($now));
            $day           = array_search($dayNameOfWeek, $standardDate);
            Log::info($user->check_wh);
            $checkwh = explode(',', $user->check_wh);
            $newdcid = array();
            foreach ($checkwh as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            if($request->wh_user == "Y" ) {
                $bigwarehouse = DB::table('mod_warehouse')
                ->select('id',DB::raw("cust_no as dc_id"),DB::raw("cname as dc_name"))
                ->whereIn('cust_no',$newdcid)
                ->whereNull('dc_id')
                ->get();
            } else {
                $bigwarehouse = DB::table('mod_warehouse')
                ->select('id',DB::raw("cust_no as dc_id"),DB::raw("cname as dc_name"))
                ->whereNull('dc_id')
                ->get();
            }
            
            // 
            Log::info('count');
            Log::info(count($bigwarehouse));
            // business_hours
            foreach ($bigwarehouse as $key => $row) {
                Log::info('bigwarehouse id');
                Log::info($row->id);
                $notworks = DB::table('mod_warehouse_notwork')
                ->select('years','month','days')
                ->where('warehouse_id',$row->id)
                ->get();
                
                $notworkArray = array();
                foreach ($notworks as $notworkkey => $notworkrow) {
                    $notworkdays  = explode(',',$notworkrow->days);
                    foreach ($notworkdays as $key => $value) {
                        $notworkArray[]  = $notworkrow->years.'-'.str_pad($notworkrow->month, 2, "0", STR_PAD_LEFT) .'-'.$value ;
                    }
                }
                $businessArray = array();

                $notworkday = DB::table('mod_warehouse_notwork')
                ->where('warehouse_id', $row->id)
                ->where('years', substr($now, 0, 4))
                ->where('month', substr($now, 6, 1))
                ->get();

                $isNotWorkDay = false;

                foreach ($notworkday as $item) {
                    $days = explode(",", $item->days);
                    $targetDay = substr($now, -2);
                    foreach ($days as $d) {
                        $d = str_pad($d, 2, "0", STR_PAD_LEFT);
                        if ($d == $targetDay) {
                           //預約日期未營業
                            $businessArray[] = '預約日期未營業';
                            $isNotWorkDay = true;
                        }
                    }
                }

                if (!$isNotWorkDay) {
                    if(!in_array($date, $notworkArray)) {
                        $business = DB::table('mod_warehouse_detail')
                        ->select('start_at', 'end_at')
                        ->where('warehouse_id',$row->id)
                        ->where('dayofweek', $standardDate[$day])
                        ->first();
                    }
                    if(isset($business)) {
                        $start  = $business->start_at;
                        $endat = $business->end_at;
        
                        $starttime = strtotime($start);
                        $endtime = strtotime($endat);
                        for ($i = $starttime; $i < $endtime; $i= $i+ (15*60)) {
                            if($today == $now) {
                                if($i >= $nowtime) {
                                    $newtime =date('H:i', $i);
                                    $businessArray[] = $newtime;
                                }
                            } else {
                                $newtime =date('H:i', $i);
                                $businessArray[] = $newtime;
                            }

                        }
                    } else {
                        //無時段可預約
                        $businessArray[] = '無時段可預約';
                    }
                }
                $row->business_hours = $businessArray;

                $userOwnerGroup = explode(',',$user->check_owner);
                Log::info('userOwnerGroup');
                Log::info($user->check_owner);
                Log::info($row->dc_id);
                $owner = DB::table('sys_customers')
                ->select('cust_no',DB::raw("cname as cust_name"))
                ->where('wh_no', 'REGEXP', $row->dc_id)
                ->where('type', 'other')
                ->get();
                if($user->check_owner !="*") {
                    $checkOwner = DB::table('mod_customer_relation')
                    ->whereIn('group_cd', $userOwnerGroup)
                    ->pluck('cust_no')->toArray();

                    $owner = DB::table('sys_customers')
                    ->select('cust_no',DB::raw("cname as cust_name"))
                    ->where('wh_no', 'REGEXP', $row->dc_id)
                    ->whereIn('cust_no', $checkOwner)
                    ->where('type', 'other')
                    ->get();

                    Log::info('ownercount');
                    Log::info(count($owner));
                }
                $row->owner = $owner;

                ////////////////////分隔線
                $cartypeinfo = "";
                $custimport = DB::table('bscode')
                ->where('cd_type', 'IMPORTCARTYPE')
                ->where('cd', $request->car_type)
                ->first();
                if(isset($custimport)) {
                    if($custimport->cd_descp == "貨櫃") {
                        $cartypeinfo = $custimport->cd_descp;
                    } else {
                        $cartypeinfo = mb_substr($custimport->cd_descp ,0,1);
                    }
                }
                $totalcount = DB::table('mod_mark')
                ->where('wh_no', 'like', '%'.substr($row->dc_id,0,2).'%')
                ->where('is_enabled', 'Y')
                ->where('type', '!=', 'code4')
                ->where('parked_models_name', 'like', '%'.$request->car_type.'%')
                ->count();
        
                if(isset($custimport->cd_descp)) {
                    $mainWh = DB::table('mod_mark_reserve')
                    ->select('full_date')
                    ->where('dc_id', $row->dc_id)
                    ->where('reserve_date', $now)
                    ->where('onsite','N')
                    ->where('car_size',$custimport->cd_descp)
                    ->groupBy('full_date')
                    ->orderBy('full_date')
                    ->get();
                    foreach ($mainWh as $whkey => $rowwh) {
                  
                        $next = DB::table('mod_mark_reserve')
                        ->where('dc_id', $row->dc_id)
                        ->where('car_size',$custimport->cd_descp)
                        ->where('reserve_date', $now)
                        ->where('full_date', '>',  $rowwh->full_date)
                        ->first();
            
                        if(isset($next) && $whkey > 0) {
                            $detailreserve = DB::table('mod_mark_reserve')
                            ->where('dc_id', $row->dc_id)
                            ->where('reserve_date', $now)
                            ->where('end_at' ,'>', $rowwh->full_date)
                            ->where('end_at' ,'<', $next->end_at)
                            ->where('car_size',$custimport->cd_descp)
                            ->where('onsite' , 'N')
                            ->count();
                        } else {
                            $detailreserve = DB::table('mod_mark_reserve')
                            ->where('dc_id', $row->dc_id)
                            ->where('reserve_date', $now)
                            ->where('full_date',  $rowwh->full_date)
                            ->where('car_size',$custimport->cd_descp)
                            ->where('onsite' , 'N')
                            ->count();
                        }
                        $rowwh->reservecount = $detailreserve;
                        $rowwh->totalcount = $totalcount;
                        $rowwh->full_date = substr(str_replace($now.' ', '', $rowwh->full_date),0,5);
                    }
                    $row->marknew = $mainWh;
                } else {
                    $row->marknew = null;
                }
            }

            $userOwnerGroup = explode(',',$user->check_owner);

            $owner = DB::table('sys_customers')
            ->select('cust_no',DB::raw("cname as cust_name"))
            ->where('type', 'other')
            ->get();
            if($user->check_owner !="*") {
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();
                $owner = DB::table('sys_customers')
                ->select('cust_no',DB::raw("cname as cust_name"))
                ->whereIn('cust_no', $checkOwner)
                ->where('type', 'other')
                ->get();
            }

            $container = DB::table('bscode')
            ->select('cd','cd_descp')
            ->where('cd_type', 'CONTAINER')
            ->get();
            
            $unloading = DB::table('bscode')
            ->select('cd','cd_descp')
            ->where('cd_type', 'UNLOADTYPE')
            ->get();
            
            $worktype = DB::table('bscode')
            ->select('cd','cd_descp')
            ->where('cd_type', 'WORKTYPE')
            ->get();

            $cartrader = DB::table('mod_car_trader')
            ->select('cust_no','cust_name')
            ->get();

            $cartype = DB::table('bscode')
            ->select('cd','cd_descp')
            ->where('cd_type', 'CARTYPE')
            ->get();

            $importcartype = DB::table('bscode')
            ->select('cd',
                DB::raw("cd as cd_descp")
            )
            ->where('cd_type', 'IMPORTCARTYPE')
            ->get();

            $data['dc_id']          = $bigwarehouse;
            $data['owner']          = $owner;
            $data['container']      = $container;
            $data['unloading_type'] = $unloading;
            $data['work_type']      = $worktype;
            $data['cartrader']      = $cartrader;
            $data['car_type']       = $cartype;
            $data['importcartype']  = $importcartype;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    
    public function importdischarged(Request $request) {
        Log::info("importdischarged");
        Log::info($request->all());
        try {
            $data = array();
            $checkin = DB::table('mod_driver_checkin')
            ->select('id','job_type','cust_ord_no',
            DB::raw("car_type as car_type"),
            DB::raw("con_type_desc as con_type"),
            'phone','driver_name',
            'con_id','unloading_type',
            'box_num','items_num','remark','owner_cd','owner_nm',
            'mark_no','car_no','button_status')
            ->whereNull('discharged_status')
            ->where('job_type', '!=', '出貨')
            ->Where(function($query) {
                $query->whereIn('job_type', ['拉空櫃','換櫃','進貨'])
                ->where('mark_no','!=','空地')
                ->whereNotNull('mark_no')
                ->orWhereNull('discharged_status');
            })
            ->get();
            // ,'button_status'
            // id 作業類型 客戶進貨單號 車型 司機電話 司機姓名 貨櫃類型 貨櫃號碼 下貨類型 碼頭編號 車號 
            // 先顯示報到資料 
            // 確認下貨 確認放行 
            // 修改資料

            foreach ($checkin as $key => $row) {
                $imglist = DB::table('mod_file')
                ->select('id','empty_img','type_no',
                    DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
                )
                ->where('ref_no',  $row->id)
                ->get();
                $row->img = $imglist;

                $row->mark_no  = $row->job_type == "拉空櫃" ? "拉空櫃" : $row->mark_no ;
                if( empty($row->mark_no) && $row->job_type =="換櫃") {
                    unset($checkin[$key]);
                }
                # code...
                $row->button_status = $row->job_type == "拉空櫃" ? "確認放行" : $row->button_status;
            }

            $optionData = DB::table('bscode')
            ->select('cd','cd_descp')
            ->where('cd_type', 'DISCHARGEDTYPE')
            ->get();

            $data['check_in'] =  $checkin;
            $data['optionData'] =  $optionData;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }

    public function importdischargedbyid(Request $request) {
        Log::info("importdischargedbyid");
        Log::info($request->all());
        try {
            $checkin = DB::table('mod_driver_checkin')
            ->select('id','job_type','cust_ord_no',
            DB::raw("car_type as car_type"),
            DB::raw("con_type_desc as con_type"),
            'phone','driver_name',
            'con_id','unloading_type','owner_cd','owner_nm',
            'box_num','items_num','remark',
            'mark_no','car_no','button_status')
            ->where('id',$request->id)
            ->first();

            $checkin->button_status = $checkin->job_type == "拉空櫃" ? "確認放行" : $checkin->button_status;

            $imglist = DB::table('mod_file')
            ->select('id','empty_img','type_no',
                DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
            )
            ->where('ref_no',  $checkin->id)
            ->get();
            $checkin->img = $imglist;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $checkin]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }



    public function updateimportdischarged(Request $request) {
        Log::info("updateimportdischarged");
        Log::info($request->all());
        $now = date("Y-m-d H:i:s");
        try {
            $checkindata =  DB::table('mod_driver_checkin')
            ->where('id', $request->id)
            ->first();

            $warehouseinfo = DB::table('mod_warehouse')
            ->where('dc_id', $checkindata->dc_id)
            ->first();
            if(!empty($request->con_id)) {
                $checkCon = $this->checkCon($request->con_id);
                if($checkCon == false) {
                    return response()->json(['msg' => '櫃號錯誤請確認', 'code' => '01', 'data'=> NULL]);
                }
            }
            // con_type
            if($request->do_status == "修改資料") {
                DB::table('mod_driver_checkin')
                ->where('id', $request->id)
                ->update([
                    'owner_nm'          => $request->owner_nm,
                    'owner_cd'          => $request->owner_cd,
                    'discharged_status' => $request->cd_descp,
                    'job_type'          => $request->job_type,
                    'cust_ord_no'       => $request->cust_ord_no,
                    'car_type_desc'     => $request->car_type,
                    'phone'             => $request->phone,
                    'driver_name'       => $request->driver_name,
                    'con_type_desc'     => $request->con_type,
                    'con_id'            => $request->con_id,
                    'unloading_type'    => $request->unloading_type,
                    'mark_no'           => $request->mark_no,
                    'box_num'           => $request->box_num,
                    'items_num'         => $request->items_num,
                    'car_no'            => $request->car_no,
                ]);
            } else {
                $status      = "";
                $statusdescp = "";
                // 確認下貨 確認放行 
                // 2023-07-04 excel 292;
                if($request->do_status == "確認下貨") {
                    $status      = "delivery";
                    $statusdescp = "確認放行";
                    DB::table('mod_driver_checkin')
                    ->where('id', $request->id)
                    ->update([
                        'owner_nm'           => $request->owner_nm,
                        'owner_cd'           => $request->owner_cd,
                        'entry_time'         => $now,
                        'job_type'           => $request->job_type,
                        'cust_ord_no'        => $request->cust_ord_no,
                        'car_type_desc'      => $request->car_type,
                        'phone'              => $request->phone,
                        'driver_name'        => $request->driver_name,
                        'con_type_desc'      => $request->con_type,
                        'con_id'             => $request->con_id,
                        'unloading_type'     => $request->unloading_type,
                        'mark_no'            => $request->mark_no,
                        'car_no'             => $request->car_no,
                        'button_status'      => $statusdescp,
                        'box_num'            => $request->box_num,
                        'items_num'          => $request->items_num,
                        'import_unload_time' => $now,//開始下貨時間
                    ]);
                } else {
                    $statusdescp = "已放行";
                    // 空車頭放行特別紀錄
                    // discharged_status

                    if($checkindata->job_type == '拉空櫃' && empty($request->con_id) ) {
                        Log::info('請輸入櫃號');
                        return response()->json(['msg' => '請輸入櫃號', 'code' => '01', 'data'=> NULL]);
                    }
                    if(($checkindata->job_type == '換櫃' || $request->job_type == '換櫃') && $request->con_id == $checkindata->con_id) {
                        Log::info('換櫃櫃號不可相同');
                        return response()->json(['msg' => '換櫃櫃號不可相同', 'code' => '01', 'data'=> NULL]);
                    }
                    if($checkindata->job_type == '換櫃' || $request->job_type == '換櫃') {
                        DB::table('mod_driver_checkin')
                        ->where('id', $request->id)
                        ->update([
                            'owner_nm'          => $request->owner_nm,
                            'owner_cd'          => $request->owner_cd,
                            'discharged_status' => '('.$request->cd_descp.')',
                            'job_type'          => $request->job_type,
                            'cust_ord_no'       => $request->cust_ord_no,
                            'car_type_desc'     => $request->car_type,
                            'phone'             => $request->phone,
                            'driver_name'       => $request->driver_name,
                            'con_type_desc'     => $request->con_type,
                            'con_id2'           => $request->con_id,
                            'unloading_type'    => $request->unloading_type,
                            'mark_no'           => $request->mark_no,
                            'car_no'            => $request->car_no,
                            'box_num'           => $request->box_num,
                            'items_num'         => $request->items_num,
                            'button_status'     => $statusdescp,
                            'cabinet_flag'      => "Y",
                            'status'            => $warehouseinfo->leave_decision == 'warehouse' ? 'export' : 'import',
                            'status_desc'       => $warehouseinfo->leave_decision == 'warehouse' ? '離場' : '進場',
                            'leave_time'        => $warehouseinfo->leave_decision == 'warehouse' ? Carbon::now()->toDateTimeString() : null
                        ]);
                    } else {
                        DB::table('mod_driver_checkin')
                        ->where('id', $request->id)
                        ->update([
                            'owner_nm'          => $request->owner_nm,
                            'owner_cd'          => $request->owner_cd,
                            'discharged_status' => '('.$request->cd_descp.')',
                            'job_type'          => $request->job_type,
                            'cust_ord_no'       => $request->cust_ord_no,
                            'car_type_desc'     => $request->car_type,
                            'phone'             => $request->phone,
                            'driver_name'       => $request->driver_name,
                            'con_type_desc'     => $request->con_type,
                            'con_id'            => $request->con_id,
                            'unloading_type'    => $request->unloading_type,
                            'mark_no'           => $request->mark_no,
                            'car_no'            => $request->car_no,
                            'box_num'           => $request->box_num,
                            'items_num'         => $request->items_num,
                            'button_status'     => $statusdescp,
                            'finish_time'       => Carbon::now()->toDateTimeString(), //下貨完成時間
                            'cabinet_flag'      => $request->cd_descp == "空車頭放行" ? "Y" : 'N',
                            'status'            => $warehouseinfo->leave_decision == 'warehouse' ? 'export' : 'import',
                            'status_desc'       => $warehouseinfo->leave_decision == 'warehouse' ? '離場' : '進場',
                            'leave_time'        => $warehouseinfo->leave_decision == 'warehouse' ? Carbon::now()->toDateTimeString() : null
                        ]);

                    }
                    // 警衛室放行資料
                    if ($warehouseinfo->leave_decision == 'guard') {
                        $this->wharfReserveService->sendCheckCarNoByAppByWebsocket(
                            array(
                                'car_no'            => $request->car_no,
                                'discharged_status' => '('.$request->cd_descp.')',
                                'dc_id'             => isset($checkindata->dc_id) ? $checkindata->dc_id : NULL,
                                'can_go'            => 'Y'
                            )
                        );
                    }
                }

            }


            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> NULL]);
        }
    }
    public function assigncheckincar(Request $request) {
        Log::info("getassigncheckincar");
        Log::info($request->all());
        $user = Auth::user();
        try {
            //
            $now  = date("Y-m-d 00:00:00");
            $data = DB::table('mod_driver_checkin')
            ->select('id','mark_no','car_no','driver_name','dc_id','discharged_status','leave_time')
            ->where('created_at', '>=', $now )
            ->whereNull('leave_time')
            ->whereIn('job_type', ['拉空櫃','換櫃','進貨'])
            ->Where(function($query) {
                $query->where('mark_no','!=','空地')
                ->orWhereNull('discharged_status');
            })
            ->get();
            //
            foreach ($data as $key => $row) {
                $row->status = empty($row->mark_no) ? "已報到" :"下貨中";
                if($row->discharged_status == '(空櫃檢查)') {
                    $row->mark_no = "拉空櫃";
                    $row->status  = empty($row->leave_time) ? '已報到' : "已離場";
                }
                
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    public function assigncheckincardetail(Request $request) {
        Log::info("assigncheckincardetail");
        Log::info($request->all());
        $user = Auth::user();
        try {
            //
            $data       = array();
            $min        = (int)date("i");
            $hour       = date("H");
            $day        = date("Y-m-d");
            $data       = array();
            $reserveMin = (int)($min/15) *15;

            $detail = DB::table('mod_driver_checkin')
            ->select('id','car_no','driver_name')
            ->where('id',$request->id)
            ->first();

            $bigwarehouse = DB::table('mod_warehouse')
            ->select('dc_id','cust_no','cname')
            ->where('cust_no',$detail->dc_id)
            ->get();

            foreach ($bigwarehouse as $key => $row) {
                $porcess = DB::table('mod_mark_reserve')
                ->select('id','car_no','reserve_h', DB::raw("null as reserve_m"))
                ->where('dc_id', $request->dc_id)
                ->where('wh_no', $row->cust_no)
                ->where('reserve_date', $day)
                ->where('reserve_h', $hour)
                ->where('reserve_m', $reserveMin)
                ->first();

                $next = DB::table('mod_mark_reserve')
                ->select('id','car_no', 'reserve_h', 'reserve_m')
                ->where('dc_id', $request->dc_id)
                ->where('wh_no', $row->cust_no)
                ->where('reserve_date', $day)
                ->where('reserve_h', $hour)
                ->where('reserve_m','>=', $reserveMin)
                ->first();

                $row->process = $porcess;
                $row->next = $next;
                $detail->dc_id = $row->dc_id;
            }
            $data['detail'] = $detail;
            $data['wh_list'] = $bigwarehouse;

            //

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    public function assigncheckincarwh(Request $request) {
        Log::info("assigncheckincarwh");
        Log::info($request->all());
        $user = Auth::user();
        try {
            //
            $checkindata = DB::table('mod_driver_checkin')
            ->where('id',$request->id)
            ->first();

            //檢查是否所選倉庫是否已有車輛
            $checkfull =  DB::table('mod_mark')
            ->where('cust_no', $request->cust_no)
            ->where('cust_name', $request->cname)
            ->where('in_use','Y')
            ->first();
            if(isset($checkfull)) {
                return response()->json(['msg' => '此碼頭目前已有車輛', 'code' => '01', 'data'=> NULL]);
            }

            //先將司機目前碼頭使用中清除
            $ogdrivermark = DB::table('mod_driver_checkin')
            ->where('id',$request->id)
            ->first();
     
            if($request->cust_no == '空地') {
                DB::table('mod_mark')
                ->where('cust_no', $ogdrivermark->mark_no)
                ->update([
                    'in_use'=>'N' 
                 ]);
            }

            if(!empty($ogdrivermark->mark_no)) {
                DB::table('mod_mark')
                ->where('cust_no', $ogdrivermark->mark_no)
                ->update([
                    'in_use'=>'N' 
                 ]);
            } else {
                // 第一次分派的車輛，送警衛室場內車輛
                $this->wharfReserveService->sendCheckCarNoByAppByWebsocket(
                    array(
                        'car_no'         => $checkindata->car_no,
                        'dc_id'          => isset($checkindata->dc_id) ? $checkindata->dc_id : NULL,
                        'can_go'         => 'N'
                    )
                );
            }

            //將新選碼頭鎖定
            DB::table('mod_mark')
            ->where('cust_no', $request->cust_no)
            ->update([
               'in_use'=>'Y' 
            ]);

            DB::table('mod_driver_checkin')
            ->where('id',$request->id)
            ->update([
                'wh_no'     => $request->wh_no,
                'mark_no'   => $request->cust_no,
                'mark_name' => $request->cust_no
            ]);

            DB::table('mod_dss_dlv')
            ->where('driver_phone',$checkindata->phone)
            ->update([
                'mark_no'   => $request->cust_no,
            ]);

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    public function markreserve(Request $request) {
        Log::info("markreserve");
        Log::info($request->all());
        $user = Auth::user();
        $now = date("Y-m-d H:i:s");
        try {
            // APP- 碼頭管理 - 碼頭預約那邊的必填欄位
            // 貨櫃類型 下貨類型 車型 貨櫃號碼 非必填，目前有擋必填
            if(!empty($request->con_type) && empty($request->con_no) ) {
                Log::info('請輸入貨櫃號碼');
                return response()->json(['msg' => '請輸入貨櫃號碼', 'code' => '01', 'data'=> NULL]);
            }
            if(empty($request->box_num) && $request->work_type != "拉空櫃" ) {
                Log::info('請輸入箱數');
                return response()->json(['msg' => '請輸入箱數', 'code' => '01', 'data'=> NULL]);
            }
            if(empty($request->truck_cmp_name)) {
                Log::info('請輸入車商');
                return response()->json(['msg' => '請輸入車商', 'code' => '01', 'data'=> NULL]);
            }
            if(empty($request->work_type)) {
                Log::info('請輸入作業類型');
                return response()->json(['msg' => '請輸入作業類型', 'code' => '01', 'data'=> NULL]);
            }
            $data = $this->wharfReserveService->create($request);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> NULL]);
        }

        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);

    }
    public function updatemarkreserve(Request $request) {
        Log::info("updatemarkreserve");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $now = date("Y-m-d H:i:s");

            // $sameTimeCcount = DB::table('mod_mark_reserve')
            // ->where('dc_id',$request->dc_id)
            // ->where('reserve_date',$request->reserve_date)
            // ->where('reserve_h',$request->reserve_h)
            // ->where('reserve_m',$request->reserve_m)
            // ->count();
            // $totalCount = DB::table('mod_mark')
            // ->where('wh_no',$request->dc_id)->where('is_enabled','Y')
            // ->where('tyPe','!=','code4')->count();
            // $wharfReserve = $totalCount - $sameTimeCcount;

            // if ($wharfReserve <= 0) {
            //     return response()->json(['msg' => '相同時段預約已滿', 'code' => '01', 'data'=> NULL]);
            // }
            $ogtime = DB::table('mod_mark_reserve')
            ->where('id',$request->id)
            ->first();
            $newfull = $request->reserve_date.' '.$request->reserve_h.':'.$request->reserve_m.':00';
            $newend  = date('Y-m-d H:i:00',strtotime($newfull.' + '. $ogtime->process_time.' minute'));
            DB::table('mod_mark_reserve')
            ->where('id',$request->id)
            ->update([
                'dc_id'           => $request->dc_id,
                'dc_name'         => $request->dc_name,
                'reserve_date'    => $request->reserve_date,
                'reserve_h'       => $request->reserve_h,
                'reserve_m'       => $request->reserve_m,
                'owner_no'        => $request->owner_no,
                'owner_name'      => $request->owner_name,
                'con_type'        => $request->con_type,
                'con_type_desc'   => $request->con_type_desc,
                'con_no'          => $request->con_no,
                'unloading_type'  => $request->unloading_type,
                'unloading_descp' => $request->unloading_descp,
                'work_type'       => $request->work_type,
                'work_descp'      => $request->work_descp,
                'cust_import_no'  => $request->cust_import_no,
                'car_no'          => $request->car_no,
                'car_type'        => $request->car_type,
                'car_type_descp'  => $request->car_type_descp,
                'truck_cmp_no'    => $request->truck_cmp_no,
                'truck_cmp_name'  => $request->truck_cmp_name,
                'box_num'         => $request->box_num,
                'items_num'       => $request->items_num,
                'driver_name'     => $request->driver_name,
                'driver_phone'    => $request->driver_phone,
                'updated_by'      => $user->email,
                'updated_at'      => $now,
                'full_date'       => $newfull,
                'end_at'          => $newend
            ]);
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }

    public function markreservesearch (Request $request) {
        Log::info("markreservesearch");
        Log::info($request->all());
        //
        $today = date("Y-m-d");
        $user = Auth::user();
        try {
            $this_query = null;
            $this_query = DB::table('mod_mark_reserve');
            $this_query->select('*');
            if($request->dc_id != null) {
                $this_query->where('dc_id',$request->dc_id);
            }
            if($request->date != null) {
                $this_query->where('reserve_date', $request->date);
            } else {
                $this_query->where('reserve_date','>=', $today);
            }
            $this_query->where('onsite', 'N');
            $result = $this_query->get();
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $result]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }

    public function markusestatus (Request $request) {
        Log::info("markusestatus");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $mark = DB::table('mod_mark')
            ->select('cust_no','cust_name','type','wh_name','wh_no')
            ->where('is_enabled','Y')
            ->where('wh_no', 'like', '%'.substr($request->dc_id,0,2).'%')
            ->get();
            foreach ($mark as $markkey => $markrow) {

                $firstmarkreserve = DB::table('mod_driver_checkin')
                ->select('car_no', DB::raw(" '目前時間' as time"),'process_time' ,'entry_time','con_id','job_type')
                ->where('dc_id', $request->dc_id)
                ->where('mark_no', $markrow->cust_no)
                ->whereNull('leave_time')
                ->Where(function($query) {
                    $query->whereIn('job_type', ['拉空櫃','換櫃','進貨','出貨'])
                    ->where('mark_no', '!=', '空地')
                    ->orWhereNull('discharged_status');
                })
                ->orderBy('created_at','desc')
                ->first();

                $formateEntry_time = '';
                if(isset($firstmarkreserve)) {
                    $formateEntry_time = date("H:i", strtotime($firstmarkreserve->entry_time));
                }

                $markrow->title        = '車號';
                $markrow->car_no       = isset($firstmarkreserve->car_no) ? $firstmarkreserve->car_no :'';

                $markrow->time         = isset($firstmarkreserve->entry_time) ? $formateEntry_time :'';
                $markrow->process_time = empty($firstmarkreserve->process_time) ? '' : $firstmarkreserve->process_time;

                // $markrow->test = $firstmarkreserve;
                // $markrow->cust_name = $markrow->wh_name.'-'.$markrow->cust_name;
                if($markrow->type == "code4" ) {
                    $markrow->show_name = $markrow->wh_name.'-'.$markrow->cust_name.'(備用)';
                } else {
                    $markrow->show_name = $markrow->wh_name.'-'.$markrow->cust_name;
                }

            }
            $mark[]  = array(
                "wh_no"        => "",
                "cust_no"      => "空地",
                "cust_name"    => "空地",
                "show_name"    => "空地",
                "type"         => "999",
                "car_no"       => "",
                "time"         => "",
                "process_time" => ""
            );

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $mark]);


        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> $e->getMessage()]);
        }
    }

    public function getcartrader(Request $request) {
        Log::info("getcartrader");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $cartrader = DB::table('mod_car_trader')
            ->select(
                'cust_no','cust_name'
            )
            ->get();

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $cartrader]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }

    public function getowner(Request $request) {
        Log::info("getowner");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $owner = DB::table('sys_customers')
            ->select(
                'cust_no',DB::raw("cname as cust_name")
            )
            ->where('type', 'other')
            ->get();

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $owner]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL]);
        }
    }
    public function getorderdetail(Request $request) {
        Log::info("getorderdetail");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $order = DB::table('mod_order')
            ->where('cust_ord_no', $request->ord_no)
            ->first();
            $data = DB::table('mod_order_detail')
            ->select('ord_no','sn_no','sku_no','sku_desc','sku_spec','quantity','unit','transfer_mq','batch_no','expiry_date')
            ->where('ord_id', $order->id)
            ->get();
            Log::info($data);
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL,'errorline'=>$e->getLine(),'errormsg'=>$e->getMessage()]);
        }
    }
    public function getdrivermission(Request $request) {
        Log::info("getdrivermission");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $finishArray = array();
            $dlvArray = array();
            //正式啟用的時候須把日期條件拿掉
            $limitday = DB::table('bscode')
            ->where('cd_type', 'DRIVERLOGINDAY')
            ->where('cd', 'driverloginday')
            ->value('value1');
            $days = date("Y-m-d ", strtotime("-".$limitday." day"));
            $warehouseAddress = array();
            $allwarehouse =  DB::table('mod_warehouse')
            ->groupBy('address')
            ->pluck('address')
            ->toArray();

            foreach ($allwarehouse as $index => $addressrow) {
                $warehouseAddress[$addressrow] = $addressrow;
            }
            $mission = DB::table('mod_dss_dlv')
            ->select(
                'id','sys_dlv_no','dlv_no',
                'dlv_date','dlv_type','dlv_type_desc',
                'car_type','car_type_name',
                'truck_cmp_no','truck_cmp_name',
                'car_no','driver_nm','driver_phone',
                'total_cbm','total_gw','total_box_num',
                'mark_no','dlv_point','status','status_desc',
                DB::raw(" 'N' as only_inverse"),
                DB::raw("pallet_num as total_pallet")
            )
            ->where('driver_phone', $user->email)
            ->where('dlv_date', '>=', $days)
            ->orderBy('created_at','asc')
            ->get();
            Log::info($days);
            Log::info('mission finish');
            foreach ($mission as $key => $row) {
                # code...hava
                $missiondetail = DB::table('mod_dlv_plan')
                ->select(
                    'id','status','status_desc','sys_dlv_no','dlv_no','trs_mode',
                    'trs_mode_desc','dlv_ord_no','owner_cd','lock_pallet',
                    'owner_name','ord_no','cust_name','cust_zip','dlv_no_seq',
                    'cust_address','cbm','gw','box_num','have_detail',DB::raw(" '' as ord_remark"),
                    'wms_ord_no','order_remark','dsv_remark','button_status','button_status_desc','fromwh',
                    'dlv_to_wh','error_cd','error_name','error_remark','remark','high_risk','backdelivery', 'twice_checkin'
                )
                ->where('sys_dlv_no', $row->sys_dlv_no)
                ->orderBy('dlv_no_seq','asc')
                ->orderBy('trs_mode','desc')
                ->orderBy('dlv_no','desc')
                ->orderBy('ord_no','desc')
                ->get();
                Log::info('detail finish');
                // Log::info('紀錄判斷');
                $custNameArray = array();
                foreach ($missiondetail as $key => $foraddressrow) {
                    $custNameArray[] = $foraddressrow->owner_name;
                }
                array_unique($custNameArray);
                // $userhave = DB::table('mod_pallet_manage')
                // ->where('type_cd', 'driver')
                // ->where('dlv_no', $row->dlv_no)
                // ->where('pallet_num', '>' , 0)
                // ->groupBy('pallet_code')
                // ->pluck('pallet_code')->toArray();

                $custhave = DB::table('mod_pallet_manage')
                ->whereIn('cust_name', $custNameArray)
                ->where('type_cd', 'cust')
                ->where('pallet_num', '>' , 0)
                ->groupBy('pallet_code')
                ->pluck('pallet_code')->toArray();
                $canback = array_merge( array(), $custhave);
                
                $palletback = DB::table('mod_pallet')
                ->select(DB::raw(" pallet_no as pallet_code"),'pallet_name')
                ->whereIn('pallet_no', $canback)
                ->get();
                $palletperss = DB::table('mod_pallet_manage')
                ->select(DB::raw(" pallet_type as pallet_name"),'pallet_code')
                ->where('type_cd', 'driver')
                ->where('pallet_num', '>' , 0)
                ->where('owner_by', $user->email)
                ->where('dlv_no', $row->dlv_no)
                ->groupBy('pallet_code')
                ->get();
                Log::info('other finish');
                foreach ($missiondetail as $detailkey => $detailrow) {

                    if(strlen($detailrow->owner_name) > 4) {
                        $detailrow->show_ord_no = mb_substr($detailrow->owner_name,0,4).'-'.$detailrow->ord_no;
                    } else {
                        $detailrow->show_ord_no = $detailrow->owner_name.'-'.$detailrow->ord_no;
                    }

                    //有判斷倉庫地址就是非純逆物流
                    if( isset($warehouseAddress[$detailrow->cust_address]) && $detailrow->trs_mode == "pick") {
                        $row->only_inverse = "Y";
                    }
                    if($detailrow->error_cd ) {
                        $historyError = DB::table('mod_error_report')
                        ->where('wms_ord_no', $detailrow->wms_ord_no)
                        ->orderBy('id','desc')
                        ->get();
                        $detailrow->error_history = $historyError ;
                    } else {
                        $detailrow->error_history = [] ;
                    }
                    $detailrow->pallet_back  = $palletback;
                    $detailrow->pallet_press  = $palletperss;

                }


                $row->total_pallet = empty( $row->total_pallet) ? 0 : $row->total_pallet;

                $row->detail  = $missiondetail;

                $row->status = $row->only_inverse == "N" && $row->status!='FINISHED' ? "DLV" : $row->status;
                
                // $row->total_pallet = $totalnum;
                if($row->status == "FINISHED" ) {
                    $finishArray[] =  $row;
                } else {
                    $dlvArray[] =  $row;
                }
            }
            Log::info('all finish');
            $data = array();
            $data['FINISHED'] = $finishArray ;
            $data['DLV'] = $dlvArray;

            Log::info('finish return getdrivermission');
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL,'errorline'=>$e->getLine(),'errormsg'=>$e->getMessage()]);
        }
    }
    public function checkinInfo(Request $request) {
        Log::info("checkinInfo");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $mission = DB::table('mod_dss_dlv')
            ->select(
                'car_no','car_type','car_type_name','truck_cmp_no','truck_cmp_name'
            )
            ->where('driver_phone', $user->email)
            ->orderBy('created_at','desc')
            ->first();

            $missionforcarno = DB::table('mod_dss_dlv')
            ->select(
                'car_no'
            )
            ->where('driver_phone', $user->email)
            ->whereNotNull('car_no')
            ->orderBy('created_at','desc')
            ->first();

            $allwarehouse =  DB::table('mod_warehouse')
            ->whereNull('dc_id')
            ->get();
            $warehouse = array();
            foreach ($allwarehouse as $key => $whrow) {
                $distince =  $this->getdistancestation($whrow->lat,$whrow->lng,$request->lat,$request->lng);
                Log::info($whrow->cname);
                Log::info($distince);
                Log::info($whrow->gps_range);
                if($distince <= $whrow->gps_range) {
                    $warehouse = $whrow;
                }
            }

            $userinfo         = array();

            $userinfo['driver_name']    = $user->name;
            $userinfo['driver_phone']   = $user->email;
            $userinfo['car_no']         = empty($missionforcarno->car_no) ? NULL : $missionforcarno->car_no;
            $userinfo['car_type']       = $mission->car_type;
            $userinfo['car_type_name']  = $mission->car_type_name;
            $userinfo['truck_cmp_no']   = $mission->truck_cmp_no;
            $userinfo['truck_cmp_name'] = $mission->truck_cmp_name;
            $userinfo['wh_no']          = isset($warehouse->cust_no) ? $warehouse->cust_no : NULL;
            $userinfo['wh_name']        = isset($warehouse->cname) ? $warehouse->cname : NULL;
            $userinfo['msg']            = '請確認目前位置';
            $today = date("Y-m-d");
            $checkin = DB::table('mod_driver_checkin')
            ->where('created_at', '>=', $today)
            ->where('phone',  $user->email)
            ->where('delivery_type', '還板放行')
            ->whereNull('leave_time')
            ->orderBy('id','desc')
            ->count();
            $backdelivery = "出貨報到";
            if( $checkin > 0) {
                $backdelivery = '還板報到';
            }

            $userinfo['delivery_type']  = $backdelivery;
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $userinfo]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL ,'errormgs'=>$e->getMessage()]);
        }
    }
    public function updatemissionsort(Request $request) {
        Log::info("updatemissionsort");
        Log::info($request->all());
        $user = Auth::user();
        $postdata = $request->data;

        try {
            foreach ($postdata as $key => $row) {
                # code...
                DB::table('mod_dlv_plan')
                ->where('id',$row['id'])
                ->update([
                    'dlv_no_seq'=>$row['dlv_no_seq']
                ]);
                DB::table('mod_order')
                ->where('ord_no', $row['wms_ord_no'])
                ->update([
                    'dlv_no_seq'=>$row['dlv_no_seq']
                ]);
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL ,'errormgs'=>$e->getMessage()]);
        }


    }
    public function missiondetailById(Request $request) {
        Log::info("missiondetailById");
        Log::info($request->all());
        $user = Auth::user();
        //mod_order_detail
        try {
            $missiondetail = DB::table('mod_dlv_plan')
            ->select(
                'id','status','status_desc','sys_dlv_no','dlv_no','trs_mode',
                'trs_mode_desc','dlv_ord_no','owner_cd',
                'owner_name','ord_no','cust_name','cust_zip','lock_pallet',
                'cust_address','cbm','gw','box_num','have_detail',DB::raw(" '' as ord_remark"),
                'wms_ord_no','order_remark','dsv_remark','button_status','button_status_desc','fromwh',
                'dlv_to_wh','error_cd','error_name','error_remark','remark','high_risk','backdelivery', 'twice_checkin'
            )
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('trs_mode', $request->trs_mode)
            ->where('ord_no', $request->ord_no)
            ->orderBy('dlv_no_seq','asc')
            ->orderBy('trs_mode','desc')
            ->orderBy('dlv_no','desc')
            ->first();


            if(strlen($missiondetail->owner_name) > 4) {
                $missiondetail->show_ord_no = mb_substr($missiondetail->owner_name,0,4).'-'.$missiondetail->ord_no;
            } else {
                $missiondetail->show_ord_no = $missiondetail->owner_name.'-'.$missiondetail->ord_no;
            }

            if($missiondetail->status == 'FINISHED' ) {
                //
                $palletlog =  DB::table('mod_pallet_manage_log')
                ->select ('id' ,'pallet_code', DB::raw(" pallet_type as pallet_name"),'back_num','press_num')
                ->where('dlv_no', $missiondetail->dlv_no)
                ->where('wms_ord_no', $missiondetail->wms_ord_no)
                ->where('is_update', 'N')
                ->where('type_cd', 'driver')
                ->where('trs_mode', $request->trs_mode)
                ->get();
                foreach ($palletlog as $palletlogkey => $palletlogrow) {
                    $palletlogrow->press_num = abs($palletlogrow->press_num);
                }
                $missiondetail->finish_pallet = $palletlog;
                //

            } else {
                $missiondetail->finish_pallet = [];
            }

            if($missiondetail->error_cd ) {
                $historyError = DB::table('mod_error_report')
                ->where('wms_ord_no', $missiondetail->wms_ord_no)
                ->orderBy('id','desc')
                ->get();
                foreach ($historyError as $historyErrorkey => $historyErrorrow) {
                    $img = DB::table('mod_file')
                    ->select('id',
                        DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
                    )
                    ->where('error_id', $historyErrorrow->uuid)
                    ->orderBy('id','desc')
                    ->get();
                    $historyErrorrow->img = $img ;
                }
                $missiondetail->error_history = $historyError ;
            } else {
                $missiondetail->error_history = [] ;
            }

            $upstairs =  DB::table('mod_upstairs')
            ->select ('floor','unit_num')
            ->where('ord_no', $missiondetail->wms_ord_no)
            ->first();

            $downpoint =  DB::table('mod_downpoint')
            ->select ('point')
            ->where('ord_no', $missiondetail->wms_ord_no)
            ->first();

            $missiondetail->upstairs = $upstairs;
            $missiondetail->downpoint = $downpoint;
            $imglist = DB::table('mod_file')
            ->select('id','empty_img','type_no',
                DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
            )
            ->where('trs_mode',  $missiondetail->trs_mode)
            ->where('ref_no',  $missiondetail->wms_ord_no)
            ->get();

            $finishimg = array();
            $detailimg = array();
            $upstairsimg = array();
            $downpointimg = array();
            // $errorimg = array();
            foreach ($imglist as $imgkey => $imgrow) {
                $imgrow->pickdlv_flag ="N";
                if($imgrow->type_no == "upstairs") {
                    $upstairsimg[] = $imgrow;
                } else if($imgrow->type_no == "downpoint") {
                    $downpointimg[] = $imgrow;
                } else if($imgrow->empty_img == "Y") {
                    $finishimg[] = $imgrow;
                } else if($imgrow->type_no == "error") {
                    $errorimg[] = $imgrow;
                } else if($imgrow->empty_img == "signreport") {
                    if($imgrow->type_no == "pickdlv") {
                        $imgrow->pickdlv_flag = "Y";
                    }
                    $detailimg[] = $imgrow;
                }
            }
            $missiondetail->upstairsimg = $upstairsimg;
            $missiondetail->downpointimg = $downpointimg;
            $missiondetail->imglist = $finishimg;
            $missiondetail->detailimglist = $detailimg;
            // $missiondetail->errorimg = $errorimg;
            $userhave = DB::table('mod_pallet_manage')
            ->where('type_cd', 'driver')
            ->where('pallet_num', '>' , 0)
            ->where('dlv_no', $missiondetail->dlv_no)
            ->groupBy('pallet_code')
            ->pluck('pallet_code')->toArray();

            $custhave = DB::table('mod_pallet_manage')
            ->where('type_cd', 'cust')
            ->where('pallet_num', '>' , 0)
            ->where('cust_name', $missiondetail->owner_name)
            ->groupBy('pallet_code')
            ->pluck('pallet_code')->toArray();
            $canback = array_merge( array(), $custhave);
            
            $palletback = DB::table('mod_pallet')
            ->select(DB::raw(" pallet_no as pallet_code"),'pallet_name')
            ->whereIn('pallet_no', $canback)
            ->get();
            $missiondetail->pallet_back  = $palletback;

            $palletperss = DB::table('mod_pallet_manage')
            ->select(DB::raw(" pallet_type as pallet_name"),'pallet_code')
            ->where('dlv_no',$missiondetail->dlv_no)
            ->where('pallet_num', '>' , 0)
            ->where('type_cd', 'driver')
            ->where('owner_by', $user->email)
            ->groupBy('pallet_code')
            ->get();

            $missiondetail->pallet_press  = $palletperss;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $missiondetail]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL ,'errormgs'=>$e->getMessage()]);
        }
    }
    public function missiondetail(Request $request) {
        Log::info("missiondetail");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $missiondetailnew = array();
            $missiondetail = DB::table('mod_dlv_plan')
            ->select(
                'id','status','status_desc','sys_dlv_no','dlv_no','trs_mode',
                'trs_mode_desc','dlv_ord_no','owner_cd','lock_pallet',
                'owner_name','ord_no','cust_name','cust_zip','dlv_no_seq',
                'cust_address','cbm','gw','box_num','have_detail',DB::raw(" '' as ord_remark"),
                'wms_ord_no','order_remark','dsv_remark','button_status','button_status_desc','fromwh',
                'dlv_to_wh','error_cd','error_name','error_remark','remark','high_risk','backdelivery', 'twice_checkin'
            )
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->orderBy('dlv_no_seq','asc')
            ->orderBy('trs_mode','desc')
            ->orderBy('dlv_no','desc')
            ->orderBy('ord_no','desc')
            ->get();

            $dlvNo = "";
            $wmsOrdNoArray = array();
            $custNameArray = array();
            $imgListMap = array();
            $palletlogListMap = array();

            foreach ($missiondetail as $key => $row) {
                $dlvNo = $row->dlv_no;
                array_push($wmsOrdNoArray, $row->wms_ord_no);
                array_push($custNameArray, $row->cust_name);
            }
            array_unique($wmsOrdNoArray);
            array_unique($custNameArray);

            $imglist = DB::table('mod_file')
                ->select('id','empty_img','type_no', 'trs_mode', 'ref_no', 'error_id',
                    DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
                )
                ->whereIn('ref_no',  $wmsOrdNoArray)
                ->get();
            foreach ($imglist as $img) {
                if (empty($imgListMap[$img->trs_mode.'_'.$img->ref_no])) {
                    $imgListMap[$img->trs_mode.'_'.$img->ref_no] = array();
                }
                array_push($imgListMap[$img->trs_mode.'_'.$img->ref_no], $img);
            }
    
            $palletlog =  DB::table('mod_pallet_manage_log')
            ->select ('id' ,'pallet_code', DB::raw(" pallet_type as pallet_name"),'back_num','press_num', 'wms_ord_no')
            ->where('dlv_no', $dlvNo)
            ->where('is_update', 'N')
            ->where('type_cd', 'driver')
            ->get();
            foreach ($palletlog as $log) {
                if (empty($palletlogListMap[$log->wms_ord_no])) {
                    $palletlogListMap[$log->wms_ord_no] = array();
                }
                array_push($palletlogListMap[$log->wms_ord_no], $log);
            }

            // $userhave = DB::table('mod_pallet_manage')
            //     ->where('pallet_num', '>' , 0)
            //     ->where('type_cd', 'driver')
            //     ->where('dlv_no', $dlvNo)
            //     ->groupBy('pallet_code')
            //     ->pluck('pallet_code')->toArray();

            $custhaveAll = DB::table('mod_pallet_manage')
                ->where('pallet_num', '>' , 0)
                ->where('type_cd', 'cust')
                ->whereIn('cust_name', $custNameArray)
                ->groupBy('pallet_code')
                ->get(); 
                

            $palletperss = DB::table('mod_pallet_manage')
                ->select(DB::raw(" pallet_type as pallet_name"),'pallet_code')
                ->where('dlv_no', $dlvNo)
                ->where('pallet_num', '>' , 0)
                ->where('type_cd', 'driver')
                ->where('owner_by', $user->email)
                ->groupBy('pallet_code')
                ->get();

            foreach ($missiondetail as $key => $row) {
                if(strlen($row->owner_name) > 4) {
                    $row->show_ord_no = mb_substr($row->owner_name,0,4).'-'.$row->ord_no;
                } else {
                    $row->show_ord_no = $row->owner_name.'-'.$row->ord_no;
                }

                if ($row->error_cd) {
                    $historyError = DB::table('mod_error_report')
                    ->where('wms_ord_no', $row->wms_ord_no)
                    ->where('trs_mode', $row->trs_mode)
                    ->orderBy('id','desc')
                    ->get();

                    foreach ($historyError as $historyErrorkey => $historyErrorrow) {
                        $img = array();
                        if (isset($imgListMap[$row->trs_mode.'_'.$row->wms_ord_no])) {
                            foreach ($imgListMap[$row->trs_mode.'_'.$row->wms_ord_no] as $item) {
                                if ($item->error_id == $historyErrorrow->uuid) {
                                    array_push($img, $item);
                                }
                            }
                        }
                        $img = collect($img)->sortBy('id')->reverse()->values();
                        $historyErrorrow->img = $img;
                    }

                    $row->error_history = $historyError;
                } else {
                    $row->error_history = [] ;
                }
    
                //混合單判斷
                if($row->backdelivery == "Y" && $row->trs_mode=="pick" 
                && $row->button_status != "STARTUPLOADING" && $row->button_status != "PICKFINISHED"
                && $row->status != "FINISHED"  && $row->status != "CLOSED"  ) {
                    Log::info('混合單判斷');
                    Log::info($row->id);
                    $row->button_status      = "ARRIVE";
                    $row->button_status_desc = "到點";
                }

                //純逆物流
                if($row->backdelivery == "Y" && $row->trs_mode=="dlv" &&$row->dlv_to_wh=="Y" && $row->button_status_desc=="到點") {
                    $detail = DB::table('mod_dlv_plan')
                    ->where('trs_mode', 'pick')
                    ->where('wms_ord_no', $row->wms_ord_no)
                    ->where('dlv_no', $row->dlv_no)
                    ->first();
                    if($detail->button_status == "FINISHED" ) {
                        Log::info('enter change');
                        $row->button_status      = "STARTUNLOADING";
                        $row->button_status_desc = "開始下貨";
                    }
                }

                if($row->status == 'FINISHED' ||  $row->status == 'CLOSED') {
                    if (isset($palletlogListMap[$row->wms_ord_no])) {
                        foreach ($palletlogListMap[$row->wms_ord_no] as $palletlogrow) {
                            $palletlogrow->press_num = abs($palletlogrow->press_num);
                        }
                        $row->finish_pallet = $palletlogListMap[$row->wms_ord_no];
                    } else {
                        $row->finish_pallet = [];
                    }
                    //

                } else {
                    $row->finish_pallet = [];
                }          
                $upstairs =  DB::table('mod_upstairs')
                ->select ('floor','unit_num')
                ->where('ord_no', $row->wms_ord_no)
                ->first();

                $downpoint =  DB::table('mod_downpoint')
                ->select ('point')
                ->where('ord_no', $row->wms_ord_no)
                ->first();

                $row->upstairs = $upstairs;
                $row->downpoint = $downpoint;

                $finishimg = array();
                $detailimg = array();
                $upstairsimg = array();
                $downpointimg = array();
                $errorimg = array();

                if (isset($imgListMap[$row->trs_mode.'_'.$row->wms_ord_no])) {
                    foreach ($imgListMap[$row->trs_mode.'_'.$row->wms_ord_no] as $imgkey => $imgrow) {
                        $imgrow->pickdlv_flag ="N";
                        if($imgrow->type_no == "upstairs") {
                            $upstairsimg[] = $imgrow;
                        } else if($imgrow->type_no == "downpoint") {
                            $downpointimg[] = $imgrow;
                        } else if($imgrow->empty_img == "Y") {
                            $finishimg[] = $imgrow;
                        } else if($imgrow->type_no == "error") {
                            $errorimg[] = $imgrow;
                        } else if($imgrow->empty_img == "signreport") {
                            if($imgrow->type_no == "pickdlv") {
                                $imgrow->pickdlv_flag = "Y";
                            }
                            $detailimg[] = $imgrow;
                        }
                    }
                }

                $row->upstairsimg   = $upstairsimg;
                $row->downpointimg  = $downpointimg;
                $row->imglist       = $finishimg;
                $row->detailimglist = $detailimg;
                $row->errorimg      = $errorimg;

                $custhave = array();
                foreach ($custhaveAll as $item) {
                    if ($item->cust_name == $row->cust_name) {
                        array_push($custhave, $item->pallet_code);
                    }
                }

                $canback = array_merge( array(), $custhave);
                
                $palletback = DB::table('mod_pallet')
                ->select(DB::raw(" pallet_no as pallet_code"),'pallet_name')
                ->whereIn('pallet_no', $canback)
                ->get();

                $row->pallet_back  = $palletback;


                $row->pallet_press  = $palletperss;
                $missiondetailnew[] = $row;
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $missiondetailnew]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> NULL ,'errormgs'=>$e->getMessage()]);
        }
    }
    public function getpallet(Request $request) {
        Log::info("getpallet");
        Log::info($request->all());
        $user = Auth::user();
        try {        
            $pallet = DB::table('mod_pallet')
            ->select(
                DB::raw("pallet_no as pallet_code"),'pallet_name'
            )
            ->get();
            
            

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $pallet]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }
    }

    public function orderpallet(Request $request) {
        Log::info("orderpallet");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $now = date("Y-m-d H:i:s");
            $orderData = DB::table('mod_dlv_order')
            ->where('cust_dlv_no', $request->cust_dlv_no)
            ->where('cust_name', $request->cust_name)
            ->first();

            if(isset($orderData)) {
                //clear data
                DB::table('mod_order_pallet')
                ->where('ord_id', $orderData->id)
                ->delete();

                $palletdatas = $request->palletdatas;

                $insertData = array();
                foreach ($palletdatas as $key => $palletdata) {
                    # code...
                    $data = [
                        'ord_id'      => $orderData->id,
                        'pallet_num'  => $palletdata['pallet_num'],
                        'pallet_code' => $palletdata['pallet_no'],
                        'pallet_type' => $palletdata['pallet_name'],
                        'g_key'       => $orderData->g_key,
                        'c_key'       => $orderData->c_key,
                        's_key'       => $orderData->s_key,
                        'd_key'       => $orderData->d_key,
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => $now,
                        'updated_at'  => $now,
                    ];
                    
                    array_push($insertData, $data);
                }
               
                DB::table('mod_order_pallet')->insert($insertData);
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

    }

    public function belongdriverpallet(Request $request) {
        Log::info("belongdriverpallet");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $data = array();
            $driverData = DB::table('users')
            ->where('email',$request->phone)
            ->first();

            $checkWarehouse = explode(',',$user->check_wh);

            $warehousedata = DB::table('mod_warehouse')
            ->select(
                DB::raw(' cust_no as wh_no'),
                DB::raw(' cname as wh_name')
            )
            ->whereIn('cust_no',$checkWarehouse)
            ->get();
            
            $totalpallet = 0;
            $warehousearray = array();
            foreach ($warehousedata as $warehouskey => $warehousrow) {
                # code...
                $palltetdata = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw(' SUM(num_from_wh) as pallet_num')
                    ,'pallet_code','wh_no',
                    DB::raw(' pallet_type as pallet_name'),
                    DB::raw(' any_value(wh_name) as wh_name'),
                    'owner_by')
                ->where('type_cd','driver' )
                ->where('wh_no', $warehousrow->wh_no)
                ->where('owner_by',$request->phone)
                ->where('pallet_num','>', 0)
                ->where('ready_go','Y')
                ->groupBy('pallet_code','pallet_type','owner_by','wh_no')
                ->get();
                
                $warehousrow->pallets = $palltetdata ;
                foreach ($palltetdata as $palletkey => $row) {
                    $row->pallet_num  = (int)abs($row->pallet_num);
                }
                if(count($palltetdata) != 0 ){
                    $warehousearray[] = $warehousrow;
                }

            }
            $totalpallet = DB::table('mod_pallet_manage')
            ->select(
                DB::raw(' SUM(num_from_wh) as pallet_num')
            )
            ->where('ready_go','Y')
            ->where('type_cd','driver' )
            ->where('owner_by',$request->phone)
            ->value('pallet_num');
            // $totalpallet     += $row->pallet_num;
            Log::info('log for pallet');
            Log::info($totalpallet);

            $data['driver_name']  = $driverData->name;
            $data['driver_phone'] = $request->phone;
            $data['total_pallet'] = (int)$totalpallet;
            $data['pallets']      = $palltetdata;
            $data['warehouse']    = $warehousearray;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $data]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }

    }

    public function checkdlvpalletdlvNo(Request $request) {
        Log::info("checkdlvpalletdlvNo");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $dlvnos = array();
            if($request->wh_user == "Y") {
                $limitday = DB::table('bscode')
                ->where('cd_type', 'DRIVERLOGINDAY')
                ->where('cd', 'driverloginday')
                ->value('value1');
                $days = date("Y-m-d ", strtotime("-".$limitday." day"));

                $alldlvno = DB::table('mod_dss_dlv')
                ->where('driver_phone', $request->driver_phone)
                ->where('dlv_date', '>=', $days)
                ->where('status','!=', 'FINISHED')
                ->pluck('dlv_no')
                ->toArray();

                $dlvaddress = DB::table('mod_dlv_plan')
                ->where('button_status', 'STARTUPLOADING')
                ->where('dlv_date', '>=', $days)
                ->whereIn('dlv_no', $alldlvno)
                ->groupBy('cust_address')
                ->pluck('cust_address')
                ->toArray();
                $dlvbyno = DB::table('mod_dlv_plan')
                ->where('button_status', 'STARTUPLOADING')
                ->where('dlv_date', '>=', $days)
                ->whereIn('dlv_no', $alldlvno)
                ->groupBy('cust_address')
                ->pluck('dlv_no')
                ->toArray();
                Log::info($alldlvno);
                Log::info($dlvbyno);
                //直接判斷倉管人員的可查看倉庫地址
                // $dlvwh = DB::table('mod_warehouse')
                // ->whereNotNull('dc_id')
                // ->whereIn('address', $dlvaddress)
                // ->get();
                // foreach ($dlvwh as $dlvwhkey => $dlvwhrow) {
                //     $whnos[] =  $dlvwhrow->cust_no;
                // }
                // foreach ($whnos as $key => $value) {
                //     $dcid =substr($value,0,2);
                //     if($dcid == "A0" || $dcid == "B0") {
                //         $whnos[] =  $value;
                //     } else {
                //         $whnos[] =  $dcid;
                //     }
                // }
                $dlvnos = DB::table('mod_dlv_plan')
                ->select('dlv_no')
                ->where('button_status', 'STARTUPLOADING')
                ->whereIn('dlv_no', $alldlvno)
                ->whereIn('cust_address', $dlvaddress)
                ->groupBy('dlv_no')
                ->pluck('dlv_no')->toArray();
                Log::info($dlvnos);
            } else {
                $dlvnos = DB::table('mod_dss_dlv')
                ->select('dlv_no')
                ->where('driver_phone', $request->driver_phone)
                ->where('status','!=', 'FINISHED')
                ->groupBy('dlv_no')
                ->pluck('dlv_no')->toArray();
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $dlvnos]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> $e->getMessage()]);
        }
    }
    public function checkdlvpalletBywh(Request $request) {
        Log::info("checkdlvpalletBywh");
        Log::info($request->all());
        //
        $user = Auth::user();
        try {

            $palletByWh =  DB::table('mod_pallet_manage')
            ->select(
                DB::raw("wh_no as from_wh_cd"),
                DB::raw("wh_name as from_wh_name")
            )
            ->where('wh_no', $request->wh_no)
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd', 'driver')
            ->where('pallet_num' ,'>', 0)
            ->groupBy('wh_no','wh_name')
            ->get();


            foreach ($palletByWh as $whkey => $whrow) {
                $deliverycount =  DB::table('mod_pallet_manage_log')
                ->where('dlv_no', $request->dlv_no)
                ->where('wh_no', $whrow->from_wh_cd)
                ->orderBy('delivery_count', 'desc')
                ->value('delivery_count');
                $dlvPallet = DB::table('mod_pallet_manage_log')
                ->select(
                    DB::raw("pallet_type as pallet_name"),
                    'pallet_code',
                    DB::raw("sum( pallet_num )as pallet_num")
                )
                ->where('ready_go' ,'N')
                ->where('dlv_no', $request->dlv_no)
                ->where('wh_no', $whrow->from_wh_cd)
                ->where('type_cd', 'driver')
                ->where('pallet_num' ,'>', 0)
                ->where('delivery_count' , $deliverycount)
                ->groupBy('pallet_code','pallet_type','wh_name')
                ->get();
                $whrow->pallet_detail = $dlvPallet;
            }


            $data = array();
            if($request->wh_user == "Y") {
                $custpick = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('discharged', 'Y')
                ->where('trs_mode', 'pick')
                ->groupBy('owner_cd')
                ->get();
            } else {
                $custpick = [];
            }
            $checklock = 0;
            $custpick  = count($custpick);
            $checkdata = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('lock_pallet', 'Y')
            ->pluck('wms_ord_no');
            $errorwh = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->whereIn('wms_ord_no', $checkdata)
            ->groupBy('wms_ord_no')
            ->pluck('wms_ord_no')
            ->toArray();
            $errorwh = count($errorwh);
            $notfinish = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('status','!=','finished')
            ->groupBy('wms_ord_no')
            ->pluck('wms_ord_no')
            ->toArray();
            $notfinish = count($notfinish);
            //2023-06-26 逆物流下貨完成
            //提貨任務 這個車次編號 倉庫地址的
            if($errorwh == $notfinish ) {
                $checklock = 1;
            }
            //2023-06-29 逆物流下貨完成
            $backdelivery = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('backdelivery', 'Y')
            ->where('trs_mode', 'dlv')
            ->count();
            $totalcount = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode', 'dlv')
            ->count();
            if($backdelivery == $totalcount) {
                $checklock = 1;
            }
            // if($checkdata)
            Log::info('checklockpick 0');
            Log::info($checklock);

            $checktranfer = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode', 'dlv')
            ->where('main_trs_mode', '轉運')
            ->whereNotNull('arrive_time')
            ->count();
            if($checktranfer > 0) {
                Log::info('checktranfer 1');
                $checklock = 1;
            }

            $optionData = array();
            $optionData[0]['cd']       = '一般放行';
            $optionData[0]['cd_descp'] = '一般放行';
            $optionData[1]['cd']       = '空車放行';
            $optionData[1]['cd_descp'] = '空車放行';
            $optionData[2]['cd']       = '還板放行';
            $optionData[2]['cd_descp'] = '還板放行';

            $controlWh  = explode(',',$user->check_wh);
            $deliveryWh = explode(',',$user->delivery_wh);
            $editpallet = "Y";
            if(in_array($request->wh_no , $controlWh)) {
                $editpallet = "Y";
            } else if(in_array($request->wh_no , $deliveryWh)) {
                $editpallet = "N";
            }
            if($request->wh_no == "C0-1" || $request->wh_no == "D0-1" || $request->wh_no == "E0-1") {
                $request->wh_no = substr($request->wh_no,0,2);
            }
            $wh = DB::table('mod_mark')
            ->select(
                DB::raw("cust_no as mark_no"),
                DB::raw("CONCAT(wh_name ,'-', cust_no)as mark_name"),
                DB::raw("wh_name as og_name"),
                'wh_name','wh_no'
            )
            ->where('wh_no',$request->wh_no)
            ->where('is_enabled','Y')
            ->groupBy('cust_no','wh_name','wh_no')
            ->get();
            $data['wh_data']     = $wh;
            $data['lock_pallet'] = $checklock > 0 ? 'Y' : 'N';
            $data['edit_pallet'] = $editpallet;
            $data['dlvPallet']   = $palletByWh;
            $data['show_view']   = 'Y';
            $data['optionData']  = $optionData;
            Log::info('final return'. '---' .$data['lock_pallet']);

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $data]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> $e->getMessage()]);
        }
    }
    public function checkdlvpallet(Request $request) {
        Log::info("checkdlvpallet");
        Log::info($request->all());
        //
        $user = Auth::user();
        try {

            $palletByWh =  DB::table('mod_pallet_manage')
            ->select(
                DB::raw("wh_no as from_wh_cd"),
                DB::raw("wh_name as from_wh_name")
            )
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd', 'driver')
            ->groupBy('wh_no','wh_name')
            ->get();

            $dssdlv = DB::table('mod_dss_dlv')
            ->where('dlv_no', $request->dlv_no)
            ->first();
            $final =  DB::table('mod_driver_checkin')
            ->where('phone', $dssdlv->driver_phone)
            ->where('dc_id', $dssdlv->dc_id)
            ->orderBy('id','desc')
            ->first();
            $maxdeliverycount =  DB::table('mod_pallet_manage_log')
            ->where('dlv_no', $request->dlv_no)
            ->where('ready_go', 'Y')
            ->orderBy('delivery_count','desc')
            ->value('delivery_count');
            $maxdeliverycount = (int)$maxdeliverycount;
            foreach ($palletByWh as $whkey => $whrow) {
                Log::info('from_wh_cd');
                Log::info($whrow->from_wh_cd);
                Log::info('final->id');
                Log::info($final->id);
                $checkincount = 0;
                $delivery =  DB::table('mod_delivery_log')
                ->where('dlv_no', $request->dlv_no)
                ->where('checkin_id', $final->id)
                ->count();
                if($delivery == 0) {
                    $checkincount ++;
                }
                if($request->wh_user == 'Y' ) {
                    $checkincount = 0;
                }
                Log::info('delivery');
                Log::info($delivery);
                Log::info('maxdeliverycount');
                Log::info($maxdeliverycount);
                Log::info('checkincount');
                Log::info($checkincount);
                Log::info('for sql count');
                Log::info(($maxdeliverycount + $checkincount ));
                $finalcount = $maxdeliverycount + $checkincount ;
                if($finalcount == 0 ) {
                    $finalcount = 1;
                }
                $dlvPallet = DB::table('mod_pallet_manage_log')
                ->select(
                    DB::raw("pallet_type as pallet_name"),
                    'pallet_code',
                    DB::raw("sum( pallet_num )as pallet_num")
                )
                ->where('dlv_no', $request->dlv_no)
                ->where('wh_no', $whrow->from_wh_cd)
                ->where('type_cd', 'driver')
                ->where('move_type', '借出')
                ->where('delivery_count', ($finalcount))
                ->whereNotNull('checkin_id')
                ->groupBy('pallet_code','pallet_type','wh_name')
                ->get();
                $whrow->pallet_detail = $dlvPallet;
            }


            $data = array();
            if($request->wh_user == "Y") {
                $custpick = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('discharged', 'Y')
                ->where('trs_mode', 'pick')
                ->groupBy('owner_cd')
                ->get();
            } else {
                $custpick = [];
            }

            $custpick  = count($custpick);
            $checklock = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('lock_pallet','Y')
            ->where('trs_mode', 'dlv')
            ->count();
            Log::info('checklockpick 0');
            Log::info($checklock);
            $checklock2 = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('backdelivery','Y')
            ->where('button_status_desc','待放行')
            ->where('trs_mode', 'dlv')
            ->count();

            $checklockpick = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode', 'dlv')
            ->count();
            if($checklock2 == $checklockpick) {
                Log::info('checklockpick 1');
                $checklock = 1;
            }

            $checktranfer = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode', 'dlv')
            ->where('main_trs_mode', '轉運')
            ->whereNotNull('arrive_time')
            ->count();
            if($checktranfer > 0) {
                Log::info('checktranfer 1');
                $checklock = 1;
            }

            $optionData = array();
            $optionData[0]['cd']       = '一般放行';
            $optionData[0]['cd_descp'] = '一般放行';
            $optionData[1]['cd']       = '空車放行';
            $optionData[1]['cd_descp'] = '空車放行';

            $data['lock_pallet'] = $checklock > 0 ? 'Y' : 'N';
            $data['dlvPallet']   = $palletByWh;
            $data['show_view']   = 'Y';
            $data['optionData']  = $optionData;
            Log::info('checkdlvpallet return data');
            // Log::info($data);

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $data]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
    }
    public function reverselogistics(Request $request) {
        //逆物流
        Log::info("reverselogistics");
        Log::info($request->all());
        $user = Auth::user();
        $custdoPallets = $request->palletdatas;
        try { 
            //'warehouse'CHECKIN
            $dssdlv = DB::table('mod_dss_dlv')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->first();

            $detail = DB::table('mod_dlv_plan')
            ->where('dlv_no', $dssdlv->dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->where('trs_mode', $request->trs_mode)
            ->first();

            // $dlvdetail = DB::table('mod_dlv_plan')
            // ->where('dlv_no', $dssdlv->dlv_no)
            // ->where('dlv_ord_no', $request->dlv_ord_no)
            // ->where('trs_mode', 'dlv')
            // ->first();
            $detailhaswhno = DB::table('mod_dlv_plan')
            ->where('dlv_no', $dssdlv->dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->whereNotNull('wh_no')
            ->first();

            $detailforarray = DB::table('mod_dlv_plan')
            ->where('dlv_no', $dssdlv->dlv_no)
            ->whereIn('id', $request->ids)
            ->pluck('wms_ord_no')
            ->toArray();


            $pressPallets = array();
            $backPallets = array();
            $hintmessage = "";
            foreach ($custdoPallets as $key => $custdoPallet) {
                $pressPallets[$custdoPallet['pallet_name']] = 0;
                $backPallets[$custdoPallet['pallet_name']] = 0;
                foreach ($custdoPallet['detail'] as $key => $row) {
                    if( $row['type'] == "press" ) {
                        $pressPallets[$custdoPallet['pallet_name']] += $row['num'];
                    } else if($row['type'] == "back") {
                        $backPallets[$custdoPallet['pallet_name']] += $row['num'];
                    }
                    // $callbackNum += $row['num'];
                }
                $custnumcheck = DB::table('mod_pallet_manage')
                ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                ->where('type_cd', 'cust')
                ->where('cust_name', $detail->cust_name)
                ->where('pallet_code', $custdoPallet['pallet_code'])
                ->where('wh_no', 'like', '%'.substr($dssdlv->dc_id,0,2).'%')
                ->value('pallet_num');
                
                if($backPallets[$custdoPallet['pallet_name']] > $custnumcheck) {
                    $hintmessage = $hintmessage.$custdoPallet['pallet_name'].',';
                    // return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
                }
            }
            if($hintmessage  != '') {
                return response()->json(['msg' =>  $hintmessage.'請確認數量', 'code' => '01', 'data'=> null]);
            }

            if($detail->backdelivery == "Y" && $detail->dlv_to_wh =="N") {
                Log::info("reverselogistics2");
                //2023-08-02
                DB::table('mod_dlv_plan')
                ->where('dlv_no', $dssdlv->dlv_no)
                ->whereIn('id', $request->ids)
                ->update([
                    'report_pallet' => 'Y',
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                    'status'        => 'DLV',
                    'status_desc'   => '任務已分派',
                ]);
            } else {
                Log::info("reverselogistics3");
                DB::table('mod_dlv_plan')
                ->where('dlv_no', $dssdlv->dlv_no)
                ->whereIn('id', $request->ids)
                ->where('trs_mode', 'dlv')
                ->update([
                    'report_pallet'      => 'Y',
                    'button_status'      => 'CHECKIN',
                    'button_status_desc' => '報到',
                    'status'             => 'FINISHED',
                    'updated_at'         => Carbon::now()->toDateTimeString(),
                ]); 
            }


            $forapidetail = DB::table('mod_dlv_plan')
            ->where('dlv_no', $dssdlv->dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->where('trs_mode', $request->trs_mode)
            ->get();
            foreach ($forapidetail as $key => $apidetail) {
                DB::table('sys_wms_api')
                ->insert([
                    'wms_ord_no'  => $apidetail->wms_ord_no,
                    'ord_no'      => $apidetail->ord_no,
                    'status'      => 'LOADING',
                    'status_desc' => '貨物裝載中',
                    'action_time' => Carbon::now()->toDateTimeString(),
                    'created_by'  => $user->email,
                    'updated_by'  => $user->email,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString(),
                ]);
            }
            //UPDATE ORDER 

            DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->whereIn('id', $request->ids)
            ->where('trs_mode', 'pick')
            ->update([
                'button_status'      => 'FINISHED',
                'button_status_desc' => '修改回報資料',
                'status'             => 'FINISHED',
                'status_desc'        => '任務完成',
                'finish_time'        => Carbon::now()->toDateTimeString(),
            ]);

            //逆物流堤貨完成
            Log::info('訂單提貨完成');
            Log::info($detailforarray);
            DB::table('mod_order')
            ->whereIn('ord_no', $detailforarray)
            ->update([
                'status'             => 'UPLOADFINISH',
                'status_desc'        => '提貨完成',
                'upload_finish_time' => Carbon::now()->toDateTimeString(),
                'updated_at'         => Carbon::now()->toDateTimeString(),
            ]);



            $warehouse = DB::table('mod_warehouse')
            ->where('cust_no',$detailhaswhno->wh_no)
            ->first();
            $now = date("Y-m-d H:i:s");
            foreach ($custdoPallets as $key => $palletdata) {
                //客戶減少
                Log::info('test123');
                Log::info($detail->cust_name);
                Log::info($detail->wh_no);
                Log::info($detail->cust_name);
                Log::info($detail->id);
                Log::info($warehouse->cust_no);
                Log::info('test123 end');

                // 2023-06-26 判斷客戶倉庫
                $checkCustexist = DB::table('mod_pallet_manage')
                ->where('type_cd', 'cust')
                ->where('cust_name', $detail->cust_name)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('wh_no', 'like', '%'.substr($dssdlv->dc_id,0,2).'%')
                ->orderBy('pallet_num', 'desc')
                ->get();

                Log::info('紀錄狀況');
                if(isset($checkCustexist)) {
                    Log::info('客戶扣除');
                    Log::info( $backPallets[$palletdata['pallet_name']]);
                    // 2023-06-26 客戶扣除調先依照數量 再依照建立時間
                    $totalminus = $backPallets[$palletdata['pallet_name']];
                    foreach ($checkCustexist as $key => $updaterow) {

                        $driverPallet = DB::table('mod_pallet_manage')
                        ->where('owner_by', $user->email)
                        ->where('pallet_code', $palletdata['pallet_code'])
                        ->where('wh_no', $updaterow->wh_no)
                        ->first();

                        Log::info('客戶更新id');
                        Log::info($updaterow->id);
                        $palletnum  = 0;
                        $drivernum  = 0;
                        if($updaterow->pallet_num >= $totalminus) {
                            if($totalminus > 0) {
                                Log::info('updaterow->pallet_num');
                                Log::info($updaterow->pallet_num);
                                Log::info('totalminus');
                                Log::info($totalminus);
                                $palletnum = $updaterow->pallet_num - $totalminus;
                                DB::table('mod_pallet_manage')
                                ->where('id', $updaterow->id)
                                ->update([
                                    'pallet_num'  => $updaterow->pallet_num - $totalminus,
                                    'updated_at'  => date("Y-m-d H:i:s"),
                                ]);
                            }
                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'move_type'      => '逆物流',
                                'control_from'   => 'app',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $totalminus * -1,
                                'back_num'       => abs($totalminus),
                                'press_num'      => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'type_cd'        => 'cust',
                                'type_name'      => '客戶',
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->from_wh_name,
                                'wh_no'          => $updaterow->wh_no,
                                'wh_name'        => $updaterow->wh_name,
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                'created_by'     => $user->email,
                                'updated_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);

                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'move_type'      => '司機完成',
                                'control_from'   => 'app',
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $totalminus,
                                'back_num'       => $totalminus,
                                'press_num'      => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->from_wh_name,
                                'wh_no'          => $updaterow->wh_no,
                                'wh_name'        => $updaterow->wh_name,
                                'dlv_no'         => $dssdlv->dlv_no,
                                'is_update'      => 'N',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                'updated_by'     => $user->email,
                                'created_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);

                            if(isset($driverPallet)) {
                                Log::info('司機更新');
                                DB::table('mod_pallet_manage')
                                ->where('id', $driverPallet->id)
                                ->update([
                                    'cust_no'     => $detail->owner_cd,
                                    'cust_name'   => $detail->cust_name,
                                    'pallet_num'  => $driverPallet->pallet_num + $totalminus,
                                    'num_from_wh' => $driverPallet->num_from_wh + $totalminus,
                                    'updated_at'  => date("Y-m-d H:i:s"),
                                    'ready_go'    => 'Y',
                                ]);
                            } else {
                                Log::info('司機新增');
                                DB::table('mod_pallet_manage')
                                ->insert([
                                    'dlv_no'         => $dssdlv->dlv_no,
                                    'from_wh_cd'     => $updaterow->from_wh_cd,
                                    'from_wh_name'   => $updaterow->from_wh_name,
                                    'wh_no'          => $updaterow->wh_no,
                                    'wh_name'        => $updaterow->wh_name,
                                    'owner_by'       => $user->email,
                                    'owner_name'     => $user->name,
                                    'pallet_num'     => $totalminus,
                                    'num_from_wh'    => $totalminus,
                                    'pallet_code'    => $palletdata['pallet_code'],
                                    'pallet_type'    => $palletdata['pallet_name'],
                                    'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                    'ready_go'       => 'Y',
                                    'g_key'          => 'DSS',
                                    'c_key'          => 'DSS',
                                    's_key'          => 'DSS',
                                    'd_key'          => 'DSS',
                                    'updated_by'     => $user->email,
                                    'created_by'     => $user->email,
                                    'created_at'     => $now,
                                    'updated_at'     => $now,
                                    'type_cd'        => 'driver',
                                    'type_name'      => '司機',
                                    'dlv_no'         => $detail->dlv_no,
                                ]);
                            }
                            
                            break;
                        } else {
                            Log::info('客戶更新id 2');
                            Log::info($updaterow->id);
                            $palletnum = $totalminus;
                            DB::table('mod_pallet_manage')
                            ->where('id', $updaterow->id)
                            ->update([
                                'pallet_num'  => 0,
                                'updated_at'  => date("Y-m-d H:i:s"),
                            ]);
                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'move_type'      => '逆物流',
                                'control_from'   => 'app',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $updaterow->pallet_num * -1,
                                'back_num'       => abs($updaterow->pallet_num),
                                'press_num'      => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'type_cd'        => 'cust',
                                'type_name'      => '客戶',
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->from_wh_name,
                                'wh_no'          => $updaterow->wh_no,
                                'wh_name'        => $updaterow->wh_name,
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                'created_by'     => $user->email,
                                'updated_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);

                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'move_type'      => '司機完成',
                                'control_from'   => 'app',
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $updaterow->pallet_num,
                                'back_num'       => $updaterow->pallet_num,
                                'press_num'      => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->from_wh_name,
                                'wh_no'          => $updaterow->wh_no,
                                'wh_name'        => $updaterow->wh_name,
                                'dlv_no'         => $dssdlv->dlv_no,
                                'is_update'      => 'N',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                'updated_by'     => $user->email,
                                'created_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);

                            if(isset( $driverPallet)) {
                                Log::info('司機更新');
                                DB::table('mod_pallet_manage')
                                ->where('id', $driverPallet->id)
                                ->update([
                                    'cust_no'     => $detail->owner_cd,
                                    'cust_name'   => $detail->cust_name,
                                    'pallet_num'  => $driverPallet->pallet_num + $updaterow->pallet_num,
                                    'num_from_wh' => $driverPallet->num_from_wh + $updaterow->pallet_num,
                                    'updated_at'  => date("Y-m-d H:i:s"),
                                    'ready_go'    => 'Y',
                                ]);
                            } else {
                                Log::info('司機新增');
                                DB::table('mod_pallet_manage')
                                ->insert([
                                    'dlv_no'         => $dssdlv->dlv_no,
                                    'from_wh_cd'     => $updaterow->from_wh_cd,
                                    'from_wh_name'   => $updaterow->from_wh_name,
                                    'wh_no'          => $updaterow->wh_no,
                                    'wh_name'        => $updaterow->wh_name,
                                    'owner_by'       => $user->email,
                                    'owner_name'     => $user->name,
                                    'pallet_num'     => $updaterow->pallet_num,
                                    'num_from_wh'    => $updaterow->pallet_num,
                                    'pallet_code'    => $palletdata['pallet_code'],
                                    'pallet_type'    => $palletdata['pallet_name'],
                                    'truck_cmp_name' => $dssdlv->truck_cmp_name,
                                    'ready_go'       => 'Y',
                                    'g_key'          => 'DSS',
                                    'c_key'          => 'DSS',
                                    's_key'          => 'DSS',
                                    'd_key'          => 'DSS',
                                    'updated_by'     => $user->email,
                                    'created_by'     => $user->email,
                                    'created_at'     => $now,
                                    'updated_at'     => $now,
                                    'type_cd'        => 'driver',
                                    'type_name'      => '司機',
                                    'dlv_no'         => $detail->dlv_no,
                                ]);
                            }

                            $totalminus = $totalminus - $updaterow->pallet_num;
                        }
                    }
                } else {
                    Log::info('補資料');
                    $now = date("Y-m-d H:i:s");
                    if($detail->dlv_to_wh == "Y") {
                        DB::table('mod_pallet_manage')
                        ->insert([
                            'cust_no'        => $detail->owner_cd,
                            'cust_name'      => $detail->cust_name,
                            'truck_cmp_name' => $dssdlv->truck_cmp_name,
                            'pallet_num'     => $backPallets[$palletdata['pallet_name']]*-1,
                            'pallet_code'    => $palletdata['pallet_code'],
                            'pallet_type'    => $palletdata['pallet_name'],
                            'g_key'          => 'DSS',
                            'c_key'          => 'DSS',
                            's_key'          => 'DSS',
                            'd_key'          => 'DSS',
                            'created_by'     => $user->email,
                            'updated_by'     => $user->email,
                            'created_at'     => $now,
                            'updated_at'     => $now,
                            'type_cd'        => 'cust',
                            'type_name'      => '客戶',
                            'wh_no'          => $warehouse->cust_no,
                            'wh_name'        => $warehouse->cname,
                            'from_wh_cd'     => $warehouse->cust_no,
                            'from_wh_name'   => $warehouse->cname,
                        ]);

                        DB::table('mod_pallet_manage_log')
                        ->insert([
                            'ready_go'       => 'Y',
                            'move_type'      => '逆物流',
                            'control_from'   => 'app',
                            'wms_ord_no'     => $detail->wms_ord_no,
                            'cust_no'        => $detail->owner_cd,
                            'cust_name'      => $detail->cust_name,
                            'wms_ord_no'     => $detail->wms_ord_no,
                            'truck_cmp_name' => $dssdlv->truck_cmp_name,
                            'pallet_num'     => $palletdata['pallet_num'] * -1,
                            'back_num'       => abs($palletdata['pallet_num']),
                            'press_num'      => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                            'pallet_code'    => $palletdata['pallet_code'],
                            'pallet_type'    => $palletdata['pallet_name'],
                            'type_cd'        => 'cust',
                            'type_name'      => '客戶',
                            'from_wh_cd'     => $warehouse->cust_no,
                            'from_wh_name'   => $warehouse->cname,
                            'wh_no'          => $warehouse->cust_no,
                            'wh_name'        => $warehouse->cname,
                            'created_by'     => $user->email,
                            'updated_by'     => $user->email,
                            'created_at'     => $now,
                            'updated_at'     => $now,
                        ]);
                    } else {
                        Log::info('補資料2');
                        $detail = DB::table('mod_dlv_plan')
                        ->where('dlv_no', $dssdlv->dlv_no)
                        ->where('sys_dlv_no', $request->sys_dlv_no)
                        ->where('trs_mode', 'pick')
                        ->first();
            
                        DB::table('mod_pallet_manage')
                        ->insert([
                            'cust_no'      => $detail->owner_cd,
                            'cust_name'    => $detail->cust_name,
                            'truck_cmp_name' => $dssdlv->truck_cmp_name,
                            'pallet_num'   => $backPallets[$palletdata['pallet_name']]*-1,
                            'pallet_code'  => $palletdata['pallet_code'],
                            'pallet_type'  => $palletdata['pallet_name'],
                            'g_key'        => 'DSS',
                            'c_key'        => 'DSS',
                            's_key'        => 'DSS',
                            'd_key'        => 'DSS',
                            'created_by'   => $user->email,
                            'updated_by'   => $user->email,
                            'created_at'   => $now,
                            'updated_at'   => $now,
                            'type_cd'      => 'cust',
                            'type_name'    => '客戶',
                            'wh_no'        => $warehouse->cust_no,
                            'wh_name'      => $warehouse->cname,
                            'from_wh_cd'   => $warehouse->cust_no,
                            'from_wh_name' => $warehouse->cname,
                        ]);


                        DB::table('mod_pallet_manage_log')
                        ->insert([
                            'ready_go'       => 'Y',
                            'move_type'    => '逆物流',
                            'control_from' => 'app',
                            'wms_ord_no'     => $detail->wms_ord_no,
                            'truck_cmp_name' => $dssdlv->truck_cmp_name,
                            'wms_ord_no'  => $detail->wms_ord_no,
                            'cust_no'     => $detail->owner_cd,
                            'cust_name'   => $detail->cust_name,
                            'pallet_num'  => $palletdata['pallet_num'] * -1 ,
                            'back_num'    => abs($palletdata['pallet_num']) ,
                            'press_num'   => isset($pressPallets[$palletdata['pallet_name']]) ? $pressPallets[$palletdata['pallet_name']] : 0,
                            'pallet_code' => $palletdata['pallet_code'],
                            'pallet_type' => $palletdata['pallet_name'],
                            'type_cd'     => 'cust',
                            'type_name'   => '客戶',
                            'from_wh_cd'  => $warehouse->cust_no,
                            'from_wh_name'=> $warehouse->cname,
                            'wh_no'       => $warehouse->cust_no,
                            'wh_name'     => $warehouse->cname,
                            'created_by'  => $user->email,
                            'updated_by'  => $user->email,
                            'created_at'  => $now,
                            'updated_at'  => $now,
                        ]);
                        
                    }

                }
            }


            $this->updatedashboardstatus();
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  null]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
    }

    
    /*public function deliverydlvnobywh(Request $request) {
        Log::info("deliverydlvnobywh");
        Log::info($request->all());
        try {
            $today      = date("Y-m-d 00:00:00");
            $user       = Auth::user();
            $returndata = array();

            $backdelivery = 'N';
            $checkin = DB::table('mod_driver_checkin')
            ->where('created_at', '>=', $today)
            ->where('phone', $request->driver_phone)
            ->where('delivery_type', '還板報到')
            ->whereNull('leave_time')
            ->orderBy('id','desc')
            ->count();

            if( $checkin > 0) {
                $backdelivery = 'Y';
            }

            $checkWh = explode(',',$user->check_wh);
            // $deliveryWh = explode(',',$user->delivery_wh);
            // $seewh = array_merge( $checkWh,$deliveryWh);
            $wh = DB::table('mod_warehouse')
            ->select(
                'address',
                DB::raw('cust_no as wh_no'),
                DB::raw('cname as wh_name')
            )
            ->whereIn('cust_no', $checkWh)
            ->get();

            $whaddress = array();
            foreach ($wh as $key => $whrowforaddress) {
                $whaddress[$whrowforaddress->address]  = $whrowforaddress->wh_no;
            }

            $dlvnos = DB::table('mod_dss_dlv')
            ->where('driver_phone', $request->driver_phone)
            ->where('status', '!=', 'FINISHED')
            ->pluck('dlv_no')
            ->toArray();

            $dlvaddress = DB::table('mod_dlv_plan')
            ->where('button_status_desc', '待放行')
            // ->where('dlv_date', '>=', $days)
            ->whereIn('dlv_no', $dlvnos)
            ->groupBy('cust_address')
            ->pluck('cust_address')
            ->toArray();

            $drivermission = DB::table('mod_dlv_plan')
            ->select('dlv_no','wh_no','cust_address','cust_name','wms_ord_no')
            ->whereIn('dlv_no', $dlvnos)
            ->where('button_status_desc', '待放行')
            ->whereIn('cust_address', $dlvaddress)
            ->groupBy('dlv_no','wh_no')
            ->get();

            $forwhdlv = array();
            foreach ($drivermission as $key => $row) {
                if(! empty($row->wh_no) ) {
                    //提貨
                    $forwhdlv[$row->wh_no][] = $row->dlv_no;
                } else if(isset($whaddress[$row->cust_address]))  {
                    //配送
                    $addressWhno = $whaddress[$row->cust_address];
                    $forwhdlv[$addressWhno][] = $row->dlv_no;
                } else {
                    // 逆物流
                    $backdata = DB::table('mod_dlv_plan')
                    ->where('wms_ord_no', $row->wms_ord_no)
                    ->where('dlv_no', $row->dlv_no)
                    ->whereNotNull('wh_no')
                    ->first();
                    $forwhdlv[$backdata->wh_no][] = $row->dlv_no;
                }
            }

            $newmarklist = DB::table('mod_mark')
            ->select(
                DB::raw("cust_no as mark_no"),
                DB::raw("CONCAT(wh_name ,'-', cust_no)as mark_name"),
                DB::raw("wh_name as og_name"),
                'wh_name','wh_no'
            )
            ->where('is_enabled','Y')
            ->groupBy('cust_no','wh_name','wh_no')
            ->get();

            $markArray = array();
            foreach ($newmarklist as $key => $markrow) {
                if($markrow->wh_no == "C0" || $markrow->wh_no == "D0" || $markrow->wh_no == "E0") {
                    $markArray[$markrow->wh_no.'-1'][] =array( 'mark_no'=> $markrow->mark_no, 'mark_name'=>$markrow->mark_name);
                } else {
                    $markArray[$markrow->wh_no][] =array( 'mark_no'=> $markrow->mark_no, 'mark_name'=>$markrow->mark_name);
                }
            }
            $backdeliveryno = array();
            foreach ($wh as $key => $whrow) {
                if($backdelivery == "Y") {
                    $forwhdlv[$whrow->wh_no][] = '還板放行';
                    $backdeliveryno[] =  '還板放行';
                }
                if(isset($forwhdlv[$whrow->wh_no])) {
                    $whrow->dlv_nos = $forwhdlv[$whrow->wh_no] ;
                } else {
                    $whrow->dlv_nos = $backdeliveryno;
                    $whrow->marks = NULL;
                }
                if(isset($markArray[$whrow->wh_no])) {
                    $whrow->marks = $markArray[$whrow->wh_no];
                } else {
                    $whrow->marks = NULL;
                }
            }
            $optionData = array();
            $optionData[0]['cd']       = '一般放行';
            $optionData[0]['cd_descp'] = '一般放行';
            $optionData[1]['cd']       = '空車放行';
            $optionData[1]['cd_descp'] = '空車放行';
            if($backdelivery == "Y") {
                $optionData[2]['cd']       = '還板放行';
                $optionData[2]['cd_descp'] = '還板放行';
            }


            $returndata['wh_data']       = $wh;
            $returndata['back_delivery'] = $backdelivery;
            $returndata['optionData']    = $optionData;


            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $returndata]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }*/

    public function deliverydlvnobywh(Request $request) {
        Log::info("deliverydlvnobywh");
        Log::info($request->all());
        try {
            $today      = date("Y-m-d 00:00:00");
            $user       = Auth::user();
            $returndata = array();

            $backdelivery = 'N';

            $checkin = DB::table('mod_driver_checkin')
            ->where('phone', $request->driver_phone)
            ->whereNull('leave_time')
            ->orderBy('id','desc')
            ->first();

            if ($checkin->delivery_type == '還板報到') {
                $backdelivery = 'Y';
            }

            $checkWh = explode(',',$user->check_wh);
            $deliveryWh = explode(',',$user->delivery_wh);
            $seewh = array_merge( $checkWh,$deliveryWh);
            $wh = DB::table('mod_warehouse')
            ->select(
                'address',
                DB::raw('cust_no as wh_no'),
                DB::raw('cname as wh_name')
            )
            ->whereIn('cust_no', $checkWh)
            ->get();

            $whaddress = array();
            foreach ($wh as $key => $whrowforaddress) {
                $whaddress[$whrowforaddress->address]  = $whrowforaddress->wh_no;
            }

            $dlvnos = DB::table('mod_dss_dlv')
            ->where('driver_phone', $request->driver_phone)
            ->where('status', '!=', 'FINISHED')
            ->pluck('dlv_no')
            ->toArray();

            $drivermission = DB::table('mod_dlv_plan')
            ->select('dlv_no','wh_no','cust_address','cust_name','wms_ord_no')
            ->whereIn('dlv_no', $dlvnos)
            ->where('button_status_desc', '待放行')
            ->groupBy('dlv_no','wh_no')
            ->get();

            $forwhdlv = array();
            foreach ($drivermission as $key => $row) {
                if(! empty($row->wh_no) ) {
                    //提貨
                    $forwhdlv[$row->wh_no][] = $row->dlv_no;
                } else if(isset($whaddress[$row->cust_address]))  {
                    //配送
                    $addressWhno = $whaddress[$row->cust_address];
                    $forwhdlv[$addressWhno][] = $row->dlv_no;
                } else {
                    // 逆物流
                    $backdata = DB::table('mod_dlv_plan')
                    ->where('wms_ord_no', $row->wms_ord_no)
                    ->where('dlv_no', $row->dlv_no)
                    ->whereNotNull('wh_no')
                    ->first();
                    $forwhdlv[$backdata->wh_no][] = $row->dlv_no;
                }
            }

            $newmarklist = DB::table('mod_mark')
            ->select(
                DB::raw("cust_no as mark_no"),
                DB::raw("CONCAT(wh_name ,'-', cust_no)as mark_name"),
                DB::raw("wh_name as og_name"),
                'wh_name','wh_no'
            )
            ->where('is_enabled','Y')
            ->groupBy('cust_no','wh_name','wh_no')
            ->get();

            $markArray = array();
            foreach ($newmarklist as $key => $markrow) {
                if($markrow->wh_no == "C0" || $markrow->wh_no == "D0" || $markrow->wh_no == "E0") {
                    $markArray[$markrow->wh_no.'-1'][] =array( 'mark_no'=> $markrow->mark_no, 'mark_name'=>$markrow->mark_name);
                } else {
                    $markArray[$markrow->wh_no][] =array( 'mark_no'=> $markrow->mark_no, 'mark_name'=>$markrow->mark_name);
                }
            }
            $backdeliveryno = array();
            foreach ($wh as $key => $whrow) {
                if($backdelivery == "Y") {
                    $forwhdlv[$whrow->wh_no][] = '還板放行';
                    $backdeliveryno[] =  '還板放行';
                }
                if(isset($forwhdlv[$whrow->wh_no])) {
                    $whrow->dlv_nos = $forwhdlv[$whrow->wh_no] ;
                } else {
                    $whrow->dlv_nos = $backdeliveryno;
                    $whrow->marks = NULL;
                }
                if(isset($markArray[$whrow->wh_no])) {
                    $whrow->marks = $markArray[$whrow->wh_no];
                } else {
                    $whrow->marks = NULL;
                }
            }
            $optionData = array();
            $optionData[0]['cd']       = '一般放行';
            $optionData[0]['cd_descp'] = '一般放行';
            $optionData[1]['cd']       = '空車放行';
            $optionData[1]['cd_descp'] = '空車放行';
            if($backdelivery == "Y") {
                $optionData[2]['cd']       = '還板放行';
                $optionData[2]['cd_descp'] = '還板放行';
            }


            $returndata['wh_data']       = $wh;
            $returndata['back_delivery'] = $backdelivery;
            $returndata['optionData']    = $optionData;


            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  $returndata]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }

    public function checkfinishaddress(Request $request) {
        Log::info("checkfinishaddress");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $detail = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->where('trs_mode', 'dlv')
            ->first();

            $hasSameCount =  DB::table('mod_dlv_plan')
            ->where('status','!=','FINISHED')
            ->where('id', '!=', $detail->id)
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('cust_address', $detail->cust_address)
            ->where('trs_mode', 'dlv')
            ->get();

            $flag = count($hasSameCount) > 1 ? "Y" :"N";
            Log::info($flag);

            $strArray = array();
            foreach ($hasSameCount as $key => $row) {
                $strArray[] = array('show_ord_no'=>mb_substr($row->owner_name,0,4).'-'.$row->ord_no , 'id'=>$row->id ) ;
            }

            $returndata = array();
            $returndata['flag'] = $flag ;
            $returndata['info'] = $strArray ;

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>   $returndata]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null]);
        }
    }
    public function driverunloading(Request $request) {
        Log::info("driverunloading");
        Log::info($request->all());
        $user = Auth::user();
        $custdoPallets = $request->palletdatas;
        try {
            //
            Log::info('driverunloading 1');
            $drivermissionMain = DB::table('mod_dss_dlv')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->first();
            // nick
            Log::info('driverunloading 2');
            $dlvdata = DB::table('mod_dlv_plan')
            ->whereIn('id', $request->ids)
            ->first();
            Log::info('driverunloading 3');
            $backtowarehouse = DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('dlv_ord_no', $request->dlv_ord_no)
                ->where('trs_mode', 'dlv')
                ->where('dlv_to_wh', 'Y')
                ->where('main_trs_mode', '轉運')
                ->whereIn('id', $request->ids)
                ->first();
            Log::info('driverunloading 4');
            $checkbackdelivery = DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('dlv_ord_no', $request->dlv_ord_no)
                ->where('trs_mode', 'dlv')
                ->where('dlv_to_wh', 'Y')
                ->where('backdelivery', 'Y')
                ->first();
            //判斷拒收 正收
            Log::info('driverunloading 4');
            $checkSp = DB::table('mod_error_report')
                ->select('ord_no')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('ord_no', $dlvdata->ord_no)
                ->where('mission_status', 'GOBACK')
                ->groupBy('ord_no')
                ->pluck('ord_no')
                ->toArray();
            Log::info(isset($backtowarehouse));
            //轉運司機到達完成
            if(isset($backtowarehouse) || isset($checkbackdelivery)  ) {

                if(!isset($backtowarehouse)) {
                    $backtowarehouse = $checkbackdelivery;
                }

                $detailpick = DB::table('mod_pallet_manage')
                ->where('dlv_no', $drivermissionMain->dlv_no)
                ->where('type_cd', 'driver')
                ->first();
    
                if(isset($detailpick)) {
                    $pickwarehouse = DB::table('mod_warehouse')
                    ->where('cust_no',$detailpick->from_wh_cd)
                    ->first();
                    //sys_customer
    
                    $now = date("Y-m-d H:i:s");
                    foreach ($custdoPallets as $key => $palletdata) {
    
                        $drivermission = DB::table('mod_dlv_plan')
                        ->where('sys_dlv_no', $request->sys_dlv_no)
                        ->where('dlv_ord_no', $request->dlv_ord_no)
                        ->where('trs_mode', 'dlv')
                        ->first();
    
                        Log::info('data1');
                        Log::info($backtowarehouse->cust_address);
                        Log::info('data2');
                        Log::info($backtowarehouse->wh_no);
                        Log::info($backtowarehouse->id);
                        $dlvwarehouse = DB::table('mod_warehouse')
                        ->where('address',$backtowarehouse->cust_address)
                        ->where('cust_no',$backtowarehouse->wh_no)
                        ->first();
                        if(!isset($dlvwarehouse)) {
                            $dlvwarehouse = DB::table('mod_warehouse')
                            ->where('address',$backtowarehouse->cust_address)
                            ->whereNotNull('dc_id')
                            ->first();
                        }

                        $checkDriverexist = DB::table('mod_pallet_manage')
                        ->where('owner_by', $detailpick->owner_by)
                        ->where('pallet_code', $palletdata['pallet_code'])
                        ->where('dlv_no', $drivermission->dlv_no)
                        ->where('type_cd','driver')
                        ->whereNotNull('from_wh_cd')
                        ->first();

                        $fochecknum = DB::table('mod_pallet_manage')
                        ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                        ->where('owner_by', $detailpick->owner_by)
                        ->where('pallet_code', $palletdata['pallet_code'])
                        ->where('dlv_no', $drivermission->dlv_no)
                        ->where('type_cd','driver')
                        ->whereNotNull('from_wh_cd')
                        ->first();
                        
                        Log::info('num compare');
                        Log::info($palletdata['pallet_num']);
                        Log::info($fochecknum->pallet_num);
                        Log::info('num compare end');
    
                        Log::info('id pk');
                        Log::info($detailpick->id);
    
                        if($palletdata['pallet_num'] > $fochecknum->pallet_num && $drivermission->status!='FINISHED' ) {
                            Log::info('提前結束 請確認數量');
                            return response()->json(['msg' => $palletdata['pallet_name'].'請確認數量', 'code' => '01', 'data'=> null]);
                        }
        
                        $checkusers = DB::table('users')
                        ->where('email', $detailpick->owner_by)
                        ->first();
        
                        if(!isset($checkusers )) {
                            Log::info('提前結束 此司機不存在');
                            return response()->json(['msg' => '此司機不存在', 'code' => '01', 'data'=> null ]);
                        }
                        Log::info('cust_no');
                        Log::info( $dlvwarehouse->id);
                        $warehouseData =  DB::table('mod_pallet_manage')
                        ->where('wh_no', $dlvwarehouse->cust_no)
                        ->where('pallet_code', $palletdata['pallet_code'])
                        ->where('type_cd', 'warehouse')
                        ->first();
                       
                        DB::table('mod_pallet_manage_log')
                        ->insert([
                            'ready_go'       => 'Y',
                            'owner_by'     => $user->email,
                            'owner_name'   => $user->name,
                            'move_type'    => '歸還',
                            'control_from' => 'app',
                            'pallet_num'   => $palletdata['pallet_num'],
                            'pallet_code'  => $palletdata['pallet_code'],
                            'pallet_type'  => $palletdata['pallet_name'],
                            'type_cd'      => 'warehouse',
                            'type_name'    => '倉庫',
                            'wh_no'        => $dlvwarehouse->cust_no,
                            'wh_name'      => $dlvwarehouse->cname,
                            'updated_by'   => $user->email,
                            'created_by'   => $user->email,
                            'created_at'   => $now,
                            'updated_at'   => $now,
                            'from_wh_cd'   => $warehouseData->wh_no,
                            'from_wh_name' => $warehouseData->wh_name,
                        ]);
                        Log::info('11111');
                        Log::info($dlvwarehouse->cust_no);
                        Log::info('frommangetotaldata');
                        Log::info($dlvwarehouse->id);
                        Log::info('update total info');
                        Log::info($dlvwarehouse->cust_no);
                        Log::info($pickwarehouse->cust_no);
                        $mangetotaldata =  DB::table('mod_pallet_manage_total')
                        ->where('owner_by', $dlvwarehouse->cust_no)
                        ->where('type_cd','warehouse')
                        ->where('from_wh_cd', $checkDriverexist->wh_no)
                        ->first();
                        if($backtowarehouse->backdelivery == "Y" ) {
                            $mangetotaldata =  DB::table('mod_pallet_manage_total')
                            ->where('type_cd','warehouse')
                            ->where('from_wh_cd', $pickwarehouse->cust_no)
                            ->first();
                        }
                        Log::info($mangetotaldata->id);
                        if($backtowarehouse->backdelivery != "Y" ) {
                            Log::info('backdelivery Y');
                            Log::info('總數 當前數量'.$mangetotaldata->pallet_num);
                            Log::info('總數 處理數量數量'.$palletdata['pallet_num']);
                            //加總數
                            DB::table('mod_pallet_manage_total')
                            ->where('id', $mangetotaldata->id)
                            ->where('type_cd','warehouse')
                            ->update([
                                'pallet_num' => $mangetotaldata->pallet_num + ($palletdata['pallet_num'] ),
                                'updated_at' => $now,
                            ]);
                        }
                        Log::info('updateid');
                        $ogmangetotaldata =  DB::table('mod_pallet_manage_total')
                        ->where('owner_by', $dlvwarehouse->cust_no)
                        ->where('type_cd','warehouse')
                        ->whereNull('from_wh_cd')
                        ->first();

                        Log::info('總數 當前數量'.$ogmangetotaldata->pallet_num);
                        Log::info('總數 處理數量數量'.$palletdata['pallet_num']);
        
                        DB::table('mod_pallet_manage_total')
                        ->where('id', $ogmangetotaldata->id)
                        ->update([
                            'pallet_num' => $ogmangetotaldata->pallet_num + ($palletdata['pallet_num'] ),
                        ]);
    
                        if(isset($warehouseData)) {
                            //加倉庫
                            DB::table('mod_pallet_manage')
                            ->where('wh_no', $dlvwarehouse->cust_no)
                            ->where('pallet_code', $palletdata['pallet_code'])
                            ->where('type_cd', 'warehouse')
                            ->where('from_wh_cd', $pickwarehouse->cust_no)
                            ->update([
                                'pallet_num' =>$warehouseData->pallet_num + $palletdata['pallet_num'],
                                'updated_at' => $now,
                            ]);
                            Log::info('nick debug');
                            Log::info($dlvwarehouse->cust_no);
                            Log::info($pickwarehouse->cust_no);
                            Log::info($palletdata['pallet_code']);
    
                            $minusehouseData =  DB::table('mod_pallet_manage')
                            ->where('pallet_code', $palletdata['pallet_code'])
                            ->where('type_cd', 'warehouse')
                            ->where('from_wh_cd', $pickwarehouse->cust_no)
                            ->first();
                            if($backtowarehouse->backdelivery == "Y" ) {
                                Log::info('is back');
                                $minusehouseData =  DB::table('mod_pallet_manage')
                                ->where('pallet_code', $palletdata['pallet_code'])
                                ->where('type_cd', 'warehouse')
                                ->where('wh_no', $pickwarehouse->cust_no)
                                ->first();
                            }
                            if($backtowarehouse->backdelivery == "Y" ) {
                                Log::info('backdelivery Y');
                                Log::info('當前數量'.$minusehouseData->pallet_num);
                                Log::info('處理數量數量'.$palletdata['pallet_num']);
                                DB::table('mod_pallet_manage')
                                ->where('id', $minusehouseData->id)
                                ->update([
                                    'pallet_num' =>$minusehouseData->pallet_num + ($palletdata['pallet_num']),
                                    'updated_at' => $now,
                                ]);
                            } else {
                                $minusehouseData =  DB::table('mod_pallet_manage')
                                ->where('pallet_code', $palletdata['pallet_code'])
                                ->where('type_cd', 'warehouse')
                                ->where('wh_no', $dlvwarehouse->cust_no)
                                ->first();
                                Log::info($palletdata['pallet_code']);
                                Log::info( $dlvwarehouse->cust_no);
                                Log::info('backdelivery N');
                                Log::info('棧板數量');
                                Log::info('當前數量'.$minusehouseData->pallet_num);
                                Log::info('處理數量數量'.$palletdata['pallet_num']);
                                DB::table('mod_pallet_manage')
                                ->where('id', $minusehouseData->id)
                                ->update([
                                    'pallet_num' =>$minusehouseData->pallet_num + ($palletdata['pallet_num'] ),
                                ]);
                            }
        
                        } else {
                            DB::table('mod_pallet_manage')
                            ->insertGetId([
                                'pallet_num'   => $palletdata['pallet_num'],
                                'pallet_code'  => $palletdata['pallet_code'],
                                'pallet_type'  => $palletdata['pallet_name'],
                                'g_key'        => 'DSS',
                                'c_key'        => 'DSS',
                                's_key'        => 'DSS',
                                'd_key'        => 'DSS',
                                'updated_by'   =>  $user->email,
                                'created_by'   =>  $user->email,
                                'created_at'   => $now,
                                'updated_at'   => $now,
                                'type_cd'      => 'warehouse',
                                'type_name'    => '倉庫',
                                'wh_no'        => $dlvwarehouse->cust_no,
                                'wh_name'      => $dlvwarehouse->cname,
                                'from_wh_cd'   => $pickwarehouse->cust_no,
                                'from_wh_name' => $pickwarehouse->cname,
                            ]);
                        }
                        //
                        if(isset($checkDriverexist)) {
                            Log::info($checkDriverexist->wh_no);
                            Log::info($checkDriverexist->wh_name);
                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'from_wh_cd'     => $dlvwarehouse->cust_no,
                                'from_wh_name'   => $dlvwarehouse->cname,
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'move_type'      => '歸還',
                                'control_from'   => 'app',
                                'pallet_num'     => 0 - $palletdata['pallet_num'],
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'truck_cmp_name' => $drivermissionMain->truck_cmp_name,
                                'wh_no'          => $dlvwarehouse->cust_no,
                                'wh_name'        => $dlvwarehouse->cname,
                                'updated_by'     => $user->name,
                                'created_by'     => $user->name,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);
    
                            //扣除司機
                            DB::table('mod_pallet_manage')
                            ->where('owner_by', $checkusers->email)
                            ->where('pallet_code', $palletdata['pallet_code'])
                            ->where('type_cd','driver')
                            ->whereNotNull('from_wh_cd')
                            ->update([
                                'pallet_num'  => $checkDriverexist->pallet_num - $palletdata['pallet_num'],
                                'num_from_wh' => $checkDriverexist->num_from_wh - $palletdata['pallet_num'],
                                'updated_at'  => $now,
                            ]);
                        }
        
                    }
                    // 2023-05-10 下貨增加放行通知
                    $dlvwarehouse = DB::table('mod_warehouse')
                    ->where('address', $dlvdata->cust_address)
                    ->whereNotNull('dc_id')
                    ->first();

                    $senddata = DB::table('users')
                    ->where('role', 'like', '%STORKEEPER%')
                    ->where('check_wh', 'like', '%'.$dlvwarehouse->cust_no.'%')
                    ->get();
                    $deliverymsg =  DB::table('mod_sys_message')
                    ->where('title', '放行推播訊息')
                    ->value('content');

                    $deliverymsg = str_replace('{dlv_no}', $dlvdata->dlv_no, $deliverymsg );
                    $deliverymsg = str_replace('{driver_name}',  $user->name, $deliverymsg);
                    foreach ($senddata as $key => $senduser) {
                        $messagedata    = array();
                        $messagedata['sys_ord_no']   = $dlvdata->ord_no;
                        $messagedata['title']        = '請協助放行';
                        $messagedata['from_by']      = $user->id;
                        $messagedata['from_by_name'] = $user->name;
                        $messagedata['to_by']        = $senduser->email;
                        $messagedata['to_by_name']   = $senduser->name;
                        $messagedata['content']      = $deliverymsg;
                        $messagedata['created_by']   = $user->name;
                        $messagedata['updated_by']   = $user->name;
                        $messagedata['created_at']   = date("Y-m-d H:i:s");
                        $messagedata['updated_at']   = date("Y-m-d H:i:s");
                        // $this->createMessage($messagedata);
                        $this->createFcm($messagedata);
                        FcmHelper::sendToFcm( '協助放行' , $deliverymsg, $senduser->d_token);
                    }
                }

    
                $detail = DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('trs_mode', 'dlv')
                ->first();

                Log::info('normal update');
                
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->whereIn('id',  $request->ids)
                ->where('trs_mode', 'dlv')
                ->where('status' ,'!=', 'FINISHED')
                ->where('status' ,'!=', 'CLOSED')
                ->update([
                    'button_status'      => 'STARTUPLOADING',
                    'button_status_desc' => '待放行',
                    "lock_pallet"        => "Y",
                    "updated_by"         => $user->email,
                    "updated_at"         => Carbon::now()->toDateTimeString(),
                    "arrive_time"        => Carbon::now()->toDateTimeString(),
                ]);

                $checkisover =  DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('status', '!=', 'FINISHED')
                ->count();
    
                if( $checkisover == 0 ){
                    DB::table('mod_dss_dlv')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->update([
                        "status"      => "FINISHED",
                        "status_desc" => "任務完成",
                        "updated_by"  => $user->email,
                        "updated_at"  => Carbon::now()->toDateTimeString(),
                        'finish_time' => Carbon::now()->toDateTimeString(),
                    ]);
                }

                Log::info('update all');
                $dsspick = DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->whereIn('id',  $request->ids)
                ->pluck('wms_ord_no')->toArray();

                //拒收
                DB::table('mod_order')
                ->whereIn('ord_no', $dsspick)
                ->update([
                    'finish_time' => DB::raw("COALESCE(finish_time, '".Carbon::now()->toDateTimeString()."')"),
                    'updated_at'  => Carbon::now()->toDateTimeString(),
                ]);

                $checkisover =  DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('trs_mode', 'dlv')
                ->where('status', '!=', 'FINISHED')
                ->count();
    
                if( $checkisover == 0 ) {
                    DB::table('mod_dss_dlv')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->update([
                        "status"      => "FINISHED",
                        "status_desc" => "任務完成",
                        "updated_by"  => $user->email,
                        "updated_at"  => Carbon::now()->toDateTimeString(),
                    ]);
                }
                $deliverymsg =  DB::table('mod_sys_message')
                ->where('title', '放行推播訊息')
                ->value('content');
                $deliverymsg = str_replace('{dlv_no}', $dlvdata->dlv_no, $deliverymsg );
                $deliverymsg = str_replace('{driver_name}',  $user->name, $deliverymsg);
                // 2023-06-27 問題224 放行 
                $postdata     = array();
                $postdata['content']    = $deliverymsg ;
                $postdata['created_by'] = $user->name;
                $postdata['updated_at'] = date("Y-m-d H:i:s");
                $postdata['created_at'] = date("Y-m-d H:i:s");
                $senddata = array();
                $senddata = DB::table('users')
                ->where('role', 'like', '%STORKEEPER%')
                ->where('check_wh', 'like', '%'.$detail->wh_no.'%')
                ->get();
                $dTokenList = array();
                $messagedataList = array();
                foreach ($senddata as $key => $senduser) {
                    $messagedata    = array();
                    $messagedata['title']        = '請協助放行';
                    $messagedata['to_by']        = $senduser->email;
                    $messagedata['to_by_name']   = $senduser->name;
                    $messagedata['content']      = $deliverymsg ;
                    $messagedata['created_by']   = $user->name;
                    $messagedata['updated_by']   = $user->name;
                    $messagedata['created_at']   = date("Y-m-d H:i:s");
                    $messagedata['updated_at']   = date("Y-m-d H:i:s");
                    array_push($dTokenList, $senduser->d_token);
                    array_push($messagedataList, $messagedata);
                }
                //0802 判斷要不要做推播
                //
                if($dlvdata->status !="FINISHED" && $dlvdata->status !="CLOSED" ) {
                    FcmHelper::batchSendToFcm( '協助放行' , $deliverymsg , $dTokenList);
                    $messagedataArray = collect($messagedataList);
                    $chunks = $messagedataArray->chunk(500);
                    foreach($chunks as $chunk) {
                        Fcm::insert($chunk->toArray());
                    }
                }

                Log::info('轉運結束下貨');
                Log::info('正常結束 轉運結束下貨');
                return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
            }
            if(isset($drivermissionMain)) {
                // Log::info('轉運結束下貨 不正常繼續');
                //
            }
            
            //mod_dss_dlv
            Log::info('一般司機到達完成 繼續');
            // 一般司機到達完成

            //提貨倉庫
            $dsspick = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->whereIn('id', $request->ids)
            ->where('trs_mode', 'pick')
            ->first();
            if(isset($dsspick)) {
                $pickwarehouse = DB::table('mod_warehouse')
                ->where('cname', $dsspick->cust_name)
                ->first(); 
            }

            $dssdlv = DB::table('mod_dss_dlv')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->first();

            $detail = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->whereIn('id', $request->ids)
            ->where('trs_mode', 'dlv')
            ->first();

            //UPDATE mod_order

            ///

            $backnum = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('trs_mode','dlv')
            ->where('backdelivery','Y')
            ->count();

            $totalnum = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('trs_mode','dlv')
            ->count();

            if($backnum == $totalnum) {
                DB::table('mod_dlv_plan')
                ->where('dlv_no', $dssdlv->dlv_no)
                ->whereIn('id', $request->ids)
                ->where('trs_mode', 'dlv')
                ->update([
                    'status'      => 'FINISHED',
                    'updated_at'  => Carbon::now()->toDateTimeString(),
                    "arrive_time" => Carbon::now()->toDateTimeString(),
                ]);
            } else {

            }
            $callbackNum = 0;
            $pressPallets = array();
            $backPallets = array();
            foreach ($custdoPallets as $key => $custdoPallet) {
                $pressPallets[$custdoPallet['pallet_name']] = 0;
                $backPallets[$custdoPallet['pallet_name']] = 0;
                foreach ($custdoPallet['detail'] as $key => $row) {
                    if( $row['type'] == "press" ) {
                        $pressPallets[$custdoPallet['pallet_name']] += $row['num'];
                    } else if($row['type'] == "back") {
                        $backPallets[$custdoPallet['pallet_name']] += $row['num'];
                    }
                    $callbackNum += $row['num'];
                }

            }

            DB::table('mod_pallet_manage_log')
            ->where('is_update','N')
            ->where('type_cd','driver')
            ->where('dlv_no',$detail->dlv_no)
            ->where('wms_ord_no',$detail->wms_ord_no)
            ->where('move_type','司機完成')
            ->where('control_from','app')
            ->update([
                'is_update' =>'Y'
            ]);
            // 取得最後一筆修改的
            $getfinalupdate = DB::table('mod_pallet_manage_log')
            ->where('is_update','N')
            ->where('type_cd','driver')
            ->where('dlv_no',$detail->dlv_no)
            ->where('wms_ord_no',$detail->wms_ord_no)
            ->where('move_type','司機完成')
            ->where('control_from','app')
            ->orderBy('created_at', 'desc')
            ->first();
            
            //清除舊資料
            if(isset($getfinalupdate)) {
                $logfordelete = DB::table('mod_pallet_manage_log')
                ->where('is_update','N')
                ->where('type_cd','driver')
                ->where('dlv_no',$detail->dlv_no)
                ->where('wms_ord_no',$detail->wms_ord_no)
                ->where('move_type','司機完成')
                ->where('control_from','app')
                ->where('created_at','!=', $getfinalupdate->created_at)
                ->where('ready_go','N')
                ->pluck('id')->toArray();
                Log::info('log for delete 1 ');
                Log::info($logfordelete);
                DB::table('mod_pallet_manage_log')
                ->where('is_update','N')
                ->where('type_cd','driver')
                ->where('dlv_no',$detail->dlv_no)
                ->where('wms_ord_no',$detail->wms_ord_no)
                ->where('move_type','司機完成')
                ->where('control_from','app')
                ->where('created_at','!=', $getfinalupdate->created_at)
                ->where('ready_go','N')
                ->delete();
           


                $revertdata = DB::table('mod_pallet_manage_log')
                ->where('is_update','N')
                ->where('type_cd','driver')
                ->where('dlv_no',$detail->dlv_no)
                ->where('move_type','司機完成')
                ->where('control_from','app')
                ->where('created_at','=', $getfinalupdate->created_at)
                ->get();
            
                foreach ($revertdata as $revertkey => $revertrow) {
                    $driverrevert = DB::table('mod_pallet_manage')
                    ->where('owner_by', $user->email)
                    ->where('pallet_type', $revertrow->pallet_name)
                    ->where('dlv_no', $dssdlv->dlv_no)
                    ->first();
                    DB::table('mod_pallet_manage')
                    ->where('owner_by', $user->email)
                    ->where('pallet_type', $revertrow->pallet_name)
                    ->where('dlv_no', $dssdlv->dlv_no)
                    ->update([
                        'pallet_num' => $driverrevert->pallet_num + $revertrow['press_num'],
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);
                }
            }
            //

            /////////////////判斷數量防呆
            $now = date("Y-m-d H:i:s");
            $overnum = 'N';
            $overnumMessage = '';
            $palletflag = "N";
            foreach ($custdoPallets as $key => $palletdata) {
                $palletflag = "Y";
                Log::info('司機 棧板');
                $resultnum =  ($pressPallets[$palletdata['pallet_name']] * -1) + $backPallets[$palletdata['pallet_name']];
                Log::info($resultnum);
                Log::info('司機 棧板 end');
                $fochecknum = DB::table('mod_pallet_manage')
                ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                ->where('owner_by', $user->email)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('dlv_no', $dssdlv->dlv_no)
                ->where('type_cd','driver')
                ->whereNotNull('from_wh_cd')
                ->first();
                if(!isset($fochecknum) || $pressPallets[$palletdata['pallet_name']] > $fochecknum->pallet_num ) {
                    $overnum = 'Y';
                    Log::info('請確認數量 司機');
                    $overnumMessage = $overnumMessage.$palletdata['pallet_name'].',';
                }
            }
            if($overnum == "Y") {
                return response()->json(['msg' => $overnumMessage.'請確認數量', 'code' => '01', 'data'=> null]);
            } 
            foreach ($custdoPallets as $key => $palletdata) {
                //司機減少
                $palletflag = "Y";
                $driverPallet = DB::table('mod_pallet_manage')
                ->where('owner_by', $user->email)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('dlv_no', $dssdlv->dlv_no)
                ->first();
                $fochecknum = DB::table('mod_pallet_manage')
                ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                ->where('owner_by', $user->email)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('dlv_no', $dssdlv->dlv_no)
                ->where('type_cd','driver')
                ->whereNotNull('from_wh_cd')
                ->first();
                Log::info('司機 棧板');
                $resultnum =  ($pressPallets[$palletdata['pallet_name']] * -1) + $backPallets[$palletdata['pallet_name']];
                Log::info($resultnum);
                Log::info('司機 棧板 end');
                
                if(!isset($fochecknum) || $pressPallets[$palletdata['pallet_name']] > $fochecknum->pallet_num ) {
                    Log::info('請確認數量 司機');
                    return response()->json(['msg' => $palletdata['pallet_name'].'請確認數量', 'code' => '01', 'data'=> null]);
                }
                


                $checkCustexist = DB::table('mod_pallet_manage')
                ->where('cust_name', $detail->cust_name)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('wh_no', $driverPallet->wh_no)
                ->where('wh_name', $driverPallet->wh_name)
                ->where('from_wh_cd', $driverPallet->from_wh_cd)
                ->where('from_wh_name', $driverPallet->from_wh_name)
                ->first();

                // if($palletdata['pallet_num'] > $checkCustexist->pallet_num) {
                //     return response()->json(['msg' => '棧板數量異常', 'code' => '01', 'data'=> null ]);
                // }
                if(isset($driverPallet)) {


                    Log::info('司機');
                    Log::info($driverPallet->pallet_num .'-'. $pressPallets[$palletdata['pallet_name']] .'+'. $backPallets[$palletdata['pallet_name']]);
                    Log::info('司機end');
                    $drivermission = DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->where('dlv_ord_no', $request->dlv_ord_no)
                    ->whereIn('id', $request->ids)
                    ->where('trs_mode', 'dlv')
                    ->first();

                    if($drivermission->button_status == 'FINISHED') {
                        //完成提前結束
                        // Log::info('完成提前結束 需確認');
                        // $palletdata['pallet_num'] = 0;
                        // return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  null]);
                    }
                    Log::info($drivermission->status);
                    Log::info('driverPallet id');
                    Log::info($driverPallet->id);
                    Log::info($driverPallet->pallet_num .'/'. $pressPallets[$palletdata['pallet_name']] .'/'. $backPallets[$palletdata['pallet_name']]);
                    $forupdate = DB::table('mod_pallet_manage')
                    ->where('owner_by', $user->email)
                    ->where('pallet_code', $palletdata['pallet_code'])
                    ->where('dlv_no', $dssdlv->dlv_no)
                    ->where('pallet_num','>', 0)
                    ->get();
                    //2023 06-20 下貨下超過一個倉庫的棧板
                    $totalminus = $pressPallets[$palletdata['pallet_name']];

                    $backtotalminus = $backPallets[$palletdata['pallet_name']];
                    //防呆判斷 至少 > 1
                    if($totalminus > 0 ) {
                        foreach ($forupdate as $key => $updaterow) {
                            # code...
                            Log::info('update id');
                            Log::info($updaterow->id);
                            Log::info('此筆需扣掉:'.$totalminus);
                            if($updaterow->pallet_num >= $totalminus) {
                                DB::table('mod_pallet_manage')
                                ->where('id', $updaterow->id)
                                ->update([
                                    'pallet_num'  => $updaterow->pallet_num - $totalminus,
                                    'num_from_wh' => $updaterow->num_from_wh - $totalminus,
                                    'updated_at'  => date("Y-m-d H:i:s"),
                                ]);

                                DB::table('mod_pallet_manage_log')
                                ->insert([
                                    'ready_go'       => 'Y',
                                    'wms_ord_no'     => $detail->wms_ord_no,
                                    'move_type'      => '司機完成',
                                    'control_from'   => 'app',
                                    'owner_by'       => $user->email,
                                    'owner_name'     => $user->name,
                                    'cust_no'        => $detail->owner_cd,
                                    'cust_name'      => $detail->cust_name,
                                    'pallet_num'     => $totalminus * - 1,
                                    'back_num'       => $backPallets[$palletdata['pallet_name']],
                                    'press_num'      => 0-$pressPallets[$palletdata['pallet_name']],
                                    'pallet_code'    => $palletdata['pallet_code'],
                                    'pallet_type'    => $palletdata['pallet_name'],
                                    'dlv_no'         => $detail->dlv_no,
                                    'is_update'      => 'N',
                                    'type_cd'        => 'driver',
                                    'type_name'      => '司機',
                                    'truck_cmp_name' => $detail->truck_cmp_name,
                                    'from_wh_cd'     => $updaterow->from_wh_cd,
                                    'from_wh_name'   => $updaterow->wh_name,
                                    'wh_no'          => $updaterow->from_wh_cd,
                                    'wh_name'        => $updaterow->wh_name,
                                    'updated_by'     => $user->email,
                                    'created_by'     => $user->email,
                                    'created_at'     => $now,
                                    'updated_at'     => $now,
                                ]);
                                break;
                            } else {
                                DB::table('mod_pallet_manage')
                                ->where('id', $updaterow->id)
                                ->update([
                                    'pallet_num'  => 0,
                                    'updated_at'  => date("Y-m-d H:i:s"),
                                ]);

                                DB::table('mod_pallet_manage_log')
                                ->insert([
                                    'ready_go'       => 'Y',
                                    'wms_ord_no'     => $detail->wms_ord_no,
                                    'move_type'      => '司機完成',
                                    'control_from'   => 'app',
                                    'owner_by'       => $user->email,
                                    'owner_name'     => $user->name,
                                    'cust_no'        => $detail->owner_cd,
                                    'cust_name'      => $detail->cust_name,
                                    'pallet_num'     => $updaterow->pallet_num * -1 ,
                                    'back_num'       => $backPallets[$palletdata['pallet_name']],
                                    'press_num'      => 0-$pressPallets[$palletdata['pallet_name']],
                                    'pallet_code'    => $palletdata['pallet_code'],
                                    'pallet_type'    => $palletdata['pallet_name'],
                                    'dlv_no'         => $detail->dlv_no,
                                    'is_update'      => 'N',
                                    'type_cd'        => 'driver',
                                    'type_name'      => '司機',
                                    'truck_cmp_name' => $detail->truck_cmp_name,
                                    'from_wh_cd'     => $updaterow->from_wh_cd,
                                    'from_wh_name'   => $updaterow->wh_name,
                                    'wh_no'          => $updaterow->from_wh_cd,
                                    'wh_name'        => $updaterow->wh_name,
                                    'updated_by'     => $user->email,
                                    'created_by'     => $user->email,
                                    'created_at'     => $now,
                                    'updated_at'     => $now,
                                ]);
                                $totalminus = $totalminus - $updaterow->pallet_num ;
                            }
                        }
                    }
                    //2023-06-26 回板
                    Log::info('回板資料 id');
                    Log::info($backtotalminus );
                    if($backtotalminus > 0 ) {
                        $forupdate = DB::table('mod_pallet_manage')
                        ->where('owner_by', $user->email)
                        ->where('pallet_code', $palletdata['pallet_code'])
                        ->where('dlv_no', $dssdlv->dlv_no)
                        ->where('wh_no', $driverPallet->wh_no)
                        ->first();
                        # code...
                        Log::info('update id');
                        Log::info($forupdate->id);
                        Log::info('此筆需增加:'.$backtotalminus);
                        if(isset($forupdate)) {
                            DB::table('mod_pallet_manage')
                            ->where('id', $forupdate->id)
                            ->update([
                                'pallet_num'  => $forupdate->pallet_num + $backtotalminus,
                                'updated_at'  => date("Y-m-d H:i:s"),
                            ]);

                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'move_type'      => '司機完成',
                                'control_from'   => 'app',
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $backtotalminus,
                                'back_num'       => $backPallets[$palletdata['pallet_name']],
                                'press_num'      => 0-$pressPallets[$palletdata['pallet_name']],
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'dlv_no'         => $detail->dlv_no,
                                'is_update'      => 'N',
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'truck_cmp_name' => $detail->truck_cmp_name,
                                'from_wh_cd'     => $forupdate->from_wh_cd,
                                'from_wh_name'   => $forupdate->wh_name,
                                'wh_no'          => $forupdate->from_wh_cd,
                                'wh_name'        => $forupdate->wh_name,
                                'updated_by'     => $user->email,
                                'created_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);
                        } else {
                            Log::info('司機回板增加');
                            DB::table('mod_pallet_manage')
                            ->insert([
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'num_from_wh'    => $totalminus,
                                'pallet_num'     => $totalminus,
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'truck_cmp_name' => $dlvdata->truck_cmp_name,
                                'ready_go'       => 'Y',
                                'g_key'          => 'DSS',
                                'c_key'          => 'DSS',
                                's_key'          => 'DSS',
                                'd_key'          => 'DSS',
                                'created_by'     => $user->email,
                                'updated_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'dlv_no'         => $request->dlv_no,
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->wh_name,
                                'wh_no'          => $updaterow->from_wh_cd,
                                'wh_name'        => $updaterow->wh_name,
                            ]);

                            DB::table('mod_pallet_manage_log')
                            ->insert([
                                'ready_go'       => 'Y',
                                'wms_ord_no'     => $detail->wms_ord_no,
                                'move_type'      => '司機完成',
                                'control_from'   => 'app',
                                'owner_by'       => $user->email,
                                'owner_name'     => $user->name,
                                'cust_no'        => $detail->owner_cd,
                                'cust_name'      => $detail->cust_name,
                                'pallet_num'     => $backtotalminus,
                                'back_num'       => $backPallets[$palletdata['pallet_name']],
                                'press_num'      => 0-$pressPallets[$palletdata['pallet_name']],
                                'pallet_code'    => $palletdata['pallet_code'],
                                'pallet_type'    => $palletdata['pallet_name'],
                                'dlv_no'         => $detail->dlv_no,
                                'is_update'      => 'N',
                                'type_cd'        => 'driver',
                                'type_name'      => '司機',
                                'truck_cmp_name' => $detail->truck_cmp_name,
                                'from_wh_cd'     => $updaterow->from_wh_cd,
                                'from_wh_name'   => $updaterow->wh_name,
                                'wh_no'          => $updaterow->from_wh_cd,
                                'wh_name'        => $updaterow->wh_name,
                                'updated_by'     => $user->email,
                                'created_by'     => $user->email,
                                'created_at'     => $now,
                                'updated_at'     => $now,
                            ]);

                        }
                        
                    }
                    // DB::table('mod_pallet_manage')
                    // ->where('id', $driverPallet->id)
                    // ->update([
                    //     'pallet_num' => $driverPallet->pallet_num - $pressPallets[$palletdata['pallet_name']] + $backPallets[$palletdata['pallet_name']] ,
                    //     'updated_at' => date("Y-m-d H:i:s"),
                    // ]);
                    DB::table('mod_pallet_manage_log')
                    ->where('is_update', 'N')
                    ->where('type_cd', 'cust')
                    ->where('dlv_no', $detail->dlv_no)
                    ->where('move_type', '貨物接收')
                    ->where('control_from', 'app')
                    ->update([
                        'is_update' =>'Y'
                    ]);
                    $resultnumforcust =  $pressPallets[$palletdata['pallet_name']] + ($backPallets[$palletdata['pallet_name']]* -1);
                    Log::info('客戶增加 棧板 end');
                    DB::table('mod_pallet_manage_log')
                    ->insert([
                        'ready_go'         => 'Y',
                        'move_type'        => '貨物接收',
                        'control_from'     => 'app',
                        'wms_ord_no'       => $detail->wms_ord_no,
                        'cust_no'          => $detail->cust_name,
                        'cust_name'        => $detail->cust_name,
                        'pallet_num'       => $resultnumforcust,
                        'back_num'         => $backPallets[$palletdata['pallet_name']],
                        'press_num'        => $pressPallets[$palletdata['pallet_name']],
                        'pallet_code'      => $palletdata['pallet_code'],
                        'pallet_type'      => $palletdata['pallet_name'],
                        'dlv_no'           => $detail->dlv_no,
                        'is_update'        => 'N',
                        'type_cd'          => 'cust',
                        'type_name'        => '客戶',
                        'truck_cmp_name'   => $detail->truck_cmp_name,
                        'goods_owner'      => $detail->owner_cd,
                        'goods_owner_name' => $detail->owner_name,
                        'from_wh_cd'       => $driverPallet->from_wh_cd,
                        'from_wh_name'     => $driverPallet->from_wh_name,
                        'wh_no'            => $driverPallet->wh_no,
                        'wh_name'          => $driverPallet->wh_name,
                        'created_by'       => $user->email,
                        'updated_by'       => $user->email,
                        'created_at'       => $now,
                        'updated_at'       => $now,
                    ]);

                    // dd($checkCustexist);
                    if(isset($checkCustexist)) {
                        Log::info('已存在客戶進行修改');
                        Log::info($checkCustexist->pallet_num .'+'. $resultnumforcust );
                        DB::table('mod_pallet_manage')
                        ->where('id', $checkCustexist->id)
                        ->update([
                            'pallet_num' => $checkCustexist->pallet_num + $resultnumforcust,
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]);
                    } else {
                        $now = date("Y-m-d H:i:s");
                        Log::info('不存在客戶新增');
                        Log::info($resultnumforcust);
                        DB::table('mod_pallet_manage')
                        ->insert([
                            'cust_no'          => $detail->owner_cd,
                            'cust_name'        => $detail->cust_name,
                            'pallet_num'       => $resultnumforcust,
                            'pallet_code'      => $palletdata['pallet_code'],
                            'pallet_type'      => $palletdata['pallet_name'],
                            'goods_owner'      => $detail->owner_cd,
                            'goods_owner_name' => $detail->owner_name,
                            'g_key'            => 'DSS',
                            'c_key'            => 'DSS',
                            's_key'            => 'DSS',
                            'd_key'            => 'DSS',
                            'created_by'       => $user->email,
                            'updated_by'       => $user->email,
                            'created_at'       => $now,
                            'updated_at'       => $now,
                            'type_cd'          => 'cust',
                            'type_name'        => '客戶',
                            'wh_no'            => $driverPallet->wh_no,
                            'wh_name'          => $driverPallet->wh_name,
                            'from_wh_cd'       => $driverPallet->from_wh_cd,
                            'from_wh_name'     => $driverPallet->from_wh_name,
                        ]);
                    }
                } else {
                    Log::info('不存在司機 出現此LOG 應查詢');
                }
            }

            if(count($checkSp) > 0) {
                Log::info('update all 456');
                $updateStatus = '任務回倉';
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('trs_mode', 'dlv')
                ->whereIn('id', $request->ids)
                ->whereIn('ord_no', $checkSp)
                ->whereNotNull('twice_checkin')
                ->update([
                    'button_status'      => 'STARTUPLOADING',
                    'button_status_desc' => '待放行',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                    'lock_pallet'        => 'Y',
                    "updated_by"         => $user->email,
                    "updated_at"         => Carbon::now()->toDateTimeString(),
                ]);
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('trs_mode', 'dlv')
                ->whereIn('id', $request->ids)
                ->where('button_status_desc', '!=', $updateStatus)
                ->whereIn('ord_no', $checkSp)
                ->whereNull('twice_checkin')
                ->update([
                    'button_status'      => 'CHECKIN',
                    'button_status_desc' => $updateStatus,
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                    'lock_pallet'        => 'Y',
                    "updated_by"         => $user->email,
                    "updated_at"         => Carbon::now()->toDateTimeString(),
                    "twice_checkin"      => Carbon::now()->toDateTimeString(),
                ]);
                $deliverymsg =  DB::table('mod_sys_message')
                ->where('title', '放行推播訊息')
                ->value('content');
                $deliverymsg = str_replace('{dlv_no}', $dlvdata->dlv_no, $deliverymsg );
                $deliverymsg = str_replace('{driver_name}',  $user->name, $deliverymsg);
                $postdata     = array();
                $postdata['content']    = $deliverymsg;
                $postdata['created_by'] = $user->name;
                $postdata['updated_at'] = date("Y-m-d H:i:s");
                $postdata['created_at'] = date("Y-m-d H:i:s");
                $senddata = array();
                
                $checkdlvdata = DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->whereIn('id', $request->ids)
                ->first();

                $senddata = DB::table('users')
                ->where('role', 'like', '%STORKEEPER%')
                ->where('check_wh', 'like', '%'.$checkdlvdata->wh_no.'%')
                ->get();

                $dTokenList = array();
                $messagedataList = array();
                foreach ($senddata as $key => $senduser) {
                    $messagedata    = array();
                    $messagedata['title']        = '請協助放行';
                    $messagedata['to_by']        = $senduser->email;
                    $messagedata['to_by_name']   = $senduser->name;
                    $messagedata['content']      = $deliverymsg;
                    $messagedata['created_by']   = $user->name;
                    $messagedata['updated_by']   = $user->name;
                    $messagedata['created_at']   = date("Y-m-d H:i:s");
                    $messagedata['updated_at']   = date("Y-m-d H:i:s");
                    array_push($dTokenList, $senduser->d_token);
                    array_push($messagedataList, $messagedata);
                }
                //0802 判斷要不要做推播
                //
                if($dlvdata->status !="FINISHED" && $dlvdata->status !="CLOSED" ) {
                    FcmHelper::batchSendToFcm( '協助放行' , $deliverymsg, $dTokenList);
                    $messagedataArray = collect($messagedataList);
                    $chunks = $messagedataArray->chunk(500);
                    foreach($chunks as $chunk) {
                        Fcm::insert($chunk->toArray());
                    }
                }

            } else {
                Log::info('沒有特殊回報');
                if($backnum == $totalnum) {
                    Log::info('純逆物流˙');
                    DB::table('mod_dlv_plan')
                    ->where('dlv_ord_no', $request->dlv_ord_no)
                    ->whereIn('id', $request->ids)
                    ->where('backdelivery','Y')
                    ->update([
                        'lock_pallet'        => 'Y',
                        'button_status'      => 'STARTUPLOADING',
                        'button_status_desc' => '待放行',
                        'status'             => 'DLV',
                        'status_desc'        => '任務已分派',
                    ]);
                } else {
                    Log::info('非逆物流˙');
                    //web-碳排計算設定 A023032000011
                    DB::table('mod_dlv_plan')
                    ->whereIn('id', $request->ids)
                    ->where('backdelivery','N')
                    ->update([
                        'report_pallet'      => 'N',
                        'button_status'      => 'FINISHED',
                        'button_status_desc' => '回報資料',
                        'status'             => 'FINISHED',
                        'status_desc'        => '任務完成',
                        'finish_time'        => Carbon::now()->toDateTimeString(),
                    ]);
                    DB::table('mod_dlv_plan')
                    ->whereIn('id', $request->ids)
                    ->where('backdelivery','Y')
                    ->update([
                        'lock_pallet'        => 'Y',
                        'button_status'      => 'STARTUPLOADING',
                        'button_status_desc' => '待放行',
                        'status'             => 'DLV',
                        'status_desc'        => '任務已分派',
                    ]);
                }
            }
            //第一筆額外判斷
            $firstdata = DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('dlv_ord_no', $request->dlv_ord_no)
            ->whereIn('id', $request->ids)
            ->first();
            $imgcount = DB::table('mod_file')
            ->where('ref_no', $firstdata->wms_ord_no)
            ->where('type_no', 'pickdlv')
            ->count();
            $buttondescforid   = '修改回報資料';
            $buttonstatusforid = 'FINISHED';
            $statusforid       = '';
            $statusdescforid   = '';
            Log::info('palletflag start');
            Log::info($palletflag);
            Log::info('palletflag end');
            if($imgcount == 0 ) {
                $statusdescforid = '任務完成';
                $statusforid     = 'FINISHED';
            } else {
                $statusdescforid = '已結案';
                $statusforid     = 'CLOSED';
            }
            //回報資料
            if(count($checkSp) > 0) {
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('dlv_ord_no', $request->dlv_ord_no)
                ->where('trs_mode', 'dlv')
                ->update([
                    'report_pallet'      => 'Y',
                    'button_status'      => 'STARTUPLOADING',
                    'button_status_desc' => '待放行',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                ]);
            } else {
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->where('dlv_ord_no', $request->dlv_ord_no)
                ->update([
                    'report_pallet'      => 'Y',
                    'button_status'      => $buttonstatusforid,
                    'button_status_desc' => $buttondescforid,
                    'status'             => $statusforid,
                    'status_desc'        => $statusdescforid,
                ]);
            }
            //第一筆額外判斷 end

            $forapidetail = DB::table('mod_dlv_plan')
            ->whereIn('id', $request->ids)
            ->get();
            foreach ($forapidetail as $key => $apidetail) {
                DB::table('sys_wms_api')
                ->insert([
                    'wms_ord_no'  => $apidetail->wms_ord_no,
                    'ord_no'      => $apidetail->ord_no,
                    'status'      => 'FINISHED',
                    'status_desc' => '配送完成',
                    'action_time' => Carbon::now()->toDateTimeString(),
                    'created_by'  => $user->email,
                    'updated_by'  => $user->email,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString(),
                ]);
            }
            Log::info('update single');
            //提貨倉庫
            $dsspick = DB::table('mod_dlv_plan')
            ->whereIn('id', $request->ids)
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('backdelivery','N')
            ->pluck('wms_ord_no')->toArray();

            if($backnum == $totalnum) {
            } else {
                Log::info('update 配送完成');
                DB::table('mod_order')
                ->whereIn('ord_no', $dsspick)
                ->update([
                    'status'      => 'FINISHED',
                    'status_desc' => '配送完成',
                    'finish_time' => DB::raw("COALESCE(finish_time, '".Carbon::now()->toDateTimeString()."')"),
                    'updated_at'  => Carbon::now()->toDateTimeString(),
                ]);
            }
            Log::info('count');
            $checkisover =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('trs_mode', 'dlv')
            ->where('status', '!=', 'FINISHED')
            ->count();

            $backdeliverycount =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $request->sys_dlv_no)
            ->where('trs_mode', 'dlv')
            ->where('backdelivery','N')
            ->where('status', '!=', 'FINISHED')
            ->count();

            if( $checkisover == 0 ) {
                if(  $backdeliverycount == $checkisover) {
                    // DB::table('mod_dss_dlv')
                    // ->where('sys_dlv_no', $request->sys_dlv_no)
                    // ->update([
                    //     "status"      => "FINISHED",
                    //     "status_desc" => "任務完成",
                    //     "updated_by"  => $user->email,
                    //     "updated_at"  => Carbon::now()->toDateTimeString(),
                    // ]);
                } else {
                    DB::table('mod_dss_dlv')
                    ->where('sys_dlv_no', $request->sys_dlv_no)
                    ->update([
                        "status"      => "FINISHED",
                        "status_desc" => "任務完成",
                        "updated_by"  => $user->email,
                        "updated_at"  => Carbon::now()->toDateTimeString(),
                    ]);
                }

            }
            Log::info('update finished time');
            // Log::info($dsspick->ord_no);
            // DB::table('mod_order')
            // ->where('ord_no', $dsspick->ord_no)
            // ->update([
            //     'status'            => 'FINISHED',
            //     'status_desc'       => '配送完成',
            //     'finish_time' => Carbon::now()->toDateTimeString(),
            // ]);

            Log::info('正常結束下貨');
            $this->updatedashboardstatus();
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=>  null]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
        return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
    }
    
    public function confirmdlvpallet(Request $request) {
        Log::info("confirmdlvpallet");
        Log::info($request->all());
        $user = Auth::user();
        $palletdatas = $request->palletdatas;
        $editType    = $request->edit_type;
        // $request->dlv_no = 'A022102800085';
        $today = date("Y-m-d");
        $needupdate = "N";
        // wh_no
        $ogwh = $request->wh_no;
        $checkWh = explode(',',$ogwh);
        $deliveryWh = explode(',',$user->delivery_wh);
        $seewh = array_merge( $checkWh,$deliveryWh);
        try {
            $tempforwh = substr($request->wh_no,0,2);
            if($tempforwh != "A0"  && $tempforwh != "B0") {
                $request->wh_no = $tempforwh.'-1';
            }
            $now = date("Y-m-d H:i:s");
            //mark
            //mod_dlv_plan
            $dlvdata = DB::table('mod_dss_dlv')
            ->where('dlv_no', $request->dlv_no)
            ->first();
            // $whaddress = DB::table('mod_warehouse')
            // ->groupBy('address')
            // ->pluck('address')
            // ->toArray();
            $finalid =  DB::table('mod_driver_checkin')
            ->where('phone', $request->driver_phone)
            ->orderBy('id','desc')
            ->first();
            $checkdetail = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('lock_pallet', 'Y')
            ->count();
            if(isset($dlvdata) && $request->ready_go == "Y" && $dlvdata->status == "UNTREATED" ) {
                return response()->json(['msg' => '此單尚未報到不可放行', 'code' => '01', 'data'=> null ]);
            }
            $warehouseinfo = DB::table('mod_warehouse')
            ->where('cust_no', $request->wh_no)
            ->first();

            $deliverycount = DB::table('mod_pallet_manage_log')
            ->where('owner_by', $request->driver_phone)
            ->where('wh_no', $warehouseinfo->cust_no)
            ->where('dlv_no', $request->dlv_no)
            ->orderBy('delivery_count', 'desc')
            ->value('delivery_count');

            $maxdeliverycount = DB::table('mod_pallet_manage_log')
            ->where('dlv_no', $request->dlv_no)
            ->orderBy('delivery_count', 'desc')
            ->value('delivery_count');
            
            if(isset($finalid)) {
                $checkWhinsert = DB::table('mod_delivery_log')
                ->where('checkin_id', $finalid->id)
                ->where('dlv_no', $request->dlv_no)
                ->where('wh_no', $request->wh_no)
                ->count();
            } else {
                $checkWhinsert = 0;
            }
            Log::info('before');
            Log::info($deliverycount);
            if (isset($finalid) && (empty($finalid->discharged_status) || $finalid->discharged_status !='()' )) {
                $deliverycount = $deliverycount + 1;
            }
            $deliverycount = (int) $deliverycount;
            $checkWhinsert = (int) $checkWhinsert;
            Log::info('after');
            Log::info($deliverycount);
            Log::info( $request->ready_go.'///'.$deliverycount .'////'.  $checkWhinsert);
            if($request->ready_go == "N" && $checkWhinsert >= 1) {
                Log::info('應跳出訊息');
                return response()->json(['msg' => '此張單已放行無法新增棧板', 'code' => '01', 'data'=> null ]);
            }

            if(isset($warehouseinfo)) {
                Log::info($warehouseinfo->leave_decision);
                if($warehouseinfo->leave_decision == "warehouse" && $request->ready_go == "Y" ) {
                    DB::table('mod_mark')
                    ->select('cust_no','cust_name')
                    ->where('cust_no', $request->mark_no)
                    ->update([
                        'in_use' => 'N'
                    ]);

                    Log::info('final id');
                    Log::info($finalid->id);
                    if($finalid->delivery_type != "還板報到" ) {
                        if(!empty($request->cd_descp)) {
                            DB::table('mod_driver_checkin')
                            ->where('id', $finalid->id)
                            ->update([
                                'status'            => 'export',
                                'status_desc'       => '離場',
                                'leave_time'        => Carbon::now()->toDateTimeString(),
                                'updated_at'        => Carbon::now()->toDateTimeString(),
                                'discharged_status' => empty($request->cd_descp) ? "" :'('.$request->cd_descp.')',
                            ]);
                        }
                    }
                }
            }
            $logfodelete2 =  DB::table('mod_pallet_manage_log')
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd','!=', 'warehouse')
            ->where('from_wh_cd', $request->wh_no)
            ->where('owner_by', $request->driver_phone)
            ->where('wh_no', $warehouseinfo->cust_no)
            ->pluck('id')
            ->toArray();
            Log::info('log for delete 2 ');
            Log::info($logfodelete2);
            DB::table('mod_pallet_manage_log')
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd','!=', 'warehouse')
            ->where('from_wh_cd', $request->wh_no)
            ->where('owner_by', $request->driver_phone)
            ->where('wh_no', $warehouseinfo->cust_no)
            ->where('ready_go','=', 'N')
            ->where('move_type','=', '借出')
            ->delete();

            $checkusers = DB::table('users')
            ->where('email', $request->driver_phone)
            ->first();

            //2023-06-17 增加放行次數到棧板紀錄
            $checkincount = 0 ;
            //1 先找出目前此車次編號最大放行次數
            $maxdeliverycount = DB::table('mod_pallet_manage_log')
            ->where('dlv_no', $request->dlv_no)
            ->orderBy('delivery_count', 'desc')
            ->value('delivery_count');
            // 
            // $totaldeliverycount = DB::table('mod_delivery_log')
            // ->where('dlv_no', $request->dlv_no)
            // ->where('checkin_id', $finalid->id)
            // ->first();
            // Log::info('final checkin id:');
            // Log::info($finalid->id);
            if(isset($finalid->id) ) {
                $totaldeliverycount = DB::table('mod_pallet_manage_log')
                ->where('dlv_no', $request->dlv_no)
                ->where('checkin_id', $finalid->id)
                ->orderBy('delivery_count', 'desc')
                ->value('delivery_count');
            }

            if($editType == 'edit') {
                $checkincount = "1";
            } else if(isset($totaldeliverycount) ) {
                $checkincount =  $maxdeliverycount;
            } else {
                //
                $checkincount =  $maxdeliverycount + 1 ;
            }

            Log::info('結果:'.$checkincount);
            // delivery_count

            $checkstatus = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('wh_no', $request->wh_no)
            ->where('backdelivery','N')
            ->where('button_status_desc', '上貨完成')
            ->pluck('ord_no')
            ->toArray();
            $wmsstr = implode(', '."\n", $checkstatus);
            $checkstatus = count($checkstatus );
            if( $checkstatus > 0 && $request->ready_go == "Y" ) {
                return response()->json(['msg' =>  '錯誤資訊：未完成上貨'."\n"."客戶訂單編號："."\n".$wmsstr, 'code' => '01', 'data'=> null ]);
            }

            //pallet_num
            DB::table('mod_mark_reserve')
            ->where('driver_phone', $request->driver_phone)
            ->where('reserve_date', $today)
            ->update([
                'mark_no' => $request->mark_no,
                'status' => $request->cd_descp,
            ]);
            $finalid =  DB::table('mod_driver_checkin')
            ->where('phone', $request->driver_phone)
            ->orderBy('id','desc')
            ->first();
            if(isset($finalid)) {
                if(!empty($request->cd_descp)) {
                    DB::table('mod_driver_checkin')
                    ->where('id', $finalid->id)
                    ->update([
                        'updated_at'        => Carbon::now()->toDateTimeString(),
                        'discharged_status' => '('.$request->cd_descp.')',
                    ]);
                }
            }
            Log::info('放行狀態 修改 end');

            if($request->dlv_no == "還板放行") {
                $finalid =  DB::table('mod_driver_checkin')
                ->where('phone', $request->driver_phone)
                ->orderBy('id','desc')
                ->first();
                Log::info('還板放行id');
                Log::info($finalid->id);
                Log::info('還板放行id');
                // discharged_status
                if($warehouseinfo->leave_decision == "warehouse" && $request->ready_go == "Y" ) {
                    if(!empty($request->cd_descp)) {
                        DB::table('mod_driver_checkin')
                        ->where('id', $finalid->id)
                        ->update([
                            'leave_time'  => Carbon::now()->toDateTimeString(),
                            'updated_at'        => Carbon::now()->toDateTimeString(),
                            'discharged_status' => '('.$request->cd_descp.')',
                        ]);
                    }
                } else {
                    if(!empty($request->cd_descp)) {
                        DB::table('mod_driver_checkin')
                        ->where('id', $finalid->id)
                        ->update([
                            'updated_at'        => Carbon::now()->toDateTimeString(),
                            'discharged_status' => '('.$request->cd_descp.')',
                        ]);
                    }
                }
                return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
            }
            if( isset($request->mark_no) ) {
                DB::table('mod_dss_dlv')
                ->where('dlv_no', $request->dlv_no)
                ->update([
                    'mark_no' => $request->mark_no
                ]);
            }
            //2023-07-26 一次放行所選倉庫以及可放行倉庫
            if($checkdetail > 0 ) {
                Log::info('has locak data');
                $dlvdetail = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('lock_pallet', 'Y')
                ->pluck('wms_ord_no')->toArray();
            } else {
                Log::info('do not has locak data');
                $dlvdetail = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('button_status_desc', '待放行')
                ->pluck('wms_ord_no')
                ->toArray();
                Log::info(count($dlvdetail));
                Log::info($dlvdetail);
            }


            // if(!isset($warehouseinfo) == "Y") {
            //     $warehouseinfo = DB::table('mod_warehouse')
            //     ->whereIn('cust_no',explode(',',$user->check_wh) )
            //     ->first();
            // }


            DB::table('mod_pallet_manage')
            ->where('owner_by', $request->driver_phone)
            ->where('wh_no', $warehouseinfo->cust_no)
            ->where('dlv_no', $request->dlv_no)
            ->update([
                'is_old' => "Y"
            ]);

            //2023-07-26 一次放行所選倉庫以及可放行倉庫
            DB::table('mod_pallet_manage')
            ->where('owner_by', $request->driver_phone)
            ->where('wh_no', $warehouseinfo->cust_no)
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd','!=', 'warehouse')
            ->where('ready_go','=', 'N')
            ->delete();
            $lognum = 0;
            foreach ($palletdatas as $key => $palletdata) {
                $lognum = $lognum + $palletdata['pallet_num'];
                $needupdate = "Y";
                Log::info('every data');
                Log::info($palletdata['pallet_num']);

                $checkDriverexist = DB::table('mod_pallet_manage')
                ->where('owner_by', $request->driver_phone)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('wh_no', $warehouseinfo->cust_no)
                ->where('dlv_no', $request->dlv_no)
                ->whereNull('is_old')
                ->first();


                if(!isset($checkusers )) {
                    return response()->json(['msg' => '此司機不存在', 'code' => '01', 'data'=> null ]);
                }
                
                $now = date("Y-m-d H:i:s");
                //扣倉庫
                $warehouseData =  DB::table('mod_pallet_manage')
                ->where('wh_no', $request->wh_no)
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('type_cd', 'warehouse')
                ->first();

                if(!isset($warehouseData)) {
                    return response()->json(['msg' => '此棧板在倉庫中沒有數量', 'code' => '01', 'data'=> null ]);
                }

                // mod_pallet_manage
                if($palletdata['pallet_num'] > $warehouseData->pallet_num) {
                    return response()->json(['msg' => $palletdata['pallet_name'].'超過倉庫庫存數量', 'code' => '01', 'data'=> null ]);
                }
                if($request->ready_go == "Y" ) {
                    if(isset($warehouseData)) {
                        $mangetotaldata =  DB::table('mod_pallet_manage_total')
                        ->where('owner_by', $warehouseinfo->cust_no)
                        ->where('type_cd','warehouse')
                        ->whereNull('from_wh_cd')
                        ->first();
    
                        DB::table('mod_pallet_manage')
                        ->where('id', $warehouseData->id)
                        ->update([
                            'pallet_num' =>$warehouseData->pallet_num - $palletdata['pallet_num'],
                        ]);
                        //扣總數
                        DB::table('mod_pallet_manage_total')
                        ->where('id', $mangetotaldata->id)
                        ->update([
                            'pallet_num' =>$mangetotaldata->pallet_num - $palletdata['pallet_num'],
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]);

                        DB::table('mod_pallet_manage_log')
                        ->insert([
                            'ready_go'       => 'Y',
                            'truck_cmp_name' => $dlvdata->truck_cmp_name,
                            'owner_by'       => $checkusers->email,
                            'owner_name'     => $checkusers->name,
                            'move_type'      => '借出',
                            'control_from'   => 'app',
                            'pallet_num'     => 0-$palletdata['pallet_num'],
                            'pallet_code'    => $palletdata['pallet_code'],
                            'pallet_type'    => $palletdata['pallet_name'],
                            'type_cd'        => 'warehouse',
                            'type_name'      => '倉庫',
                            'dlv_no'         => $request->dlv_no,
                            'wh_no'          => $warehouseinfo->cust_no,
                            'wh_name'        => $warehouseinfo->cname,
                            'created_by'     => $user->email,
                            'updated_by'     => $user->email,
                            'created_at'     => $now,
                            'updated_at'     => $now,
                        ]);
    
                    }
                }

                if(isset($checkDriverexist)) {
                    DB::table('mod_pallet_manage_log')
                    ->insert([
                        'ready_go'       => 'Y',
                        'checkin_id'     => isset($finalid->id) ? $finalid->id : -999,
                        'delivery_count' => $checkincount,
                        'move_type'      => '借出',
                        'control_from'   => 'app',
                        'owner_by'       => $checkusers->email,
                        'owner_name'     => $checkusers->name,
                        'pallet_num'     => $palletdata['pallet_num'],
                        'pallet_code'    => $palletdata['pallet_code'],
                        'pallet_type'    => $palletdata['pallet_name'],
                        'type_cd'        => 'driver',
                        'type_name'      => '司機',
                        'truck_cmp_name' => $dlvdata->truck_cmp_name,
                        'dlv_no'         => $request->dlv_no,
                        'from_wh_cd'     => $warehouseinfo->cust_no,
                        'from_wh_name'   => $warehouseinfo->cname,
                        'created_by'     => $user->email,
                        'updated_by'     => $user->email,
                        'created_at'     => $now,
                        'updated_at'     => $now,
                    ]);

                    Log::info('更新紀錄');
                    Log::info($request->driver_phone);
                    Log::info($checkDriverexist->pallet_num);
                    Log::info($palletdata['pallet_num']);

                    DB::table('mod_pallet_manage')
                    ->where('owner_by', $request->driver_phone)
                    ->where('pallet_code', $palletdata['pallet_code'])
                    ->where('dlv_no', $request->dlv_no)
                    ->where('wh_no', $warehouseinfo->cust_no)
                    ->update([
                        'num_from_wh' =>$checkDriverexist->num_from_wh + $palletdata['pallet_num'],
                        'pallet_num' =>$checkDriverexist->pallet_num + $palletdata['pallet_num'],
                        'updated_at' => date("Y-m-d H:i:s"),
                        'ready_go'   => $request->ready_go,
                    ]);
                } else {
                    DB::table('mod_pallet_manage')
                    ->insert([
                        'owner_by'       => $checkusers->email,
                        'owner_name'     => $checkusers->name,
                        'num_from_wh'    => $palletdata['pallet_num'],
                        'pallet_num'     => $palletdata['pallet_num'],
                        'pallet_code'    => $palletdata['pallet_code'],
                        'pallet_type'    => $palletdata['pallet_name'],
                        'truck_cmp_name' => $dlvdata->truck_cmp_name,
                        'ready_go'       => $request->ready_go,
                        'g_key'          => 'DSS',
                        'c_key'          => 'DSS',
                        's_key'          => 'DSS',
                        'd_key'          => 'DSS',
                        'created_by'     => $user->email,
                        'updated_by'     => $user->email,
                        'created_at'     => $now,
                        'updated_at'     => $now,
                        'type_cd'        => 'driver',
                        'type_name'      => '司機',
                        'dlv_no'         => $request->dlv_no,
                        'from_wh_cd'     => $warehouseinfo->cust_no,
                        'from_wh_name'   => $warehouseinfo->cname,
                        'wh_no'          => $warehouseinfo->cust_no,
                        'wh_name'        => $warehouseinfo->cname,
                    ]);

                    DB::table('mod_pallet_manage_log')
                    ->insert([
                        'ready_go'       => 'Y',
                        'checkin_id'     => isset($finalid->id) ? $finalid->id : -999,
                        'delivery_count' => $checkincount,
                        'move_type'      => '借出',
                        'control_from'   => 'app',
                        'owner_by'       => $checkusers->email,
                        'owner_name'     => $checkusers->name,
                        'pallet_num'     => $palletdata['pallet_num'],
                        'pallet_code'    => $palletdata['pallet_code'],
                        'pallet_type'    => $palletdata['pallet_name'],
                        'truck_cmp_name' => $dlvdata->truck_cmp_name,
                        'ready_go'       => $request->ready_go,
                        'type_cd'        => 'driver',
                        'type_name'      => '司機',
                        'dlv_no'         => $request->dlv_no,
                        'from_wh_cd'     => $warehouseinfo->cust_no,
                        'from_wh_name'   => $warehouseinfo->cname,
                        'wh_no'          => $warehouseinfo->cust_no,
                        'wh_name'        => $warehouseinfo->cname,
                        'created_by'     => $user->email,
                        'updated_by'     => $user->email,
                        'created_at'     => $now,
                        'updated_at'     => $now,
                    ]);
                }

            }


            if($request->ready_go == "Y" && $editType == "delivery") {

                $logfodelete2 =  DB::table('mod_pallet_manage_log')
                ->where('dlv_no', $request->dlv_no)
                ->where('type_cd','!=', 'warehouse')
                ->where('from_wh_cd' ,'!=', $ogwh)
                ->where('owner_by', $request->driver_phone)
                ->where('wh_no' ,'!=', $ogwh)
                ->pluck('id')
                ->toArray();
                Log::info('log for delete 2 ');
                Log::info($logfodelete2);
                DB::table('mod_pallet_manage_log')
                ->where('dlv_no', $request->dlv_no)
                ->where('type_cd','!=', 'warehouse')
                ->where('from_wh_cd' ,'!=', $ogwh)
                ->where('owner_by', $request->driver_phone)
                ->where('wh_no' ,'!=', $ogwh)
                ->where('ready_go','=', 'N')
                ->where('move_type','=', '借出')
                ->delete();

                $ldeletemanage =  DB::table('mod_pallet_manage')
                ->where('dlv_no', $request->dlv_no)
                ->where('type_cd','!=', 'warehouse')
                ->where('from_wh_cd' ,'!=', $ogwh)
                ->where('owner_by', $request->driver_phone)
                ->where('wh_no' ,'!=', $ogwh)
                ->pluck('id')
                ->toArray();
                Log::info('log for delete manage 2 ');
                Log::info($ldeletemanage);
                DB::table('mod_pallet_manage')
                ->where('dlv_no', $request->dlv_no)
                ->where('type_cd','!=', 'warehouse')
                ->where('from_wh_cd' ,'!=', $ogwh)
                ->where('owner_by', $request->driver_phone)
                ->where('wh_no' ,'!=', $ogwh)
                ->where('ready_go','=', 'N')
                ->delete();

                $fordeliverylogcount = DB::table('mod_dlv_plan')
                ->select('wh_no')
                ->where('dlv_no', $request->dlv_no)
                ->where('button_status_desc', '待放行')
                ->whereIn('wh_no',$seewh)
                ->groupBy('wh_no')
                ->pluck('wh_no')
                ->toArray();
                foreach ($fordeliverylogcount as $key => $deliverywh) {
                    if( $deliverywh == $ogwh ) {
                        DB::table('mod_delivery_log')
                        ->insert([
                            'pallet_num'        => $lognum,
                            'dlv_no'            => $request->dlv_no,
                            'wh_no'             => $warehouseinfo->cust_no,
                            'wh_name'           => $warehouseinfo->cname,
                            'driver_name'       => $checkusers->name,
                            'driver_phone'      => $checkusers->email,
                            'discharged_status' => $request->cd_descp,
                            'delivery_by'       => $user->name,
                            'delivery_time'     => $now,
                            'created_at'        => $now,
                            'updated_at'        => $now,
                            'checkin_id'        => $finalid->id,
                        ]);
                    } else {
                        $newwhdata = DB::table('mod_warehouse')
                        ->where('cust_no', $deliverywh)
                        ->first();
                        DB::table('mod_delivery_log')
                        ->insert([
                            'pallet_num'        => 0,
                            'dlv_no'            => $request->dlv_no,
                            'wh_no'             => $deliverywh,
                            'wh_name'           => $newwhdata->cname,
                            'driver_name'       => $checkusers->name,
                            'driver_phone'      => $checkusers->email,
                            'discharged_status' => $request->cd_descp,
                            'delivery_by'       => $user->name,
                            'delivery_time'     => $now,
                            'created_at'        => $now,
                            'updated_at'        => $now,
                            'checkin_id'        => $finalid->id,
                        ]);
                    }

                }
            }

            if($needupdate == "Y") {
                if($editType == "delivery") {
                    $totalpallet = DB::table('mod_pallet_manage_log')
                    ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                    ->where('owner_by', $request->driver_phone)
                    ->where('dlv_no', $request->dlv_no)
                    ->where('type_cd','=', 'driver')
                    ->where('delivery_count', $checkincount)
                    ->where('ready_go', 'Y')
                    ->value('pallet_num');
                } else {
                    $totalpallet = DB::table('mod_pallet_manage_log')
                    ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
                    ->where('owner_by', $request->driver_phone)
                    ->where('dlv_no', $request->dlv_no)
                    ->where('type_cd','=', 'driver')
                    ->where('delivery_count', $checkincount)
                    ->value('pallet_num');
                }
                
                Log::info('total pallet');
                DB::table('mod_dss_dlv')
                ->where('dlv_no', $request->dlv_no)
                ->update([
                    'pallet_num' => $totalpallet
                ]);
            }


            if($request->ready_go == "Y" ) {
                Log::info('update 報到 by '.$request->driver_phone);
                Log::info('total pallet');
                Log::info('放行狀態2 修改 start');
                if(!empty($request->cd_descp)) {
                    DB::table('mod_driver_checkin')
                    ->where('id', $finalid->id)
                    ->update([
                        'discharged_status' => '('.$request->cd_descp.')',
                    ]);
                }
                Log::info('放行狀態2 修改 end');
                // 轉運逆物流 特別處理
                if($checkdetail > 0 ) {
                    Log::info($dlvdetail);
                    Log::info('has locak data2');

                    $tranferbyno = DB::table('mod_dlv_plan')
                    ->where('dlv_no', $request->dlv_no)
                    ->where('trs_mode', 'DLV')
                    ->whereNotNull('arrive_time')
                    ->pluck('wms_ord_no')
                    ->toArray();
                    foreach ($tranferbyno as $tranferbynokey => $tranferbynorow) {
                        $tranferFinal =  DB::table('mod_dlv_plan')
                        ->where('dlv_no', '!=', $request->dlv_no)
                        ->where('wms_ord_no', $tranferbynorow)
                        ->where('main_trs_mode', '!=', '轉運')
                        ->count();
                        if($tranferFinal > 0 && $dlvdata->dlv_type_desc == '轉運') {
                            DB::table('mod_order')
                            ->whereIn('ord_no', $dlvdetail)
                            ->update([
                                'status'             => 'FINISHED',
                                'status_desc'        => '配送完成(轉運)',
                                'upload_finish_time' => Carbon::now()->toDateTimeString(),
                                'updated_at'         => Carbon::now()->toDateTimeString(),
                            ]);
                        } else {
                            DB::table('mod_order')
                            ->whereIn('ord_no', $dlvdetail)
                            ->update([
                                'status'             => 'FINISHED',
                                'status_desc'        => '配送完成',
                                'upload_finish_time' => Carbon::now()->toDateTimeString(),
                                'updated_at'         => Carbon::now()->toDateTimeString(),
                            ]);
                        }
                    }
                    //2023-07-26 一次放行所選倉庫以及可放行倉庫
                    //2023-06-13 逆物流正常單 狀態補正
                    $forupload = DB::table('mod_dlv_plan')
                    ->where('dlv_no', $request->dlv_no)
                    ->whereIn('wh_no',$seewh)
                    ->where('button_status_desc', '待放行')
                    ->where('trs_mode', 'pick')
                    ->pluck('wms_ord_no')
                    ->toArray();
                    Log::info('記錄特殊形況');
                    Log::info($forupload);
                    Log::info('記錄特殊形況end');
                    DB::table('mod_order')
                    ->whereIn('ord_no', $forupload)
                    ->update([
                        'status'             => 'UPLOADFINISH',
                        'status_desc'        => '提貨完成',
                        'upload_finish_time' => Carbon::now()->toDateTimeString(),
                        'updated_at'         => Carbon::now()->toDateTimeString(),
                    ]);
                } else {
                    Log::info('do not has locak data');

                    //
                    $tranferbyno = DB::table('mod_dlv_plan')
                    ->where('dlv_no', $request->dlv_no)
                    ->where('trs_mode', 'DLV')
                    ->whereNotNull('arrive_time')
                    ->pluck('wms_ord_no')
                    ->toArray();
                    Log::info( $tranferbyno );
                    if(count($tranferbyno) > 0) {
                        Log::info('轉運完成');
                        DB::table('mod_order')
                        ->whereIn('ord_no', $tranferbyno)
                        ->update([
                            'status'             => 'FINISHED',
                            'status_desc'        => '配送完成',
                            'upload_finish_time' => Carbon::now()->toDateTimeString(),
                            'updated_at'         => Carbon::now()->toDateTimeString(),
                        ]);
                    } else {
                        Log::info('一般完成');
                        DB::table('mod_order')
                        ->whereIn('ord_no', $dlvdetail)
                        ->update([
                            'status'             => 'UPLOADFINISH',
                            'status_desc'        => '提貨完成',
                            'upload_finish_time' => Carbon::now()->toDateTimeString(),
                            'updated_at'         => Carbon::now()->toDateTimeString(),
                        ]);
                    }


                }
    

                
                DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvdata->dlv_no)
                ->where('trs_mode', 'dlv')
                ->where('lock_pallet','N')
                ->where('button_status', '!=', 'FINISHED')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'status'     => 'DLV',
                    'status_desc' => '任務已分派',
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                $forapidetail = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('status', '!=','FINISHED')
                ->where('wh_no' , $ogwh)
                ->where('trs_mode', 'pick')
                ->get();
                foreach ($forapidetail as $key => $apidetail) {
                    DB::table('sys_wms_api')
                    ->insert([
                        'wms_ord_no'  => $apidetail->wms_ord_no,
                        'ord_no'      => $apidetail->ord_no,
                        'status'      => 'PICKED',
                        'status_desc' => '提貨完成',
                        'action_time' => Carbon::now()->toDateTimeString(),
                        'created_by'  => $user->email,
                        'updated_by'  => $user->email,
                        'created_at'  => Carbon::now()->toDateTimeString(),
                        'updated_at'  => Carbon::now()->toDateTimeString(),
                    ]);
                }
                //2023-07-26 一次放行所選倉庫以及可放行倉庫
                Log::info('放行 UPDATE');
                Log::info($request->dlv_no);
                Log::info(json_encode($seewh));
                DB::table('mod_dlv_plan')
                ->where('trs_mode','pick')
                ->whereIn('wh_no', $seewh)
                ->where('dlv_no', $request->dlv_no)
                ->where('backdelivery', '!=', 'Y')
                ->where('button_status_desc', '待放行')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '修改回報資料',
                    'status'             => 'FINISHED',
                    'status_desc'        => '任務完成',
                    'finish_time'        => Carbon::now()->toDateTimeString(),
                ]);
    
                DB::table('mod_dlv_plan') 
                ->where('dlv_no', $request->dlv_no)
                ->where('trs_mode','dlv')
                ->where('lock_pallet','N')
                ->where('dlv_to_wh','Y')
                ->where('button_status_desc', '待放行')
                ->where('button_status' ,'!=','FINISHED')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'CHECKIN',
                    'button_status_desc' => '報到',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                ]);


                DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('trs_mode','dlv')
                ->where('lock_pallet','N')
                ->where('dlv_to_wh','N')
                ->where('button_status' ,'!=','FINISHED')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'ARRIVE',
                    'button_status_desc' => '到點',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                ]);
                //2023-07-26 一次放行所選倉庫以及可放行倉庫

                DB::table('mod_dlv_plan') 
                ->where('dlv_no', $request->dlv_no)
                ->whereIn('wh_no', $seewh)
                ->where('button_status', 'STARTUPLOADING')
                ->where('trs_mode','dlv')
                ->where('lock_pallet','Y')
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '修改回報資料',
                    'status'             => 'FINISHED',
                    'status_desc'        => '任務完成',
                    'updated_at'         => Carbon::now()->toDateTimeString(),
                    'finish_time'        => Carbon::now()->toDateTimeString(),
                ]);
                //2023-07-26 一次放行所選倉庫以及可放行倉庫
                Log::info('放行 UPDATE');
                Log::info($request->dlv_no);
                Log::info(json_encode($seewh));
                DB::table('mod_dlv_plan')
                ->where('trs_mode','pick')
                ->whereIn('wh_no', $seewh)
                ->where('dlv_no', $request->dlv_no)
                ->where('backdelivery', '!=', 'Y')
                ->where('button_status_desc', '待放行')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '任務完成',
                    'status'             => 'FINISHED',
                    'status_desc'        => '任務完成',
                    'finish_time'        => Carbon::now()->toDateTimeString(),
                ]);
    
                DB::table('mod_dlv_plan') 
                ->where('dlv_no', $request->dlv_no)
                ->where('trs_mode','dlv')
                ->where('lock_pallet','N')
                ->where('dlv_to_wh','Y')
                ->where('button_status_desc', '待放行')
                ->where('button_status' ,'!=','FINISHED')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'CHECKIN',
                    'button_status_desc' => '報到',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                ]);


                DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->where('trs_mode','dlv')
                ->where('lock_pallet','N')
                ->where('dlv_to_wh','N')
                ->where('button_status' ,'!=','FINISHED')
                ->where('status', '!=', 'FINISHED')
                ->update([
                    'button_status'      => 'ARRIVE',
                    'button_status_desc' => '到點',
                    'status'             => 'DLV',
                    'status_desc'        => '任務已分派',
                ]);
                //2023-07-26 一次放行所選倉庫以及可放行倉庫

                DB::table('mod_dlv_plan') 
                ->where('dlv_no', $request->dlv_no)
                ->whereIn('wh_no', $seewh)
                ->where('button_status', 'STARTUPLOADING')
                ->where('trs_mode','dlv')
                ->where('lock_pallet','Y')
                ->where('backdelivery', '!=', 'Y')
                ->where('button_status_desc', '待放行')
                ->where('status', '!=', 'FINISHED')
                ->where('report_pallet',  'Y')
                ->where('trs_mode',  'dlv')
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '任務完成',
                    'status'             => 'FINISHED',
                    'status_desc'        => '任務完成',
                    'updated_at'         => Carbon::now()->toDateTimeString(),
                    'finish_time'        => Carbon::now()->toDateTimeString(),
                ]);
                //2023-07-26 一次放行所選倉庫以及可放行倉庫
                Log::info('逆物流 UPDATE');
                DB::table('mod_dlv_plan')
                ->where('dlv_no', $dlvdata->dlv_no)
                ->where('wh_no', $seewh)
                ->where('trs_mode', 'dlv')
                ->where('lock_pallet','N')
                ->whereNotNull('arrive_time')
                ->where('dlv_to_wh','Y')
                ->where('backdelivery','Y')
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '任務完成',
                    'status'             => 'FINISHED',
                    'status_desc'        => '任務完成',
                    'updated_at'         => Carbon::now()->toDateTimeString(),
                    'finish_time'        => Carbon::now()->toDateTimeString(),
                ]);
                Log::info('update plan ');
                DB::table('mod_dss_dlv')
                ->where('dlv_no', $request->dlv_no)
                ->update([
                    'status'      => 'DLV',
                    'status_desc' => '配送中',
                ]);
            }
            $this->updatedashboardstatus();

            //2023-07-26 一次放行所選倉庫以及可放行倉庫
            DB::table('mod_dlv_plan')
            ->where('dlv_no', $dlvdata->dlv_no)
            ->where('lock_pallet','Y')
            ->update([
                'lock_pallet' => 'N'
            ]);

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' =>$e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }


    public function getpalletBydriver(Request $request) {
        Log::info("getpalletBydriver");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $data = array();

            $checkWarehouse = explode(',',$user->check_wh);

            $warehousedata = DB::table('mod_warehouse')
            ->select('cust_no', 'cname')
            ->whereIn('cust_no',$checkWarehouse)
            ->get();

            $driverNames = DB::table('mod_pallet_manage')
            ->select('owner_name')
            ->where('type_cd','driver' )
            ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
            ->where('ready_go','Y')
            ->groupBy('owner_name')
            ->pluck('owner_name')->toArray();
            foreach ($driverNames as $key => $driverName) {
                $warehousearray = array();
                $palltetdata = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw(' SUM(num_from_wh) as pallet_num')
                    ,'pallet_code',
                    DB::raw(' pallet_type as pallet_name'),
                    DB::raw(' any_value(wh_name) as wh_name'),
                    'owner_by')
                ->where('type_cd','driver' )
                ->where('owner_name',$driverName)
                ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
                ->where('num_from_wh','>', 0)
                ->where('ready_go','Y')
                ->groupBy('pallet_code','pallet_type','owner_by','wh_no','wh_name')
                ->get();

                foreach ($warehousedata as $warehouskey => $warehousrow) {
                        # code...
                    $newpalltetdata = DB::table('mod_pallet_manage')
                    ->select(
                        DB::raw(' SUM(num_from_wh) as pallet_num')
                        ,'pallet_code','wh_no',
                        DB::raw(' pallet_type as pallet_name'),
                        DB::raw(' any_value(wh_name) as wh_name'),
                        'owner_by')
                    ->where('type_cd','driver')
                    ->where('owner_name',$driverName)
                    ->where('wh_no', $warehousrow->cust_no)
                    ->where('num_from_wh','>', 0)
                    ->where('ready_go','Y')
                    ->groupBy('pallet_code','pallet_type','owner_by','wh_no','wh_name')
                    ->get();
                    foreach ($newpalltetdata as $keyforpallet => $rowforpallet) {
                        # code...
                        $rowforpallet->pallet_num = (int)$rowforpallet->pallet_num;
                    }
                    if(count($newpalltetdata) != 0 ){
                        $warehousearray[] = array( 'wh_name'=>$warehousrow->cname,'wh_no'=>$warehousrow->cust_no, 'pallets'=> $newpalltetdata) ;
                    }      
                }

                $totalpallet = 0;
                foreach ($palltetdata as $palletkey => $row) {
                    $row->pallet_num = (int)$row->pallet_num;
                    $totalpallet +=$row->pallet_num;
                }
                $today = date("Y-m-d 00:00:00");
                $backdelivery = 'N';
                if (!$palltetdata->isEmpty()) {
                    $checkin = DB::table('mod_driver_checkin')
                    ->where('created_at', '>=', $today)
                    ->where('phone', $palltetdata[0]->owner_by)
                    ->where('delivery_type', '還板報到')
                    ->whereNull('leave_time')
                    ->orderBy('id','desc')
                    ->count();
                    Log::info('log3');
                    if( $checkin > 0) {
                        $backdelivery = 'Y';
                    }
                }

                $truckcmpname = "";
                $truckcmpno = "";
                if (!$palltetdata->isEmpty()) {
                    $dlvinfo = DB::table('mod_dss_dlv')
                    ->where('driver_phone',$palltetdata[0]->owner_by)
                    ->orderBy('id','desc')
                    ->first();
                    $truckcmpname = $dlvinfo->truck_cmp_name;
                    $truckcmpno   = $dlvinfo->truck_cmp_no;
                } 
                // $row->backdelivery = $backdelivery;
                if (!$palltetdata->isEmpty()) {
                    // $row->backdelivery = $backdelivery;
                    $data[$key]['truck_cmp_name'] = $truckcmpname;
                    $data[$key]['truck_cmp_no']   = $truckcmpno;
                    $data[$key]['driver_name']    = $driverName;
                    $data[$key]['driver_phone']   = $palltetdata[0]->owner_by;
                    $data[$key]['total_pallet']   = (int)$totalpallet;
                    $data[$key]['back_delivery']  = $backdelivery;
                    $data[$key]['pallets']        = $palltetdata;
                    $data[$key]['warehouse']      = $warehousearray;
                    Log::info('log5');
                }
                $data = array_values($data);
            }

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $data]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }

    public function getpalletBydlvno(Request $request) {
        Log::info("getpalletBydlvno");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $driverdata = DB::table('mod_pallet_manage')
            ->select('id','pallet_num','pallet_code','pallet_type','dlv_no')
            ->where('dlv_no', $request->dlv_no)
            ->where('type_cd','driver' )
            ->where('pallet_num','>', 0 )
            ->get();

            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> $driverdata]);
        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }


    public function checkdriverpallet(Request $request) {
        Log::info("checkdriverpallet");
        Log::info($request->all());
        $user = Auth::user();
        $palletdatas = $request->palletdatas;
        try {
            $errorMessage = '';
            foreach ($palletdatas as $key => $palletdata) {
                //加倉庫
                $warehouseData =  DB::table('mod_pallet_manage')
                ->where('wh_no',$request->wh_no )
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('type_cd','warehouse')
                ->first();

                $forcheckpallet = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw('SUM(pallet_num) as pallet_num')
                )
                ->where('owner_by', $request->driver_phone)
                ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('wh_no', $warehouseData->wh_no)
                ->where('type_cd','driver')
                ->value('pallet_num');

                if ($palletdata['pallet_num'] > $forcheckpallet) {
                    $errorMessage .= $palletdata['pallet_name']." : ".$forcheckpallet."個\n"; 
                   
                }
            }
            if (!empty($errorMessage)) {
                return response()->json(['msg' => "目前司機身上只有\n".$errorMessage, 'code' => '01', 'data'=> null ]);
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);

        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }

    public function recycledriverpallet(Request $request) {
        Log::info("recycledriverpallet");
        Log::info($request->all());
        $user = Auth::user();
        $palletdatas = $request->palletdatas;
        try {
            //
            $today = date("Y-m-d");
            if($request->back_delivery == "Y") {

                $warehouseinfo = DB::table('mod_warehouse')
                ->where('cust_no', $request->wh_no)
                ->first();
                $finalid =  DB::table('mod_driver_checkin')
                ->where('phone', $request->driver_phone)
                ->where('delivery_type', '還板報到')
                ->orderBy('id','desc')
                ->first();

                //2023-06-12 寫入放行資料
                DB::table('mod_delivery_log')
                ->insert([
                    'dlv_no'            => '還板放行',
                    'wh_no'             => $warehouseinfo->cust_no,
                    'wh_name'           => $warehouseinfo->cname,
                    'driver_name'       => $finalid->driver_name,
                    'driver_phone'      => $finalid->phone,
                    'discharged_status' => '空車放行',
                    'delivery_by'       => $user->name,
                    'delivery_time'     => Carbon::now()->toDateTimeString(),
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ]);
                Log::info($finalid->id);
                if($warehouseinfo->leave_decision == "warehouse" ) {
                    Log::info('倉庫');
                    DB::table('mod_driver_checkin')
                    ->where('id', $finalid->id)
                    ->update([
                        'status'            => 'export',
                        'status_desc'       => '離場',
                        'discharged_status' => '(空車放行)',
                        'leave_time'        => Carbon::now()->toDateTimeString(),
                    ]);
                } else {
                    Log::info('非倉庫');
                    DB::table('mod_driver_checkin')
                    ->where('id', $finalid->id)
                    ->update([
                        'discharged_status' => '(空車放行)',
                    ]);
                }
            }
            $modPalletManageLogInsertArray = array();
            foreach ($palletdatas as $key => $palletdata) {
                //加倉庫
                $warehouseData =  DB::table('mod_pallet_manage')
                ->where('wh_no',$request->wh_no )
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('type_cd','warehouse')
                ->first();

                //2023-07-03 增加紀錄 
                $logfordata = DB::table('mod_pallet_manage')
                ->select(
                    DB::raw('GROUP_CONCAT(id) as lodids'),
                    DB::raw('SUM(pallet_num) as pallet_num')
                )
                ->where('owner_by', $request->driver_phone)
                ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
                ->where('pallet_code', $palletdata['pallet_code'])
                ->where('wh_no', $warehouseData->wh_no)
                ->where('type_cd', 'driver')
                ->where('ready_go', 'Y')
                ->first();

                Log::info(json_encode($logfordata));
                $forcheckpallet =  $logfordata->pallet_num;

                $checkusers = DB::table('users')
                ->where('email', $request->driver_phone)
                ->first();

                if(!isset($checkusers )) {
                    return response()->json(['msg' => '此司機不存在', 'code' => '01', 'data'=> null ]);
                }
                Log::info('棧板數量');
                Log::info($palletdata['pallet_num']);
                Log::info($forcheckpallet);
                Log::info('棧板數量');
                if($palletdata['pallet_num'] > $forcheckpallet) {
                    return response()->json(['msg' => '輸入棧板數量已超出司機身上掛帳數量', 'code' => '01', 'data'=> null ]);
                }

                $now = date("Y-m-d H:i:s");

                $stationtotal =  DB::table('mod_pallet_manage_total')
                ->where('owner_by', $warehouseData->wh_no)
                ->whereNull('from_wh_cd')
                ->first();
                
                DB::table('mod_pallet_manage_total')
                ->where('id', $stationtotal->id)
                ->update([
                    'pallet_num' => $stationtotal->pallet_num + $palletdata['pallet_num'],
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
                /*DB::table('mod_pallet_manage_log')
                ->insert([
                    'move_type'    => '歸還',
                    'control_from' => 'app',
                    'owner_by'     => $checkusers->email,
                    'owner_name'   => $checkusers->name,
                    'pallet_num'   => $palletdata['pallet_num'],
                    'pallet_code'  => $palletdata['pallet_code'],
                    'pallet_type'  => $palletdata['pallet_name'],
                    'type_cd'      => 'warehouse',
                    'type_name'    => '倉庫',
                    'wh_no'        => $warehouseData->wh_no,
                    'wh_name'      => $warehouseData->wh_name,
                    'created_by'   => $user->email,
                    'updated_by'   => $user->email,
                    'created_at'   => $now,
                    'updated_at'   => $now,
                ]);*/
                array_push($modPalletManageLogInsertArray, array(
                    'ready_go'     => 'Y',
                    'move_type'    => '歸還',
                    'control_from' => 'app',
                    'owner_by'     => $checkusers->email,
                    'owner_name'   => $checkusers->name,
                    'pallet_num'   => $palletdata['pallet_num'],
                    'pallet_code'  => $palletdata['pallet_code'],
                    'pallet_type'  => $palletdata['pallet_name'],
                    'type_cd'      => 'warehouse',
                    'type_name'    => '倉庫',
                    'wh_no'        => $warehouseData->wh_no,
                    'wh_name'      => $warehouseData->wh_name,
                    'created_by'   => $user->email,
                    'updated_by'   => $user->email,
                    'created_at'   => $now,
                    'updated_at'   => $now,
                    'from_wh_cd'   => null,
                    'from_wh_name' => null,
                ));

                if(isset($warehouseData)) {
                    DB::table('mod_pallet_manage')
                    ->whereIn('wh_no',explode(',',$user->check_wh) )
                    ->where('pallet_code', $palletdata['pallet_code'])
                    ->where('type_cd','warehouse')
                    ->update([
                        'pallet_num' =>$warehouseData->pallet_num + $palletdata['pallet_num'],
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);
                }

                if(isset($forcheckpallet)) {
                    //司機
                    /*DB::table('mod_pallet_manage_log')
                    ->insert([
                        'move_type'    => '歸還',
                        'control_from' => 'app',
                        'owner_by'     => $checkusers->email,
                        'owner_name'   => $checkusers->name,
                        'pallet_num'   => $palletdata['pallet_num'] * - 1,
                        'pallet_code'  => $palletdata['pallet_code'],
                        'pallet_type'  => $palletdata['pallet_name'],
                        'type_cd'      => 'driver',
                        'type_name'    => '司機',
                        'wh_no'        => $warehouseData->wh_no,
                        'wh_name'      => $warehouseData->wh_name,
                        'created_by'   => $user->email,
                        'updated_by'   => $user->email,
                        'created_at'   => $now,
                        'updated_at'   => $now,
                        'from_wh_cd'   => $warehouseData->wh_no,
                        'from_wh_name' => $warehouseData->wh_name,
                    ]);*/
                    array_push($modPalletManageLogInsertArray, array(
                        'ready_go'     => 'Y',
                        'move_type'    => '歸還',
                        'control_from' => 'app',
                        'owner_by'     => $checkusers->email,
                        'owner_name'   => $checkusers->name,
                        'pallet_num'   => $palletdata['pallet_num'] * - 1,
                        'pallet_code'  => $palletdata['pallet_code'],
                        'pallet_type'  => $palletdata['pallet_name'],
                        'type_cd'      => 'driver',
                        'type_name'    => '司機',
                        'wh_no'        => $warehouseData->wh_no,
                        'wh_name'      => $warehouseData->wh_name,
                        'created_by'   => $user->email,
                        'updated_by'   => $user->email,
                        'created_at'   => $now,
                        'updated_at'   => $now,
                        'from_wh_cd'   => $warehouseData->wh_no,
                        'from_wh_name' => $warehouseData->wh_name,
                    ));

                    /*$forupdatelog = DB::table('mod_pallet_manage')
                    ->where('owner_by', $request->driver_phone)
                    ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
                    ->where('pallet_code', $palletdata['pallet_code'])
                    ->where('wh_no', $warehouseData->wh_no)
                    ->where('pallet_num','>', 0)
                    ->pluck('id')
                    ->toArray();
                    Log::info('update id total');
                    Log::info( $forupdatelog);*/
                    $forupdate = DB::table('mod_pallet_manage')
                    ->where('owner_by', $request->driver_phone)
                    ->whereIn('from_wh_cd',explode(',',$user->check_wh) )
                    ->where('pallet_code', $palletdata['pallet_code'])
                    ->where('wh_no', $warehouseData->wh_no)
                    ->where('pallet_num','>', 0)
                    ->where('ready_go', 'Y')
                    ->get();

                    $totalminus = $palletdata['pallet_num'];
                    foreach ($forupdate as $key => $updaterow) {
                        # code...
                        Log::info('update id');
                        Log::info($updaterow->id);
                        Log::info('og num');
                        Log::info($updaterow->pallet_num);
                        Log::info('need - num');
                        Log::info($totalminus );
                        if($updaterow->pallet_num >= $totalminus) {
                            DB::table('mod_pallet_manage')
                            ->where('id', $updaterow->id)
                            ->update([
                                'pallet_num'  => $updaterow->pallet_num - $totalminus ,
                                'num_from_wh' => $updaterow->num_from_wh - $totalminus ,
                                'updated_at'  => date("Y-m-d H:i:s"),
                            ]);
                            break;
                        } else {
                            DB::table('mod_pallet_manage')
                            ->where('id', $updaterow->id)
                            ->update([
                                'pallet_num' => 0,
                                'num_from_wh'=> 0,
                                'updated_at' => date("Y-m-d H:i:s"),
                            ]);
                            $totalminus = $totalminus - $updaterow->pallet_num ;
                        }
                    }
                }
            }
            $modPalletManageLogCollect = collect($modPalletManageLogInsertArray);
            $chunks = $modPalletManageLogCollect->chunk(500);
            foreach($chunks as $chunk) {
                DB::table('mod_pallet_manage_log')->insert($chunk->toArray());
            }
            return response()->json(['msg' => 'success', 'code' => '00', 'data'=> null]);

        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => $e->getMessage(), 'code' => '01', 'data'=> null ]);
        }
    }

    public function palletCalNum(Request $request) {
        Log::info("palletCalNum");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $sys_ord_no  = $request->sys_ord_no;
            $dlv_no      = $request->dlv_no;
            $from_type   = $request->from_type;
            $from        = $request->from;
            $to          = $request->to;
            $palletdatas = $request->palletdata;


            $type = "driver";
            if($from_type == "warehouse") {
                $type = "warehouse";
            } else if($from_type == "driver") {
                $type = "driver";
            } else if($from_type == "cust") {
                $type = "cust";
            }

            //配送 倉庫->客戶
            //轉運 倉庫->客戶
            //司機只會做回板跟壓板的數量

            //訂單下方明細多一個棧板資料 = 倉管人員輸入的棧板?
            //訂單配送類型 提配
            //


            //新增 修改的時候
            //司機會自己key
            //定點定線
            foreach ($palletdatas as $key => $palletdata) {
                # code...

            }

            //

        } catch(\Exception $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'code' => '01', 'data'=> null]);
        }
        
    }
    public function delimg(Request $request) {
        Log::info("delimg");
        Log::info($request->all());
        $user       = Auth::user();
        try {
            DB::table('mod_file')
            ->where('id',$request->id)
            ->delete();
            return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
        
    }
    public function upstairs(Request $request) {
        Log::info("upstairs");
        Log::info("上樓api");
        Log::info($request->wms_ord_no);
        Log::info($request->sys_dlv_no);
        Log::info($request->floor);
        Log::info($request->unit_num);
        // Log::info($request->all());
        $user       = Auth::user();
        try {

            $filecount = DB::table('mod_file')
            ->where('ref_no', $request->wms_ord_no)
            ->count();
            $filecount = $filecount +1 ;

            $maindata =  DB::table('mod_dss_dlv')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->first();

            $data =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('wms_ord_no',$request->wms_ord_no)
            ->first();

            DB::table('mod_upstairs')
            ->where('ord_no',$data->wms_ord_no)
            ->delete();

            DB::table('mod_order')
            ->where('ord_no',$request->wms_ord_no)
            ->update([
                'upstairsfloor' => $request->floor,
                'upstairsamt'   => $request->unit_num,
            ]);
            // order         dss
            // cust_ord_no   ord_no

            $filename = 'upstairs_'.$data->owner_name.'_'.$maindata->dlv_date.'_'.$data->ord_no.'_'.$data->cust_name.$filecount.'.png';

            DB::table('mod_upstairs')->insert([
                'ord_no'     => $request->wms_ord_no,
                'floor'      => $request->floor,
                'unit_num'   => $request->unit_num,
                'g_key'      => 'DSS',
                'g_key'      => 'DSS',
                'c_key'      => 'DSS',
                's_key'      => 'DSS',
                'd_key'      => 'DSS',
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);

            $insertData = array();
            if(!empty($request->img)) {
                // Storage::disk('local')->put('uploadimg/'.$filename, base64_decode($request->img));
                $s3 = \Storage::disk('s3');
                $filePath = 'photo/'.$filename;
                $s3->put($filePath, base64_decode($request->img),'public');
                array_push($insertData, [
                    'guid'       => $filename,
                    'ref_no'     => $request->wms_ord_no,
                    'type_no'    => 'upstairs',
                    'trs_mode'   => 'dlv',
                    'g_key'      => 'DSS',
                    'g_key'      => 'DSS',
                    'c_key'      => 'DSS',
                    's_key'      => 'DSS',
                    'd_key'      => 'DSS',
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
                DB::table('mod_file')->insert($insertData);
            }

            return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
    }

    public function downpoint(Request $request) {
        Log::info("mod_downpoint");
        Log::info("下點api");
        // Log::info($request->all());
        $user       = Auth::user();
        try {
            //
            $filecount = DB::table('mod_file')
            ->where('ref_no', $request->wms_ord_no)
            ->count();
            $filecount = $filecount + 1 ;

            $maindata =  DB::table('mod_dss_dlv')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->first();

            $data =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('wms_ord_no',$request->wms_ord_no)
            ->first();

            $filename = 'downpoint_'.$data->owner_name.'_'.$maindata->dlv_date.'_'.$data->ord_no.'_'.$data->cust_name.$filecount.'.png';
            DB::table('mod_downpoint')
            ->where('ord_no',$request->wms_ord_no)
            ->delete();

            DB::table('mod_order')
            ->where('ord_no',$request->wms_ord_no)
            ->update([
                'downpointamt'=>$request->point,
            ]);
            DB::table('mod_downpoint')->insert([
                'ord_no'     => $request->wms_ord_no,
                'point'      => $request->point,
                'g_key'      => 'DSS',
                'c_key'      => 'DSS',
                's_key'      => 'DSS',
                'd_key'      => 'DSS',
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);

            $insertData = array();
            if(!empty($request->img)) {
                // Storage::disk('local')->put('uploadimg/'.$filename, base64_decode($request->img));
                $s3 = \Storage::disk('s3');
                $filePath = 'photo/'.$filename;
                $s3->put($filePath, base64_decode($request->img),'public');
                array_push($insertData, [
                    'guid'       => $filename,
                    'ref_no'     => $request->wms_ord_no,
                    'type_no'    => 'downpoint',
                    'trs_mode'   => 'dlv',
                    'g_key'      => 'DSS',
                    'c_key'      => 'DSS',
                    's_key'      => 'DSS',
                    'd_key'      => 'DSS',
                    'created_by' => $user->email,
                    'updated_by' => $user->email,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
                DB::table('mod_file')->insert($insertData);
            }
            Log::info("resoonse");
            return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
        Log::info("resoonse finished");
        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }
    public function geterrorbyid(Request $request) {
        Log::info("geterror");
        try {
            $data = DB::table('mod_error_report')
            ->where('id',  $request->id)
            ->first();

            $imglist = DB::table('mod_file')
            ->select('id','empty_img','type_no',
                DB::raw("CONCAT('".env('PRINTER_URL')."' , guid)as imgurl")
            )
            ->where('error_id',  $data->uuid)
            ->get();
            $data->img = $imglist;

        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
        return response()->json(['msg' => 'success','code'=>'00', 'data'=> $data]);
    }

    public function errorremark(Request $request) {
        Log::info("errorremark");
        Log::info($request->all());
        //對應 cust_ord_no
        Log::info("errorremark start end ");
        // Log::info($request->all());
        Log::info("errorremark request end");
        $user       = Auth::user();
        try {
            // 客戶名稱+流水號

            $maindata =  DB::table('mod_dss_dlv')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->first();

            $data =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('ord_no',$request->ord_no)
            ->where('trs_mode',$request->trs_mode)
            ->first();
            Log::info("wms_ord_no wms_ord_no wms_ord_no");
            Log::info($data->wms_ord_no);
            $orderdata =  DB::table('mod_order')
            ->where('ord_no',$data->wms_ord_no)
            ->first();
            // $dataforpick =  DB::table('mod_dlv_plan')
            // ->where('sys_dlv_no',$request->sys_dlv_no)
            // ->where('ord_no',$request->ord_no)
            // ->where('trs_mode','pick')
            // ->first();

            $erroreType = DB::table('mod_abnormal')
            ->where('error_cd', $request->error_cd)
            ->where('error_name', $request->error_name)
            ->first();

            $lockflag = 'N';
            //已完成的情況 且特殊異常回報才進行 下再進行下獵
            if(($request->error_name =="客戶拒收" || $request->error_name =="順收") && $data->button_status=="FINISHED") {
                $lockflag = 'Y';
                $updateStatus = '';
                if($request->error_name == '客戶拒收') {
                    $updateStatus = '退貨報到';
                } else {
                    $updateStatus = '順收報到';
                }
                // 2Z520230317009
                Log::info("特殊異常");
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no',$request->sys_dlv_no)
                ->where('ord_no',$request->ord_no)
                ->where('trs_mode',$request->trs_mode)
                ->update([
                    'error_cd'           => $request->error_cd,
                    'error_name'         => $request->error_name,
                    'error_remark'       => $request->error_remark,
                    'error_type'         => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                ]);
            } else {
                Log::info("一般異常");
                if($erroreType->finish_mission == "Y" ) {
                    DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no',$request->sys_dlv_no)
                    ->where('ord_no',$request->ord_no)
                    ->update([
                        'error_cd'           => $request->error_cd,
                        'error_name'         => $request->error_name,
                        'error_remark'       => $request->error_remark,
                        'error_type'         => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                    ]);
                    DB::table('mod_order')
                    ->where('ord_no',$request->wms_ord_no)
                    ->update([
                        'error_type'      => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                        'abnormal_remark' => $request->error_remark,
                        'error_remark'    => $request->error_name,
                        'exp_reason'      => $request->error_cd,
                    ]);
                    // $this->updatedashboardstatus();
                    $this->checkdlvfinish($maindata->dlv_no);
                } else {
                    DB::table('mod_dlv_plan')
                    ->where('sys_dlv_no',$request->sys_dlv_no)
                    ->where('ord_no',$request->ord_no)
                    ->where('trs_mode',$request->trs_mode)
                    ->update([
                        'error_cd'     => $request->error_cd,
                        'error_name'   => $request->error_name,
                        'error_remark' => $request->error_remark,
                        'error_type'   => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                    ]);

                    DB::table('mod_order')
                    ->where('ord_no',$request->wms_ord_no)
                    ->update([
                        'error_type'      => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                        'abnormal_remark' => $request->error_remark,
                        'error_remark'    => $request->error_name,
                        'exp_reason'      => $request->error_cd
                    ]);

                }
            }

            // 客戶名稱 依照 正物流 取配送的 逆物流 取提貨的 a地b送的 取自己的
            $whaddress = DB::table('mod_warehouse')
            ->pluck('address')
            ->toArray();
            $forerror =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('ord_no',$request->ord_no)
            ->whereNotIn('cust_address',$whaddress)
            ->first();
            $errorcustname = "";
            if($data->backdelivery == "Y") {
                $errorcustname = $forerror->cust_name;
            } else {
                $errorcustname = $forerror->cust_name;
            }

            $errorId = DB::table('mod_error_report')
            ->insertGetId([
                'phone'         => $maindata->driver_phone,
                'cust_name'     => $errorcustname,
                'uuid'          => $request->uuid,
                'sys_dlv_no'    => $request->sys_dlv_no,
                'ord_no'        => $request->ord_no,
                'wms_ord_no'    => $request->wms_ord_no,
                'dlv_ord_no'    => $request->dlv_ord_no,
                'error_type'    => isset($erroreType->belong_name) ? $erroreType->belong_name : null,
                'error_cd'      => $request->error_cd,
                'error_name'    => $request->error_name,
                'error_remark'  => $request->error_remark,
                'return_detail' => $request->return_detail,
                'trs_mode'      => $request->trs_mode,
                'owner_cd'      => $data->owner_cd,
                'owner_name'    => $data->owner_name,
                'dc_id'         => $data->dc_id,
                'g_key'         => 'DSS',
                'c_key'         => 'DSS',
                's_key'         => 'DSS',
                'd_key'         => 'DSS',
                'created_by'    => $user->name,
                'updated_by'    => $user->name,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString()
            ]);
            Log::info("errorId");
            Log::info($errorId);

            try {
                $postdata     = array();
                $postdata['content']    = $request->error_name;
                $postdata['created_by'] = $user->name;
                $postdata['updated_at'] = date("Y-m-d H:i:s");
                $postdata['created_at'] = date("Y-m-d H:i:s");
                // $senddata = DB::table('users')
                // ->where('role', 'like', '%STORKEEPER%')
                // ->get();

                $administrative = DB::table('users')
                ->where('role', 'like', '%CS%')
                ->Where(function($query)  use ($orderdata) {
                    if($orderdata->from_wh == '台北倉') {
                        $orderdata->from_wh = '台北';
                    }
                    if($orderdata->from_wh == '觀音一倉1F' || $orderdata->from_wh == '觀音一倉3F'  || $orderdata->from_wh == '觀音一倉4F' ) {
                        $orderdata->from_wh = '觀音一倉';
                    }
                    $query->orwhere('check_wh', 'like', '%'.$orderdata->dc_id.'%');
                    $query->orwhere('check_wh_name', 'like', '%'.$orderdata->from_wh.'%');
                    // $query->whereIn('job_type', ['拉空櫃','換櫃','進貨'])
                    // ->where('mark_no','!=','空地')
                    // ->whereNotNull('mark_no')
                    // ->orWhereNull('discharged_status');
                })
                ->get();

                $dTokenList = array();
                $sendUserIdList = array();
                $messagedataList = array();
                foreach ($administrative as $key => $senduser) {
                    # code...
                    Log::info("userid");
                    Log::info($senduser->id);
                    $messagedata    = array();

                    $messagedata['owner_cd']     = $data->owner_cd;
                    $messagedata['owner_name']   = $data->owner_name;
                    $messagedata['cust_name']    = $errorcustname;
                    $messagedata['error_id']     = $errorId;
                    $messagedata['sys_ord_no']   = $request->ord_no;
                    $messagedata['title']        = '異常回報';
                    $messagedata['from_by']      = $user->id;
                    $messagedata['from_by_name'] = $user->name;
                    $messagedata['to_by']        = $senduser->id;
                    $messagedata['to_by_name']   = $senduser->name;
                    $messagedata['content']      = $request->error_name;
                    $messagedata['created_by']   = $user->name;
                    $messagedata['updated_by']   = $user->name;
                    $messagedata['phone']        = $user->email;
                    $messagedata['created_at']   = date("Y-m-d H:i:s");
                    $messagedata['updated_at']   = date("Y-m-d H:i:s");
                    if($senduser->check_owner != "*" ) {
                        //
                        $userOwnerGroup = explode(',',$senduser->check_owner);

                        $checkOwner = DB::table('mod_customer_relation')
                        ->whereIn('group_cd', $userOwnerGroup)
                        ->where('cust_no',  $data->owner_cd)
                        ->count();

                        /*$orderdata =  DB::table('mod_order')
                        ->where('ord_no',$data->wms_ord_no)
                        ->first();*/

                        $checkWarehouse = explode(',',$senduser->check_wh);
                        $checkWarehousename = explode(',',$senduser->check_wh_name);

                        Log::info("checkOwner");
                        Log::info($checkOwner);
                        /*Log::info("dc_id");
                        Log::info($orderdata->dc_id);
                        Log::info("from_wh");
                        Log::info($orderdata->from_wh);*/
                        Log::info("checkWarehouse");
                        Log::info($checkWarehouse);
                        
                        // if($checkOwner >= 1 && (in_array($orderdata->from_wh, $checkWarehousename) || in_array($dataforpick->dc_id, $checkWarehouse) ))  {
                        if($checkOwner >= 1 )  {
                            Log::info("double userid");
                            Log::info($senduser->id);
                            //$this->createMessage($messagedata);
                            //FcmHelper::sendToFcm( '異常回報' , $request->error_name, $senduser->d_token);
                            array_push($messagedataList, $messagedata);
                            array_push($dTokenList, $senduser->d_token);
                            array_push($sendUserIdList, $senduser->id);
                            //$this->updateUnReadCount($senduser->id);
                            $this->sendDsvTmsUnReadMessageByWebsocket($postdata, $senduser->id);
                        }
                    }  else {
                        // 0410 異常回報 待測試
                        // $userwh = explode(',',$senduser->check_wh);
                        // if(in_array($data->wh_no, $userwh) ) {
                        // }
                        // $checkWarehouse = explode(',',$senduser->check_wh);
                        // if( in_array($data->dc_id, $checkWarehouse) )  {
                        //     $this->createMessage($messagedata);
                        //     FcmHelper::sendToFcm( '異常回報' , $request->error_name, $senduser->d_token);
                        //     $this->updateUnReadCount($senduser->id);
                        //     $this->sendDsvTmsUnReadMessageByWebsocket($postdata, $senduser->id);
                        // }
                        //$this->createMessage($messagedata);
                        //FcmHelper::sendToFcm( '異常回報' , $request->error_name, $senduser->d_token);
                        array_push($messagedataList, $messagedata);
                        array_push($dTokenList, $senduser->d_token);
                        array_push($sendUserIdList, $senduser->id);
                        //$this->updateUnReadCount($senduser->id);
                        $this->sendDsvTmsUnReadMessageByWebsocket($postdata, $senduser->id);
                    }
                }
                // 2023-06-13 By Tony 推播 & mod_message insert & users unReadCount update 改成批量
                FcmHelper::batchSendToFcm( '異常回報' , $request->error_name, $dTokenList);
                $messagedataArray = collect($messagedataList);
                $chunks = $messagedataArray->chunk(500);
                foreach($chunks as $chunk) {
                    Message::insert($chunk->toArray());
                }
                $this->batchUpdateUnReadCount($sendUserIdList);
            } catch(\Exception $e) {
                Log::info($e->getMessage());
                Log::info($e->getLine());
            }

        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
        Log::info('finishreport');
        $this->updatedashboardstatus();
        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }
    public function errorreport(Request $request) {
        Log::info("errorreport");
        Log::info("errorreport start end ");
        Log::info($request->all());
        Log::info("errorreport request end");
        $user       = Auth::user();
        try {
            // 客戶名稱+流水號
            $filename = $request->fileName;
            $checkname = DB::table('mod_file')
            ->where('guid', $filename)
            ->first();
            if(isset($checkname)) {
                Log::info("同檔名照片移除");
                return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
            }
            $insertData = array();
            if(!empty($request->img) && ($filename!=null)) {
                $s3 = \Storage::disk('s3');
                $filePath = 'photo/'.$filename;
                $s3->put($filePath, base64_decode($request->img),'public');
                array_push($insertData, [
                    'error_id'    => $request->uuid,
                    'guid'        => $filename,
                    'ref_no'      => $request->wms_ord_no,
                    'type_no'     => 'error',
                    'file_format' => 'jpg',
                    'empty_img'   => $request->return_detail,
                    'trs_mode'    => $request->trs_mode,
                    'g_key'       => 'DSS',
                    'g_key'       => 'DSS',
                    'c_key'       => 'DSS',
                    's_key'       => 'DSS',
                    'd_key'       => 'DSS',
                    'created_by'  => $user->name,
                    'updated_by'  => $user->name,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString()
                ]);
                DB::table('mod_file')->insert($insertData);
            }
        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01','errormessage'=>$e->getLine()]);
        }
        Log::info('finishreport img');
        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }
    public function updateUnReadCountByname($username) {
        $updateUser = DB::table('users')
        ->where('name', $username)
        ->first();

       DB::table('users')
        ->where('id', $updateUser->id)
        ->update([
            "unReadCount" => $updateUser->unReadCount +1
        ]);
        $finalcount =  $updateUser->unReadCount +1;
        return $updateUser;
    }
    public function updateUnReadCount($userId) {
        $updateUser = DB::table('users')
        ->where('id', $userId)
        ->first();

       DB::table('users')
        ->where('id', $userId)
        ->update([
            "unReadCount" => $updateUser->unReadCount +1
        ]);
        $finalcount =  $updateUser->unReadCount +1;
        return $finalcount;
    }
    public function batchUpdateUnReadCount($userIds) {
        $updateUsers = DB::table('users')
        ->whereIn('id', $userIds)
        ->get();

        foreach ($updateUsers as $updateUser) {
            DB::table('users')
            ->where('id', $updateUser->id)
            ->update([
                "unReadCount" => $updateUser->unReadCount +1
            ]);
        }
    }
    public function checkdlvfinish ($dlvno) {
        //未完成數量
        $user       = Auth::user();
        $checkisover =  DB::table('mod_dlv_plan')
        ->where('dlv_no', $dlvno)
        ->where('trs_mode', 'dlv')
        ->where('status', '!=', 'FINISHED')
        ->count();

        $backdeliverycount =  DB::table('mod_dlv_plan')
        ->where('dlv_no', $dlvno)
        ->where('trs_mode', 'dlv')
        ->where('backdelivery','N')
        ->where('status', '!=', 'FINISHED')
        ->count();

        if( $checkisover == 0 ) {
            if(  $backdeliverycount == $checkisover) {
                // 不計算逆物流
                // DB::table('mod_dss_dlv')
                // ->where('sys_dlv_no', $request->sys_dlv_no)
                // ->update([
                //     "status"      => "FINISHED",
                //     "status_desc" => "任務完成",
                //     "updated_by"  => $user->email,
                //     "updated_at"  => Carbon::now()->toDateTimeString(),
                // ]);
            } else {
                DB::table('mod_dss_dlv')
                ->where('dlv_no', $dlvno)
                ->update([
                    "status"      => "FINISHED",
                    "status_desc" => "任務完成",
                    "updated_by"  => $user->email,
                    "updated_at"  => Carbon::now()->toDateTimeString(),
                ]);
            }
        }

    }
    public function updatedashboardstatus () {
        $postData = array(
            "groupId" => "groupId_Dsvupdatedatsbboard",
            "msg" => 'update'
        );
        try {
            // Log::info("enter DsvTmsUnReadMessageByWebsocket");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://dss-ws.target-ai.com:9599/websocket/notifyByGroupId',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($postData, JSON_UNESCAPED_UNICODE),
                CURLOPT_HTTPHEADER => array(
                    'cache-control: no-cache',
                    'content-type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            // Log::info($response);
        }
        catch(\Exception $e) {
            Log::info("GpsDataByWebsocket error");
            Log::info($e->getMessage());
            Log::info($e->getLine());
        }
    }
    public function sendDsvTmsUnReadMessageByWebsocket ($newsData, $sendUser) {
        // Log::info("app sendDsvTmsUnReadMessageByWebsocket");
        $updateUser = DB::table('users')
        ->where('id', $sendUser)
        ->first();
        $allUnRead     = $updateUser->unReadCount;
        $unReadData    = array();

        $unReadData['newData']     = $newsData;
        $unReadData['unReadCount'] = $allUnRead;
        $postData = array(
            "groupId" => "groupId_DsvTmsUnReadMessageByWebsocket{$sendUser}",
            "msg" => $unReadData
        );
        try {
            // Log::info("enter DsvTmsUnReadMessageByWebsocket");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://dss-ws.target-ai.com:9599/websocket/notifyByGroupId',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($postData, JSON_UNESCAPED_UNICODE),
                CURLOPT_HTTPHEADER => array(
                    'cache-control: no-cache',
                    'content-type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            // Log::info($response);
        }
        catch(\Exception $e) {
            Log::info("GpsDataByWebsocket error");
            Log::info($e->getMessage());
            Log::info($e->getLine());
        }
        // Log::info("DsvTmsUnReadMessageByWebsocket finish");
        return;
    }
    public function createMessage(array $data) {
        return Message::create($data);
    }
    public function createFcm(array $data) {
        return Fcm::create($data);
    }

    public function signreport(Request $request) {
        Log::info("signreport");
        Log::info($request->sys_dlv_no);
        Log::info($request->wms_ord_no);
        Log::info($request->remark);
        Log::info($request->pickdlv_flag);
        $user       = Auth::user();
        try {
            DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('wms_ord_no',$request->wms_ord_no)
            ->update([
                'remark'     => $request->remark,
            ]);
            $typeno = 'normal';
            $signpic = null;
            $delivereyflag = '否';
            if($request->pickdlv_flag =="Y") {
                $typeno = 'pickdlv';
                $signpic ='1';
            }
            // 客戶名稱+流水號
            $filecount = DB::table('mod_file')
            ->where('ref_no', $request->wms_ord_no)
            ->count();
            $filecount = $filecount +1 ;

            $maindata =  DB::table('mod_dss_dlv')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->first();

            $data =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no',$request->sys_dlv_no)
            ->where('wms_ord_no',$request->wms_ord_no)
            ->first();

            $filename = $data->owner_name.'_'.$maindata->dlv_date.'_'.$data->ord_no.'_'.$data->cust_name.$filecount.'.png';
            Log::info('123');
            
            if($request->pickdlv_flag =="Y") {
                DB::table('mod_order')
                ->where('ord_no',$request->wms_ord_no)
                ->update([
                    'sign_pic'      => $signpic,
                    'driver_remark' => $request->remark
                ]);
                DB::table('mod_dlv_plan')
                ->where('sys_dlv_no',$request->sys_dlv_no)
                ->where('wms_ord_no',$request->wms_ord_no)
                ->where('button_status_desc','!=','待放行')
                ->update([
                    'status'             => 'CLOSED',
                    'status_desc'        => '已結案',
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => '修改回報資料',
                ]);
            } else {
                DB::table('mod_order')
                ->where('ord_no',$request->wms_ord_no)
                ->update([
                    'driver_remark' => $request->remark
                ]);
            }

            $insertData = array();
            if(!empty($request->img)) {
                // Storage::disk('local')->put('uploadimg/'.$filename, base64_decode($request->img));
                $s3 = \Storage::disk('s3');
                $filePath = 'photo/'.$filename;
                $s3->put($filePath, base64_decode($request->img),'public');
                array_push($insertData, [
                    'guid'        => $filename,
                    'ref_no'      => $request->wms_ord_no,
                    'trs_mode'    => $request->trs_mode,
                    'type_no'     => $typeno,
                    'file_format' => 'jpg',
                    'empty_img'   => 'signreport',
                    'g_key'       => 'DSS',
                    'g_key'       => 'DSS',
                    'c_key'       => 'DSS',
                    's_key'       => 'DSS',
                    'd_key'       => 'DSS',
                    'created_by'  => $user->email,
                    'updated_by'  => $user->email,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString()
                ]);
                DB::table('mod_file')->insert($insertData);
            }


        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }

    public function deleveryimgupload(Request $request) {
        Log::info("deleveryimgupload");
        Log::info($request->all());
        $user       = Auth::user();
        try {
            DB::table('mod_driver_checkin')
            ->where('id',$request->id)
            ->update([
                'remark'     => $request->remark,
            ]);
            $typeno = 'normal';
            if($request->pickdlv_flag == "Y" ) {
                $typeno = 'pickdlv';
                Log::info('上傳簽收照片');
                DB::table('mod_order')
                ->where('ord_no',$request->wms_ord_no)
                ->update([
                    'driver_remark'   => $request->remark
                ]);
            }
            // 客戶名稱+流水號
            // $filecount = DB::table('mod_file')
            // ->where('ref_no', $request->id)
            // ->count();
            // $filecount = $filecount +1 ;

            $filename = $request->fileName;

            $insertData = array();
            if(!empty($request->img)) {
                $s3 = \Storage::disk('s3');
                $filePath = 'photo/'.$filename;
                $s3->put($filePath, base64_decode($request->img),'public');

                array_push($insertData, [
                    'guid'        => $filename,
                    'ref_no'      => $request->id,
                    'trs_mode'    => 'delivery',
                    'type_no'     => $typeno,
                    'file_format' => 'jpg',
                    'empty_img'   => 'deleveryimgupload',
                    'g_key'       => 'DSS',
                    'g_key'       => 'DSS',
                    'c_key'       => 'DSS',
                    's_key'       => 'DSS',
                    'd_key'       => 'DSS',
                    'created_by'  => $user->email,
                    'updated_by'  => $user->email,
                    'created_at'  => Carbon::now()->toDateTimeString(),
                    'updated_at'  => Carbon::now()->toDateTimeString()
                ]);
                DB::table('mod_file')->insert($insertData);
            }


        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00', 'data'=> null]);
    }

    public function getwarehouse(Request $request) {
        Log::info("getwarehouse");
        Log::info($request->all());
        $user       = Auth::user();
        //點選派車單號資料
        // 'dlv_no' => 'C023032900008',
        // 'wh_user' => 'Y',      
        try {
            $data = array();

            $dssdata = DB::table('mod_dss_dlv')
            ->where('dlv_no', $request->dlv_no)
            ->first();

            $checkindata = DB::table('mod_driver_checkin')
            ->where('phone', $dssdata->driver_phone)
            ->orderby('id','desc')
            ->first();

            $dssdetail = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->pluck('owner_cd')->toArray();

            $whnos = DB::table('mod_cust_wh')
            ->where('dc_id', $dssdata->dc_id)
            ->whereIn('cust_no', $dssdetail)
            ->pluck('wh_no')->toArray();

            $wh = DB::table('mod_warehouse')
            ->select(
                DB::raw('cust_no as wh_no'),
                DB::raw('cname as wh_name')
            )
            ->whereIn('cust_no', $whnos)
            ->get();

            if($dssdata->dlv_type_desc == '轉運') {
                //增加轉運的
                $dlvaddress = DB::table('mod_dlv_plan')
                ->where('trs_mode', 'dlv')
                ->where('dlv_no', $request->dlv_no)
                ->pluck('cust_address')
                ->toArray();
                $dlvwh = DB::table('mod_warehouse')
                ->whereNotNull('dc_id')
                ->whereIn('address', $dlvaddress)
                ->get();
                foreach ($dlvwh as $dlvwhkey => $dlvwhrow) {
                    $whnos[] =  $dlvwhrow->cust_no;
                }
                $detaildcid = DB::table('mod_dlv_plan')
                ->where('dlv_no', $request->dlv_no)
                ->pluck('wh_no')->toArray();

                //
                if($dssdata->dc_id == $checkindata->dc_id ) {
                    $wh = DB::table('mod_warehouse')
                    ->select(
                        DB::raw('cust_no as wh_no'),
                        DB::raw('cname as wh_name')
                    )
                    // ->whereIn('dc_id', [$checkindata->dc_id])
                    ->whereIn('cust_no', $detaildcid)
                    ->get();
                    Log::info('whdata1');
                } else {
                    //2023-05-04 轉運到著點
                    $wh = DB::table('mod_warehouse')
                    ->select(
                        DB::raw('cust_no as wh_no'),
                        DB::raw('cname as wh_name')
                    )
                    ->whereIn('dc_id', [$checkindata->dc_id])
                    ->get();
                    $dssdata->mark_no = "";
                    Log::info('whdata2');
                }
            }
            $whnoforMark = array();
            foreach ($wh as $key => $row) {
                $dcid =substr($row->wh_no,0,2);
                if($dcid == "A0" || $dcid == "B0") {
                    $whnoforMark [] = $row->wh_no;
                } else {
                    $whnoforMark [] = $dcid;
                }
                $whmark = DB::table('mod_mark')
                ->where('wh_no', $row->wh_no)
                ->where('is_enabled','Y')
                ->pluck('cust_no')
                ->toArray();

                $row->marklist = $whmark;
            }
            Log::info('getwarehouse no start');
            Log::info($whnoforMark);
            Log::info('getwarehouse no end');
            $marklist = DB::table('mod_mark')
            ->where('is_enabled','Y')
            ->pluck('cust_no')->toArray();

            $newmarklist = DB::table('mod_mark')
            ->select(
                DB::raw("cust_no as mark_no"),
                DB::raw("CONCAT(wh_name ,'-', cust_no)as mark_name"),
                'cust_name'
            )
            ->where('is_enabled','Y')
            ->whereIn('wh_no', $whnoforMark)
            ->get();
            $data['warehouse']   = $wh;
            $data['mark_name']   = isset($newmarklist[0]->mark_name) ? $newmarklist[0]->mark_name :null;
            $data['mark_no']     = $dssdata->mark_no;
            $data['marklist']    = $marklist;
            $data['newmarklist'] = $newmarklist;
            //


            return response()->json(['msg' => 'success','code'=>'00', 'data'=> $data]);
        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }
    public function geterrorreport(Request $request) {
        Log::info("geterrorreport");
        Log::info($request->all());
        $user       = Auth::user();
        try {
            $reportdatas = DB::table('mod_abnormal')
            ->select('error_name','belong_cd','belong_name')
            ->where('type', 'type')
            ->get();
            
            foreach ($reportdatas as $key => $row) {
                # code...
                $errordetail = DB::table('mod_abnormal')
                ->select('error_cd', 'error_name','showonpick', 'finish_mission')
                ->where('belong_cd', $row->belong_cd)
                ->where('type', 'reason')
                ->get();
                $row->option = $errordetail;
            }


        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }

        return response()->json(['msg' => 'success','code'=>'00', 'data'=> $reportdatas]);
    }
    
    public function messagelist (Request $request) {
        //app畫面
        Log::info("messagelist");
        Log::info($request->all());
        $user       = Auth::user();
        try {
            $time = empty($user->read_time) ? "2000-01-01 00:00:00" : $user->read_time;

            $message = DB::table('mod_message')
            ->select('id','content','title','type', 'from_by_name',
            DB::raw("'' as sys_ord_no"),
            DB::raw("'' as dlv_no"),
            DB::raw("'' as wms_ord_no"),
            DB::raw("created_at as send_at")
            )
            ->where('to_by', $user->email);
            
            $data = DB::table('mod_fcm')
            ->select('id','content','title','type',DB::raw("'' as from_by_name"),'sys_ord_no','dlv_no','wms_ord_no',
                DB::raw("created_at as send_at")
            )
            ->where('flag', '=', 'N')
            ->where('to_by', $user->email)
            ->union($message)
            ->orderBy('send_at', 'desc')
            ->get();
            foreach ($data as $key => $row) {
                $row->replayflag =  ($row->title == "請協助放行" || $row->title == "離場超時") ? "N":"Y";
            }
            return response()->json(['msg' => 'success','code'=>'00', 'data'=> $data]);

        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }

    public function replaymessage (Request $request) {
        Log::info("replaymessage");
        Log::info($request->all());
        $user       = Auth::user();
        try {

            $data = DB::table('mod_fcm')
            ->where('id', $request->id)
            ->first();
            if(isset( $data)) {
                $postdata     = array();
                $postdata['content']    = $request->content;
                $postdata['created_by'] = $user->name;
                $postdata['updated_at'] = date("Y-m-d H:i:s");
                $postdata['created_at'] = date("Y-m-d H:i:s");
    
                $realuser = $this->updateUnReadCountByname($data->created_by);
                $this->sendDsvTmsUnReadMessageByWebsocket($postdata, $realuser->id);
    
                $reportdata = DB::table('mod_error_report')
                ->where('id', $data->sys_ord_no)
                ->first();
    
                $messagedata      = array();
                $messagedata['title']          = $data->title == "推播通知" ? "異常訊息(回覆)" : '推播訊息(回覆)';
                $messagedata['owner_cd']       = $reportdata->owner_cd;
                $messagedata['owner_name']     = $reportdata->owner_name;
                $messagedata['cust_name']      = $reportdata->cust_name;
                $messagedata['sys_ord_no']     = $reportdata->ord_no;
                $messagedata['from_by']        = $user->id;
                $messagedata['from_by_name']   = $user->name;
                $messagedata['to_by']          = $realuser->email;
                $messagedata['to_by_name']     = $realuser->name;
                $messagedata['content']        = $data->content;
                $messagedata['replay_message'] = $request->content;
                $messagedata['created_by']     = $user->name;
                $messagedata['updated_by']     = $user->name;
                $messagedata['created_at']     = date("Y-m-d H:i:s");
                $messagedata['updated_at']     = date("Y-m-d H:i:s");
                $messagedata['replay_id']      = $request->id;
                $messagedata['phone']          = $user->email;
                $messagedata['to_by']          = $realuser->id;
                $messagedata['to_by_name']     = $realuser->name;
                $this->createMessage($messagedata);

    
    
                // $senduser = DB::table('users')
                // ->where('id', $data->from_by)
                // ->first();
    
                FcmHelper::sendToFcm( $data->title , $request->content, $realuser->d_token);
            }
 
            return response()->json(['msg' => 'success','code'=>'00', 'data'=> NULL]);

        } catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }

    public function fcmtest (Request $request) {
        Log::info("fcmtest");
        try{
            $checkusers = DB::table('users')
            ->where('email', $request->phone)
            ->first();

            $messagedata  = array();
            $messagedata['dlv_no']     = $request->dlv_no;
            $messagedata['wms_ord_no'] = $request->wms_ord_no;
            $messagedata['sys_ord_no'] = $request->sys_ord_no;
            $messagedata['to_by']      = $checkusers->email;
            $messagedata['type']       = 'ontime';
            $messagedata['flag']       = 'N';
            $messagedata['title']      = '準時到達';
            $messagedata['content']    = '單號:'.$request->sys_ord_no.'是否在預定時間內到達';
            $messagedata['created_by'] = $request->name;
            $messagedata['updated_by'] = $request->name;
            $messagedata['created_at'] = date("Y-m-d H:i:s");
            $messagedata['updated_at'] = date("Y-m-d H:i:s");
            // $this->createMessage($messagedata);
            $this->createFcm($messagedata);
            FcmHelper::sendToFcm( '準時到達' , '單號:'.$request->sys_ord_no.'是否在預定時間內到達', $checkusers->d_token);
            return response()->json(['msg' => 'success', 'data'=> null, 'code'=>'00']);
        }catch(\Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return response()->json(['msg' => $e->getMessage(), 'data'=> null, 'code'=>'01']);
        }
    }

    public function userByPhone (Request $request) {
        Log::info("userByPhone");
        Log::info($request->all()); 
        $user       = Auth::user();
        try {
            $data = DB::table('users')
            ->select('name', DB::raw("email as phone") )
            ->where('email', $request->phone)
            ->first();

            //逆物流次數
            $wmsforback = DB::table('mod_dlv_plan')
            ->where('driver_phone', $request->phone)
            ->where('trs_mode', 'pick')
            ->where('status', 'FINISHED')
            ->where('backdelivery', 'Y')
            ->pluck('wms_ord_no')->toArray();
            if(count( $wmsforback) > 0 ) {
                //逆物流次數
                $dlvbackcount = DB::table('mod_dlv_plan')
                ->whereIn('wms_ord_no', $wmsforback)
                ->where('trs_mode', 'dlv')
                ->where('status', '!=','FINISHED')
                ->count();
                if( $dlvbackcount > 0) {
                    return response()->json(['msg' => '此司機有回倉庫的貨物請確認', 'code'=> '00','data'=> $data]);
                }
            }

            return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>$data]);
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }
    public function remindWhFcm (Request $request) {
        Log::info("userByPhone");
        Log::info($request->all()); 
        $user       = Auth::user();
        try {

            return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>NULL]);
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }

    public function getdetailtest (Request $request) {
        Log::info("getdetailtest");
        Log::info($request->all());
        
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://logistics.com.tw/DSS01/api/WMS2DSS',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'[   
                {
                    "wms_ord_no": "T1111103000031"
                }
            ]',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic RFNWdG9EU1M6OHdsOCtKV00pMWhwOHdsOCtKV00pMWhwdzhhKCkrQHoqQ2pRdzhhKCkrQHoqQ2pR',
                'Content-Type: application/json',
                'Cookie: NSC_JOznd5c2epuap5newdw2o0dn4yhaqe3=ffffffff9ea0f91645525d5f4f58455e445a4a42378b'
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            Log::info($response);
            $response = json_decode($response);
            foreach ($response->data as $orderkey => $orderrow) {
                $detailinsertArray = array();
                foreach ($orderrow->detail as $orderdetailkey => $orderdetailrow) {
                    $a = array(
                        'ord_no'      => 'T1111103000031',
                        "wms_ord_no"  => isset($orderdetailrow->wms_ord_no) ? trim($orderdetailrow->wms_ord_no) : null,
                        "sn_no"       => isset($orderdetailrow->id) ? trim($orderdetailrow->id) : null,
                        "sku_no"      => isset($orderdetailrow->sku_no) ? trim($orderdetailrow->sku_no) : null,
                        "sku_desc"    => isset($orderdetailrow->sku_desc) ? trim($orderdetailrow->sku_desc) : null,
                        "transfer_mq" => isset($orderdetailrow->TransferMQ) ? trim($orderdetailrow->TransferMQ) : null,
                        "quantity"    => isset($orderdetailrow->quantity) ? trim($orderdetailrow->quantity) : null,
                        "unit"        => isset($orderdetailrow->unit) ? trim($orderdetailrow->unit) : null,
                        "batch_no"    => isset($orderdetailrow->batch_no) ? trim($orderdetailrow->batch_no) : null,
                        "expiry_date" => isset($orderdetailrow->expiry_date) ? trim($orderdetailrow->expiry_date) : null,
                        "g_key"       => 'STD',
                        "c_key"       => 'STD',
                        "s_key"       => 'STD',
                        "d_key"       => 'STD',
                        "created_by"  => 'SYSTEM',
                        "updated_by"  => 'SYSTEM',
                        'created_at'  => Carbon::now(),
                        'updated_at'  => Carbon::now(),
                    );
                    array_push($detailinsertArray, $a);
                }
                DB::table('mod_order_detail')->insert($detailinsertArray);
            }

        } catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
        return response()->json(['msg' => 'success', 'data'=> null, 'code'=>'00']);
    }

    public function checkCon($conId) {
        try {
            Log::info($conId);
            if (strlen($conId) != 11) {
                Log::info('櫃號檢查異常1');
                return false;
            }
            $conId = strtoupper($conId);
            //子母對應表
            $alphaArray = array(
                'A' => 10, 'B' => 12, 'C' => 13, 'D' => 14, 'E' => 15, 'F' => 16, 'G' => 17, 'H' => 18, 'I' => 19, 'J' => 20,
                'K' => 21, 'L' => 23, 'M' => 24, 'N' => 25, 'O' => 26, 'P' => 27, 'Q' => 28, 'R' => 29, 'S' => 30, 'T' => 31,
                'U' => 32, 'V' => 34, 'W' => 35, 'X' => 36, 'Y' => 37, 'Z' => 38
            ); 
            //檢查碼
            $checkCode = substr($conId, -1);
            //驗證碼
            $verification = substr($conId, 0, (strlen($conId)-1));
            $logarray     = array();
            $stringArray  = str_split($verification);
            $tempvalue    = 0;
            foreach ($stringArray as $key => $value) {
                $powValue = pow(2, $key) ;
                if ($key <= 3 && isset($alphaArray[$value])) {
                    $tempvalue += ($alphaArray[$value] * $powValue);
                    array_push($logarray, ($alphaArray[$value] * $powValue));
                } elseif ($key > 3) {
                    $tempvalue += ($value * $powValue);
                    array_push($logarray, ($value * $powValue));
                }
            }
    
            $tempvalue    = ($tempvalue / 11);
            $intvalue     = (int)$tempvalue;
            $decimalvalue = $tempvalue - $intvalue;
            $decimalvalue = round($decimalvalue * 11, 0);
            if ($decimalvalue >= 10) {
                $decimalvalue = $decimalvalue - 10;
            }
            if ($decimalvalue != $checkCode) {
                Log::info('櫃號檢查異常2');
                return false;
            }
            return true;
        } catch (\Throwable $e) {
            Log::info('櫃號檢查異常3');
            Log::info($e->getMessage());
            Log::info($e->getLine());
            return false;
        }
        return true;
    }

    public function dssontime(Request $request) {
        Log::info("dssontime");
        Log::info($request->all());
        $user = Auth::user();
        try {
            $fcmdata = DB::table('mod_fcm')
            ->where('id', $request->id)
            ->first();
            DB::table('mod_fcm')
            ->where('id', $request->id)
            ->update([
                'flag' =>'Y'
            ]);
            if($request->on_time_flag == true) {
                Log::info('on_time_flag Y');
                if(isset($fcmdata)) {
                    DB::table('mod_order')
                    ->where('ord_no', $fcmdata->wms_ord_no)
                    ->update([
                        'on_time_flag' => $request->ontime,
                        'on_time_date' => $request->confirmtime,
                    ]);
                }
            } else {
                DB::table('mod_order')
                ->where('ord_no', $fcmdata->wms_ord_no)
                ->update([
                    'on_time_flag' => $request->ontime,
                    'on_time_date' => $request->confirmtime,
                    'error_type'   => '配送問題',
                    'exp_reason'   => '11',
                    'error_remark' => '提醒：可能遲到',
                ]);
                Log::info('on_time_flag N');
                $detaildata =  DB::table('mod_dlv_plan')
                ->where('wms_ord_no', $fcmdata->wms_ord_no)
                ->where('dlv_no', $fcmdata->dlv_no)
                ->where('trs_mode', 'DLV')
                ->first();
                Log::info( $detaildata->id);
                $orderdata =  DB::table('mod_order')
                ->where('ord_no', $fcmdata->wms_ord_no)
                ->first();
                Log::info( $orderdata->id);
                if(isset($detaildata)) {
                    $errorId = DB::table('mod_error_report')
                    ->insertGetId([
                        'phone'         => $user->email,
                        'cust_name'     => $detaildata->cust_name,
                        'sys_dlv_no'    => $detaildata->sys_dlv_no,
                        'ord_no'        => $detaildata->ord_no,
                        'wms_ord_no'    => $detaildata->wms_ord_no,
                        'dlv_ord_no'    => $detaildata->dlv_ord_no,
                        'error_type'    => '配送問題',
                        'error_cd'      => '11',
                        'error_name'    => '提醒：可能遲到',
                        'trs_mode'      => 'DLV',
                        'owner_cd'      => $detaildata->owner_cd,
                        'owner_name'    => $detaildata->owner_name,
                        'dc_id'         => $detaildata->dc_id,
                        'g_key'         => 'DSS',
                        'c_key'         => 'DSS',
                        's_key'         => 'DSS',
                        'd_key'         => 'DSS',
                        'created_by'    => $user->name,
                        'updated_by'    => $user->name,
                        'created_at'    => Carbon::now()->toDateTimeString(),
                        'updated_at'    => Carbon::now()->toDateTimeString()
                    ]);
                    Log::info("errorId");
                    Log::info($errorId);
                    try {
                        $administrative = DB::table('users')
                        ->where('role', 'like', '%CS%')
                        ->Where(function($query)  use ($orderdata) {
                            if($orderdata->from_wh == '台北倉') {
                                $orderdata->from_wh = '台北';
                            }
                            if($orderdata->from_wh == '觀音一倉1F' || $orderdata->from_wh == '觀音一倉3F'  || $orderdata->from_wh == '觀音一倉4F' ) {
                                $orderdata->from_wh = '觀音一倉';
                            }
                            $query->orwhere('check_wh', 'like', '%'.$orderdata->dc_id.'%');
                            $query->orwhere('check_wh_name', 'like', '%'.$orderdata->from_wh.'%');
                        })
                        ->get();
        
                        $dTokenList = array();
                        $sendUserIdList = array();
                        $messagedataList = array();
                        foreach ($administrative as $key => $senduser) {
                            # code...
                            Log::info("userid");
                            Log::info($senduser->id);
                            $messagedata    = array();
                            $messagedata['owner_cd']     = $detaildata->owner_cd;
                            $messagedata['owner_name']   = $detaildata->owner_name;
                            $messagedata['cust_name']    = $detaildata->cust_name;
                            $messagedata['error_id']     = $errorId;
                            $messagedata['sys_ord_no']   = $detaildata->ord_no;
                            $messagedata['title']        = '異常回報';
                            $messagedata['from_by']      = $user->id;
                            $messagedata['from_by_name'] = $user->name;
                            $messagedata['to_by']        = $senduser->id;
                            $messagedata['to_by_name']   = $senduser->name;
                            $messagedata['content']      = '提醒：可能遲到';
                            $messagedata['created_by']   = $user->name;
                            $messagedata['updated_by']   = $user->name;
                            $messagedata['phone']        = $user->email;
                            $messagedata['created_at']   = date("Y-m-d H:i:s");
                            $messagedata['updated_at']   = date("Y-m-d H:i:s");
                            
                            if($senduser->check_owner != "*" ) {
                                //
                                $userOwnerGroup = explode(',',$senduser->check_owner);
        
                                $checkOwner = DB::table('mod_customer_relation')
                                ->whereIn('group_cd', $userOwnerGroup)
                                ->where('cust_no',  $detaildata->owner_cd)
                                ->count();
        
                                $checkWarehouse = explode(',',$senduser->check_wh);
                                Log::info("checkOwner");
                                Log::info($checkOwner);
                                Log::info("checkWarehouse");
                                Log::info($checkWarehouse);
                                if($checkOwner >= 1 )  {
                                    Log::info("double userid");
                                    Log::info($senduser->id);
                                    array_push($messagedataList, $messagedata);
                                    array_push($dTokenList, $senduser->d_token);
                                    array_push($sendUserIdList, $senduser->id);
                                    $this->sendDsvTmsUnReadMessageByWebsocket($messagedata, $senduser->id);
                                }
                            }  else {
                                array_push($messagedataList, $messagedata);
                                array_push($dTokenList, $senduser->d_token);
                                array_push($sendUserIdList, $senduser->id);
                                $this->sendDsvTmsUnReadMessageByWebsocket($messagedata, $senduser->id);
                            }
                        }
                        // 2023-06-13 By Tony 推播 & mod_message insert & users unReadCount update 改成批量
                        FcmHelper::batchSendToFcm( '異常回報' , $request->error_name, $dTokenList);
                        $messagedataArray = collect($messagedataList);
                        $chunks = $messagedataArray->chunk(500);
                        foreach($chunks as $chunk) {
                            Message::insert($chunk->toArray());
                        }
                        $this->batchUpdateUnReadCount($sendUserIdList);
                    } catch(\Exception $e) {
                        Log::info($e->getMessage());
                        Log::info($e->getLine());
                    }

                }
                
                
            }

        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
        
        return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>NULL]);
    }

    public function meessagelist(Request $request) {
        Log::info("meessagelist");
        Log::info("dia");
        $user       = Auth::user();
        try {
            $time = empty($user->read_time) ? "2000-01-01 00:00:00" : $user->read_time;

            //
            $checkwh = array();
            if(!empty($user->truck_cmp_no) ) {
                $checkwhdata = DB::table('mod_order')
                ->where('driver_phone', $user->email )
                ->groupBy('dc_id')
                ->pluck('dc_id')
                ->toArray();

                foreach ($checkwhdata as $testkey => $testrow) {
                    $dcid =substr($testrow,0,2);
                    if($dcid != 'A0' && $dcid !='B0') {
                         $checkwh[] = $dcid.'-1';
                    } else {
                      $checkwh[] = $testrow;
                    }
                 }

            } else {
                $checkwh =  explode(',', strtoupper($user->check_wh));
            }

            $today = date("Y-m-d");
            $unReadann = DB::table('mod_announce')
            ->where('announce_start_date', '<=', $today)
            ->where('announce_end_date', '>=', $today)
            ->where('updated_at', '>=', $time)
            ->where('is_enabled', 'Y')
            ->orderBy('announce_date','desc')
            ->orderBy('id','desc')
            ->get();
            $returndata = array();
            $userrole = explode(',', strtoupper($user->role));
            Log::info($user->id);
            if(!in_array('ADMIN', $userrole) ) {
                Log::info('not admin');
                Log::info('unread count');
                Log::info(count($unReadann));
                foreach ($unReadann as $key => $row) {
                    $announcerole = explode(',', $row->announce_type);
                    $resultdata = array_intersect($userrole, $announcerole);
                    $warehouse = explode(',', $row->wh_no);
                    $resultwh = array_intersect($warehouse, $checkwh); 
                    Log::info('count($resultdata)');
                    Log::info(count($resultdata));
                    Log::info('count($resultwh)');
                    Log::info(count($resultwh));
                    if( (count($resultdata) > 0 && (count($resultwh) > 0)) || $row->wh_no == '*' ) {
                        $returndata[] = $row;
                    }
                }
            } else {
                Log::info('admin');
                foreach ($unReadann as $key => $row) {
                    $warehouse = explode(',', $row->wh_no);
                    $resultwh = array_intersect($warehouse, $checkwh); 
                    if(count($resultwh) > 0) {
                        $returndata[] = $row;
                    }
                    
                }
            }

            $unmessage = DB::table('mod_message')
            ->select('id','content','title','flag','type',
                'sys_ord_no',
                DB::raw("'' as dlv_no"),
                DB::raw("'' as wms_ord_no"),
                DB::raw("created_at as send_at"),
                DB::raw("'Y' as replayflag")
            )
            ->where('to_by', $user->id)
            ->where('created_at', '>=', $time)
            ->where('title', '!=', '請協助放行')
            ->orderBy('id', 'desc')
            ->get()->toArray();

            foreach ($unmessage as $key => &$row) {
                Log::info('fcm'.$row->id);
                $row->replayflag =  $row->title=="請協助放行" ? "N":"Y";
            }
            $normalmessage = DB::table('mod_fcm')
            ->select('id','content','title','flag','type',
                'sys_ord_no','dlv_no','wms_ord_no',
                DB::raw("created_at as send_at"),
                DB::raw("'Y' as replayflag")
            )
            ->where('flag', 'N')
            ->where('to_by', $user->email)
            ->where('created_at', '>=', $time)
            ->where('title', '!=', '請協助放行')
            ->whereNull('type')
            ->orderBy('id', 'desc')
            ->get()->toArray();
            $ontimemessage = DB::table('mod_fcm')
            ->select('id','content','title','flag','type',
                'sys_ord_no','dlv_no','wms_ord_no',
                DB::raw("created_at as send_at"),
                DB::raw("'Y' as replayflag")
            )
            ->where('flag', 'N')
            ->where('to_by', $user->email)
            ->where('type','ontime')
            ->orderBy('id', 'desc')
            ->get()->toArray();

            $retunmessage = array_merge($unmessage,$ontimemessage,$normalmessage);

            $data = array();

            $data['announce']  = $returndata ;
            $data['fcm']       = $retunmessage ;
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }

        return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>$data]);
    }

    public function updateUserRead(Request $request) {
        Log::info("updateUserRead");
        $user       = Auth::user();
        $now = date("Y-m-d H:i:s");
        try {

            DB::table('users')
            ->where('id', $user->id)
            ->update([
                'read_time' => $request->readtime
            ]);

            return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>NULL]);
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }
    public function getdetailbydlvno(Request $request) {
        Log::info("getdetailbydlvno");
        Log::info($request->all());
        $user       = Auth::user();

        try {
            $data = DB::table('mod_dlv_plan')
            ->where('dlv_no', $request->dlv_no)
            ->where('trs_mode','DLV')
            ->where('handover_flag', 'N')
            ->get();

            return response()->json(['msg' => 'success', 'code'=> '00', 'data'=>$data ]);
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }
    }

    public function handover(Request $request) {
        Log::info("handover");
        Log::info($request->all());
        $user       = Auth::user();
        try {
            $wmsordnos = array();

            $apiuser = DB::table('bscode')
            ->where('token','ODQ0OTI0OTIyMDIyMTExNDEzMzA0MQ==')
            ->where('cd_type', 'OPENAPICUST')
            ->first();

            $detaildlvid = DB::table('mod_dlv_plan')
            ->whereIn('id', $request->ids)
            ->pluck('wms_ord_no')->toArray();

            $detailbydlvno = DB::table('mod_dlv_plan')
            ->whereIn('id', $request->ids)
            ->first();

            $detaildata = DB::table('mod_dlv_plan')
            ->whereIn('wms_ord_no', $detaildlvid)
            ->where('dlv_no', $detailbydlvno->dlv_no)
            ->get();

            foreach ($detaildata as $key => $row) {
                $wmsordnos[] = $row->wms_ord_no;
            }

            DB::table('mod_dlv_plan')
            ->where('dlv_no', $detailbydlvno->dlv_no)
            ->whereIn('wms_ord_no', $wmsordnos)
            ->where('trs_mode', 'DLV')
            ->update([
                'handover_flag'      => 'Y',
                'error_name'         => '換車運送',
                'button_status'      => 'FINISHED',
                'button_status_desc' => '修改回報資料',
                'status'             => 'FINISHED',
                'status_desc'        => '任務完成',
                'finish_time'        => Carbon::now()->toDateTimeString(),
            ]);
            DB::table('mod_dlv_plan')
            ->where('dlv_no', $detailbydlvno->dlv_no)
            ->whereIn('wms_ord_no', $wmsordnos)
            ->where('trs_mode', 'PICK')
            ->update([
                'handover_flag'      => 'Y',
                'button_status'      => 'FINISHED',
                'button_status_desc' => '修改回報資料',
                'status'             => 'FINISHED',
                'status_desc'        => '任務完成',
                'finish_time'        => Carbon::now()->toDateTimeString(),
            ]);

            Log::info("detaildata");
            Log::info($detaildata[0]->dlv_no);
            $maindata = DB::table('mod_dss_dlv')
            ->where('dlv_no', $detaildata[0]->dlv_no)
            ->first();
            Log::info($maindata->id);
            //取得系統訂單號
            $today       = new \DateTime();
            $str_date    = $today->format('Ym');
            $params = array(
                "c_key"   => $apiuser->value2,
                "date_ym" => substr($str_date, 2, strlen($str_date))
            );
            $BaseModel = new BaseModel();
            try {
                $autoDlvNo = $BaseModel->getAutoNumberApi("DLV",$params,$apiuser->value2);
            } catch (\Throwable $th) {
                $autoDlvNo = $BaseModel->getAutoNumberApi("DLV",$params,$apiuser->value2);
            }
            $sysdlvno =  $autoDlvNo;

            //取得系統訂單號
            $today       = new \DateTime();
            $str_date    = $today->format('Ymd');
            $params = array(
                "c_key"   => $apiuser->value2,
                "date_ym" => substr($str_date, 2, strlen($str_date))
            );
            try {
                $newdlvno = $BaseModel->getAutoNumberApi("NEWDLV",$params,$apiuser->value2);
            } catch (\Throwable $th) {
                $newdlvno = $BaseModel->getAutoNumberApi("NEWDLV",$params,$apiuser->value2);
            }
            $newdlvno =  $newdlvno;
            $cartrader = DB::table('mod_car_trader')
            ->select('cust_no','cust_name')
            ->where('cust_no', $user->truck_cmp_no)
            ->first();

            DB::table('mod_dss_dlv')->insert([
                'send_count'     => $maindata->send_count,
                'sys_dlv_no'     => $sysdlvno,
                'dc_id'          => $maindata->dc_id,
                'og_pallet_num'  => $maindata->og_pallet_num,
                'dlv_no'         => $newdlvno,
                'dlv_date'       => $maindata->dlv_date,
                'dlv_type'       => $maindata->dlv_type,
                'dlv_type_desc'  => $maindata->dlv_type_desc,
                'car_type'       => $maindata->car_type,
                'car_type_name'  => $maindata->car_type_name,
                'truck_cmp_no'   => $cartrader->cust_no,
                'truck_cmp_name' => $cartrader->cust_name,
                'status'         => 'DLV',
                'car_no'         => $maindata->car_no,
                'driver_nm'      => $user->name,
                'driver_phone'   => $user->email,
                'total_cbm'      => $maindata->total_cbm,
                'total_gw'       => $maindata->total_gw,
                'total_box_num'  => $maindata->total_box_num,
                'mark_no'        => $maindata->mark_no,
                'dlv_point'      => $maindata->dlv_point,
                'g_key'          => 'DSS',
                'c_key'          => 'DSS',
                's_key'          => 'DSS',
                'd_key'          => 'DSS',
                'updated_by'     => 'SYSTEM',
                'created_by'     => 'SYSTEM',
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ]);

            foreach ($detaildata as $key => $row) {
                $button_status      = $row->button_status;
                $button_status_desc = $row->button_status_desc;
                $status             = $row->status;
                $status_desc        = $row->status_desc;
                
                if($row->trs_mode == "pick") {
                    $button_status      = 'FINISHED';
                    $button_status_desc = '修改回報資料';
                    $status             = 'FINISHED';
                    $status_desc        = '任務完成';
                } else {
                    $button_status      = 'ARRIVE';
                    $button_status_desc = '到點';
                    $status             = 'DLV';
                    $status_desc        = '任務已分派';
                }
                if($row->trs_mode == "pick" && $row->status == 'FINISHED') {
                    continue;
                }

                DB::table('mod_dlv_plan')->insert([
                    'main_trs_mode'      => $row->main_trs_mode,
                    'button_status'      => $button_status,
                    'button_status_desc' => $button_status_desc,
                    'status'             => $status ,
                    'status_desc'        => $status_desc,
                    'sys_dlv_no'         => $sysdlvno,
                    'dc_id'              => $row->dc_id,
                    'from_wh'            => $row->from_wh,
                    'dlv_no_seq'         => $row->dlv_no_seq,
                    'cust_contact'       => $row->cust_contact,
                    'cust_phone'         => $row->cust_phone,
                    'itemp'              => $row->itemp,
                    'floor'              => $row->floor,
                    'collect_amount'     => $row->collect_amount,
                    'dlv_no'             => $newdlvno,
                    'trs_mode'           => $row->trs_mode,
                    'trs_mode_desc'      => $row->trs_mode_desc,
                    'dlv_ord_no'         => $row->dlv_ord_no,
                    'owner_cd'           => $row->owner_cd,
                    'owner_name'         => $row->owner_name,
                    'ord_no'             => $row->ord_no,
                    'cust_name'          => $row->cust_name,
                    'cust_address'       => $row->cust_address,
                    'cust_zip'           => $row->cust_zip,
                    'driver_nm'          => '',
                    'driver_phone'       => $user->email,
                    'cbm'                => $row->cbm,
                    'gw'                 => $row->gw,
                    'box_num'            => $row->box_num,
                    'wms_ord_no'         => $row->wms_ord_no,
                    'order_remark'       => $row->order_remark,
                    'dsv_remark'         => $row->dsv_remark,
                    'upload_start_time'  => $row->upload_start_time,
                    'upload_finish_time' => $row->upload_finish_time,
                    'high_risk'          => $row->high_risk,
                    'route_name'         => $row->route_name,
                    'route_type'         => $row->route_type,
                    'dlv_to_wh'          => $row->dlv_to_wh,
                    'wh_no'              => $row->wh_no,
                    'truck_cmp_name'     => $cartrader->cust_name,
                    'fromwh'             => $row->fromwh,
                    'car_no'             => $row->car_no,
                    'dlv_date'           => $row->dlv_date,
                    'backdelivery'       => $row->backdelivery,
                    'dlv_to_wh'          => $row->dlv_to_wh,
                    'from_handover'      => 'Y',
                    'g_key'              => 'dss',
                    'c_key'              => 'dss',
                    's_key'              => 'dss',
                    'd_key'              => 'dss',
                    'updated_by'         => 'SYSTEM',
                    'created_by'         => 'SYSTEM',
                    'created_at'         => Carbon::now(),
                    'updated_at'         => Carbon::now(),
                 ]);
            }
            DB::table('mod_order')
            ->whereIn('ord_no', $wmsordnos)
            ->update([
                'dlv_no'       => $newdlvno,
                'truck_cmp_nm' => $cartrader->cust_name,
                'truck_cmp_no' => $cartrader->cust_no,
                'driver_phone' => $user->email,
                'driver'       => '',
            ]);

            $checkisover =  DB::table('mod_dlv_plan')
            ->where('sys_dlv_no', $maindata->sys_dlv_no)
            ->where('trs_mode', 'dlv')
            ->where('status', '!=', 'FINISHED')
            ->count();
            if( $checkisover == 0 ) {
                DB::table('mod_dss_dlv')
                ->where('sys_dlv_no', $request->sys_dlv_no)
                ->update([
                    "status"      => "FINISHED",
                    "status_desc" => "任務完成",
                    "updated_by"  => 'SYSTEM',
                    "updated_at"  => Carbon::now()->toDateTimeString(),
                ]);
            }



            return response()->json(['msg' => 'success', 'code'=> '00', 'data'=> null ]);
        }catch (\Throwable $e) {
            Log::info($e->getLine());
            Log::info($e->getMessage());
            return response()->json(['msg' => 'error', 'data'=> null, 'code'=>'01']);
        }


    }
}