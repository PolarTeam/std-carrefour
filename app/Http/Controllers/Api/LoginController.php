<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\User;                
use Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\UserService;
use DB;
class LoginController extends ApiController
{
    public function __construct(UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;
    }
    // 登录用户名标示为phone字段
    
    //登录接口，调用了ApiController中一些其他函数succeed\failed，上文未提及，用于接口格式化输出
    // public function login(Request $request)
    // {
    //     \Log::info('login data');
    //     \Log::info($request->all());
    //     $validator = Validator::make($request->all(), [
    //         'email'    => 'required|exists:users',
    //         'password' => 'required|between:6,32',
    //         ]);

    //         if ($validator->fails()) {
    //         $request->request->add([
    //             'errors' => $validator->errors()->toArray(),
    //             'code' => 401,
    //         ]);
    //         return ["cdoe"=>"01","msg"=>"check account email or Password"];
    //         // return $this->sendFailedLoginResponse($request);
    //     }
    
    //     $credentials = $this->credentials($request);

    //     if ($this->guard('api')->attempt($credentials, $request->has('remember'))) {
    //         $account_enable = DB::table('users')->where('email', $request->email)->value('account_enable');
    //         // $userdata = DB::table('users')->where('email', $request->email)->first();
    //         if($account_enable=="Y"){
    //             DB::table('users')->where('email', $request->email)->update(['online' => '1', 'd_token' => $request->d_token, 'phonetype'=>  $request->phone_type]);
    //             $id = DB::table('users')->where('email', $request->email)->value('id');
    //             DB::table('oauth_access_tokens')->where('user_id', $id)->delete();
    //             return $this->sendLoginResponse($request);
    //         }
    //         return ["cdoe"=>"error","msg"=>"account Error"];
    //     }

    //     return ["cdoe"=>"01","msg"=>"Password Error"];
    // }

    public function login(Request $request) {
        $response = $this->userRepository->login($request);
        return response()->json($response["data"]);
    }
}