<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Controllers\Controller;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\LogActivity;

class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService) {
        $this->permissionService = $permissionService;
    }

    public function index() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $user = Auth::user();
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='permissions' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));

            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('permissions.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        => trans('permissions.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }

        $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='permissions' AND user_id={$user->id} LIMIT 1";
        $gridLayout = DB::select($sql);

        $columns = array();
        if(!$gridLayout) {
            //預設layout
            $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='permissions' AND user_id = '29' LIMIT 1";
            $gridLayout = DB::select($sql);

            //連預設都沒有的時候用產生的
            if(!$gridLayout) {
                $columns = json_encode($cmptycolumns);
            } else {
                $columns = $gridLayout[0]->layout_content;
            }

        }

        $lang = app()->getLocale();
        return view('permission.index', array(
            'title'      => trans('permissions.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));;

    }

    public function edit($lang,$id) {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('permissions.titleName');
        $this->data['entry'] = array();
        $this->data['id']    = $id;
        $this->data['lang']  = $lang;

        return view('permission.edit', $this->data);
    }

    public function create() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('permissions.titleName');
        $this->data['entry'] = array();
        $this->data['id'] = null;
        $this->data['lang']     = app()->getLocale();
        return view('permission.edit', $this->data);
    }

    public function show($lang,$id) {
        try {
            $permissiondata = $this->permissionService->get($id);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => "get permission success",
            'data'    => $permissiondata,
            'filed'   => $this->permissionService->getfiled('permissions')
        ]);
    }



    public function store (Request $request) {

        $result = array();
        try {
            $result = $this->permissionService->create($request);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('新增權限');
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'lastId' => $result['id']
        ]);
    }

    public function update (Request $request, $lang, $id) {
        $result = array();
        try {
            $result = $this->permissionService->update($request, $id);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        
        LogActivity::addToLog('修改權限');
        return response()->json([
            'success' => true,
            'message' => '修改成功',
            'data'    => $this->permissionService->get($id),
            'filed'   => $this->permissionService->getfiled('permissions')
        ]);
    }

    public function destroy (Request $request, $lang, $id) {
        try {
            $this->permissionService->delete($id);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除權限');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function batchDelete (Request $request) {
        DB::beginTransaction();

        try {
            $this->permissionService->batchDelete($request);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        DB::commit();

        LogActivity::addToLog('刪除權限');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function query(Request $request) {
        $result = array();

        try {
            $result = $this->permissionService->query($request);
        }
        catch(Exception $e) {
            return response()->json(array(
                'success'    => false,
                'data'       => array(),
                'totalCount' => 0,
                'message'    => $e->getMessage()
            ));
        }

        return response()->json(array(
            'success'    => true,
            'data'       => $result['data'],
            'totalCount' => $result['totalCount'],
            'message'    => 'success'
        ));
    }

    public function exportdata(Request $request) {

        $downloadLink = '';

        try {
            $downloadLink = $this->permissionService->export($request);
        }
        catch(\Exception $e) {
            Log::error($e);
            return response()->json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }

        if(empty($downloadLink)) {
            return response()->json(array(
                'success' => false,
                'message' => '下載錯誤'
            ));
        }

        return response()->json(array(
            'success' => true,
            'donwloadLink' => $downloadLink,
            'message' => 'success'
        ));
    }
}
