<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class AppCrashLogController extends Controller
{
    protected $appCrashLogService;

    public function store (Request $request) {
        try {
            $user = Auth::user();
            if ($user) {
                $appCrashLog=  DB::table('app_crash_log')->insert([
                    'crash_log'       => $request->crash_log,
                    'created_by'      => $user->id,
                    'created_by_name' => $user->name,
                    'updated_by'      => $user->id,
                    'updated_by_name' => $user->name,
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now(),
                ]);
            } else {
                $appCrashLog = DB::table('app_crash_log')->insert([
                    'crash_log'       => $request->crash_log,
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now(),
                ]);
            }

            if (!$appCrashLog) {
                throw new Exception("create AppCrashLog failed");
            }

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => "create appCrashLog success",
            'data' => $appCrashLog
        ]);
    }
}
