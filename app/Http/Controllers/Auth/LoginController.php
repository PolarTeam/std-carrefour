<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/user';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginView() {
        return view('login', array(
            'loginStatus' => ''
        ));
    }

    public function logout(Request $request) {
        Auth::logout();
    
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        $input = $request->all();
        
        if (array_key_exists($request->input('lang'), config('app.locales'))) {
            session(['applocale' => $request->input('lang')]);
        }

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        // $checkuser = DB::table('users')
        // ->where('email', $request->email)
        // ->first();

        // if (Hash::check($input['password'], $checkuser->password)) { 
        //     $now = strtotime(date("Y-m-d 00:00:00"));
        //     $expireDate = strtotime($checkuser->expire_date);
        //     if($now >= $expireDate) {
                
        //         $this->data['title']       = trans('users.titleName');
        //         $this->data['entry']       = array();
        //         $this->data['id']          = $checkuser->id;
        //         $this->data['lang']        = 'zh-TW';
        //         $this->data['role']        = NULL;
        //         $this->data['check_wh']    = NULL;
        //         $this->data['check_owner'] = NULL;
        
        //         return view('user.passwordedit', $this->data);
        //     }
            
        // }

        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if (auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'] , 'account_enable' => 'Y'))) {
            $user = Auth::user();
            Auth::logoutOtherDevices($input['password']);
            $unreadcount = DB::table('mod_message')
            ->where('to_by',  $user->id)
            ->whereNull('is_read')
            ->count();

            DB::table('users')
            ->where('id',  $user->id)
            ->update([
                'unReadCount'   => $unreadcount,
            ]);
            $unReadData    = array();
            $newsData  = array();

            $newsData  ['created_by']  = "系統通知";
            $newsData  ['content']     = "已從其他登入";
            $newsData  ['created_at']  = date("Y-m-d H:i:s");
            $unReadData['newData']     = $newsData;
            $unReadData['unReadCount'] = $unreadcount;

            $postData = array(
                "groupId" => "groupId_DsvdevTmsUnReadMessageByWebsocket{$user->id}",
                "msg" => $unReadData
            );
            try {
                // Log::info("enter DsvTmsUnReadMessageByWebsocket");
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://dss-ws.target-ai.com:9599/websocket/notifyByGroupId',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>json_encode($postData, JSON_UNESCAPED_UNICODE),
                    CURLOPT_HTTPHEADER => array(
                        'cache-control: no-cache',
                        'content-type: application/json'
                    ),
                ));
    
                $response = curl_exec($curl);
    
                curl_close($curl);
                // Log::info($response);
            }
            catch(\Exception $e) {
                Log::info("GpsDataByWebsocket error");
                Log::info($e->getMessage());
                Log::info($e->getLine());
            }

            session(['locale' => $user->lang]);
            app()->setLocale($user->lang);

            $ueserrole = explode(',', $user->role);
            $redirectto = DB::table('bscode')->where('cd_type', 'LOGINPAGE')->whereIn('cd', $ueserrole)->value('value1');
            if(isset($redirectto)){
                return redirect('/'.$redirectto);
            }else{
                return redirect()->route('login')
                ->with('error', 'Email-Address And Password Are Wrong.');
            }
        } else {
            return redirect()->route('login')
                ->with('error', 'Email-Address And Password Are Wrong.');
        }
    }
}
