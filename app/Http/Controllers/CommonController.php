<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\CommonModel;
use App\Models\CustomerItemModel;
use App\Models\CustomerProfileModel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use HTML;
use Excel;
use Illuminate\Support\Facades\Crypt;
class CommonController extends Controller
{
    
    public function getData($table=null,$id=null) {
        $data = DB::table($table)->where('id', $id)->first();

        return response()->json($data);
    }

    public function getCompnayList() {
        $gData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'G')->get();
        $cData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'C')->get();
        $sData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'S')->get();
        $dData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'D')->get();

        $data = array(
            'gData' => $gData,
            'cData' => $cData,
            'sData' => $sData,
            'dData' => $dData,
        );
        
        return response()->json($data);
    }

    public function getSearchLayoutList($key) {
        $user = Auth::user();

        $data = DB::table('sys_search_layout')
                    ->select('id', 'title', 'layout_default')
                    ->where('key', $key)
                    ->where('created_by', $user->email)
                    ->get();
        
        return response()->json(['msg' => 'success', 'data' => $data]);
    }

    public function getSearchHtml($id=null) {

        return response()->json(['msg' => 'success', 'data' => DB::table('sys_search_layout')->where('id', $id)->first()]);

    }
    


    public function delSearchLayout($id=null) {
        DB::table('sys_search_layout')
            ->where('id', $id)
            ->delete();
            
        return response()->json(['msg' => 'success']);    
    }
    public function saveSearchLayout(Request $request) {
        $user = Auth::user();

        $data  = $request->data;
        $title = $request->title;
        $id    = $request->id;
        $key   = $request->key;

        if($id == null) {
            $id = DB::table('sys_search_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'title'      => $title,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);
        }
        else {
            DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'data'       => $data,
                    'updated_by' => $user->email,
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
        }

        return response()->json(['msg' => 'success', 'id' => $id]);
    }
    public function setSearchDefault($key, $id) {
        $user = Auth::user();

        DB::table('sys_search_layout')
                ->where('key', $key)
                ->where('created_by', $user->email)
                ->update([
                    'layout_default' => 'N'
                ]);
        
        DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'layout_default' => 'Y'
                ]);

        return response()->json(['msg' => 'success']);
    }

    public function getCarType() {
        $user = Auth::user();
        $carType = request('carType');
        $result = array();
        if(isset($carType)) {
            $a = explode(',', $carType);

            $result = DB::table('bscode')
                ->select('cd', 'cd_descp')
                ->where('cd_type', 'CARTYPE')
                ->whereIn('cd', $a)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->get();
                
            if(count($result) == 0) {
                $result = DB::table('bscode')
                ->select('cd', 'cd_descp')
                ->where('cd_type', 'CARTYPE')
                ->whereIn('cd_descp', $a)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->get();
            }
        }

        return response()->json($result);
    }

    public function updatePwd() {
        $password = request('password');
        $confirmPassword = request('confirmPassword');
        $originalPassword = request('originalPassword');

        if($password != $confirmPassword) {
            return response()->json(['msg' => 'error', 'msgLog' => '密碼不一致，請再檢查']);
        }

        $user = Auth::user();

        if (!(Hash::check($originalPassword, $user->password))) {
            return response()->json(['msg' => 'error', 'msgLog' => '原始密碼錯誤']);
        }
        $user->password = bcrypt($password);
        $user->save();

        return response()->json(['msg' => 'success']);
    }
    public function changelang() {
        // $changelang = request('chagneLang');
        // session(['locale' => $changelang]);
        // app()->setLocale($changelang);

        // $user = Auth::user();
        // $user->lnag = $changelang;
        // $user->save();
        
        return response()->json(['msg' => 'success']);
    }

    public function showChkImg($path) {

        return view('files.showImg')->with(['imgData' => $path]);
    }


    function num2alpha($n)  //數字轉英文(0=>A、1=>B、26=>AA...以此類推)
    {
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n%26 + 0x41) . $r; 
        return $r; 
    } 




    public function Layout() {
        $title = "layout";
        echo "<title>$title</title>";
        $user = Auth::user();
        //$carData = DB::table('sys_customers')->where('type', 'SELF')->where('cust_no', $user->c_key)->where('identity', 'c')->first();
        try{
            if(Auth::user()->hasPermissionTo('LAYOUTSETTING'))
            {
                $lang = app()->getLocale();
                return view('layout.index', array(
                    'lang'       => $lang
                ));;
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    }
    public function changerror($msg)
    {
        $newmsg ="";
        if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;SELF&"){
            $newmsg="集團建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv_plan_view"){
            $newmsg="派車作業總覽";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_close"){
            $newmsg="結案管理";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_confirm"){
            $newmsg="回單作業";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order"){
            $newmsg="訂單作業";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_car"){
            $newmsg="車輛建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_detail_view"){
            $newmsg="訂單明細總覽";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view"){
            $newmsg="訂單總覽";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_total_view as mod_order_total_view"){
            $newmsg="訂單總覽";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_users_view"){
            $newmsg="客戶使用者權限";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_dlv"){
            $newmsg="派車作業";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJsonForMongo/mod_order"){
            $newmsg="訂單作業";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/sys_area"){
            $newmsg="區域建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/sys_country"){
            $newmsg="國家建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/bscode_kind"){
            $newmsg="基本建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_goods"){
            $newmsg="料號建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/sys_customers?basecon=type;EQUAL;OTHER&"){
            $newmsg="客戶建檔";
        }else if($msg=="layoutGrid"){
            $newmsg="Layout管理";
        }else if($msg=="dlvCarGrid"){
            $newmsg="訂單匯入";
        }else if($msg=="excelOrderGrid"){
            $newmsg="派車作業";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_order_view"){
            $newmsg="派車作業(舊)";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_mi_view"){
            $newmsg="小米";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_shippen_temp_view"){
            $newmsg="BenQ";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/sys_customer_view?basecon=type;EQUAL;OTHER&"){
            $newmsg="客戶建檔";
        }else if($msg=="https://syltms.standard-info.com/api/admin/baseApi/getGridJson/mod_pchome"){
            $newmsg="pchome";
        }

        return $newmsg;
    }
    public function copylayout(Request $request) {
        $user         = Auth::user();
        $copytouser   = $request->copytouser;
        $layoutids    = $request->ids;
        $insertData   = array();
        $msg          = "";
        $keydata = DB::table('sys_layout')->whereIn('id', $layoutids)->pluck("key");
        $copytouser_layout = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->count();
        if($copytouser_layout>0){
            $errormsg = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->get();
            foreach($errormsg as $row2) {
                $generror = $this->changerror($row2->key);        
                $msg = $msg.$generror.",";
            }
            $msg = rtrim($msg, ", ");
            return response()->json(["msg" => "error","errormsg" => $msg]);
        }else{
            $newkey = DB::table('sys_layout')->whereIn('id', $layoutids)->get();
            $cnt =count($newkey);
            for($i=0; $i<$cnt; $i++) {
                $data = [
                    'key'           => $newkey[$i]->key,
                    'data'          => $newkey[$i]->data,
                    'g_key'         => $user->g_key,
                    'c_key'         => $user->c_key,
                    's_key'         => $user->s_key,
                    'd_key'         => $user->d_key,
                    'lang'          => $newkey[$i]->lang,
                    'table_name'    => $newkey[$i]->table_name,
                    'created_by'    => $copytouser,
                    'updated_by'    => $copytouser,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
                array_push($insertData, $data);
            }
            DB::table('sys_layout')->insert($insertData);
            return response()->json(["msg" => "success"]);
        }
    }
    public function updatelayout(Request $request) {
        $user         = Auth::user();
        $layoutid   = $request->layoutid;

        $keydata = DB::table('sys_layout')
        ->where('id', $layoutid)
        ->first();

        $layoutcontent = json_decode($keydata->data);

        $updateContent = array();

        foreach ($layoutcontent->columns as $columnName => $columnData) {
            if($columnName !="_checkboxcolumn") {
                $columnData->text = trans($keydata->table_name.'.'.$columnName);
            }
            # code...
        }
        $layoutcontent = json_encode($layoutcontent);
        DB::table('sys_layout')
        ->where('id', $layoutid)
        ->update([
            'data'=>$layoutcontent
        ]);

        return response()->json(["message" => "success"  ]);
    }
    public function copylayoutconfirm(Request $request) {
        $user         = Auth::user();
        $insertData = array();
        $copytouser   = $request->copytouser;
        $layoutids    = $request->ids;
        $keydata = DB::table('sys_layout')->whereIn('id', $layoutids)->pluck("key");
        $newkey = DB::table('sys_layout')->whereIn('id', $layoutids)->get();
        $copytouser_layout = DB::table('sys_layout')->where('created_by', $copytouser)->whereIn('key',$keydata)->delete();
        $cnt =count($newkey);

        for($i=0; $i<$cnt; $i++) {
            $data = [
                'key'           => $newkey[$i]->key,
                'data'          => $newkey[$i]->data,
                'g_key'         => $user->g_key,
                'c_key'         => $user->c_key,
                's_key'         => $user->s_key,
                'd_key'         => $user->d_key,
                'lang'         =>  $newkey[$i]->lang,
                'table_name'    => $newkey[$i]->table_name,
                'created_by'    => $copytouser,
                'updated_by'    => $copytouser,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ];
            array_push($insertData, $data);
        }
        DB::table('sys_layout')->insert($insertData);
        return response()->json(["msg" => "success"]);
    }

    public function getownerlookup(Request $request) {
        $user         = Auth::user();
        $ownercd = $request->dc_id;
        $codition = "";
        $condition = "";
        if($user->check_owner !="*") {
            $userOwnerGroup = explode(',',$user->check_owner);

            $checkOwner = DB::table('mod_customer_relation')
            ->whereIn('group_cd', $userOwnerGroup)
            ->pluck('cust_no')->toArray();
            $condition = "'".implode("','",$checkOwner)."'";
            $str =  "status='A' AND type='OTHER' and wh_no like('%".$ownercd."%') and  cust_no in(".$condition.")";
            $val =  Crypt::encrypt("status='A' AND type='OTHER' and wh_no like('%".$ownercd."%') and  cust_no in(".$condition.")");
        } else {
            $str =  "status='A' AND type='OTHER' and wh_no like('%".$ownercd."%')";
            $val =  Crypt::encrypt("status='A' AND type='OTHER' and wh_no like('%".$ownercd."%')");
        }
       

       return response()->json(['msg' => 'success','val'=>$val  ]);
    }


}
