<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Controllers\Controller;
use App\Services\SampleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;

class HomeController extends Controller
{
    protected $sampleService;

    public function __construct(SampleService $sampleService) {
        $this->sampleService = $sampleService;
    }

    public function index (Request $request) {
        return view('welcome');
    }
}
