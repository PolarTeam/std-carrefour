<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Controllers\Controller;
use App\Services\LanguageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\LogActivity;

class languageController extends Controller
{
    protected $languageService;

    public function __construct(LanguageService $languageService) {
        $this->languageService = $languageService;
    }
    
    public function index() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $user = Auth::user();
        $sql = "SELECT company_id, seq, func_name, grid_field_type, field_name, display_name FROM function_schema WHERE func_name='function_schema_lang' and show_on_grid = 'Y'";
        $functionSchema = DB::select($sql);

        $datafields = array();
        $cmptycolumns = array();

        foreach($functionSchema as $row) {
            array_push($datafields, array(
                'name' => $row->field_name,
                'type' => $row->grid_field_type
            ));

            if ($row->grid_field_type == "date") {
                array_push($cmptycolumns, array(
                    'text'        => trans('function_schema_lang.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => 100,
                    "nullable"    => true,
                    "cellsformat" => "yyyy-MM-dd HH:mm:ss",
                    "filtertype"  => "range",
                    "cellsalign"  => "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            } else {
                array_push($cmptycolumns, array(
                    'text'        => trans('function_schema_lang.'.$row->display_name),
                    'datafield'   => $row->field_name,
                    "width"       => 100,
                    "dbwidth"     => "10",
                    "nullable"    => false,
                    "cellsformat" => "",
                    "filtertype"  => "textbox",
                    "cellsalign"  => $row->grid_field_type == 'number' ? "right" : "left",
                    "values"      => "",
                    "filterdelay" => 99999999
                ));
            }

        }

        $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='function_schema_lang' AND user_id={$user->id} LIMIT 1";
        $gridLayout = DB::select($sql);

        $columns = array();
        if(!$gridLayout) {
            //預設layout
            $sql = "SELECT id, user_id, layout_code, layout_content, is_enabled, is_deleted, created_at, updated_at FROM grid_layout WHERE layout_code='function_schema_lang' AND user_id = '29' LIMIT 1";
            $gridLayout = DB::select($sql);

            //連預設都沒有的時候用產生的
            if(!$gridLayout) {
                $columns = json_encode($cmptycolumns);
            } else {
                $columns = $gridLayout[0]->layout_content;
            }

        }

        $lang = app()->getLocale();
        return view('language.index', array(
            'title'      => trans('language.titleName'),
            'datafields' => json_encode($datafields),
            'columns'    => $columns,
            'lang'       => $lang
        ));
    }

    public function edit($lang,$id) {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('language.titleName');
        $this->data['entry'] = array();
        $this->data['id']    = $id;
        $this->data['lang']  = $lang;
        $this->data['detailColumn']  =  $this->languageService->getdetail($id);
        return view('language.edit', $this->data);
    }

    public function create() {
        if(Auth::user()->hasPermissionTo('SYSCONTROL')){
            
        } else {
            return back();
        }
        $this->data['title'] = trans('language.titleName');
        $this->data['entry'] = array();
        $this->data['id'] = null;
        $this->data['detailColumn'] = null;
        $this->data['lang']     = app()->getLocale();
        return view('language.edit', $this->data);
    }

    public function show($lang,$id) {
        try {
            $languagedata = $this->languageService->get($id);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        $detailColumn = $this->languageService->getdetail($id);
        $filed = $this->languageService->getfiled('function_schema_lang');

        foreach ($detailColumn as $key => $row) {
            $filed[] = array( 'field_type'=> 'string', 'field_name'=>$row->id.'-'.$row->func_name.'-'.$row->field_name);
            $filed[] = array( 'field_type'=> 'string', 'field_name'=>$row->id.'-'.$row->func_name.'-'.$row->field_name.'-en');
            $filed[] = array( 'field_type'=> 'string', 'field_name'=>$row->id.'-'.$row->func_name.'-'.$row->field_name.'-tw');
        }

        return response()->json([
            'success' => true,
            'message' => "get language success",
            'data'    => $languagedata,
            'filed'   => $filed
        ]);
    }

    public function store (Request $request) {

        $result = array();
        try {
            $result = $this->languageService->create($request);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('新增多語系');
        return response()->json([
            'success' => true,
            'message' => '新增成功',
            'data' => $result
        ]);
    }

    public function update (Request $request, $id) {
        $result = array();
        try {
            $result = $this->languageService->update($request, $id);

        } catch (Exception $e) {
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }

        LogActivity::addToLog('修改多語系');
        return response()->json([
            'success' => true,
            'message' => '修改成功',
            'data' => $result
        ]);
    }

    public function destroy (Request $request, $id) {
        try {
            $this->languageService->delete($id);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        LogActivity::addToLog('刪除多語系');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }

    public function batchDelete (Request $request) {
        DB::beginTransaction();

        try {
            $this->languageService->batchDelete($request);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        DB::commit();

        LogActivity::addToLog('刪除多語系');
        return response()->json([
            'success' => true,
            'message' => '刪除成功'
        ]);
    }


    public function exportdata(Request $request) {

        $downloadLink = '';

        try {
            $downloadLink = $this->languageService->export($request);
        }
        catch(\Exception $e) {
            Log::error($e);
            return response()->json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }

        if(empty($downloadLink)) {
            return response()->json(array(
                'success' => false,
                'message' => '下載錯誤'
            ));
        }

        return response()->json(array(
            'success' => true,
            'donwloadLink' => $downloadLink,
            'message' => 'success'
        ));
    }

}
