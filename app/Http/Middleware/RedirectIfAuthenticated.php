<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user();
            // session(['locale' => $user->lnag]);
            $ueserrole = explode(',', $user->role);
            $redirectto = DB::table('bscode')->where('cd_type', 'LOGINPAGE')->whereIn('cd', $ueserrole)->value('value1');
            return redirect('/'.$redirectto);
            // return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
