<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailFormat extends Model
{
    protected $table = 'mail_format';
    protected $fillable = [
        'id',
        'type',
        'type_descp',
        'name',
        'content',
        'title',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
        'created_at',
        'updated_at'
    ];
}