<?php

namespace App\Repositories;

use App\ModFcm;
use App\SysMessage;
use Illuminate\Support\Facades\DB;
class FcmRepository
{
    public function get($id) {
        return ModFcm::find($id);
    }

    public function create(array $data) {
        return ModFcm::create($data);
    }

    public function batchInsert(array $data) {
        return ModFcm::insert($data);
    }

    public function update($id, array $data) {
        $fcm = ModFcm::find($id);
        return $fcm->update($data);
    }

    public function delete ($id) {
        $fcm = ModFcm::find($id);
        return $fcm->delete();
    }

    // public function getByName ($id,$mark_name) {
    //     return ModFcm::where('mark_name', $mark_name)->where('id', '!=', $id)->first();
    // }
    
}