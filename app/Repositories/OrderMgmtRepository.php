<?php

namespace App\Repositories;

use App\OrderMgmtModel;
use App\DlvModel;

class OrderMgmtRepository
{
    public function get($id) {
        return OrderMgmtModel::find($id);
    }
    public function getByno($wms_ord_no) {
        return OrderMgmtModel::where('cust_ord_no',$wms_ord_no)->first();
    }
    public function create(array $data) {
        return OrderMgmtModel::create($data);
    }
    public function getdataByids($orderIds) {
        return OrderMgmtModel::whereIn('id',$orderIds)->get();
    }
    public function updateStatus($id, array $data) {
        return OrderMgmtModel::whereIn('id',$id)->update($data);
    }

    public function batchInsert(array $data) {
        return OrderMgmtModel::insert($data);
    }

    public function update($id, array $data) {
        $order = OrderMgmtModel::find($id);
        return $order->update($data);
    }

    public function delete ($id) {
        $order = OrderMgmtModel::find($id);
        return $order->delete();
    }

    public function getByName ($name) {
        return OrderMgmtModel::where('name', $name)->first();
    }

    
}