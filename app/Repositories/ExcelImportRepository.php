<?php

namespace App\Repositories;

use App\TmpModOrderModel;
use App\TmpDssOrderModel;
class ExcelImportRepository
{
    public function get($id) {
        return TmpModOrderModel::find($id);
    }

    public function create(array $data) {
        return TmpModOrderModel::create($data);
    }
    public function createdss(array $data) {
        return TmpDssOrderModel::create($data);
    }
    public function batchInsert(array $data) {
        return TmpModOrderModel::insert($data);
    }

    public function update($id, array $data) {
        $tmpModOrderModel = TmpModOrderModel::find($id);
        return $tmpModOrderModel->update($data);
    }

    public function delete ($id) {
        $tmpModOrderModel = TmpModOrderModel::find($id);
        return $tmpModOrderModel->delete();
    }

    public function getDetail($id) {
        return TmpModOrderModel::find($id);
    }
    
}