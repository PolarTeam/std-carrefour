<?php

namespace App\Repositories;

use App\OrderDetailModel;

class OrderDetailRepository
{
    public function get($id) {
        return OrderDetailModel::find($id);
    }

    public function create(array $data) {
        return OrderDetailModel::create($data);
    }

    public function batchInsert(array $data) {
        return OrderDetailModel::insert($data);
    }
    public function update($id, array $data) {
        $orderDetail = OrderDetailModel::find($id);
        return $orderDetail->update($data);
    }

    public function delete ($id) {
        $orderDetail = OrderDetailModel::find($id);
        return $orderDetail->delete();
    }

    public function getDetail ($ord_id) {
        return OrderDetailModel::where('ord_id', $ord_id)->get();
    }

    
}