<?php

namespace App\Repositories\User;

use App\User;
use App\Warehouse;
use App\WhUser;
use App\MenuItem;
use App\FunctionSchema;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as OClient;
use GuzzleHttp\Exception\ClientException;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
class UserRepository implements UserRepositoryInterface {
    const SUCCUSUS_STATUS_CODE = 200;
    const UNAUTHORISED_STATUS_CODE = 401;
    const BASE_URL = 'https://std-wms.target-ai.com:17890';

    public function __construct(Client $client) {
        $this->http = $client;
    }

    public function register(Request $request) {
        $email = $request->email;
        $username = $request->username;
        $password = $request->password;
        $client = $request->client;
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        User::create($input);
        $response = $this->getTokenAndRefreshToken($username, $password, $client);
        return $this->response($response["data"], $response["statusCode"]);
    }

    public function login(Request $request) {
        $username  = $request->email;
        $password  = $request->password;
        $client    = $request->client_id;
        $d_token   = $request->d_token;
        $phoneType = $request->phone_type;
        Log::info("login data");
        Log::info($request->all());
        $checkTruck = DB::table('mod_car_trader')
        ->where('cust_no',$username)
        ->first();
        $whuser = 'Y';
        if(isset($checkTruck)) {
            $limitday = DB::table('bscode')
            ->where('cd_type', 'DRIVERLOGINDAY')
            ->where('cd', 'driverloginday')
            ->value('value1');
            $days = date("Y-m-d ", strtotime("-".$limitday." day"));



            $checkuserexist = DB::table('users')
            ->where('email', $request->phone)
            ->where('truck_cmp_no', $request->email)
            ->first();
            if(!isset($checkuserexist) && $checkTruck->need_order == "Y" ) {
                $userId = DB::connection('mysql::write')->table('users')
                ->insertGetId([
                    'role'           => 'driver',
                    'truck_cmp_no'   => $request->email,
                    'name'           => $request->email,
                    'email'          => $request->phone,
                    'password'       => bcrypt($request->email),
                    'created_at'     => Carbon::now()->toDateTimeString(),
                    'updated_at'     => Carbon::now()->toDateTimeString(),
                    'username'       => $request->phone.$request->email,
                    'account_enable' => "Y",
                    'g_key'          => "DSS",
                    'c_key'          => "DSS",
                    's_key'          => "DSS",
                    'd_key'          => "DSS",
                    'created_by'     => "SYSTEM",
                    'updated_by'     => "SYSTEM",
                ]);
                DB::connection('mysql::write')->table('model_has_roles')
                ->insert([
                    'role_id'    => 43,
                    'model_type' => 'App\User',
                    'model_id'   => $userId,
                ]);
            }

            $doublecheck = DB::table('users')
            ->where('email', $request->phone)
            ->where('truck_cmp_no', $request->email)
            ->first();

            $hasmission = DB::table('mod_dss_dlv')
            ->where('driver_phone', $request->phone)
            ->where('dlv_date' ,'>=', $days)
            ->count();

            Log::info($checkTruck->need_order);
            if(!isset($doublecheck) || $hasmission <= 0 && $checkTruck->need_order == "N") {
                Log::info('登入失敗');
                $sample_detail = array(
                    "code"            => "01",
                    "msg"           => "請確認您的帳號密碼是否輸入正確 以及是否有任務",
                );
                $statusCode =  self::SUCCUSUS_STATUS_CODE;
                return $this->response($sample_detail, $statusCode);
            }
            // $password = $doublecheck->truck_cmp_no;
            $username =$request->phone.$request->password;
            $whuser = 'N';
            $driveraccount =  $request->phone.$request->password;
        }


        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();
            $Oclient = $this->getOClient($client);
            Log::info('getcounfinish');
            Log::info( $user);
            DB::table('users')
            ->where('d_token', $d_token)
            ->update([
                'phoneType'=>$phoneType,
                'd_token' => NULL
            ]);

            DB::table('users')
            ->where('id', $user->id)
            ->update([
                'd_token' => $d_token
            ]);

            DB::table('oauth_access_tokens')
            ->where('user_id', $user->id)
            ->where('client_id', $Oclient->id)
            ->update([
                'revoked' => true
            ]);
            if( $whuser  == "N") {
                Log::info('司機登入資訊');
                Log::info($driveraccount);
                $response   = $this->getTokenAndRefreshToken($driveraccount, $password, $client);
            } else {
                $response   = $this->getTokenAndRefreshToken($username, $password, $client);
            }
            Log::info($response);
            $data       = $response["data"];
            $statusCode = $response["statusCode"];

            $user = Auth::user();
            Log::info('登入成功');
            LogActivity::addToLog('登入成功');
        } else {
            //
            //每次修改密碼 不產生新的帳號
            //
            Log::info('登入失敗');
            $sample_detail = array(
                "code"            => "01",
                "msg"           => "請確認您的帳號密碼是否輸入正確!",
            );
            $statusCode =  self::SUCCUSUS_STATUS_CODE;
            return $this->response($sample_detail, $statusCode);



            // $data = [
            //     'success' => false,
            //     'message' => '帳號或密碼錯誤'
            // ];
            // $statusCode =  self::SUCCUSUS_STATUS_CODE;
        }
        $sample_detail = array(
            "msg"            => "success",
            "data"           => null,
        );
        $refresh_time = DB::table('bscode')->where('cd_type','REPASSTIME')->where('g_key',$user->g_key)->first();
        $gps_time = DB::table('bscode')->where('cd_type','SENDGPSTIME')->where('g_key',$user->g_key)->first();

        $this_query = DB::table('users');
        $this_query->select('id', 'name', 'email', 'remember_token', 'online');
        $this_query->where('id', $user->id);
        
        $imglimit = DB::table('bscode')->where('g_key',$user->g_key)->where('cd_type', 'UPLOADIMGLIMIT')->where('cd', 'uploadimglimit')->value('value1');
        $ntervaltime = DB::table('bscode')->where('g_key',$user->g_key)->where('cd_type', 'ntervaltime')->where('cd', 'ntervaltime')->value('value1');
        
        $sample_detail['data']["headers"]                    = (object)[];
        $sample_detail['data']['original']["data"]           = $this_query->get();
        $sample_detail['data']['original']["wh_user"]        = $whuser;
        $sample_detail['data']['original']["token_type"]     = $data['token_type'];
        $sample_detail['data']['original']["access_token"]   = $data['access_token'];
        $sample_detail['data']['original']["refresh_token"]  = $data['refresh_token'];
        $sample_detail['data']['original']["errorcd"]        = "0";
        $sample_detail['data']['original']["app_permission"] = [];
        $sample_detail['data']['original']["refresh_time"]   = $refresh_time->value1;
        $sample_detail['data']['original']["gps_time"]       = $gps_time->value1;
        $sample_detail['data']['original']["img_limit"]      = (int)$imglimit;
        $sample_detail['data']['original']["nterval_time"]   = $ntervaltime;
        $sample_detail['data']["exception"]                  = null;
        return $this->response($sample_detail, $statusCode);
    }

    public function refreshToken(Request $request) {
        if (is_null($request->header('Refreshtoken'))) {
            return $this->response(['error'=>'Unauthorised'], self::UNAUTHORISED_STATUS_CODE);
        }

        $refresh_token = $request->header('Refreshtoken');
        $Oclient = $this->getOClient($request->client);
        $formParams = [ 'grant_type' => 'refresh_token',
                        'refresh_token' => $refresh_token,
                        'client_id' => $Oclient->id,
                        'client_secret' => $Oclient->secret,
                        'scope' => '*'];

        return $this->sendRequest("/oauth/token", $formParams);
    }

    public function details() {
        $user = Auth::user();
        return $this->response($user, self::SUCCUSUS_STATUS_CODE);
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return $this->response(['message' => 'Successfully logged out'], self::SUCCUSUS_STATUS_CODE);
    }

    public function response($data, int $statusCode) {
        $response = ["data"=>$data, "statusCode"=>$statusCode];
        return $response;
    }

    public function getTokenAndRefreshToken(string $username, string $password, string $client) {
        $Oclient = $this->getOClient($client);
        $formParams = [ 'grant_type' => 'password',
                        'client_id' => $Oclient->id,
                        'client_secret' => $Oclient->secret,
                        'username' => $username,
                        'password' => $password,
                        'scope' => '*'];

        return $this->sendRequest("/oauth/token", $formParams);


        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        // CURLOPT_URL => 'https://dsstms.standard-info.com/oauth/token',
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => '',
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => true,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => 'POST',
        // CURLOPT_POSTFIELDS => array(
        //     'grant_type'    => 'password',
        //     'client_id'     => $Oclient->id,
        //     'client_secret' => $Oclient->secret,
        //     'username'      => $username,
        //     'password'      => $password,
        //     'scope'         => '*'
        // ),
        // CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json',
        //     'Accept: application/json'
        // ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // dd($response);
        // return $response;

    }

    public function sendRequest(string $route, array $formParams) {
        try {
            $url = env('APP_URL').$route;
            $response = $this->curlRequest($url, $formParams);//$this->http->request('POST', $url, ['form_params' => $formParams]);
            $statusCode = self::SUCCUSUS_STATUS_CODE;
            $data = json_decode($response, true);//json_decode((string) $response->getBody(), true);
        } catch (ClientException $e) {
            $statusCode = $e->getCode();
            $data = ['error'=>'OAuth client error'];
        }

        return ["data" => $data, "statusCode"=>$statusCode];
    }

    public function getOClient($client) {
        return OClient::where('id', $client)->first();
    }

    public function getOnlineUser(Request $request) {
        $Oclient = $this->getOClient($request->client);
        $currentUsers = DB::table('oauth_access_tokens')->join('users', 'users.id', '=', 'oauth_access_tokens.user_id')
                        ->select('oauth_access_tokens.*')
                        ->where('users.is_admin', '<>', 1)
                        ->where('oauth_access_tokens.revoked', 0)
                        ->where('oauth_access_tokens.client_id', $Oclient->id)
                        ->get();

        $currentCnt = count($currentUsers);

        $userIds = array();

        foreach($currentUsers as $currentUser) {
            array_push($userIds, $currentUser->user_id);
        }

        $onlineUser = User::select('id', 'email', 'name', 'username')->whereIn('id', $userIds)->where('is_admin', '<>', 1)->get();
        
        $statusCode = self::SUCCUSUS_STATUS_CODE;
        
        return $this->response([
            'onlineUserCount' => $currentCnt,
            'user'            => $onlineUser,
            'superUser'       => ['timfine', 'tonyfine'],
            'onlineLimitUser' => (int)Crypt::decrypt(env('ONLINE_USER'))
        ], $statusCode);
    }

    public function superLogout(Request $request) {
        $Oclient = $this->getOClient($request->client);
        DB::table('oauth_access_tokens')
            ->where('user_id', $request->userId)
            ->where('client_id', $Oclient->id)
            ->update([
                'revoked' => true
            ]);


        return $this->response(['message' => 'Successfully logged out'], self::SUCCUSUS_STATUS_CODE);
    }

    private function curlRequest($url, $formParams) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $formParams,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}