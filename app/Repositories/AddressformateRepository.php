<?php

namespace App\Repositories;

use App\AddressFormate;
use App\AddressFormateDetail;
use Illuminate\Support\Facades\DB;
class AddressformateRepository
{
    public function get($id) {
        return AddressFormate::find($id);
    }

    public function create(array $data) {
        return AddressFormate::create($data);
    }

    public function batchInsert(array $data) {
        return AddressFormate::insert($data);
    }

    public function update($id, array $data) {
        $addressFormate = AddressFormate::find($id);
        return $addressFormate->update($data);
    }

    public function delete ($id) {
        $addressFormate = AddressFormate::find($id);
        return $addressFormate->delete();
    }
    public function insertdetail ($id,$belong_cd) {
        $addressFormate = AddressFormate::find($id);
        $data = array();
        $data['main_id'] = $belong_cd;
        $data['replace_address'] = $addressFormate['formate_address'];
        AddressFormateDetail::create($data);

        return $addressFormate->delete();
    }
    // public function getByName ($cust_no,$wh_no) {
    //     return AddressFormate::where('cust_no', $cust_no)->where('wh_no', $wh_no)->first();
    // }
    // public function getExist ($id,$cust_no,$wh_no) {
    //     return AddressFormate::where('id','!=', $id)->where('cust_no', $cust_no)->where('wh_no', $wh_no)->first();
    // }

    public function getdetail ($mainid) {
        return AddressFormateDetail::where('main_id', $mainid)->get();
    }
    
    public function createdetail(array $data) {
        return AddressFormateDetail::create($data);
    }

    // public function getDetailByCode ($cdType ,$cd) {
    //     return AddressFormateDetail::where('cd_type', $cdType)->where('cd', $cd)->first();
    // }

    public function updatedetail($id, array $data) {
        $AddressFormateDetail = AddressFormateDetail::find($id);
        return $AddressFormateDetail->update($data);
    }

    public function deletedetail ($id) {
        $AddressFormateDetail = AddressFormateDetail::find($id);
        return $AddressFormateDetail->delete();
    }
    
}