<?php

namespace App\Repositories;

use App\Message;
use Illuminate\Support\Facades\DB;
class MessageRepository
{
    public function get($id) {
        return Message::find($id);
    }
    public function getChoosedata($ids) {
        return Message::whereIn('id',$ids)->get();
    }
    public function create(array $data) {
        return Message::create($data);
    }

    public function update($id, array $data) {
        $message = Message::find($id);
        return $message->update($data);
    }

    public function delete ($id) {
        $message = Message::find($id);
        return $message->delete();
    }
    public function getUnread($userName) {
        return Message::where('to_by', $userName)->whereNull('read_time')->count();
    }
    public function getMessageByUser($userName) {
        return Message::where('from_by', $userName)->orwhere('from_by','userName')->get();
    }
    
}