<?php

namespace App\Repositories;

use App\BaseMessage;
use App\Message;
use App\Fcm;
use Illuminate\Support\Facades\DB;
class BasemessageRepository
{
    public function get($id) {
        return BaseMessage::find($id);
    }
    public function getChoosedata($ids) {
        return BaseMessage::whereIn('id',$ids)->get();
    }
    public function create(array $data) {
        return BaseMessage::create($data);
    }
    public function createMessage(array $data) {
        return Message::create($data);
    }
    public function createFcm(array $data) {
        return Fcm::create($data);
    }
    public function batchInsert(array $data) {
        return BaseMessage::insert($data);
    }

    public function update($id, array $data) {
        $basemessage = BaseMessage::find($id);
        return $basemessage->update($data);
    }

    public function delete ($id) {
        $basemessage = BaseMessage::find($id);
        return $basemessage->delete();
    }
    public function getUnread($userName) {
        return Message::where('to_by', $userName)->whereNull('read_time')->count();
    }
    // public function getByName ($id,$basemessage_name) {
    //     return BaseMessage::where('pallet_name', $basemessage_name)->where('id', '!=', $id)->first();
    // }
    
}