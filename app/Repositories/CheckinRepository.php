<?php

namespace App\Repositories;

use App\CheckInModel;
use App\MarkReserve;
use Illuminate\Support\Facades\DB;
class CheckinRepository
{
    public function get($id) {
        return CheckInModel::find($id);
    }

    public function create(array $data) {
        return CheckInModel::create($data);
    }

    public function checkreserve($data) {
        MarkReserve::where('reserve_date','')->first();
        return ;
    }

    public function batchInsert(array $data) {
        return CheckInModel::insert($data);
    }

    public function update($id, array $data) {
        $checkInModel = CheckInModel::find($id);
        return $checkInModel->update($data);
    }

    public function delete ($id) {
        $checkInModel = CheckInModel::find($id);
        return $checkInModel->delete();
    }

    public function getByName ($name) {
        return CheckInModel::where('name', $name)->first();
    }

    
}