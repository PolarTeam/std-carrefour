<?php

namespace App\Repositories;

use App\CarbonCal;
use Illuminate\Support\Facades\DB;
class CarboncalConRepository
{
    public function get($id) {
        return CarbonCal::find($id);
    }

    public function create(array $data) {
        return CarbonCal::create($data);
    }

    public function batchInsert(array $data) {
        return CarbonCal::insert($data);
    }

    public function update($id, array $data) {
        $carboncal = CarbonCal::find($id);
        return $carboncal->update($data);
    }

    public function delete ($id) {
        $carboncal = CarbonCal::find($id);
        return $carboncal->delete();
    }

    // public function getByName ($id,$mark_name) {
    //     return CarbonCal::where('mark_name', $mark_name)->where('id', '!=', $id)->first();
    // }
    
}