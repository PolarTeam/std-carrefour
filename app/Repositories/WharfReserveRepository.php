<?php

namespace App\Repositories;

use App\MarkReserve;
use App\MarkProfile;
use App\WareHouse;
use App\WareHouseDetail;
use Illuminate\Support\Facades\DB;
class WharfReserveRepository
{
    public function get($id) {
        return MarkReserve::find($id);
    }

    public function create(array $data) {
        return MarkReserve::create($data);
    }

    public function checkdate(array $data) {

        $date   = explode( '-',$data['reserve_date']);
        $year   = $date[0];
        $month = $date[1];
        $reserveday    = $date[2];
        $mainWh = WareHouse::where('cust_no',$data['dc_id'])
        ->first();

        $notworkdays = DB::table('mod_warehouse_notwork')
        ->where('warehouse_id', $mainWh->id)
        ->where('years',  $year)
        ->where('month',  (int)$month)
        ->get();

        foreach ($notworkdays as $key => $row) {
            $days = explode(',',$row->days);
            if( in_array($reserveday, $days) ) {
                return true;
            }
        }

        return false;

    }
    public function checktime(array $data) {
        $warehouse = DB::table('mod_warehouse')
        ->where('cust_no', $data['dc_id'])
        ->first();
        $standardDate  = array("Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday" ,"Saturday");
        $date          = $data['reserve_date'];
        $now           = $data['reserve_date'];
        $dayNameOfWeek = date("l", strtotime($now));
        $day           = array_search($dayNameOfWeek, $standardDate);

        $notworks = DB::table('mod_warehouse_notwork')
        ->select('years','month','days')
        ->where('warehouse_id',$warehouse->id)
        ->get();
        
        $notworkArray = array();
        foreach ($notworks as $notworkkey => $notworkrow) {
            $notworkdays  = explode(',',$notworkrow->days);
            foreach ($notworkdays as $key => $value) {
                $notworkArray[]  = $notworkrow->years.'-'.str_pad($notworkrow->month, 2, "0", STR_PAD_LEFT) .'-'.$value ;
            }
        }
        $businessArray = array();
        
        if(!in_array($date, $notworkArray)) {
            $business = DB::table('mod_warehouse_detail')
            ->select('start_at', 'end_at')
            ->where('warehouse_id',$warehouse->id)
            ->where('dayofweek', $standardDate[$day])
            ->first();
        }

        if(isset($business)) { 
            return false;
        } else {
            //無時段可預約
            return true;
        }

    }
    public function checkCarTypeAndReserve(array $data) {

        //
        $sameTimeCcount = MarkReserve::where('dc_id',$data['dc_id'])
        ->where('reserve_date',$data['reserve_date'])
        ->where('car_size', $data['car_size'])
        ->where('full_date', '>', $data['full_date'] )
        ->where('end_at','<', $data['full_date'])
        ->count();

        $totalCount = MarkProfile::where('wh_no',$data['dc_id'])
        ->where('is_enabled','Y')
        ->where('tyPe','!=','code4')
        ->where('parked_models', $data['car_size'])
        ->whereRaw("( owner_cd like (%".$data['owner_cd']."%) )")
        ->count();
        //
        $result = $totalCount - $sameTimeCcount;
        return $result ;
    }
    public function checkCarType(array $data) {

        //車型
        $carType = DB::table('bscode')
        ->where('cd_type', 'IMPORTCARTYPE')
        ->where('cd', $data['car_type'])
        ->first();

        // $carType->value1 = empty($data['car_type']) ? $carType->value1 : '貨櫃';
        // if($carType->cd_descp == "小車" || $carType->cd_descp =="大車") {
        //     $carType->cd_descp = mb_substr($carType->cd_descp ,0,1);
        // }
        $totalCount = MarkProfile::where('wh_no', 'like', '%'.$data['dc_id'].'%')
        ->where('is_enabled','Y')
        ->where('type','!=','code4')
        ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
        ->count();
    
        return $totalCount;

    }
    public function checkByTruckcmpCount(array $data, $id = null) {

        $sameConid = MarkReserve::where('dc_id',$data['dc_id'])
        ->where('dc_id',$data['dc_id'])
        ->where('truck_cmp_name',$data['truck_cmp_name'])
        ->where('reserve_date',$data['reserve_date']);
        if (isset($id)) {
            $sameConid->where('id', '!=', $id);
        }
        $sameConid = $sameConid->count();

        if($sameConid >= 3 ) {
            return "貨櫃號碼";
        } 
        return null;
    }
    public function checkByTruckcmp(array $data) {
        if(empty($data['id'])) {
            $sameConid = MarkReserve::where('dc_id',$data['dc_id'])
            ->where('dc_id',$data['dc_id'])
            ->where('truck_cmp_name',$data['truck_cmp_name'])
            ->where('reserve_date',$data['reserve_date'])
            ->where('con_no',$data['con_no'])
            ->count();
        } else {
            $sameConid = MarkReserve::where('dc_id',$data['dc_id'])
            ->where('id','!=',$data['id'])
            ->where('dc_id',$data['dc_id'])
            ->where('truck_cmp_name',$data['truck_cmp_name'])
            ->where('reserve_date',$data['reserve_date'])
            ->where('con_no',$data['con_no'])
            ->count();
        }


        if($sameConid > 0 ) {
            return "貨櫃號碼";
        } 
        return null;
        
    }
    public function checkReserve(array $data, $id = null) {

        $carType = DB::table('bscode')
        ->where('cd_type', 'IMPORTCARTYPE')
        ->where('cd', $data['car_type'])
        ->first();

        // $carType->value1 = empty($data['con_type']) ? $carType->value1 : '貨櫃';

        // if($carType->cd_descp == "小車" || $carType->cd_descp =="大車") {
        //     $carType->cd_descp = mb_substr($carType->cd_descp ,0,1);
        // }

        $sameTimeCcount = MarkReserve::where('dc_id', 'like', '%'.$data['dc_id'].'%')
        ->where('reserve_date',$data['reserve_date'])
        ->where('car_size', $carType->cd_descp)
        ->where('full_date', '<=', $data['end_at'] )
        ->where('end_at','>', $data['full_date'] );
        if (isset($id)) {
            $sameTimeCcount->where('id', '!=', $id);
        }
        $sameTimeCcount = $sameTimeCcount->count();

        $totalCount = MarkProfile::where('wh_no', 'like', '%'.$data['dc_id'].'%')
        ->where('is_enabled','Y')
        ->where('tyPe','!=','code4')
        ->where('parked_models_name', 'like', '%'.$data['car_type'].'%')
        ->count();
        $result = $totalCount - $sameTimeCcount;
        return $result ;
    }
    public function batchInsert(array $data) {
        return MarkReserve::insert($data);
    }

    public function update($id, array $data) {
        $markReserve = MarkReserve::find($id);
        return $markReserve->update($data);
    }

    public function delete ($id) {
        $markReserve = MarkReserve::find($id);
        return $markReserve->delete();
    }

    // public function getByName ($id,$mark_name) {
    //     return MarkReserve::where('mark_name', $mark_name)->where('id', '!=', $id)->first();
    // }
    
}