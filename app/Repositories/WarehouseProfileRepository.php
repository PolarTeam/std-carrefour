<?php

namespace App\Repositories;

use App\WareHouse;
use App\WareHouseDetail;
use App\WareHouseNotwork;
use Illuminate\Support\Facades\DB;

class WarehouseProfileRepository
{
    public function get($id) {
        return WareHouse::find($id);
    }
    public function getdetailByid($id) {
        return WareHouseDetail::find($id);
    }
    public function getdetail($warehouseId) {
        return WareHouseDetail::where('warehouse_id', $warehouseId)->get();
    }
    public function createdetail(array $data) {
        return WareHouseDetail::create($data);
    }
    public function deletedetail ($id) {
        $wareHouseDetail = WareHouseDetail::find($id);
        return $wareHouseDetail->delete();
    }
    public function updatedetail($id, array $data) {
        $wareHouseDetail = WareHouseDetail::find($id);
        return $wareHouseDetail->update($data);
    }
    public function checkdetail($id,$warehouse_id, $dayofweek) {
        return WareHouseDetail::where('id', '!=', $id)->where('warehouse_id', $warehouse_id)->where('dayofweek', $dayofweek)->first();
    }

    public function create(array $data) {
        return WareHouse::create($data);
    }

    public function batchInsert(array $data) {
        return WareHouse::insert($data);
    }

    public function update($id, array $data) {
        $wareHouse= WareHouse::find($id);
        return $wareHouse->update($data);
    }

    public function delete ($id) {
        $wareHouse= WareHouse::find($id);
        return $wareHouse->delete();
    }

    // key值判斷
    public function getByKey ($cust_no) {
        return WareHouse::where('cust_no', $cust_no)->first();
    }

    // key值判斷
    public function checkexist ($id,$cust_no) {
        return WareHouse::where('cust_no', $cust_no)->where('id', '!=', $id)->first();
    }

    public function getnotwork($warehouseId) {
        return WareHouseNotwork::where('warehouse_id', $warehouseId)->get();
    }
    public function createnotwork(array $data) {
        return WareHouseNotwork::create($data);
    }
    public function deletenotwork ($id) {
        $wareHousenotwork = WareHouseNotwork::find($id);
        return $wareHousenotwork->delete();
    }
    public function updatenotwork($id, array $data) {
        $wareHousenotwork = WareHouseNotwork::find($id);
        return $wareHousenotwork->update($data);
    }

}