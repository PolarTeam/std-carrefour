<?php

namespace App\Repositories;

use App\CloseOrderModel;


class CloseOrderRepository
{
    public function get($id) {
        return CloseOrderModel::find($id);
    }
    public function getbyordid($id) {
        return CloseOrderModel::where('ord_id',$id)->first();
    }
    public function getByno($wms_ord_no) {
        return CloseOrderModel::where('cust_ord_no',$wms_ord_no)->first();
    }
    public function create(array $data) {
        return CloseOrderModel::create($data);
    }
    public function getdataByids($orderIds) {
        return CloseOrderModel::whereIn('id',$orderIds)->get();
    }
    public function updateStatus($id, array $data) {
        return CloseOrderModel::whereIn('id',$id)->update($data);
    }

    public function batchInsert(array $data) {
        return CloseOrderModel::insert($data);
    }

    public function update($id, array $data) {
        $order = CloseOrderModel::find($id);
        return $order->update($data);
    }

    public function delete ($id) {
        $order = CloseOrderModel::find($id);
        return $order->delete();
    }

    public function getByName ($name) {
        return CloseOrderModel::where('name', $name)->first();
    }

    
}