<?php

namespace App\Repositories;

use App\SysAreaModel;
use Illuminate\Support\Facades\DB;
class SysAreaProfileRepository
{
    public function get($id) {
        return SysAreaModel::find($id);
    }

    public function create(array $data) {
        return SysAreaModel::create($data);
    }

    public function batchInsert(array $data) {
        return SysAreaModel::insert($data);
    }

    public function update($id, array $data) {
        $sysAreaModel = SysAreaModel::find($id);
        return $sysAreaModel->update($data);
    }

    public function delete ($id) {
        $sysAreaModel = SysAreaModel::find($id);
        return $sysAreaModel->delete();
    }

    public function getByName ($name) {
        return SysAreaModel::where('name', $name)->first();
    }

    
}