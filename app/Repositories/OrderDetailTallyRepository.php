<?php

namespace App\Repositories;

use App\OrderGoodsCheckModel;

class OrderDetailTallyRepository
{
    public function get($id) {
        return OrderGoodsCheckModel::find($id);
    }

    public function create(array $data) {
        return OrderGoodsCheckModel::create($data);
    }

    public function batchInsert(array $data) {
        return OrderGoodsCheckModel::insert($data);
    }

    public function update($id, array $data) {
        $orderDetaordergoodscheckilFee = OrderGoodsCheckModel::find($id);
        return $ordergoodscheck->update($data);
    }

    public function delete ($id) {
        $ordergoodscheck= OrderGoodsCheckModel::find($id);
        return $ordergoodscheck->delete();
    }

    public function getDetail ($ord_id) {
        return OrderGoodsCheckModel::where('ord_id', $ord_id)->get();
    }

    
}