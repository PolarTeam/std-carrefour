<?php

namespace App\Repositories;

use App\AbnormalModel;
use App\AbnormalDetailModel;
use Illuminate\Support\Facades\DB;
class AbnormalRepository
{
    public function get($id) {
        return AbnormalModel::find($id);
    }

    public function create(array $data) {
        return AbnormalModel::create($data);
    }

    public function batchInsert(array $data) {
        return AbnormalModel::insert($data);
    }

    public function update($id, array $data) {
        $abnormalModel = AbnormalModel::find($id);
        return $abnormalModel->update($data);
    }

    public function delete ($id) {
        $abnormalModel = AbnormalModel::find($id);
        return $abnormalModel->delete();
    }

    public function getByName ($name) {
        return AbnormalModel::where('name', $name)->first();
    }


    public function createdetail(array $data) {
        return AbnormalDetailModel::create($data);
    }
    public function getAbnormaldetail ($cdType) {
        return AbnormalDetailModel::where('main_id', $cdType)->orderBy('seq','asc')->get();
    }
    public function getdetail ($cdType ,$cd) {
        return AbnormalDetailModel::where('main_id', $cdType)->where('error_code', $cd)->first();
    }
    public function updatedetail($id, array $data) {
        $detail = AbnormalDetailModel::find($id);
        return $detail->update($data);
    }

    public function deletedetail ($id) {
        $detail = AbnormalDetailModel::find($id);
        return $detail->delete();
    }
    
}