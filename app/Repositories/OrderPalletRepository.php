<?php

namespace App\Repositories;

use App\OrderPalletModel;

class OrderPalletRepository
{
    public function get($id) {
        return OrderPalletModel::find($id);
    }

    public function create(array $data) {
        return OrderPalletModel::create($data);
    }

    public function batchInsert(array $data) {
        return OrderPalletModel::insert($data);
    }

    public function update($id, array $data) {
        $orderPallet = OrderPalletModel::find($id);
        return $orderPallet->update($data);
    }

    public function delete ($id) {
        $orderPallet = OrderPalletModel::find($id);
        return $orderPallet->delete();
    }

    public function getDetail ($ord_id) {
        return OrderPalletModel::where('ord_id', $ord_id)->get();
    }

    
}