<?php

namespace App\Repositories;

use App\Roles;

class RolesRepository
{
    public function get($id) {
        return Roles::find($id);
    }

    public function create(array $data) {
        return Roles::create($data);
    }

    public function batchInsert(array $data) {
        return Roles::insert($data);
    }

    public function update($id, array $data) {
        $roles = Roles::find($id);
        return $roles->update($data);
    }

    public function delete ($id) {
        $roles = roles::find($id);
        return $roles->delete();
    }

    public function getByName ($displayName) {
        return roles::where('display_name', $displayName)->first();
    }

}