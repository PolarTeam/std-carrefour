<?php

namespace App\Repositories;

use App\Models\DlvModel;
use Illuminate\Support\Facades\DB;
class ModDlvRepository
{
    public function get($id) {
        return DlvModel::find($id);
    }

    public function create(array $data) {
        return DlvModel::create($data);
    }

    public function batchInsert(array $data) {
        return DlvModel::insert($data);
    }

    public function update($id, array $data) {
        $dlv = DlvModel::find($id);
        return $dlv->update($data);
    }

    public function delete ($id) {
        $dlv = DlvModel::find($id);
        return $dlv->delete();
    }

    public function getByName ($name) {
        return DlvModel::where('name', $name)->first();
    }

    
}