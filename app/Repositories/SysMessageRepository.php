<?php

namespace App\Repositories;

use App\SysMessage;
use Illuminate\Support\Facades\DB;
class SysMessageRepository
{
    public function get($id) {
        return SysMessage::find($id);
    }

    public function create(array $data) {
        return SysMessage::create($data);
    }

    public function batchInsert(array $data) {
        return SysMessage::insert($data);
    }

    public function update($id, array $data) {
        $sysMessage = SysMessage::find($id);
        return $sysMessage->update($data);
    }

    public function delete ($id) {
        $sysMessage = SysMessage::find($id);
        return $sysMessage->delete();
    }

    // public function getByName ($id,$mark_name) {
    //     return SysMessage::where('mark_name', $mark_name)->where('id', '!=', $id)->first();
    // }
    
}