<?php

namespace App\Repositories;

use App\CustomerRelationModel;
use App\CustomerGroupModel;
use Illuminate\Support\Facades\DB;
class CustomerGroupRepository
{
    public function get($id) {
        return CustomerGroupModel::find($id);
    }

    public function create(array $data) {
        return CustomerGroupModel::create($data);
    }

    public function batchInsert(array $data) {
        return CustomerGroupModel::insert($data);
    }

    public function update($id, array $data) {
        $customerGroup = CustomerGroupModel::find($id);
        return $customerGroup->update($data);
    }

    public function delete ($id) {
        $customerGroup = CustomerGroupModel::find($id);
        return $customerGroup->delete();
    }

    public function getByName ($cust_no) {
        return CustomerGroupModel::where('group_cd', $cust_no)->first();
    }
    
    public function getExist ($id,$group_cd) {
        return CustomerGroupModel::where('id','!=', $id)->where('group_cd', $group_cd)->first();
    }

    public function createRelation (array $data) {
        return CustomerRelationModel::create($data);
    }
    public function clearRelation ($group_cd) {
        return CustomerRelationModel::where('group_cd', $group_cd)->delete();
    }
}