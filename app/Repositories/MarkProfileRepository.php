<?php

namespace App\Repositories;

use App\MarkProfile;
use Illuminate\Support\Facades\DB;
class MarkProfileRepository
{
    public function get($id) {
        return MarkProfile::find($id);
    }

    public function create(array $data) {
        return MarkProfile::create($data);
    }

    public function batchInsert(array $data) {
        return MarkProfile::insert($data);
    }

    public function update($id, array $data) {
        $mark = MarkProfile::find($id);
        return $mark->update($data);
    }

    public function delete ($id) {
        $mark = MarkProfile::find($id);
        return $mark->delete();
    }

    public function getByName ($cust_no,$wh_no) {
        return MarkProfile::where('cust_no', $cust_no)->where('wh_no', $wh_no)->first();
    }
    public function getExist ($id,$cust_no,$wh_no) {
        return MarkProfile::where('id','!=', $id)->where('cust_no', $cust_no)->where('wh_no', $wh_no)->first();
    }
    
}