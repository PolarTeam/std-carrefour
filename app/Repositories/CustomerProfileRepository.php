<?php

namespace App\Repositories;

use App\CustomerProfileModel;
use App\CustFeeModel;
use App\CustErrorModel;
use App\CustWh;
class CustomerProfileRepository
{
    public function get($id) {
        return CustomerProfileModel::find($id);
    }
    public function checkWhDc(array $data) {
        return CustWh::where('cust_no', $data['cust_no'])
        ->where('dc_id',$data['dc_id'])
        ->where('wh_no', '!=', $data['wh_no'])
        ->first();
    }
    public function createWhrelation(array $data) {
        return CustWh::create($data);
    }
    public function clearRelationByCust ($cust) {
        return CustWh::where('cust_no', $cust)->delete();
    }

    public function create(array $data) {
        return CustomerProfileModel::create($data);
    }

    public function batchInsert(array $data) {
        return CustomerProfileModel::insert($data);
    }

    public function update($id, array $data) {
        $customerProfile = CustomerProfileModel::find($id);
        return $customerProfile->update($data);
    }

    public function delete ($id) {
        $customerProfile = CustomerProfileModel::find($id);
        return $customerProfile->delete();
    }

    public function getdetail ($id) {
        return CustFeeModel::where('cust_id', $id)->get();
    }
    public function createdetail(array $data) {
        return CustFeeModel::create($data);
    }

    public function updatedetail($id, array $data) {
        $custfee = CustFeeModel::find($id);
        return $custfee->update($data);
    }

    public function deletedetail ($id) {
        $custfee = CustFeeModel::find($id);
        return $custfee->delete();
    }

    public function getErrordetail ($id) {
        return CustErrorModel::where('cust_id', $id)->get();
    }
    public function createErrordetail(array $data) {
        return CustErrorModel::create($data);
    }

    public function updateErrordetail($id, array $data) {
        $custfee = CustErrorModel::find($id);
        return $custfee->update($data);
    }

    public function deleteErrordetail ($id) {
        $custfee = CustErrorModel::find($id);
        return $custfee->delete();
    }
    
}