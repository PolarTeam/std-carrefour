<?php

namespace App\Repositories;

use App\ModfileModel;

class FileImgRepository
{
    public function get($id) {
        return ModfileModel::find($id);
    }

    public function create(array $data) {
        return ModfileModel::create($data);
    }

    public function batchInsert(array $data) {
        return ModfileModel::insert($data);
    }

    public function update($id, array $data) {
        $orderDetail = ModfileModel::find($id);
        return $orderDetail->update($data);
    }

    public function delete ($id) {
        $orderDetail = ModfileModel::find($id);
        return $orderDetail->delete();
    }

    public function getDetail($ref_no) {
        return ModfileModel::where('ref_no',$ref_no);
    }
    
}