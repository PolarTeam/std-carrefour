<?php

namespace App\Repositories;

use App\User;
use App\PasswordHistory;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
class UserMgmtRepository
{
    public function get($id) {
        return User::find($id);
    }
    public function getBymail($email) {
        return User::where('email',$email)->first();
    }
    public function createPasswordHistory(array $data) {
        return PasswordHistory::create($data);
    }
    public function checkPasswordHistory($userId,$password) {
        return PasswordHistory::where('user_id',$userId)->orderBy('id','desc')->take(10)->get();
    }
    public function create(array $data) {
        return User::create($data);
    }

    public function batchInsert(array $data) {
        return User::insert($data);
    }

    public function update($id, array $data) {
        $user = User::find($id);
        return $user->update($data);
    }

    public function delete ($id) {
        $user = User::find($id);
        return $user->delete();
    }

    public function getByName ($email) {
        return User::where('email', $email)->first();
    }
    public function removeRole ($id) {
        DB::table('user_role')
        ->where('user_id', $id)
        ->delete();
        return;
    }

    public function getUserByUserName ($userName) {
        return User::where('username', $userName)->first();
    }
    public function getRolesNames($name) {
        return Role::orderBy('name', 'asc')->whereIn('name', $name)->pluck('display_name')->toArray();
    }
    public function getRoles() {
        return Role::orderBy('name', 'asc')->get();
    }

    public function assignRole ($id, $roleId) {
        DB::table('user_role')
        ->insert([
            'user_id'      => $id,
            'role_id'      => $roleId,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_time' => date('Y-m-d H:i:s'),
            'is_enabled'   => 1,
            'is_deleted'   => 0,
        ]);

        return;
    }
    public function getByuser() {
        return  DB::table('users')
        ->connection('mysql::write')
        ->select('id', 'username')
        ->where('is_disabled', '0')
        ->get();

    }

}