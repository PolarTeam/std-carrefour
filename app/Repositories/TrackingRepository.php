<?php

namespace App\Repositories;

use App\TrackingModel;
use Illuminate\Support\Facades\DB;
class TrackingRepository
{
    public function get($id) {
        return TrackingModel::find($id);
    }

    public function create(array $data) {
        return TrackingModel::create($data);
    }

    public function batchInsert(array $data) {
        return TrackingModel::insert($data);
    }

    public function update($id, array $data) {
        $tracking = TrackingModel::find($id);
        return $tracking->update($data);
    }

    public function delete ($id) {
        $tracking = TrackingModel::find($id);
        return $tracking->delete();
    }

    public function insertTracking($ref_no1, $ref_no2, $ref_no3, $ref_no4, $ref_no5, $ts_type, $order, $g_key, $c_key, $s_key, $d_key) {
        $now = date('Y-m-d H:i:s');
        $ordData = DB::table('mod_order')
                    ->select('ord_no')
                    ->where('sys_ord_no', $ref_no4)
                    ->first();
        
        if(isset($ordData)) {
            $ref_no2 = $ordData->ord_no;
        }
        
        $tsData = DB::table('mod_trans_status')
                    ->select('*')
                    ->where('g_key', $g_key)
                    ->where('c_key', $c_key)
                    ->where('ts_type', $ts_type)
                    ->where('order', $order)
                    ->first();
        
        if(isset($tsData)) {
            $modTsRecord = array();
            $modTsRecord['ts_no']   = $tsData->id;
            $modTsRecord['ts_type']  = $tsData->ts_type;
            $modTsRecord['ts_name']  = $tsData->ts_name;
            $modTsRecord['ts_desc']  = $tsData->ts_desc;
            $modTsRecord['sort']     = $tsData->order;
            $modTsRecord['ref_no1']  = $ref_no1;
            $modTsRecord['ref_no2']  = $ref_no2;
            $modTsRecord['ref_no3']  = $ref_no3;
            $modTsRecord['ref_no4']  = $ref_no4;
            $modTsRecord['ref_no5']  = $ref_no5;
            $modTsRecord['g_key']    = $g_key;
            $modTsRecord['c_key']    = $c_key;
            $modTsRecord['s_key']    = $s_key;
            $modTsRecord['d_key']    = $d_key;

            TrackingModel::create($modTsRecord);
            if(isset($ordData)) {
                $ordall = DB::table('mod_order')
                ->where('sys_ord_no', $ref_no4)
                ->first();
                $statunm="";
                if($ordall->status_desc=="尚未安排"){
                    $statunm = "訂單處理中";
                }
                if($ordall->status_desc=="已派單未出發"){
                    $statunm = "待出貨";
                }
                if($ordall->status_desc=="出發"){
                    $statunm = "運送中";
                }
                if($ordall->status_desc=="拒收"){
                    $statunm = "拒收";
                }
                if($ordall->status_desc=="配送發生問題"){
                    $statunm = "配送發生問題";
                }
                if($ordall->status_desc=="配送完成"){
                    $statunm = "配送完成";
                }
                if($tsData->ts_name=="待出貨"){
                    $statunm = "待出貨";
                    $ordall->status ="SEND";
                }
                if($tsData->ts_name=="運送中"){
                    $statunm = "運送中";
                    $ordall->status="SETOFF";
                }
                $custdata = DB::table('sys_customers')
                ->where('cust_no', $ordall->owner_cd)
                ->whereIn('ftptype',["E","EM","M"])
                ->count();
                if( $custdata!=0 ){
                    DB::table('mod_ts_order')
                    ->insert([
                        "owner_cd"        => $ordall->owner_cd,
                        "owner_nm"        => $ordall->owner_nm,
                        "ord_no"          => $ordall->ord_no,
                        "cust_ord_no"     => $ordall->cust_ord_no,
                        "wms_ord_no"      => $ordall->wms_order_no,
                        "sys_ord_no"      => $ordall->sys_ord_no,
                        "ord_created_at"  => $ordall->created_at,
                        "status_desc"     => $statunm,
                        "finish_date"     => $now,
                        "exp_reason"      => "",
                        "error_remark"    => "",
                        "status"          => $ordall->status,
                        "abnormal_remark" => $ordall->abnormal_remark,
                        "is_send"         => "N",
                        "updated_at"      => $now,
                        "updated_by"      => "SYSTEM",
                        "created_at"      => $now,
                        "created_by"      => "SYSTEM",
                        "g_key"           => $g_key,
                        "c_key"           => $c_key,
                        "s_key"           => $s_key,
                        "d_key"           => $d_key,
                    ]);
                }
            }
        }
        
        
        return;
    }
    
}