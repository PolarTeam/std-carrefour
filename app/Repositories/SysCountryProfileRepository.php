<?php

namespace App\Repositories;

use App\SysCountryModel;
class SysCountryProfileRepository
{
    public function get($id) {
        return SysCountryModel::find($id);
    }

    public function create(array $data) {
        return SysCountryModel::create($data);
    }

    public function batchInsert(array $data) {
        return SysCountryModel::insert($data);
    }

    public function update($id, array $data) {
        $sysCountryModel = SysCountryModel::find($id);
        return $sysCountryModel->update($data);
    }

    public function delete ($id) {
        $sysCountryModel = SysCountryModel::find($id);
        return $sysCountryModel->delete();
    }

    
}