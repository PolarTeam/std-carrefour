<?php

namespace App\Repositories;

use App\AnnounceModel;
use Illuminate\Support\Facades\DB;
class AnnounceRepository
{
    public function get($id) {
        return AnnounceModel::find($id);
    }

    public function create(array $data) {
        return AnnounceModel::create($data);
    }
    public function getForsidebar() {
        return AnnounceModel::select( 'id','wh_no',DB::raw("CONCAT(announce_subject ,'(', announce_detail,')')as content"),'announce_type' )
        ->where('announce_start_date', '<=', date('Y-m-d'))
        ->where('announce_end_date', '>=', date('Y-m-d'))
        ->where('is_enabled', 'Y')
        ->get();
    }
    public function batchInsert(array $data) {
        return AnnounceModel::insert($data);
    }

    public function update($id, array $data) {
        $announceModel = AnnounceModel::find($id);
        return $announceModel->update($data);
    }

    public function delete ($id) {
        $announceModel = AnnounceModel::find($id);
        return $announceModel->delete();
    }

    public function getByName ($name) {
        return AnnounceModel::where('name', $name)->first();
    }

    
}