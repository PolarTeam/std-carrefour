<?php

namespace App\Repositories;

use App\MarkSetting;
use Illuminate\Support\Facades\DB;
class MarksettingRepository
{
    public function get($id) {
        return MarkSetting::find($id);
    }

    public function create(array $data) {
        return MarkSetting::create($data);
    }

    public function batchInsert(array $data) {
        return MarkSetting::insert($data);
    }

    public function update($id, array $data) {
        $mark = MarkSetting::find($id);
        return $mark->update($data);
    }

    public function delete ($id) {
        $mark = MarkSetting::find($id);
        return $mark->delete();
    }

    // public function getByName ($id,$mark_name) {
    //     return MarkSetting::where('mark_name', $mark_name)->where('id', '!=', $id)->first();
    // }
    
}