<?php

namespace App\Repositories;

use App\PalletModel;
use Illuminate\Support\Facades\DB;
class PalletProfileRepository
{
    public function get($id) {
        return PalletModel::find($id);
    }

    public function create(array $data) {
        return PalletModel::create($data);
    }

    public function batchInsert(array $data) {
        return PalletModel::insert($data);
    }

    public function update($id, array $data) {
        $pallet = PalletModel::find($id);
        return $pallet->update($data);
    }

    public function delete ($id) {
        $pallet = PalletModel::find($id);
        return $pallet->delete();
    }

    public function getByName ($id,$pallet_name) {
        return PalletModel::where('pallet_name', $pallet_name)->where('id', '!=', $id)->first();
    }
    
}