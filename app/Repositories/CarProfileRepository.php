<?php

namespace App\Repositories;

use App\CarProfileModel;

class CarProfileRepository
{
    public function get($id) {
        return CarProfileModel::find($id);
    }

    public function create(array $data) {
        return CarProfileModel::create($data);
    }

    public function batchInsert(array $data) {
        return CarProfileModel::insert($data);
    }

    public function update($id, array $data) {
        $modCar = CarProfileModel::find($id);
        return $modCar->update($data);
    }

    public function delete ($id) {
        $modCar = CarProfileModel::find($id);
        return $modCar->delete();
    }

    public function getByName ($name) {
        return CarProfileModel::where('name', $name)->first();
    }

    
}