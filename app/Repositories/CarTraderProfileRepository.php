<?php

namespace App\Repositories;

use App\CarTraderModel;
use Illuminate\Support\Facades\DB;
class CarTraderProfileRepository
{
    public function get($id) {
        return CarTraderModel::find($id);
    }

    public function create(array $data) {
        return CarTraderModel::create($data);
    }

    public function batchInsert(array $data) {
        return CarTraderModel::insert($data);
    }

    public function update($id, array $data) {
        $carTrader = CarTraderModel::find($id);
        return $carTrader->update($data);
    }
    public function updatePassword($id, array $data) {
        DB::table('users')
        ->where('truck_cmp_no', $data['cust_no'])->update([
            'password'=>bcrypt($data['carpassword']),
        ]);
        return true;
    }
    public function delete ($id) {
        $carTrader = CarTraderModel::find($id);
        return $carTrader->delete();
    }

    public function getByName ($cust_no) {
        return CarTraderModel::where('cust_no', $cust_no)->first();
    }
    public function getExist ($id,$cust_no) {
        return CarTraderModel::where('id','!=', $id)->where('cust_no', $cust_no)->first();
    }
    
}