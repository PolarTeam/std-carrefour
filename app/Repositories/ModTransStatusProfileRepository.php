<?php

namespace App\Repositories;

use App\ModTransStatusModel;
use Illuminate\Support\Facades\DB;
class ModTransStatusProfileRepository
{
    public function get($id) {
        return ModTransStatusModel::find($id);
    }

    public function create(array $data) {
        return ModTransStatusModel::create($data);
    }

    public function batchInsert(array $data) {
        return ModTransStatusModel::insert($data);
    }

    public function update($id, array $data) {
        $modTransStatus= ModTransStatusModel::find($id);
        return $modTransStatus->update($data);
    }

    public function delete ($id) {
        $modTransStatus = ModTransStatusModel::find($id);
        return $modTransStatus->delete();
    }

    public function getByName ($name) {
        return ModTransStatusModel::where('name', $name)->first();
    }

    
}