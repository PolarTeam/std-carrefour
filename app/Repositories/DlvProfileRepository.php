<?php

namespace App\Repositories;

use App\DssdlvProfileModel;
use App\DssdlvProfileDetailModel;
class DlvProfileRepository
{
    public function get($id) {
        return DssdlvProfileModel::find($id);
    }
    public function getBYNo($sysDlvNo) {
        return DssdlvProfileModel::where('sys_dlv_no',$sysDlvNo)->first();
    }
    public function getBydetail($id) {
        return DssdlvProfileDetailModel::find($id);
    }
    public function getdtailByid($id) {
        $dssdlv =  DssdlvProfileModel::find($id);
        return DssdlvProfileDetailModel::where('sys_dlv_no',$dssdlv['sys_dlv_no'])->get();
    }

    public function create(array $data) {
        return DssdlvProfileModel::create($data);
    }

    public function batchInsert(array $data) {
        return DssdlvProfileModel::insert($data);
    }

    public function update($id, array $data) {
        $dssdlv = DssdlvProfileModel::find($id);
        return $dssdlv->update($data);
    }

    public function delete ($id) {
        $dssdlv = DssdlvProfileModel::find($id);
        return $dssdlv->delete();
    }

    public function getByName ($name) {
        return DssdlvProfileModel::where('name', $name)->first();
    }

    
}