<?php

namespace App\Repositories;

use App\PalletMangeModel;
use App\WareHouse;
use App\PalletMangeTotalModel;
use App\PalletMangeLogModel;
use Illuminate\Support\Facades\DB;

class PalletManageRepository
{
    public function get($id)
    {
        return PalletMangeTotalModel::find($id);
    }
    public function getTotalBy($id)
    {
        return PalletMangeTotalModel::find($id);
    }
    public function getlogdata($id)
    {
        return PalletMangeLogModel::find($id);
    }
    public function getogdata($id)
    {
        return PalletMangeModel::find($id);
    }
    public function gesumpalletforlog($id)
    {
        $ogdata =  DB::table('mod_pallet_manage_log')->where('id', $id)->first();
        $this_query = DB::table('mod_pallet_manage_log');
        $this_query->select(DB::raw(' SUM(pallet_num) as pallet_num'));
        $this_query->where('type_cd', 'warehouse');
        $this_query->where('pallet_code', $ogdata->pallet_code);
        $this_query->where('wh_no', $ogdata->wh_no);

        // if(!empty($ogdata->move_type)) {
        //     $this_query->where('move_type', $ogdata->move_type);
        // }
        if(!empty($ogdata->from_wh_cd)) {
            $this_query->where('from_wh_cd', $ogdata->from_wh_cd);
        }
        $samepalletTotal =  $this_query->value('pallet_num');

        return $samepalletTotal;
    }
    public function gesumpallet($id)
    {
        $ogdata =  DB::table('mod_pallet_manage')->where('id', $id)->first();
        if($ogdata->type_cd == "warehouse") {
            $samepalletTotal = DB::table('mod_pallet_manage')
            ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
            ->where('type_cd', 'warehouse')
            ->where('wh_no', $ogdata->wh_no)
            ->where('pallet_code', $ogdata->pallet_code)
            ->where('wh_no', $ogdata->wh_no)
            ->value('pallet_num');
        } else if($ogdata->type_cd == "driver") {
            $samepalletTotal = DB::table('mod_pallet_manage')
            ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
            ->where('type_cd', 'driver')
            ->where('owner_by', $ogdata->owner_by)
            ->where('pallet_code', $ogdata->pallet_code)
            ->where('wh_no', $ogdata->wh_no)
            ->where('ready_go','Y')
            ->value('pallet_num');
        } else {
            $samepalletTotal = DB::table('mod_pallet_manage')
            ->select(DB::raw(' SUM(pallet_num) as pallet_num'))
            ->where('type_cd', 'cust')
            ->where('cust_name', $ogdata->cust_name)
            ->where('pallet_code', $ogdata->pallet_code)
            ->where('wh_no', $ogdata->wh_no)
            ->value('pallet_num');
        }


        return $samepalletTotal;
    }
    public function gettotaldata($id)
    {
        return DB::table('mod_pallet_manage_by_driver')->where('id', $id)->first();
    }
    public function gettotaldataBycust($id)
    {
        return DB::table('mod_pallet_manage_by_cust')->where('id', $id)->first();
    }
    public function gettotaldataBycustMerge($data)
    {
        return DB::table('mod_pallet_manage_by_cust_detail_veiw')
            ->where('cust_name', $data->cust_name)
            ->where('pallet_code', $data->pallet_code)
            ->first();
    }
    public function gettotaldataByid($id)
    {
        return DB::table('mod_pallet_manage')->where('id', $id)->first();
    }
    public function create(array $data)
    {
        return PalletMangeModel::create($data);
    }
    public function updatewh($id,$newnum)
    {
        return PalletMangeModel::where('id',$id)
        ->update([
            'pallet_num' => $newnum,
        ]);
    }
    public function updatewhtozero($checkwhtowh)
    {
        return PalletMangeTotalModel::where('id',$checkwhtowh->id)
        ->update([
            'pallet_num' => 0,
        ]);
    }
    public function createlog(array $data)
    {
        return PalletMangeLogModel::create($data);
    }
    public function updatewhtowhTotal(array $data)
    {
        $totalData = PalletMangeTotalModel::where('owner_by', $data['wh_no'])
        ->where('from_wh_cd', $data['from_wh_cd'])
        ->first();
        $totalData['pallet_num'] +=  $data['pallet_num'];
        $totalData->save();
        return;
    }
    public function updateTotalNum(array $data)
    {
        $totalData = PalletMangeTotalModel::where('owner_by', $data['wh_no'])->whereNull('from_wh_cd')->first();
        $totalData['pallet_num'] +=  $data['pallet_num'];
        $totalData->save();
        return;
    }
    public function createTotal(array $data)
    {
        return PalletMangeTotalModel::create($data);
    }
    public function checkTotalexist(array $data)
    {
        return PalletMangeTotalModel::where('from_wh_cd',$data['wh_no'])
        ->where('owner_by',$data['from_wh_cd'])
        ->where('type_cd','warehouse')
        ->first();
    }
    public function checkwhtowh(array $data)
    {
        return PalletMangeTotalModel::where('owner_by',$data['from_wh_cd'])
        ->where('from_wh_cd',$data['wh_no'])
        ->where('type_cd','warehouse')
        ->first();
    }
    public function checkManage(array $data)
    {
        return PalletMangeModel::where('pallet_code',$data['pallet_code'])
        ->where('wh_no',$data['wh_no'])
        ->where('type_cd','warehouse')
        ->first();
    }
    public function getwarehouse($wh_no)
    {
        return WareHouse::where('cust_no', '!=', $wh_no)->where('cust_no', '!=', 'A0')->get();
    }
    public function batchInsert(array $data)
    {
        return PalletMangeModel::insert($data);
    }

    public function update($id, array $data)
    {
        $palletmange = PalletMangeModel::find($id);
        return $palletmange->update($data);
    }
    public function updatemanage($id, array $data)
    {
        $palletmange = PalletMangeTotalModel::find($id);
        return $palletmange->update($data);
    }

    public function delete($id)
    {
        $palletmange = PalletMangeModel::find($id);
        return $palletmange->delete();
    }

    public function getByName($id, $pallet_name)
    {
        return PalletMangeModel::where('pallet_name', $pallet_name)->where('id', '!=', $id)->first();
    }
}
