<?php

namespace App\Repositories;

use App\GoodsModel;
use Illuminate\Support\Facades\DB;
class GoodsProfileRepository
{
    public function get($id) {
        return GoodsModel::find($id);
    }

    public function create(array $data) {
        return GoodsModel::create($data);
    }

    public function batchInsert(array $data) {
        return GoodsModel::insert($data);
    }

    public function update($id, array $data) {
        $goodsModel = GoodsModel::find($id);
        return $goodsModel->update($data);
    }

    public function delete ($id) {
        $goodsModel = GoodsModel::find($id);
        return $goodsModel->delete();
    }

    public function getByName ($name) {
        return GoodsModel::where('name', $name)->first();
    }

    
}