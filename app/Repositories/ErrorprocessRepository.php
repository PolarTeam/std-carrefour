<?php

namespace App\Repositories;

use App\Errorprocess;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ErrorprocessRepository
{
    public function get($id) {
        return Errorprocess::find($id);
    }
    public function getByerrorId($id) {
        return Errorprocess::where('id',$id)->first();
    }
    public function create(array $data) {
        return Errorprocess::create($data);
    }

    public function batchInsert(array $data) {
        return Errorprocess::insert($data);
    }

    public function update($id, array $data) {
        $errorprocess = Errorprocess::find($id);
        return $errorprocess->update($data);
    }

    public function delete ($id) {
        $errorprocess = Errorprocess::find($id);
        return $errorprocess->delete();
    }

    public function getByName ($name) {
        return Errorprocess::where('name', $name)->first();
    }

    public function updatemissiondata(array $data, $ogdata) {
        $now = Carbon::now()->toDateTimeString();

        $warehousaddress = DB::table('mod_warehouse')
        ->whereNull('dc_id')
        ->pluck('address')
        ->toArray();

        $backwhcount = DB::table('mod_dlv_plan')
        ->where('wms_ord_no',$ogdata['wms_ord_no'])
        ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
        ->whereIn('cust_address', $warehousaddress)
        ->where('trs_mode','DLV')
        ->count();

        $planddata = DB::table('mod_dlv_plan')
        ->where('wms_ord_no',$ogdata['wms_ord_no'])
        ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
        ->where('trs_mode',$ogdata['trs_mode'])
        ->first();

        $backdeliverycount = DB::table('mod_dlv_plan')
        ->where('wms_ord_no',$ogdata['wms_ord_no'])
        ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
        ->where('backdelivery', 'Y')
        ->count();

        $imgcount = DB::table('mod_file')
        ->where('ref_no',$ogdata['wms_ord_no'])
        ->where('type_no','pickdlv')
        ->count();
        $status     = "FINISHED";
        $statusdesc = "任務完成";
        if($imgcount > 0) {
            $status = "CLOSED";
            $statusdesc = "已結案";
        }

        if($data['mission_status'] == "GOBACK") {

            DB::table('mod_dlv_plan')
            ->where('wms_ord_no',$ogdata['wms_ord_no'])
            ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
            ->where('trs_mode',$ogdata['trs_mode'])
            ->update([
                'lock_pallet'        => "Y",
                'twice_checkin'      => Carbon::now()->toDateTimeString(),
                'button_status'      => 'CHECKIN',
                'button_status_desc' => '回倉報到',
                'status'             => 'DLV',
                'status_desc'        => '任務已分派',
            ]);

        } else if ($data['mission_status'] == "CANCEL") {
            // a地b送
            $buttonstatus = "修改回報資料";
            if($planddata->report_pallet == "N" ) {
                $buttonstatus = "回報資料";
            }
            if( $ogdata['trs_mode'] == "pick" && ($backwhcount == 0 || $backdeliverycount >0) ) {
                DB::table('mod_dlv_plan')
                ->where('wms_ord_no',$ogdata['wms_ord_no'])
                ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => $buttonstatus,
                    'status'             => $status,
                    'status_desc'        => $statusdesc,
                    'finish_time'        => $now
                ]);
            } else {
                DB::table('mod_dlv_plan')
                ->where('wms_ord_no',$ogdata['wms_ord_no'])
                ->where('sys_dlv_no',$ogdata['sys_dlv_no'])
                ->where('trs_mode',$ogdata['trs_mode'])
                ->update([
                    'button_status'      => 'FINISHED',
                    'button_status_desc' => $buttonstatus,
                    'status'             => $status,
                    'status_desc'        => $statusdesc,
                    'finish_time'        => $now
                ]);
            }

        } else {


        }
        DB::table('mod_order')
        ->where('ord_no',$ogdata['wms_ord_no'])
        ->update([
            'status'             => 'FINISHED',
            'status_desc'        => '配送完成',
            'finish_time'        => $now
        ]);

        // return Errorprocess::create($data);
        return;
    }

}