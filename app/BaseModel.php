<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\AutoNo;
use App\AutoNoT;
use DateTime;
use Illuminate\Http\Request;
use Backpack\LangFileManager\app\Models\Language;
use Illuminate\Support\Facades\App;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Crypt;
class BaseModel extends Model
{

    public function baseGetFiled($table) {
        $query = DB::table('function_schema');
        $query->where('root_id', $table);
        $result = $query->get();

        return $result;
    }
    public function getuserbymail($email) {
        $query = DB::table('users');
        $query->where('email', $email);
        $result = $query->first();

        return $result;
    }
    public function getuserbyName($name) {
        $query = DB::table('users');
        $query->where('name', $name);
        $result = $query->first();

        return $result;
    }
    public function updateUnReadCount($userId) {
        $updateUser = DB::table('users')
        ->where('id', $userId)
        ->first();

       DB::table('users')
        ->where('id', $userId)
        ->update([
            "unReadCount" => $updateUser->unReadCount +1
        ]);

        return ;
    }
    public function getkeeper() {
        $query = DB::table('users');
        $query->where('role', 'like', '%keeper%');
        $result = $query->pluck('email')->toArray();

        return $result;
    }
    public function getuserbyid($id) {
        $query = DB::table('users');
        $query->where('id', $id);
        $result = $query->first();

        return $result;
    }
    public function getCustomerName($cd) {
        $query = DB::table('sys_customers');
        $query->where('type', 'other');
        $query->whereIn('cust_no', $cd);
        $result = $query->pluck('cname')
        ->toArray();

        return $result;
    }
    public function getCustomerGroupName($cd) {
        $query = DB::table('mod_customer_group');
        $query->whereIn('group_cd', $cd);
        $result = $query->pluck('group_name')
        ->toArray();

        return $result;
    }
    public function getWarehouseName($cd) {
        $query = DB::table('mod_warehouse');
        $query->whereIn('cust_no', $cd);
        $result = $query->pluck('cname')
        ->toArray();

        return $result;
    }
    public function getBsocdeNameByCd($cdType,$cd) {
        $query = DB::table('bscode');
        $query->where('cd_type', $cdType);
        $query->whereIn('cd', $cd);
        $result = $query->pluck('cd_descp')
        ->toArray();

        return $result;
    }

    public function getuserbyidmulti($id) {
        $query = DB::table('users');
        $query->whereIn('id', $id);
        $result = $query->pluck('email')->toArray();

        return $result;
    }

    public function getuserbyids($ids) {
        $query = DB::table('users');
        $query->whereiN('id', $ids);
        $result = $query->pluck('email')->toArray();
        return $result;
    }
    public function getFcmuserById($ids) {
        $query = DB::table('users');
        $query->whereiN('id', $ids);
        $result = $query->get();
        return $result;
    }

    public function getUserNameByids($userIds) {
        $query = DB::table('users');
        $query->whereIn('id', $userIds);
        $result = $query->pluck('name')->toArray();

        return $result;
    }
    public function getRoleNames($roleNames) {
        $query = DB::table('roles');
        $query->whereIn('name', $roleNames);
        $result = $query->pluck('display_name')->toArray();

        return $result;
    }
    public function getAlldriverByid($driverid) {
        $phones = DB::table('users')
        ->whereIn('id', $driverid)
        ->pluck('email')->toArray();

        $ids = DB::table('users')
        ->whereIn('email', $phones)
        ->pluck('id')->toArray();
        return $ids;
    }
    public function getRole($roleName) {

        $roledata = DB::table('roles')
        ->whereIn('name', $roleName)
        ->pluck('id')->toArray();

        $result = DB::table('model_has_roles')
        ->whereIn('role_id', $roledata)
        ->pluck('model_id')->toArray();

        return $result;
    }
    public function baseQuery($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $subSelect=array()) {

        // array_push($baseCondition, [
        //     'is_deleted', '=', 0
        // ]);

        // DB::listen(function ($query){
        //     Log::info($query->sql);
        //     Log::info($query->bindings);
        // });

        $query = DB::table($table);

        if ( is_array($subSelect) && count($subSelect) > 0) {
            $query->addselect('*');
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            // Log::info($baseCondition);

            foreach($baseCondition as $condition) {
                if( is_array($condition) && count($condition)) {
                    if($condition[1] === 'in') {
                        $query->whereIn($condition[0], $condition[2]);
                    }
                    else {
                        $query->where($condition[0], $condition[1], $condition[2]);
                    }
                }
            }
            
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        $result = $query->take($pageSize)->skip($startNum)->get();

        $bindings = $query->getBindings();
        $sql = str_replace('?', '%s', $query->toSql());
        $sql = sprintf($sql, ...$bindings);
        Log::info($sql);

        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query '.$table.' success',
            'data' => $result
        );
    }

    public function spcBaseQuery($query, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition) {

        // array_push($baseCondition, [
        //     'is_deleted', '=', 0
        // ]);

        // DB::listen(function ($query){
        //     Log::info($query->sql);
        //     Log::info($query->bindings);
        // });

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            // Log::info($baseCondition);

            foreach($baseCondition as $condition) {
                if($condition[1] === 'in') {
                    $query->whereIn($condition[0], $condition[2]);
                }
                else {
                    $query->where($condition[0], $condition[1], $condition[2]);
                }
            }
            
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        $result = $query->take($pageSize)->skip($startNum)->get();


        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query success',
            'data' => $result
        );
    }

    public function joinBaseQuery($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $relationship, $selectField, $subSelect=array()) {
        $query = DB::table($table);

        if (count($subSelect) > 0) {
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        if (  is_array($relationship) && count($relationship) > 0) {
            foreach($relationship as $relationData) {
                if ($relationData[1] == 'inner') {
                    $query->join($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                } else {
                    $query->leftJoin($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                }
            }
            
        }

        if ( is_array($selectField) && count($selectField) > 0) {
            foreach ($selectField as $key => $val) {
                $query->addSelect($val.'.'.$key);
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            $query->where($baseCondition);
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        try {
            $result = $query->take($pageSize)->skip($startNum)->get();
        }
        catch(\Exception $e) {
            return array(
                'success' => false,
                'totalCount' => 0,
                'message' => $e->getMessage(),
                'data' => null
            );
        }


        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query '.$table.' success',
            'data' => $result
        );
    }

    public function joinBaseQueryForExcel($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $relationship, $selectField) {
        $query = DB::table($table);
        $subSelect = array();
        if ( is_array($subSelect) && count($subSelect) > 0) {
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        if ( is_array($relationship) && count($relationship) > 0) {
            foreach($relationship as $relationData) {
                if ($relationData[1] == 'inner') {
                    $query->join($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                } else {
                    $query->leftJoin($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                }
            }
            
        }

        if ( is_array($selectField) && count($selectField) > 0) {
            foreach ($selectField as $key => $val) {
                if($key == 'product_code') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'product_name') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'order_nums') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'unit_price') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'detail_remark') {
                    $query->addSelect('order_detail.'.$key);
                }
                else {
                    $query->addSelect($val.'.'.$key);
                }
                
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            $query->where($baseCondition);
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        return $query->take($pageSize)->skip($startNum)->get();
    }
    public function getAutoNumber($model,$params){
        $user = Auth::user();
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'" and c_key="'.$user->c_key.'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }
    public function getAutoNumberJds($params){
        $model = new AutoNo();

        $numObj = $model->create([
            'num_format' => $params['num_format'],
            'length' => $params['length'],
        ]);
        return $numObj->auto_no;
    }
    public function getAutoNumberT($params){
        $model = new AutoNoT();

        $numObj = $model->create([
            'num_format' => $params['num_format'],
            'length' => $params['length'],
        ]);
        return $numObj->auto_no;
    }


    public function tolanguageStr($key) {
        $result = $key;
        $length = strlen($key);
        $thisWordCodeVerdeeld = array();
        for ($i=0; $i<$length; $i++) {
            if($key[$i-1] == "_" && $i >0 ) {
                $key[$i] =  strtoupper($key[$i]);
            } 
            $thisWordCodeVerdeeld[$i] = $key[$i];
        }
        
        $result = str_replace('_', '', implode('', $thisWordCodeVerdeeld));

        return $result;
    }

    public function checkCon($conId) {
        try {
            Log::info($conId);
            if (strlen($conId) != 11) {
                Log::info('櫃號檢查異常');
                return false;
            }
            $conId = strtoupper($conId);
            //子母對應表
            $alphaArray = array(
                'A' => 10, 'B' => 12, 'C' => 13, 'D' => 14, 'E' => 15, 'F' => 16, 'G' => 17, 'H' => 18, 'I' => 19, 'J' => 20,
                'K' => 21, 'L' => 23, 'M' => 24, 'N' => 25, 'O' => 26, 'P' => 27, 'Q' => 28, 'R' => 29, 'S' => 30, 'T' => 31,
                'U' => 32, 'V' => 34, 'W' => 35, 'X' => 36, 'Y' => 37, 'Z' => 38
            ); 
            //檢查碼
            $checkCode = substr($conId, -1);
            //驗證碼
            $verification = substr($conId, 0, (strlen($conId)-1));
            $logarray     = array();
            $stringArray  = str_split($verification);
            $tempvalue    = 0;
            foreach ($stringArray as $key => $value) {
                $powValue = pow(2, $key) ;
                if ($key <= 3 && isset($alphaArray[$value])) {
                    $tempvalue += ($alphaArray[$value] * $powValue);
                    array_push($logarray, ($alphaArray[$value] * $powValue));
                } elseif ($key > 3) {
                    $tempvalue += ($value * $powValue);
                    array_push($logarray, ($value * $powValue));
                }
            }
    
            $tempvalue    = ($tempvalue / 11);
            $intvalue     = (int)$tempvalue;
            $decimalvalue = $tempvalue - $intvalue;
            $decimalvalue = round($decimalvalue * 11, 0);
            if ($decimalvalue >= 10) {
                $decimalvalue = $decimalvalue - 10;
            }
            if ($decimalvalue != $checkCode) {
                Log::info('櫃號檢查異常');
                return false;
            }
            return true;
        } catch (\Throwable $th) {
            Log::info('櫃號檢查異常');
            return false;
        }
        return true;
    }

    public function getAutoNumberApi($model,$params,$ckey){
        $format = DB::select(DB::raw('SELECT value1 FROM bscode WHERE cd_type = "autono" and  cd = "'.$model.'" and c_key="'.$ckey.'"'))[0]->value1;
        foreach ($params as $key => $value) {
            $format = str_replace('{'.$key.'}',$value,$format);
        }
        Log::info('format value');
        Log::info( $format);
        $model = new AutoNoModel();
      
        $numObj = $model->create([
            'num_format' => $format
        ]);
        return $numObj->auto_no;
    }


    public function getSearchData($request,$table_name,$status=true,$columns=null,$filter=null,$input=null) {
        $user = Auth::user();
        // $logid =  DB::table('sys_search_grid')
        // ->insertGetId([
        //     'gridtable'  => $table_name,
        //     'g_key'      => $user->g_key,
        //     'c_key'      => $user->c_key,
        //     's_key'      => $user->s_key,
        //     'd_key'      => $user->d_key,
        //     'created_by' => $user->email,
        //     'updated_by' => $user->email,
        //     'created_at' => Carbon::now()->toDateTimeString(),
        // ]);
        $table_name = $table_name;
        $this_query = null;
        if(isset($user)) {
            $this_query = DB::table($table_name);
        }
        else{
            $this_query = DB::table($table_name);
        }
        $startnum = $request->pagenum * $request->pagesize;
        if($columns != null){
            $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
        }else{
            $frist_col = "";
        }
        if($table_name=="bscode_kind") {
            if(!Auth::user()->hasRole('Admin')){
                $this_query->where("adminonly", "=", "Y");
            }
        }

        if(isset($request->basecon)) {            
            $filters = explode(')*(', $request->basecon);
            if(count($filters) > 0) {
                for($i=0; $i<count($filters); $i++) {
                    $con_array = explode(";", $filters[$i]);
                    $df = $con_array[0];
                    $cf = $con_array[1];
                    $vf = $con_array[2];

                    switch($cf) {
                        case "EQUAL":
                            $cf = "=";
                            break;
                        case "NOT_EQUAL":
                            $cf = "<>";
                            break;
                        case "IN":
                            $cf = "in";
                            break;
                        case "NULL":
                            $cf = "NULL";
                            break;
                        case "NOT_NULL":
                            $cf = "NOT_NULL";
                            break;
                        default:
                            $cf = "=";
                            break;
                    }
                    if( $cf == "in") {
                        $this_query->whereIn($df, json_decode($vf));
                    } else if($cf == "NULL" ) {
                        $this_query->whereNull($df);
                    } else if($cf == "NOT_NULL" ) {
                        $this_query->whereNotNull($df);
                    } else {
                        $this_query->where($df, $cf, $vf);
                    }
                }
            }
            
        }
        
        //特別處理條件
        if($table_name == "mod_driver_checkin") {
            $this_query = DB::table('mod_driver_checkin_view');
        }
        
        if($table_name == "mod_car_trader") {
            $this_query = DB::table('mod_car_trader_view');
        }
        if($table_name == "mod_warehouse_for_look") {
            $this_query = DB::table('mod_warehouse');

            $checkWarehouse = explode(',',$user->check_wh);

            $userwh = DB::table('mod_warehouse')
            ->whereIn('cust_no', $checkWarehouse)
            ->pluck('dc_id')
            ->toArray();
            $this_query->whereIn("cust_no", $userwh);
        }
        // if($table_name == "sys_customers") {
        //     $this_query = DB::table('sys_customers_view');
        // }
        if($table_name == "mod_order" || $table_name == "mod_order_close") {
            //只可查看自己底下的貨主
            if($user->check_owner != "*" ) {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
            }
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                // $dcid =  substr($fordetailrow,0,2);
                $newdcid[] = $fordetailrow;
            }
            $this_query->where(function ($query) use ($user) {
                $checkWarehouse = explode(',',$user->check_wh);
                $forcheckuser = DB::table('mod_warehouse')
                ->whereIn('cust_no',  $checkWarehouse)
                ->get();
                foreach ($forcheckuser as $key => $forordrrow) {
                    $forordrrow->cname = mb_substr($forordrrow->cname ,0,4);
                    if($forordrrow->cname == "台北統倉" || $forordrrow->cname == "台北雀巢" ) {
                        $forordrrow->cname ="台北倉";
                    }
                    $query->orWhere('from_wh',  'like', '%'.$forordrrow->cname.'%');
                    $query->orWhere('dc_id',  'like', '%'.$forordrrow->dc_id.'%');
                }
            });

            // $newdcid[] = NULL;
            // $this_query->whereIn("dc_id", $newdcid);
           
        }
        if($table_name == "pallet_total_by_wh_view") {
            $this_query->where("pallet_num", '>=','0');
        }
        if($table_name == "mod_dss_dlv") {
            $userOwnerGroup = explode(',',$user->check_owner);
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            $checkdlv = array();
            if($user->check_owner != "*" ) {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $checkdlv = DB::table('mod_dss_dlv_detail')
                ->whereIn("owner_cd", $checkOwner)
                ->whereIn('dc_id', $newdcid)
                ->groupBy('dlv_no')
                ->pluck('dlv_no')
                ->toArray();
            } else {
                $checkdlv = DB::table('mod_dss_dlv_detail')
                ->whereIn('dc_id', $newdcid)
                ->groupBy('dlv_no')
                ->pluck('dlv_no')
                ->toArray();
            }

            //只可查看自己底下的倉庫
            $this_query->whereIn("dlv_no", $checkdlv);
        }

        if($table_name == "mod_dss_dlv_detail") {
            // Log::info('mod_dss_dlv_detail enter this');
            // Log::info($user->id);
            // Log::info($user->check_owner);
            if($user->check_owner != "*" ) {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
            }
            // 只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }

            $this_query->whereIn("dc_id", $newdcid);
        }
        if($table_name == "mod_error_report") {
            if($user->check_owner != "*" ) {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
            }
        }
        if($table_name == "mod_driver_checkin") {
            // if($user->check_owner != "*" ) {
            //     $userOwnerGroup = explode(',',$user->check_owner);
            //     $checkOwner = DB::table('mod_customer_relation')
            //     ->whereIn('group_cd', $userOwnerGroup)
            //     ->pluck('cust_no')->toArray();

            //     $this_query->whereIn("owner_cd", $checkOwner);
            // }
            //只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            $this_query->whereIn("dc_id", $newdcid);
        }

        // if($table_name == "mod_dlv_plan") {
        //     if($user->check_owner != "*" ) {
        //         $userOwnerGroup = explode(',',$user->check_owner);
        //         $checkOwner = DB::table('mod_customer_relation')
        //         ->whereIn('group_cd', $userOwnerGroup)
        //         ->pluck('cust_no')->toArray();

        //         $this_query->whereIn("owner_cd", $checkOwner);
        //     }
        //     $checkWarehouse = explode(',',$user->check_wh);
        //     $newdcid = array();
        //     foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
        //         $newdcid[] = substr($fordetailrow,0,2);
        //     }
        //     //只可查看自己底下的倉庫

        //     $this_query->whereIn("dc_id", $newdcid);
        // }

        if($table_name == "mod_users_view") {
            //司機不顯示
            $this_query->where("web_user", 'Y');
        }

        if($table_name == "mod_mark_setting") {
            //只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            $this_query->whereIn("warehouse_id", $checkWarehouse);
        }

        if($table_name == "mod_mark_reserve") {
            //只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            $this_query->whereIn("dc_id", $newdcid);
            $this_query->where("onsite", 'N');
        }

        if($table_name == "mod_error_report") {
            //只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            $this_query->whereIn("dc_id", $newdcid);
        }

        if($table_name == "mod_error_report") {
            if($user->check_owner !="*") {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();
                $this_query->whereIn("owner_cd", $checkOwner);
            }
        }

        if($table_name == "mod_pallet_manage_total") {
            //只可查看自己底下的倉庫
            $checkWarehouse = explode(',',$user->check_wh);
            $this_query->whereIn("owner_by", $checkWarehouse);
        }
        //特別處理條件end 

        // if($user->identity == 'G') {
        //     $this_query->whereRaw("(g_key='".$user->g_key."')");
        // }
        // else if($user->identity == 'C') {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."') or g_key='*')");
        // }
        // else if($user->identity == 'S') {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."') or g_key='*')");
        // }
        // else {
        //     $this_query->whereRaw("((g_key='".$user->g_key."' and c_key='".$user->c_key."' and s_key='".$user->s_key."' and d_key='".$user->d_key."') or g_key='*')");
        // }


        if(isset($request->dbRaw)) {
            $this_query->whereRaw(Crypt::decrypt($request->dbRaw));
        }

        if(isset($request->defOrder)) {
            $order_array = explode(";", $request->defOrder);
            $df = $order_array[0];
            $cf = $order_array[1];

            $this_query->orderBy($df, $cf);
        }

        $neddconditionTable = ['mod_order','mod_dlv_plan','mod_dss_dlv','mod_order_close']; 
        $neddcondition = "N";
        
        if (in_array($table_name, $neddconditionTable)) {
            $neddcondition = "Y";
        } else {
            $neddcondition = "N";
        }

         if (isset($request->filterscount)) {
             $filterscount = $request->filterscount;
            //  dd(isset($request->basecon));
            if($neddcondition == "Y" && !isset($request->dbRaw)){
                if ($filterscount > 0) {
                    $where_type = true; // true means AND, false means OR
                    $tmpdatafield = "";
                    $tmpfilteroperator = "";
                    $valuesPrep = "";
                    $values = [];
                    $date_format = "";
                for ($i = 0; $i < $filterscount; $i++) {
                   // get the filter's value.
                   $filtervalue = $request["filtervalue" . $i];
                   // get the filter's condition.
                   $filtercondition = $request["filtercondition" . $i];
                   // get the filter's column.
                   $filterdatafield = $request["filterdatafield" . $i];
                   // get the filter's operator.
                   $filteroperator = $request["filteroperator" . $i];
                   if ($tmpdatafield == "")
                   {
                   $tmpdatafield = $filterdatafield;
                   }
                   else if ($tmpdatafield <> $filterdatafield)
                   {
                       $where_type = true;
                   }
                   else if ($tmpdatafield == $filterdatafield)
                   {
                   if ($tmpfilteroperator == 0)
                   {
                       $where_type = true;
                   }
                   else $where_type = false;
                   }
                    // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    if(!is_array($multiValue)) {
                        $multiValue = array();
                    }
                   switch ($filtercondition)
                   {
                   case "CONTAINS":
                       $condition = " LIKE ";
                       if(is_array($multiValue) && count($multiValue) > 1) {
                           for($k=0;$k<count($multiValue);$k++) {
                               if($k == 0) {
                                   $value = "%{$multiValue[$k]}%;";
                               }
                               else {
                                   $value .= "%{$multiValue[$k]}%;";
                               }
                               
                           }
                           $value = rtrim($value,";");
                       }
                       else {
                           $value = "%{$multiValue[0]}%";
                       }
                       
                       break;
                    case "CONTAINSLIKE":
                        $condition = "like_in";
                        $value = $filtervalue;
                        break;
                   case "DOES_NOT_CONTAIN":
                       $condition = " NOT LIKE ";
                       $value = "%{$filtervalue}%";
                       break;
                   case "EQUAL":
                       $condition = " = ";
                       $value = $filtervalue;
                       break;
                   case "NOT_EQUAL":
                       $condition = "not_in";
                       $value = $filtervalue;
                       break;
                    case "IN":
                        $condition = "in";
                        $value = $filtervalue;
                        break;
                   case "GREATER_THAN":
                       $condition = " > ";
                       $value = $filtervalue;
                       break;
                   case "LESS_THAN":
                       $condition = " < ";
                       $value = $filtervalue;
                       break;
                   case "GREATER_THAN_OR_EQUAL":
                       $condition = " >= ";
                       $value = $filtervalue;
                       break;
                   case "LESS_THAN_OR_EQUAL":
                       $condition = " <= ";
                       $value = $filtervalue;
                       break;
                   case "STARTS_WITH":
                       $condition = " LIKE ";
                       $value = "{$filtervalue}%";
                       break;
                   case "ENDS_WITH":
                       $condition = " LIKE ";
                       $value = "%{$filtervalue}";
                       break;
                   case "NULL":
                       $condition = " IS NULL ";
                       $value = "%{$filtervalue}%";
                       break;
                   case "NOT_NULL":
                       $condition = " IS NOT NULL ";
                       $value = "%{$filtervalue}%";
                       break;
                   }

                   if($this->validateDateTime($value)){
                       switch($filterdatafield){
                           case "created_at":
                           case "updated_at":
                               //$value =  strtotime($value);
                               $date_format = "timestamp";
                           default:
                               $date_format = "datetime";
                           break;
                       }
                   }elseif($this->validateDateTime($value)){
                       $date_format = "date";
                   }
      
                   try{
                    //    dd($filterscount."/".($filterdatafield=="status").$condition."/"."/".$value);
                       if($filterscount==1 &&$filterdatafield=="status" &&$value=="%FINISHED%"){
                        $this_query->where("id","=", "null");
                       }
                       if($where_type){
                           if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                               // $d = Carbon::parse($value)->format('Y-m-d');
                               if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                   $this_query->whereDate($filterdatafield,trim($condition), $value);
                               }else{
                                   $this_query->where($filterdatafield,trim($condition), $value);
                                   // dd($filterdatafield."/".trim($condition)."/".$value);
                               }
                           }else{
                               $multiValue = explode(';', $value);

                               if(count($multiValue) > 1) {
                                   $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                       for($k=0;$k<count($multiValue); $k++) {
                                           $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                       }
                                   });
                               }
                               else {
                                   //$this_query->where($filterdatafield,  trim($condition), $value);
                                   if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else if(trim($condition) == "like_in") {
                                        $this_query->Where(function($query) use ($filterdatafield, $value)
                                        {
                                            $multiValues = explode(',', $value);
                                            foreach ($multiValues as $key => $multiValue) {
                                                $query->orwhere($filterdatafield,"like","%".$multiValue."%");
                                            }
                                        });
                                    } else if(trim($condition) == "in") {
                                        $this_query->whereIn($filterdatafield, explode(',', $multiValue[0]));
                                    }  else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                               }
                               
                           }
                           
                       }else{
                           $this_query->orWhere($filterdatafield, trim($condition), $value);
                       }
                           $tmpfilteroperator = $filteroperator;
                           $tmpdatafield = $filterdatafield;
                       
                   }catch(\Exception $ex){
                       return $ex;
                   }
               }
           }else{
                Log::info('before finish search 123');
                
                $data[] = array(
                    'TotalRows' => 0,
                    'Rows' => array(),
                    'StatusCount' => 0
                );
                return $data;
           }
            }else{

             if ($filterscount > 0)
                 {
                 $where_type = true; // true means AND, false means OR
                 $tmpdatafield = "";
                 $tmpfilteroperator = "";
                 $valuesPrep = "";
                 $values = [];
                 $date_format = "";
                 for ($i = 0; $i < $filterscount; $i++)
                {
                    // get the filter's value.
                    $filtervalue = $request["filtervalue" . $i];
                    // get the filter's condition.
                    $filtercondition = $request["filtercondition" . $i];
                    // get the filter's column.
                    $filterdatafield = $request["filterdatafield" . $i];
                    // get the filter's operator.
                    $filteroperator = $request["filteroperator" . $i];
                    if ($tmpdatafield == "")
                    {
                    $tmpdatafield = $filterdatafield;
                    }
                    else if ($tmpdatafield <> $filterdatafield)
                    {
                        $where_type = true;
                    }
                    else if ($tmpdatafield == $filterdatafield)
                    {
                    if ($tmpfilteroperator == 0)
                    {
                        $where_type = true;
                    }
                    else $where_type = false;
                    }
                     // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    switch ($filtercondition)
                    {
                    case "CONTAINS":
                        $condition = " LIKE ";
                        if( is_array($multiValue) && count($multiValue ) > 1) {
                            for($k=0;$k<count($multiValue);$k++) {
                                if($k == 0) {
                                    $value = "%{$multiValue[$k]}%;";
                                }
                                else {
                                    $value .= "%{$multiValue[$k]}%;";
                                }
                                
                            }
                            $value = rtrim($value,";");
                        }
                        else {
                            $value = "%{$multiValue[0]}%";
                        }
                        
                        break;
                    case "CONTAINSLIKE":
                        $condition = "like_in";
                        $value = $filtervalue;
                        break;
                    case "DOES_NOT_CONTAIN":
                        $condition = " NOT LIKE ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "EQUAL":
                        $condition = " = ";
                        $value = $filtervalue;
                        break;
                    case "NOT_EQUAL":
                        $condition = "not_in";
                        $value = $filtervalue;
                        break;
                    case "IN":
                        $condition = "in";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN":
                        $condition = " > ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN":
                        $condition = " < ";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $condition = " >= ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $condition = " <= ";
                        $value = $filtervalue;
                        break;
                    case "STARTS_WITH":
                        $condition = " LIKE ";
                        $value = "{$filtervalue}%";
                        break;
                    case "ENDS_WITH":
                        $condition = " LIKE ";
                        $value = "%{$filtervalue}";
                        break;
                    case "NULL":
                        $condition = " IS NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "NOT_NULL":
                        $condition = " IS NOT NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    }

                    if($this->validateDateTime($value)){
                        switch($filterdatafield){
                            case "created_at":
                            case "updated_at":
                                //$value =  strtotime($value);
                                $date_format = "timestamp";
                            default:
                                $date_format = "datetime";
                            break;
                        }
                    }elseif($this->validateDateTime($value)){
                        $date_format = "date";
                    }
       
                    try{
                        if($where_type){
                            if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                // $d = Carbon::parse($value)->format('Y-m-d');
                                if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                    $this_query->whereDate($filterdatafield,trim($condition), $value);
                                }else{
                                    $this_query->where($filterdatafield,trim($condition), $value);
                                    // dd($filterdatafield."/".trim($condition)."/".$value);
                                }
                            }else{
                                $multiValue = explode(';', $value);

                                if(count($multiValue) > 1) {
                                    $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                        for($k=0;$k<count($multiValue); $k++) {
                                            $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                        }
                                    });
                                }
                                else {
                                    //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else if(trim($condition) == "like_in") {
                                        $this_query->Where(function($query) use ($filterdatafield, $value)
                                        {
                                            $multiValues = explode(',', $value);
                                            foreach ($multiValues as $key => $multiValue) {
                                                $query->orwhere($filterdatafield,"like","%".$multiValue."%");
                                            }
                                        });
                                    } else if(trim($condition) == "in") {
                                        $this_query->whereIn($filterdatafield, explode(',', $multiValue[0]));
                                    }  else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                                }
                                
                            }
                            
                        }else{
                            $this_query->orWhere($filterdatafield, trim($condition), $value);
                        }
                            $tmpfilteroperator = $filteroperator;
                            $tmpdatafield = $filterdatafield;
                        
                    }catch(\Exception $ex){
                        return $ex;
                    }
                }
            }
            }
        }
        if($filter!=null){
            $this_query->whereRaw($filter);
        }

        if($input!=null && $columns!=null){
            $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
        }
        $count_query = clone $this_query;

        if($status){
            $status_count = $count_query->select(DB::raw("count(*) as count, status, '' AS statustext"))
            ->groupBy('status')
            ->orderBy('status', 'ASC' )
            ->get();
            foreach ($status_count as $sc){
                $sc->statustext = trans($this->dashesToCamelCase($table_name).'.STATUS_'.$sc->status);
            }
        }else{
            $status_count = 0;
        }

        if(isset($request->sortdatafield) && isset($request->sortorder)){
            $sortfield = $request->sortdatafield;
            $sortorder = $request->sortorder;
            $this_query->orderBy($sortfield,$sortorder);
        }
       
        if($input==null){
            $total_rows = $this_query->count();
        }
     
        if($columns==null){
            $rows = $this_query->take($request->pagesize)->skip($startnum)->get();
        }else{
            if($input!=null){
                $rows = $this_query->select(DB::raw($frist_col." AS label,".$frist_col." AS value".",".$columns))->take(10)->get();
                $data = $rows;
                return $data;
                
            }else{
                $rows = $this_query->select(DB::raw($columns))->take($request->pagesize)->skip($startnum)->get();//
            }
           
        }
        try {
            Log::info('FOR INDEX');
            $bindings = $this_query->getBindings();
            $sql = str_replace('?', '%s', $this_query->toSql());
            $sql = sprintf($sql, ...$bindings);
            Log::info($sql);
            Log::info('FOR INDEX END');
        } catch (\Throwable $th) {
            //throw $th;
        }

        
        // DB::table('sys_search_grid')
        // ->where('id', $logid)
        // ->update([
        //     'sqlstr'     => $sql,
        //     'updated_at' => Carbon::now()->toDateTimeString()
        // ]);

        Log::info('finish search grid final');
        $data[] = array(
            'TotalRows' => $total_rows,
            'Rows' => $rows,
            'StatusCount' => $status_count
        );
        return $data;
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return $ex;
        }
       
    }

    public function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return $ex;
        }
       
    }

    public function getColumnInfo($table_name,$columns=null,$languagename=null,$lang=null){
        $user = Auth::user();
        $cols_sql = "";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }
        // text
        // session(['locale' => $lang]);
        // app()->setLocale($lang);
        if($table_name == "mod_warehouse_for_look") {
            $table_name = 'mod_warehouse';
        }
        $values_source = "";
        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $this_type = explode("(", $value->Type)[0];
            $filtertype = "input";
            $cellsformat = "";
            $width = filter_var($value->Type, FILTER_SANITIZE_NUMBER_INT);
            $width = $width==""?100:$width;
            $db_width = $width;
            $cellsalign = "left";
            $statusCode = array();


            $trsMode = array();
            if($value->Field == 'trs_mode') {
                $trsData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'TM')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                
                if(count($trsData) > 0) {
                    foreach($trsData as $key=>$row) {
                        $trsMode[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $carType = array();
            if($value->Field == 'car_type') {
                $baseData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'CARTYPE')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                if(count($baseData) > 0) {
                    foreach($baseData as $key=>$row) {
                        $carType[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $dlvType = array();
            if($value->Field == 'dlv_type') {
                $dlvType[0] = array (
                    'label' => '提貨(P)',
                    'value' => 'P'
                );

                $dlvType[1] = array (
                    'label' => '配送(D)',
                    'value' => 'D'
                );
            }
            switch($this_type){
                case "timestamp":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "date":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "datetime":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "decimal":
                case "int":
                case "float":
                    $this_type = "number";
                    $filtertype = "number";
                    $cellsalign = "right";
                    $width*=3;
                    break;
                case "text":
                case "varchar":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                break;
                case "enum":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                    $values_source = array('source' =>  $value->Field."GridObj",
                                        'value' => "val", 
                                        'name'  => "key");
                break;
                default:
                    $width*=3;
                break;
            }

            if($width < 100){
                $width = 100;
            }

           
            $fields_info[] = array(
                'name' => $value->Field,
                'type' => $this_type
            );
         
            // $columns_model[] = array(
            //     'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
            //     'datafield'   => $value->Field,
            //     'width'       => $width,
            //     'dbwidth'     => $db_width,
            //     'nullable'    => $value->Null == "YES"?true: false,
            //     'cellsformat' => $cellsformat,
            //     'filtertype'  => $filtertype,
            //     'cellsalign'  => $cellsalign,
            //     'values'      => $values_source
            // );
            
            if($value->Field == "status") {
                $columns_model[] = array(
                    'text'        => trans($table_name.'.'.$value->Field),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'statusCode'  => $statusCode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "trs_mode") {
                $columns_model[] = array(
                    'text'        => trans($table_name.'.'.$value->Field),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'trsMode'     => $trsMode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "car_type") {
                $columns_model[] = array(
                    'text'        => trans($table_name.'.'.$value->Field),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'carType'     => $carType,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "dlv_type") {
                $columns_model[] = array(
                    'text'        => trans($table_name.'.'.$value->Field),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'dlvType'     => $dlvType,
                    'filterdelay' => 99999999
                );
            }
            else {
                $columns_model[] = array(
                    'text'        => trans($languagename.'.'.$value->Field),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'filterdelay' => 99999999
                );
            }

            $values_source = "";
           // dd($value->Type);
           //echo "'" . $value->Field . "' => '" . $value->Type . "|" . ( $value->Null == "NO" ? 'required' : '' ) ."', <br/>" ;
        }

        /*
        
            { text: 'id', datafield: 'id' , width: 250 },
                { text: 'name', datafield: 'name' , width: 250 },
                { text: 'email', datafield: 'email' , width: 250 },
                { text: 'password', datafield: 'password' , width: 250 },
                { text: 'Group', datafield: 'g_key' , width: 250 },
                { text: 'Company', datafield: 'c_key' , width: 250 },
                { text: 'Station', datafield: 's_key' , width: 250 },
                { text: 'Derpartment', datafield: 'd_key' , width: 250 },
                { text: 'remember_token', datafield: 'remember_token' , width: 250 },
                { text: 'created_at',filtertype: 'date', datafield: 'created_at' , width: 250, cellsformat: 'yyyy-MM-dd' },
                { text: 'updated_at',filtertype: 'date', datafield: 'updated_at' , width: 250 },
        
        */
        $data[0] = $fields_info;
        $data[1] = $columns_model;

        return $data;
    }

    public function getColumnSpec($table_name,$columns=null){
        $cols_sql = "";
        $spec_text = "return [<br />";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }

        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $spec_text.= '"'.$this->dashesToCamelCase($value->Field).'"        =>      "'.trans(''.$this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)).'",<br />';
        }
        $spec_text.="];";

        return $spec_text;
    }

    public function saveLayout($key,$data,$lang,$table,$user){

        try{
            $now = date("Y-m-d H:i:s");
            DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', $lang)->delete();

            $id = DB::table('sys_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'lang'       => $lang,
                'table_name' => $table,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                'd_key'      => $user->d_key,
                's_key'      => $user->s_key,
                'created_at' => $now,
                'updated_at' => $now,
                'created_by' => $user->email,
                'updated_by' => $user->email
            ]);
            return "true";
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function getLayout($key,$lang="zh-TW"){
        $user = Auth::user();
        try{
            $layout = array();
            $checklayout = DB::table('sys_layout')
            ->where('key', '=', $key)
            ->where('created_by', $user->email)
            ->where('lang', '=', $lang)
            ->first();
            if(isset($checklayout)) {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', $lang)
                ->pluck('data');
            } else {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', 'standardlayout@standard-info.com')
                ->where('lang', '=', $lang)
                ->pluck('data');
            }
            $newLayout = json_decode($layout[0]);
            $languagename = 'zh-TW';
            // if($languagename != null) {
            //     foreach ($newLayout->columns as $columnKey => $columnData) {
            //         if( $columnKey !='_checkboxcolumn') {
            //             $newcolumnName = $this->tolanguageStr($columnKey);
            //             App::setLocale($lang);
            //             $columnData->text = trans($languagename.'.'.$newcolumnName);
            //         }
            //         # code...
            //     }
            //     return  json_encode($newLayout);
            // }

            return $layout[0];
        }catch(\Exception $ex){
            return "false";
        }
        
    }
    function dashesToCamelCase($string, $capitalizeFirstCharacter = false) 
    {
    
        $str = str_replace('_', '', ucwords($string, '_'));
    
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
    
        return $str;
    }


    public static function getPossbileStatuses($tableName, $colName){
        $type = DB::select(DB::raw('SHOW COLUMNS FROM '.$tableName.' WHERE Field = "'.$colName.'"'))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $values = array();
        foreach(explode(',', $matches[1]) as $value){
            $values[] = trim($value, "'");
        }
        return $values;
    }


    function get_enum_values( $type ) {
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    function getStatusCount($tableName, $langname=null) {
        $user = Auth::user();
        $status_count = DB::table($tableName)->select(DB::raw("count(*) as count, status, '' AS statustext"))
        ->groupBy('status')
        ->orderBy('status', 'ASC' )
        ->get();

        $this_query = DB::table($tableName);
        //特別處理條件
        if($tableName == "mod_order") {
            //只可查看自己底下的貨主
            $status_query = DB::table($tableName);
            $status_query->select(DB::raw("count(*) as count, status, '' AS statustext"));
            if($user->check_owner !="*") {
                $userOwnerGroup = explode(',',$user->check_owner);
                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
                $status_query->whereIn("owner_cd", $checkOwner);
            }
            $this_query->where(function ($query) use ($user) {
                $checkWarehouse = explode(',',$user->check_wh);
                $forcheckuser = DB::table('mod_warehouse')
                ->whereIn('cust_no',  $checkWarehouse)
                ->get();
                foreach ($forcheckuser as $key => $forordrrow) {
                    $forordrrow->cname = mb_substr($forordrrow->cname ,0,4);
                    $query->orWhere('from_wh',  'like', '%'.$forordrrow->cname.'%');
                    $query->orWhere('dc_id',  'like', '%'.$forordrrow->dc_id.'%');
                }
            });
            $status_query->where(function ($query) use ($user) {
                $checkWarehouse = explode(',',$user->check_wh);
                $forcheckuser = DB::table('mod_warehouse')
                ->whereIn('cust_no',  $checkWarehouse)
                ->get();
                
                foreach ($forcheckuser as $key => $forordrrow) {
                    $forordrrow->cname = mb_substr($forordrrow->cname ,0,4);
                    $query->orWhere('from_wh',  'like', '%'.$forordrrow->cname.'%');
                    $query->orWhere('dc_id',  'like', '%'.$forordrrow->dc_id.'%');
                }
            });
            $status_query->groupBy('status');
            $status_query->orderBy(DB::raw("FIELD( status,'PICKEDFINISH', 'LOADING', 'UPLOADFINISH', 'OFFSTAFF','FINISHED')"));
            $status_count = $status_query->get();
        }
        if($tableName == "mod_dlv_plan") {
            if($user->check_owner !="*") {
                $userOwnerGroup = explode(',',$user->check_owner);

                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
                $status_query->whereIn("owner_cd", $checkOwner);
            }
            $checkWarehouse = explode(',',$user->check_wh);
            $status_query->whereIn("dc_id", $checkWarehouse);
            $this_query->whereIn("dc_id", $checkWarehouse);
        }
        if($tableName == "mod_dss_dlv") {
            if($user->check_owner !="*") {
                $userOwnerGroup = explode(',',$user->check_owner);

                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
                $status_query->whereIn("owner_cd", $checkOwner);
            }
            $checkWarehouse = explode(',',$user->check_wh);
            $this_query->whereIn("dc_id", $checkWarehouse);
        }
        if($tableName == "mod_error_report") {
            if($user->check_owner !="*") {
                $userOwnerGroup = explode(',',$user->check_owner);

                $checkOwner = DB::table('mod_customer_relation')
                ->whereIn('group_cd', $userOwnerGroup)
                ->pluck('cust_no')->toArray();

                $this_query->whereIn("owner_cd", $checkOwner);
            }
            $checkWarehouse = explode(',',$user->check_wh);
            $newdcid = array();
            foreach ($checkWarehouse as $fordetailkey => $fordetailrow) {
                $newdcid[] = substr($fordetailrow,0,2);
            }
            $this_query->whereIn("dc_id", $newdcid);
            $status_count = DB::table($tableName)->select(DB::raw("count(*) as count, status, '' AS statustext"))
            ->whereIn("dc_id", $newdcid)
            ->groupBy('status')
            ->orderBy('status', 'ASC' )
            ->get();    
        }

        foreach ($status_count as $sc){
            $sc->statustext = trans($tableName.'.STATUS'.$sc->status);
        }
        //特別處理條件end 

        $data[] = array(
            'TotalRows'   =>$this_query->count(),
            'StatusCount' => $status_count
        );

        return $data;
    }

}