<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsModel extends Model
{
    protected $table = 'mod_goods';
    protected $fillable = [
        'id',
        'goods_no',
        'goods_nm',
        'goods_no2',
        'goods_nm2',
        'gw',
        'gwu',
        'cbm',
        'cbmu',
        'g_height',
        'g_width',
        'g_length',
        'created_by',
        'updated_by',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_at',
        'updated_at',
        'pkg_unit',
        'owner_nm',
        'owner_cd',
        'price',
        'goods_type_nm',
        'goods_type_cd',
        'sn_flag',
    ];

}