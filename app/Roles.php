<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'id',
        'name',
        'display_name',
        'guard_name',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}