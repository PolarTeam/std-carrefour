<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'languagepath' => [
            'driver' => 'local',
            'root' => '/var/www/ddstms/html/resources/lang/',
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'dpex' => [
            'driver' => 'local',
            'root' => storage_path('app/public/dpex'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'dpexftp' => [
            'driver' => 'ftp',
            'host' => env('IMPORT_DPEX_FTP_HOST'),
            'port' => env('IMPORT_DPEX_FTP_PORT'),
            'username' => env('IMPORT_DPEX_FTP_ID'),
            'password' => env('IMPORT_DPEX_FTP_PWD')
        ],

        'ecan' => [
            'driver' => 'local',
            'root' => storage_path('app/public/ecan_upload_data'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'ecanftp' => [
            'driver'   => 'ftp',
            'host'     => 'p2ap3.e-can.com.tw',
            'port'     => 21,
            'username' => 'webedi',
            'password' => '0922430089'
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],

    ],

];
