<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    //'middleware' => ['web', 'admin'],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
// Route::post('login', 
    Route::get('example/getData', 'ExampleController@getData');
    Route::get('example/getFieldsJson', 'ExampleController@getFieldsJson');
    Route::get('baseApi/getGridJson/{table}/{status?}', 'BaseApiController@getGridJson');
    Route::get('baseApi/getGridJsonForMongo/{table}/{status?}', 'BaseApiController@getGridJsonForMongo');
    Route::get('baseApi/getStatusCount/{tableName}', 'BaseApiController@getStatusCount');
    Route::get('baseApi/getFieldsJson/{table}', 'BaseApiController@getFieldsJson');
    Route::get('baseApi/getLookupGridJson/{info1}/{info2}/{info3}', 'BaseApiController@getLookupGridJson');
    Route::get('baseApi/getLookupFieldsJson/{info1}/{info2}', 'BaseApiController@getLookupFieldsJson');
    Route::get('baseApi/getAutocompleteJson/{info1}/{info2}/{info3}/{input}', 'BaseApiController@getAutocompleteJson');
    Route::get('baseApi/getAutoNumber/{model}', 'BaseApiController@getAutoNumber');
    
    Route::get('baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
    Route::post('baseApi/saveLayoutJson', 'BaseApiController@saveLayoutJson');
    Route::post('baseApi/clearLayout', 'BaseApiController@clearLayout');

    //貨況相關
    Route::get('trackingApi/get/{ord_no?}', 'TrackingApiController@getTrackingByOrder');
    Route::get('tracking/get/{ord_no?}', 'TrackingApiController@getTsRecord');
    

    Route::get('dlvApi/getDlvData', 'DlvCrudController@getDlvData');

    Route::get('getOrderDetail/{ord_no?}', 'OrderMgmtCrudController@getOrderDetail');
    Route::post('uploadChk', 'OrderMgmtCrudController@orderUpload');
    Route::post('sendMessage', 'FcmController@sendMessage');
    Route::post('export/data', 'CommonController@exportData');
    
});

Route::group([    
    'middleware' => ['auth:api','cors']
], function () {
    Route::get('show/{ord_no?}', 'OrderMgmtCrudController@showChkImg');
    //app api
    Route::get('dlvData/get/{car_no?}', 'DlvCrudController@getDlvDataByCarNo');
    //send logout
    Route::post('logout', 'UserController@logout'); // (APP)

    //-------------------------------------
    //修改使用者資訊
    Route::post('/std/refreshpass', 'Api\DataController@stdrefreshpass');
    //抓取Dlv 主檔 配送中
    Route::post('std/user/getDlvData','Api\DataController@stdgetDlvData' );
    //抓取Dlv 細檔 配送中NEW
    Route::post('/std/user/getDlvDetailDataNew','Api\DataController@stdgetDlvDetailData_NEW' );
    //send gps
    Route::post('/std/sendGpsData','Api\DataController@stdsendGpsData' );
    //std找取訂單明細new(含照片)
    Route::get('/std/stdordDetail/get/{sysOrdNo?}/{type?}/','Api\DataController@stdordDetail');
    //查詢任務
    Route::get('/std/missonfind/get/{sysOrdNo?}','Api\DataController@stdmissonfind' );


    // 異常回報
    Route::post('/std/error/reportnew','Api\DataController@stderrorReport_JSON' );
    // 出發_new
    Route::post('/std/departurenew/{dlvNo?}','Api\DataController@stddeparturenew' );
    //send 提貨資料
    Route::post('/std/sendpickorder','Api\DataController@stdsendpickorder' );
    //查詢任務
    Route::get('/std/getorderinfo','Api\DataController@stdgetorderinfo' );
    //上傳照片
    Route::post('/std/uploadSignImg', 'Api\DataController@stduploadSignImg');
    //上傳異常照片
    Route::post('/std/uploadAbnormalImg', 'Api\DataController@stduploadAbnormalImg');
    //更新序號new
    Route::post('/std/updateSnNo_new', 'Api\DataController@stdupdateSnNo_new');
    //刪除照片
    Route::post('/std/delPhoto', 'Api\DataController@delPhoto');
    //接單
    Route::post('/std/user/checkdlvorder','Api\DataController@stdcheckdlvorder' );
    //小車司機刷訂單派車
    Route::post('/std/user/dlvorder','Api\DataController@stddlvorder' );
    Route::post('/std/reorder', 'Api\DataController@stdreorderDlvPlan');
    //-------------------------------------

    //抓取Dlv 主檔 配送中
    Route::post('/user/getDlvData','Api\DataController@getDlvData' );
    //抓取Dlv 細檔 配送中
    Route::post('/user/getDlvDetailData','Api\DataController@getDlvDetailData' );
    //抓取Dlv 細檔 配送中NEW
    Route::post('/user/getDlvDetailDataNew','Api\DataController@getDlvDetailData_NEW' );
    //找取訂單明細
    Route::get('/ordDetail/get/{sysOrdNo?}/{type?}','Api\DataController@getOrdDetail' );
    //找取訂單明細_new
    Route::get('/ordDetail_new/get/{sysOrdNo?}/{type?}/{dlv_no?}','Api\DataController@getOrdDetail_new' );
    


    //查詢任務
    Route::get('/missonfind/get/{sysOrdNo?}','Api\DataController@missonfind' );
    //查詢任務
    Route::get('/getorderinfo','Api\DataController@getorderinfo' );

    // // 異常回報
    // Route::post('/error/report','Api\DataController@errorReport' );
    // // 異常回報
    // Route::post('/error/reportnew','Api\DataController@errorReport_JSON' );
    // 版本檢查
    Route::post('/user/appcheck','Api\DataController@checkversion' );
    // 出發
    Route::post('/departure/{dlvNo?}','Api\DataController@departure' );
    // 出發_new
    Route::post('/departurenew/{dlvNo?}','Api\DataController@departurenew' );
    
    //抓取異常原因下拉
    Route::get('getErrorData','Api\DataController@getErrorData' );
    //抓取代收款下拉
    Route::get('getExtraData','Api\DataController@getExtraData' );

    //抓取異常原因下拉
    Route::get('getErrorData_new','Api\DataController@getErrorData_new' );
    //抓取代收款下拉
    Route::get('getExtraData_new','Api\DataController@getExtraData_new' );

    //抓取異常原因下拉
    Route::post('getErrorData_new','Api\DataController@getErrorData_new' );
    //抓取代收款下拉
    Route::post('getExtraData_new','Api\DataController@getExtraData_new' );


    //抓取系統公告
    Route::get('/user/getNoticeData','Api\DataController@getNoticeData' );
    //系統公告設為已讀
    Route::post('/user/updateNoticeData','Api\DataController@updateNoticeData' );
    //send gps
    Route::post('/sendGpsData','Api\DataController@sendGpsData' );
    Route::post('/trackingadd','Api\DataController@trackingadd' );
    //send 提貨資料
    Route::post('/sendpickorder','Api\DataController@sendpickorder' );
    //修改使𡪤者資訊
    Route::post('/user/update', 'Api\DataController@modifyUser');
    //修改使𡪤者資訊
    Route::post('/refreshpass', 'Api\DataController@refreshpass');
    //取得user資訊
    Route::post('/user/getData','Api\DataController@getUserData' );
    //取得聊天列表
    Route::get('/chatroom/get', 'Api\DataController@getChatroomList');
    //Tracking開關
    Route::post('/tracking/switch/{switch?}', 'Api\DataController@trackingSwitch');
    //Tracking開關
    Route::get('/tracking/get/status', 'Api\DataController@getTrackingStatus');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/user/dToken', 'Api\DataController@updateDtoken');

    Route::get('/notice/count/get', 'Api\DataController@getNoticeCnt');
    Route::get('/notice/read', 'Api\DataController@setNoticeRead');
    Route::post('/reorder', 'Api\DataController@reorderDlvPlan');
    Route::post('uploadSignImg', 'Api\DataController@uploadSignImg');
    Route::post('uploadAbnormalImg', 'Api\DataController@uploadAbnormalImg');
    Route::post('updateSnNo', 'Api\DataController@updateSnNo');

    Route::post('updateSnNo_new', 'Api\DataController@updateSnNo_new');
    Route::post('delPhoto', 'Api\DataController@delPhoto');

    //小車司機刷訂單派車檢查
    Route::post('/user/checkdlvorder','Api\DataController@checkdlvorder' );
    //小車司機刷訂單派車
    Route::post('/user/dlvorder','Api\DataController@dlvorder' );
    //派單接單權限
    Route::get('/user/menupersion','Api\DataController@menupersion' );

    //get order img
    Route::post('/user/get/order/img','Api\DataController@getorderimg');
    //delete order img
    Route::post('/user/del/order/img','Api\DataController@delorderimg');

    //百及
    Route::get('/getpallet','Api\DataController@getpallet' ); //取得棧板建檔
    Route::post('/orderpallet','Api\DataController@orderpallet' );//訂單棧板新增

    Route::post('/checkdlvpalletdlvNo','Api\DataController@checkdlvpalletdlvNo' );//倉管確認派車棧板數量
    Route::post('/checkdlvpallet','Api\DataController@checkdlvpallet' );//倉管確認派車棧板數量
    Route::post('/checkdlvpalletBywh','Api\DataController@checkdlvpalletBywh' );//倉管確認派車棧板數量 by 倉庫
    
    Route::post('/confirmdlvpallet','Api\DataController@confirmdlvpallet' );//倉管放行 新增棧板
    Route::post('/confirmdlvpallet_android','Api\DataController@confirmdlvpallet' );//倉管放行 新增棧板
    Route::post('/driverunloading','Api\DataController@driverunloading' );//司機下貨完成
    Route::post('/recycledriverpallet','Api\DataController@recycledriverpallet' );//消棧板
    Route::post('/checkdriverpallet','Api\DataController@checkdriverpallet' );//檢查棧板

    Route::post('/tranferrecycledriverpallet','Api\DataController@tranferrecycledriverpallet' );//轉運消棧板
    Route::post('/belongdriverpallet','Api\DataController@belongdriverpallet' );//倉管確認派車棧板數量(qrcode 進來的)
    Route::get('/getpalletBydlvno','Api\DataController@getpalletBydlvno' );//取得司機棧版by車次編號
    Route::get('/getpalletBydriver','Api\DataController@getpalletBydriver' );//取得所有司機棧板

    Route::post('/deleteimg','Api\DataController@delimg' );//刪除照片
    Route::post('/errorreport','Api\DataController@errorreport' );//異常圖片上傳
    Route::post('/errorremark','Api\DataController@errorremark' );//異常備註
    Route::post('/geterrorbyid','Api\DataController@geterrorbyid' );//取得異常明細byid

    Route::post('/upstairs','Api\DataController@upstairs' );//上樓費
    Route::post('/downpoint','Api\DataController@downpoint' );//下點費

    Route::post('/signreport','Api\DataController@signreport' );//一般上傳
    Route::post('/deleveryimgupload','Api\DataController@deleveryimgupload' );//一般上傳

    Route::get('/getannounce','Api\DataController@getannounce' ); //取得公告
    Route::get('/geterrorreport','Api\DataController@geterrorreport' ); //取得棧板建檔
    Route::post('/getwarehouse','Api\DataController@getwarehouse' ); //取得倉庫建檔
    Route::post('/getwarehouseBywh','Api\DataController@getwarehouseBywh' ); //取得倉庫建檔by倉庫
    Route::post('/whscandriver','Api\DataController@whscandriver' ); //倉管掃描司機

    Route::post('/checkin','Api\DataController@checkin' ); //報到
    Route::post('/beforecheckin','Api\DataController@beforecheckin' ); //報到
    Route::post('/uploading','Api\DataController@uploading' ); //開始上貨
    Route::post('/pickfinish','Api\DataController@pickfinish' ); //提貨完成(逆物流)

    Route::post('/arrivercheck','Api\DataController@arrivercheck' ); //到點檢查
    Route::post('/driverarriver','Api\DataController@driverarriver' ); //到點
    Route::post('/starttakeoff','Api\DataController@starttakeoff' ); //開始下貨
    Route::post('/checklackpallet','Api\DataController@checklackpallet' ); //所欠棧板
    Route::get('/getcartrader','Api\DataController@getcartrader' ); //取得車商
    Route::get('/getowner','Api\DataController@getowner' ); //取得貨主
    Route::get('/palletprocess','Api\DataController@palletprocess' ); //訂單棧板處理
    Route::post('/checkpalletbydlvno','Api\DataController@checkpalletbydlvno' ); //派車單號棧板明細

    Route::post('/deliverydlvnobywh','Api\DataController@deliverydlvnobywh' ); //可放行派車單號by倉庫
    
    Route::post('/checkfinishaddress','Api\DataController@checkfinishaddress' ); //同地點完成的資料


    Route::get('/getdrivermission','Api\DataController@getdrivermission' ); //司機任務
    Route::post('/getorderdetail','Api\DataController@getorderdetail' ); //訂單明細

    Route::post('/missiondetail','Api\DataController@missiondetail' ); //任務明細
    Route::post('/missiondetailById','Api\DataController@missiondetailById' ); //任務明細
    Route::post('/updatemissionsort','Api\DataController@updatemissionsort' ); //更新排序
    // Route::post('/getreportpallet','Api\DataController@getreportpallet' ); //司機取得可回報棧板

    Route::post('/get/markreserveinfo','Api\DataController@markreserveinfo' ); //碼頭預約相關資料取得
    Route::post('/markreserve','Api\DataController@markreserve' ); //碼頭預約
    Route::post('/updatemarkreserve','Api\DataController@updatemarkreserve' ); //更新碼頭預約
    Route::post('/markreservesearch','Api\DataController@markreservesearch' ); //碼頭預約搜尋
    Route::post('/markusestatus','Api\DataController@markusestatus' ); //碼頭預約狀態

    Route::get('/get/assigncheckincar','Api\DataController@assigncheckincar' ); //分派報到車輛
    Route::post('/assigncheckincardetail','Api\DataController@assigncheckincardetail' ); //報到明細
    Route::post('/assigncheckincarwh','Api\DataController@assigncheckincarwh' ); //指派碼頭

    Route::get('/get/importdischarged','Api\DataController@importdischarged' ); //進貨放行list
    Route::post('/importdischargedbyid','Api\DataController@importdischargedbyid' ); //進貨放行更新
    Route::post('/updateimportdischarged','Api\DataController@updateimportdischarged' ); //進貨放行更新

    
    Route::post('/additionalfee','Api\DataController@additionalfee' ); //上樓 下點費
    Route::post('/updateadditionalfee','Api\DataController@updateadditionalfee' ); //更新上樓下點費
    Route::post('/reverselogistics','Api\DataController@reverselogistics' ); // 逆物流
    Route::post('app/crash/log', 'AppCrashLogController@store');

    Route::get('/messagelist','Api\DataController@messagelist' ); //訊息清單
    Route::post('replaymessage', 'Api\DataController@replaymessage'); //app回復訊息

    Route::post('userByPhone', 'Api\DataController@userByPhone'); //取得
    Route::post('checkinInfo', 'Api\DataController@checkinInfo'); //取得

    Route::post('remindWhFcm', 'Api\DataController@remindWhFcm'); //發送給倉管人員推播

    Route::post('getdetailbydlvno', 'Api\DataController@getdetailbydlvno'); //取得接單明細
    Route::post('handover', 'Api\DataController@handover'); //接單

    Route::post('updateUserRead', 'Api\DataController@updateUserRead'); //更新閱讀時間
    Route::get('meessagelist', 'Api\DataController@meessagelist'); //即時訊息
    Route::post('dssontime', 'Api\DataController@dssontime'); //準點回報
    
    
});

Route::get('/updateTime/get', function (Request $request) {
    return response()->json(['msg' => 'success', 'time1' => 1000, 'time2' => 60000]);
});

Route::get('/version/get', function (Request $request) {
    return response()->json(['msg' => 'success', 'version' => '1.0.6']);
});
//benq
Route::post('/fcmtest','Api\DataController@fcmtest' ); //fcm測試
Route::post('/getdetailtest','Api\DataController@getdetailtest' ); //訂單明細測試
Route::middleware('cors')->post('/user/benqxmlget','Api\DataController@benqxmlget');
Route::middleware('cors')->post('/user/benqxmlReverseget','Api\DataController@benqxmlunget');
Route::middleware('cors')->post('/user/login','Api\LoginController@login');
Route::post('trackingApi/sendGps', 'TrackingApiController@testGpsData');
Route::get('/out/ordDetail/get/{sysOrdNo?}/{type?}','Api\DataController@getOrdDetail' );
Route::post('checkCon', 'Api\DataController@checkCon');


Route::post('/geteditoken','Api\DataController@geteditoken' );
Route::post('/utmsorder','Api\DataController@ediblno' );

Route::post('/getorderdata','Api\DataController@getorderdata' ); //
Route::post('/apiOrderDlv','Api\DataController@apiOrderDlv' );
Route::get('/ordertodlv','Api\DataController@ordertodlv' );
Route::post('/apiOrderDlv','Api\DataController@apiOrderDlv' );


Route::get('/gettestOrder','Api\DataController@gettestOrder' );

Route::post('/testcustimport','Api\DataController@testcustimport' );
Route::get('/tempdsstonormal','Api\DataController@tempdsstonormal' );

Route::post('/getOrderfromlogistics','Api\DataController@getOrderfromlogistics' );

//mi
// https://core.standard-info.com/api/miorder/

// 版本檢查
Route::post('/std/user/appcheck','Api\DataController@stdcheckversion' );

Route::get('/check', function(){
    return response()->json(["msg" => "success"]);
});

Route::get('/getsql/status','Api\DataController@getsqlstatus' );


    
