<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// https://ddstms.standard-info.com/baseApi/getFieldsJson/mod_order?key=dlv
Route::get('login', 'Auth\LoginController@loginView')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('admin/baseApi/getGridJson/{table}/{status?}', 'BaseApiController@getGridJson');
Route::get('admin/baseApi/getStatusCount/{tableName}/{langname?}', 'BaseApiController@getStatusCount');
Route::get('admin/baseApi/getFieldsJson/{table}', 'BaseApiController@getFieldsJson');
Route::get('admin/baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
Route::get('admin/baseApi/getinboundbymy/{table}/{status?}/{userid?}', 'BaseApiController@getinboundbymy');
Route::get('admin/baseApi/getinboundbymyall/{table}/{status?}/{userid?}', 'BaseApiController@getinboundbymyall');

Route::get('admin/baseApi/getLookupFieldsJson/{info1}/{info2}/{info5?}/{lang?}', 'BaseApiController@getLookupFieldsJson');
Route::get('admin/baseApi/getLookupGridJson/{info1}/{info2}/{info3}', 'BaseApiController@getLookupGridJson');
Route::get('admin/baseApi/getAutocompleteJson/{info1}/{info2}/{info3}/{input}', 'BaseApiController@getAutocompleteJson');

Route::get('show/public/img/{path?}', 'CommonController@showChkImg');

Route::get('admin/baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
Route::post('admin/baseApi/saveLayoutJson', 'BaseApiController@saveLayoutJson');
Route::post('admin/baseApi/clearLayout', 'BaseApiController@clearLayout');

Route::get('searchList/get/{key}', 'CommonController@getSearchLayoutList');
Route::get('searchHtml/get/{id?}', 'CommonController@getSearchHtml');
Route::post('userPwd/update', 'CommonController@updatePwd');
Route::post('userlang/change', 'CommonController@changelang');
Route::put('setSearchDefault/{key}/{id}', 'CommonController@setSearchDefault');
Route::DELETE('delSearchLayout/{id}', 'CommonController@delSearchLayout');
Route::post('delSearchLayout/{id}', 'CommonController@delSearchLayout');
Route::post('getownerlookup', 'CommonController@getownerlookup');

Route::post('webcheckin', 'CheckinProfileController@store');
Route::post('getfinalcheckin', 'CheckinProfileController@getfinalcheckin');

Route::get('getAmtReportByMonth', 'DashboardController@getAmtReportByMonth');
Route::get('getDriver', 'DashboardController@getDriver');
Route::get('getAmtReportByDay', 'DashboardController@getAmtReportByDay');
Route::get('getOrderReportByMonth', 'DashboardController@getOrderReportByMonth');
Route::get('getOrderReportByDay', 'DashboardController@getOrderReportByDay');
Route::get('orderExcelSample/download', 'ExcelImportController@getOrderExcel');
Route::get('getDlvPlan/{dlvNo?}', 'DlvCrudController@getDlvPlan');

Route::get('DriverSigIn/{dcid?}', 'DashboardController@DriverSigIn');
Route::post('DriverSigIn/{dcid?}', 'DashboardController@DriverSigIn');
Route::get('security', 'DashboardController@security');
Route::get('wharfStatus', 'DashboardController@wharfStatus');
Route::get('wharfStatus2', 'DashboardController@wharfStatus2');//測試用 等移除
Route::post('get/notwokday', 'WarehouseProfileController@notwokday');
Route::post('get/reservedata', 'WarehouseProfileController@reservedata');

Route::get('show/{ord_no?}/{type?}', 'OrderMgmtController@showChkImg');
Route::post('get/finalcardata', 'DashboardController@finalcardata');
Route::post('carleave', 'DashboardController@carleave');
Route::get('usereditshow/{id}', 'UserMgmtController@usereditshow');
Route::post('userupdatepassword', 'UserMgmtController@expireupdate');
Route::post('checkexpire', 'UserMgmtController@checkexpire');


Route::group( ['middleware' => ['auth', 'locale'], 'prefix' => '{lang?}' ], function () {

    Route::get('/', function () {
        return redirect('/dashboardChartWh');
    });
    Route::post('public/announceProfile', 'AnnounceProfileController@getannounce');
    Route::post('saveSearchLayout', 'CommonController@saveSearchLayout');
    Route::get('/board', 'DashboardController@dashboard');
    Route::get('/map', 'DashboardController@map');
    Route::get('/dashboardChartWh', 'DashboardController@dashboardChartWh');
    Route::post('/dashboardChartWhbydate', 'DashboardController@dashboardChartWhbydate');
    Route::get('/dashboardChartWhbydateview/{date}', 'DashboardController@dashboardChartWhbydateview');
    Route::get('/dashboardChartOwner', 'DashboardController@dashboardChartOwner');
    Route::get('/dashboardChartOwnerfortest', 'DashboardController@dashboardChartOwnerfortest');
    Route::post('/dashboardChartOwnerbydate', 'DashboardController@dashboardChartOwnerbydate');
    Route::get('/dashboardText', 'DashboardController@dashboardText');
    Route::get('/dashboardText2', 'DashboardController@dashboardText2');
    Route::post('/dashboardTextbydate', 'DashboardController@dashboardTextbydate');

    Route::get('carPath/get', 'TrackingApiController@getCarPath');
    Route::get('car/get/{car_no?}', 'TrackingApiController@getCar');
    Route::get('car/getcar', 'TrackingApiController@initgetcar');
    Route::get('testPath', 'TrackingApiController@testPath');
    
    //使用者管理
    Route::resource('user', 'UserMgmtController');
    
    Route::get('custuser', 'UserMgmtController@custuser');
    Route::post('user/batchDelete', 'UserMgmtController@batchDelete');
    Route::post('export/data', 'UserMgmtController@exportdata');
    Route::get('permission/user', 'UserMgmtController@getpermissionUser');
    Route::post('user/query', 'UserMgmtController@query');
    Route::post('user/queryemail', 'UserMgmtController@queryemail');
    Route::post('user/queryemailall', 'UserMgmtController@queryemailall');
    Route::post('user/excel/export', 'UserMgmtController@exportdata');
    Route::post('user/set/dashboard/read/list', 'UserMgmtController@setDashboardReadList');

    //角色
    Route::resource('role', 'RolesController');
    Route::post('role/batchDelete', 'RolesController@batchDelete');
    Route::post('role/excel/export', 'RolesController@exportdata');

    //倉庫建檔
    Route::resource('warehouseProfile', 'WarehouseProfileController');
    Route::post('warehouseProfile/batchDelete', 'WarehouseProfileController@batchDelete');
    Route::delete('warehouseProfile/batchDelete/{id}', 'WarehouseProfileController@destroy');
    Route::post('warehouseProfile/excel/export', 'WarehouseProfileController@exportdata');
    Route::post('warehousedetail/store', 'WarehouseProfileController@detailStore');//倉庫明細
    Route::post('warehousedetail/update', 'WarehouseProfileController@detailUpdate');//倉庫明細
    Route::get('warehousedetail/delete/{id}', 'WarehouseProfileController@detailDel');//倉庫明細
    Route::get('warehousedetail/get/{warehouse_id?}', 'WarehouseProfileController@detailGet');//倉庫明細


    Route::post('warehousenotwork/store', 'WarehouseProfileController@notworkStore');//倉庫不預約
    Route::post('warehousenotwork/update', 'WarehouseProfileController@notworkUpdate');//倉庫不預約
    Route::get('warehousenotwork/delete/{id}', 'WarehouseProfileController@notworkDel');//倉庫不預約
    Route::get('warehousenotwork/get/{warehouse_id?}', 'WarehouseProfileController@notworkGet');//倉庫不預約

    //訊息夾
    Route::resource('message', 'MessageController');
    Route::post('message/batchDelete', 'MessageController@batchDelete');
    Route::post('message/excel/export', 'MessageController@exportdata');
    Route::post('message/replay', 'MessageController@messagereplay');

    //訊息建檔
    Route::resource('basemessage', 'BasemessageController');
    Route::post('basemessage/batchDelete', 'BasemessageController@batchDelete');
    Route::post('basemessage/excel/export', 'BasemessageController@exportdata');
    Route::post('basemessage/batchSendMessage', 'BasemessageController@batchSendMessage');
    
    //碼頭設定
    Route::resource('marksetting', 'MarksettingController');
    Route::post('marksetting/batchDelete', 'MarksettingController@batchDelete');
    Route::post('marksetting/excel/export', 'MarksettingController@exportdata');

    Route::resource('wharfReserve', 'WharfReserveController');
    Route::post('wharfReserve/batchDelete', 'WharfReserveController@batchDelete');
    Route::post('wharfReserve/excel/export', 'WharfReserveController@exportdata');
    Route::get('truckcarTraderProfile', 'WharfReserveController@indexfortruck');

    // 系統推播
    Route::resource('sysmessage', 'SysMessageController');
    Route::post('sysmessage/batchDelete', 'SysMessageController@batchDelete');
    Route::post('sysmessage/excel/export', 'SysMessageController@exportdata');

    // 推播紀錄
    Route::resource('fcm', 'FcmController');
    Route::post('fcm/batchDelete', 'FcmController@batchDelete');
    Route::post('fcm/excel/export', 'FcmController@exportdata');

    //碳排計算
    Route::resource('carboncal', 'CarboncalController');
    Route::post('carboncal/batchDelete', 'CarboncalController@batchDelete');
    Route::post('carboncal/excel/export', 'CarboncalController@exportdata');

    

    //棧板建檔
    Route::resource('palletProfile', 'PalletProfileController');
    Route::post('palletProfile/batchDelete', 'PalletProfileController@batchDelete');
    Route::post('palletProfile/excel/export', 'PalletProfileController@exportdata');

    //棧板管理(倉庫)
    Route::resource('palletManageWarehouse', 'PalletManageController');
    
    Route::post('palletManageWarehouselog', 'PalletManageController@createlog');
    Route::post('palletManageWarehouseToWarehouse', 'PalletManageController@createwhtowh');
    Route::get('palletManageWarehouseeditdetail', 'PalletManageController@editdetail');//手動調整
    Route::get('palletManageWarehouseeditwh', 'PalletManageController@editwh');//倉對倉調整
    Route::get('palletManageWarehouse/showmanage/{id}', 'PalletManageController@showmanage');//倉庫明細
    Route::post('palletManageWarehouse/excel/export', 'PalletManageController@exportdata');
    Route::get('palletManageWarehousedetailedit/{id}', 'PalletManageController@detailedit');
    Route::get('palletManageWarehousetotaledit/{id}', 'PalletManageController@totaledit');
    Route::get('palletWarehouselog/{id}', 'PalletManageController@showtotal');
    Route::get('palletWarehousedetail/{id}', 'PalletManageController@showmanage');
    Route::put('palletWarehousedetail/{id}', 'PalletManageController@update');
    Route::put('palletManageWarehouseManage/{id}', 'PalletManageController@updatemanage');//倉庫明細
    // palletManageWarehouseManage

    
    //棧板管理(司機)
    Route::resource('palletManageDriver', 'PalletManageDriverController');
    Route::post('palletManageDriver/excel/export', 'PalletManageDriverController@exportdata');
    Route::get('palletManageDriverdetailedit/{id}', 'PalletManageDriverController@detailedit');
    Route::get('palletManageDriverdetaildetail/{id}', 'PalletManageDriverController@showmanage');

    //棧板管理(客戶)
    Route::resource('palletManageCust', 'PalletManageCustController');
    Route::post('palletManageCust/excel/export', 'PalletManageCustController@exportdata');
    Route::get('palletManageCustdetailedit/{id}', 'PalletManageCustController@detailedit');
    Route::get('palletManageCustdetaildetail/{id}', 'PalletManageCustController@showmanage');
    Route::get('palletManageCustdetail/{id}', 'PalletManageCustController@showmanage');
    
    //碼頭建檔
    Route::resource('markProfile', 'MarkProfileController');
    Route::post('markProfile/batchDelete', 'MarkProfileController@batchDelete');
    Route::post('markProfile/excel/export', 'MarkProfileController@exportdata');

    //地址正規化 addressformate
    Route::resource('addressformate', 'AddressformateController');
    Route::post('addressformate/batchDelete', 'AddressformateController@batchDelete');
    Route::post('addressformate/insertdetail', 'AddressformateController@insertdetail');
    Route::post('addressformate/excel/export', 'AddressformateController@exportdata');
    Route::get('addressformateDetail/{error_type?}', 'AddressformateController@detailget');
    Route::post('addressformateDetail/store', 'AddressformateController@detailStore');
    Route::post('addressformateDetail/update/{error_type}', 'AddressformateController@detailUpdate');
    Route::get('addressformateDetail/delete/{id}', 'AddressformateController@detailDel');


    //車商建檔
    Route::resource('carTraderProfile', 'CarTraderProfileController');
    Route::post('carTraderProfile/batchDelete', 'CarTraderProfileController@batchDelete');
    Route::post('carTraderProfile/excel/export', 'CarTraderProfileController@exportdata');
    
    //客戶建檔
    Route::resource('customerProfile', 'CustomerProfileController');
    Route::post('customerProfile/batchDelete', 'CustomerProfileController@batchDelete');
    Route::post('customerProfile/excel/export', 'CustomerProfileController@exportdata');
    Route::post('customerdetailProfile/store', 'CustomerProfileController@feeStore');//服務項目
    Route::post('customerdetailProfile/update/{cust_id}', 'CustomerProfileController@feeUpdate');//服務項目
    Route::get('customerdetailProfile/delete/{id}', 'CustomerProfileController@feeDel');//服務項目
    Route::get('customerdetailProfile/get/{cust_id?}', 'CustomerProfileController@feeGet');//服務項目
    Route::post('customererrorProfile/store', 'CustomerProfileController@errorStore');//異常項目
    Route::post('customererrorProfile/update/{cust_id}', 'CustomerProfileController@errorUpdate');//異常項目
    Route::get('customererrorProfile/delete/{id}', 'CustomerProfileController@errorDel');//異常項目
    Route::get('customererrorProfile/get/{cust_id?}', 'CustomerProfileController@errorGet');//異常項目

    //客戶群組建檔
    Route::resource('customerGroup', 'CustomerGroupProfileController');
    Route::post('customerGroup/batchDelete', 'CustomerGroupProfileController@batchDelete');
    Route::post('customerGroup/excel/export', 'CustomerGroupProfileController@exportdata');

    //商品建檔
    Route::resource('goodsProfile', 'GoodsProfileController');
    Route::post('goodsProfile/batchDelete', 'GoodsProfileController@batchDelete');
    Route::post('goodsProfile/excel/export', 'GoodsProfileController@exportdata');

    //區域建檔
    Route::resource('sysArea', 'SysAreaProfileController');
    Route::post('sysArea/batchDelete', 'SysAreaProfileController@batchDelete');
    Route::post('sysArea/excel/export', 'SysAreaProfileController@exportdata');

    //異常建檔
    Route::resource('abnormal', 'AbnormalController');
    Route::post('abnormal/batchDelete', 'AbnormalController@batchDelete');
    Route::post('abnormal/excel/export', 'AbnormalController@exportdata');
    Route::get('abnormaldetail/{cd_type?}', 'AbnormalController@detailget');
    Route::post('abnormaldetail/store', 'AbnormalController@detailStore');
    Route::post('abnormaldetail/update/{cd_type}', 'AbnormalController@detailUpdate');
    Route::get('abnormaldetail/delete/{id}', 'AbnormalController@detailDel');

    //公告欄
    Route::resource('announceProfile', 'AnnounceProfileController');
    Route::post('announceProfile/batchDelete', 'AnnounceProfileController@batchDelete');
    Route::post('announceProfile/excel/export', 'AnnounceProfileController@exportdata');


    //公告欄
    Route::resource('errorprocess', 'ErrorprocessController');
    Route::post('errorprocess/batchDelete', 'ErrorprocessController@batchDelete');
    Route::post('errorprocess/excel/export', 'ErrorprocessController@exportdata');
    Route::post('errorprocessfcm/excel/export', 'ErrorprocessController@exportfcmdata');
    Route::post('errorprocess/sendMessage', 'ErrorprocessController@sendMessage');
    Route::get('openerror/{error_id?}', 'ErrorprocessController@openerror');
    Route::get('errorprocessinit/{ordno?}', 'OrderMgmtController@errorprocessinit');//圖片


    //司機報到
    Route::resource('checkin', 'CheckinProfileController');
    Route::post('checkin/batchDelete', 'CheckinProfileController@batchDelete');
    Route::post('checkin/excel/export', 'CheckinProfileController@exportdata');
    Route::post('deliverylog/excel/export', 'CheckinProfileController@deliverylogexportdata');
    Route::get('checkin/imgget/{ref_no?}', 'CheckinProfileController@imgGet');//圖片
    Route::get('deliverylog', 'CheckinProfileController@deliverylog');
    Route::post('checkin/manualleave', 'CheckinProfileController@manualleave');

    
    //貨況建檔
    Route::resource('modTransStatus', 'ModTransStatusProfileController');
    Route::post('modTransStatus/batchDelete', 'ModTransStatusProfileController@batchDelete');
    Route::post('modTransStatus/excel/export', 'ModTransStatusProfileController@exportdata');

    //common
    Route::get('getCarType', 'CommonController@getCarType');

    //派車資訊
    Route::resource('dlvCarnew', 'DlvCrudController');
    Route::post('batchDelete', 'DlvCrudController@batchDelete');
    Route::post('excel/export', 'DlvCrudController@exportdata');
    Route::get('dlvPlanSearch', 'DlvCrudController@DlvPlanSearch');   //運輸計劃
    Route::get('modDlvPlan/{id}/edit', 'DlvCrudController@dlvplanedit');
    Route::post('sendCar/getPack', 'DlvCrudController@getPack');
    Route::get('carLoad/get/{dlv_no?}', 'DlvCrudController@getCarLoad');
    Route::post('sendCar/handPickup', 'DlvCrudController@handPickup');
    Route::post('sendCar/autoPickup', 'DlvCrudController@autoPickup');
    Route::post('insertToDlv', 'DlvCrudController@insertToDlv');
    Route::post('sendToApp', 'DlvCrudController@sendToApp');
    Route::post('error/changecar', 'DlvCrudController@errorchangecar');
    Route::get('dlvCar', 'DlvCrudController@DlvCar');
    Route::get('getcar', 'DlvCrudController@getcar');
    Route::get('GetOrd', 'DlvCrudController@GetOrd');

    Route::get('sendCar', 'DlvCrudController@sendCar');   //運輸計劃
    Route::get('sendCar/{id}/edit', 'DlvCrudController@sendCaredit');   //運輸計劃
    

    Route::post('sendDlvCar', 'DlvCrudController@sendDlvCar');
    Route::post('autosend', 'DlvCrudController@autosend');
    
    Route::post('sendormsg', 'DlvCrudController@sendappmsg');
    Route::get('getStantiondata', 'DlvCrudController@getStantiondata');
    Route::get('getMessagedata', 'DlvCrudController@getMessagedata');
    Route::get('getDlvEtaPlan/{dlvNo?}', 'DlvCrudController@getDlvEtaPlan');
    Route::get('getDlvsortPlan/{dlvNo?}', 'DlvCrudController@getDlvsortPlan');
    Route::get('getDlvsortPlan2/{dlvNo?}/{type?}/{sorttype?}', 'DlvCrudController@getDlvsortPlan2');
    Route::get('getDlvsortPlan3/{dlvNo?}', 'DlvCrudController@getDlvsortPlan3');
    Route::post('upDlvPlansort', 'DlvCrudController@upDlvPlansort');
    Route::post('customupDlvPlansort', 'DlvCrudController@customupDlvPlansort');
    Route::post('autosendorder', 'DlvCrudController@autosendorder');
    Route::post('rmDlvPlan', 'DlvCrudController@rmDlvPlan');
    Route::post('delDlvPlan', 'DlvCrudController@delDlvPlan');


    //集團建檔
    Route::resource('companyProfile', 'CompanyProfileController');
    Route::post('companyProfile/batchDelete', 'CompanyProfileController@batchDelete');
    Route::post('companyProfile/excel/export', 'CompanyProfileController@exportdata');

    
    //集團建檔
    Route::resource('mappingexcel', 'MappingexcelController');
    Route::post('mappingexcel/multi/del', 'MappingexcelController@multiDel'); 
    Route::post('mappingDetail/store', 'MappingexcelController@detailStore');
    Route::post('mappingDetail/update', 'MappingexcelController@detailUpdate');
    Route::get('mappingDetail/delete/{id}', 'MappingexcelController@detailDel');
    Route::get('mappingDetail/get/{mapping_id?}', 'MappingexcelController@detailget');
    Route::post('mappingexcel/importExcel/{custid}', 'MappingexcelController@changeExcel');

    //費用管理
    Route::resource('sysRefFeeCar', 'SysRefFeeCrudController');
    Route::get('sysRefFeeCar', 'SysRefFeeCrudController@sysRefFeeCar');
    Route::post('sysRefFeeCar/store', 'SysRefFeeCrudController@CarStore');
    Route::post('sysRefFeeCar/update', 'SysRefFeeCrudController@CarUpdate');
    Route::get('sysRefFeeCar/delete/{id}', 'SysRefFeeCrudController@CarDel');
    Route::get('sysRefFeeCar/get', 'SysRefFeeCrudController@CarGet');
    Route::post('sysRefFeeCar/importExcel', 'SysRefFeeCrudController@importCarExcel');
    Route::get('sysRefFeeCar/download', 'SysRefFeeCrudController@getCarDownload');

    Route::post('sysRefFeePiece/store', 'SysRefFeeCrudController@PieceStore');
    Route::post('sysRefFeePiece/update', 'SysRefFeeCrudController@PieceUpdate');
    Route::get('sysRefFeePiece/delete/{id}', 'SysRefFeeCrudController@PieceDel');
    Route::get('sysRefFeePiece/get', 'SysRefFeeCrudController@PieceGet');

    Route::post('sysRefFeeCbm/store', 'SysRefFeeCrudController@CbmStore');
    Route::post('sysRefFeeCbm/update', 'SysRefFeeCrudController@CbmUpdate');
    Route::get('sysRefFeeCbm/delete/{id}', 'SysRefFeeCrudController@CbmDel');
    Route::get('sysRefFeeCbm/get', 'SysRefFeeCrudController@CbmGet');

    Route::post('sysRefFeeGw/store', 'SysRefFeeCrudController@GwStore');
    Route::post('sysRefFeeGw/update', 'SysRefFeeCrudController@GwUpdate');
    Route::get('sysRefFeeGw/delete/{id}', 'SysRefFeeCrudController@GwDel');
    Route::get('sysRefFeeGw/get', 'SysRefFeeCrudController@GwGet');

    //國家建檔
    Route::resource('sysCountry', 'SysCountryProfileController');
    Route::post('sysCountry/batchDelete', 'SysCountryProfileController@batchDelete');
    Route::post('sysCountry/excel/export', 'SysCountryProfileController@exportdata');

    //layoutcopy
    Route::get('layout', 'CommonController@Layout');
    Route::post('layout/copylayout', 'CommonController@copylayout');
    Route::post('layout/updatelayout', 'CommonController@updatelayout');
    Route::post('layout/copylayoutconfirm', 'CommonController@copylayoutconfirm');
    
    //結單相關
    Route::resource('closeOrder', 'CloseOrderController');

    Route::get('closeOrder/imgget/{ref_no?}', 'CloseOrderController@imgGet');//圖片

    //訂單相關
    Route::resource('OrderMgmt', 'OrderMgmtController');

    Route::get('dlvorder/check/{ord_no?}', 'OrderMgmtController@dlvordercheck'); 
    Route::post('confirmordfee/{feecd?}/{ord_id?}', 'OrderMgmtController@confirmordfee');
    Route::get('ordOverView', 'OrderMgmtController@ordOverView');   //訂單管理
    Route::post('order/checkforsn/{ord_no?}', 'OrderMgmtController@checkforsn');
    Route::post('order/snnostore', 'OrderMgmtController@snnostore');
    Route::post('OrderMgmt/changeReady', 'OrderMgmtController@changeReady');
    Route::post('order/compulsiveDel/del', 'OrderMgmtController@compulsiveDel');
    Route::post('order/error/changecar', 'OrderMgmtController@errorchangecar');

    Route::post('order/toclose', 'OrderMgmtController@toclose');
    
    Route::post('order/statusupdate', 'OrderMgmtController@statusupdate'); 

    Route::post('order/getdetailinfo', 'OrderMgmtController@getdetailinfo');
    Route::post('order/urltoprocess', 'OrderMgmtController@urltoprocess');

    Route::post('order/revertdata', 'OrderMgmtController@revertdata');
    Route::post('OrderMgmt/updatecarbon', 'OrderMgmtController@updatecarbon'); 
    Route::post('OrderMgmt/deliveryimg', 'OrderMgmtController@deliveryimg'); 
    Route::get('openorder/{ord_no?}', 'OrderMgmtController@openorder');


    Route::post('img/get', 'OrderMgmtController@imgDownload');
    Route::post('order/mistakecomplete', 'OrderMgmtController@mistakecomplete');

    Route::post('order/checkasnrask', 'OrderMgmtController@checkasnrask');
    Route::post('saveboxdetail', 'OrderMgmtController@saveboxdetail');
    Route::post('clearbotask', 'OrderMgmtController@clearbotask');
    Route::get('ordDetailOverView', 'OrderMgmtController@ordDetailOverView');   //訂單明細
    Route::get('ordDetailOverCloseView', 'OrderMgmtController@ordDetailOverCloseView');   //訂單明細總覽(結案)
    Route::get('ordOverViewN', 'OrderMgmtController@ordOverViewN');   //訂單總覽

    Route::get('importorder', 'ExcelImportController@index');
    Route::post('importorder/excel/import', 'ExcelImportController@excelimport');
    Route::get('clearTmpOrder', 'ExcelImportController@clearTmpOrder');
    Route::post('ExcelImport/store', 'ExcelImportController@saveToModOrder');

    Route::get('importdssorder', 'ExcelImportdssController@index');
    Route::post('importdssorder/excel/import', 'ExcelImportdssController@excelimport');
    Route::get('cleardssTmpOrder', 'ExcelImportdssController@clearTmpOrder');
    Route::post('ExcelImportdss/store', 'Api\DataController@gettestOrder');


    
    Route::post('OrderMgmtclose/excel/export', 'OrderMgmtController@exportdata');
    Route::post('ordOverViewN/excel/export', 'OrderMgmtController@exportdata');
    Route::post('ordDetailOverView/excel/export', 'OrderMgmtController@exportdata');
    Route::post('ordDetailOverCloseView/excel/export', 'OrderMgmtController@exportdata');
    Route::post('sendCar/excel/export', 'OrderMgmtController@exportdata');
    Route::post('dlvPlanSearch/excel/export', 'OrderMgmtController@exportdata');
    Route::post('tranPlan/excel/export', 'OrderMgmtController@exportdata');
    Route::post('modDlvPlan/excel/export', 'OrderMgmtController@exportdata');

    
    Route::get('OrderMgmt/getTsData', 'OrderMgmtController@getTsData');
    
    Route::get('OrderMgmt/detailget/{ord_id?}', 'OrderMgmtController@detailGet');//訂單明細
    Route::post('OrderMgmt/detailstore', 'OrderMgmtController@detailStore');//訂單明細
    Route::post('OrderMgmt/detailupdate/{ord_id}', 'OrderMgmtController@detailUpdate');//訂單明細
    Route::get('OrderMgmt/detaildelete/{id}', 'OrderMgmtController@detailDel');//訂單明細
    
    Route::get('OrderMgmt/palletget/{ord_id?}', 'OrderMgmtController@palletget');//服務項目
    Route::post('OrderMgmt/palletstore', 'OrderMgmtController@palletstore');//服務項目
    Route::post('OrderMgmt/palletupdate/{ord_id}', 'OrderMgmtController@palletupdate');//服務項目
    Route::get('OrderMgmt/palletdelete/{id}', 'OrderMgmtController@palletdelete');//服務項目
    
    Route::get('OrderMgmt/imgget/{ref_no?}', 'OrderMgmtController@imgGet');//圖片
    Route::get('errorprocess/imgget/{ref_no?}', 'OrderMgmtController@errorimgGet');//圖片
    Route::get('errorprocess/fcmget/{sys_ord_no?}', 'OrderMgmtController@errorfcmGet');//圖片
    Route::post('OrderMgmt/imgstore', 'OrderMgmtController@imgStore');//圖片
    Route::post('OrderMgmt/imgupdate/{ord_id}', 'OrderMgmtController@imgUpdate');//圖片
    Route::get('OrderMgmt/imgdelete/{id}', 'OrderMgmtController@imgDel');//圖片

    
    Route::get('OrderMgmt/tallyget/{ord_id?}', 'OrderMgmtController@tallyGet');//點貨紀錄
    Route::post('OrderMgmt/insertTracking', 'OrderMgmtController@insertTracking');
    Route::post('OrderMgmt/batchDelete', 'OrderMgmtController@batchDelete');
    Route::post('OrderMgmt/excel/export', 'OrderMgmtController@exportdata');
    



    //集團建檔
    Route::resource('companyProfile', 'CompanyProfileController');
    Route::post('companyProfile/batchDelete', 'CompanyProfileController@batchDelete');
    Route::post('companyProfile/excel/export', 'CompanyProfileController@exportdata');
    
    //車輛建檔
    Route::resource('carProfile', 'CarProfileController');
    Route::post('carProfile/batchDelete', 'CarProfileController@batchDelete');
    Route::post('carProfile/excel/export', 'CarProfileController@exportdata');

    //權限
    Route::resource('permission', 'PermissionController');
    Route::post('permission/batchDelete', 'PermissionController@batchDelete');
    Route::post('permission/excel/export', 'PermissionController@exportdata');

    
    
    //tms派車單
    Route::resource('dlvProfile', 'DlvProfileController');
    Route::post('dlvProfile/batchDelete', 'DlvProfileController@batchDelete');
    Route::post('dlvProfile/excel/export', 'DlvProfileController@exportdata');
    Route::get('dlvProfilePICKDLV/{id}', 'DlvProfileController@getdlvpick');
    Route::post('dlvProfile/revertdata', 'DlvProfileController@revertdata');
    Route::post('dlvProfile/revertdatabycar', 'DlvProfileController@revertdatabycar');
    Route::post('dlvProfile/revertdatabytranfer', 'DlvProfileController@revertdatabytranfer');

    //基本健檔
    Route::resource('bscodeKind', 'BscodeKindController');
    Route::post('bscodeKind/batchDelete', 'BscodeKindController@batchDelete');
    Route::post('bscodeKind/excel/export', 'BscodeKindController@exportdata');
    Route::get('bscode/{error_type?}', 'BscodeKindController@detailget');
    Route::post('bscode/store', 'BscodeKindController@detailStore');
    Route::post('bscode/update/{error_type}', 'BscodeKindController@detailUpdate');
    Route::get('bscode/delete/{id}', 'BscodeKindController@detailDel');

    //匯入excel
    Route::resource('mapping', 'MappingController');
    Route::post('mapping/batchDelete', 'MappingController@batchDelete');
    Route::post('mapping/excel/export', 'MappingController@exportdata');
    Route::get('mappingDetail/{mapping_code?}', 'MappingController@detailget');
    Route::post('mappingDetail/store', 'MappingController@detailStore');
    Route::post('mappingDetail/update/{mapping_code}', 'MappingController@detailUpdate');
    Route::get('mappingDetail/delete/{id}', 'MappingController@detailDel');

    //多語系建檔
    Route::resource('language', 'languageController');
    Route::post('language/excel/export', 'languageController@exportdata');

    //mailformat
    Route::resource('mailFormat', 'MailFormatController');
    Route::post('mailFormat/batchDelete', 'MailFormatController@batchDelete');
    Route::post('mailFormat/excel/export', 'MailFormatController@exportdata');
    Route::post('mailFormat/send/mail', 'MailFormatController@sendmail');



    //檔案(附件)
    Route::post('tranferuploadfile', 'FileController@tranferuploadfile');
    Route::post('file/upload/{applono?}', 'FileController@uploadfile');
    Route::post('file/delete/{applono?}', 'FileController@deletefile');
    Route::post('file/download/{applono?}', 'FileController@downloadfile');


    Route::get('/queryTracking', "TrackingApiController@trackingView");

    Route::get('/tracking', function () {
        $title = trans('tracking.titleName');
        echo "<title>$title</title>";
        try{
            if(Auth::user()->hasPermissionTo('TRACKING'))
            {
                return view('tracking.tracking');
            }else{
                return back();
            }
        }
        catch(\Exception $e) {
            return back();
        }
        return back();
    });

    //excel匯入範本
    Route::resource('jdsDataImport', 'JdsDataImportController');

    //sample
    Route::resource('sample', 'SampleController');

    Route::get('baseApi/getFunctionSchemaForGrid/{functionName}', 'BaseApiController@getFunctionSchemaForGrid');

});