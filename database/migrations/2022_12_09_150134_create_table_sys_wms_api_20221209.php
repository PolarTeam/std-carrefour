<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSysWmsApi20221209 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_wms_api', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wms_ord_no')->nullable()->comment('');
            $table->string('ord_no')->nullable()->comment('');
            $table->string('status')->nullable()->comment('');
            $table->string('status_desc')->nullable()->comment('');
            $table->dateTime('action_time')->nullable()->comment('');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_wms_api');
    }
}
