<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModBaseMessage20221205 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_base_message', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('send_user',1000)->nullable()->comment('發送類型代碼');
            $table->string('send_name',1000)->nullable()->comment('發送類型');
            $table->string('send_role',1000)->nullable()->comment('發送類型代碼');
            $table->string('send_role_name',1000)->nullable()->comment('發送類型');
            $table->bigInteger('send_count')->nullable()->comment('發送次數');
            $table->text('content')->nullable()->comment('內容');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_base_message');
    }
}
