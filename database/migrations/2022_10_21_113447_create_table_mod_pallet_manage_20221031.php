<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModPalletManage20221031 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_pallet_manage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('warehouse_no')->nullable()->comment('倉庫代碼');
            $table->string('warehouse_name')->nullable()->comment('倉庫名稱');
            $table->string('owner_by')->nullable()->comment('司機代碼');
            $table->string('owner_name')->nullable()->comment('司機名稱');
            $table->string('cust_no')->nullable()->comment('客戶代碼');
            $table->string('cust_name')->nullable()->comment('客戶名稱');
            $table->string('pallet_num')->nullable()->comment('棧板數量');
            $table->string('pallet_code')->nullable()->comment('棧板代碼');
            $table->string('pallet_type')->nullable()->comment('棧板類別');
            $table->string('type_cd')->nullable()->comment('帳務所屬代碼');
            $table->string('type_name')->nullable()->comment('帳務所屬');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_pallet_manage');
    }
}
