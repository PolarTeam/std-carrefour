<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModDlvOrder20221027 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_dlv_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cust_dlv_no')->nullable()->comment('客戶派車單號');
            $table->string('cust_dlv_no_group')->nullable()->comment('客戶派車單號群組');
            $table->bigInteger('pallet_num')->nullable()->comment('棧板數量');
            $table->string('pallet_code')->nullable()->comment('棧板代碼');
            $table->string('pallet_type')->nullable()->comment('棧板類別');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_dlv_order');
    }
}
