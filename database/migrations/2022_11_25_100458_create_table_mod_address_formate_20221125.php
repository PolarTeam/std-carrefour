<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModAddressFormate20221125 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_address_formate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('formate_name')->nullable()->comment('客戶名稱');
            $table->string('formate_address')->nullable()->comment('客戶地址');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_address_formate');
    }
}
