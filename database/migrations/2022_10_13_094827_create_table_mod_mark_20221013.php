<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModMark20221013 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_mark', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cust_no')->nullable()->comment('嘪頭代碼');
            $table->string('cust_name')->nullable()->comment('嘪頭名稱');
            $table->string('wh_no')->nullable()->comment('倉庫代碼');
            $table->string('wh_name')->nullable()->comment('倉庫名稱');
            $table->string('type')->nullable()->comment('類型');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('is_enabled')->nullable()->comment('啟用');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_mark');
    }
}
