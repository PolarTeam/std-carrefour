<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModDssDlvDetail20221101 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_dss_dlv_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sys_dlv_no')->nullable()->comment('系統車次編號');
            $table->string('dlv_no')->nullable()->comment('車次編號');
            $table->string('trs_mode')->nullable()->comment('配送型態代碼');
            $table->string('trs_mode_desc')->nullable()->comment('配送型態');
            $table->string('sys_ord_no')->nullable()->comment('配送單號');
            $table->string('owner_cd')->nullable()->comment('客戶代碼');
            $table->string('owner_name')->nullable()->comment('客戶');
            $table->string('ord_no')->nullable()->comment('客戶單號');
            $table->string('cust_name')->nullable()->comment('客戶店家名稱');
            $table->string('cust_zip')->nullable()->comment('郵遞區號');
            $table->string('cust_address')->nullable()->comment('住址');
            $table->float('cbm', 8, 2)->nullable()->comment('材積');
            $table->float('gw', 8, 2)->nullable()->comment('重量');
            $table->string('box_num')->nullable()->comment('箱數');
            $table->string('wms_ord_no')->nullable()->comment('訂單編號');
            $table->string('order_remark')->nullable()->comment('訂單備註');
            $table->string('dsv_remark')->nullable()->comment('備註(DSV客服)');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_dss_dlv_detail');
    }
}
