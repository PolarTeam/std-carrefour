<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDssTempOrder20221129 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dss_temp_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detailCount')->nullable()->comment('');
            $table->string('dc_id')->nullable()->comment('');
            $table->string('dlv_no')->nullable()->comment('');
            $table->string('dlv_date')->nullable()->comment('');
            $table->string('dlv_type')->nullable()->comment('');
            $table->string('dlv_type_desc')->nullable()->comment('');
            $table->string('car_type')->nullable()->comment('');
            $table->string('car_type_name')->nullable()->comment('');
            $table->string('truck_cmp_no')->nullable()->comment('');
            $table->string('truck_cmp_name')->nullable()->comment('');
            $table->string('driver_nm')->nullable()->comment('');
            $table->string('driver_phone')->nullable()->comment('');
            $table->bigInteger('total_cbm')->nullable()->comment('');
            $table->bigInteger('total_gw')->nullable()->comment('');
            $table->bigInteger('total_box_num')->nullable()->comment('');
            $table->bigInteger('mark_no')->nullable()->comment('');
            $table->bigInteger('dlv_num')->nullable()->comment('');
            $table->string('creatUser')->nullable()->comment('');
            $table->string('creatTime')->nullable()->comment('');
            $table->string('actUser')->nullable()->comment('');
            $table->string('actTime')->nullable()->comment('');

            $table->string('cust_name')->nullable()->comment('');
            $table->string('cust_address')->nullable()->comment('');
            $table->string('itemp')->nullable()->comment('');
            $table->string('floor')->nullable()->comment('');
            $table->bigInteger('collect_amount')->nullable()->comment('');
            $table->string('trs_mode_desc')->nullable()->comment('');
            $table->string('trs_mode')->nullable()->comment('');
            $table->string('dlv_ord_no')->nullable()->comment('');
            $table->string('owner_cd')->nullable()->comment('');
            $table->string('owner_name')->nullable()->comment('');
            $table->string('ord_no')->nullable()->comment('');
            $table->string('cust_zip')->nullable()->comment('');
            $table->bigInteger('cbm')->nullable()->comment('');
            $table->bigInteger('gw')->nullable()->comment('');
            $table->bigInteger('box_num')->nullable()->comment('');
            $table->string('wms_ord_no')->nullable()->comment('');
            $table->string('order_remark')->nullable()->comment('');
            $table->string('dsv_remark')->nullable()->comment('');
            $table->timestamps();




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dss_temp_order');
    }
}
