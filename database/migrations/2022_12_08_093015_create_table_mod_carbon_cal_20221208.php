<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModCarbonCal20221208 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_carbon_cal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trs_mode')->nullable()->comment('運輸溫別代碼');
            $table->string('trs_mode_desc')->nullable()->comment('運輸溫別');
            $table->string('car_type')->nullable()->comment('車型代碼');
            $table->string('car_type_desc')->nullable()->comment('車型');
            $table->string('carbon_value')->nullable()->comment('碳排係數KgCo2/噸公里');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_carbon_cal');
    }
}
