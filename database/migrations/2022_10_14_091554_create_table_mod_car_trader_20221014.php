<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModCarTrader20221014 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_car_trader', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cust_no')->nullable()->comment('車商代碼');
            $table->string('cust_name')->nullable()->comment('車商名稱');
            $table->string('attn')->nullable()->comment('聯絡人');
            $table->string('phone')->nullable()->comment('電話');
            $table->string('address')->nullable()->comment('地址');
            $table->string('carpassword')->nullable()->comment('車輛密碼');
            
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('is_enabled')->nullable()->comment('啟用');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_car_trader');
    }
}
