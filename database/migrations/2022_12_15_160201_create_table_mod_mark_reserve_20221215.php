<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModMarkReserve20221215 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_mark_reserve', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wh_no')->nullable()->comment('倉庫代碼');
            $table->string('wh_name')->nullable()->comment('倉庫名稱');
            $table->date('reserve_date')->nullable()->comment('預約日期');
            $table->string('reserve_h')->nullable()->comment('預約日期-時');
            $table->string('reserve_m')->nullable()->comment('預約日期-分');
            $table->string('owner_no')->nullable()->comment('貨主代碼');
            $table->string('owner_name')->nullable()->comment('貨主名稱');
            $table->string('mark_no')->nullable()->comment('貨主代碼');
            $table->string('mark_name')->nullable()->comment('貨主名稱');
            $table->string('process_time')->nullable()->comment('作業分鐘數');
            $table->string('driver_phone')->nullable()->comment('司機電話');
            $table->string('driver_name')->nullable()->comment('司機姓名');
            $table->string('con_type')->nullable()->comment('貨櫃類型代碼');
            $table->string('con_type_desc')->nullable()->comment('貨櫃類型');
            $table->string('con_no')->nullable()->comment('貨櫃號碼');
            $table->string('unloading_type')->nullable()->comment('下貨類型代碼');
            $table->string('unloading_descp')->nullable()->comment('下貨類型');
            $table->string('work_type')->nullable()->comment('下貨類型代碼');
            $table->string('work_descp')->nullable()->comment('下貨類型');
            $table->string('cust_import_no')->nullable()->comment('客戶進貨單號');
            $table->string('car_no')->nullable()->comment('車號');
            $table->string('car_type')->nullable()->comment('車型代碼');
            $table->string('car_type_descp')->nullable()->comment('車型');
            $table->string('truck_cmp_no')->nullable()->comment('車商代碼');
            $table->string('truck_cmp_name')->nullable()->comment('車商');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_mark_reserve');
    }
}
