<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModMarkSetting20221207 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_mark_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('warehouse_id')->nullable()->comment('倉別代碼');
            $table->string('warehouse_name')->nullable()->comment('倉別');
            $table->string('port_type')->nullable()->comment('進出貨型態');
            $table->string('car_type')->nullable()->comment('車型代碼');
            $table->string('car_type_desc')->nullable()->comment('車型');
            $table->string('action')->nullable()->comment('動作');
            $table->string('con_type')->nullable()->comment('櫃型');
            $table->bigInteger('box_num')->nullable()->comment('箱數');
            $table->bigInteger('items_num')->nullable()->comment('品項數');
            $table->bigInteger('process_time')->nullable()->comment('上/下貨時間(分鐘)');
            $table->string('trans_time')->nullable()->comment('轉場時間(分鐘)');
            $table->string('trs_mode')->nullable()->comment('運輸型態代碼');
            $table->string('trs_mode_desc')->nullable()->comment('運輸型態');
            $table->string('start_time')->nullable()->comment('出貨預計報到開始時間');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_mark_setting');
    }
}
