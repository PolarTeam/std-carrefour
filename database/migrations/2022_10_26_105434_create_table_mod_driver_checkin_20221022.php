<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModDriverCheckin20221022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_driver_checkin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('job_type')->nullable()->comment('作業類型代碼');
            $table->string('job_type_desc')->nullable()->comment('作業類型代碼');
            $table->string('cust_ord_no')->nullable()->comment('客戶進貨單號');
            $table->string('car_no')->nullable()->comment('車號');
            $table->string('car_type')->nullable()->comment('車型代碼');
            $table->string('car_type_desc')->nullable()->comment('車型');
            $table->string('phone')->nullable()->comment('司機電話');
            $table->string('driver_name')->nullable()->comment('司機姓名');
            $table->string('con_type')->nullable()->comment('貨櫃類型代碼');
            $table->string('con_type_desc')->nullable()->comment('貨櫃類型');
            $table->string('con_id')->nullable()->comment('貨櫃號碼');
            $table->string('unloading_type')->nullable()->comment('下貨類型代碼');
            $table->string('unloading_type_desc')->nullable()->comment('下貨類型');
            $table->string('mark_no')->nullable()->comment('碼頭');
            $table->string('warehouse')->nullable()->comment('報到倉');
            $table->string('status')->nullable()->comment('進/出場狀態代碼');
            $table->string('status_desc')->nullable()->comment('進/出場狀態');
            $table->dateTime('entry_time')->nullable()->comment('進場時間');
            $table->dateTime('leave_time')->nullable()->comment('離場時間');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_driver_checkin');
    }
}
