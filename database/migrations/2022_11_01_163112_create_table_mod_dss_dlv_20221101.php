<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModDssDlv20221101 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_dss_dlv', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sys_dlv_no')->nullable()->comment('系統車次編號');
            $table->string('dlv_no')->nullable()->comment('車次編號');
            $table->date('dlv_date')->nullable()->comment('配送日期');
            $table->string('dlv_type')->nullable()->comment('運輸型態');
            $table->string('car_type')->nullable()->comment('車型');
            $table->string('truck_cmp_no')->nullable()->comment('貨運公司');
            $table->string('car_no')->nullable()->comment('車號');
            $table->string('driver')->nullable()->comment('駕駛');
            $table->string('phone')->nullable()->comment('連絡電話');
            $table->float('cbm', 8, 2)->nullable()->comment('材積');
            $table->float('gw', 8, 2)->nullable()->comment('重量');
            $table->string('box_num')->nullable()->comment('箱數');
            $table->string('mark_no')->nullable()->comment('碼頭編號');
            $table->string('dlv_point')->nullable()->comment('配送點數');
            $table->string('g_key')->nullable()->comment('集團');
            $table->string('c_key')->nullable()->comment('公司');
            $table->string('s_key')->nullable()->comment('站別');
            $table->string('d_key')->nullable()->comment('部門');
            $table->string('created_by')->nullable()->comment('建立人');
            $table->string('updated_by')->nullable()->comment('修改人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_dss_dlv');
    }
}
