#2021-09-02 add 已更新
INSERT INTO `fine_wms`.`function_schema` (`company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES ('1', '0', 'goodsSetup', 'goodsMgmt', 'String', 'stock_minimal', '安全庫存量', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`, `created_at`, `updated_at`) VALUES (NULL, '1', '2', 'PRODUCT_IMPORT', '31', 'stock_minimal', '安全庫存量', '安全庫存量', '1', '0', '2021-06-29 15:10:00', '2021-06-29 15:10:00');
INSERT INTO `fine_wms`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '10', 'inventoryQueryByProd', 'inventoryMgmt', 'Number', 'stock_minimal', '安全庫存水位', '2021-06-15 15:53:00', '2021-06-15 15:53:00');

ALTER TABLE `fine_wms`.`stock_by_product` 
ADD COLUMN `stock_minimal` BIGINT NULL AFTER `actual_nums_bad`;

#2021-09-02 add
INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES (NULL, '1', '6', 'INBOUND_IMPORT', '24', 'inventory_lottable1', '屬性1', '屬性1', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES ('1', '6', 'INBOUND_IMPORT', '25', 'inventory_lottable2', '屬性2', '屬性2', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES ('1', '6', 'INBOUND_IMPORT', '26', 'inventory_lottable3', '屬性3', '屬性3', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES ('1', '6', 'INBOUND_IMPORT', '27', 'inventory_lottable4', '屬性4', '屬性4', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES ('1', '6', 'INBOUND_IMPORT', '28', 'inventory_lottable5', '屬性5', '屬性5', '1', '0');

INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES (NULL, '1', '1', 'OUTBOUND_IMPORT', '25', 'outbound_lottable2', '指定屬性2', '指定屬性2', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES (NULL, '1', '1', 'OUTBOUND_IMPORT', '26', 'outbound_lottable3', '指定屬性3', '指定屬性3', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES (NULL, '1', '1', 'OUTBOUND_IMPORT', '27', 'outbound_lottable4', '指定屬性4', '指定屬性4', '1', '0');
INSERT INTO `fine_wms`.`mapping_detail` (`id`, `wh_id`, `mapping_id`, `mapping_code`, `seq`, `col_name`, `excel_col_name`, `excel_col_name2`, `is_enabled`, `is_deleted`) VALUES (NULL, '1', '1', 'OUTBOUND_IMPORT', '28', 'outbound_lottable5', '指定屬性5', '指定屬性5', '1', '0');

#2021-09-03 add
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '10', 'inventoryQueryByProd', 'inventoryMgmt', 'String', 'product_spec', '商品規格', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '9', 'inventoryQueryByProd', 'inventoryMgmt', 'Number', 'box_in_num', '箱入數', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '9', 'inventoryQueryByStorage', 'inventoryMgmt', 'Number', 'box_in_num', '箱入數', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '9', 'inventoryQueryByStorage', 'inventoryMgmt', 'String', 'product_spec', '商品規格', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '9', 'inventoryQueryByinboundDate', 'inventoryMgmt', 'String', 'product_spec', '商品規格', '2021-06-15 15:53:00', '2021-06-15 15:53:00');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`, `created_at`, `updated_at`) VALUES (NULL, '1', '9', 'inventoryQueryByinboundDate', 'inventoryMgmt', 'Number', 'box_in_num', '箱入數', '2021-06-15 15:53:00', '2021-06-15 15:53:00');

#2021-09-06
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`) VALUES (NULL, '1', '10', 'inventoryQuery', 'putInMgmt', 'Number', 'box_in_num', '箱入數');
INSERT INTO `fine_db`.`function_schema` (`id`, `company_id`, `seq`, `func_name`, `root_id`, `field_type`, `field_name`, `display_name`) VALUES (NULL, '1', '13', 'outboundDetailQuery', 'outboundMgmt', 'Number', 'box_in_num', '箱入數');


#2021-09-30
ALTER TABLE `wms2`.`product` 
ADD COLUMN `created_by_name` VARCHAR(45) NULL AFTER `cust_field11`,
ADD COLUMN `updated_by_name` VARCHAR(45) NULL AFTER `created_by_name`;

ALTER TABLE `wms2`.`order_detail` 
ADD COLUMN `date_put_away_confirm` DATE NULL AFTER `days_allowed`;
ALTER TABLE `wms2`.`outbound_detail` 
ADD COLUMN `date_put_away_confirm` DATE NULL AFTER `days_allowed`;


#2021-10-12
CREATE TABLE `wms2`.`api_error_log` (
    `id` BIGINT NOT NULL,
    `subject` VARCHAR(45) NULL,
    `url` VARCHAR(255) NULL,
    `method` VARCHAR(45) NULL,
    `ip` VARCHAR(45) NULL,
    `agent` VARCHAR(255) NULL,
    `post_data` TEXT NULL,
    `message` VARCHAR(45) NULL,
    `created_by` BIGINT NULL,
    `created_by_name` VARCHAR(100) NULL,
    `updated_by` BIGINT NULL,
    `updated_by_name` VARCHAR(100) NULL,
    `created_at` TIMESTAMP NULL,
    `updated_at` TIMESTAMP NULL,
    PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;
ALTER TABLE `wms2`.`api_error_log` 
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;


CREATE TABLE `wms2`.`api_config` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `platform` VARCHAR(45) NULL,
    `login_info` VARCHAR(255) NULL,
    `created_at` TIMESTAMP NULL,
    `updated_at` TIMESTAMP NULL,
    PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

#2021-10-24 update to all server
ALTER TABLE `lanni_wms`.`rec_inbound` 
ADD COLUMN `po_date` DATE NULL AFTER `consignee_tel`,
ADD COLUMN `sales_user` VARCHAR(45) NULL AFTER `date_put_away_confirm_display`,
ADD COLUMN `po_user` VARCHAR(45) NULL AFTER `sales_user`,
ADD COLUMN `mbl_no` VARCHAR(100) NULL AFTER `po_user`,
ADD COLUMN `shipping_no` VARCHAR(45) NULL AFTER `mbl_no`,
ADD COLUMN `shipping_type` VARCHAR(45) NULL AFTER `shipping_no`,
ADD COLUMN `atd_for_tw` DATETIME NULL AFTER `shipping_no`;

ALTER TABLE `lanni_wms`.`inbound_detail` 
ADD COLUMN `ec_product_code` VARCHAR(255) NULL AFTER `product_code`;

ALTER TABLE `lanni_wms`.`inbound_detail` 
ADD COLUMN `outbound_etd` VARCHAR(45) NULL AFTER `owner_name`;

ALTER TABLE `lanni_wms`.`rec_inbound` 
ADD COLUMN `vendor_name` VARCHAR(150) NULL AFTER `date_put_away_confirm_display`;




ALTER TABLE `lanni_wms`.`inbound_detail` 
ADD COLUMN `is_preorder` VARCHAR(1) NULL DEFAULT 'N' AFTER `inbound_unit`,
ADD COLUMN `ecstore_code` VARCHAR(45) NULL AFTER `is_preorder`,
ADD COLUMN `inbound_detail_note` TEXT NULL AFTER `ecstore_code`;

ALTER TABLE `lanni_wms`.`rec_outbound` 
ADD COLUMN `etd` DATE NULL COMMENT '預計出貨日' AFTER `wh_id`;

ALTER TABLE `gg_wms`.`product` 
ADD COLUMN `random_nums` INT(11) NULL DEFAULT NULL AFTER `product_relation`;

ALTER TABLE `lanni_wms`.`inbound_detail` 
ADD COLUMN `pre_order_no` TEXT NULL AFTER `ecstore_code`;


ALTER TABLE `lanni_wms`.`rec_inbound` 
ADD COLUMN `is_printed` VARCHAR(1) NULL DEFAULT 'N' AFTER `import_time`;

CREATE TABLE `ecan_no` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `outbound_no` varchar(155) DEFAULT NULL,
    `shipping_no` varchar(155) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `shipping_no_UNIQUE` (`shipping_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `rec_transfer` 
ADD COLUMN `created_by_name` VARCHAR(45) NULL AFTER `created_by`,
ADD COLUMN `updated_by_name` VARCHAR(45) NULL AFTER `updated_by`;

ALTER TABLE `inbound_detail` 
ADD COLUMN `po_no` VARCHAR(150) NULL AFTER `inbound_detail_note`;

ALTER TABLE `rec_inbound` 
ADD COLUMN `sales_expect` DATE NULL AFTER `etd_for_tw`;

ALTER TABLE `lanni_wms`.`rec_outbound` 
ADD COLUMN `merge_order_no` VARCHAR(500) NULL AFTER `outbound_no`;





