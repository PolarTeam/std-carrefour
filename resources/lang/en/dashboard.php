<?php 

 return [
    "action" => "Action",
    "addProducts" => "Add Products to Cart",
    "anotherAction" => "Another Action",
    "completePurchase" => "Complete Purchase",
    "customerService" => "Customer Service",
    "titleName" => "Shortcut Button",
    "eta" => "最後出車時間",
    "goalCompletions" => "GOAL COMPLETIONS",
    "goalCompletion" => "Goal Completion",
    "monthly" => "Monthly Recap Report",
    "onlineTruck" => "Online Truck",
    "sendInquiries" => "Send Inquiries",
    "separatedLink" => "Separated link",
    "somethingElseHere" => "Something else here",
    "speed" => "Speed",
    "totalCost" => "TOTAL COST",
    "totalProfit" => "TOTAL PROFIT",
    "totalRevenue" => "TOTAL REVENUE",
    "updated" => "Updated",
    "visitPremium" => "Visit Premium Page",
    "battery" => "Battery",
    "error" => "Abnormal",
    "rate" => "Completion",
    "order" => "Order Q'ty",
    "map" => "GPS"
];