<?php 

 return [
    "id"          => "id",
    "name"        => "Name",
    "type"        => "Type",
    "sorted"        => "Sort",
    "baseinfo"    => "Basic Information",
    "titleName"   => "Petmission Management",
    "displayName" => "Display Name",
    "createdAt"   => "Created At",
    "updatedAt"   => "Updated At",
];