<?php 

 return [
    "msg1" => "Incorrect order number",
    "inputOrdNo" => "Please enter the order number!",
    "ordNo" => "Order No.",
    "msg2" => "Please check the order number!",
    "titleName" => "Tracking"
];