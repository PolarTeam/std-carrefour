<?php 

 return [
    "autoFilling" => "Automatic Filling (By Cbm)",
    "cancelCar" => "Cancel To Dispatch A Car",
    "carNo" => "Car No",
    "carType" => "Car Type",
    "chooseOrder" => "Choose An Order",
    "delivering" => "Delivering",
    "deliveryCompleted" => "Delivery Completed",
    "deliveryError" => "Delivery Error",
    "dlvNo" => "Delivery No",
    "titleName" => "Delivery Overview",
    "dlvPlan" => "Delivery Planning",
    "driver" => "Driver",
    "driverPhone" => "Driver Phone",
    "loadRate" => "Load Rate",
    "loadTcbm" => "Load Total Cbm",
    "loadWeight" => "Load Weight",
    "loadingDetails" => "Loading Details",
    "msg5" => "Loading Failed",
    "loadingPlan" => "Loading Plan",
    "msg4" => "Loading Success",
    "manualFilling" => "Manual Filling",
    "notYet" => "Not yet arranged",
    "ordQuery" => "Order Query",
    "msg2" => "Please Enter The Car Number",
    "routingPlan" => "Routing Plan",
    "sendApp" => "Send APP",
    "sendCarInfo" => "Send Car Information",
    "status" => "Status",
    "msg3" => "Success Message",
    "custNo" => "The Car Dealer",
    "msg1" => "Warnning Message"
];