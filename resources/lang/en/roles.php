<?php 

 return [
    "id"          => "id",
    "name"        => "Name",
    "baseinfo"    => "Basic Information",
    "titleName"   => "Role Management",
    "displayName" => "Display Name",
    "createdAt"   => "Created At",
    "updatedAt"   => "Updated At",
];