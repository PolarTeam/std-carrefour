<?php 

 return [
    "id"              => "id",
    "mappingId"       => "Mapping Id",
    "mappingCode"     => "Mapping Code",
    "seq"             => "Seq",
    "colName"         => "Col Name",
    "excelColName"    => "Excel Col Name",
    "excelColName2"   => "Excel Col Name 2",
    "delimiter"       => "Delimiter",
    "strLeft"         => "Str Left",
    "strRight"        => "Str Right",
    "isEnabled"       => "Is Enabled",
    "isDeleted"       => "Is Deleted",
    "createdByName"   => "Created By Name",
    "updatedByName"   => "Updated By Name",
    "createdAt"       => "Created At",
    "updatedAt"       => "Updated At",
];