<?php 

 return [
    "id"              => "id",
    "mappingId"       => "Mapping Id",
    "mappingCode"     => "Mapping Code",
    "mappingName"     => "Mapping Name",
    "headingRow"      => "Heading Row",
    "mappingType"     => "Mapping Type",
    "isEnabled"       => "Is Enabled",
    "isDeleted"       => "Is Deleted",
    "createdByName"   => "Created By Name",
    "updatedByName"   => "Updated By Name",
    "createdAt"       => "Created At",
    "updatedAt"       => "Updated At",
    "detail"          => "Detail",
    "titleName"       => "Mapping",
    "titleAddName"    => "Mapping Detail"
];