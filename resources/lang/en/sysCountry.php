<?php 

 return [
    "cKey" => "Company",
    "cntryCd" => "Country Code",
    "cntryNm" => "Country",
    "createdAt" => "Created Time",
    "createdBy" => "Created User",
    "dKey" => "Dept.",
    "gKey" => "Group",
    "id" => "id",
    "sKey" => "Station",
    "updatedAt" => "Updated Time",
    "updatedBy" => "Updated User",
    "titleName" => "國家資料彙總",
    "titleAddName" => "國家建檔"
];