<?php 

 return [
    "id"               => "id",
    "partNo"           => "料品编号",
    "partName"         => "料品名称",
    "status"           => "状态",
    "baseinfo"         => "基本资讯",
    "titleName"        => "Wind Chil ",
    "createdAt"        => "建立时间",
    "updatedAt"        => "修改时间",
    "updatedByName"    => "修改人",
    "createdByName"    => "创建人",
];