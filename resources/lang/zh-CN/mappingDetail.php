<?php 

return [
    "id"              => "id",
    "mappingId"       => "对应 Id",
    "mappingCode"     => "对应代码",
    "seq"             => "序号",
    "colName"         => "栏位名称",
    "excelColName"    => "Excel表头名称",
    "excelColName2"   => "Excel表头名称 2",
    "delimiter"       => "分隔符号",
    "strLeft"         => "文字左",
    "strRight"        => "文字右",
    "isEnabled"       => "启用",
    "isDeleted"       => "删除",
    "createdByName"   => "创建人",
    "updatedByName"   => "修改人",
    "createdAt"       => "建立时间",
    "updatedAt"       => "修改时间",
    ];