<?php 

return [
    "id"              => "id",
    "mappingId"       => "对应 Id",
    "mappingCode"     => "对应代码",
    "mappingName"     => "对应名称",
    "headingRow"      => "表头列数",
    "mappingType"     => "对应类别",
    "isEnabled"       => "启用",
    "isDeleted"       => "删除",
    "createdByName"   => "新增人",
    "updatedByName"   => "修改人",
    "createdAt"       => "建立时间",
    "updatedAt"       => "修改时间",
    "detail"          => "明细",
    "titleName"       => "excel 汇入对应设定",
    "titleAddName"    => "对应明细"
    ];