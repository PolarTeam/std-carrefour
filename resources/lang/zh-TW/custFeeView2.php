<?php 

 return [
    "titleName" => "BSCODE",
    "cd" => "Code",
    "id" => "id",
    "value1" => "value1",
    "value2" => "value2",
    "value3" => "value3",
    "cdDescp" => "代碼名稱",
    "cdType" => "代碼類別",
    "updatedBy" => "修改人",
    "updatedAt" => "修改時間",
    "cKey" => "公司",
    "createdBy" => "創建人",
    "createdAt" => "創建時間",
    "sKey" => "站別",
    "dKey" => "部門",
    "gKey" => "集團"
];