<?php 

 return [
    "id"              => "id",
    "mappingId"       => "對應 Id",
    "mappingCode"     => "對應代碼",
    "mappingName"     => "對應名稱",
    "headingRow"      => "表頭列數",
    "mappingType"     => "對應類別",
    "isEnabled"       => "啟用",
    "isDeleted"       => "刪除",
    "createdByName"   => "新增人",
    "updatedByName"   => "更新人",
    "createdAt"       => "新增時間",
    "updatedAt"       => "更新時間",
    "detail"          => "明細",
    "titleName"       => "excel 匯入對應設定",
    "titleAddName"    => "對應明細"
];