<?php 

 return [
    "id"              => "id",
    "mappingId"       => "對應 Id",
    "mappingCode"     => "對應代碼",
    "seq"             => "序號",
    "colName"         => "欄位名稱",
    "excelColName"    => "Excel表頭名稱",
    "excelColName2"   => "Excel表頭名稱 2",
    "delimiter"       => "分隔符號",
    "strLeft"         => "文字左",
    "strRight"        => "文字右",
    "isEnabled"       => "啟用",
    "isDeleted"       => "刪除",
    "createdByName"   => "新增人",
    "updatedByName"   => "更新人",
    "createdAt"       => "新增時間",
    "updatedAt"       => "更新時間",
];