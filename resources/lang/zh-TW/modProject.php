<?php 

 return [
    "id"               => "id",
    "projectName"      => "專案",
    "manageByName"     => "主管",
    "manageName"       => "主管",
    "secondManageName" => "次要主管",
    "secondManage"     => "次要主管",
    "baseinfo"         => "基本資訊",
    "titleName"        => "專案建檔",
    "createdAt"        => "建立時間",
    "updatedAt"        => "修改時間",
    "updatedByName"    => "修改人",
    "createdByName"    => "創建人",
];