<?php 

 return [
    "titleName" => "Excel mapping",
    "titleAddName" => "Excel mapping",
    "id" => "ID",
    "cKey" => "公司",
    "mappingName" => "名稱",
    "mappingCode" => "客戶代碼",
    "createdBy" => "建立人員",
    "createdAt" => "建立時間",
    "updatedBy" => "最後修改人員",
    "updatedAt" => "最後修改時間",
    "sKey" => "站別",
    "dKey" => "部門",
    "gKey" => "集團",
    "stationTranferType" => "郵遞區號轉換站所"
];