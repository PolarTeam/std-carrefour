<?php 

 return [
    "id"            => "id",
    "vendorNo"      => "供應商代碼",
    "vendorName"    => "名稱",
    "vendorAddress" => "地址",
    "vendorAttn"    => "聯絡人",
    "type"          => "類別",
    "sorted"        => "排序",
    "baseinfo"      => "基本資訊",
    "titleName"     => "供應商建檔",
    "displayName"   => "顯示名稱",
    "createdAt"     => "建立時間",
    "updatedAt"     => "修改時間",
    "updatedByName" => "修改人",
    "createdByName" => "創建人",
];