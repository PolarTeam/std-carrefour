<?php 

 return [
    "ftpport" => "FTP PORT",
    "ftppassword" => "FTP 密碼",
    "ftpuser" => "FTP 帳號",
    "ftpcat" => "FTP/SFTP",
    "ftppath" => "FTP位置",
    "ftpforder" => "FTP檔案位置",
    "sendmail" => "FTP發送mail",
    "email" => "email",
    "id" => "id",
    "STATUS_B" => "使用中",
    "updatedBy" => "修改人",
    "updatedAt" => "修改時間",
    "remark" => "備註",
    "fax" => "傳真",
    "cKey" => "公司",
    "cmpAbbr" => "公司簡稱",
    "receiveMail" => "到貨通知",
    "createdBy" => "創建人",
    "createdAt" => "創建時間",
    "areaNm" => "區域",
    "areaId" => "區域代碥",
    "address" => "地址",
    "cityNm" => "城市",
    "cityNo" => "城市代碼",
    "baseInfo" => "基本資訊",
    "cname" => "客戶中文名稱",
    "custNo" => "客戶代碼",
    "titleAddName" => "客戶建檔",
    "titleName" => "客戶彙總",
    "ename" => "客戶英文名稱",
    "custType" => "客戶類型",
    "STATUS_A" => "未使用",
    "status" => "狀態",
    "ftptype" => "發送方式",
    "sKey" => "站別",
    "taxId" => "統一編號",
    "contact" => "聯絡人",
    "emailType" => "貨況通知類型",
    "type" => "資料類型",
    "identity" => "身份",
    "dKey" => "部門",
    "zip" => "郵地區號",
    "gKey" => "集團",
    "phone" => "電話",
    "def" => "預設",
    "phone2" => "電話2"
];