<?php 

 return [
    "id"             => "id",
    "storageName"    => "儲位",
    "name"           => "名稱",
    "code"           => "代碼",
    "floorHouseName" => "樓層房名",
    "shelfNo"        => "貨架編號",
    "layer"          => "層",
    "remark"         => "備註",
    "baseinfo"       => "基本資訊",
    "titleName"      => "儲位建檔",
    "createdAt"      => "建立時間",
    "updatedAt"      => "修改時間",
    "updatedByName"    => "修改人",
    "createdByName"    => "創建人",
];