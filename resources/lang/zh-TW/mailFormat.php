<?php 

 return [
    "id"            => "id",
    "title"         => "主旨",
    "updatedBy"     => "修改人",
    "updatedAt"     => "修改時間",
    "cKey"          => "公司",
    "createdBy"     => "創建人",
    "createdAt"     => "創建時間",
    "createdByName" => "創建人",
    "updatedByName" => "修改人",
    "sKey"          => "站別",
    "dKey"          => "部門",
    "content"       => "內容",
    "titleName"     => "郵件標題",
    "type"          => "郵件類型",
    "name"          => "郵件類型名稱",
    "typeDescp"     => "郵件格式",
    "gKey"          => "集團"
];