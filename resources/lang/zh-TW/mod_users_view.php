<?php 

 return [
    "email" => "Email address not found",
    "password" => "Incorrect password",
    "failed" => "These credentials do not match our records.",
    "throttle" => "Too many login attempts. Please try again in :seconds seconds.",
    "identity" => "身份",
    "name" => "名稱",
    "email" => "信箱",
    "id" => "id",
    "createdAt" => "建立時間",
    "updatedAt" => "修改時間",
    "accountEnable" => "啟用",
    'titleName'=>'使用者管理',
    "role" => "角色",
    "gKey" => "集團",
    "cKey" => "公司",
    "dKey" => "部門",
    "sKey" => "站別"
];