@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('roles.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/user') }}">{{trans('roles.titleName')}}</a></li>
		<li class="active">{{trans('roles.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">{{trans('roles.name')}} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="name" name="name" required="required">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="display_name">{{trans('roles.display_name')}} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="display_name" name="display_name" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                    <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true" disabled="disabled">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                    <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true" disabled="disabled">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                    <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true" disabled="disabled">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                    <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true" disabled="disabled">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row"> 
                            @if(isset($id))
                                <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                <input type="hidden" name="_method" value="PUT" class="form-control">
                            @endif
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="false">web</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">app</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_2">
                        <div class="box-body">
                            <div class="row">
                                @foreach($web as $row)
                                    <div class="form-group col-md-3">
                                        <input type="checkbox" required  id="{{$row->name}}" name="{{$row->name}}" >
                                        <label for="" style="color: {{$row->color}};" >{{$row->display_name}}(第{{$row->level}}層)</label>
                                    </div>
                                @endforeach	
                            </div>
                        </div>
                    </div>
                    <div id="tab_3" class="tab-pane">
                        <div class="box-body">
                            <div class="row">
                                @foreach($app as $row)
                                <div class="form-group col-md-3">
                                    <input type="checkbox" required  id="{{$row->name}}" name="{{$row->name}}" >
                                    <label for="">{{$row->display_name}}</label>
                                </div>
                                @endforeach	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif
    
    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/role" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#role').select2();
            
        };
        formOpt.addFunc = function() {
        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }

        formOpt.saveSuccessFunc = function(data) {
            $('#myForm input:checkbox').each(function(){
                
                var id    = $(this).attr('id');
                document.getElementById(id).disabled=true;
            
            
            });
        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"display_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    $.get( formOpt.fieldsUrl , function( data ) {
        if(typeof formOpt.afterInit === "function") {
            console.log(data.permission);

            $.each(data.enabledPermission, function(i, v){
                $('#'+v).attr('checked', true);
            });
        }
    });
});
</script>
@endsection