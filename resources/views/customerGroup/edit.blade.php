@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_customer_group.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/customerGroup') }}">{{ trans('mod_customer_group.titleName') }}</a></li>
		<li class="active">{{ trans('mod_customer_group.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('mod_customer_group.group_cd') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="group_cd" name="group_cd">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="cust_name">{{ trans('mod_customer_group.group_name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="group_name" name="group_name">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('mod_customer_group.cust_name') }} <span style="color:red">*<span></label>
                                        <input type="hidden" name="cust_name" id = "cust_name" class="form-control">
                                        <select class="form-control select2" id="cust_no" name="cust_no" multiple="multiple">
                                            @foreach($customers as $row)
                                            <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                    </div>
                                </div>


                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
            },"wh_name");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerGroup" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#customerGroup').select2();
            $('#cust_no').select2();
        };
        formOpt.addFunc = function() {
            $('#cust_no').val();
            $('#cust_no').trigger('change');
        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;


            var requiredColumn = [
                {
                    "column_filed":"cust_no",
                    "column_type":"select2",
                    "ranked" : 0
                },
                {
                    "column_filed":"group_cd",
                    "column_type":"string",
                },
                {
                    "column_filed":"group_name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection