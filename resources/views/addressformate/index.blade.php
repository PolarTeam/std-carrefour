@extends('layout.layout')
@section('header')
<section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
    <h1>
        {{ trans('mod_address_formate.titleName') }}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('mod_address_formate.titleName') }}</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />
    <input type="hidden" value="{{$lang}}" id="lang" />
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
<div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="searchWindow2" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody2">
        <div id="searchContent2">

        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter2">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName2"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch2">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault2">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch2">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn2">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd2">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>
<div id="searchWindow3" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody2">
        <div id="searchContent2">

        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter2">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName3"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch3">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault3">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch3">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn3">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd3">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('before_scripts')
<style>
    .select2-selection__rendered {
        text-align: left;
    }
    .swal2-popup .swal2-content {
        font-size: 14px;
    }
</style>
<script>
    var lang = "{{$lang}}";
    var languagename = "";
    var tableName = 'mod_address_formate_view';
    var fileName = 'addressformate';
    var condition ='?basecon=is_exist;EQUAL;Y';
    var sqlcondition = ['is_exist', '=','Y'];
    var sqlconditiondetail = ['is_exist', '=','N'];
    var enabledheaderdetail = [];
    var btnGroupList = ["btnExportExcel","btnSearchWindow",  "{{$canStore==true?'btnAdd':''}}"];
    var btnGroup = [
        {
            btnId: "btnEdithelthy",
            btnIcon: "fa fa-exclamation-circle",
            btnText: "修改正規化地址",
            btnFunc: function () {
                var ordHtml = "";
                    var rows = $("#jqxGrid_2").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid_2").jqxGrid('getrowdata', rows[m]);
                        if (typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if (ids.length == 0) {
                            swal("{{ trans('common.msg1') }}", "", "warning");
                        return;
                    }

                    (async function getFormValues () {
                    
                        var optinon = "";
                        var first = "";
                        @foreach($formatedata as $row)
                        optinon =optinon+"<option value='{{$row->id}}'>"+"({{$row->formate_name}}"+")"+"{{$row->formate_address}}"+"</option>"
                        @endforeach
                        const {value: formValues} = await swal({
                        title: '加入',
                        html:
                            '<select class="form-control select2" id="swal-input1" name="status">' +
                                optinon+
                            '</select>',
                        focusConfirm: false,
                        focusConfirm: false,
                        showCancelButton: true,
                        cancelButtonText:  '取消',
                        confirmButtonText: '確定',
                        onOpen: function () {
                            $('#swal-input1').select2();
                        },
                        preConfirm: () => {
                            return [
                            document.getElementById('swal-input1').value,
                            ]
                        }
                        })
                        if (formValues) {
                            var belong_cd = document.getElementById('swal-input1').value;
                            $.post(BASE_URL +"/zh-TW" +"/addressformate/insertdetail", {belong_cd:belong_cd, ids: ids}, function(data){
                                if(data.success) {
                                    swal("完成", "", "success");
                                    $('#jqxGrid_2').jqxGrid('updatebounddata');
                                    $("#jqxGrid_2").jqxGrid('clearselection');
                                    
                                }
                            });
                        }
                    })()

            }
        },
        @can('ADDRESSFORMATE-DELETE')
        {
            btnId: "btnDelete",
            btnIcon: "fa fa-trash-o",
            btnText: btnDelete,
            btnFunc: function() {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                if(focus == "2" ) {
                    rows =   $("#jqxGrid_2").jqxGrid('selectedrowindexes');
                }
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(focus == "2" ) {
                        row = $("#jqxGrid_2").jqxGrid('getrowdata', rows[m]);
                    }
                    if (typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if (ids.length == 0) {
                    swal(msg1, "", "warning");
                    return;
                }

                swal({
                    title: msgnew2,
                    text: msgnew3,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: confirmButtonText,
                    cancelButtonText: cancelButtonText
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/' + lang + '/' + fileName + '/batchDelete', {
                            'ids': ids
                        }, function(data) {
                            if (data.success) {
                                swal(msgnew4, "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                                $("#jqxGrid_2").jqxGrid('updatebounddata');
                                $("#jqxGrid_2").jqxGrid('clearselection');
                            } else {
                                swal(msgnew5, '', "error");
                            }
                        });

                    } else {

                    }
                });

            }
        },
        @endcan
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                if(focus=="1"){
                    $('#gridOptModal').modal('show');
                }else if(focus=="2"){
                    $('#gridOptModal2').modal('show');
                }else if(focus=="3"){
                    $('#gridOptModal3').modal('show');
                }
               
            }
        },
        //
        {
            btnId: "btnExportExcelexist",
            btnIcon: "fa fa-cloud-download",
            btnText: 'Excel匯出',
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var filterGroups = $('#jqxGrid_3').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                var filtercolumn = [];
                var pageNum = 1;
                var pageSize = 50;
                var rows = $("#jqxGrid_3").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid_3").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                var anotherCondition = typeof sqlconditiondetail !== 'undefined' ? sqlconditiondetail : [];
                if(ids.length == 0) {
                    var pageNum = $('#jqxGrid_3').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                    var pageSize = $('#jqxGrid_3').jqxGrid('getdatainformation').paginginformation.pagesize;
                    for (var i = 0; i < filterGroups.length; i++) {
                        var filterGroup = filterGroups[i];
                        var filters = filterGroup.filter.getfilters();
                        for (var k = 0; k < filters.length; k++) {
                            if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                // new Date(unix_timestamp * 1000)
                                filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                            } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                            } else if(filters[k].condition == "contains" ) {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            } else {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            }
                            // filtercolumndata.push(filtercolumn);
                        }
                    }
                } else {
                    filtercolumn[m] = ['id', `in`, ids];
                }
                var sortby = [];
                var sortinformation = $('#jqxGrid_3').jqxGrid('getsortinformation');
                if(sortinformation.sortcolumn != null ) {
                    var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                    var obj = {};
                    obj["sortField"] = sortinformation.sortcolumn;
                    obj["sortValue"] = sortvalue;
                    sortby.push(obj);
                    // sortby.push([ sortinformation.sortcolumn,  tpye]);
                }
                $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                    'table'           : 'mod_address_formate_detail_view',
                    'fileName'        : fileName,
                    'baseCondition'   : filtercolumn,
                    'anotherCondition': null,
                    'pageNum'         : pageNum,
                    'pageSize'        : pageSize,
                    'sort'            : sortby,
                    'header'          : enabledheaderdetail,
                }, function(data){
                    if(data.message == "success") {
                        window.open(data.donwloadLink);
                    }

                });
            }
        },
        {
            btnId: "btnExportExceldetail",
            btnIcon: "fa fa-cloud-download",
            btnText: 'Excel匯出',
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var filterGroups = $('#jqxGrid_2').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                var filtercolumn = [];
                var pageNum = 1;
                var pageSize = 50;
                var rows = $("#jqxGrid_2").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid_2").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                var anotherCondition = typeof sqlconditiondetail !== 'undefined' ? sqlconditiondetail : [];
                if(ids.length == 0) {
                    var pageNum = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                    var pageSize = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagesize;
                    for (var i = 0; i < filterGroups.length; i++) {
                        var filterGroup = filterGroups[i];
                        var filters = filterGroup.filter.getfilters();
                        for (var k = 0; k < filters.length; k++) {
                            if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                // new Date(unix_timestamp * 1000)
                                filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                            } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                            } else if(filters[k].condition == "contains" ) {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            } else {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            }
                            // filtercolumndata.push(filtercolumn);
                        }
                    }
                } else {
                    filtercolumn[m] = ['id', `in`, ids];
                }
                var sortby = [];
                var sortinformation = $('#jqxGrid_2').jqxGrid('getsortinformation');
                if(sortinformation.sortcolumn != null ) {
                    var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                    var obj = {};
                    obj["sortField"] = sortinformation.sortcolumn;
                    obj["sortValue"] = sortvalue;
                    sortby.push(obj);
                    // sortby.push([ sortinformation.sortcolumn,  tpye]);
                }
                $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                    'table'           : tableName,
                    'fileName'        : fileName,
                    'baseCondition'   : filtercolumn,
                    'anotherCondition': anotherCondition,
                    'pageNum'         : pageNum,
                    'pageSize'        : pageSize,
                    'sort'            : sortby,
                    'header'          : enabledheaderdetail,
                }, function(data){
                    if(data.message == "success") {
                        window.open(data.donwloadLink);
                    }

                });
            }
        },
        //
        
    ]

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row" id="mainForm">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li name="prodDetail" class="active">
                <a onclick="tab_1()" href="#tab_1" data-toggle="tab" aria-expanded="false">已存在</a>
            </li>
            <li name="detail">
                <a onclick="tab_3()" href="#tab_3" data-toggle="tab" aria-expanded="false">已存在(明細)</a>
            </li>
            <li name="prodGift">
                <a onclick="tab_2()" href="#tab_2" data-toggle="tab" aria-expanded="false">未存在</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="griddiv">
                        <div id="jqxGrid"></div>
                        <div id="jqxGrid_3"></div>
                        <div id="jqxGrid_2"></div>
                    </div>
                    <input type="button" style="display:none" id="updategridsign" >
                    <input type="button" style="display:none" id="updategriderror" >
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid2">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid2">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal3">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox3"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid2">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid2">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>

<script>
    $(document).ready(function() {
        // 在這撰寫javascript程式碼
        $("[href='#tab_1']").click();   
    });
    function tab_1() {
        focus = "1";
        $("#btnExportExcel").show();
        $("#btnExportExceldetail").hide();
        $("#btnEdithelthy").hide();
        $("#btnExportExcelexist").hide();
        $("#btnAdd").show();
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid_2').style.display = 'none';
        document.getElementById('jqxGrid_3').style.display = 'none';
    }
    function tab_2() {
        focus = "2";
        $("#btnEdithelthy").show();
        $("#btnExportExcel").hide();
        $("#btnExportExceldetail").show();
        $("#btnExportExcelexist").hide();
        $("#btnAdd").hide();
        $('#jqxGrid_2').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_2"></div>')
        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'block';
        document.getElementById('jqxGrid_3').style.display = 'none';
        ///


        var sourcedetail = {
            datatype: "json",
            datafields: JSON.parse($("#datafields").val()),
            root: "Rows",
            pagenum: 0,
            beforeprocessing: function(data) {
                sourcedetail.totalrecords = data[0].TotalRows;
                if (data[0].StatusCount.length > 0) {
                    data[0].StatusCount.push({
                        'count': data[0].TotalRows,
                        'status': 'ALL',
                        'statustext': 'ALL'
                    });
                }
            },
            filter: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'sort');
            },
            cache: false,
            pagesize: 50,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_address_formate/false') }}"+"?basecon=is_exist;EQUAL;N;"
        }

        var dataAdapterdetail = new $.jqx.dataAdapter(sourcedetail, {
            async: false,
            loadError: function(xhr, status, error) {
                alert('Error loading "' + sourcedetail.url + '" : ' + error);
            },
            loadComplete: function() {

            }
        });

        var winHeight2 = $(window).height() - 300;
        $("#jqxGrid_2").jqxGrid({
            width: '100%',
            height: winHeight2,
            source: dataAdapterdetail,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions:[50, 100, 500, 2000,9999],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
                loadState2();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: JSON.parse($("#columns").val())
        });
        $("#jqxGrid_2").jqxGrid('updatebounddata');


        function loadState2() {
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: 'un_mod_address_formate',
                    lang: 'zh-TW'
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid_2" ).jqxGrid('loadstate', response);
                    }

                    var listSource2 = [];
                    state2 = $("#jqxGrid_2").jqxGrid('getstate');
                    console.log(state2);
                    $('#jqxLoader').jqxLoader('close');

                    $.get(BASE_URL + "/searchList/get/" + "un_mod_address_formate", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName2']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName2']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" +"un_mod_address_formate/"+ $("select[name='selSearchName2']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent2").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state2);
                                        initSearchWindow2(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow2').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 200,
                                        minWidth: 200,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow2').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd2']").on("click", function() {
                                        var seasrchTpl = getSearchTpl2(state2);
                                        $("#searchContent2").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove2']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj2[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent2[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn2']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField2[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent2[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp2[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter2(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state2.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource2.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheaderdetail.push(headerdata);
                            }
                        }
                    });
                    console.log(listSource2);
                    $("#jqxlistbox2").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource2,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });
        }
        function initSearchWindow2(searchTpl) {
            $("#searchContent2").append(searchTpl);
        }
        $("#saveGrid2").on("click", function() {
            var items = $("#jqxlistbox2").jqxListBox('getItems');
            var gridname = "jqxGrid_2";
            var savekey = "un_mod_address_formate";
            // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
            enabledheaderdetail    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                }
                if (item.checked) {
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheaderdetail.push(headerdata);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'un_mod_address_formate',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("save successful");
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid2").on("click", function() { 
            savekey = "un_mod_address_formate";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });

        // $("#jqxGrid_2").on("rowdoubleclick", function(event) {
        //     // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
        //     var args = event.args;
        //     // row's bound index.
        //     var boundIndex = args.rowindex;
        //     // row's visible index.
        //     var visibleIndex = args.visibleindex;
        //     // right click.
        //     var rightclick = args.rightclick;
        //     // original event.
        //     var ev = args.originalEvent;
        //     if(typeof toedit !== "undefined" && toedit == "false") {
        //         return;
        //     }
        //     var datarow = $("#jqxGrid_2").jqxGrid('getrowdata', boundIndex);
        //     console.log(datarow);

        //     // status
        //     if(typeof customdirect !== "undefined") {
        //         var editUrl = BASE_URL  + "/" +lang +"/"+customdirect+"/" + datarow.id ;
        //         window.open(editUrl);
        //         return;
        //     }

        //     var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
        //     //location.href = editUrl;
        //     window.open(editUrl);
        // })

    }
    function tab_3() {
        focus = "3";
        $("#btnEdithelthy").show();
        $("#btnExportExcel").hide();
        $("#btnExportExceldetail").hide();
        $("#btnExportExcelexist").show();
        $("#btnAdd").hide();
        $('#jqxGrid_3').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_3"></div>')
        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'none';
        document.getElementById('jqxGrid_3').style.display = 'block';
        ///

        var datafieldsthird = [
            { name: 'id', type: 'number' },
            { name: 'replace_address', type: 'string' },
            { name: 'cust_name', type: 'string' }, 
            { name: 'cust_addr', type: 'string' }, 
        ];
        var sourcethird = {
            datatype: "json",
            datafields: datafieldsthird,
            root: "Rows",
            pagenum: 0,
            beforeprocessing: function(data) {
                sourcethird.totalrecords = data[0].TotalRows;
                if (data[0].StatusCount.length > 0) {
                    data[0].StatusCount.push({
                        'count': data[0].TotalRows,
                        'status': 'ALL',
                        'statustext': 'ALL'
                    });
                }
            },

            //ids

            filter: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_3").jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_3").jqxGrid('updatebounddata', 'sort');
            },

            cache: false,
            pagesize: 50,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_address_formate/false') }}"
        }

        var dataAdapterthird = new $.jqx.dataAdapter(sourcethird, {
            async: false,
            loadError: function(xhr, status, error) {
                alert('Error loading "' + sourcethird.url + '" : ' + error);
            },
            loadComplete: function() {

            }
        });

        var winHeight2 = $(window).height() - 300;
        $("#jqxGrid_3").jqxGrid({
            width: '100%',
            height: winHeight2,
            source: dataAdapterthird,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions:[50, 100, 500, 2000,9999],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
                loadState2();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: [
                {text: "id", datafield: "id", width: 100, hidden: true},
                {text: "索引地址", datafield: "replace_address", width: 450},
                {text: "提配送客戶名稱", datafield: "cust_name", width: 450},
                {text: "客戶地址", datafield: "cust_addr", width: 450},
            ]
        });

        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ischeck = "N";
        var mainids = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                mainids.push(row.id);
                ischeck = "Y";
            }
        }
        var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
        if ( ischeck == "N") {
            if(filterGroups.length > 0 ) {
                var allrows = $('#jqxGrid').jqxGrid('getrows');
                for(var i = 0; i < allrows.length; i++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', i);
                    mainids.push(row.id);
                }
                sourcethird.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_address_formate_detail_view/false') }}"+"?basecon=main_id;IN;"+JSON.stringify(mainids); 
            } else {
                sourcethird.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_address_formate_detail_view/false') }}";
            }

        } else {
            sourcethird.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_address_formate_detail_view/false') }}"+"?basecon=main_id;IN;"+JSON.stringify(mainids); 
        }


        $("#jqxGrid_3").jqxGrid('updatebounddata');


        function loadState2() {
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: 'ex_mod_address_formate_detail',
                    lang: 'zh-TW'
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid_3" ).jqxGrid('loadstate', response);
                    }

                    var listSource3 = [];
                    state3 = $("#jqxGrid_3").jqxGrid('getstate');
                    console.log(state3);
                    $('#jqxLoader').jqxLoader('close');

                    $.get(BASE_URL + "/searchList/get/" + "ex_mod_address_formate_detail", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName3']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName3']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" +"ex_mod_address_formate_detail/"+ $("select[name='selSearchName3']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent2").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state3);
                                        initSearchWindow3(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow3').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 200,
                                        minWidth: 200,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow3').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd3']").on("click", function() {
                                        var seasrchTpl = getSearchTpl2(state3);
                                        $("#searchContent2").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove3']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField3[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj3[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp3[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent3[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn3']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField3[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent3[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp3[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter3(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state3.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource3.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheaderdetail.push(headerdata);
                            }
                        }
                    });
                    console.log(listSource3);
                    $("#jqxlistbox3").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource3,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });
        }
        function initSearchWindow3(searchTpl) {
            $("#searchContent3").append(searchTpl);
        }
        $("#saveGrid3").on("click", function() {
            var items = $("#jqxlistbox3").jqxListBox('getItems');
            var gridname = "jqxGrid_3";
            var savekey = "ex_mod_address_formate_detail";
            // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
            enabledheaderdetail    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                }
                if (item.checked) {
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheaderdetail.push(headerdata);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'ex_mod_address_formate_detail',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("save successful");
                        $('#gridOptModal3').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid3").on("click", function() { 
            savekey = "ex_mod_address_formate_detail";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal3').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });

    }
</script>
@endsection