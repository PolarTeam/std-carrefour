@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_address_formate.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/addressformate') }}">{{ trans('mod_address_formate.titleName') }}</a></li>
		<li class="active">{{ trans('mod_address_formate.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 

        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">

                                    <div class="form-group col-md-2">
                                        <label for="formate_name">{{ trans('mod_address_formate.formate_name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="formate_name" name="formate_name">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="formate_address">{{ trans('mod_address_formate.formate_address') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="formate_address" name="formate_address">
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="high_risk">{{ trans('sys_customers.high_risk') }} <span style="color:red">*<span></label>
                                        <select class="form-control" id="high_risk" name="high_risk">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="route_type">{{ trans('mod_address_formate.route_type') }} </label>
                                        <input type="text" class="form-control" id="route_type" name="route_type">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="route_name">{{ trans('mod_address_formate.route_name') }} </label>
                                        <input type="text" class="form-control" id="route_name" name="route_name">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="lng">{{ trans('mod_address_formate.lng') }}</label>
                                        <input type="text" class="form-control" id="lng" name="lng">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="lat">{{ trans('mod_address_formate.lat') }}</label>
                                        <input type="text" class="form-control" id="lat" name="lat">
                                    </div>
                                </div>


                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>

<div class="row">

	<div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('bscode_kind.detail') }}</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">明細</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="replace_address">索引地址</label>
                                    <input type="text" class="form-control input-sm" name="replace_address" grid="true" >
                                </div>                                  
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">儲存</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">取消</button>
                        </div>
                        </button>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
	</div>
	
</div>


@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    // $('#myForm button[btnName="wh_name"]').on('click', function(){
    //     $('#lookupModal').modal('show');
    //     initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
    //         console.log(data);
    //     });
    // });

    // $('#myForm input[name="wh_name"]').on('click', function(){
    //     var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
    //     if(check == false) {
    //         initAutocomplete("myForm","wh_name",callBackFunc=function(data){
    //             console.log(data);
                
    //         },"wh_name");
    //     }
    // });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/addressformate" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#owner_cd').select2();
            $('#parked_models').select2();
            $('#container').select2();

        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            // var sel = document.getElementById("is_enabled");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#is_enabled_desc').val(text);

            // var sel = document.getElementById("type");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#type_desc').val(text);

            var requiredColumn = [
                {
                    "column_filed":"formate_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"formate_address",
                    "column_type":"string",
                },
                {
                    "column_filed":"high_risk",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        
        if(mainId != "") {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "replace_address", type: "string"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "索引地址", datafield: "replace_address", width: 450},
                ]
            ];
            var opt                = {};
            opt.gridId         = "jqxGrid";
            opt.fieldData      = col;
            opt.formId         = "subForm";
            opt.saveId         = "Save";
            opt.cancelId       = "Cancel";
            opt.showBoxId      = "subBox";
            opt.height         = 300;
            opt.getUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/addressformateDetail/" + mainId;
            opt.addUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/addressformateDetail/store";
            opt.updateUrl      = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/addressformateDetail/update/"+  mainId;
            opt.delUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/addressformateDetail/delete/";
            opt.defaultKey     = {'main_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn      = true;
            opt.inlineEdit     = false;
            opt.showtoolbar    = true;
            opt.selectionmode  = 'singlerow';
            opt.beforeSave     = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
                if(!data.success) {
                   swal("{{trans('common.warning')}}", data.message, "warning");
                }
                $("#jqxGrid").jqxGrid('updatebounddata');
            }

            genDetailGrid(opt);
        }


    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection