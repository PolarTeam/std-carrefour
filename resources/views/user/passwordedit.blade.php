@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('users.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/user') }}">{{ trans('users.titleName') }}</a></li>
		<li class="active">{{ trans('users.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" class="form-control" id="session_id" name="session_id">
                                        <label for="name">{{ trans('users.name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" required="required" disabled="disabled" readonly="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('users.email') }}</label>
                                        <input type="text" class="form-control" id="email" name="email" required="required" disabled="disabled" readonly="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password">密碼</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password_confirmation">密碼確認</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>


@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId    = "";
    var lang      = "en";
    var cust_no   = "";
    var editData  = null;
    var editObj   = null;
    var fieldData = null;
    var fieldObj  = null;
    var oldwarehouse   = "";

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/useredit"
    // var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/useredit/create"
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+"/userupdatepassword"
    // var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/useredit"
   
    function updateManageWarehouse() {
        // $new 
        var warehouse = $("#manage_warehouse").val();
        var whArray = [];
        $("#check_wh option:selected").each(function (index, sel) {
            whArray.push(sel.value);
        });
        whArray.push(warehouse);
        $('#check_wh').val(whArray);
        $('#check_wh').trigger('change'); 
    }


    $(function(){
        $('#manage_warehouse').on('change', function(){
            updateManageWarehouse();
        });

        $('#myForm button[btnName="manage_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('manage_name', "管理者搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="manage_name"]').on('click', function(){
            var check = $('#subBox input[name="manage_name"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","manage_name",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_name");
            }
        });

        $('#myForm button[btnName="dep"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('dep', "部門搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="dep"]').on('click', function(){
            var check = $('#subBox input[name="dep"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","dep",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_depname");
            }
        });

        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/usereditshow/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/usereditshow/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/userupdatepassword";
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#role').select2();
            $('#check_wh').select2();
            $('#check_owner').select2();
        };
        formOpt.addFunc = function() {
            $('#role').val();
            $('#role').trigger('change');
            
            $('#check_wh').val();
            $('#check_wh').trigger('change');

            $('#check_owner').val();
            $('#check_owner').trigger('change');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;


            var checkpassword = $('#password').val();
            var regex = new RegExp('^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,15}');
            if(!regex.test(checkpassword)) {
                swal("{{trans('common.warning')}}", "密碼請包含大小寫英文字母數字長度超過6碼", "warning");
                return false;
            }
            

            if($('#password').val() != "" ) {
                if( $('#password').val() !=  $('#password_confirmation').val()) {
                    swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
                    return false;
                }
            }
            if( mainId == "" && $('#password').val() == "") {
                document.getElementById("password").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if(iserror) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }


            
            return true;
        }
        formOpt.saveSuccessFunc = function (){
            location.href = BASE_URL  + "/login" ;
            //console.log("todo custom func for save success");
        };
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


    })
</script> 
<script>
$(function(){

});
</script>
@endsection