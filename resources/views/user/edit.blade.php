@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('users.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/user') }}">{{ trans('users.titleName') }}</a></li>
		<li class="active">{{ trans('users.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" class="form-control" id="session_id" name="session_id">
                                        <label for="name">{{ trans('users.name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="name" name="name" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('users.email') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="email" name="email" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password">密碼 <span style="color:red">*<span></label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password_confirmation">密碼確認</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('users.check_wh_name') }} <span style="color:red">*<span></label>
                                        <input type="hidden" name="check_wh_name" id = "check_wh_name" class="form-control">
                                        <select class="form-control select2" id="check_wh" name="check_wh" multiple="multiple">
                                            @foreach($check_wh as $row)
                                            <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('users.delivery_wh_name') }}</label>
                                        <input type="hidden" name="delivery_wh_name" id = "delivery_wh_name" class="form-control">
                                        <select class="form-control select2" id="delivery_wh" name="delivery_wh" multiple="multiple">
                                            @foreach($check_wh as $row)
                                            <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('users.check_owner_name') }} <span style="color:red">*<span></label>
                                        <input type="hidden" name="check_owner_name" id = "check_owner_name" class="form-control">
                                        <select class="form-control select2" id="check_owner" name="check_owner" multiple="multiple">
                                            <option value="*">ALL</option>
                                            @foreach($check_owner as $row)
                                            <option value="{{$row->group_cd}}">{{$row->group_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('users.role') }} <span style="color:red">*<span></label>
                                        <select class="form-control select2" id="role" name="role" multiple="multiple">
                                            @foreach($role as $row)
                                            <option value="{{$row->name}}">{{$row->display_name}}</option>
                                            @endforeach	
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="isEnabled">{{ trans('users.account_enable_desc') }}</label>
                                        <input type="hidden" name="account_enable_desc" id = "account_enable_desc" class="form-control">
                                        <select class="form-control" id="account_enable" name="account_enable">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sys_area.remark') }}</label>
                                            <textarea class="form-control" rows="3" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>


@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId    = "";
    var lang      = "en";
    var cust_no   = "";
    var editData  = null;
    var editObj   = null;
    var fieldData = null;
    var fieldObj  = null;
    var oldwarehouse   = "";

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/create"
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
   
    function updateManageWarehouse() {
        // $new 
        var warehouse = $("#manage_warehouse").val();
        var whArray = [];
        $("#check_wh option:selected").each(function (index, sel) {
            whArray.push(sel.value);
        });
        whArray.push(warehouse);
        $('#check_wh').val(whArray);
        $('#check_wh').trigger('change'); 
    }


    $(function(){
        $('#manage_warehouse').on('change', function(){
            updateManageWarehouse();
        });

        $('#myForm button[btnName="manage_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('manage_name', "管理者搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="manage_name"]').on('click', function(){
            var check = $('#subBox input[name="manage_name"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","manage_name",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_name");
            }
        });

        $('#myForm button[btnName="dep"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('dep', "部門搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="dep"]').on('click', function(){
            var check = $('#subBox input[name="dep"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","dep",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_depname");
            }
        });

        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user";
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#role').select2();
            $('#check_wh').select2();
            $('#delivery_wh').select2();
            $('#check_owner').select2();
        };
        formOpt.addFunc = function() {
            $('#account_enable').val('Y');
            $('#role').val();
            $('#role').trigger('change');
            
            $('#check_wh').val();
            $('#check_wh').trigger('change');

            $('#delivery_wh').val();
            $('#delivery_wh').trigger('change');

            $('#check_owner').val();
            $('#check_owner').trigger('change');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            // $('#check_owner_name').val($("#check_owner option:selected").text()); 
            $('#account_enable_desc').val($("#account_enable option:selected").text());
            // $('#manage_warehouse_name').val($("#manage_warehouse option:selected").text());
            if($('#name').val() == "") {
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("name").style.backgroundColor = "";
            }

            if($('#email').val() == "") {
                document.getElementById("email").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("email").style.backgroundColor = "";
            }

            if($('#check_wh').val() == "" || $('#check_wh').val() == null) {
                document.getElementsByClassName('select2-selection__rendered')[0].style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementsByClassName('select2-selection__rendered')[0].style.backgroundColor = "";
            }

            if($('#check_owner').val() == "" || $('#check_owner').val() == null) {
                document.getElementsByClassName('select2-selection__rendered')[1].style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementsByClassName('select2-selection__rendered')[1].style.backgroundColor = "";
            }

            if($('#role').val() == "" || $('#role').val() == null) {
                document.getElementsByClassName('select2-selection__rendered')[2].style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementsByClassName('select2-selection__rendered')[2].style.backgroundColor = "";
            }

            var password = document.getElementById('password').value;
            if(password != null && password !='') {
                var newpassword = document.getElementById('password').value;
                var newpasswordconfirm = document.getElementById('password_confirmation').value;
                var regex = new RegExp('^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#%&]).{8,15}');
                if( newpassword != newpasswordconfirm) {
                    swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
                    return false;
                }

                if(!regex.test(newpassword)) {
                    swal("{{trans('common.warning')}}", "密碼請包含大小寫英文字母特殊符號數字 長度超過8碼", "warning");
                    return false;
                }
            }



            // 可查看倉庫、可查看貨主、角色為必輸，要有粉紅底
            // if($('#g_key').val() == "") {
            //     document.getElementById("g_key").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // } else {
            //     document.getElementById("g_key").style.backgroundColor = "";
            // }

            // if($('#c_key').val() == "") {
            //     document.getElementById("c_key").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // } else {
            //     document.getElementById("c_key").style.backgroundColor = "";
            // }

            // if($('#s_key').val() == "") {
            //     document.getElementById("s_key").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // } else {
            //     document.getElementById("s_key").style.backgroundColor = "";
            // }

            // if($('#d_key').val() == "") {
            //     document.getElementById("d_key").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // } else {
            //     document.getElementById("d_key").style.backgroundColor = "";
            // }


            if($('#password').val() != "" ) {
                if( $('#password').val() !=  $('#password_confirmation').val()) {
                    swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
                    return false;
                }
            }
            if( mainId == "" && $('#password').val() == "") {
                document.getElementById("password").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if(iserror) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }

            if($('#role').val() == null) {
                swal("{{trans('common.warning')}}", "{{trans('common.rolenotempty')}}", "warning");
                return false;
            }

            if($('#check_wh').val() == null) {
                swal("{{trans('common.warning')}}", "可查看倉庫必輸", "warning");
                return false;
            }

            if($('#check_owner').val() == null) {
                swal("{{trans('common.warning')}}", "可查看貨主", "warning");
                return false;
            }
            
            return true;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


    })
</script> 
<script>
$(function(){

});
</script>
@endsection