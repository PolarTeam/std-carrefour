@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        司機建檔<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/carProfile') }}">司機建檔</a></li>
		<li class="active">司機建檔</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('modCar.custNo') }}</label>
                                        <input type="hidden" class="form-control" id="cname" name="cname" placeholder="Enter No" >
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="cust_no"
                                                    info1="{{Crypt::encrypt('mod_car_trader')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cust_name,cust_no,cust_name")}}" 
                                                    info3="{{Crypt::encrypt("")}}"
                                                    info5="{{Crypt::encrypt('mod_car_trader')}}"
                                                    info4="cust_no=cust_no;cname=cname;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="car_no">{{ trans('modCar.carNo') }}</label>
                                        <input type="text" class="form-control" id="car_no" name="car_no" placeholder="Enter No.">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>{{ trans('modCar.carType') }}</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="car_type[]">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="car_ton">{{ trans('modCar.carTon') }}</label>
                                        <input type="text" class="form-control" id="car_ton" name="car_ton" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="load_weight">{{ trans('modCar.loadWeight') }}</label>
                                        <input type="number" class="form-control" id="load_weight" name="load_weight" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="driver_no">{{ trans('modCar.driverNo') }}</label>
                                        <input type="text" class="form-control" id="driver_no" name="driver_no" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="driver_nm">{{ trans('modCar.driverNm') }}</label>
                                        <input type="text" class="form-control" id="driver_nm" name="driver_nm" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('modCar.phone') }}</label>
                                        <input type="text" class="form-control" id="phone" name="phone" >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cbm">{{ trans('modCar.cbm') }}</label>
                                        <input type="number" class="form-control" id="cbm" name="cbm" min="0.01" max="9999999999999999.99">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbmu">{{ trans('modCar.cbmu') }}</label>
                                        <input type="text" class="form-control" id="cbmu" name="cbmu" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="load_rate">{{ trans('modCar.loadRate') }}</label>
                                        <input type="number" class="form-control" id="load_rate" name="load_rate" min="0.01" max="100.00">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('modCar.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('modCar.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('modCar.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('modCar.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="cust_no"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('cust_no', "車商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="cust_no"]').on('click', function(){
        var check = $('#subBox input[name="cust_no"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","cust_no",callBackFunc=function(data){
                console.log(data);
            },"cust_no");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carProfile" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#carProfile').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            if($('#display_name').val() == ""){
                document.getElementById("display_name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if($('#name').val() == ""){
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            var requiredColumn = [

            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection