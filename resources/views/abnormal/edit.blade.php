@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_abnormal.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_abnormal') }}">{{ trans('mod_abnormal.titleName') }}</a></li>
		<li class="active">{{ trans('mod_abnormal.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="error_type">{{ trans('mod_abnormal.error_cd') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="error_cd"  name="error_cd">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="error_name">{{ trans('mod_abnormal.error_name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="error_name"  name="error_name">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">所屬項目</label>
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="belong_cd" name="belong_cd">
                                            <input type="text" class="form-control" id="belong_name" name="belong_name">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="belong_name"  grid="true"
                                                    info1="{{Crypt::encrypt('mod_abnormal')}}" 
                                                    info2="{{Crypt::encrypt('error_cd+error_name,error_cd,error_name')}}" 
                                                    info3="{{Crypt::encrypt("type='type'")}}"
                                                    info5="{{Crypt::encrypt('mod_abnormal')}}"
                                                    info4="error_cd=belong_cd;error_name=belong_name;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                        {{-- <input type="hidden" class="form-control" id="belong_cd" name="belong_cd">
                                        <input type="text" class="form-control" id="belong_name" name="belong_name"> --}}
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="type">類別</label>
                                        <input type="hidden" class="form-control" id="type_desc" name="type_desc">
                                        <select class="form-control" id="type" name="type">
                                                <option value="type">類別</option>
                                                <option value="reason">原因</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="seq">排序</label>
                                        <input type="number"  min="0" class="form-control" id="seq" name="seq">
                                    </div>
    
                                    <div class="form-group col-md-3">
                                        <label for="is_enabled">啟用</label>
                                        <input type="hidden" class="form-control" id="is_enabled_desc" name="is_enabled_desc">
                                        <select class="form-control" id="is_enabled" name="is_enabled">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="showonpick">顯示 <span style="color:red">*<span></label>
                                        <select class="form-control" id="showonpick" name="showonpick">
                                            <option value="PICK">提</option>
                                            <option value="DLV">配</option>
                                            <option value="ALL">提配</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="showonpick">{{ trans('mod_abnormal.finish_mission') }} <span style="color:red">*<span></label>
                                        <select class="form-control" id="finish_mission" name="finish_mission">
                                            <option value="N">否</option>
                                            <option value="Y">是</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                  <div class="form-group col-md-3">
                                      <label for="created_by">{{ trans('mod_abnormal.created_by') }}</label>
                                      <input type="text" class="form-control" id="created_by" name="created_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="created_at">{{ trans('mod_abnormal.created_at') }}</label>
                                      <input type="text" class="form-control" id="created_at" name="created_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_by">{{ trans('mod_abnormal.updated_by') }}</label>
                                      <input type="text" class="form-control" id="updated_by" name="updated_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_at">{{ trans('mod_abnormal.updated_at') }}</label>
                                      <input type="text" class="form-control" id="updated_at" name="updated_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                              </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cdType     = "";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
        cdType = "{{$error_type}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/abnormal" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#mod_abnormal').select2();
            
        };

        formOpt.addFunc = function() {
            $('#is_enabled').val('Y');
            $('#finish_mission').val('N');
        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"showonpick",
                    "column_type":"string",
                },
                {
                    "column_filed":"error_cd",
                    "column_type":"string",
                },
                {
                    "column_filed":"error_name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            $('#type_desc').val($("#type option:selected").text());
            $('#is_enabled_desc').val($("#is_enabled option:selected").text());
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


        $('#myForm button[btnName="belong_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('belong_name', "類別查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="belong_name"]').on('click', function(){
            var check = $('#subBox input[name="belong_name"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","belong_name",callBackFunc=function(data){
                    console.log(data);
                    
                },"belong_name");
            }
        });





    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection