@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('language.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/language') }}">{{trans('language.titleName')}}</a></li>
		<li class="active">{{trans('language.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="func_name">{{trans('language.funcName')}}</label>
                                        <input type="text" class="form-control" id="func_name" name="func_name" required="required" readonly="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="en_name">{{trans('language.enName')}}</label>
                                        <input type="text" class="form-control" id="en_name" name="en_name" required="required" readonly="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="tw_name">{{trans('language.twName')}}</label>
                                        <input type="text" class="form-control" id="tw_name" name="tw_name" required="required" readonly="true">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <div class="row">  
                                
                                </div>
                                @foreach($detailColumn as $row)
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="{{$row->field_name}}">{{trans('language.funcName')}}</label>
                                            <input type="text" class="form-control" id="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}" name="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}" required="required" readonly="true" value = "{{$row->field_name}}" >
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="{{$row->field_name}}">{{trans('language.enName')}}</label>
                                            <input type="text" class="form-control" id="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}-en" name="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}-en" required="required" value = "{{$row->en_name}}">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="{{$row->field_name}}">{{trans('language.twName')}}</label>
                                            <input type="text" class="form-control" id="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}-tw" name="{{$row->id}}-{{$row->func_name}}-{{$row->field_name}}-tw" required="required" value = "{{$row->tw_name}}">
                                        </div>

                                    </div>
                                @endforeach
                                {{-- <option value="{{$row->id}}">{{$row->email}}</option>	 --}}

                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="created_at" class="form-control">
                                        <input type="hidden" name="updated_at"  class="form-control">
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language";
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif
   

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/language" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#role').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var changeData = $('#myForm').serializeArray();

            $.each(OLD_DATA, function(i, v) {
                var valeue = $('#myForm'+' [name="'+i+'"]').val();

                if(valeue == "") {
                    document.getElementById(i).style.backgroundColor = 'FCE8E6';
                    iserror = true;
                }

            });



            if(iserror) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return true;
        }

        

        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
});
</script>
@endsection