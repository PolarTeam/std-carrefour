@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('sys_country.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/project') }}">{{trans('sys_country.titleName')}}</a></li>
		<li class="active">{{trans('sys_country.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">國家建檔</a></li>                    
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="cntry_cd">{{ trans('sys_country.cntry_cd') }}</label>
                                        <input type="text" class="form-control" id="cntry_cd" name="cntry_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cntry_nm">{{ trans('sys_country.cntry_nm') }}</label>
                                        <input type="text" class="form-control" id="cntry_nm" name="cntry_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>                                

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sys_country.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sys_country.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sys_country.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sys_country.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry/batchDelete";

    $(function(){

        $('#myForm button[btnName="zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="zip"]').on('click', function(){
            var check = $('#subBox input[name="zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","zip",callBackFunc=function(data){
                    console.log(data);
                    
                },"zip");
            }
        });


        $('#subBox button[btnName="fee_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('fee_name', "代收款建檔", callBackFunc=function(data){
            });
        });

        $('#subBox button[btnName="goods_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goods_no', "料號建檔", callBackFunc=function(data){
            });
        });


        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysCountry" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#cust_type').select2();

            
        };
        formOpt.addFunc = function() {
            $('#cust_type').val();
            $('#cust_type').trigger('change');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            $('#type').val('OTHER');
        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                // {
                //     "column_filed":"project_name",
                //     "column_type":"string",
                // },
                // {
                //     "column_filed":"manage_by",
                //     "column_type":"string",
                // },
                // {
                //     "column_filed":"manage_name",
                //     "column_type":"string",
                // },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
});
</script>
@endsection