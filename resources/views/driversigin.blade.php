<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->

<head>
  <title>DSS 配送服務系統</title>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="配送服務系統">
  <!-- Meta Keyword -->
  <meta name="keywords" content="DSS">
  <meta name="author" content="">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="{{asset('')}}/fordriver/assets/plugins/bootstrap4/css/bootstrap.min.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- CSS Customization -->
  <link rel="stylesheet" href="{{asset('')}}/fordriver/assets/css/main.css">
</head>

<body>
  <!-- ===== header ===== -->
  <!-- <header id="header">
  </header> -->
  <!-- ===== End header ===== -->
  <div class="wrapper">
    <div class="row">
      <div class="col-lg-6">
          <div class="title-box">
            <div class="logo">
              <a href="#"><img src="{{asset('')}}/fordriver/assets/img/dss_logo.png" alt="dss_logo" class="img-fluid"></a>
            </div>
            <div class="content">
              <div class="text-center">
                <i class="fa fa-truck"></i>
                <h1>DSS 貨車進場報到表</h1>
              </div>
            </div>
            <div class="car"></div>
            <div class="bg-logo"><img src="{{asset('')}}/fordriver/assets/img/bg_logo.svg" alt="dss_logo"></div>
          </div>
      </div>
      
      <div class="col-lg-6">
        <div class="input-box" id="driverSigInForm">
          <div class="row mb-4">
            <div class="col-lg-6 pr-lg-4">
              <div class="w-100">
                <lable class="best-lable">司機電話 <span style="color:red">*<span></lable>
                <input type="text" class="best-input" id="phone">
              </div>
              <div class="w-100">
                <lable class="best-lable">司機姓名 <span style="color:red">*<span></lable>
                <input type="text" class="best-input" id="driver_name">
              </div>
              <div class="w-100">
                <lable class="best-lable">車商 <span style="color:red">*<span></lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="truck_cmp_name">
                </div>
              </div>

              <div class="w-100">
                <lable class="best-lable">車型 <span style="color:red">*<span></lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="car_type">
                  <input type="hidden" id="car_type_str">
                  <div class="autocomplet-search-box" id="autocomplet-search-box-carType">
                  </div>
                </div>
              </div>

              <div class="w-100">
                <lable class="best-lable">車號 <span style="color:red">*<span></lable>
                <input type="text" class="best-input" id="car_no">
              </div>

              <div class="w-100">
                <lable class="best-lable">作業類型 <span style="color:red">*<span></lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="job_type_desc">
                  <input type="hidden" id="job_type_desc_str">
                  <div class="autocomplet-search-box" id="autocomplet-search-box-jobTypeDesc">
                  </div>
                </div>
              </div>

              <div class="w-100">
                <lable class="best-lable">貨主 <span style="color:red">*<span></lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="owner_cd">
                  <input type="hidden" id="owner_cd_str">
                  <div class="autocomplet-search-box" id="autocomplet-search-box-ownerCd">
                  </div>
                </div>
              </div>

            </div>
            <div class="col-lg-6 pl-lg-4">

              <div class="w-100">
                <lable class="best-lable">客戶進貨單號</lable>
                <input type="text" class="best-input" id="cust_ord_no">
              </div>

              <div class="w-100">
                <lable class="best-lable">箱數</lable>
                <input type="number" class="best-input" id="box_num">
              </div>

              <div class="w-100">
                <lable class="best-lable">品項數</lable>
                <input type="number" class="best-input" id="items_num">
              </div>

              <div class="w-100">
                <lable class="best-lable">貨櫃類型</lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="con_type_desc">
                  <input type="hidden" id="con_type_desc_str">
                  <div class="autocomplet-search-box" id="autocomplet-search-box-conTypeDesc">
                  </div>
                </div>
              </div>

              <div class="w-100">
                <lable class="best-lable">下貨類型</lable>
                <div class="autocomplet">
                  <input type="text" class="best-input" id="unloading_type_desc">
                  <input type="hidden" id="unloading_type_desc_str">
                  <div class="autocomplet-search-box" id="autocomplet-search-box-unloadingTypeDesc">
                  </div>
                </div>
              </div>

              <div class="w-100">
                <lable class="best-lable">貨櫃號碼</lable>
                <input type="text" class="best-input" id="con_id">
              </div>
            </div>
          </div>
          <button type="button" class="best-btn w-100" id="driverSigInSubmit">送出</button>
          <p></p>
          <button type="button" class="best-btn w-100" id="driverSearch">查詢</button>
        </div>

        <div class="input-box" id="driverSigInRes">
          <h2>歡迎進入 DSV 倉庫，請至以下位置作業。</h2>
          <div class="w-100">
            <lable class="best-lable mb-2">碼頭代碼</lable>
            <div class="best-border-bottom-value"> <span id ="mark_name"></span></div>
          </div>
          <div class="w-100">
            <lable class="best-lable mb-2">報到倉</lable>
            <div class="best-border-bottom-value"> <span id ="warehouse_name">A倉庫</span></div>
          </div>
          <div class="w-100">
            <lable class="best-lable mb-2">進場時間</lable>
            <div class="best-border-bottom-value"><span id ="entry_time">2022-10-28 14:00:20</span></div>
          </div>
          <div class="mt-5">
            <button type="button" class="best-btn gray-btn w-100" id="driverSigInReturn">回上一步</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ===== footer ===== -->
  <!-- <footer id="footer" class="footer">
  </footer> -->
  <!-- ===== End footer ===== -->
  <!-- /container -->
  <!-- JS Global Compulsory -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{asset('')}}/fordriver/assets/plugins/bootstrap4/js/bootstrap.js"></script>
  <!-- JS Plugins -->
  <!-- 深淺模式 -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/darkmode-js@1.4.0/lib/darkmode-js.min.js"></script> -->
  <!-- END 深淺模式 -->
  <!-- JS Customization -->
  <script src="{{asset('')}}/fordriver/assets/js/best_select.js"></script>
  <script>
    // autocomplet
    function setAutocompletEvent(inputId, searchBoxId, searchItemId, searchLength, condition, dataList, noResultClear) {
      // console.log(inputId);
      document.getElementById(inputId).addEventListener('focus', function (e) {
        if (!e.target.value) {
          let dataListLength = dataList.length;
          let searchItem = '';
          for (let i = 0; i < dataListLength; i++) {
            searchItem += `<button type="button" class="search-item" id="${searchItemId}${i}" data-val="${dataList[i].value}">${dataList[i][condition]}</button>`
          }
          document.getElementById(searchBoxId).innerHTML = searchItem;
          document.getElementById(searchBoxId).style.display = 'block';
          // 設定選項監聽事件
          let itemList = document.getElementById(searchBoxId).children;
          let itemListLength = itemList.length;
          for (let i = 0; i < itemListLength; i++) {
              setAutocompletOptionListener(itemList[i], i, itemListLength, itemList, inputId, searchBoxId);
          }
        } else {
          // 篩選符合的資料
          let list = [];
          let dataListLength = dataList.length;
          for (let i = 0; i < dataListLength; i++) {
              if ((dataList[i][condition].toLowerCase()).indexOf((e.target.value).toLowerCase()) !== -1) {
                  list.push(dataList[i]);
              }
          }
          // 置入選項
          let listLength = list.length;
          let searchItem = '';
          for (let i = 0; i < listLength; i++) {
              searchItem += `<button type="button" class="search-item" id="${searchItemId}${i}" data-val="${list[i].value}">${list[i][condition]}</button>`
          }
          document.getElementById(searchBoxId).innerHTML = searchItem;
          document.getElementById(searchBoxId).style.display = 'block';
          // 設定選項監聽事件
          let itemList = document.getElementById(searchBoxId).children;
          let itemListLength = itemList.length;
          for (let i = 0; i < itemListLength; i++) {
              setAutocompletOptionListener(itemList[i], i, itemListLength, itemList, inputId, searchBoxId);
          }
        }
      })
      document.getElementById(inputId).addEventListener('keydown', function (e) {
          e.stopPropagation();
          // console.log(e.key);
          // console.log(e.keyCode);
          let searchValue;
          if (e.key.length === 1) {
              searchValue = `${e.target.value}${e.key}`;
          }
          if (e.key === 'Enter') {
              searchValue = e.target.value;
          }
          if (e.key === 'Backspace') {
              // 後退刪除，內容少於搜尋內容長度不顯示選單
              searchValue = (e.target.value).substring(0, e.target.value.length - 1);
          }
          // console.log(searchValue);
          if (e.key.length === 1 || e.key === 'Enter' || e.key === 'Backspace') {
              if (searchValue.length >= searchLength) {
                  // 篩選符合的資料
                  let list = [];
                  let dataListLength = dataList.length;
                  for (let i = 0; i < dataListLength; i++) {
                      if ((dataList[i][condition].toLowerCase()).indexOf(searchValue.toLowerCase()) !== -1) {
                          list.push(dataList[i]);
                      }
                  }
                  // 置入選項
                  let listLength = list.length;
                  let searchItem = '';
                  for (let i = 0; i < listLength; i++) {
                      // console.log(list[i]);
                      searchItem += `<button type="button" class="search-item" id="${searchItemId}${i}" data-val="${list[i].value}">${list[i][condition]}</button>`
                  }
                  document.getElementById(searchBoxId).innerHTML = searchItem;
                  document.getElementById(searchBoxId).style.display = 'block';
                  // 設定選項監聽事件
                  let itemList = document.getElementById(searchBoxId).children;
                  let itemListLength = itemList.length;
                  for (let i = 0; i < itemListLength; i++) {
                      setAutocompletOptionListener(itemList[i], i, itemListLength, itemList, inputId, searchBoxId);
                  }
              } else {
                  // 內容少於搜尋內容長度不顯示選單
                  document.getElementById(searchBoxId).style.display = 'none';
                  document.getElementById(searchBoxId).innerHTML = '';
              }
          }
          // 下鍵選擇
          if (e.key === 'ArrowDown' && document.getElementById(searchBoxId).children.length > 0) {
              // console.log(document.getElementById(`${searchItemId}0`));
              e.preventDefault();
              document.getElementById(searchBoxId).children[0].classList.add('focused');
              document.getElementById(searchBoxId).children[0].focus();
              document.getElementById(inputId).blur();
          }
      })
      if (noResultClear) {
          window.addEventListener('click', function(e) {
              // console.log(e.target);
              // console.log(e.target.id);
              let searchValue = document.getElementById(inputId).value;
              let dataListLength = dataList.length;
              let hasResult = false;
              for (let i = 0; i < dataListLength; i++) {
                  if (dataList[i][condition].toLowerCase() === searchValue.toLowerCase()) {
                      hasResult = true
                  }
              }
              if (!hasResult && e.target.id !== searchBoxId && e.target.id !== searchItemId) {
                  document.getElementById(inputId).value = '';
                  document.getElementById(`${inputId}_str`).value = '';
                  document.getElementById(inputId).classList.remove('error');
                  document.getElementById(searchBoxId).style.display = 'none';
                  document.getElementById(searchBoxId).innerHTML = '';
              }
          })
      }
    }
    // 設定 autocomplet 選項監聽事件
    function setAutocompletOptionListener(item, i, itemListLength, itemList, inputId, searchBoxId) {
      // click
      item.addEventListener('click', function (event) {
          document.getElementById(inputId).value = item.textContent;
          // console.log(item.dataset.val);
          document.getElementById(`${inputId}_str`).value = item.dataset.val;
          document.getElementById(inputId).classList.remove('error');
          document.getElementById(searchBoxId).style.display = 'none';
          document.getElementById(searchBoxId).innerHTML = '';
      });
      item.addEventListener('keydown', function (event) {
          event.stopPropagation();
          event.preventDefault();
          // console.log(event.key);
          // 上鍵
          if (event.key === 'ArrowUp') {
              item.blur();
              item.classList.remove('focused');
              if (i === 0) {
                  document.getElementById(inputId).focus();
              } else {
                  itemList[i - 1].focus();
                  itemList[i - 1].classList.add('focused');
              }
          }
          // 下鍵
          if (event.key === 'ArrowDown') {
              if (i != itemListLength - 1) {
                  item.blur();
                  item.classList.remove('focused');
                  itemList[i + 1].focus();
                  itemList[i + 1].classList.add('focused');
              } else {
                  itemList[i - 1].classList.remove('focused');
              }
          }
          // enter
          if (event.key === 'Enter') {
              document.getElementById(inputId).value = item.textContent;
              document.getElementById(inputId).classList.remove('error');
              document.getElementById(searchBoxId).style.display = 'none';
              document.getElementById(searchBoxId).innerHTML = '';
          }
      });
    }
    // 設定關閉 autocomplet 選項
    window.addEventListener("click", function(e) {
      // console.log(e.target)
      // console.log(e.target.classList.value)
      if ((e.target.classList.value).indexOf('best-input') === -1 && (e.target.classList.value).indexOf('search-item') === -1) {
        let searchBoxList = document.querySelectorAll('.autocomplet-search-box');
        // console.log(searchBoxList)
        let searchBoxListLength = searchBoxList.length;
        for (let i = 0; i < searchBoxListLength; i++) {
          searchBoxList[i].style.display = 'none';
        }
      }
    });
    // 貨櫃類型
    const containerRow = JSON.parse(('{{$viewData["container"]}}').split('&quot;').join('"'));
    // console.log(containerRow);
    let dataConTypeDesc = []
    containerRow.forEach((item) => {
      dataConTypeDesc.push(
        {
          value: item.cd,
          str: item.cd_descp
        }
      )
    });
    setAutocompletEvent('con_type_desc', 'autocomplet-search-box-conTypeDesc', 'conTypeDesc-', 1, 'str', dataConTypeDesc);
    // 下貨類型
    const dataUnloadingTypeDesc = [
      {
        value: '人工櫃',
        str: '人工櫃'
      },
      {
        value: '棧板櫃',
        str: '棧板櫃'
      }
    ]
    setAutocompletEvent('unloading_type_desc', 'autocomplet-search-box-unloadingTypeDesc', 'UnloadingTypeDesc-', 1, 'str', dataUnloadingTypeDesc);
    // 作業類型
    const dataJobTypeDesc = [
      {
        value: '進貨',
        str: '進貨'
      },
      {
        value: '拉空櫃',
        str: '拉空櫃'
      }
    ]
    setAutocompletEvent('job_type_desc', 'autocomplet-search-box-jobTypeDesc', 'jobTypeDesc-', 1, 'str', dataJobTypeDesc);
    // 貨主
    const ownerCdRow = JSON.parse(('{{$viewData["customer"]}}').split('&quot;').join('"'));
    let dataOwnerCd = []
    ownerCdRow.forEach((item) => {
      dataOwnerCd.push(
        {
          value: item.cust_no,
          str: item.cust_no + " / " + item.cname
        }
      )
    });
    setAutocompletEvent('owner_cd', 'autocomplet-search-box-ownerCd', 'ownerCd-', 1, 'str', dataOwnerCd);
    // 車型
    const carTypeDescRow = JSON.parse(('{{$viewData["carType"]}}').split('&quot;').join('"'));
    // console.log(carTypeDescRow);
    let dataCarTypeDesc = [];
    carTypeDescRow.forEach((item) => {
      dataCarTypeDesc.push(
        {
          value: item.cd,
          str: item.cd_descp
        }
      )
    });
    setAutocompletEvent('car_type', 'autocomplet-search-box-carType', 'carType-', 1, 'str', dataCarTypeDesc);
  </script>
  <script>
    $("#phone").keydown(function(event) {
        if(event.keyCode == 13){

            console.log('enter car_no');
            $.ajax({
                type: "post", //  OR POST WHATEVER...
                dataType : 'json', // 預期從server接收的資料型態
                contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
                url: "{{ url('') }}" + '/get/finalcardata',
                data:JSON.stringify({ 
                    "phone"             : $("#phone").val(),
                    "_token"             : "{{csrf_token()}}",
                }),
                success: function(data) {
                  console.log(data);
                  $("#driver_name").val(data.driver_name);
                  $("#truck_cmp_no").val(data.truck_cmp_no);
                  $("#truck_cmp_name").val(data.truck_cmp_name);
                  $("#car_type").val(data.car_type);
                  $("#car_type_str").val(data.car_type_desc);
                  $("#car_no").val(data.car_no);
                  $("#owner_cd").val(data.owner_cd + " / " + data.owner_name);
                  $("#owner_cd_str").val(data.owner_cd);
                  $("#con_type").val(data.con_type);
                  $("#con_type_desc").val(data.con_type_desc);
                  $("#con_id").val(data.con_no);
                  $(`#con_type_desc_str`).text(data.con_type_desc);
                  $(`#car_type_desc_str`).text(data.car_type_descp);
                  $(`#unloading_type_desc`).val(data.unloading_type);
                  $(`#unloading_type_desc_str`).val(data.unloading_type);
                }
            });

        };
    });
    // 最後一個欄位填寫完 enter 送出
    document.getElementById('unloading_type_desc').addEventListener('keydown', function (event) {
      event.stopPropagation();
      if (event.key === 'Enter') {
        submitEvent();
      }
    })
    // 送出按鈕
    document.getElementById('driverSigInSubmit').addEventListener('click', function (event) {
      submitEvent();
    })
    // 查詢按鈕
    document.getElementById('driverSearch').addEventListener('click', function (event) {
      searchEvent();
    })
    // 查詢
    function searchEvent() {
      let isError = false;
      if(($("#phone").val() == '') || !($("#phone").val())) {
        $("#phone").addClass('error');
        isError = true;
      }
      console.log(isError);
      if(isError) {
        return;
      }
      $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: "{{ url('') }}" + '/getfinalcheckin',
            data:JSON.stringify({
                "phone": $("#phone").val(),
                "_token"             : "{{csrf_token()}}",
            }),
            success: function(data) {
              console.log(data);
              if(!data.success ) {
                document.getElementById("mark_name").textContent='';
                document.getElementById("warehouse_name").textContent='';
                document.getElementById("entry_time").textContent='';
              } else {
                if(data.data == null ) {
                  alert('目前無報到資料 請先進行報到');
                  return;
                }
                document.getElementById('driverSigInForm').style.display = 'none';
                document.getElementById('driverSigInRes').style.display = 'block';
                document.getElementById("warehouse_name").textContent=data.data.warehouse;
                document.getElementById("entry_time").textContent=data.data.created_at;
                if(data.data.mark_name == null || data.data.mark_name == "") {
                  document.getElementById("mark_name").textContent='請找倉管人員指派碼頭-' + data.data.contactinfo;
                } else {
                  document.getElementById("mark_name").textContent=data.data.mark_name;
                }
              }
            }
      });
    }
    // 送出
    function submitEvent() {
      console.log("{{ url('') }}");
      let isError = false;
      // 檢查必填
      // 司機電話
      if(($("#phone").val() == '') || !($("#phone").val())) {
        $("#phone").addClass('error');
        isError = true;
      }
      // 司機姓名
      if(($("#driver_name").val() == '') || !($("#driver_name").val())) {
        $("#driver_name").addClass('error');
        isError = true;
      }
      // 車商
      if(($("#truck_cmp_name").val() == '') || !($("#truck_cmp_name").val())) {
        $("#truck_cmp_name").addClass('error');
        isError = true;
      }
      // 車號
      if(($("#car_no").val() == '') || !($("#car_no").val())) {
        $("#car_no").addClass('error');
        isError = true;
      }
      // 車型
      if(($("#car_type").val() == '') || !($("#car_type").val())) {
        $("#car_type").addClass('error');
        isError = true;
      }
      // 貨主
      if(($("#owner_cd").val() == '') || !($("#owner_cd").val())) {
        $("#owner_cd").addClass('error');
        isError = true;
      }
      // 作業類型
      if(($("#job_type_desc").val() == '') || !($("#job_type_desc").val())) {
        $("#job_type_desc").addClass('error');
        isError = true;
      } else {
        switch ($("#job_type_desc").val()) {
          case '進貨':
            // 客戶進貨單號
            if(($("#cust_ord_no").val() == '') || !($("#cust_ord_no").val())) {
              $("#cust_ord_no").addClass('error');
              isError = true;
            }
            // 箱數
            if(($("#box_num").val() == '') && !($("#box_num").val())) {
              $("#box_num").addClass('error');
              isError = true;
            }
            // 品項數
            if(($("#items_num").val() == '') && !($("#items_num").val())) {
              $("#items_num").addClass('error');
              isError = true;
            }
            // 有選貨櫃類型
            if ($("#con_type_desc").val()) {
              // 下貨類型
              if ($("#unloading_type_desc").val() == '' || !($("#unloading_type_desc").val())) {
                $("#unloading_type_desc").addClass('error');
                isError = true;
              }
              // 貨櫃號碼
              if ($("#con_id").val() == '' || !($("#con_id").val())) {
                $("#con_id").addClass('error');
                isError = true;
              }
            }
            break;
          case '拉空櫃':
            if($("#con_id").val() != "") {
              alert('拉空櫃不可輸入櫃號');
              isError = true;
            }
            break;
        }
      }
      console.log(isError);
      if (isError) {
        return
      }
      /* 避免使用者複製貼上 Autocomplet 送出前塞值 STRAT */
      if ($("#owner_cd").val()) {
        // 貨主
        let data = dataOwnerCd.find(item => item.str === $("#owner_cd").val());
        // console.log(data);
        document.getElementById('owner_cd_str').value = data.value;
      }
      if ($("#car_type").val()) {
        // 車型
        let data = dataCarTypeDesc.find(item => item.str === $("#car_type").val());
        // console.log(data);
        document.getElementById('car_type_str').value = data.value;
      }
      // console.log('送出前塞值');
      // console.log($("#owner_cd_str").val());
      // console.log($("#car_type_str").val());
      /* 避免使用者複製貼上 Autocomplet 送出前塞值 END */
      document.getElementById('driverSigInForm').style.display = 'none';
      document.getElementById('driverSigInRes').style.display = 'block';
      // 抓車商代碼
      let truckCmp = $('#truck_cmp_no').val();
      // let truckCmpName = (dataTruck.filter((item) => { return item.name === truckCmp}))[0].value;
      // console.log(truckCmpName);
      var owner_cd = document.getElementById("owner_cd_str").value;
      var owner_name = $("#owner_cd").val().split(' / ');

      $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: "{{ url('') }}" + '/webcheckin',
            data:JSON.stringify({
                "job_type"           : $("#job_type_desc").val(),
                "job_type_desc"      : $("#job_type_desc").val(),
                "cust_ord_no"        : $("#cust_ord_no").val(),
                "car_no"             : $("#car_no").val(),
                "car_type"           : $("#car_type_str").val(),
                "car_type_desc"      : $("#car_type_str").val(),
                "phone"              : $("#phone").val(),
                "driver_name"        : $("#driver_name").val(),
                "con_type"           : $("#con_type_desc").val(),
                "con_type_desc"      : $("#con_type_desc").val(),
                "con_id"             : $("#con_id").val(),
                "unloading_type"     : $("#unloading_type_desc").val(),
                "unloading_type_desc": $("#unloading_type_desc").val(),
                "status"             : $("#status").val(),
                "status_desc"        : $("#status_desc").val(),
                "truck_cmp_name"     : $("#truck_cmp_name").val(),
                "box_num"            : $("#box_num").val(),
                "items_num"          : $("#items_num").val(),
                "owner_cd"           : owner_cd,
                "owner_nm"           : owner_name[1],
                "dc_id"              : "{{$viewData['dc_id']}}",
                "_token"             : "{{csrf_token()}}",
            }),
            success: function(data) {
              console.log(data);
              if(!data.success ) {
                document.getElementById("mark_name").textContent='';
                document.getElementById("warehouse_name").textContent='';
                document.getElementById("entry_time").textContent='';
              } else {
                document.getElementById("warehouse_name").textContent=data.data.warehouse;
                document.getElementById("entry_time").textContent=data.data.entry_time;
                if(data.data.mark_name == null || data.data.mark_name == "") {
                  document.getElementById("mark_name").textContent='請找倉管人員指派碼頭-' + data.data.contactinfo;
                } else {
                  document.getElementById("mark_name").textContent=data.data.mark_name;
                }
              }

            }
        });
    }
    // 回上一步
    document.getElementById('driverSigInReturn').addEventListener('click', function (event) {
      document.getElementById('driverSigInForm').style.display = 'block';
      document.getElementById('driverSigInRes').style.display = 'none';
      $("#job_type_desc").val('');
      $("#job_type_desc").val('');
      $("#cust_ord_no").val('');
      $("#car_no").val('');
      $("#car_type_str").val('');
      $("#phone").val('');
      $("#driver_name").val('');
      $("#con_type_desc").val('');
      $("#con_type_desc").val('');
      $("#con_id").val('');
      $("#unloading_type_desc").val('');
      $("#unloading_type_desc").val('');
      $("#status").val('');
      $("#status_desc").val('');
      $("#truck_cmp_name").val('');
      $("#box_num").val('');
      $("#items_num").val('');
      $("#owner_cd").val('');
      $("#car_type").val('');
    })
    // 監聽輸入框改變
    function setInputListenerEvent(data) {
      document.getElementById(data).addEventListener('change', function (event) {
        console.log(data);
        console.log(($("#job_type_desc").val()).indexOf('拉') != -1);
        document.getElementById(data).classList.remove('error');
        if (data === 'job_type_desc' && ($("#job_type_desc").val()).indexOf('拉') != -1) {
          document.getElementById('cust_ord_no').classList.remove('error'); // 客戶進貨單號
          document.getElementById('box_num').classList.remove('error'); // 箱數
          document.getElementById('items_num').classList.remove('error'); // 品項數
          document.getElementById('unloading_type_desc').classList.remove('error'); // 下貨類型
          if (!($("#con_id").val())) {
            document.getElementById('con_id').classList.remove('error'); // 貨櫃號碼
          }
        }
      })
    }
    const inputList = ['phone', 'driver_name', 'truck_cmp_name', 'car_no', 'car_type', 'owner_cd', 'job_type_desc', 'cust_ord_no', 'box_num', 'items_num', 'unloading_type_desc', 'con_id'];
    inputList.forEach((item) => {
      setInputListenerEvent(item);
    })
    $(document).ready(function() {
        // 在這撰寫javascript程式碼
    });
  </script>
  <!-- 深淺模式 -->
  <!-- <script>
    new Darkmode().showWidget();
  </script> -->
  <!-- END 深淺模式 -->
  <!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

</html>