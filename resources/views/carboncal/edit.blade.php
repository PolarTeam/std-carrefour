@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_carbon_cal.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/carboncal') }}">{{ trans('mod_carbon_cal.titleName') }}</a></li>
		<li class="active">{{ trans('mod_carbon_cal.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="trs_mode_desc">{{ trans('mod_carbon_cal.trs_mode_desc') }} <span style="color:red">*<span></label>
                                            <input type="hidden" name="trs_mode_desc" id = "trs_mode_desc" class="form-control">
                                            <select  class="form-control" id="trs_mode" name="trs_mode" >
                                                <option value="NORMAL">常溫</option>
                                                <option value="LOWTEMP">低溫</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="car_type_desc">{{ trans('mod_mark_setting.car_type_desc') }} <span style="color:red">*<span></label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="car_type" name="car_type" required="required">
                                                <input type="text" class="form-control" id="car_type_desc" name="car_type_desc" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="car_type_desc"
                                                        info1="{{Crypt::encrypt('bscode')}}" 
                                                        info2="{{Crypt::encrypt("cd+cd_descp,cd,cd_descp")}}" 
                                                        info3="{{Crypt::encrypt("cd_type='CARTYPE'")}}"
                                                        info5="{{Crypt::encrypt('bscode_kind')}}"
                                                        info4="cd=car_type;cd_descp=car_type_desc;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-3">
                                            <label for="carbon_value">{{ trans('mod_carbon_cal.carbon_value') }} <span style="color:red">*<span></label>
                                            <input type="number" min="0" class="form-control" id="carbon_value" name="carbon_value">
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
    
    
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="car_type_desc"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('car_type_desc', "車型", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="car_type_desc"]').on('click', function(){
        var check = $('#subBox input[name="car_type_desc"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","car_type_desc",callBackFunc=function(data){
                console.log(data);
                
            },"car_type_desc");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/carboncal" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#owner_cd').select2();
            $('#parked_models').select2();
            $('#container').select2();

        };

        formOpt.addFunc = function() {
            $('#trs_mode').val('NORMAL');
        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"trs_mode",
                    "column_type":"string",
                },
                {
                    "column_filed":"car_type_desc",
                    "column_type":"string",
                },
                {
                    "column_filed":"carbon_value",
                    "column_type":"string",
                },
            ];

            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            var sel = document.getElementById("trs_mode");
            var text = sel.options[sel.selectedIndex].text;
            $('#trs_mode_desc').val(text); 
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection