@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_base_message.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/basemessage') }}">{{ trans('mod_base_message.titleName') }}</a></li>
		<li class="active">{{ trans('mod_base_message.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="status">{{ trans('mod_base_message.send_name') }} <span style="color:red">*<span></label>
                                            <input type="hidden" name="send_name" id = "send_name" class="form-control">
                                            <select class="form-control select2" id="send_user" name="send_user" multiple="multiple">
                                                @foreach($send_user as $row)
                                                <option value="{{$row->id}}">{{$row->email}}-{{$row->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="status">{{ trans('mod_base_message.send_role_name') }} <span style="color:red">*<span></label>
                                            <input type="hidden" name="send_role_name" id = "send_role_name" class="form-control">
                                            <select class="form-control select2" id="send_role" name="send_role" multiple="multiple">
                                                @foreach($send_role as $row)
                                                <option value="{{$row->name}}">{{$row->display_name}}</option>
                                                @endforeach	
                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="content">{{ trans('mod_base_message.title') }} <span style="color:red">*<span></label>
                                            <input type="text" name="title" id = "title" class="form-control">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="status">{{ trans('mod_base_message.is_send') }}</label>
                                            <select class="form-control" id="is_send" name="is_send" switch="off" disabled="disabled" readonly= 'true'>
                                                <option value="是">是</option>
                                                <option value="否">否</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="content">{{ trans('mod_base_message.content') }} <span style="color:red">*<span></label>
                                            <textarea class="form-control" id='content' rows="3" name="content"></textarea>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_base_message.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_base_message.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_base_message.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_base_message.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
    
    
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
                
            },"wh_name");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/basemessage" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#send_role').select2();
            $('#send_user').select2();
        };
        formOpt.addFunc = function() {
            $('#is_send').val('否');
            $('#send_role').val();
            $('#send_role').trigger('change');

            $('#send_user').val();
            $('#send_user').trigger('change');
        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"content",
                    "column_type":"string",
                },
                {
                    "column_filed":"title",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;

            if($("#send_user").val() == null && $("#send_role").val() == null ) {
                // 下拉選單多選
                document.getElementsByClassName('select2-selection__rendered')[0].style.backgroundColor = "FCE8E6";
                document.getElementsByClassName('select2-selection__rendered')[1].style.backgroundColor = "FCE8E6";
                cansafe = false;
            } 
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


        var btnGroup = [
            {
                btnId: "btnSendMessage",
                btnIcon: "fa fa-commenting",
                btnText: '發送訊息',
                btnFunc: function () {
                    //
                    $.post(BASE_URL + '/' + lang + '/' + 'basemessage' + '/batchSendMessage', {
                        'ids': [mainId]
                    }, function(data) {
                        if (data.success) {
                            $('#is_send').val('是');
                            swal('操作成功', "", "success");
                            $("#jqxGrid").jqxGrid('updatebounddata');
                            $("#jqxGrid").jqxGrid('clearselection');
                        } else {
                            swal('操作失敗', '', "error");
                        }
                    });
                    //
                }
            }
        ];
        initBtn(btnGroup);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection