<!-- Modal -->
<div class="modal fade" id="lookupModal" tabindex="-1" role="dialog" aria-labelledby="lookupTitle" aria-hidden="true" style="z-index:99999999999999999">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="lookupTitle">Modal title</h5>
      </div>
      <div class="modal-body" id="lookupbody">
        <div id="lookupGrid"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <input type="hidden" id="lookupEvent" name="lookupEvent" />
</div>
@section('lookup_scripts')

<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js"></script>
<script>
function initLookup(fieldName, title, callBackFunc=null){
  console.log('------------------------------------------------------------------');
  var fieldObj = $('button[btnName="'+fieldName+'"]');
  fieldObj.info1 = fieldObj.attr("info1");
  fieldObj.info2 = fieldObj.attr("info2");
  fieldObj.info3 = fieldObj.attr("info3");
  fieldObj.info4 = fieldObj.attr("info4");
  fieldObj.info5 = fieldObj.attr("info5");
  fieldObj.selectionmode = fieldObj.attr("selectionmode");
  fieldObj.triggerfunc = fieldObj.attr("triggerfunc");
  var gridOpt = {};
  gridOpt.gridId = "lookupGrid";
  gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getLookupFieldsJson') }}"+"/"+fieldObj.info1 +"/"+fieldObj.info2+"/"+fieldObj.info5 +"/{{$lang}}";
  gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getLookupGridJson') }}"+"/"+fieldObj.info1+"/"+fieldObj.info2+"/"+fieldObj.info3;
  
  if(fieldObj.selectionmode === "undefined" || fieldObj.selectionmode === "singlerow"){
    gridOpt.selectionmode = "singlerow";
  }else if(fieldObj.selectionmode === "checkbox"){
    gridOpt.selectionmode = "checkbox";
  }

  
  gridOpt.height = 500;
  gridOpt.who = "lookup";
  gridOpt.searchOpt = true;
  $('#lookupTitle').text(title);
  var docHeight = $( document ).height();
  if(docHeight < 800) {
      gridOpt.height = 300;
  }
  $.get( gridOpt.fieldsUrl, function( fieldData ) {
      console.log(fieldData);
      $('#' + gridOpt.gridId).jqxGrid('destroy');
      $('#lookupbody').append('<div id="lookupGrid"></div>')
      initGrid(gridOpt.gridId, fieldData, gridOpt.dataUrl,gridOpt, fieldObj);
      // if(callBackFunc !== null) {
      //   callBackFunc(datarow);
      // }
  });


  
}

function initAutocomplete(formId,fieldName,callBackFunc=null,btnName=""){
  var fieldObj = $('button[btnName="'+fieldName+'"]');
  fieldObj.info1 = fieldObj.attr("info1");
  fieldObj.info2 = fieldObj.attr("info2");
  fieldObj.info3 = fieldObj.attr("info3");
  fieldObj.info4 = fieldObj.attr("info4");
  fieldObj.info5 = fieldObj.attr("info5");
  var fieldMapping = fieldObj.info4.split(";");
  console.log('initAutocomplete fro nick');
  if(btnName != "") {
    fieldName = btnName;
  }

  $('#'+formId+' input[name="'+fieldName+'"]').autocomplete({
      source: function( request, response ) {
          $.getJSON( "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getAutocompleteJson') }}"+"/"+fieldObj.info1+"/"+fieldObj.info2+"/"+fieldObj.info3+"/"+request.term, function (data) {
            response(data);
        });
        },
      minLength: 1,
      autoFocus: true,
      select: function( event, ui ) {
        var thisId = $(this).attr("id");
        $.each(fieldMapping,function(k,v){
          k = v.split("=");
          if(thisId == k[1]){
            ui.item.value = ui.item[k[0]];
          }else{
            console.log(k[1]);
            $("#"+k[1]).val(ui.item[k[0]]);
            if(ui.item['area_nm']!=undefined){
              if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
                $("#"+k[1]).val(ui.item['city_nm']+ui.item['area_nm']);
              }
            }
            if(ui.item['dist_nm']!=undefined){
              if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
              $("#"+k[1]).val(ui.item['city_nm']+ui.item['dist_nm']);
              }
            }
          }
        });
        if(callBackFunc !== null) {
          callBackFunc(ui.item);
        }
      },
      change: function( event, ui ) {
        console.log(ui.item);
        var old_part_no=  $("#part_no").val();
        var old_part_name=  $("#part_name").val();
        if(fieldName !="part_no"){
          if(ui.item === null){
            $.each(fieldMapping,function(k,v){
              k = v.split("=");
              $("#"+k[1]).val("");
            });
            $("#part_no").val(old_part_no);
            $("#part_name").val(old_part_name);
          }
        }

      }
  });
}
</script>    


@endsection