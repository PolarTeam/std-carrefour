{{--  <div class="col-md-2">
    <div class="box box-default">
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked" id="btnArea">
                <li>
                    <a class="MenuButton" id="iAdd"><i class="fa fa-plus"></i> {{ trans('common.add') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iEdit"><i class="fa fa-pencil"></i> {{ trans('common.edit') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iDel"><i class="fa fa-trash-o"></i> {{ trans('common.delete') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iSave"><i class="fa fa-floppy-o"></i> {{ trans('common.save') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iCancel"><i class="fa fa-ban"></i> {{ trans('common.cancel') }}</a>
                </li>
                <li>
                    <a class="MenuButton" id="iCopy"><i class="fa fa-files-o"></i> {{ trans('common.copy') }}</a>
                </li>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /. box -->
</div>  --}}

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body" id="tool-bar">
                <a class="btn btn-app" id="iBack" style="margin-right: 35px !important;"><i class="fa fa-arrow-left"></i> {{ trans('common.close') }}</a>
                {{--  @can($crud->pmsName.'.add')  --}}
                <a class="btn btn-app" id="iAdd"><i class="fa fa-plus"></i> {{ trans('common.add') }}</a>
                {{--  @endcan
                @can($crud->pmsName.'.edit')  --}}
                <a class="btn btn-app" id="iEdit"><i class="fa fa-pencil"></i>{{ trans('common.edit') }}</a>
                {{--  @endcan
                @can($crud->pmsName.'.delete')  --}}
                <a class="btn btn-app" id="iDel"><i class="fa fa-trash-o"></i>{{ trans('common.delete') }}</a>
                {{--  @endcan  --}}
                <a class="btn btn-app" id="iSave"><i class="fa fa-floppy-o"></i> {{ trans('common.save') }}</a>
                <a class="btn btn-app" id="iCancel"><i class="fa fa-ban"></i> {{ trans('common.cancel') }}</a>
                {{-- <a class="btn btn-app" id="iCopy"><i class="fa fa-files-o"></i> {{ trans('common.copy') }}</a> --}}
            </div>
        </div>
    </div>
</div>

@section('toolbar_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/edit-var.js"></script>
<script>

$(function(){
            formOpt.initFieldCustomFunc();
            
            //初始化欄位
            $.get( formOpt.fieldsUrl , function( data ) {
                if(mainId == "") {
                    //initField(data);
                    menuBtnFunc.init();
                    menuBtnFunc.disabled(['iEdit', 'iDel', 'iCopy']);
                    formOpt.addFunc();
                    setField.init(formOpt.formId,data.filed, true);
                    $("#iAdd").click();
                }
                else {
                    //initField(data);
                    console.log('init set data');
                    setField.init(formOpt.formId,data.filed, true);

                    //若是編輯進來的，要把資料塞入Form中
                    if(data !== null) {
                        setFormData(formOpt.formId,data);
                        formOpt.setFormDataFunc();
                        menuBtnFunc.init();
                    }
                }

                if(typeof formOpt.afterInit === "function") {
                    formOpt.afterInit(data);
                }
            });
            $('#iBack').on('click', function(){
                if(mainId != "") {
                    try {
                        try {
                            window.opener.document.getElementById("updategrid").click();
                        } catch (error) {
                            
                        }
                        try {
                            location.href = formOpt.saveUrl;
                            window.close();
                        } catch (error) {
                            console.log('123'); 
                        }
                    } catch (error) {
                        if(formOpt.backurl != 'undefined') {
                            location.href = formOpt.backurl;
                            return;
                        }
                        location.href = formOpt.saveUrl;
                        window.close();
                    }
                } else {
                    history.back();
                }

            });
            $('#iAdd').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $(':input:not(:checkbox, :radio)').not('[no-clear="Y"]').val('');
                $('input[name="_method"]').val('POST');
                SAVE_URL = formOpt.saveUrl;
                if(typeof OLD_DATA != "undefined") {
                    OLD_DATA = [];
                }
                menuBtnFunc.addFunc();
                
                formOpt.addFunc();
            });
    
            $('#iEdit').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('select').not('[grid="true"]').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                
                SAVE_URL = formOpt.saveUrl + '/' + mainId;
                menuBtnFunc.editFunc();
                
                formOpt.editFunc();
            });
    
            $('#iCopy').on('click', function(){
                setField.disabled(formOpt.formId, ['created_by', 'created_at', 'updated_by', 'updated_at']);
                $('input').not('[switch="off"]').prop('disabled', false);
                $('select').not('[switch="off"]').prop('disabled', false);
                $('textarea').not('[switch="off"]').prop('disabled', false);
                $('input[name="_method"]').val('POST');
                menuBtnFunc.addFunc();
                if(typeof OLD_DATA != "undefined") {
                    OLD_DATA = [];
                }else {
                    OLD_DATA = [];
                }
                
                formOpt.copyFunc();
            });
    
            $('#iCancel').on('click', function(){
                location.reload();
                formOpt.cancelFunc();
            });
            
            $('#iSave').on('click', function(){
                //var changeData = getChangeData(formOpt.formId, new FormData($('#'+formOpt.formId)[0]));
                if(formOpt.beforesaveFunc()){
                    var changeData = getChangeData(formOpt.formId, $('#'+formOpt.formId).serializeArray());
                    console.log($.param(changeData));
                    var method = (typeof $("#"+formOpt.formId + " input[name='_method']").val() !== "undefined")? $("#"+formOpt.formId + " input[name='_method']").val():"POST";
                    if(formOpt.uploadtype == "file" ) {
                        changeData =  new FormData($('#'+formOpt.formId)[0]);
                        $.ajax({
                            url: SAVE_URL,
                            //type: 'POST',
                            type: method,
                            //data: changeData,
                            data: changeData,
                            async: false,
                            processData: false,
                            contentType : false,
                            dataType: 'JSON',
                            beforeSend: function () {
                                loadingFunc.show();
                            },
                            error: function (jqXHR, exception) {
                                try{
                                    var msgObj = JSON.parse(jqXHR.responseText);
                                    $('#errorMsg').find('ul').html('');
                                    $.each(msgObj, function(i, v){
                                        $('#errorMsg').find('ul').append('<li>' + v + '</li>');
                                        $('#errorMsg').show();
                                    });
                                    
                                }
                                catch(err){
                                    new PNotify(PNotifyOpt.saveError);
                                }
                                formOpt.saveErrorFunc();
                                loadingFunc.hide();
                                return false;
                            },
                            success: function (data) {
                                if(data.success) {
                                    $('#errorMsg').hide();
                                    new PNotify(PNotifyOpt.saveSuccess);
            
                                    if(typeof data.lastId != "undefined") {
                                        location.href = EDIT_URL  + "/" + data.lastId + "/edit";
                                    }
            
                                    setField.disabledAll(formOpt.formId,data.filed, true);
                                    menuBtnFunc.init();
                                    try{
                                        if(typeof data.updated_by !== "undefined") {
                                            $("#updated_by").val(data.updated_by);
                                        }
                                        if(typeof data.updated_at !== "undefined") {
                                            $("#updated_at").val(data.updated_at);
                                        }
                                    } catch(err) {
                                        
                                    }

                                    formOpt.saveSuccessFunc(data);
                                    
                                }else{
                                    formOpt.saveErrorFunc(data);
                                    swal(data.message, "", "warning");
                                }
                                loadingFunc.hide();
                                return false;
                            },
                            cache: false,
                        });
                    }else {
                        $.ajax({
                            url: SAVE_URL,
                            //type: 'POST',
                            type: method,
                            //data: changeData,
                            data: changeData,
                            async: false,
                            dataType: 'JSON',
                            beforeSend: function () {
                                loadingFunc.show();
                            },
                            error: function (jqXHR, exception) {
                                try{
                                    var msgObj = JSON.parse(jqXHR.responseText);
                                    $('#errorMsg').find('ul').html('');
                                    $.each(msgObj, function(i, v){
                                        $('#errorMsg').find('ul').append('<li>' + v + '</li>');
                                        $('#errorMsg').show();
                                    });
                                    
                                }
                                catch(err){
                                    new PNotify(PNotifyOpt.saveError);
                                }
                                formOpt.saveErrorFunc();
                                loadingFunc.hide();
                                return false;
                            },
                            success: function (data) {
                                if(data.success) {
                                    $('#errorMsg').hide();
                                    new PNotify(PNotifyOpt.saveSuccess);
            
                                    if(typeof data.lastId != "undefined") {
                                        location.href = EDIT_URL  + "/" + data.lastId + "/edit";
                                    }
            
                                    setField.disabledAll(formOpt.formId,data.filed, true);
                                    menuBtnFunc.init();
                                    formOpt.saveSuccessFunc(data);
                                    try{
                                        if(typeof data.data.updated_by !== "undefined") {
                                            $("#updated_by").val(data.data.updated_by);
                                        }
                                        if(typeof data.data.updated_at !== "undefined") {
                                            $("#updated_at").val(data.data.updated_at);
                                        }
                                    } catch(err) {
                                        
                                    }
                                }else{
                                    formOpt.saveErrorFunc(data);
                                    swal(data.message, "", "warning");
                                }
                                loadingFunc.hide();
                                return false;
                            },
                            cache: false,
                        });
                    }
                }
            });
    
            $('#iDel').on('click', function(e){
                // 刪除時 跳出確認刪除 用swal
                e.preventDefault();
                var delete_button = $(this);
                var delete_url = DELETE_URL + '/' + mainId;

                var confirmSave = 'N';
                swal({
                    title: msgnew2,
                    text: msgnew3,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: confirmButtonText,
                    cancelButtonText: cancelButtonText
                    }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.value) {
                        if (formOpt.beforeDelFunc()) {
                            $.ajax({
                                url: delete_url,
                                type: 'DELETE',
                                success: function(result) {
                                    // Show an alert with the result
                                    if(result.success) {
                                        new PNotify(PNotifyOpt.delSuccess);
                                        location.href = CREATE_URL;
                                    } else {
                                        swal(result.message, "", "warning");
                                    }
                                },
                                error: function(result) {
                                    swal(result.message, "", "warning");
                                    // Show an alert with the result
                                    new PNotify(PNotifyOpt.delError);
                                }
                            });
                        } else {
                            //new PNotify(PNotifyOpt.delError);
                        }
                    } else {
                        return;
                    }
                })


    
            });
        });
        function beforesave(requiredColumn){

            console.log(requiredColumn);
            var cansafe = true;
            $.each(requiredColumn,function(i,item) {
                var filedvalue = $("#"+item.column_filed).val();

                if(item.column_type == "string" ) {
                    // 輸入框
                    if(filedvalue == "" ||  filedvalue == null) {
                    document.getElementById(item.column_filed).style.backgroundColor = 'FCE8E6';
                    cansafe = false;
                    } else {
                        try{
                            document.getElementById(item.column_filed).style.backgroundColor = "";
                        }
                        catch(err){
                        }
                    }
                } else if(item.column_type == "lookup") {
                    // 放大鏡
                    var binding_with_value = $("#"+item.binding_with).val();
                    if(filedvalue == "" || binding_with_value == "") {
                        document.getElementById(item.column_filed).style.backgroundColor = 'FCE8E6';
                        cansafe = false;
                    } else {
                        try{
                            document.getElementById(item.column_filed).style.backgroundColor = "";
                        }
                        catch(err){
                        }
                    }

                } else if(item.column_type == "select2") {
                    // 下拉選單多選
                    if(filedvalue == "" || filedvalue == null) {
                        document.getElementsByClassName('select2-selection__rendered')[item.ranked].style.backgroundColor = "FCE8E6";
                        cansafe = false;
                    } else {
                        document.getElementsByClassName('select2-selection__rendered')[item.ranked].style.backgroundColor = "";
                    }
                    
                } else {
                    // 其他
                    if(filedvalue == "" ) {
                        document.getElementById(item.column_filed).style.backgroundColor = 'FCE8E6';
                        cansafe = false;
                    } else {
                        try{
                            document.getElementById(item.column_filed).style.backgroundColor = "";
                        }
                        catch(err){
                        }
                    }
                }

            });

            return cansafe;

        }

        function initBtn(btnGroup){
            $.each(btnGroup, function(i, item) {
                var btnHtml = '<a class="btn btn-app" id="{btnId}"><i class="fa {btnIcon}"></i>{btnText}</a>';

                btnHtml = btnHtml.replace("{btnId}",item.btnId);
                btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
                btnHtml = btnHtml.replace("{btnText}",item.btnText);
                $("#tool-bar").append(btnHtml);
                $("#"+item.btnId).on("click",function(){
                    item.btnFunc();
                });
            })
        }
</script>
@endsection