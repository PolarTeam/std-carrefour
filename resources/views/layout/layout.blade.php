<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>
      {{ isset($title) ? $title.' :: '.config('backpack.base.project_name').' DSS' : config('backpack.base.project_name').' DSS' }}
    </title>

    @yield('before_styles')

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    {{--  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/timepicker/bootstrap-timepicker.min.css">  --}}
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/pnotify/pnotify.custom.min.css') }}">

    <!-- BackPack Base CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/backpack/backpack.base.css') }}">

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}?v={{Config::get('app.version')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="{{ asset('vendor/jquery') }}/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">

    <style>
      .swal2-container {
        z-index: 10000;
      }
      .select2-dropdown {
        z-index: 10001;
      }
    </style>

    @yield('after_styles')
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue pace-done pace-done sidebar-mini">
	<script type="text/javascript">
    var BASE_URL = "{{ url(config('backpack.base.route_prefix', '')) }}";
    var BASE_API_URL = "{{ url(config('backpack.base.api_route_prefix', 'api')) }}";
    var C_KEY = '#';
		/* Recover sidebar state */
		(function () {
			if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
				var body = document.getElementsByTagName('body')[0];
				body.className = body.className + ' sidebar-collapse';
			}
		})();
	</script>
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
          DSS
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('base.toggle_navigation') }}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          @include('layout.inc.menu')
        </nav>
      </header>

      <!-- =============================================== -->

      @include('layout.inc.sidebar')
      @yield('sidebar_scripts')
      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         @yield('header')

        <!-- Main content -->
        <section class="content" style="position: relative; height: 100%;">
        <div id="overlay"></div>
        <div class="cssload-wraper" style="z-index: 9999999">
          <div class="cssload-dots"></div>
        </div>

          @yield('content')

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer">
        @if (config('backpack.base.show_powered_by'))
            <div class="pull-right hidden-xs">
              {{ trans('base.powered_by') }} <a target="_blank" href="http://standard-info.com">Standard Infomation</a>
            </div>
        @endif
        {{ trans('base.handcrafted_by') }} <a target="_blank" href="{{ config('backpack.base.developer_link') }}">{{ config('backpack.base.developer_name') }}</a>.
      </footer>
    </div>
    <!-- ./wrapper -->
    <style>
      .pace {
        display: none!important;
      }
      .content-wrapper {
        height: auto;
      }
      .skin-blue .main-header .logo {
        background: #3c8dbc;
        display: inline-flex;
        align-items: center;
        justify-content: center;
      }
      .skin-blue .main-header .navbar {
        background: #3c8dbc;
      }
    </style>

    <!-- jQuery 2.2.0 -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.0.min.js"><\/script>')</script>
    <script>
      $("#jqxGrid").keydown(function(e) {
          if (e.keyCode == 67 && e.ctrlKey) {
              /* close fancybox here */
              document.execCommand("Copy");
          }
      });
    </script>
    <!-- 語系 btnGroup -->
    <script>
      var btnSearchWindow   = "{{ trans('common.search') }}";
      var btnExportExcel    = "{{ trans('common.exportExcel') }}";
      var btnOpenGridOpt    = "{{ trans('common.gridOption') }}";
      var btnAdd            = "{{ trans('common.add') }}";
      var btnDelete         = "{{ trans('common.delete') }}";
      var btnUploadExcel    = "{{ trans('common.excelImport') }}";
      var btnReport9        = "{{ trans('common.report') }}";
      var msg1              = "{{ trans('common.msg1') }}";
      var msgnew2           = "{{ trans('common.msgnew2') }}";
      var msgnew3           = "{{trans('common.msgnew3')}}";
      var msgnew4           = "{{trans('common.msgnew4')}}";
      var msgnew5           = "{{trans('common.msgnew5')}}";
      var confirmButtonText = "{{trans('common.sure')}}";
      var cancelButtonText  = "{{trans('common.cancel')}}";
    </script>

    @yield('before_scripts')


    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('vendor/adminlte') }}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/pace/pace.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/fastclick/fastclick.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/dist/js/app.min.js"></script>

    <!-- page script -->
    <script type="text/javascript">
        /* Store sidebar state */
        $('.sidebar-toggle').click(function(event) {
          event.preventDefault();
          if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            sessionStorage.setItem('sidebar-toggle-collapsed', '');
          } else {
            sessionStorage.setItem('sidebar-toggle-collapsed', '1');
          }
        });
        // To make Pace works on Ajax calls
        $(document).ajaxStart(function() { 
          Pace.restart(); 
        });

        // Ajax calls should always have the CSRF token attached to them, otherwise they won't work
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        // Set active state on menu element
        var current_url = "{{ Request::fullUrl() }}";
        var full_url = current_url+location.search;
        var $navLinks = $("ul.sidebar-menu li a");
        // First look for an exact match including the search string
        var $curentPageLink = $navLinks.filter(
            function() { return $(this).attr('href') === full_url; }
        );
        // If not found, look for the link that starts with the url
        if(!$curentPageLink.length > 0){
            $curentPageLink = $navLinks.filter(
                function() { return $(this).attr('href').startsWith(current_url) || current_url.startsWith($(this).attr('href')); }
            );
        }
        
        $curentPageLink.parents('li').addClass('active');
        {{-- Enable deep link to tab --}}
        var activeTab = $('[href="' + location.hash.replace("#", "#tab_") + '"]');
        activeTab && activeTab.tab('show');
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            location.hash = e.target.hash.replace("#tab_", "#");
        });

        var transLang = {};
            transLang["addNewRow"] = "{{ trans('common.addNewRow') }}";
            transLang["deleteSelectRow"] = "{{ trans('common.deleteSelectRow') }}";
            transLang["msg1"] = "{{ trans('common.msg1') }}";
            transLang["browse"] = "{{ trans('common.browse') }}";

        $(".switch-online").on("click", function(){
          var status = $(this).attr("status");
          var imgUrl = "";
          if(status == "1") {
              imgUrl = "https://placehold.it/160x160/5cb85c/ffffff/&text=T";
          }
          else if(status == "2") {
              imgUrl = "https://placehold.it/160x160/f0ad4e/ffffff/&text=T";
          }
          else {
              imgUrl = "https://placehold.it/160x160/636c72/ffffff/&text=T";
          }
          $("#userPhoto").attr("src", imgUrl);
      });

      $(function(){
        $("#modifyPwdBtn").on("click", function(){
          var userPassword        = $("#userPassword").val();
          var userConfirmPassword = $("#userConfirmPassword").val();
          var originalPassword    = $("#originalPassword").val();

          var newpassword = userPassword;
          var regex = new RegExp('^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#%&]).{8,15}');

          if(!regex.test(newpassword)) {
              swal("{{trans('common.warning')}}", "密碼請包含大小寫英文字母特殊符號數字 長度超過8碼", "warning");
              return false;
          }
          if(userPassword != userConfirmPassword) {
            swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
            return;
          }
          var postData = {'password': userPassword, 'confirmPassword': userConfirmPassword, 'originalPassword': originalPassword}
          $.post(BASE_URL+'/userPwd/update', postData, function(result){
            if(result.msg == "success") {
              swal("{{trans('common.msgnew13')}}", "", "success");
            }
            else {
              swal("{{trans('common.msgnew5')}}", result.msgLog, "warning");
            }
          });
        });

        $("#changelang").on("click", function(){
          var chagneLang = $("#mutiLang").val();
          var langNow    = "{{app()->getLocale()}}";
          var postData   = {'chagneLang': chagneLang}
          $.post(BASE_URL+'/userlang/change', postData, function(result){
            if(result.msg == "success") {
              swal("{{trans('common.msgnew13')}}", "", "success");
              var redirectUrl = window.location.href.replace("/"+langNow+"/" , "/"+chagneLang+"/");
              window.location.replace(redirectUrl);
            }
            else {
              swal("{{trans('common.msgnew5')}}", result.msgLog, "warning");
            }
          });
        });

      });
      $(function(){
        $("#mutiLang").on("click", function(event){
          event.stopPropagation();
        });
      });
</script>


    <link rel="stylesheet" href="{{ asset('vendor/jqwidgets') }}/styles/jqx.base.css?v={{Config::get('app.version')}}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('vendor/jqwidgets') }}/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxcore.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxtabs.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxcheckbox.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxnumberinput.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxmenu.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxbuttons.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxscrollbar.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxlistbox.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxcombobox.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdata.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxchart.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.filter.js"></script>

    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxsortable.js"></script>

    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxcalendar.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdatetimeinput.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxlistbox.js"></script> 
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdragdrop.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.storage.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.columnsreorder.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.columnsresize.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.aggregates.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.edit.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdata.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxdata.export.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxgrid.export.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>
    
    <script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxloader.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{ asset('js/core') }}/common.js?v={{Config::get('app.version')}}"></script>
    <script src="{{ asset('vendor/jquery') }}/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.all.min.js"></script>
    
    <div id="jqxLoader">
    </div>

    @include('layout.inc.alerts')

    @yield('after_scripts')

    @yield('toolbar_scripts')

    @yield('lookup_scripts')

    <!-- JavaScripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
