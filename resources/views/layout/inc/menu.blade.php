<div class="navbar-custom-menu pull-left">
    <ul class="nav navbar-nav">
        <div class="announce">
            <i class="fa fa-bullhorn" aria-hidden="true"></i>
            <span id="announce">
                <!-- 公告內容放置處 -->
            </span>
        </div>
        <!-- =================================================== -->
        <!-- ========== Top menu items (ordered left) ========== -->
        <!-- =================================================== -->

        <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->

        <!-- ========== End of top menu left items ========== -->
    </ul>
</div>


<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- ========================================================= -->
      <!-- ========== Top menu right items (ordered left) ========== -->
      <!-- ========================================================= -->

      <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->
        @if (Auth::guest())
            <li><a href="{{ url('/login') }}">{{ trans('base.login') }}</a></li>
            @if (config('backpack.base.registration_open'))
            <li><a href="{{ url('/register') }}">{{ trans('base.register') }}</a></li>
            @endif
        @else
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    {{-- <img src="https://placehold.it/160x160/5cb85c/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image" width="12" height="12" id="userPhoto"> --}}
                    <span class="hidden-xs">{{Auth::user()->name}}</span>
                </a>
                {{-- <ul class="dropdown-menu">
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-success btn-sm switch-online" status="1">線上</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-warning btn-sm switch-online" status="2">離開</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button class="btn btn-muted btn-sm switch-online" status="0">離線</button>
                            </div>
                        </div>
                    <!-- /.row -->
                    </li>
                </ul> --}}
            </li>

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="hidden-xs">修改密碼</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="originalPassword" name="originalPassword" placeholder="{{ trans('common.msgnew7') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="{{ trans('common.msgnew8')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="password" class="form-control" id="userConfirmPassword" name="userConfirmPassword" placeholder="{{ trans('common.msgnew9')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" style="text-align: center">
                                <button class="btn btn-sm btn-primary" id="modifyPwdBtn">{{ trans('common.msgnew6') }}</button>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                </ul>
            </li>
            <li>
            <a href="#"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="fa fa-btn fa-sign-out"></i> {{ trans('base.logout') }}
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </li>
            <!-- <li><a href="{{ url(config('backpack.base.route_prefix', '').'/logout') }}"><i class="fa fa-btn fa-sign-out"></i> {{ trans('base.logout') }}</a></li> -->
        @endif

       <!-- ========== End of top menu right items ========== -->
    </ul>
</div>

<style>
    .announce {
        color: #fff;
        font-size: 20px;
        padding: 13px 0;
    }
    .announce i {
        margin-right: 8px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    // 公告輪播
    $.ajax({
        type: "post", //  OR POST WHATEVER...
        dataType : 'json', // 預期從server接收的資料型態
        contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
        url: BASE_URL + '/zh-TW' + '/public/announceProfile',
        data:JSON.stringify({ 
            "_token"             : "{{csrf_token()}}",
        }),
        success: function(data) {
            console.log(data);
            //
            let announceList = data.data;
            let announceActive = 0;
            let announceChengeTimeout = 3000;
            let announceCarousel;
            function starAnnounceCarouselEvent () {
                announceCarousel = setInterval(() => {
                    announceActive += 1;
                    if (announceActive > (announceList.length - 1)) {
                        announceActive = 0;
                    };
                    document.getElementById('announce').textContent = announceList[announceActive];
                }, announceChengeTimeout);
            }
            function endAnnounceCarouselEvent () {
                clearInterval(announceCarousel);
            }

            document.getElementById('announce').textContent = announceList[announceActive]; // 第一次給值
            setTimeout(() => {
                starAnnounceCarouselEvent();
            }, announceChengeTimeout);
            //
        }
    });


</script>