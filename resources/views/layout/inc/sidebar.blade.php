@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 44px">
          <div class="pull-left image">
            {{-- <img src="http://placehold.it/160x160/00a7d0/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image"> --}}
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            {{--  <a href="#" id="onlineStatus"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">管理</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          {{-- @can('MESSAGE')
            <li><a href="{{ url(config('backpack.base.route_prefix', '').'/message') }}"><i class="fa fa-envelope"></i> <span>訊息</span><span class="num-box" id="unReadCount">{{Auth::user()->unReadCount}}</span></a></li>
          @endcan --}}
          @can('FILEBOX')
          <li><a href="{{ url(config('backpack.base.route_prefix', '').'/message') }}"><i class="fa fa-envelope"></i> <span>訊息夾</span><span class="num-box" id="unReadCount">{{Auth::user()->unReadCount}}</span></a></li>
          @endcan
          @can('DASHBOARD')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('DASHBOARDCHARTWH')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/dashboardChartWh') }}"><i class="fa fa-university"></i> <span>Dashboard - 倉庫圖表</span></a></li>
              @endcan      

              @can('DASHBOARDCHARTOWNER')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/dashboardChartOwner') }}"><i class="fa fa-pie-chart"></i> <span>Dashboard - 貨主圖表</span></a></li>
              @endcan
              
              @can('DASHBOARDTEXT')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/dashboardText') }}"><i class="fa fa-dashboard"></i> <span>Dashboard - 數據</span></a></li>
              @endcan
              
              @can('DASHBOARDTEXT2')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/dashboardText2') }}"><i class="fa fa-exclamation-circle"></i> <span>Dashboard - 數據2</span></a></li>    
              @endcan
            </ul>
          </li>
          @endcan

          @can('ANNOUNCEMANAGE')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>公告及訊息管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('ANNOUNCE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/announceProfile') }}"><i class="fa fa-newspaper-o"></i> <span>{{ trans('menu.bulletin') }}</span></a></li>
              @endcan

              @can('BASEMESSAGE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/basemessage') }}"><i class="fa fa-commenting"></i> <span>推播訊息</span></a></li>
              @endcan

              @can('SYSMESSAGE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/sysmessage') }}"><i class="fa fa-volume-up"></i> <span>系統推播</span></a></li>
              @endcan
              @can('FCM')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/fcm') }}"><i class="fa fa-volume-up"></i> <span>推播紀錄</span></a></li>
              @endcan
            </ul>
          </li>
          @endcan
         
          @can('SYSCONTROL')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>使用者及權限</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('USER')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/user') }}"><i class="fa fa-user"></i> <span>使用者管理(Admin)</span></a></li>
              @endcan

              @can('CUSTUSER')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/custuser') }}"><i class="fa fa-user"></i> <span>使用者管理</span></a></li>
              @endcan

              @can('LAYOUTSETTING')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/layout') }}"><i class="fa fa-clone"></i> <span>Layout管理</span></a></li>
              @endcan

              @can('ROLE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/role') }}"><i class="fa fa-group"></i> <span>{{trans('roles.titleName')}}</span></a></li>
              @endcan

              @can('PERMISSION')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/permission') }}"><i class="fa fa-key"></i> <span>{{trans('permissions.titleName')}}</span></a></li> 
              @endcan

              @can('LANGUAGE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/language') }}"><i class="fa fa-language"></i> <span>{{trans('language.titleName')}}</span></a></li> 
              @endcan
            </ul>
          </li>
          @endcan

          @can('BSCODE')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.basicInfo') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

              @can('BSCODEKIND')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/bscodeKind') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.bscode') }}</span></a></li>
              @endcan

              @can('WAREHOUSEPROFILE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/warehouseProfile') }}"><i class="fa fa-university"></i> <span>倉庫建檔</span></a></li>
              @endcan

              @can('CARTRADERPROFILE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/carTraderProfile') }}"><i class="fa fa-truck"></i> <span>車商建檔</span></a></li>
              @endcan

              @can('CUSTOMERPROFILE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/customerProfile') }}"><i class="fa fa-id-card-o"></i> <span>{{ trans('menu.customerProfile') }}</span></a></li>
              @endcan

              @can('CUSTOMERGROUP')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/customerGroup') }}"><i class="fa fa-group"></i> <span>客戶群組建檔</span></a></li>
              @endcan

              @can('SYSAREA')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/sysArea') }}"><i class="fa fa-map"></i> <span>{{ trans('menu.sysArea') }}</span></a></li>
              @endcan

              @can('ADDRESSFORMATE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/addressformate') }}"><i class="fa fa-map-marker"></i> <span>地址正規化</span></a></li>
              @endcan

              @can('CARBONCAL')
              <li><a href="{{ url(config('bcackpack.base.route_prefix', '') . '/carboncal') }}"><i class="fa fa-cloud"></i> <span>碳排計算設定</span></a></li>
              @endcan

              @can('COMPANYPROFILE')
              {{-- <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/companyProfile') }}"><i class="fa fa-building-o"></i> <span>{{ trans('menu.groupSetting') }}</span></a></li> --}}
              @endcan

              @can('MAILFORMAT')
              {{-- <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mailFormat') }}"><i class="fa fa-envelope"></i> <span>{{ trans('menu.mailFormat') }}</span></a></li> --}}
              @endcan

              @can('SYSCOUNTRY')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/sysCountry') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.sysCountry') }}</span></a></li>
              @endcan

              @can('MODTRANSSTATUS')
              {{-- <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/modTransStatus') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.modTransStatus') }}</span></a></li>         --}}
              @endcan
            </ul>
          </li>
          @endcan

          @can('MARKMANAGE')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>碼頭管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('WHARFSTATUS')
                <li><a href="{{ url(config('backpack.base.route_prefix', '').'/wharfStatus') }}"><i class="fa fa-list"></i> <span>碼頭使用狀態</span></a></li>
              @endcan
              @can('MARKSETTING')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/marksetting') }}"><i class="fa fa-cog"></i> <span>碼頭設定</span></a></li>
              @endcan
              @can('TRUCKCARTRADERPROFILE')
              <li><a href="{{ url(config('backpacx rk.base.route_prefix', '') . '/truckcarTraderProfile') }}"><i class="fa fa-truck"></i> <span>車商碼頭預約</span></a></li>
              @endcan
              @can('WHARFRESERVE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/wharfReserve') }}"><i class="fa fa-calendar-o"></i> <span>碼頭預約</span></a></li>
              @endcan
              @can('MARKPROFILE')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/markProfile') }}"><i class="fa fa-plus"></i> <span>碼頭建檔</span></a></li>
              @endcan
            </ul>
          </li>
          @endcan

          @can('PALLETMANAGE')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('mod_pallet_manage.titleName') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('PALLETMANAGEWAREHOUSE')
                <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletManageWarehouse') }}"><i class="fa fa-university"></i> <span>棧板管理(倉庫)</span></a></li>
              @endcan
              @can('PALLETMANAGEDRIVER')
                <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletManageDriver') }}"><i class="fa fa-truck"></i> <span>棧板管理(司機)</span></a></li>
              @endcan
              @can('PALLETMANAGECUST')
                <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletManageCust') }}"><i class="fa fa-user"></i> <span>棧板管理(客戶)</span></a></li>
              @endcan
              @can('PALLET')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletProfile') }}"><i class="fa fa-plus"></i> <span>棧板建檔</span></a></li>
              @endcan
            </ul>
          </li>
          @endcan

          {{-- @can('ORDER')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.orderBasic') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/importdssorder') }}"><i class="fa fa-file"></i> <span>dss{{ trans('menu.orderImport') }}</span></a></li>
            </ul>
          </li>
          @endcan --}}

          @can('TRANSPORTATIONPLAN')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>配送管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('SENDCAR')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/dlvProfile') }}"><i class="fa fa-file-text-o"></i> <span>TMS派車單</span></a></li> 
              @endcan
              @can('ORDERMGMT')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/OrderMgmt') }}"><i class="fa fa-list-alt"></i> <span>訂單資訊</span></a></li>
              @endcan
              @can('CLOSEORDER')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/closeOrder') }}"><i class="fa fa-list-alt"></i> <span>結案作業</span></a></li>
              @endcan
              @can('DLVPLANSEARCH')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/dlvPlanSearch') }}"><i class="fa fa-sitemap"></i> <span>{{ trans('menu.dlvPlanSearch') }}</span></a></li> 
              @endcan
              @can('CHECKIN')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/checkin') }}"><i class="fa fa-check-square-o"></i> <span>司機報到</span></a></li>
              @endcan
              @can('DELIVERYLOG')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/deliverylog') }}"><i class="fa fa-check-square-o"></i> <span>放行紀錄</span></a></li>
              @endcan
              @can('DLVCARNEW')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/dlvCarnew') }}"><i class="fa fa-truck"></i> <span>{{ trans('menu.sendCarDetail') }}</span></a></li>
              @endcan

              @can('SECURITY')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/security') }}"><i class="fa fa-shield"></i> <span>警衛放行</span></a></li>
              @endcan

            </ul>
          </li>  
          @endcan
          {{-- @can('QUOTE')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.quotMgmt') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/sysRefFeeCar') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.sysRefFeeCar') }}</span></a></li>
            </ul>
          </li>
          @endcan --}}
          @can('ERRORCONTROL')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>異常管理</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('ERRORPROCESS')
              <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/errorprocess') }}"><i class="fa fa-pencil-square-o"></i> <span>異常處理</span></a></li>
              @endcan
              @can('ABNORMAL')
              <li><a href="{{ url(config('backpack.base.route_prefix', '').'/abnormal') }}"><i class="fa fa-exclamation-circle"></i> <span>異常建檔</span></a></li>
              @endcan
            </ul>
          </li>
          @endcan



          <!-- ======================================= -->
          <li class="header">使用者</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', '').'/logout') }}"><i class="fa fa-sign-out"></i> <span>登出</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
    <div class="new-message-box">
    </div>
    <style>
      .num-box {
        position: absolute;
        right: 15px;
        width: 24px;
        height: 24px;
        background: #dd4b39;
        color: #fff;
        font-weight: bold;
        border-radius: 50%;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        overflow: hidden;
      }
      .new-message-box {
        position: fixed;
        right: 16px;
        top: 16px;
        z-index: 10000;
      }
      .new-message-box .new-message-item {
        display: flex;
        padding: 16px;
        background: #fff;
        border-left: 5px solid #00c0ef;
        box-shadow: 2px 2px 10px #0000004d;
        width: 240px;
        margin-bottom: 12px;
        position: relative;
        opacity: 1;
        transition: all 0.2;
      }
      .new-message-box .icon-box {
        font-size: 45px;
        margin-right: 16px;
        color: #00c0ef;
      }
      .new-message-box .new-message-content {
        font-size: 18px;
      }
      .new-message-box .new-message-time {
        color: #666;
      }
      .new-message-box .message-close-btn button {
        position: absolute;
        right: 4px;
        top: 8px;
        background: transparent;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        color: #97a0b3;
      }
      .new-message-box .message-close-btn button:hover, .new-message-box .message-close-btn button:focus, .new-message-box .message-close-btn button:active {
        border: 0px solid transparent;
        outline: 0px solid transparent;
      }
    </style>
@endif

<!-- <link rel="stylesheet" href="{{ asset('vendor/jquery') }}/jquery-ui.min.css">
<link rel="stylesheet" href="{{ asset('vendor/jqwidgets') }}/styles/jqx.bootstrap.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/pnotify/pnotify.custom.min.css">
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.0.min.js"><\/script>')</script>
<script src="{{ asset('vendor/adminlte') }}/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('js/core') }}/edit-var.js"></script>
@include('layout.inc.alerts') -->
@section('sidebar_scripts')
<script>
  /* 進入頁面開啟目前頁面右邊選單所屬分類 */
  let locationHref = location.href.split('/')[4];
  let treeviewList = document.querySelectorAll('.treeview');
  let treeviewListLength = treeviewList.length;
  let activeIndex = null;
  for (let i = 0; i < treeviewListLength; i++) {
    // console.log(treeviewList[i].children[1].children);
    let ulList = treeviewList[i].children[1].children;
    let ulListLength = ulList.length;
    // console.log(ulList);
    for (let index = 0; index < ulListLength; index++) {
      let link = ulList[index].children[0].href.split('/')[3]
      if (locationHref.indexOf(link) !== -1) {
        activeIndex = i;
        ulList[index].children[0].style.color = '#fff';
      }
    }
  }
  // console.log(activeIndex);
  if (activeIndex !== null) {
    treeviewList[activeIndex].classList.add('active');
  }
  /* 進入頁面開啟目前頁面右邊選單所屬分類 END */
</script>
<script type="text/javascript">
  // 手動關閉右上提醒按鈕監聽事件
  function setMessageCloseBtn () {
    let newMessageItemList = document.querySelectorAll('.new-message-item') || [];
    let messageCloseBtnList = document.querySelectorAll('.message-close-btn') || [];
    let messageCloseBtnListLength = messageCloseBtnList.length;
    for (let i = 0; i < messageCloseBtnListLength; i++) {
      messageCloseBtnList[i].addEventListener('click', function (e) {
        e.stopPropagation();
        newMessageItemList[i].style.opacity = 0;
        setTimeout(() => {
            newMessageItemList[i].style.display = 'none';
          });
        }, 1000);
    }
  }
  // 新增右上提醒
  let removeTimeOut = 10000; // 設定消除時間
  function addNewMessageItem (data, removeTimeOut) {
    let newItem = document.createElement('div');
    let newItemId = 'newMessageItem' + Number(data.id)
    newItem.id = newItemId;
    newItem.classList.add('new-message-item');
    let newItemContent = '\
      <div class="icon-box"><i class="fa fa-envelope"></i></div>\
      <div>\
        <div class="new-message-title"> 來自' + data.created_by + '的新訊息</div>\
        <div class="new-message-content">' + data.content + '</div>\
        <div class="new-message-time">' + data.created_at + '</div>\
      </div>\
      <div class="message-close-btn"><button type="button"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
    newItem.innerHTML = newItemContent;
    let messageBox = document.querySelector('.new-message-box');
    messageBox.insertBefore(newItem, messageBox.firstChild);
    setMessageCloseBtn();
    setTimeout(() => {
      document.getElementById(newItemId).style.opacity = 0;
      messageBox.removeChild(document.getElementById(newItemId));
    }, removeTimeOut);
  }
  // 更新未讀數字
  function updateUnReadCount (num) {
    document.getElementById('unReadCount').textContent = Number(num);
  }
</script>
<script>
    /* websocket test */
    initWebsocket();
    function initWebsocket () {
        let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/'+"{{env('WEBSOCKET_URL')}}" + '{{Auth::user()->id}}';
        let ws = new WebSocket(wsURL); // 建立連線
        ws.onopen = function(e) {
            // websocketonopen(e);
            // console.log('ws 連線成功~~');
        };
        ws.error = function(error) {
            // websocketonerror(error);
            // console.error('ws 連線失敗', error);
        };
        ws.onmessage = function(e) {
            websocketonmessage(e);
        };
        ws.onclose = function(e) {
            setTimeout(function() {
                initWebsocket();
            }, 1000);
            if (e.wasClean) {
                websocketclose(e);
            } else {
                console.log(e.code);
            }
        };
    }
    // function websocketonopen (e) {
    //     console.log('ws 連線成功~~');
    // }
    // function websocketonerror (error) {
    //     console.error('ws 連線失敗', error);
    // }
    function websocketonmessage (e) {
        // 後端通知前端，前端取得資料
        // console.log(e);
        let _data = e.data;
        console.log('ws 取得資料', _data);
        if (_data.indexOf('{') !== -1) {
          let dataObj = JSON.parse('[' + _data.split('=').join('":"').split(', ').join('","').split('{').join('{"').split('}"').join('}').split('"{').join('{').split('}').join('"}') + ']');
          console.log('ws 取得資料', _data);
          console.log(dataObj);
          // 新增右上提醒
          addNewMessageItem(dataObj[0].newData, removeTimeOut);
          // 更新未讀數字
          updateUnReadCount(dataObj[0].unReadCount);
        }
    }
    function websocketsend (data) {
        // 前端丟資料
        console.log('send data', data);
    }
    function websocketclose () {
        console.log('ws 關閉連線');
    }
</script>
@endsection
