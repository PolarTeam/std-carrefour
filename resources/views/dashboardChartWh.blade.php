@extends('layout.layout')

@section('header')
<section class="content-header d-flex justify-content-between">
    <h1 style='display: inline;'>
        Dashboard - 倉庫圖表<small></small>
        <div class="d-flex justify-content-between" style="display: inline-block;">
            <ul class="list-unstyled owner-list" id="chartOwner-page-list">
                <li ><button style="display: none;" type="button" id ="WHA0" class="whlist">台北倉</button></li>
                <li ><button style="display: none;" type="button" id ="WHB0" class="whlist">觀音一倉</button></li>
                <li ><button style="display: none;" type="button" id ="WHC0" class="whlist">台中倉</button></li>
                <li ><button style="display: none;" type="button" id ="WHD0" class="whlist">高雄倉</button></li>
                <li ><button style="display: none;" type="button" id ="WHE0" class="whlist">特品倉</button></li>
            </ul>
        </div>
    </h1>
    <div class="d-flex">
        <button type="button" class="blue-sm-btn" id="selectWhBtn">篩選倉庫</button>
        <ul class="list-unstyled page-list" id="chartWh-page-list">
        </ul>
        <input type="date" name="" id="searchWhChartByDay" onchange="dateChanged()" value="{{date('Y-m-d')}}" style="height: 30px">
    </div>
</section>
@endsection

<link rel="stylesheet" href="https://dss.target-ai.com/vendor/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://dss.target-ai.com/vendor/adminlte/plugins/select2/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://dss.target-ai.com/vendor/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://dss.target-ai.com/vendor/adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="https://dss.target-ai.com/vendor/adminlte/plugins/pace/pace.min.css">
<link rel="stylesheet" href="https://dss.target-ai.com/vendor/backpack/pnotify/pnotify.custom.min.css">

<link rel="stylesheet" href="https://dss.target-ai.com/vendor/backpack/backpack.base.css">

<link rel="stylesheet" href="https://dss.target-ai.com/css/custom.css?v=20190305-002">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://dss.target-ai.com/vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<div class="row dashboard-chart-box">
    <div class="col-lg-4">
        <div class="chart-title">總量 - <span id="totalwarehouse">{{$viewData['totalwarehouse']}}</span></div>
        <div class="card">
            <canvas id="chartToday" class="chart-today" style="height: calc(100% - 295px); width:100%"></canvas>
            <div class="color-list-box">
                <ul class="list-unstyled color-list">
                    <li>揀貨完成</li>
                    <li>貨物裝載中</li>
                    <li>提貨完成</li>
                    <li>貨物卸載中</li>
                    <li>配送完成</li>
                </ul>
                <ul class="list-unstyled color-list" id="percent_totalwarehousedata">
<<<<<<< HEAD
                    <li>{{ empty($viewData['totalwarehousedata']['PICKEDFINISH']) ? 0 :   round(($viewData['totalwarehousedata']['PICKEDFINISH'] / $viewData['totalwarehouse']),1) *100    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['LOADING'])  ? 0 : round(($viewData['totalwarehousedata']['LOADING'] / $viewData['totalwarehouse']),1) *100    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['PICKED'])  ? 0 :  round(($viewData['totalwarehousedata']['PICKED'] / $viewData['totalwarehouse']),1) *100    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['OFFSTAFF']) ? 0 :  round(($viewData['totalwarehousedata']['OFFSTAFF'] / $viewData['totalwarehouse']),1) *100    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['FINISHED']) ? 0 :   round(($viewData['totalwarehousedata']['FINISHED'] / $viewData['totalwarehouse']),1) *100    }}%</li>
=======
                    <li>{{ empty($viewData['totalwarehousedata']['PICKEDFINISH']) ? 0 :    number_format(($viewData['totalwarehousedata']['PICKEDFINISH'] / $viewData['totalwarehouse']) * 100, 1)    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['LOADING'])  ? 0 :  number_format(($viewData['totalwarehousedata']['LOADING'] / $viewData['totalwarehouse']) * 100, 1)    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['PICKED'])  ? 0 :  number_format(($viewData['totalwarehousedata']['PICKED'] / $viewData['totalwarehouse']) *100, 1)    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['OFFSTAFF']) ? 0 :  number_format(($viewData['totalwarehousedata']['OFFSTAFF'] / $viewData['totalwarehouse']) *100, 1)    }}%</li>
                    <li>{{ empty($viewData['totalwarehousedata']['FINISHED']) ? 0 :   number_format(($viewData['totalwarehousedata']['FINISHED'] / $viewData['totalwarehouse']) *100, 1)    }}%</li>
>>>>>>> ecd0d335dc00f72d80220bfef5e3614a056d96e7
                </ul>
                <ul class="list-unstyled color-list" id="num_totalwarehousedata">
                    <li>{{$viewData['totalwarehousedata']['PICKEDFINISH']}}</li>
                    <li>{{$viewData['totalwarehousedata']['LOADING']}}</li>
                    <li>{{$viewData['totalwarehousedata']['PICKED']}}</li>
                    <li>{{$viewData['totalwarehousedata']['OFFSTAFF']}}</li>
                    <li>{{$viewData['totalwarehousedata']['FINISHED']}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-8 h-100">
        <div class="row h-100">
            @foreach($viewData['managewarehouse'] as $managewarehouserow)
            <div class="col-lg-6 h-50" id="chartWhBox{{$managewarehouserow->cust_no}}">
                <div class="chart-title">{{$managewarehouserow->cname}} - <span id="chartTitle{{$managewarehouserow->cust_no}}">{{$managewarehouserow->total_count}}</span></div>
                <div class="card mb-3">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col-lg-5 p-0">
                            <canvas id="chartWh{{$managewarehouserow->cust_no}}" style="height:100%; width:100%"></canvas>
                        </div>
                        <div class="col-lg-7">
                            <div class="color-list-box">
                                <ul class="list-unstyled color-list">
                                    <li>揀貨完成</li>
                                    <li>貨物裝載中</li>
                                    <li>提貨完成</li>
                                    <li>貨物卸載中</li>
                                    <li>配送完成</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="percent{{$managewarehouserow->cust_no}}">
<<<<<<< HEAD
                                    <li>{{ empty( $managewarehouserow->statuspickedfinish) || empty( $managewarehouserow->total_count) ? 0 : round($managewarehouserow->statuspickedfinish / $managewarehouserow->total_count,1) *100}}%</li>
                                    <li>{{ empty( $managewarehouserow->statusloading) || empty( $managewarehouserow->total_count) ? 0 : round($managewarehouserow->statusloading  / $managewarehouserow->total_count,1)  *100}}%</li>
                                    <li>{{ empty( $managewarehouserow->statuspicked) || empty( $managewarehouserow->total_count) ? 0 : round($managewarehouserow->statuspicked / $managewarehouserow->total_count,1)  *100}}%</li>
                                    <li>{{ empty( $managewarehouserow->statusoffloading) || empty( $managewarehouserow->total_count) ? 0 : round($managewarehouserow->statusoffloading / $managewarehouserow->total_count,1) *100 }}%</li>
                                    <li>{{ empty( $managewarehouserow->statusfinished) || empty( $managewarehouserow->total_count) ? 0 : round($managewarehouserow->statusfinished / $managewarehouserow->total_count,1) *100 }}%</li>
=======
                                    <li>{{ empty( $managewarehouserow->statuspickedfinish) || empty( $managewarehouserow->total_count) ? 0 : number_format(($managewarehouserow->statuspickedfinish / $managewarehouserow->total_count) * 100, 1, '.', '')  }}%</li>
                                    <li>{{ empty( $managewarehouserow->statusloading) || empty( $managewarehouserow->total_count) ? 0 : number_format(($managewarehouserow->statusloading  / $managewarehouserow->total_count) * 100, 1, '.', '')}}%</li>
                                    <li>{{ empty( $managewarehouserow->statuspicked) || empty( $managewarehouserow->total_count) ? 0 : number_format(($managewarehouserow->statuspicked / $managewarehouserow->total_count)  * 100, 1, '.', '')}}%</li>
                                    <li>{{ empty( $managewarehouserow->statusoffloading) || empty( $managewarehouserow->total_count) ? 0 : number_format(($managewarehouserow->statusoffloading / $managewarehouserow->total_count) * 100, 1, '.', '') }}%</li>
                                    <li>{{ empty( $managewarehouserow->statusfinished) || empty( $managewarehouserow->total_count) ? 0 : number_format(($managewarehouserow->statusfinished / $managewarehouserow->total_count)* 100, 1, '.', '')}}%</li>
>>>>>>> ecd0d335dc00f72d80220bfef5e3614a056d96e7
                                </ul>
                                <ul class="list-unstyled color-list" id="num{{$managewarehouserow->cust_no}}">
                                    <li>{{$managewarehouserow->statuspickedfinish}}</li>
                                    <li>{{$managewarehouserow->statusloading}}</li>
                                    <li>{{$managewarehouserow->statuspicked}}</li>
                                    <li>{{$managewarehouserow->statusoffloading}}</li>
                                    <li>{{$managewarehouserow->statusfinished}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>

</div>

<style>
    .blue-sm-btn {
        font-size: 14px;
        color: #fff;
        background: #19294A;
        padding: 4px 10px;
        border-radius: 8px;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        margin-bottom: 8px;
        margin-left: 16px;
        white-space: nowrap;
    }
    .owner-list {
        display: flex;
        overflow-x: auto;
        padding: 0 20px;
    }
    .owner-list button {
      background: transparent;
      border: 0px solid transparent;
      outline: 0px solid transparent;
      font-size: 16px;
      color: #999999;
      font-weight: 400;
      margin: 0 16px 16px 0;
      padding: 0 2px;
      white-space: nowrap;
    }
    .owner-list .active {
        color: #f47920;
        font-weight: bold;
        border-bottom: 1px solid #f47920;
    }
    .align-items-end {
        align-items: flex-end;
    }
    .orange-btn {
        color: #F47920;
        border: 1px solid #F47920;
        border-radius: 12px;
        background: #fff;
        font-size: 16px;
        padding: 12px 24px;
        margin: 0 4px 8px 8px;
    }
    .swal2-content .title {
        font-size: 20px;
        color: #333333;
        font-weight: 400;
    }
    .swal2-content .sub-title {
        font-size: 16px;
        color: #999999;
        font-weight: 400;
    }
    .swal2-content .sub-title .num {
        color: #3079ae;
    }
    .swal2-content label {
        font-size: 14px;
        color: #666666;
        font-weight: 400;
    }
    .swal2-content input {
        font-size: 14px;
        border: 1px solid #efeff1;
        padding: 12px;
        color: #333333;
        border-radius: 4px;
        width: 100%;
    }
    .swal2-content .autocomplet {
        width: 100%;
    }
    .swal2-content .autocomplet .search-btn {
        border: 1px solid #efeff1;
        color: #333333;
        border-radius: 4px;
        width: 44px;
        height: 44px;
        background: #fff;
        font-size: 14px;
        display: inline-block;
    }
    .swal2-content .autocomplet input {
        width: calc(100% - 49px);
        display: inline-block;
    }
    .swal2-content .select {
        position: relative;
    }
    .swal2-content .select select {
        font-size: 14px;
        border: 1px solid #efeff1;
        padding: 12px;
        color: transparent;
        border-radius: 4px;
        width: 100%;
        background: transparent;
        position: relative;
        z-index: 3;
    }
    .swal2-content .select::before {
        content: "\f107";
        font-family: "FontAwesome";
        font-weight:400;
        display: block;
        position: absolute;
        top: 50%;
        right: 20px;
        color: #c5c8ce;
        font-size: 20px;
        transform: translateY(-50%);
        z-index: 2;
    }
    .swal2-content .select p {
        position: absolute;
        top: 50%;
        left: 0;
        transform: translateY(-50%);
        color: #333333;
        font-size: 14px;
        z-index: 1;
        border: 1px solid #efeff1;
        padding: 12px;
        border-radius: 4px;
        background: #fff;
        height: 100%;
        width: 100%;
    }
    .swal2-content .border-bottom {
        padding-bottom: 16px;
        margin-bottom: 16px;
        border-bottom: 1px solid #D2D6DE;
    }
    .swal2-content .can-remove-list {
        text-align: start;
        max-height: 400px;
        overflow-y: auto;
    }
    .swal2-content .can-remove-list li {
        color: #444444;
        font-size: 16px;
        margin-bottom: 24px;
    }
    .swal2-content .can-remove-list button {
        background: transparent;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        color: #EA2020;
        font-size: 16px;
        display: inline-block;
        margin-right: 16px;
    }
    .mr-2 {
        margin-right: 8px;
    }
    .blue-sm-btn {
        font-size: 14px;
        color: #fff;
        background: #19294A;
        padding: 4px 10px;
        border-radius: 8px;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        margin-bottom: 8px;
    }
    .h-100 {
        height: 100%;
    }
    .h-50 {
        height: 50%;
    }
    .p-0 {
        padding: 0!important;
    }
    .mb-3 {
        margin-bottom: 16px;
    }
    .d-flex {
        display: flex;
    }
    .justify-content-between {
        justify-content: space-between;
    }
    .align-items-center {
        align-items: center;
    }
    .dashboard-chart-box {
        height: calc(100% - 122px);
    }
    .chart-title {
        font-size: 20px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .card {
        background: #fff;
        border-radius: 8px;
        padding: 20px;
        height: calc(100% - 40px);
    }
    .chart-today {
        margin-bottom: 20px;
    }
    .color-list-box {
        display: flex;
        justify-content: space-between;
    }
    .color-list-box.today {
        height: calc(100% - 418px);
        align-items: center;
    }
    .color-list {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    .color-list li {
        font-size: 25px;
        font-weight: bold;
        margin-bottom: 20px;
    }
    .color-list:first-of-type li {
        padding-left: 20px;
        position: relative;
    }
    .color-list:first-of-type li:before {
        content: "";
        display: block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
    }
    .color-list:last-of-type li {
        text-align: right;
    }
    .color-list li:nth-of-type(5n + 1) {
        color: #9C27B0;
    }
    .color-list:first-of-type li:nth-of-type(5n + 1):before {
        background: #9C27B0;
    }
    .color-list li:nth-of-type(5n + 2) {
        color: #e80c0c;
    }
    .color-list:first-of-type li:nth-of-type(5n + 2):before {
        background: #e80c0c;
    }
    .color-list li:nth-of-type(5n + 3) {
        color: #F47920;
    }
    .color-list:first-of-type li:nth-of-type(5n + 3):before {
        background: #F47920;
    }
    .color-list li:nth-of-type(5n + 4) {
        color: #333;
    }
    .color-list:first-of-type li:nth-of-type(5n + 4):before {
        background: #ffc423;
    }
    .color-list li:nth-of-type(5n + 5) {
        color: #13ab67;
    }
    .color-list:first-of-type li:nth-of-type(5n + 5):before {
        background: #13ab67;
    }
    .page-list {
        display: flex;
    }
    .page-list button {
        background: transparent;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        font-size: 16px;
        color: #999999;
        margin-right: 16px;
    }
    .page-list .active {
        color: #f47920;
        font-weight: bold;
    }
    @media (max-width: 1440px) {
        .color-list li {
            font-size: 14px;
            margin-bottom: 12px;
        }
    }
</style>




@endsection

@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    let allwh = JSON.parse(('{{$viewData["see_wh"]}}').split('&quot;').join('"'));
    console.log(allwh)
    // 總量
    var lastentrydata = "";
    let ownerList = '';
    let selectWhList = [];
    let initwh = "";
    let initWh_ = [];
    let whname = {
        'A0': '台北倉',
        'B0': '觀音一倉',
        'C0': '台中倉',
        'D0': '高雄倉',
        'E0': '特品倉'
    };
    let showarray = [
        'A0',
        'B0',
        'C0',
        'D0',
        'E0'
    ];
    let ownerAllData = "";
    let ownerRow = [];
    $(document).ready(function() {
        window.setTimeout(function() {
            try {
                document.getElementById(`chartWhBoxA0`) == null ? '' : document.getElementById('chartWhBoxA0').style.display = 'none';
                document.getElementById(`chartWhBoxB0`) == null ? '' : document.getElementById('chartWhBoxB0').style.display = 'none';
                document.getElementById(`chartWhBoxC0`) == null ? '' : document.getElementById('chartWhBoxC0').style.display = 'none';
                document.getElementById(`chartWhBoxD0`) == null ? '' : document.getElementById('chartWhBoxD0').style.display = 'none';
                document.getElementById(`chartWhBoxE0`) == null ? '' : document.getElementById('chartWhBoxE0').style.display = 'none';
            } catch (error) {
                
            }

            @if(isset($viewData['wharray']))
                initwh = "{{$viewData['wharray']}}";
            @endif
            initWh_ = initwh.split(',');
            initWh_.forEach((element, index) => {
                if(element != '') {
                    let whcode = element.substr(2,2);
                    document.getElementById(`chartWhBox`+whcode).style.display = 'block';
                    document.getElementById(`WH`+whcode).style.display = 'block';

                    ownerList += `
                    <li id="li-owner-${whcode}" style='display:'block'>
                    <button type='button' id="owner-${whcode}" data-key="${whcode}"><i class='fa fa-times' aria-hidden='true'></i></button>
                    <span>${whname[whcode]}</span>
                    </li>`
                }
            });
        }, 1000);
    });


    let selectWhBtn = document.getElementById('selectWhBtn');
    selectWhBtn.addEventListener('click', function() {
        console.log('selectWhBtn');
        let whoption = '';
        allwh.forEach((item, index) => {
            whoption += ` <option value='${item.cust_no}'>${item.cname}</option>`;
        });
        let ordOwnerHtml = 
        "<div class='col-md-12' style='text-align: left';><label class='title'>篩選倉庫</label></div>\
        <div class='col-md-12'>\
        <div class='row d-flex align-items-end border-bottom'>\
            <div class='col-md-6' style='text-align: left';><label>倉庫</label>\
                <div class='form-group'>\
                    <input type='hidden' name='check_owner_name' id = 'check_owner_name' class='form-control'>\
                    <select class='form-control select2' id='check_owner' name='check_owner' multiple='multiple'>\
                        "+whoption+
                    "</select>\
                </div>\
            </div>\
            <div class='col-md-6 d-flex align-items-end'>\
            <button type='button' class='orange-btn' id='ownerAddBottom'>新增</button>\
            <button type='button' class='orange-btn' id='ownerClearAllBottom'>全部取消</button>\
            </div>\
        </div>\
        </div>\
        <div class='col-md-12'>\
        <ul class='list-unstyled can-remove-list' id='can-remove-list'>" + ownerList + "</ul>\
        </div>"
        ;
        swal({
        title            : '',
        icon             : 'info',
        html             : ordOwnerHtml,
        width            : '40%',
        showCloseButton  : true,
        showCancelButton : false,
        focusConfirm     : false,
        showConfirmButton: false,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        onOpen: function () {
            $('#check_owner').select2();
        },
        }).then((result) => {
            console.log('confirm do status');
            dateChanged();
        });
        // 監聽刪除按鈕
        initWh_.forEach((element, index) => {
            setRemoveBtnEvent(element, index);
        });
        function setRemoveBtnEvent(item, index) {
            let code = item.substr(2,2);
            if(code != '') {
                try {
                    if(document.getElementById(`owner-${code}`) != null ){
                        document.getElementById(`owner-${code}`).addEventListener('click', function() {
                            document.getElementById(`li-owner-${code}`).style.display = 'none';
                            document.getElementById(`WH`+code).style.display = 'none';
                            document.getElementById(`chartWhBox`+code).style.display = 'none';
                            ownerList = ''
                            let initWhTmp = initWh_;
                            initWh_ = [];
                            initWhTmp.forEach(wh => {
                                let wh_ = wh.substr(2,2);
                                if (wh != item) {
                                    initWh_.push(wh);
                                    ownerList += `
                                    <li id="li-owner-${wh_}" style='display:'block'>
                                    <button type='button' id="owner-${wh_}" data-key="${wh_}"><i class='fa fa-times' aria-hidden='true'></i></button>
                                        <span>${whname[wh_]}</span>
                                    </li>`
                                    ;
                                }
                            })
                            dateChanged();
                        })
                    }
                } catch (error) {
                    console.log(error);
                }
            }
        }
        //  監聽新增按鈕
        document.getElementById('ownerAddBottom').addEventListener('click', function() {
            // console.log($("#check_owner").val());
            let selectArr = $("#check_owner").val() || [];
            el = document.getElementById(`can-remove-list`);
            ownerList = el.innerHTML ; 

            selectArr.forEach((item, index) => {
                let hasIndex = showarray.findIndex((itm) => itm ===  index);
                if(!initWh_.includes('WH'+item)) {
                    if (hasIndex === -1) {
                        ownerRow.push(whname[item]);
                        ownerList += `
                        <li id="li-owner-${item}" style='display:'block'>
                        <button type='button' id="owner-${item}" data-key="${item}"><i class='fa fa-times' aria-hidden='true'></i></button>
                            <span>${whname[item]}</span>
                        </li>`
                        ;
                        initWh_.push('WH'+item);
                    }
                    document.getElementById(`chartWhBox`+item).style.display = 'block';
                    document.getElementById(`WH`+item).style.display = 'block';
                    setRemoveBtnEvent('WH'+item, index);
                }
            });
            el.innerHTML = ownerList;
            $("#check_owner").val('');
            $("#check_owner").trigger('change');
            showarray.forEach((item, index) => {
                if(document.getElementById(`owner-${item}`)!= null ) {
                    document.getElementById(`owner-${item}`).addEventListener('click', function() {
                        document.getElementById(`li-owner-${item}`).style.display = 'none';
                        document.getElementById(`WH`+item).style.display = 'none';
                        document.getElementById(`chartWhBox`+item).style.display = 'none';
                    });
                }
            });
            if(ownerRow.length >=5 && allwh.length >=5) {
                document.getElementById(`chartWh-page-2`).style.display = 'block';
            } else if (allwh.length >=5 ) {
                document.getElementById(`chartWh-page-2`).style.display = 'none';
            }
            dateChanged();
        })
        // 監聽全部取消按鈕
        document.getElementById('ownerClearAllBottom').addEventListener('click', function() {
            allwh.forEach((item, index) => {
                document.getElementById(`chartWhBox`+item.cust_no).style.display = 'none';
                document.getElementById('WH'+item.cust_no).style.display = 'none';

            });
            ownerList  = "";
            ownerRow = [];
            initWh_ = [];
            try {
                el = document.getElementById(`can-remove-list`);
                el.innerHTML = "";
            } catch (error) {
                console.log(error);
            }

            $('#check_owner').val('');
            $('#check_owner').trigger('change');
        })

    });
    function hideOwner(index) {
        console.log('123');
        const li = document.getElementById(`li-owner-${index}`);
        li.style.display = 'none';
    }
    // searchWhChartByDay
    function dateChanged() {
        var input = document.getElementById("searchWhChartByDay");
        console.log("Selected date: " + input.value);
        var elements = document.getElementsByClassName('whlist');
        let wharray = [];
        for (let index = 0; index < elements.length; index++) {
            if(elements[index].style.display != 'none') {
                wharray.push(elements[index].id);
            }
        }
        $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: BASE_URL + '/zh-TW/dashboardChartWhbydate',
            data:JSON.stringify({
                "date"             : input.value,
                "wharray"             : wharray,
            }),
            success: function(data) {
                manageWarehouseRow = [];
                if(data.data.totalwarehouse == 0 && lastentrydata != input.value) {
                    alert(`${input.value} 目前尚無資料，請選擇其他日期`);
                    lastentrydata = input.value;
                    return
                }
                // location.href = BASE_URL + '/zh-TW/dashboardChartWhbydateview/'+input.value;
                let totalWhData = data.data.totalwarehousedata
                let totalwarehouse = data.data.totalwarehouse
                data.data.managewarehouse.forEach(item => {
                    if(initWh_.indexOf('WH'+item.cust_no) != -1) {
                        manageWarehouseRow.push(item)
                    } else {
                        totalwarehouse -= item.total_count
                        totalWhData.PICKEDFINISH -= item.statuspickedfinish
                        totalWhData.PICKED -= item.statuspicked
                        totalWhData.OFFSTAFF -= item.statusoffloading
                        totalWhData.LOADING -= item.statusloading
                        totalWhData.FINISHED -= item.statusfinished
                    }
                })
                data.data.totalwarehousedata = totalWhData
                data.data.totalwarehouse = totalwarehouse
                manageWarehouseRowLength = manageWarehouseRow.length;
                data.data.managewarehouse = manageWarehouseRow
                console.log(data.data)
                for (let i = 0; i < manageWarehouseRowLength; i++) {
                    updateConfigAsNewObject(i, manageWarehouseRow[i]);
                }
                updateTotalConfigAsNewObject(totalWhData, totalwarehouse);
                setPageBtnEventListener('chartWh', chartElList, pageSize);
                chengePageEvent('chartWh', chartElList, chartWhInPageNums, pageSize);
                if(wharray.length >=5) {
                    endChartWhCarouselEvent();
                    starChartWhCarouselEvent();
                }
            }
        });
    }
    var ctx = document.getElementById("chartToday");
    const data = {
        labels: [
            '揀貨完成',
            '貨物裝載中',
            '提貨完成',
            '貨物卸載中',
            '配送完成'
        ],
        datasets: [{
            label: 'My First Dataset',
            data: [50, 100, 150, 250, 400],
            backgroundColor: [
            '#9C27B0',
            '#e80c0c',
            '#F47920',
            '#ffc423',
            '#13ab67'
            ],
            hoverOffset: 4
        }]
    };
    var totalChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: {
            legend: {
                "display": false
            }
        },
    });
    totalChart.data.datasets[0].data = [Number('{{$viewData['totalwarehousedata']['PICKEDFINISH']}}'), Number('{{$viewData['totalwarehousedata']['LOADING']}}'), Number('{{$viewData['totalwarehousedata']['PICKED']}}'), Number('{{$viewData['totalwarehousedata']['OFFSTAFF']}}'), Number('{{$viewData['totalwarehousedata']['FINISHED']}}')]
    totalChart.update();

    var chartElList = [] // 圖表
    let manageWarehouseRow = JSON.parse(('{{$viewData["managewarehouse"]}}').split('&quot;').join('"'));
    let manageWarehouseRowLength = manageWarehouseRow.length;

    // 創建圖表
    for (let i = 0; i < manageWarehouseRowLength; i++) {
        let chartData = {
            labels: [
                '揀貨完成',
                '貨物裝載中',
                '提貨完成',
                '貨物卸載中',
                '配送完成'
            ],
            datasets: [{
                label: 'My First Dataset',
                data: [manageWarehouseRow[i].statuspickedfinish || 0, manageWarehouseRow[i].statusloading || 0, manageWarehouseRow[i].statuspicked || 0, manageWarehouseRow[i].statusoffloading || 0, manageWarehouseRow[i].statusfinished || 0],
                backgroundColor: [
                '#9C27B0',
                '#e80c0c',
                '#F47920',
                '#ffc423',
                '#13ab67'
                ],
                hoverOffset: 4
            }]
        };
        chartElList.push({
            id: manageWarehouseRow[i].cust_no,
            el: document.getElementById(`chartWh${manageWarehouseRow[i].cust_no}`),
            chart: new Chart(document.getElementById(`chartWh${manageWarehouseRow[i].cust_no}`), {
                type: 'doughnut',
                data: chartData,
                options: {
                    legend: {
                        "display": false
                    }
                },
            })
        });
    }

    // 更新總量圖表
    function updateTotalConfigAsNewObject(data, total) {
        let percent = document.getElementById(`percent_totalwarehousedata`).children;
        let num = document.getElementById(`num_totalwarehousedata`).children;
        let newChartData = [data.PICKEDFINISH || 0, data.LOADING || 0, data.PICKED || 0, data.OFFSTAFF || 0, data.FINISHED || 0];

        percent[0].textContent = `${data.PICKEDFINISH ? Math.round(data.PICKEDFINISH / total * 1000)/10 : 0}%`;
        percent[1].textContent = `${data.LOADING ? Math.round(data.LOADING / total * 1000)/10 : 0}%`;
        percent[2].textContent = `${data.PICKED ? Math.round(data.PICKED / total * 1000)/10 : 0}%`;
        percent[3].textContent = `${data.OFFSTAFF ? Math.round(data.OFFSTAFF / total * 1000)/10 : 0}%`;
        percent[4].textContent = `${data.FINISHED ? Math.round(data.FINISHED / total * 1000)/10 : 0}%`;
        var finalvalue = 0;
        num[0].textContent = data.PICKEDFINISH;
        num[1].textContent = data.LOADING;
        num[2].textContent = data.PICKED;
        num[3].textContent = data.OFFSTAFF;
        num[4].textContent = data.FINISHED;
        document.getElementById(`totalwarehouse`).textContent = total;
        // 更新圖表
        totalChart.data.datasets[0].data = newChartData;
        totalChart.update();
    }
    // 更新圖表
    function updateConfigAsNewObject(index, data) {
        // 更新數據
        let percent = document.getElementById(`percent${data.cust_no}`).children;
        let num = document.getElementById(`num${data.cust_no}`).children;
        let newChartData = [data.statuspickedfinish || 0, data.statusloading || 0, data.statuspicked || 0, data.statusoffloading || 0, data.statusfinished || 0];
        percent[0].textContent = `${data.statuspickedfinish ? Math.round(data.statuspickedfinish / data.total_count * 1000)/10 : 0}%`;
        percent[1].textContent = `${data.statusloading ? Math.round(data.statusloading / data.total_count * 1000)/10 : 0}%`;
        percent[2].textContent = `${data.statuspicked ? Math.round(data.statuspicked / data.total_count * 1000)/10 : 0}%`;
        percent[3].textContent = `${data.statusoffloading ? Math.round(data.statusoffloading / data.total_count * 1000)/10 : 0}%`;
        percent[4].textContent = `${data.statusfinished ? Math.round(data.statusfinished / data.total_count * 1000)/10 : 0}%`;
        num[0].textContent = data.statuspickedfinish;
        num[1].textContent = data.statusloading;
        num[2].textContent = data.statuspicked;
        num[3].textContent = data.statusoffloading;
        num[4].textContent = data.statusfinished;
        document.getElementById(`chartTitle${data.cust_no}`).textContent = data.total_count;
        // 更新圖表
        chartElList[index].chart.data.datasets[0].data = newChartData;
        chartElList[index].chart.update();
    }
    // 換頁
    let chartWhInPageNums = 1; // 倉庫頁碼
    let pageSize = 4;  // 一頁幾筆
    var refreshtime    = parseInt("{{$viewData['refreshtime']}}");
    let chartWhChengePageTimeout = refreshtime;
    let chartWhCarousel;
    let showCount = 0;
    function chengePageEvent(type, data, pageNums, pageSize) {
        // 
        showCount = 0
        data.forEach((item, index) => {
            if (initWh_.indexOf('WH' + item.id) != -1) {
                if (showCount > (pageSize * (pageNums - 1) - 1) && showCount < (pageNums * pageSize)) {
                    document.getElementById(`${type}Box${item.id}`).style.display = 'block';
                    showCount++;
                } else {
                    document.getElementById(`${type}Box${item.id}`).style.display = 'none';
                    showCount++;
                }
            } else {
                document.getElementById(`${type}Box${item.id}`).style.display = 'none';
            }
        });
        // 頁數 focuse
        if ( initWh_.length > pageSize) {
            let totalCont = Math.floor((initWh_.length / pageSize)) + ((initWh_.length % pageSize > 0) ? 1 : 0);
            for (let i = 0; i < totalCont; i++) {
                document.getElementById(`${type}-page-${i + 1}`).classList.remove('active');
            };
            try {
                document.getElementById(`${type}-page-${pageNums}`).classList.add('active');
            } catch (error) {
                console.log(error);
            }
        }
    }
    // 點擊頁碼換頁
    function setPageBtnEventListener(type, data, pageSize) {
        let totalCont = Math.floor((initWh_.length / pageSize)) + ((initWh_.length % pageSize > 0) ? 1 : 0);
        document.getElementById(`${type}-page-list`).innerHTML = '';
        if (totalCont > 1) {
            for (let i = 0; i < totalCont; i++) {
                let newli = document.createElement('li');
                newli.innerHTML = `<button type="button" class="active" id="${type}-page-${i + 1}">${i + 1}</button>`
                document.getElementById(`${type}-page-list`).appendChild(newli);
                document.getElementById(`${type}-page-${i + 1}`).addEventListener('click', function() {
                    chengePageEvent(type, data, i + 1, pageSize);
                    endChartWhCarouselEvent();
                    starChartWhCarouselEvent();
                })
            }
        }
    }
    // 輪播換頁
    function carouselEvent (type, data, pageSize) {
        let totalCont = Math.ceil(initWh_.length / pageSize);
        switch (type) {
            case 'chartWh':
                chartWhInPageNums += 1;
                if (chartWhInPageNums > totalCont) {
                chartWhInPageNums = 1;
                };
                break;
        }
        chengePageEvent(type, chartElList, chartWhInPageNums, pageSize);
    }
    function starChartWhCarouselEvent () {
        chartWhCarousel = setInterval(() => {
        carouselEvent('chartWh', chartElList, pageSize);
        }, chartWhChengePageTimeout);
    }
    function endChartWhCarouselEvent () {
        clearInterval(chartWhCarousel);
    }
    //
    setPageBtnEventListener('chartWh', chartElList, pageSize);
    starChartWhCarouselEvent();
    chengePageEvent('chartWh', chartElList, chartWhInPageNums, pageSize);
    var count = 0;
    dashboardWebsocket();
    function dashboardWebsocket () {
        let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/groupId_Dsvupdatedatsbboard';
        let ws = new WebSocket(wsURL); // 建立連線
        ws.onopen = function(e) {
           
            // websocketonopen(e);
            // console.log('ws 連線成功~~');
        };
        ws.error = function(error) {
            // websocketonerror(error);
            // console.error('ws 連線失敗', error);
        };
        ws.onmessage = function(e) {
            if(count >0 ) {
                dateChanged();
            }
            console.log('dashboardWebsocket');
            count ++;
        };
        ws.onclose = function(e) {

        };
    }
    function websocketsend (data) {
        // 前端丟資料
        console.log('send datanick', data);
    }
    function websocketclose () {
        console.log('ws 關閉連線nick');
    }
</script>
<script src="https://unpkg.com/vue"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


@endsection