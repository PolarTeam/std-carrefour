@extends('layout.layout')
@section('header')
<section class="content-header">
    <h1>
        Dashboard - 數據2
        <small></small>
    </h1>
</section>

@endsection
@section('before_scripts')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="配送服務系統">
<!-- Meta Keyword -->
<meta name="keywords" content="DSS">
<meta name="author" content="">
<!-- CSS Global Compulsory -->
<!-- CSS Implementing Plugins -->

@endsection

@section('content')

<div class="py-2 px-3">
    <div class="row">
      <div class="col-lg-6">
      <div>
          <div class="d-flex justify-content-between">
            <div class="sub-title">逾期配送明細</div>
              <ul class="list-unstyled page-list" id="late-page-list">
                <li><button type="button" class="active" id="late-page-1">1</button></li>
                {{-- <li><button type="button" id="late-page-2">2</button></li> --}}
              </ul>
          </div>
          <ul class="list-unstyled details-list" id="late-content">
          </ul>
        </div>
      </div>
      <div class="col-lg-6">
        <div>
          <div class="d-flex justify-content-between">
            <div class="sub-title">異常明細</div>
            <ul class="list-unstyled page-list" id="error-page-list">
                <li><button type="button" class="active" id="error-page-1">1</button></li>
                {{-- <li><button type="button" id="error-page-2">2</button></li> --}}
            </ul>
          </div>
          <ul class="list-unstyled details-list" id="error-content">

          </ul>
        </div>
      </div>
    </div>
</div>

<style>
  .d-flex {
    display: flex;
  }
  .justify-content-between {
    justify-content: space-between;
  }
  .justify-content-end {
    justify-content: flex-end;
  }
  .align-items-end {
    align-items: flex-end;
  }
  .align-items-center {
    align-items: center;
  }
  .w-100 {
    width: 100%;
  }
  .blue-sm-btn {
    font-size: 14px;
    color: #fff;
    background: #19294A;
    padding: 4px 10px;
    border-radius: 8px;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    margin-bottom: 8px;
  }
  .sub-title {
    font-size: 20px;
    color: #444444;
    margin-bottom: 10px;
    white-space: nowrap;  /* 使文字不換行 */
  }
  .list-detail {
    background-color: #fff;
    border-radius: 0 8px 8px 0;
    padding: 16px;
    margin-bottom: 8px;
    font-size: 16px;
    flex: 1;
  }
  .list-detail .list-detail-title {
    font-size: 20px;
    color: #000;
    margin-bottom: 8px;
  }
  .error-content {
    font-size: 16px;
    color: #535353;
  }
  .error-content-date {
    font-size: 16px;
    color: #999DA8;
  }
  .list-status {
    width: 16px;
    height: auto;
    margin-bottom: 8px;
    border-radius: 8px 0 0 8px;
  }
  .list-status.un-do {
    background: #E73000;
  }
  .list-status.doing {
    background: #ffcd56;
  }
  .list-status.finish {
    background: #3bb300;
  }
  .mb-3 {
    margin-bottom: 16px;
  }
  .mr-2 {
    margin-right: 8px;
  }
  .page-list {
    display: flex;
    overflow-x: auto;
  }
  .page-list button {
    background: transparent;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    font-size: 16px;
    color: #999999;
    margin-right: 16px;
  }
  .page-list .active {
    color: #f47920;
    font-weight: bold;
  }

</style>

@endsection

@section('after_scripts')
<script>
  // 換頁輪播
  const lateData = JSON.parse(('{{$viewData["expiedata"]}}').split('&quot;').join('"'));
  console.log(lateData);
  const errorData = JSON.parse(('{{$viewData["errordata"]}}').split('&quot;').join('"'));
  console.log(errorData);
  let lateInPageNums = 1; // 逾期配送明細頁碼
  let errorInPageNums = 1; // 異常明細頁碼
  let pageSize = 10;  // 一頁幾筆

  function dateChanged() {
      var input = document.getElementById("searchDataByDay");
      console.log("Selected date: " + input.value);
      $.ajax({
          type: "post", //  OR POST WHATEVER...
          dataType : 'json', // 預期從server接收的資料型態
          contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
          url: BASE_URL + '/zh-TW/dashboardText2refresh',
          data:JSON.stringify({
              "date" : input.value,
              "wharray" : whData,
              "owenerarray" : ownerData,
          }),
          success: function(data) {
              console.log(data);
          }
      });
  }
  // 換頁
  function chengePageEvent(type, data, pageNums, pageSize) {
      let el = document.getElementById(`${type}-content`);
      let contentList = data.filter((item, index)=> {
        return (index > (pageSize * (pageNums - 1) - 1) && index < (pageNums * pageSize));
      });
      let contentHtml = '';
      switch (type) {
        case 'late':
          contentList.forEach((item, index) => {
            contentHtml += `
              <li class="d-flex w-100">
                <div class="list-status ${item.status === 'PICKEDFINISH' ? 'un-do' : ''} ${item.status === 'LOADING' || item.status === 'UPLOADFINISH' || item.status === 'OFFSTAFF' ? 'doing' : ''} ${item.status === 'FINISHED' ? 'finish' : ''} ${item.status !== 'PICKEDFINISH' && item.status !== 'LOADING' && item.status !== 'PICKED' && item.status !== 'OFFLOADING' && item.status !== 'FINISHED' ? 'un-do' : ''}"></div>
                <div class="list-detail">
                  <div class="d-flex justify-content-between">
                    <div class="list-detail-title">${item.cust_ord_no}</div>
                    <div class="error-content">${item.dlv_date.substr(0,10)}</br><span style="float:right">${item.from_wh}</span></div>
                  </div>
                  <div class="d-flex justify-content-between">
                    <div>${item.owner_nm} | ${item.dlv_cust_nm}</div>
                    <div class="error-content">${item.driver} ${item.driver_phone}</div>
                  </div>
                  <div class="d-flex justify-content-between">
                    <div class="error-content">${item.status_desc}</div>
                    <div class="error-content-date">${item.show_time}</div>
                  </div>
                </div>
              </li>`;
          });
          break;
          case 'error':
          contentList.forEach((item, index) => {
            contentHtml += `
            <li class="d-flex w-100">
              <div class="list-status ${item.status === 'UNTREATED' ? 'un-do' : ''} ${item.status === 'PROCESS' ? 'doing' : ''} ${item.status !== 'PROCESS' && item.status !== 'UNTREATED' ? 'finish' : ''}"></div>
              <div class="list-detail">
                <div class="d-flex justify-content-between">
                  <a href="${BASE_URL+'/zh-TW/errorprocess/'+item.id+'/edit'}" target="_blank" class="list-detail-title" style="color:blue">${item.ord_no}</a>
                  <div class="error-content">${item.from_wh}</div>
                </div>
                <div class="d-flex justify-content-between">
                  <div>${item.owner_name} | ${item.cust_name}</div>
                  <div class="error-content">${item.created_by} ${item.phone}</div>
                </div>
                <div class="d-flex justify-content-between">
                  <div class="error-content">異常內容 : ${item.error_name}</div>
                  <div class="error-content-date">${item.created_at}</div>
                </div>
              </div>
            </li>`;
          });
          break;
      };
      // console.log(contentHtml);
      el.innerHTML = contentHtml;
      // 頁數 focuse
      let totalCont = Math.ceil(data.length / pageSize);
      switch (type) {
        case 'late':
          lateInPageNums = pageNums;
          break;
        case 'error':
          errorInPageNums = pageNums;
          break;
      }
      for (let i = 0; i < totalCont; i++) {
        document.getElementById(`${type}-page-${i + 1}`).classList.remove('active');
      };
      try {
        document.getElementById(`${type}-page-${pageNums}`).classList.add('active');
      } catch (error) {
        
      }
  };
  // 點擊頁碼換頁
  function setPageBtnEventListener(type, data, pageSize) {
    let totalCont = Math.ceil(data.length / pageSize);
    document.getElementById(`${type}-page-list`).innerHTML = '';
    // console.log(totalCont);
    for (let i = 0; i < totalCont; i++) {
      let newli = document.createElement('li');
      newli.innerHTML = `<button type="button" class="active" id="${type}-page-${i + 1}">${i + 1}</button>`
      document.getElementById(`${type}-page-list`).appendChild(newli);
      console.log(`${type}-page-${i + 1}`);
      document.getElementById(`${type}-page-${i + 1}`).addEventListener('click', function() {
        chengePageEvent(type, data, i + 1, pageSize);
        switch (type) {
          case 'late':
            endLateCarouselEvent();
            starLateCarouselEvent();
            break;
          case 'error':
            endErrorCarouselEvent();
            starErrorCarouselEvent();
            break;
        }
      });
    };
  }
  // 輪播換頁
  function carouselEvent (type, data) {
    let totalCont = Math.ceil(data.length / pageSize);
    switch (type) {
      case 'late':
        lateInPageNums += 1;
        if (lateInPageNums > totalCont) {
          lateInPageNums = 1;
        };
        chengePageEvent(type, lateData, lateInPageNums, pageSize);
        break;
      case 'error':
        errorInPageNums += 1;
        if (errorInPageNums > totalCont) {
          errorInPageNums = 1;
        };
        chengePageEvent(type, errorData, errorInPageNums, pageSize);
        break;
    }
  }
  var refreshtime    = parseInt("{{$viewData['refreshtime']}}");
  // 逾期配送明細輪播換頁
  let lateChengePageTimeout = refreshtime;
  let lateCarousel;
  function starLateCarouselEvent () {
    lateCarousel = setInterval(() => {
      carouselEvent('late', lateData);
    }, lateChengePageTimeout);
  }
  function endLateCarouselEvent () {
    clearInterval(lateCarousel);
  }
  // 異常明細輪播換頁
  let errorChengePageTimeout = refreshtime;
  let errorCarousel;
  function starErrorCarouselEvent () {
    errorCarousel = setInterval(() => {
      carouselEvent('error', errorData);
    }, errorChengePageTimeout);
  }
  function endErrorCarouselEvent () {
    clearInterval(errorCarousel);
  }
  //
  setPageBtnEventListener('late', lateData, pageSize);
  chengePageEvent('late', lateData, lateInPageNums, pageSize);
  starLateCarouselEvent();
  setPageBtnEventListener('error', errorData, pageSize);
  chengePageEvent('error', errorData, errorInPageNums, pageSize);
  starErrorCarouselEvent();

  var count = 0;
  dashboardWebsocket();
  function dashboardWebsocket () {
      let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/groupId_Dsvupdatedatsbboard';
      let ws = new WebSocket(wsURL); // 建立連線
      ws.onopen = function(e) {
          
          // websocketonopen(e);
          // console.log('ws 連線成功~~');
      };
      ws.error = function(error) {
          // websocketonerror(error);
          // console.error('ws 連線失敗', error);
      };
      ws.onmessage = function(e) {
          if(count >0 ) {
            // location.reload();
          }
          console.log('dashboardWebsocket');
          count ++;
      };
      ws.onclose = function(e) {

      };
  }
  function websocketsend (data) {
      // 前端丟資料
      console.log('send datanick', data);
  }
  function websocketclose () {
      console.log('ws 關閉連線nick');
  }
</script>
@endsection