@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_goods.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/goodsProfile') }}">{{ trans('mod_goods.titleName') }}</a></li>
		<li class="active">{{ trans('mod_goods.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="owner_cd">{{ trans('modOrder.ownerCd') }}</label>

                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="owner_cd" name="owner_cd" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="owner_cd"
                                                    info1="{{Crypt::encrypt('sys_customers')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail")}}" 
                                                    info3="{{Crypt::encrypt("status='B' AND type='OTHER'")}}"
                                                    info5="{{Crypt::encrypt('goods')}}"
                                                    info4="cust_no=owner_cd;cname=owner_nm;receive_mail=owner_send_mail;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="owner_nm">{{ trans('modOrder.ownerNm') }}</label>
                                        <input type="text" class="form-control" id="owner_nm" name="owner_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="price">費用</label>
                                        <input type="number" class="form-control" id="price" name="price">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="pkg_unit">數量單位</label>
                                        <input type="text" class="form-control" id="pkg_unit" name="pkg_unit">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="goods_no">{{ trans('modGoods.goodsNo') }}</label>
                                        <input type="text" class="form-control" id="goods_no" name="goods_no" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_nm">{{ trans('modGoods.goodsNm') }}</label>
                                        <input type="text" class="form-control" id="goods_nm" name="goods_nm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_no2">{{ trans('modGoods.goodsNo2') }}</label>
                                        <input type="text" class="form-control" id="goods_no2" name="goods_no2">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="goods_nm2">{{ trans('modGoods.goodsNm2') }}</label>
                                        <input type="text" class="form-control" id="goods_nm2" name="goods_nm2">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="gw">{{ trans('modGoods.gw') }}</label>
                                        <input type="text" class="form-control" id="gw" name="gw" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="gwu">{{ trans('modGoods.gwu') }}</label>
                                        <input type="text" class="form-control" id="gwu" name="gwu">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbm">{{ trans('modGoods.cbm') }}</label>
                                        <input type="text" class="form-control" id="cbm" name="cbm">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbmu">{{ trans('modGoods.cbmu') }}</label>
                                        <input type="text" class="form-control" id="cbmu" name="cbmu">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="g_length">{{ trans('modGoods.gLength') }}</label>
                                        <input type="text" class="form-control" id="g_length" name="g_length" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="g_width">{{ trans('modGoods.gWidth') }}</label>
                                        <input type="text" class="form-control" id="g_width" name="g_width">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="g_height">{{ trans('modGoods.gHeight') }}</label>
                                        <input type="text" class="form-control" id="g_height" name="g_height">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="sn_flag">是否刷序號</label>
                                        <select class="form-control" id="sn_flag" name="sn_flag" >
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <input type="hidden" id="created_by" name="created_by" class="form-control">
                                <input type="hidden" id="updated_by" name="updated_by" class="form-control">
                                <input type="hidden" id="created_at" name="created_at" class="form-control">
                                <input type="hidden" id="updated_at" name="updated_at" class="form-control">
                                <input type="hidden" id="g_key" name="g_key" class="form-control">
                                <input type="hidden" id="c_key" name="c_key" class="form-control">
                                <input type="hidden" id="s_key" name="s_key" class="form-control">
                                <input type="hidden" id="d_key" name="d_key" class="form-control">

                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="owner_cd"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('owner_cd', "製造商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="owner_cd"]').on('click', function(){
        var check = $('#subBox input[name="owner_cd"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","owner_cd",callBackFunc=function(data){
                console.log(data);
                
            },"owner_cd");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/goodsProfile" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#goodsProfile').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            var requiredColumn = [
                {
                    "column_filed":"owner_cd",
                    "column_type":"string",
                },
                {
                    "column_filed":"owner_nm",
                    "column_type":"string",
                },
                {
                    "column_filed":"goods_no",
                    "column_type":"string",
                },
                {
                    "column_filed":"goods_nm",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection