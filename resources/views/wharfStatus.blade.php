@extends('layout.layout')
@section('header')
<section class="content-header">
    <h1>
        碼頭使用狀態
        <small></small>
    </h1>
</section>

@endsection
@section('before_scripts')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="配送服務系統">
<!-- Meta Keyword -->
<meta name="keywords" content="DSS">
<meta name="author" content="">
<!-- CSS Global Compulsory -->
<!-- CSS Implementing Plugins -->


@endsection

@section('content')

<div id="wharfStatusContentBox">

</div>


<style>
  .d-flex {
    display: flex;
  }
  .wharf-status-box {
    margin-bottom: 40px;
  }
  .wharf-status-box .status-header{
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 10px;
  }
  .wharf-status-box .wh-title{
    font-size: 20px;
    width: 100px;
    margin-right: 30px;
  }
  .page-list {
    display: flex;
    align-items: center;
  }
  .page-list button {
    background: transparent;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    font-size: 16px;
    color: #999999;
    margin-right: 16px;
  }
  .page-list .active {
    color: #f47920;
    font-weight: bold;
  }
  .wharf-status-box .time-item{
    font-size: 16px;
    width: 70px;
    text-align: center;
    margin-right: 30px;
  }
  .wharf-status-box .status-content {
    display: flex;
    padding: 16px 0;
    background: #fff;
    border-radius: 8px;
    min-height: 107px;
    margin-bottom: 8px;
  }
  .wharf-status-box .wharf-title {
    font-size: 20px;
    width: 100px;
    margin-right: 30px;
    border-right: 1px solid #D2D6DE;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .wharf-status-box .status-item {
    color: #888;
    width: 90px;
    margin-right: 10px;
    text-align: center;
  }
  .wharf-status-box .status-item i {
    display: block;
    font-size: 40px;
    text-align: center;
    margin-bottom: 8px;
  }
  .wharf-status-box .status-item span {
    display: block;
    font-size: 18px;
    text-align: center;
  }
  .wharf-status-box .status-item.using {
    color: #F47920;
  }
  .wharf-status-box .status-item.can-reserve i {
    display: block;
    width: 30px;
    height: 8px;
    border-radius: 10px;
    background: #D1D5D9;
    margin: 33px auto;
  }
  .wharf-status-box .status-item .time {
    display: inline-block;
    font-size: 16px;
    text-align: center;
    color: #000;
  }
  .wharf-status-box .status-item.can-reserve .time {
    color: #888;
  }
  /* .wharf-status-box button {
    width: 70px;
    margin-right: 30px;
    background: transparent;
    border: 0 solid transparent;
    outline: 0 solid transparent;
    color: #888;
    font-size: 30px;
  }
  .wharf-status-box button:hover, .wharf-status-box button:focus, .wharf-status-box button:active {
    border: 0 solid transparent;
    outline: 0 solid transparent;
    color: #999DA8;
  } */
  .swal2-content .title {
      font-size: 20px;
      color: #333333;
      font-weight: 400;
  }
  .swal2-content .sub-title {
      font-size: 16px;
      color: #999999;
      font-weight: 400;
  }
  .swal2-content .sub-title .num {
      color: #3079ae;
  }
  .swal2-content label {
      font-size: 14px;
      color: #666666;
      font-weight: 400;
  }
  .swal2-content input {
      font-size: 14px;
      border: 1px solid #efeff1;
      padding: 12px;
      color: #333333;
      border-radius: 4px;
      width: 100%;
  }
  .swal2-content .autocomplet {
      width: 100%;
  }
  .swal2-content .autocomplet .search-btn {
      border: 1px solid #efeff1;
      color: #333333;
      border-radius: 4px;
      width: 44px;
      height: 44px;
      background: #fff;
      font-size: 14px;
      display: inline-block;
  }
  .swal2-content .autocomplet input {
      width: calc(100% - 49px);
      display: inline-block;
  }
  .swal2-content .select {
      position: relative;
  }
  .swal2-content .select select {
      font-size: 14px;
      border: 1px solid #efeff1;
      padding: 12px;
      color: transparent;
      border-radius: 4px;
      width: 100%;
      background: transparent;
      position: relative;
      z-index: 3;
  }
  .swal2-content .select::before {
      content: "\f107";
      font-family: "FontAwesome";
      font-weight:400;
      display: block;
      position: absolute;
      top: 50%;
      right: 20px;
      color: #c5c8ce;
      font-size: 20px;
      transform: translateY(-50%);
      z-index: 2;
  }
  .swal2-content .select p {
      position: absolute;
      top: 50%;
      left: 0;
      transform: translateY(-50%);
      color: #333333;
      font-size: 14px;
      z-index: 1;
      border: 1px solid #efeff1;
      padding: 12px;
      border-radius: 4px;
      background: #fff;
      height: 100%;
      width: 100%;
  }
  .swal2-content .border-bottom {
    padding-bottom: 16px;
    margin-bottom: 16px;
    border-bottom: 1px solid #D2D6DE;
  }
  .modal-status {
    margin-bottom: 10px;
    border-bottom: 1px solid #D2D6DE;
  }
  .modal-status ul {
    margin-bottom: 10px;
  }
  .modal-status li {
    color: #999DA8;
    font-size: 16px;
    min-width: 64px;
    margin-right: 8px;
  }
  .modal-status .modal-status-title li, .modal-status .using {
    color: #000000;
  }
  .mb-3 {
    margin-bottom: 16px;
  }
  .mr-2 {
    margin-right: 8px;
  }
</style>


@endsection

@section('after_scripts')
<script>
  const allWhData = JSON.parse(('{{$viewData["allwh"]}}').split('&quot;').join('"'));
  const allWhDataLength = allWhData.length;
  // 頁碼
  function setPagesBoxEvent (parentNo, pages) {
    let pagesStr =`<div class="page-list" id="page_box_${parentNo}">`;
    for (let i = 0; i < pages; i++) {
      pagesStr += `<button type="button" class="${i === 0 ? 'active' : ''}" id="page_${parentNo}_${i}">${i + 1}</button>`;
    }
    pagesStr += '</div>';
    return pagesStr
  };
  // 頁碼監聽事件
  function addPagesaListenerEvent (data, showLength, pages) {
    for (let i = 0; i < pages; i++) {
        console.log(`page_${data.cust_no}_${i}`);
      document.getElementById(`page_${data.cust_no}_${i}`).addEventListener('click', function() {
        console.log('按了', `page_${data.cust_no}_${i}`);
        let statusItem = setStatusItemEvent(data, showLength, i);
        console.log(`status_content_${data.cust_no}`);
        document.getElementById(`status_content_${data.cust_no}`).innerHTML = statusItem;
        for (let index = 0; index < pages; index++) {
          document.getElementById(`page_${data.cust_no}_${index}`).classList.remove('active');
        }
        document.getElementById(`page_${data.cust_no}_${i}`).classList.add('active');
      })
    }
  }
  // 分頁內容
  function setStatusItemEvent (data, showLength, pageNum) {
    let statusItemStr ='';
    for (let i = (showLength * pageNum); i < (showLength * (pageNum + 1)); i++) {
      if (data.markdata[i]) {
        if (data.markdata[i].schedule.length == 0 && data.markdata[i].schedule[0].car_no != '-') {
          statusItemStr += `<div class="status-item using">
              <span>${data.markdata[i].cust_name}</span>
              <i class="fa fa-truck" aria-hidden="true"></i>
              <span>${data.markdata[i].schedule[0].car_no}</span>
              <span class="time">${data.markdata[i].schedule[0].time}${(data.markdata[i].schedule[0].end_at === '處理時間') ? '' : ('-' + data.markdata[i].schedule[0].end_at)}</span>
            </div>`
        } else if (data.markdata[i].schedule.length > 0 && data.markdata[i].schedule[0].car_no != '-') {
          statusItemStr += `<div class="status-item using">
              <span>${data.markdata[i].cust_name}</span>
              <i class="fa fa-truck" aria-hidden="true"></i>
              <span>${data.markdata[i].schedule[0].car_no}</span>
              <span class="time">${data.markdata[i].schedule[0].time}${(data.markdata[i].schedule[0].end_at === '結束時間') ? '' : (data.markdata[i].schedule[0].end_at == ''? '': '-' + data.markdata[i].schedule[0].end_at)}</span>
            </div>`
        } else {
          statusItemStr += `<div class="status-item can-reserve">
              <span>${data.markdata[i].cust_name}</span>
              <i></i>
              <span class="time">可使用</span>
            </div>`
        }
      }
    }
    return statusItemStr
  };
  // 置入內容
  function setWharfStatusBoxEvent () {
    let wharfStatusContentBox = document.getElementById('wharfStatusContentBox');
    let contentBoxWidth = wharfStatusContentBox.offsetWidth;
    let contentBoxShowLength = Math.floor(contentBoxWidth / 100);
    // console.log(contentBoxShowLength);
    //
    let str = '';
    //
    console.log(allWhData);
    for (let i = 0; i < allWhDataLength; i++) {
      let cname = allWhData[i].cname;
      let markDataLength = allWhData[i].markdata.length;
      let pages = Math.floor(markDataLength / contentBoxShowLength) + ((markDataLength % contentBoxShowLength) ? 1 : 0 );
      let markDataLastPages = (markDataLength % contentBoxShowLength) || 0;
      // console.log(cname);
      console.log(pages);
      if (pages) {
        let pagesBox = setPagesBoxEvent(allWhData[i].cust_no, pages);
        let statusItem = setStatusItemEvent(allWhData[i], contentBoxShowLength, 0);
        // console.log(pagesBox);
        // console.log(statusItem);
        str += `<div class="wharf-status-box" style="width:${contentBoxShowLength * 100}px">
                <div class="status-header">
                  <div class="wh-title">${allWhData[i].cname}</div>
                  ${pagesBox}
                </div>
                <div class="status-content" id="status_content_${allWhData[i].cust_no}">
                  ${statusItem}
                </div>
              </div>`
      } else {
        str += `<div class="wharf-status-box">
                <div class="status-header">
                  <div class="wh-title">${allWhData[i].cname}</div>
                </div>
              </div>`
      }
      // console.log(Math.floor(markDataLength / contentBoxShowLength));
      // console.log(markDataLength % contentBoxShowLength);
    }
    // 置入內容
    wharfStatusContentBox.innerHTML = str;
    // 頁碼監聽事件
    for (let i = 0; i < allWhDataLength; i++) {
      let markDataLength = allWhData[i].markdata.length;
      let pages = Math.floor(markDataLength / contentBoxShowLength) + ((markDataLength % contentBoxShowLength) ? 1 : 0 );
      addPagesaListenerEvent(allWhData[i], contentBoxShowLength, pages);
    }
  }
  setWharfStatusBoxEvent();
</script>
@endsection