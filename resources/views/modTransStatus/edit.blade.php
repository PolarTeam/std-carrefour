@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('modTransStatus.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/modTransStatus') }}">{{ trans('modTransStatus.titleName') }}</a></li>
		<li class="active">{{ trans('modTransStatus.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="ts_type">{{ trans('modTransStatus.tsType') }}</label>
                                        <input type="text" class="form-control" id="ts_type" name="ts_type" placeholder="Enter Category" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="order">{{ trans('modTransStatus.order') }}</label>
                                        <input type="text" class="form-control" id="order" name="order" placeholder="Enter Order.">
                                    </div>                                   
                                </div>                                
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="ts_name">{{ trans('modTransStatus.tsName') }}</label>
                                        <input type="text" class="form-control" id="ts_name" name="ts_name" placeholder="Enter Name" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ts_desc">{{ trans('modTransStatus.tsDesc') }}</label>
                                        <input type="text" class="form-control" id="ts_desc" name="ts_desc" placeholder="Enter Desc.">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="trigger_code">{{ trans('modTransStatus.triggerCode') }}</label>
                                        <input type="text" class="form-control" id="trigger_code" name="trigger_code" placeholder="Enter Code.">
                                    </div>                                    
                                </div>                                                    
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('modTransStatus.createdBy') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('modTransStatus.createdAt') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('modTransStatus.updatedBy') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('modTransStatus.updatedAt') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="owner_cd"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('owner_cd', "製造商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="owner_cd"]').on('click', function(){
        var check = $('#subBox input[name="owner_cd"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","owner_cd",callBackFunc=function(data){
                console.log(data);
                
            },"owner_cd");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modTransStatus" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#modTransStatus').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            if($('#display_name').val() == ""){
                document.getElementById("display_name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if($('#name').val() == ""){
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            var requiredColumn = [

            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection