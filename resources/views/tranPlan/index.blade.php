@extends('layout.layout')
@section('header')
<section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
    <h1>
        派車作業總覽
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">派車作業總覽</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />
    <input type="hidden" value="{{$lang}}" id="lang" />
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
<div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('before_scripts')
<script>
    var lang = "{{$lang}}";
    var languagename = "";
    var tableName = 'mod_dlv';
    var fileName = 'sendCar';
    var enabledStatus = 'Y';
    var btnGroupList = [];
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "搜尋",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "Excel匯出",
            btnFunc: function () {
            var url  =BASE_URL;
            url = url.replace("admin","");
            var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
            filtercolumndata = [];
            var filtercolumn = [];

            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
            }
            if(ids.length == 0) {
                for (var i = 0; i < filterGroups.length; i++) {
                    var filterGroup = filterGroups[i];
                    var filters = filterGroup.filter.getfilters();
                        for (var k = 0; k < filters.length; k++) {
                            if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                // new Date(unix_timestamp * 1000)
                                filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                            } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                            } else if(filters[k].condition == "contains" ) {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            } else {
                                filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                            }
                            // filtercolumndata.push(filtercolumn);
                    }
                }
            } else {
                filtercolumn[m] = ['id', `in`, ids];
            }
            var sortby = [];
            var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
            if(sortinformation.sortcolumn != null ) {
                var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                var obj = {};
                obj["sortField"] = sortinformation.sortcolumn;
                obj["sortValue"] = sortvalue;
                sortby.push(obj);
                // sortby.push([ sortinformation.sortcolumn,  tpye]);
            }
            $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                'table'        : tableName,
                'fileName'     : fileName,
                'baseCondition': filtercolumn,
                'sort': sortby,
                'header'       : enabledheader,
            }, function(data){
                if(data.message == "success") {
                    window.open(data.donwloadLink);
                }

            });
        }
        },
        @can('TransportationPlan_Grid')
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "欄位調整",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        @endcan
        {
            btnId:"btnSendApp",
            btnIcon:"fa fa-mobile",
            btnText:"{{ trans('modDlv.sendApp') }}",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindexes');
                if(idx.length ==  0) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }

                var ids = [];
                for(i in idx) {
                    var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx[i]);
                    ids.push(rowData.id);
                }
                $.post(BASE_URL + "/TranPlanMgmt/sendToApp", {ids: ids}, function(data){
                    if(data.msg == "success") {
                        swal("{{ trans('modDlv.msg1') }}", "{{ trans('modDlv.msg2') }}", "success");
                        $('#jqxGrid').jqxGrid('updatebounddata');
                    }
                    else {
                        swal(data.error_log, "", "error");
                    }
                });
            }
        },
        @can('TransportationPlan_RoutingPlan')
        {
            btnId:"btnAutoRoute",
            btnIcon:"fa fa-road",
            btnText:"{{ trans('modDlv.routingPlan') }}",
            btnFunc:function(){
                swal("{{ trans('modDlv.msg3') }}", "", "success");
            }
        },
        @endcan
        {
            btnId:"btnCancelCar",
            btnIcon:"fa fa-ban",
            btnText:"{{ trans('modDlv.cancelCar') }}",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
                var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
                var dlvTypeCode = [
                    {value: "P", label: "提貨"},
                    {value: "D", label: "配送"},
                ]

                var dlvSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: dlvTypeCode
                };

                var dlvAdapter = new $.jqx.dataAdapter(dlvSource, {
                    autoBind: true
                });

                var statusCode = [
                    {value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
                    {value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
                    {value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
                    {value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
                    {value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
                ];

                var statusSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: statusCode
                };

                var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                    autoBind: true
                });

                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'id', type: 'number' },
                        { name: 'dlv_type', type: 'string', values: { source: dlvAdapter.records, value: 'value', name: 'label' } },
                        { name: 'dlv_no', type: 'string' },
                        { name: 'ord_id', type: 'number' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'status', type: 'string', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'cust_nm', type: 'string' },
                        //{ name: 'pick_cust_nm', type: 'string' },
                        //{ name: 'dlv_cust_nm', type: 'string' },
                        { name: 'addr', type: 'string' }               
                    ],
                    root:"Rows",
                    pagenum: 1,					
                    beforeprocessing: function (data) {
                    },
                    pagesize: 200,
                    cache: false,
                    url: BASE_URL + '/getDlvPlan/' + rowData.dlv_no,
                    sortcolumn: 'ord_no',
                    sortdirection: 'asc'
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#dlvPlanGrid').jqxGrid('destroy');
                $("#dlvPlanBody").html('<div id="dlvPlanGrid"></div>');
                $("#dlvPlanGrid").jqxGrid(
                {
                    width: '100%',
                    //height: '100%',
                    autoheight: true,
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    selectionmode: 'checkbox',
                    showfilterrow: true,
                    columnsresize: true,
                    ready: function () {
                        $('#dlvPlanGrid').jqxGrid('autoresizecolumns');
                    },
                    columns: [
                        { text: 'id', datafield: 'id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.dlvType")}}', datafield: 'dlv_type', width: 100, hidden: false },
                        { text: 'dlvNo', datafield: 'dlv_no', width: 50, hidden: true },
                        { text: 'ord_id', datafield: 'ord_id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110},
                        { text: '{{trans("modOrder.etd")}}', datafield: 'etd', width: 120, cellsformat: 'yyyy-MM-dd',filtertype: 'date' },
                        { text: '{{trans("modDlvPlanView.ownerNm")}}', datafield: 'owner_nm', width: 110},
                        { text: '{{trans("modOrder.ordNo")}}', datafield: 'ord_no', width: 120 },
                        { text: '{{trans("modOrder.sysOrdNo")}}', datafield: 'sys_ord_no', width: 120 },
                        { text: '{{trans("modDlvPlanView.custNm")}}', datafield: 'cust_nm', width: 120 },
                        //{ text: '{{trans("modOrder.pickCustNm")}}', datafield: 'pick_cust_nm', width: 100 },
                        //{ text: '{{trans("modOrder.dlvCustNm")}}', datafield: 'dlv_cust_nm', width: 101 },
                        { text: '{{trans("modOrder.dlvAddr")}}', datafield: 'addr', width: 300 }
                    ]
                });
                $("#dlvPlanModal").modal('show');
            }
        },
        {
            btnId:"btnExportDetail",
            btnIcon:"fa fa-arrow-circle-down",
            btnText:"匯出明細",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindexes');
                if(idx.length ==  0) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }

                var ids = '';
                for(i in idx) {
                    var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx[i]);
                    ids += rowData.dlv_no + ';';
                }
                ids = ids.substring(0, ids.length - 1);
                location.href = BASE_URL + '/export/detail?dlvNo=' + ids;
            }
        },
        @if(Auth::user()->hasRole('Admin'))
        {
            btnId:"btnCalEta",
            btnIcon:"fa fa-clock-o",
            btnText:"預計到貨",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
                if(idx == -1) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }
                var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);
                var dlvTypeCode = [
                    {value: "P", label: "提貨"},
                    {value: "D", label: "配送"},
                ]

                var dlvSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: dlvTypeCode
                };

                var dlvAdapter = new $.jqx.dataAdapter(dlvSource, {
                    autoBind: true
                });

                var statusCode = [
                    {value: "UNTREATED", label: "{{ trans('modDlvPlan.STATUS_UNTREATED') }}"},
                    {value: "SEND", label: "{{ trans('modDlvPlan.STATUS_SEND') }}"},
                    {value: "DLV", label: "{{ trans('modDlvPlan.STATUS_DLV') }}"},
                    {value: "FINISHED", label: "{{ trans('modDlvPlan.STATUS_FINISHED') }}"},
                    {value: "ERROR", label: "{{ trans('modDlvPlan.STATUS_ERROR') }}"},
                ];

                var statusSource =
                {
                        datatype: "array",
                        datafields: [
                            { name: 'label', type: 'string' },
                            { name: 'value', type: 'string' }
                        ],
                        localdata: statusCode
                };

                var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                    autoBind: true
                });

                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'id', type: 'number' },
                        { name: 'dlv_type', type: 'string', values: { source: dlvAdapter.records, value: 'value', name: 'label' } },
                        { name: 'dlv_no', type: 'string' },
                        { name: 'ord_id', type: 'number' },
                        { name: 'owner_nm', type: 'string' },
                        { name: 'status', type: 'string', values: { source: statusAdapter.records, value: 'value', name: 'label' } },
                        { name: 'etd', type: 'date', cellsformat: 'yyyy-MM-dd', filtertype: 'date' },
                        { name: 'sys_etd', type: 'date', cellsformat: 'yyyy-MM-dd HH:mm:ss', filtertype: 'date' },
                        { name: 'ord_no', type: 'string' },
                        { name: 'sys_ord_no', type: 'string' },
                        { name: 'cust_nm', type: 'string' },
                        //{ name: 'pick_cust_nm', type: 'string' },
                        //{ name: 'dlv_cust_nm', type: 'string' },
                        { name: 'addr', type: 'string' }               
                    ],
                    root:"Rows",
                    pagenum: 1,					
                    beforeprocessing: function (data) {
                    },
                    pagesize: 200,
                    cache: false,
                    url: BASE_URL + '/getDlvPlan/' + rowData.dlv_no,
                    sortcolumn: 'ord_no',
                    sortdirection: 'asc'
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $('#dlvPlanGrid1').jqxGrid('destroy');
                $("#dlvPlanBody1").html('<div id="dlvPlanGrid1"></div>');
                $("#dlvPlanGrid1").jqxGrid(
                {
                    width: '100%',
                    height: '500px',
                    //autoheight: true,
                    source: dataAdapter,
                    sortable: true,
                    filterable: true,
                    altrows: true,
                    showfilterrow: false,
                    columnsresize: true,
                    selectionmode: 'singlerow',
                    enablebrowserselection: true,
                    ready: function () {
                        $('#dlvPlanGrid1').jqxGrid('autoresizecolumns');
                    },
                    columns: [
                        { text: 'id', datafield: 'id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.dlvType")}}', datafield: 'dlv_type', width: 100, hidden: false },
                        { text: 'dlvNo', datafield: 'dlv_no', width: 50, hidden: true },
                        { text: 'ord_id', datafield: 'ord_id', width: 50, hidden: true },
                        { text: '{{trans("modDlv.status")}}', datafield: 'status', width: 110, hidden:true},
                        { text: '{{trans("modOrder.etd")}}', datafield: 'etd', width: 120, cellsformat: 'yyyy-MM-dd',filtertype: 'date', hidden:true },
                        { text: '{{trans("modOrder.etd")}}', datafield: 'sys_etd', width: 180, cellsformat: 'yyyy-MM-dd HH:mm:ss',filtertype: 'date' },
                        { text: '{{trans("modDlvPlanView.ownerNm")}}', datafield: 'owner_nm', width: 110},
                        { text: '{{trans("modOrder.ordNo")}}', datafield: 'ord_no', width: 120, hidden: true },
                        { text: '{{trans("modOrder.sysOrdNo")}}', datafield: 'sys_ord_no', width: 120, hidden:true },
                        { text: '{{trans("modDlvPlanView.custNm")}}', datafield: 'cust_nm', width: 120 },
                        //{ text: '{{trans("modOrder.pickCustNm")}}', datafield: 'pick_cust_nm', width: 100 },
                        //{ text: '{{trans("modOrder.dlvCustNm")}}', datafield: 'dlv_cust_nm', width: 101 },
                        { text: '{{trans("modOrder.dlvAddr")}}', datafield: 'addr', width: 300 }
                    ]
                });
                $("#dlvPlanModal1").modal('show');
            }
        },      
        @endif
        @can('TransportationPlan_CompulsiveDelete')
        {
            btnId:"btnCompulsiveDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"強制刪除",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                var dlvNo = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    console.log(row);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                        dlvNo.push(row.dlv_no);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                swal({
                    title: "確認視窗",
                    text: "您確定要刪除嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + "/TranPlanMgmt/delDlvPlan", {'ids': ids,'dlvNo': dlvNo}, function(data){
                            if(data.msg == "success") {
                                swal("刪除成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                            }
                        });
                    
                    } else {
                    
                    }
                });
                
            }
        },
        @endcan
        @can('errorchange')
        {
            btnId:"btnerrorchange",
            btnIcon:"fa fa-newspaper-o",
            btnText:"換車運送",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }
                if(ids.length > 1) {
                    swal("請至多選擇一筆資料", "", "warning");
                    return;
                }
                swal({
                    title: "確認視窗",
                    text: "您確定要換車嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/TranPlanMgmt/error/changecar', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("操作成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $("#jqxGrid").jqxGrid('clearselection');
                                $("#cssLoading").hide();
                            }
                            else{
                                swal("操作失敗", data.errorMsg, "error");
                                $("#cssLoading").hide();
                            }
                        });
                    
                    } else {
                    
                    }
                    $("#cssLoading").show();
                    // swal("資料處理中", "", "success");
                });
            }
        },    
        @endcan
    ];
    $("#allRemove").on("click", function(){
        var idx = $('#dlvPlanGrid').jqxGrid('getselectedrowindexes');
        if(idx.length ==  0) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }

        var ids = [];
        var dlvTypes = [];
        var dlvNo = "";
        for(i in idx) {
            var rowData = $('#dlvPlanGrid').jqxGrid('getrowdata', idx[i]);
            ids.push(rowData.id);
            dlvNo = rowData.dlv_no;
        }

        $.post(BASE_URL + "/zh-TW/rmDlvPlan", {dlvNo: dlvNo, ids: ids}, function(data){
            if(data.msg == "success") {
                swal("移除完成", "", "success");
                $("#dlvPlanModal").modal('hide');
                $('#dlvPlanGrid').jqxGrid('updatebounddata');
                $('#jqxGrid').jqxGrid('updatebounddata');
                $("#jqxGrid").jqxGrid('clearselection');
            }
            else if(data.errorLog == "已完成訂單不能取消派車"){
                swal("已完成訂單不能取消派車", "", "error");
            }
            else {
                swal("操作發生問題", "", "error");
            }
        });
    
    });

    $("#calEta").on("click", function(){
        var idx = $('#jqxGrid').jqxGrid('getselectedrowindex');
        if(idx == -1) {
            swal("請選擇一筆資料", "", "warning");
            return;
        }
        var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) {
        dd = '0' + dd;
        } 
        if (mm < 10) {
        mm = '0' + mm;
        } 
        var today = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + m + ':' + s;

        $("#dTime").text(today);

        $.get(BASE_URL + "/TranPlanMgmt/getDlvEtaPlan/"+rowData.dlv_no, {}, function(data){
            if(data.msg == "success") {
                swal("計算完成", "", "success");
                $('#dlvPlanGrid1').jqxGrid('updatebounddata');
            }
            else {
                swal("操作發生問題", "", "error");
            }
        });

    });
    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });

</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
				<span id="selectcount"></span>&nbsp;&nbsp;&nbsp; <h3 class="box-title">Status </h3><strong style="color:red;"></strong>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
                <p></p>
				<div style="width:100%;" id="statusList">
                    
                </div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="jqxGrid"></div>
                    <input type="button" style="display:none" id="updategridsign" >
                    <input type="button" style="display:none" id="updategriderror" >
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>



<div class="modal fade" tabindex="-1" role="dialog" id="dlvPlanModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">派車明細</h4>
			</div>
			<div class="modal-body" id="dlvPlanBody">
				<div id="dlvPlanGrid"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-danger" id="allRemove">確定移除</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>
@endsection