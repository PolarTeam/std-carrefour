@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        派車資料<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_driver_checkin') }}">派車資料</a></li>
		<li class="active">派車資料</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
					<div class="tab-pane active" id="tab_1">

						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-3">
									<label for="dlv_no">訂單號</label>
									<input type="text" class="form-control" id="ord_no" name="ord_no" readonly="true" disabled>
								</div>
								{{-- <div class="form-group col-md-4">
									<label for="status">{{ trans('modDlv.status') }}</label>
									<select class="form-control" id="status" name="status">
										<option value="UNTREATED">{{ trans('modDlv.STATUS_UNTREATED') }}</option>
										<option value="SEND">{{ trans('modDlv.STATUS_SEND') }}</option>
										<option value="DLV">{{ trans('modDlv.STATUS_DLV') }}</option>
										<option value="ERROR">{{ trans('modDlv.STATUS_ERROR') }}</option>
										<option value="FINISHED">{{ trans('modDlv.STATUS_FINISHED') }}</option>
									</select>
								</div> --}}
								<div class="form-group col-md-3">
									<label for="upload_start_time">開始上貨時間</label>
									<input type="text" class="form-control" id="upload_start_time" name="upload_start_time"  readonly="true" disabled>
								</div>
                                <div class="form-group col-md-3">
									<label for="upload_finish_time">上貨完成時間</label>
									<input type="text" class="form-control" id="upload_finish_time" name="upload_finish_time"  readonly="true" disabled>
								</div>
                                <div class="form-group col-md-3">
									<label for="arrive_time">到達時間</label>
									<input type="text" class="form-control" id="arrive_time" name="arrive_time"  readonly="true" disabled>
								</div>

							</div>

							<div class="row">
                                <div class="form-group col-md-3">
									<label for="dlv_date">下貨時間</label>
									<input type="text" class="form-control" id="unupload_time" name="unupload_time"  readonly="true" disabled>
								</div>
								<div class="form-group col-md-3">
									<label for="unupload_time">完成時間</label>
									<input type="text" class="form-control" id="finish_time" name="finish_time"  readonly="true" disabled>
								</div>
							</div>


							@if(isset($id))
							<input type="hidden" name="id" value="{{$id}}" class="form-control">
							<input type="hidden" name="_method" value="PUT" class="form-control"> 
							@endif

						</div>
					</div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>




@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var dlv_no = "";

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt/batchDelete";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/OrderMgmt/" + mainId;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/modDlvPlan" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#mod_driver_checkin').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            $("#iAdd").hide();
            $("#iDel").hide();

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            

            var requiredColumn = [
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        


    })
</script> 

@endsection