@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('bscode_kind.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/bscodeKind') }}">{{ trans('bscode_kind.titleName') }}</a></li>
		<li class="active">{{ trans('bscode_kind.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="cd_type">{{ trans('bscode_kind.cd_type') }} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="cd_type" name="cd_type" placeholder="Enter No.">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cd_descp">{{ trans('bscode_kind.cd_descp')}} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="cd_descp" name="cd_descp" placeholder="Enter Description">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="adminonly">{{ trans('bscode_kind.adminonly')}} <span style="color:red">*<span></label>
                                    <select class="form-control" id="adminonly" name="adminonly">
                                        <option value="Y">是</option>
                                        <option value="N">否</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="name">{{ trans('users.created_by') }}</label>
                                    <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="name">{{ trans('users.created_at') }}</label>
                                    <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="name">{{ trans('users.updated_by') }}</label>
                                    <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="name">{{ trans('users.updated_at') }}</label>
                                    <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                </div>
                            </div>
                            @if(isset($id))
                            <input style="display: none" type="text" id="created_at" name="created_at" disabled="disabled">
                            <input style="display: none" type="text" id="updated_at" name="updated_at" disabled="disabled">
                            <input type="hidden" name="id" value="{{$id}}" class="form-control">
                            <input type="hidden" name="_method" value="PUT" class="form-control"> 
                            @endif
                        </div>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>


<div class="row">

	<div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_2" data-toggle="tab" aria-expanded="false">{{ trans('bscode_kind.detail') }}</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">明細</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="cd">代碼</label>
                                    <input type="text" class="form-control input-sm" name="cd" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cd_descp">代碼名稱</label>
                                    <input type="text" class="form-control input-sm" name="cd_descp" grid="true" >
                                </div>                                    
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="value1">value1</label>
                                    <input type="text" class="form-control input-sm" name="value1" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value2">value2</label>
                                    <input type="text" class="form-control input-sm" name="value2" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value3">value3</label>
                                    <input type="text" class="form-control input-sm" name="value3" grid="true" >
                                </div>                                      
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">儲存</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">取消</button>
                        </div>
                        </button>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
	</div>
	
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId    = "";
    var lang      = "en";
    var cdType    = "";
    var cust_no   = "";
    var editData  = null;
    var editObj   = null;
    var fieldData = null;
    var fieldObj  = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';


    
    @if(isset($id))
        mainId = "{{$id}}";
        cdType = "{{$cd_type}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif   
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind";
    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/bscodeKind" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#role').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            console.log($('#role').val());
            console.log('{{$canStore}}');
            console.log('{{$canEdit}}');
            console.log('{{$canDelete}}');
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        
        formOpt.beforesaveFunc = function() {
            var requiredColumn = [
                {
                    "column_filed":"cd_type",
                    "column_type":"string",
                },
                {
                    "column_filed":"cd_descp",
                    "column_type":"string",
                },
                {
                    "column_filed":"adminonly",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;

        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


        if(mainId != "") {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "cd_type", type: "string"},
                    {name: "cd", type: "string"},
                    {name: "cd_descp", type: "string"},
                    {name: "value1", type: "string"},
                    {name: "value2", type: "string"},
                    {name: "value3", type: "string"}
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "cd_type", datafield: "cd_type", width: 130, hidden: true},
                    {text: "代碼", datafield: "cd", width: 150},
                    {text: "代碼名稱", datafield: "cd_descp", width: 150},
                    {text: "value1", datafield: "value1", width: 150},
                    {text: "value2", datafield: "value2", width: 150},
                    {text: "value3", datafield: "value3", width: 150}
                ]
            ];
            var opt                = {};
            opt.gridId         = "jqxGrid";
            opt.fieldData      = col;
            opt.formId         = "subForm";
            opt.saveId         = "Save";
            opt.cancelId       = "Cancel";
            opt.showBoxId      = "subBox";
            opt.height         = 300;
            opt.getUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/bscode/" + cdType;
            opt.addUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/bscode/store";
            opt.updateUrl      = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/bscode/update/"+  cdType;
            opt.delUrl         = "{{ url(config('backpack.base.route_prefix', ''))}}/" +lang+"/bscode/delete/";
            opt.defaultKey     = {'cd_type': cdType};
            opt.showaggregates = false;
            opt.commonBtn      = true;
            opt.inlineEdit     = false;
            opt.showtoolbar    = true;
            opt.selectionmode  = 'singlerow';
            opt.beforeSave     = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
                if(!data.success) {
                   swal("{{trans('common.warning')}}", data.message, "warning");
                }
                $("#jqxGrid").jqxGrid('updatebounddata');
            }

            genDetailGrid(opt);
        }



    })
</script> 
<script>
$(function(){
});
</script>
@endsection