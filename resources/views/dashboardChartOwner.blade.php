@extends('layout.layout')

@section('header')
<section class="content-header d-flex justify-content-between">
    <h1 style='display: inline;'>
    Dashboard - 貨主圖表<small></small>
    </h1>
    <div>
        <button type="button" class="blue-sm-btn" id="selectOwnerChartBtn">篩選貨主</button>
        <input type="date" name="" id="searchOwnerChartByDay" onchange="dateChanged()" value="{{date('Y-m-d')}}">
    </div>
</section>
@endsection

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/select2/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/adminlte/plugins/pace/pace.min.css">
<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/pnotify/pnotify.custom.min.css">

<link rel="stylesheet" href="https://core.standard-info.com/vendor/backpack/backpack.base.css">

<link rel="stylesheet" href="https://core.standard-info.com/css/custom.css?v=20190305-002">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://core.standard-info.com/vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<div class="d-flex justify-content-between">
    <ul class="list-unstyled owner-list" id="chartOwner-page-list">

        @foreach($viewData['owner'] as $key=> $ownerrow)
            @if ( $key == 0)
            <li class="active">{{$ownerrow->cust_name}}</li>
            @else
            <li>{{$ownerrow->cust_name}}</li>
            @endif
        @endforeach
    </ul>

</div>
<div class="row dashboard-chart-box">
    <div class="col-lg-4">
        <div class="chart-title">總數 - <span id="chartOwnerTotalA">{{$viewData['owner'][0]->total_count}}</span></div>
        <div class="card">
            <canvas id="chartOwnerA" class="chart-today" style="height: calc(100% - 295px); width:100%"></canvas>
            <div class="color-list-box">
                <ul class="list-unstyled color-list">
                    <li>揀貨完成</li>
                    <li>貨物裝載中</li>
                    <li>提貨完成</li>
                    <li>貨物卸載中</li>
                    <li>配送完成</li>
                </ul>
                <ul class="list-unstyled color-list" id="percentA">
                    <li>{{ empty( $viewData['owner'][0]->total_status['PICKEDFINISH']) ? 0 : $viewData['owner'][0]->total_status['PICKEDFINISH'] / $viewData['owner'][0]->total_count *10}}%</li>
                    <li>{{ empty( $viewData['owner'][0]->total_status['LOADING']) ? 0 : $viewData['owner'][0]->total_status['LOADING']  / $viewData['owner'][0]->total_count  *10}}%</li>
                    <li>{{ empty( $viewData['owner'][0]->total_status['UPLOADFINISH']) ? 0 : $viewData['owner'][0]->total_status['UPLOADFINISH'] / $viewData['owner'][0]->total_count  *10}}%</li>
                    <li>{{ empty( $viewData['owner'][0]->total_status['OFFSTAFF']) ? 0 : $viewData['owner'][0]->total_status['OFFSTAFF'] / $viewData['owner'][0]->total_count *10 }}%</li>
                    <li>{{ empty( $viewData['owner'][0]->total_status['FINISHED']) ? 0 : $viewData['owner'][0]->total_status['FINISHED'] / $viewData['owner'][0]->total_count *10 }}%</li>
                </ul>
                <ul class="list-unstyled color-list" id="numA">
                    <li>{{$viewData['owner'][0]->total_status['PICKEDFINISH']}}</li>
                    <li>{{$viewData['owner'][0]->total_status['LOADING']}}</li>
                    <li>{{$viewData['owner'][0]->total_status['UPLOADFINISH']}}</li>
                    <li>{{$viewData['owner'][0]->total_status['OFFSTAFF']}}</li>
                    <li>{{$viewData['owner'][0]->total_status['FINISHED']}}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-8 h-100">
        <div class="row h-100">
            <div class="col-lg-6 h-50">
                <div class="chart-title">北 - <span id="chartOwnerTotalB">{{$viewData['owner'][0]->north}}</span></div>
                <div class="card mb-3">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col-lg-5 p-0">
                            <canvas id="chartOwnerB" style="height:100%; width:100%"></canvas>
                        </div>
                        <div class="col-lg-7">
                            <div class="color-list-box">
                                <ul class="list-unstyled color-list">
                                    <li>揀貨完成</li>
                                    <li>貨物裝載中</li>
                                    <li>提貨完成</li>
                                    <li>貨物卸載中</li>
                                    <li>配送完成</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="percentB">
                                    <li>{{ empty( $viewData['owner'][0]->north_status['PICKEDFINISH']) ? 0 : $viewData['owner'][0]->north_status['PICKEDFINISH'] / $viewData['owner'][0]->north *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->north_status['LOADING']) ? 0 : $viewData['owner'][0]->north_status['LOADING']  / $viewData['owner'][0]->north  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->north_status['UPLOADFINISH']) ? 0 : $viewData['owner'][0]->north_status['UPLOADFINISH'] / $viewData['owner'][0]->north  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->north_status['OFFSTAFF']) ? 0 : $viewData['owner'][0]->north_status['OFFSTAFF'] / $viewData['owner'][0]->north *10 }}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->north_status['FINISHED']) ? 0 : $viewData['owner'][0]->north_status['FINISHED'] / $viewData['owner'][0]->north *10 }}%</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="numB">
                                    <li>{{$viewData['owner'][0]->north_status['PICKEDFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->north_status['LOADING']}}</li>
                                    <li>{{$viewData['owner'][0]->north_status['UPLOADFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->north_status['OFFSTAFF']}}</li>
                                    <li>{{$viewData['owner'][0]->north_status['FINISHED']}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 h-50">
                <div class="chart-title">中 - <span id="chartOwnerTotalC">{{$viewData['owner'][0]->middle}}</span></div>
                <div class="card mb-3">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col-lg-5 p-0">
                            <canvas id="chartOwnerC" style="height:100%; width:100%"></canvas>
                        </div>
                        <div class="col-lg-7">
                            <div class="color-list-box">
                                <ul class="list-unstyled color-list">
                                    <li>揀貨完成</li>
                                    <li>貨物裝載中</li>
                                    <li>提貨完成</li>
                                    <li>貨物卸載中</li>
                                    <li>配送完成</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="percentC">
                                    <li>{{ empty( $viewData['owner'][0]->middle_status['PICKEDFINISH']) ? 0 : $viewData['owner'][0]->north_status['PICKEDFINISH'] / $viewData['owner'][0]->middle *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->middle_status['LOADING']) ? 0 : $viewData['owner'][0]->north_status['LOADING']  / $viewData['owner'][0]->middle  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->middle_status['UPLOADFINISH']) ? 0 : $viewData['owner'][0]->north_status['UPLOADFINISH'] / $viewData['owner'][0]->middle  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->middle_status['OFFSTAFF']) ? 0 : $viewData['owner'][0]->north_status['OFFSTAFF'] / $viewData['owner'][0]->middle *10 }}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->middle_status['FINISHED']) ? 0 : $viewData['owner'][0]->north_status['FINISHED'] / $viewData['owner'][0]->middle *10 }}%</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="numC">
                                    <li>{{$viewData['owner'][0]->middle_status['PICKEDFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->middle_status['LOADING']}}</li>
                                    <li>{{$viewData['owner'][0]->middle_status['UPLOADFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->middle_status['OFFSTAFF']}}</li>
                                    <li>{{$viewData['owner'][0]->middle_status['FINISHED']}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 h-50">
                <div class="chart-title">南 - <span id="chartOwnerTotalD">{{$viewData['owner'][0]->south}}</span></div>
                <div class="card mb-3">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col-lg-5 p-0">
                            <canvas id="chartOwnerD" style="height:100%; width:100%"></canvas>
                        </div>
                        <div class="col-lg-7">
                            <div class="color-list-box">
                                <ul class="list-unstyled color-list">
                                    <li>揀貨完成</li>
                                    <li>貨物裝載中</li>
                                    <li>提貨完成</li>
                                    <li>貨物卸載中</li>
                                    <li>配送完成</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="percentD">
                                    <li>{{ empty( $viewData['owner'][0]->south_status['PICKEDFINISH']) ? 0 : $viewData['owner'][0]->north_status['PICKEDFINISH'] / $viewData['owner'][0]->south *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->south_status['LOADING']) ? 0 : $viewData['owner'][0]->north_status['LOADING']  / $viewData['owner'][0]->south  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->south_status['UPLOADFINISH']) ? 0 : $viewData['owner'][0]->north_status['UPLOADFINISH'] / $viewData['owner'][0]->south  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->south_status['OFFSTAFF']) ? 0 : $viewData['owner'][0]->north_status['OFFSTAFF'] / $viewData['owner'][0]->south *10 }}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->south_status['FINISHED']) ? 0 : $viewData['owner'][0]->north_status['FINISHED'] / $viewData['owner'][0]->south *10 }}%</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="numD">
                                    <li>{{$viewData['owner'][0]->south_status['PICKEDFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->south_status['LOADING']}}</li>
                                    <li>{{$viewData['owner'][0]->south_status['UPLOADFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->south_status['OFFSTAFF']}}</li>
                                    <li>{{$viewData['owner'][0]->south_status['FINISHED']}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 h-50">
                <div class="chart-title">東 - <span id="chartOwnerTotalE">{{$viewData['owner'][0]->east}}</span></div>
                <div class="card mb-3">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col-lg-5 p-0">
                            <canvas id="chartOwnerE" style="height:100%; width:100%"></canvas>
                        </div>
                        <div class="col-lg-7">
                            <div class="color-list-box">
                                <ul class="list-unstyled color-list">
                                    <li>揀貨完成</li>
                                    <li>貨物裝載中</li>
                                    <li>提貨完成</li>
                                    <li>貨物卸載中</li>
                                    <li>配送完成</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="percentE">
                                    <li>{{ empty( $viewData['owner'][0]->east_status['PICKEDFINISH']) ? 0 : $viewData['owner'][0]->north_status['PICKEDFINISH'] / $viewData['owner'][0]->east *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->east_status['LOADING']) ? 0 : $viewData['owner'][0]->north_status['LOADING']  / $viewData['owner'][0]->east  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->east_status['UPLOADFINISH']) ? 0 : $viewData['owner'][0]->north_status['UPLOADFINISH'] / $viewData['owner'][0]->east  *10}}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->east_status['OFFSTAFF']) ? 0 : $viewData['owner'][0]->north_status['OFFSTAFF'] / $viewData['owner'][0]->east *10 }}%</li>
                                    <li>{{ empty( $viewData['owner'][0]->east_status['FINISHED']) ? 0 : $viewData['owner'][0]->north_status['FINISHED'] / $viewData['owner'][0]->east *10 }}%</li>
                                </ul>
                                <ul class="list-unstyled color-list" id="numE">
                                    <li>{{$viewData['owner'][0]->east_status['PICKEDFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->east_status['LOADING']}}</li>
                                    <li>{{$viewData['owner'][0]->east_status['UPLOADFINISH']}}</li>
                                    <li>{{$viewData['owner'][0]->east_status['OFFSTAFF']}}</li>
                                    <li>{{$viewData['owner'][0]->east_status['FINISHED']}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<style>
    .h-100 {
        height: 100%;
    }
    .h-50 {
        height: 50%;
    }
    .p-0 {
        padding: 0!important;
    }
    .mb-3 {
        margin-bottom: 16px;
    }
    .d-flex {
        display: flex;
    }
    .align-items-center {
        align-items: center;
    }
    .align-items-end {
        align-items: flex-end;
    }
    .justify-content-between {
        justify-content: space-between;
    }
    .dashboard-chart-box {
        height: calc(100% - 122px);
    }
    .chart-title {
        font-size: 20px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .card {
        background: #fff;
        border-radius: 8px;
        padding: 20px;
        height: calc(100% - 40px);
    }
    .chart-today {
        margin-bottom: 20px;
    }
    .color-list-box {
        display: flex;
        justify-content: space-between;
    }
    .color-list-box.today {
        height: calc(100% - 418px);
        align-items: center;
    }
    .color-list {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    .color-list li {
        font-size: 25px;
        font-weight: bold;
        margin-bottom: 20px;
    }
    .color-list:first-of-type li {
        padding-left: 20px;
        position: relative;
    }
    .color-list:first-of-type li:before {
        content: "";
        display: block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
    }
    .color-list:last-of-type li {
        text-align: right;
    }
    .color-list li:nth-of-type(5n + 1) {
        color: #9C27B0;
    }
    .color-list:first-of-type li:nth-of-type(5n + 1):before {
        background: #9C27B0;
    }
    .color-list li:nth-of-type(5n + 2) {
        color: #e80c0c;
    }
    .color-list:first-of-type li:nth-of-type(5n + 2):before {
        background: #e80c0c;
    }
    .color-list li:nth-of-type(5n + 3) {
        color: #F47920;
    }
    .color-list:first-of-type li:nth-of-type(5n + 3):before {
        background: #F47920;
    }
    .color-list li:nth-of-type(5n + 4) {
        color: #333;
    }
    .color-list:first-of-type li:nth-of-type(5n + 4):before {
        background: #ffc423;
    }
    .color-list li:nth-of-type(5n + 5) {
        color: #13ab67;
    }
    .color-list:first-of-type li:nth-of-type(5n + 5):before {
        background: #13ab67;
    }

    .blue-sm-btn {
        font-size: 14px;
        color: #fff;
        background: #19294A;
        padding: 4px 10px;
        border-radius: 8px;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        margin-bottom: 8px;
        margin-left: 16px;
        white-space: nowrap;
    }
    .owner-list {
        display: flex;
        overflow-x: auto;
        padding: 0 20px;
    }
    .owner-list button {
      background: transparent;
      border: 0px solid transparent;
      outline: 0px solid transparent;
      font-size: 16px;
      color: #999999;
      font-weight: 400;
      margin: 0 16px 16px 0;
      padding: 0 2px;
      white-space: nowrap;
    }
    .owner-list .active {
        color: #f47920;
        font-weight: bold;
        border-bottom: 1px solid #f47920;
    }
    .orange-btn {
        color: #F47920;
        border: 1px solid #F47920;
        border-radius: 12px;
        background: #fff;
        font-size: 16px;
        padding: 12px 24px;
        margin: 0 4px 8px 8px;
    }
    .swal2-content .title {
        font-size: 20px;
        color: #333333;
        font-weight: 400;
    }
    .swal2-content .sub-title {
        font-size: 16px;
        color: #999999;
        font-weight: 400;
    }
    .swal2-content .sub-title .num {
        color: #3079ae;
    }
    .swal2-content label {
        font-size: 14px;
        color: #666666;
        font-weight: 400;
    }
    .swal2-content input {
        font-size: 14px;
        border: 1px solid #efeff1;
        padding: 12px;
        color: #333333;
        border-radius: 4px;
        width: 100%;
    }
    .swal2-content .autocomplet {
        width: 100%;
    }
    .swal2-content .autocomplet .search-btn {
        border: 1px solid #efeff1;
        color: #333333;
        border-radius: 4px;
        width: 44px;
        height: 44px;
        background: #fff;
        font-size: 14px;
        display: inline-block;
    }
    .swal2-content .autocomplet input {
        width: calc(100% - 49px);
        display: inline-block;
    }
    .swal2-content .select {
        position: relative;
    }
    .swal2-content .select select {
        font-size: 14px;
        border: 1px solid #efeff1;
        padding: 12px;
        color: transparent;
        border-radius: 4px;
        width: 100%;
        background: transparent;
        position: relative;
        z-index: 3;
    }
    .swal2-content .select::before {
        content: "\f107";
        font-family: "FontAwesome";
        font-weight:400;
        display: block;
        position: absolute;
        top: 50%;
        right: 20px;
        color: #c5c8ce;
        font-size: 20px;
        transform: translateY(-50%);
        z-index: 2;
    }
    .swal2-content .select p {
        position: absolute;
        top: 50%;
        left: 0;
        transform: translateY(-50%);
        color: #333333;
        font-size: 14px;
        z-index: 1;
        border: 1px solid #efeff1;
        padding: 12px;
        border-radius: 4px;
        background: #fff;
        height: 100%;
        width: 100%;
    }
    .swal2-content .border-bottom {
        padding-bottom: 16px;
        margin-bottom: 16px;
        border-bottom: 1px solid #D2D6DE;
    }
    .swal2-content .can-remove-list {
        text-align: start;
        max-height: 400px;
        overflow-y: auto;
    }
    .swal2-content .can-remove-list li {
        color: #444444;
        font-size: 16px;
        margin-bottom: 24px;
    }
    .swal2-content .can-remove-list button {
        background: transparent;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        color: #EA2020;
        font-size: 16px;
        display: inline-block;
        margin-right: 16px;
    }
    .mb-3 {
        margin-bottom: 16px;
    }
    .mr-2 {
        margin-right: 8px;
    }
    @media (max-width: 1440px) {
        .color-list li {
            font-size: 14px;
            margin-bottom: 12px;
        }
    }
</style>




@endsection

@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    let initowner = "";
    var initwh = "";
    var lastentrydata = "";
    // searchWhChartByDay
    $(document).ready(function() {
        window.setTimeout(function() {

            // let canRemoveList = document.getElementById('can-remove-list').children;
            // let canRemoveListLength = canRemoveList.length;
            // for (let i = 0; i < canRemoveListLength; i++) {
            //     canRemoveList[i].style.display = 'none';
            // }
            // ownerRow = [];
            // setPageBtnEventListener('chartOwner', ownerRow, pageSize);
            // endChartOwnerCarouselEvent();
            // starChartOwnerCarouselEvent();
            // chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);

            @if(isset($viewData['ownerarray']))
                initwh = "{{$viewData['ownerarray']}}";
            @endif
            initwh = initwh.split(',');
            let selectArr = $("#check_owner").val() || [];            
            initwh.forEach((item, index) => { 
                try {
                    if(item!= "") {
                        let hasIndex = ownerRow.findIndex((itm) => item === itm.cust_no);
                        let allDataIndex = ownerAllData.findIndex((itm) => item === itm.cust_no);
                        if (hasIndex === -1) {
                            ownerRow.push(ownerAllData[allDataIndex]);
                            document.getElementById('can-remove-list').children[allDataIndex].style.display = 'block';
                        }
                    }
                } catch (error) {
                    
                }
            });
            setPageBtnEventListener('chartOwner', ownerRow, pageSize);
            endChartOwnerCarouselEvent();
            starChartOwnerCarouselEvent();
            chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
        }, 1000);
    });
    function dateChanged() {
        var input = document.getElementById("searchOwnerChartByDay");
        console.log("Selected date: " + input.value);
        let wharray = [];
        
        for (let index = 0; index < ownerRow.length; index++) {
            if(ownerRow[index] != undefined) {
                wharray.push(ownerRow[index].cust_no);
            }
        }
        $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: BASE_URL + '/zh-TW/dashboardChartOwnerbydate',
            data:JSON.stringify({
                "date"             : input.value,
                "wharray"             : wharray,
            }),
            success: function(data) {
                console.log(data);
                if(data.data.owner.length == 0 && lastentrydata != input.value) {
                    alert(`${input.value} 目前尚無資料，請選擇其他日期`);
                    lastentrydata = input.value;
                    return
                }
                ownerAllData = data.data.owner || [];
                ownerAllData.forEach((item) => {
                    ownerRow.forEach((itm, indx) => {
                        if (item.cust_no === itm.cust_no) {
                            ownerRow[indx] = item;
                        }
                    })
                })
                setPageBtnEventListener('chartOwner', ownerRow, pageSize);
                endChartOwnerCarouselEvent();
                starChartOwnerCarouselEvent();
                chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
            }
        });
    }

    const chartOwnerElList = [] // 圖表
    let ownerAllData = JSON.parse(('{{$viewData["owner"]}}').split('&quot;').join('"'));
    let ownerRow = [];
    console.log(ownerRow);
    let chartOwnerInPageNums = 1; // 貨主頁碼
    let pageSize = 1;  // 一頁幾筆
    var refreshtime    = parseInt("{{$viewData['refreshtime']}}");
    let chartOwnerChengePageTimeout = refreshtime || 3000;
    let chartOwnerCarousel;
    // 圖表
    let boxKey = ['A', 'B', 'C', 'D', 'E'];
    let boxKeyLength = boxKey.length;
    const data = null;
    // 創立圖表內容
    function createChartOwner() {
        for (let i = 0; i < boxKeyLength; i++) {
            chartOwnerElList.push({
                el: document.getElementById(`chartOwner${boxKey[i]}`),
                chart: new Chart(document.getElementById(`chartOwner${boxKey[i]}`), {
                    type: 'doughnut',
                    data: {
                        labels: [
                            '揀貨完成',
                            '貨物裝載中',
                            '提貨完成',
                            '貨物卸載中',
                            '配送完成'
                        ],
                        datasets: [{
                            label: 'My '+boxKey[i]+' Dataset',
                            data: [0, 0, 0, 0, 0],
                            backgroundColor: [
                            '#9C27B0',
                            '#e80c0c',
                            '#F47920',
                            '#ffc423',
                            '#13ab67'
                            ],
                            hoverOffset: 4
                        }]
                    },
                    options: {
                        legend: {
                            "display": false
                        }
                    },
                })
            })
        }
    }
    // 更換圖表內容
    function updateConfigAsNewObject(data) {
        for (let i = 0; i < boxKeyLength; i++) {
            // 更新數據
            let percent = document.getElementById(`percent${boxKey[i]}`).children;
            let num = document.getElementById(`num${boxKey[i]}`).children;
            let childrenLength = percent.length;
            let key;
            switch (i) {
                case 0:
                    key = 'total';
                    break
                case 1:
                    key = 'north';
                    break
                case 2:
                    key = 'middle';
                    break
                case 3:
                    key = 'south';
                    break
                case 4:
                    key = 'east';
                    break
            }
            try {
                let newChartData = [];
                if (data != undefined) {
                    newChartData = [data[key + '_status'].PICKEDFINISH || 0, data[key + '_status'].LOADING || 0, data[key + '_status'].UPLOADFINISH || 0, data[key + '_status'].OFFSTAFF || 0, data[key + '_status'].FINISHED || 0];
                } else {
                    newChartData = [0,0,0,0,0]
                }
                let value1 = parseFloat(`${newChartData[0] ? Math.round(newChartData[0] / (data != undefined ? data[key === 'total' ? 'total_count' : key] : 0) * 1000)/10 : 0}`);
                let value2 = parseFloat(`${newChartData[1] ? Math.round(newChartData[1] / (data != undefined ? data[key === 'total' ? 'total_count' : key] : 0) * 1000)/10 : 0}`);
                let value3 = parseFloat(`${newChartData[2] ? Math.round(newChartData[2] / (data != undefined ? data[key === 'total' ? 'total_count' : key] : 0) * 1000)/10 : 0}`);
                let value4 = parseFloat(`${newChartData[3] ? Math.round(newChartData[3] / (data != undefined ? data[key === 'total' ? 'total_count' : key] : 0) * 1000)/10 : 0}`);
                // console.log(value1)
                let sum = value1 + value2 + value3 + value4;
                let value5 = sum > 0 ? parseFloat((100-sum).toFixed(1)) : 0 ;
                percent[0].textContent = value1+'%';
                percent[1].textContent = value2+'%';
                percent[2].textContent = value3+'%';
                percent[3].textContent = value4+'%';
                percent[4].textContent = value5+'%';
                num[0].textContent = newChartData[0];
                num[1].textContent = newChartData[1];
                num[2].textContent = newChartData[2];
                num[3].textContent = newChartData[3];
                num[4].textContent = newChartData[4];
                if (data != undefined) {
                    document.getElementById(`chartOwnerTotal${boxKey[i]}`).textContent = data[key === 'total' ? 'total_count' : key];
                } else {
                    document.getElementById(`chartOwnerTotal${boxKey[i]}`).textContent = 0;
                }
                // 更新圖表
                // console.log(chartOwnerElList[i].chart.data.datasets[0].data);
                chartOwnerElList[i].chart.data.datasets[0].data = newChartData;
                chartOwnerElList[i].chart.update();
            } catch (error) {
                console.log('error');  
            }

        }
    }
    createChartOwner();
    setPageBtnEventListener('chartOwner', ownerRow, pageSize);
    starChartOwnerCarouselEvent();
    chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
    // 換頁
    function chengePageEvent(type, data, pageNums, pageSize) {
        updateConfigAsNewObject(data[pageNums - 1]);
        // 頁數 focuse
        let totalCont = Math.ceil(data.length / pageSize);
        let scrollNum = 0;
        for (let i = 0; i < totalCont; i++) {
            if(document.getElementById(`${type}-page-${i + 1}`) != null) {
                document.getElementById(`${type}-page-${i + 1}`).classList.remove('active');
                if (i + 1 < pageNums ) {
                    scrollNum += (document.getElementById(`${type}-page-${i + 1}`).clientWidth + 16);
                }
            }
        };
        chartOwnerInPageNums = pageNums;
        try {
            document.getElementById(`${type}-page-${pageNums}`).classList.add('active');
        // 滾動到滾動到
        document.getElementById(`${type}-page-list`).scrollLeft = scrollNum;
        } catch (error) {
            console.log('this is error');
        }
    }
    // 點擊頁碼換頁
    function setPageBtnEventListener(type, data, pageSize) {
        let totalCont = Math.ceil(data.length / pageSize);
        document.getElementById(`${type}-page-list`).innerHTML = '';
        // console.log(totalCont);
        for (let i = 0; i < totalCont; i++) {
            try {
                let newli = document.createElement('li');
                newli.innerHTML = `<button type="button" class="active" id="${type}-page-${i + 1}">${data[i].cust_name}(${data[i].cust_no})</button>`
                document.getElementById(`${type}-page-list`).appendChild(newli);
                // console.log(`${type}-page-${i + 1}`);
                document.getElementById(`${type}-page-${i + 1}`).addEventListener('click', function() {
                    chengePageEvent(type, data, i + 1, pageSize);
                    endChartOwnerCarouselEvent();
                    starChartOwnerCarouselEvent();
                });
            } catch (error) {
                
            }

        }
    }
    // 輪播換頁
    function carouselEvent (type, data) {
        let totalCont = Math.ceil(initwh.length / pageSize);
        switch (type) {
            case 'chartOwner':
                chartOwnerInPageNums += 1;
                if (chartOwnerInPageNums > totalCont) {
                    chartOwnerInPageNums = 1;
                };
                break;
        }
        chengePageEvent(type, data, chartOwnerInPageNums, pageSize);
    }
    function starChartOwnerCarouselEvent () {
        chartOwnerCarousel = setInterval(() => {
        carouselEvent('chartOwner', ownerRow);
        }, chartOwnerChengePageTimeout);
    }
    function endChartOwnerCarouselEvent () {
        clearInterval(chartOwnerCarousel);
    }
    // 篩選貨主
    let selectOwnerChartBtn = document.getElementById('selectOwnerChartBtn');
    selectOwnerChartBtn.addEventListener('click', function() {
        let ownerList = '';
        let option = '';
        ownerAllData.forEach((item, index) => {
            ownerList += `
            <li id="li-owner-${index}" style='display: ${(ownerRow.findIndex((itm) => (item.cust_no === itm.cust_no)) !== -1) ? 'block' : 'none'};'>
            <button type='button' id="owner-${index}" data-key="${item.cust_name}"><i class='fa fa-times' aria-hidden='true'></i></button>
            <span>${item.cust_name}(${item.cust_no})</span>
            </li>`;
            option += `<option value="${item.cust_no}">${item.cust_name}(${item.cust_no})</option>`;
        });
        let ordOwnerHtml = 
            "<div class='col-md-12' style='text-align: left';><label class='title'>篩選貨主</label></div>\
            <div class='col-md-12'>\
            <div class='row d-flex align-items-end border-bottom'>\
                <div class='col-md-6' style='text-align: left';><label>貨主</label>\
                    <div class='form-group'>\
                        <input type='hidden' name='check_owner_name' id = 'check_owner_name' class='form-control'>\
                        <select class='form-control select2' id='check_owner' name='check_owner' multiple='multiple'>\
                            "+option+
                            "</select>\
                    </div>\
                </div>\
                <div class='col-md-6 d-flex align-items-end'>\
                <button type='button' class='orange-btn' id='ownerAddBottom'>新增</button>\
                <button type='button' class='orange-btn' id='ownerClearAllBottom'>全部取消</button>\
                </div>\
            </div>\
            </div>\
            <div class='col-md-12'>\
            <ul class='list-unstyled can-remove-list' id='can-remove-list'>" + ownerList + "</ul>\
            </div>"
            ;
        swal({
            title            : '',
            icon             : 'info',
            html             : ordOwnerHtml,
            width            : '40%',
            showCloseButton  : true,
            showCancelButton : false,
            focusConfirm     : false,
            showConfirmButton: false,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            onOpen: function () {
                $('#check_owner').select2();
            },
        }).then((result) => {
            console.log('confirm do status');
            dateChanged();
        });
        // 監聽刪除按鈕
        ownerAllData.forEach((item, index) => {
            setRemoveBtnEvent(item, index);
        });
        //  監聽新增按鈕
        document.getElementById('ownerAddBottom').addEventListener('click', function() {
            // console.log($("#check_owner").val());
            let selectArr = $("#check_owner").val() || [];
            selectArr.forEach((item) => {
                try {
                    let hasIndex = ownerRow.findIndex((itm) => item === itm.cust_no);
                    let allDataIndex = ownerAllData.findIndex((itm) => item === itm.cust_no);
                    if (hasIndex === -1) {
                        ownerRow.push(ownerAllData[allDataIndex]);
                        document.getElementById('can-remove-list').children[allDataIndex].style.display = 'block';
                    }
                } catch (error) {
                    console.log('error');
                }
            });
            setPageBtnEventListener('chartOwner', ownerRow, pageSize);
            endChartOwnerCarouselEvent();
            starChartOwnerCarouselEvent();
            chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
            dateChanged();
            $("#check_owner").val('');
            $("#check_owner").trigger('change');
        })
        // 監聽刪除按鈕 function
        function setRemoveBtnEvent(item, index) {
            document.getElementById(`owner-${index}`).addEventListener('click', function() {
                document.getElementById(`li-owner-${index}`).style.display = 'none';
                let removeIndex = ownerRow.findIndex((itm) => item.cust_name === itm.cust_name);
                ownerRow.splice(removeIndex, 1);
                // console.log(ownerRow);
                setPageBtnEventListener('chartOwner', ownerRow, pageSize);
                endChartOwnerCarouselEvent();
                starChartOwnerCarouselEvent();
                chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
            })
        }
        // 監聽全部取消按鈕
        document.getElementById('ownerClearAllBottom').addEventListener('click', function() {
            let canRemoveList = document.getElementById('can-remove-list').children;
            let canRemoveListLength = canRemoveList.length;
            for (let i = 0; i < canRemoveListLength; i++) {
                canRemoveList[i].style.display = 'none';
            }
            ownerRow = [];
            setPageBtnEventListener('chartOwner', ownerRow, pageSize);
            endChartOwnerCarouselEvent();
            starChartOwnerCarouselEvent();
            chengePageEvent('chartOwner', ownerRow, chartOwnerInPageNums, pageSize);
            dateChanged();
            $("#check_owner").val('');
            $("#check_owner").trigger('change');
        })
    });


    var count = 0;
    dashboardWebsocket();
    function dashboardWebsocket () {
        let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/groupId_Dsvupdatedatsbboard';
        let ws = new WebSocket(wsURL); // 建立連線
        ws.onopen = function(e) {
           
            // websocketonopen(e);
            // console.log('ws 連線成功~~');
        };
        ws.error = function(error) {
            // websocketonerror(error);
            // console.error('ws 連線失敗', error);
        };
        ws.onmessage = function(e) {
            if(count >0 ) {
                dateChanged();
            }
            console.log('dashboardWebsocket');
            count ++;
        };
        ws.onclose = function(e) {

        };
    }
    function websocketsend (data) {
        // 前端丟資料
        console.log('send datanick', data);
    }
    function websocketclose () {
        console.log('ws 關閉連線nick');
    }

</script>
<script src="https://unpkg.com/vue"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.0/firebase.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jqwidgets') }}/jqxwindow.js"></script>


@endsection