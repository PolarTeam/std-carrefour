@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('mod_order.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/project') }}">{{trans('mod_order.titleName')}}</a></li>
		<li class="active">{{trans('mod_order.titleName')}}</li>
      </ol>
    </section>
    <script>
        var truck_cmp_no = "";
        var truck_cmp_no = "";
        var whAddr = "";
    </script>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
						<li class=""><a href="#tab_7" data-toggle="tab" aria-expanded="false">提貨配送資料</a></li>
						<li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">其他項目</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-3">
										<label for="sys_ord_no">{{ trans('mod_order.sys_ord_no') }}</label>
										<input type="text" class="form-control" id="sys_ord_no" name="sys_ord_no" switch="off">
									</div>
									<div class="form-group col-md-3">
										<label for="ord_no">{{ trans('mod_order.ord_no') }}</label>
										<input type="text" class="form-control" id="ord_no" name="ord_no" >
									</div>
									<div class="form-group col-md-3">
										<label for="cust_ord_no">{{ trans('mod_order.cust_ord_no') }}</label>
										<input type="text" class="form-control" id="cust_ord_no" name="cust_ord_no" >
									</div>
                                    <div class="form-group col-md-3">
										<label for="dlv_ord_no">{{ trans('mod_order.dlv_ord_no') }}</label>
										<input type="text" class="form-control" id="dlv_ord_no" name="dlv_ord_no" >
									</div>

								</div>
								<div class="row">
									<div class="form-group col-md-3">
										<label for="status">狀態</label>
										<select class="form-control" id="status" name="status" switch="off">
                                            <option value="PICKEDFINISH">{{ trans('mod_order.STATUSPICKEDFINISH') }}</option>
											<option value="PICKEDFINISH">{{ trans('mod_order.STATUSUNTREATED') }}</option>
											<option value="SEND">{{ trans('mod_order.STATUSSEND') }}</option>
											<option value="LOADING">{{ trans('mod_order.STATUSLOADING') }}</option>
											<option value="SETOFF">{{ trans('mod_order.STATUSSETOFF') }}</option>
											<option value="NORMAL">{{ trans('mod_order.STATUSNORMAL') }}</option>
											<option value="PICKED">{{ trans('mod_order.STATUSPICKED') }}</option>
											<option value="ERROR">{{ trans('mod_order.STATUSERROR') }}</option>
											<option value="DELAY">{{ trans('mod_order.STATUSDELAY') }}</option>
											<option value="FINISHED">{{ trans('mod_order.STATUSFINISHED') }}</option>
											<option value="CLOSE">{{ trans('mod_order.STATUSCLOSE') }}</option>
											<option value="REJECT">{{ trans('mod_order.STATUSREJECT') }}</option>
											<option value="READY">可派車</option>
											<option value="READY_PICK">已提貨可派車</option>
										</select>
									</div>
									<div class="form-group col-md-3">
										<label for="dlv_date">{{ trans('mod_order.dlv_date') }}</label>
										<input type="text" class="form-control" id="dlv_date" name="dlv_date" >
									</div>
									<div class="form-group col-md-3">
									<label for="trs_mode">{{ trans('mod_order.trs_mode') }}</label>
										<select class="form-control" id="trs_mode" name="trs_mode" no-clear="Y"  >
                                            <option value="配送">配送</option>
                                            <option value="轉運">轉運</option>
											<option value="區配">區配</option>
											<option value="直達">直達</option>
											<option value="專車">專車</option>
										</select>
									</div>
                                    <div class="form-group col-md-3">
										<label for="driver_phone">{{ trans('mod_order.driver_phone') }}</label>
										<input type="text" class="form-control" id="driver_phone" name="driver_phone">
									</div>
								</div>
                                <div class="row">
                                    <div class="form-group col-md-3">
										<input type="hidden" class="form-control" id="owner_cd" name="owner_cd" >
										<label for="owner_nm">{{ trans('mod_order.owner_nm') }}</label>
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="owner_nm" name="owner_nm" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="owner_nm"
                                                    info1="{{Crypt::encrypt('sys_customers')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail")}}" 
                                                    info3="{{Crypt::encrypt("status='B' AND type='OTHER' and cust_type like('%CT004%') AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'  ")}}"
                                                    info5="{{Crypt::encrypt('goods')}}"
                                                    info4="cust_no=owner_cd;cname=owner_nm;receive_mail=owner_send_mail;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
									</div>
                                    <div class="form-group col-md-3">
										<label for="dlv_no_seq">{{ trans('mod_order.dlv_no_seq') }}</label>
										<input type="text" class="form-control" id="dlv_no_seq" name="dlv_no_seq" >
									</div>

                                    <div class="form-group col-md-3">
										<label for="floor">{{ trans('mod_order.floor') }}</label>
										<input type="text" class="form-control" id="floor" name="floor" >
									</div>
                                    <div class="form-group col-md-3">
										<input type="hidden" class="form-control" id="truck_cmp_no" name="truck_cmp_no">
										<label for="truck_cmp_nm">{{ trans('mod_order.truck_cmp_nm') }}</label>
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="truck_cmp_nm" name="truck_cmp_nm" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="truck_cmp_nm"
                                                    info1="{{Crypt::encrypt('mod_car_trader')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cust_name,cust_no,cust_name")}}" 
                                                    info3="{{Crypt::encrypt("")}}"
                                                    info5="{{Crypt::encrypt('mod_car_trader')}}"
                                                    info4="cust_no=truck_cmp_no;cust_name=truck_cmp_nm;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
									</div>
                                </div>

								<div class="row">
									<div class="form-group col-md-12">
										<label for="remark">{{ trans('mod_order.remark') }}</label>
										<textarea class="form-control" rows="3" name="remark" ></textarea>
									</div>
								</div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif

							</div>
						</div>

						<div id="tab_7" class="tab-pane">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-primary" id="statusDiv">
                                            <div class="box-header with-border">
                                            <h3 class="box-title">提貨資訊</h3>
                                                <div class="box-tools">
                                                    <button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class='box-body no-padding'>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <input type="text" class="form-control" id="pick_cust_no" style="display:none" name="pick_cust_no" >
                                                        <label for="pick_cust_nm">{{ trans('mod_order.pick_cust_nm') }}</label>
                                                        <div class="input-group input-group-sm">
                                                            <input type="text" class="form-control" id="pick_cust_nm" name="pick_cust_nm" required="required">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="pick_cust_nm"
                                                                    info1="{{Crypt::encrypt('sys_customers')}}" 
                                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,remark,receive_mail")}}" 
                                                                    info3="{{Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'")}}"
                                                                    info5="{{Crypt::encrypt('sys_customers')}}"
                                                                    info4="cust_no=pick_cust_no;cname=pick_cust_nm;address=pick_addr;contact=pick_attn;zip=pick_zip;city_nm=pick_city_nm;area_id=pick_area_id;area_nm=pick_area_nm;email=pick_email;phone=pick_tel;remark=pick_remark;receive_mail=pick_send_mail;" triggerfunc="" selectionmode="singlerow">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="pick_attn">{{ trans('mod_order.pick_attn') }}</label>
                                                        <input type="text" class="form-control" id="pick_attn" name="pick_attn" >
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="pick_tel">{{ trans('mod_order.pick_tel') }}</label>
                                                        <input type="text" class="form-control" id="pick_tel" name="pick_tel" >
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="pick_zip">{{ trans('mod_order.pick_zip') }}</label>
                                                        <div class="input-group input-group-sm">
                                                            <input type="text" class="form-control" id="pick_zip" name="pick_zip" >
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="pick_zip"
                                                                    info1="{{Crypt::encrypt('sys_area')}}" 
                                                                    info2="{{Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm")}}" 
                                                                    info3="{{Crypt::encrypt("")}}"
                                                                    info5="{{Crypt::encrypt('sys_customers')}}"
                                                                    info4="area_nm=pick_info;dist_cd=pick_zip;city_nm=pick_city_nm;dist_nm=pick_area_nm;" triggerfunc="" selectionmode="singlerow">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div class="row">
                                                    {{-- <div class="form-group col-md-3">
                                                        <label for="pick_city_nm">提貨城市區域名稱</label>
                                                        <input type="text" class="form-control" id="pick_info" name="pick_info"  switch="off" readonly="true">
                                                        <input type="hidden" class="form-control" id="pick_area_id" name="pick_area_id">
                                                        <input type="hidden" class="form-control" id="pick_city_nm" name="pick_city_nm">
                                                        <input type="hidden" class="form-control" id="pick_area_nm" name="pick_area_nm">
                                                        <input type="hidden" class="form-control" id="pick_addr_info" name="pick_addr_info">
                                                    </div> --}}
                                                    <div class="form-group col-md-6">
                                                        <label for="pick_addr">{{ trans('mod_order.pick_addr') }}</label>
                                                        <input type="text" class="form-control" id="pick_addr" name="pick_addr" >
                                                        <input type="hidden" class="form-control" id="pick_lat" name="pick_lat">
                                                        <input type="hidden" class="form-control" id="pick_lng" name="pick_lng">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box box-primary" id="statusDiv">
                                            <div class="box-header with-border">
                                            <h3 class="box-title">配送資訊</h3>
                                                <div class="box-tools">
                                                    <button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class='box-body no-padding'>
                                                <div class="row">
                                                    <div class="form-group col-md-3">
                                                        <input type="text" class="form-control" id="dlv_cust_no" style="display:none" name="dlv_cust_no" >
                                                        <label for="dlv_cust_nm">{{ trans('mod_order.dlv_cust_nm') }}</label>
                                                        <div class="input-group input-group-sm">
                                                            <input type="text" class="form-control" id="dlv_cust_nm" name="dlv_cust_nm" >
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="dlv_cust_nm"
                                                                    info1="{{Crypt::encrypt('sys_customers')}}" 
                                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,contact,address,zip,city_nm,area_id,area_nm,email,phone,remark,receive_mail")}}" 
                                                                    info3="{{Crypt::encrypt("status='B' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."'")}}"
                                                                    info5="{{Crypt::encrypt('sys_customers')}}"
                                                                    info4="cust_no=dlv_cust_no;cname=dlv_cust_nm;address=dlv_addr;contact=dlv_attn;zip=dlv_zip;city_nm=dlv_city_nm;area_id=dlv_area_id;area_nm=dlv_area_nm;email=dlv_email;phone=dlv_tel;remark=dlv_remark;receive_mail=dlv_send_mail;" triggerfunc="" selectionmode="singlerow">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="dlv_attn">{{ trans('mod_order.dlv_attn') }}</label>
                                                        <input type="text" class="form-control" id="dlv_attn" name="dlv_attn" >
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="dlv_tel">{{ trans('mod_order.dlv_tel') }}</label>
                                                        <input type="text" class="form-control" id="dlv_tel" name="dlv_tel" >
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="dlv_zip">{{ trans('mod_order.dlv_zip') }}</label>
                                                        <div class="input-group input-group-sm">
                                                            <input type="text" class="form-control" id="dlv_zip" name="dlv_zip" >
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="dlv_zip"
                                                                    info1="{{Crypt::encrypt('sys_area')}}" 
                                                                    info2="{{Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm")}}" 
                                                                    info3="{{Crypt::encrypt("")}}"
                                                                    info5="{{Crypt::encrypt('sys_customers')}}"
                                                                    info4="area_nm=dlv_info;dist_cd=dlv_zip;city_nm=dlv_city_nm;dist_nm=dlv_area_nm;" triggerfunc="" selectionmode="singlerow">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div class="row">
                                                    {{-- <div class="form-group col-md-3">
                                                        <label for="dlv_city_nm">配送城市區域名稱</label>
                                                        <input type="text" class="form-control" id="dlv_info" name="dlv_info" switch="off" readonly="true">
                                                        <input type="hidden" class="form-control" id="dlv_area_id" name="dlv_area_id">
                                                        <input type="hidden" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
                                                        <input type="hidden" class="form-control" id="dlv_area_nm" name="dlv_area_nm">
                                                        <input type="hidden" class="form-control" id="dlv_addr_info" name="dlv_addr_info">
                                                    </div> --}}
                                                    <!-- <div class="form-group col-md-3">
                                                        <input type="text" class="form-control" id="dlv_city_nm" name="dlv_city_nm">
                                                    </div> -->
                                                    <div class="form-group col-md-6">
                                                        <label for="dlv_addr">{{ trans('mod_order.dlv_addr') }}</label>
                                                        <input type="text" class="form-control" id="dlv_addr" name="dlv_addr" >
                                                        <input type="hidden" class="form-control" id="dlv_lat" name="dlv_lat">
                                                        <input type="hidden" class="form-control" id="dlv_lng" name="dlv_lng">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>

                        <div id="tab_5" class="tab-pane">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="box_num">{{ trans('mod_order.box_num') }}</label>
                                        <input type="text" class="form-control" id="box_num" name="box_num" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cbm">{{ trans('mod_order.cbm') }}</label>
                                        <input type="text" class="form-control" id="cbm" name="cbm" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="total_gw">{{ trans('mod_order.total_gw') }}</label>
                                        <input type="text" class="form-control" id="total_gw" name="total_gw" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="upload_start_time">開始上貨時間</label>
                                        <input type="text" class="form-control" id="upload_start_time" name="upload_start_time"  readonly="true" disabled>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="upload_finish_time">上貨完成時間</label>
                                        <input type="text" class="form-control" id="upload_finish_time" name="upload_finish_time"  readonly="true" disabled>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="arrive_time">到達時間</label>
                                        <input type="text" class="form-control" id="arrive_time" name="arrive_time"  readonly="true" disabled>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="dlv_date">下貨時間</label>
                                        <input type="text" class="form-control" id="unupload_time" name="unupload_time"  readonly="true" disabled>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="unupload_time">完成時間</label>
                                        <input type="text" class="form-control" id="finish_time" name="finish_time"  readonly="true" disabled>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="collectamt">代收金額</label>
                                        <input type="text" class="form-control" id="collectamt" name="collectamt" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cust_amt">上樓(層)</label>
                                        <input type="text" class="form-control" id="upstairsfloor" name="upstairsfloor" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cust_amt">上樓(件)</label>
                                        <input type="text" class="form-control" id="upstairsamt" name="upstairsamt" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cust_amt">下點(數)</label>
                                        <input type="text" class="form-control" id="downpointamt" name="downpointamt" >
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_amt">碳排</label>
                                        <input type="text" class="form-control" id="carbon" name="carbon" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="carbon_distance">{{ trans('mod_order.carbon_distance') }}</label>
                                        <input type="text" class="form-control" id="carbon_distance" name="carbon_distance" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="driver_remark">{{ trans('mod_order.driver_remark') }}</label>
                                        <input type="text" class="form-control" id="driver_remark" name="driver_remark" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="driver_remark">經度</label>
                                        <input type="text" class="form-control" id="arrive_lng" name="arrive_lng" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="driver_remark">緯度</label>
                                        <input type="text" class="form-control" id="arrive_lat" name="arrive_lat" >
                                    </div>
                                </div>
                            </div>
						</div>

					</div>
                </ul>
            </div>
        </form>
    </div> 
</div>

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">{{ trans('modOrder.orderDetail') }}</a></li>
                <li class=""><a href="#tab_8" data-toggle="tab" aria-expanded="false">圖片資訊</a></li>
                {{-- <li class=""><a href="#tab_12" data-toggle="tab" aria-expanded="false">棧板資訊</a></li> --}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_6">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ trans('modOrder.orderDetail') }}</h3>
                        </div>
                        <form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <!-- /.box-body -->
                            <div class="box-footer">
                                @if(isset($id))
                                    <input type="hidden" class="form-control input-sm noClear"  name="ord_id" value="{{$id}}" grid="true">
                                @endif
                                <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                            </div>
                        </form>
                    </div>
    
                    <div id="jqxGrid"></div>
                </div>
    
                <div class="tab-pane" id="tab_8">
                    <div class="box box-primary" id="subBox8" style="display:none">
                        <form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="guid">圖片</label>
                                                <input type="file" name="guid" id="guid"> 
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="status">敘述</label>
                                                <select class="form-control" id="type_no" name="type_no">
                                                    <option value="error">異常回報</option>
                                                    <option value="normal">一般回報</option>
                                                    <option value="upstairs">上樓</option>
                                                    <option value="downpoint">下點</option>
                                                    <option value="pickdlv">提配單</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                        <button type="button" class="btn btn-sm btn-primary" id="Save9">{{ trans('common.save') }}</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="Cancel9">{{ trans('common.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <div id="imgGrid2"></div>
                </div>

            </div>
            <!-- /.tab-content -->
        </div>
    </div> 
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "zh-TW";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder/batchDelete";

    $(function(){
	    $("#imgGrid2").on("rowdoubleclick", function(event){
            var selectedrowindex = $("#imgGrid2").jqxGrid('getselectedrowindex');
            var griddata =  $("#imgGrid2").jqxGrid('getrowdata', selectedrowindex);
            var boundindex =  griddata.boundindex;
            console.log(boundindex);
            var url = "{{env('PRINTER_URL')}}"+ griddata.guid.replace('public/', '');
            window.open(url);
        })
        var imagerenderer = function (row, datafield, value) {
            if(value != "") {
                console.log(BASE_URL.replace('/admin',''));
                return '<img style="margin-left: 15px;" height="60" width="50" src="'+"{{env('PRINTER_URL')}}"+ value + '"/>';
            }
            // if(value != "") {
            //     console.log(BASE_URL.replace('/admin',''));
            //     return '<img style="margin-left: 15px;" height="60" width="50" src="'+BASE_URL.replace('/admin','')+'/storage/uploadimg/' + value.replace('public/', '/zh-TW') + '"/>';
            // }
            return "";
        }
        $('#subBox button[btnName="goods_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goods_no', "料號建檔", callBackFunc=function(data){
                console.log('test auto complate');
                $("#subBox input[name='goods_nm']").val(data.goods_nm);
                $("#subBox input[name='goods_no']").val(data.goods_no);
                $("#subBox input[name='goods_no2']").val(data.goods_no2);
                $("#subBox input[name='goods_nm2']").val(data.goods_nm2);
                $("#subBox input[name='gwu']").val(data.gwu);
                $("#subBox input[name='cbm']").val(data.cbm);
                $("#subBox input[name='cbmu']").val(data.cbmu);
                $("#subBox input[name='length']").val(data['g_length']);
                $("#subBox input[name='weight']").val(data['g_width']);
                $("#subBox input[name='height']").val(data['g_height']);
                $("#subBox input[name='gw']").attr("gw", data.gw);
                $("#subBox input[name='pkg_unit']").val(data['pkg_unit']);
                var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;
                if(pkgNum > 0) {
                    $("#subBox input[name='gw']").val((data.gw * pkgNum).toFixed(2));
                }
                $("#subGw").text(data.gw);
            });
        });

        $('#myForm button[btnName="owner_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('owner_nm', "貨主查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="owner_nm"]').on('click', function(){
            var check = $('#subBox input[name="owner_nm"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","owner_nm",callBackFunc=function(data){
                    console.log(data);
                    
                },"owner_nm");
            }
        });

        $('#myForm button[btnName="pick_cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('pick_cust_nm', "客戶查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="pick_cust_nm"]').on('click', function(){
            var check = $('#subBox input[name="pick_cust_nm"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","pick_cust_nm",callBackFunc=function(data){
                    console.log(data);
                    
                },"pick_cust_nm");
            }
        });

        $('#myForm button[btnName="dlv_cust_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('dlv_cust_nm', "客戶查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="dlv_cust_nm"]').on('click', function(){
            var check = $('#subBox input[name="dlv_cust_nm"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","dlv_cust_nm",callBackFunc=function(data){
                    console.log(data);
                    
                },"dlv_cust_nm");
            }
        });

        $('#myForm button[btnName="pick_zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('pick_zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="pick_zip"]').on('click', function(){
            var check = $('#subBox input[name="pick_zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","pick_zip",callBackFunc=function(data){
                    console.log(data);
                    
                },"pick_zip");
            }
        });


        $('#myForm button[btnName="dlv_zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('dlv_zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="dlv_zip"]').on('click', function(){
            var check = $('#subBox input[name="dlv_zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","dlv_zip",callBackFunc=function(data){
                    console.log(data);
                    
                },"dlv_zip");
            }
        });

        $('#subFormpallet button[btnName="pallet_type"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('pallet_type', "棧板查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#subFormpallet input[name="pallet_type"]').on('click', function(){
            var check = $('#subBox input[name="pallet_type"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","pallet_type",callBackFunc=function(data){
                    console.log(data);
                    
                },"pallet_type");
            }
        });

        $('#myForm button[btnName="truck_cmp_nm"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('truck_cmp_nm', "卡車公司查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="truck_cmp_nm"]').on('click', function(){
            var check = $('#subBox input[name="truck_cmp_nm"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","truck_cmp_nm",callBackFunc=function(data){
                    console.log(data);
                    
                },"truck_cmp_nm");
            }
        });


        $('#subBox3 input[name="fee_name"]').on('click', function(){
            var selted = document.getElementById("prod_detail_id");
            var goodsval = selted.options[selted.selectedIndex].value;
            var owner_cd =$('#owner_cd').val();
            $.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
                if(data.msg=='success'){
                    $('button[btnName="fee_name"]').attr('info3', data.val);
                }
            })
            var check = $('#subBox input[name="fee_name"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("subForm3","fee_name",callBackFunc=function(data){
                    $("#fee_name").val(data.cd_descp);
                    $("#fee_cd").val(data.cd);
                },"fee_name");
            }
        });
        
        $('#subBox3 button[btnName="fee_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            var selted = document.getElementById("prod_detail_id");
            var goodsval = selted.options[selted.selectedIndex].value;
            var owner_cd =$('#owner_cd').val();
            $.get(BASE_URL+'/getnewfeelookup', {'detailid': goodsval,'ownercd': owner_cd}, function(data){
                if(data.msg=='success'){
                    $('button[btnName="fee_name"]').attr('info3', data.val);
                }
            })
            initLookup('fee_name', "其他服務項目建檔", callBackFunc=function(data){
                $("#subBox3 input[name='fee_cd']").val(data.cd);
                var code_cd = data.cd;
                var owner_cd =$('#owner_cd').val();
                var selted = document.getElementById("prod_detail_id");
                var goodsval = selted.options[selted.selectedIndex].value;
                $.get(BASE_URL+'/getnewfee', {'feecd': code_cd,'ownercd': owner_cd ,'goods_no': owner_cd}, function(data){
                    if(data.data!=null){
                        $("#subBox3 input[name='amount']").val(data.data.fee_amount);
                    }
                });

            });
        });

        $('#subBox input[name="goods_no"]').on('click', function(){
            var check = $('#subBox input[name="goods_no"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("subForm","goods_no",callBackFunc=function(data){
                    $("#subBox input[name='goods_nm']").val(data.goods_nm);
                    $("#subBox input[name='goods_no2']").val(data.goods_no2);
                    $("#subBox input[name='goods_nm2']").val(data.goods_nm2);
                    $("#subBox input[name='gwu']").val(data.gwu);
                    $("#subBox input[name='cbm']").val(data.cbm);
                    $("#subBox input[name='cbmu']").val(data.cbmu);
                    $("#subBox input[name='length']").val(data['g_length']);
                    $("#subBox input[name='weight']").val(data['g_width']);
                    $("#subBox input[name='height']").val(data['g_height']);
                    var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;
                    $("#subBox input[name='gw']").attr("gw", data.gw);
                    if(pkgNum > 0) {
                        $("#subBox input[name='gw']").val((data.gw * pkgNum).toFixed(2));
                    }
                    $("#subGw").text(data.gw);
                    
                },"goods_no");
            }
        });

        $("#subBox input[name='pkg_num']").on("change", function(){
            var gw = parseFloat($("#subBox input[name='gw']").attr("gw")) || 0;
            var pkgNum = parseInt($("#subBox input[name='pkg_num']").val()) || 0;

            $("#subBox input[name='gw']").val((gw * pkgNum).toFixed(2));
        });


        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/closeOrder" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#cust_type').select2();

            
        };
        formOpt.addFunc = function() {
            $('#jqxGrid').jqxGrid('clear');
				// $('#packGrid').jqxGrid('clear');
				$("#subPanel").hide();

				$("#sys_ord_no").val('');
				$("#ord_no").val('');

				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();

				if(dd<10) {
					dd = '0'+dd
				} 

				if(mm<10) {
					mm = '0'+mm
				} 

				today = yyyy+'-'+mm+'-'+dd;
				$("#etd").val(today);
				$("#status").val("UNTREATED");
				//$("#trs_mode").val("NON");
        }

        formOpt.copyFunc = function() {

        }
        formOpt.editFunc = function() {
            $("#subPanel").show();
            $("#jqxGrid").jqxGrid({'showtoolbar': false});
        }
        formOpt.afterInit = function(data) {
            $("#iAdd").hide();
            // $("#iEdit").hide();
            // $("#iSave").hide();
            $("#iCancel").hide();
            $("#iDel").hide();
            $('#type').val('OTHER');
        }
        
        formOpt.saveSuccessFunc = function(data) {
            $("#jqxGrid").jqxGrid({'showtoolbar': true});
            if(typeof data.dist !== "undefined" && data.dist > 0) {
                $("#distance").val(data.dist);
            }
        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"owner_cd",
                    "column_type":"string",
                },
                {
                    "column_filed":"owner_nm",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }

        formOpt.beforeDelFunc = function() {
            var status = $("#status").val();

            if(status != "UNTREATED") {
                alert('狀態不在「尚未安排」，故無法刪除');
                return false;
            }

            return true;
        }

        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);
        var btnGroup = [
            // {
            //     btnId: "btnShowImg",
            //     btnIcon: "fa fa-picture-o",
            //     btnText: "簽收照片",
            //     btnFunc: function () {
            //         window.open("{{ url(config('backpack.base.route_prefix', '/zh-TW').'/show') }}" + "/" + $("#sys_ord_no").val() + '/FINISH', '簽收照片', config='height=500,width=600');
            //     }
            // },
            // {
            //     btnId: "btnShowErrorImg",
            //     btnIcon: "fa fa-picture-o",
            //     btnText: "異常照片",
            //     btnFunc: function () {
            //         window.open("{{ url(config('backpack.base.route_prefix', '/zh-TW').'/show') }}" + "/" + $("#sys_ord_no").val() + '/ERROR', '異常照片', config='height=500,width=600');
            //     }
            // }
            // {
            //     btnId: "btnShowErrorImg",
            //     btnIcon: "fa fa-clone",
            //     btnText: "取得明細",
            //     btnFunc: function () {
            //         $.post(BASE_URL + '/' + lang + '/' + 'order' + '/getdetailinfo', {'id': mainId}, function(data){
            //             if (data.success) {
            //                 swal('成功', "", "success");
            //                 $("#jqxGrid").jqxGrid('updatebounddata');
            //                 // $("#jqxGrid").jqxGrid('clearselection');
            //             } else {
   
            //             }
            //         });
            //     }
            // },
            // {
            //     btnId: "btnurltoprocess",
            //     btnIcon: "fa fa-clone",
            //     btnText: "異常處理畫面",
            //     btnFunc: function () {
            //         $.post(BASE_URL + '/' + lang + '/' + 'order' + '/urltoprocess', {'id': mainId}, function(data) {
            //             location.href = BASE_URL+"/"+lang+"/errorprocessinit/"+data.initno;
            //         });
            //     }
            // }
        ];
        initBtn(btnGroup);

        //
        // id, ord_id, ord_no, wms_ord_no, sn_no, sku_no, sku_desc, sku_spec, quantity,
        // unit, transfer_mq, batch_no, expiry_date, 
        if(mainId != "") {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "ord_no", type: "string"},
                    {name: "wms_ord_no", type: "string"},
                    {name: "sn_no", type: "string"},
                    {name: "sku_no", type: "string"},
                    {name: "sku_desc", type: "string"},
                    {name: "sku_spec", type: "string"},
                    {name: "quantity", type: "string"},
                    {name: "unit", type: "string"},
                    {name: "transfer_mq", type: "string"},
                    {name: "batch_no", type: "string"},
                    {name: "expiry_date", type: "string"},
                ],
                [
                    {text: "{{ trans('modOrderDetail.id') }}", datafield: "id", width: 100, hidden: true},
                    {text: "訂單編號", datafield: "ord_no", width: 130, nullable: false,hidden:true},
                    {text: "訂單編號", datafield: "wms_ord_no", width: 150, nullable: false},
                    {text: "序號", datafield: "sn_no", width: 150, nullable: false},
                    {text: "品號", datafield: "sku_no", width: 150, nullable: false},
                    {text: "品名", datafield: "sku_desc", width: 100, nullable: false},
                    {text: "規格", datafield: "sku_spec", width: 100, nullable: false},
                    {text: "數量", datafield: "quantity", width: 100, nullable: false},
                    {text: "單位", datafield: "unit", width: 100, nullable: false},
                    {text: "箱量單位", datafield: "transfer_mq", width: 100, nullable: false},
                    {text: "批號", datafield: "batch_no", width: 100, nullable: false},
                    {text: "效期", datafield: "expiry_date", width: 100, nullable: false},
                ]
            ];
                
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/OrderMgmt/detailget') }}" + '/' + mainId;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/OrderMgmt') }}" + "/detailstore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/OrderMgmt') }}" + "/detailupdate";
            opt.delUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/OrderMgmt') }}" + "/detaildelete/";
            opt.commonBtn = true;
            @can('OrdControl')
                opt.showtoolbar = true;
            @endcan
            opt.defaultKey = {
                'ord_id': mainId
            };
            opt.beforeSave = function (row) {
                row.sn_no  = row.sn_no.replaceAll("，", ",");
                $("#sn_no").val(row.sn_no);
                @cannot('OrdControl')
                swal("沒有修改權限", "", "warning");
                return false;
                @endcan
                if(row.pkg_num == "" || row.pkg_num == null) {
                    swal("警告", "數量不能為空", "warning");
                    return false;
                }

                if(row.goods_nm == "" || row.goods_nm == null) {
                    swal("警告", "商品名稱不能為空", "warning");
                    return false;
                }
                return true;
            }

            opt.afterSave = function (data) {
                var sumPkgNum = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'pkg_num', ['sum']);
                var sumGw = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'gw', ['sum']);
                var sumCbm = $("#jqxGrid").jqxGrid('getcolumnaggregateddata', 'cbm', ['sum']);
                var sumprice = data.total_amt;
                $("#pkg_num").val(sumPkgNum.sum);
                $("#total_gw").val(sumGw.sum);
                $("#total_cbm").val(sumCbm.sum);
                $("#amt").val(sumprice);
                $("#subGw").text("");
            }

            opt.beforeCancel = function() {
                $("#subGw").text("");
            }

            genDetailGrid(opt);
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "guid", type: "string"},
                    {name: "type_no", type: "string"},
                    {name: "descp", type: "string"},
                    {name: "created_at", type: "string"}
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true },
                    {text: "圖片", datafield: "guid", width: 150, cellsrenderer: imagerenderer},
                    { 
                        text: '路徑', 
                        width: 100,
                        datafield: 'imgurl', 
                        columntype: 'button', 
                        cellsrenderer: function (row, column, value) {
                        return "點我打開圖片";
                        // return "按鈕";
                    }, buttonclick: function (row,event) {
                        var rowdata = $("#imgGrid2").jqxGrid('getrowdata', row);
                        var url ="{{env('PRINTER_URL')}}"+ rowdata.guid.replace('public/', '');
                        window.open(url);
                    }},
                    {text: "敘述", datafield: "descp", width: 300},
                    {text: "上傳時間", datafield: "created_at", width: 300}
                ]
            ];
            
            var opt = {};
            opt.gridId = "imgGrid2";
            opt.fieldData = col;
            opt.formId = "subForm8";
            opt.saveId = "Save9";
            opt.cancelId = "Cancel9";
            opt.showBoxId = "subBox8";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/closeOrder/imgget') }}" + '/' + mainId ;
            opt.delUrl  =   "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/closeOrder') }}" + "/imgdelete/";
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/closeOrder') }}" + "/imgstore";
            opt.defaultKey = {
                'ref_no': mainId
            };
            opt.commonBtn = false;
            opt.showtoolbar = false;

            opt.beforeSave = function (row) {
            }
            opt.afterSave = function (data) {
                $('#imgGrid2').jqxGrid('updatebounddata');
            }
            opt.beforeCancel = function() {
            }

            genDetailGrid(opt);


            // var col = [
            //     [
            //         {name: "id", type: "number"},
            //         {name: "ord_id", type: "string"},
            //         {name: "pallet_num", type: "string"},
            //         {name: "pallet_code", type: "string"},
            //         {name: "pallet_type", type: "string"},
            //     ],
            //     [
            //         {text: "{{ trans('modOrderDetail.id') }}", datafield: "id", width: 100, hidden: true},
            //         {text: "訂單id", datafield: "ord_id", width: 100, hidden: true},
            //         {text: "棧板類型", datafield: "pallet_type", width: 150, nullable: false},
            //         {text: "棧板類型代碼", datafield: "pallet_code", width: 150, nullable: false,hidden:true},
            //         {text: "棧板數量", datafield: "pallet_num", width: 130, nullable: false},
            //     ]
            // ];
                
            // var opt             = {};
            // opt.gridId      = "jqxGrid_pallet";
            // opt.fieldData   = col;
            // opt.formId      = "subFormpallet";
            // opt.saveId      = "Savepallet";
            // opt.cancelId    = "Cancelpallet";
            // opt.showBoxId   = "subBoxpallet";
            // opt.height      = 300;
            // opt.getUrl      = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/closeOrder/palletget') }}" + '/' + mainId;
            // opt.addUrl      = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/closeOrder') }}" + "/palletstore";
            // opt.updateUrl   = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/closeOrder') }}" + "/palletupdate";
            // opt.delUrl      = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/closeOrder') }}" + "/palletdelete/";
            // opt.commonBtn   = true;
            // opt.showtoolbar = true;
            // opt.defaultKey  = {
            //     'ord_id': mainId
            // };
            // opt.beforeSave = function (row) {
            //     var palletCode = $("#pallet_code").val();
            //     var palletNum = $("#pallet_num").val();
            //     if(palletCode == "" || palletCode == null) {
            //         swal("{{trans('common.warning')}}", "請輸入棧板類型", "warning");
            //         return false;
            //     }
            //     if(palletNum == "" || palletNum == null) {
            //         swal("{{trans('common.warning')}}", "請輸入棧板數量", "warning");
            //         return false;
            //     }

            //     return true;
            // }

            // opt.afterSave = function (data) {

            // }

            // opt.beforeCancel = function() {
                
            // }
            
            // genDetailGrid(opt);

        }

})
</script> 
<script>
$(function(){
});
</script>
@endsection