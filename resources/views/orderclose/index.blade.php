@extends('layout.layout')
@section('header')
<section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
    <h1>
        結案作業
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">結案作業</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />
    <input type="hidden" value="{{$lang}}" id="lang" />
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
<div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
            <div class="col-xs-12">
                使用包含(多值)時請用「,」隔開 (用紅色的文字)
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('before_scripts')
<script>
    var lang = "{{$lang}}";
    var languagename = "";
    var tableName = 'mod_order_close';
    var fileName = 'closeOrder';
    var enabledStatus = 'Y';
    var btnGroupList = [];
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                $('#searchWindow').jqxWindow('open');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var pageNum = 1;
                var pageSize = 50;
                var anotherCondition = typeof sqlcondition !== 'undefined' ? sqlcondition : [];
                var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                var filtercolumn = [];

                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                if($('#jqxGrid').jqxGrid('getrows').length == 0) {
                    swal("當前畫面沒有資料", "", "warning");
                    return;
                }
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    pageNum = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                    pageSize = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagesize;
                    for (var i = 0; i < $('#jqxGrid').jqxGrid('getrows').length; i++) {
                        var row = $("#jqxGrid").jqxGrid('getrowdata', i);
                        if(typeof row != "undefined" && row.id!='') {
                            ids.push(row.id);
                        }
                    }
                } else {
                    filtercolumn[filtercolumn.length] = ['id', `in`, ids];
                }
                var sortby = [];
                var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
                if(sortinformation.sortcolumn != null ) {
                    var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                    var obj = {};
                    obj["sortField"] = sortinformation.sortcolumn;
                    obj["sortValue"] = sortvalue;
                    sortby.push(obj);
                    // sortby.push([ sortinformation.sortcolumn,  tpye]);
                }
                $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                    'table'           : 'mod_order_close',
                    'fileName'        : '結案作業',
                    'baseCondition'   : filtercolumn,
                    'anotherCondition': anotherCondition,
                    'pageNum'         : pageNum,
                    'pageSize'        : pageSize,
                    'sort'            : sortby,
                    'header'          : enabledheader,
                }, function(data){

                    if(data.message == "success") {
                        window.open(data.donwloadLink);
                    }

                });
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary" id="statusDiv">
			<div class="box-header with-border">
                勾選筆數：<span id="selectcount"></span>&nbsp;&nbsp;&nbsp; <h3 class="box-title">Status </h3><strong style="color:red;">(狀態「配送完成」及「ALL」的按鈕需在下方輸入至少一個查詢條件後才可顯示資料)</strong>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body no-padding">
                <p></p>
				<div style="width:100%;" id="statusList">
                    
                </div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-12">
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="button-group">
					<div class="row" id="btnArea">

					</div>
					<div id="jqxGrid"></div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>
<script >
    $("#jqxGrid").on("rowselect", function (event) {
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        $("#selectcount").text(rows.length);
    });
    $('#jqxGrid').on('rowunselect', function (event){
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        $("#selectcount").text(rows.length);
    });


</script>
@endsection