{{-- @extends('backpack::layout') @section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-title">{{ trans('backpack::base.login') }}</div>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url(config('backpack.base.route_prefix').'/login') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">{{ trans('backpack::base.email_address') }}</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}"> @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">{{ trans('backpack::base.password') }}</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password"> @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Company</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="c_key">


                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> {{ trans('backpack::base.remember_me') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                {{ trans('backpack::base.login') }}
                            </button>

                            <a class="btn btn-link" href="{{ url(config('backpack.base.route_prefix', 'admin').'/password/reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection --}}


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/iCheck/square/blue.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page" style="height:100vh;background-image: url('../img/login_bg.jpg');background-repeat: no-repeat;background-size:cover;overflow:hidden;background-position:center center;">
    <style>
        .login-box-body {
            /* Safari 3-4, iOS 1-3.2, Android 1.6- */
            -webkit-border-radius: 20px;
            /* Firefox 1-3.6 */
            -moz-border-radius: 20px;
            /* Opera 10.5, IE 9, Safari 5, Chrome, Firefox 4, iOS 4, Android 2.1+ */
            border-radius: 20px;
        }
    </style>
    <div class="login-box" style="margin: 5% auto">

        <!-- /.login-logo -->
        <div class="login-box-body" style="
    background-color: rgba(255, 255, 255, 0);
    border: 1px solid rgba(102, 102, 102, 0.26);">
            <div class="login-logo">
                <a href="./" style="color:#000; font-size:45px">
                    <b>DSV</b>- DSS</a>
            </div>


            <form class="form-horizontal" id ="myForm" role="form" method="POST" onsubmit="return checkexpire()" action="{{ url(config('backpack.base.route_prefix').'/login') }}">
                {!! csrf_field() !!}
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" id ="email" class="form-control" placeholder="Account" name="email" name="M1" onKeyDown = "return next(event,'password');">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>The account field is required.</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" id ="ogpassword" placeholder="Password" name="password" onKeyDown = "return next(event,'c_key');">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                    <span class="help-block">
                   
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                {{-- <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Company" name="c_key" onKeyDown = "return next(event,'signin');">
                    <span class="fa fa-building-o form-control-feedback"></span>
                </div> --}}
                <div class="form-group has-feedback" style="display:none">
                    <select class="form-control" name="lang" id="mutiLang">
                        @foreach (Config::get('app.locales') as $lang => $language)
                            <option value="{{$lang}}">{{$language}}</option>
                        @endforeach
                    </select>
                    <span class="fa fa-language form-control-feedback"></span>
                </div>
                @if ($loginStatus)
                        
                <div class="form-group has-feedback">
                    <span style="color:red">
                        {{ $loginStatus }}
                    </span>
                </div>
                @endif
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" name="signin" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <div class="social-auth-links text-center"></div>
            <!-- /.social-auth-links -->

            {{--  <a href="#" style="color: #000">I forgot my password</a>  --}}
            <p>版本：{{Config::get('app.version')}}</p>
            <br />
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 3 -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('vendor/adminlte') }}/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('vendor/adminlte') }}/plugins/iCheck/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.all.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
            var lang = "zh-TW";
            switch (lang) {
                case 'zh-CN' :                        
                        $('#mutiLang option[value=zh-CN]').prop('selected', true);
                        break;
                case 'zh-TW' :
                        $('#mutiLang option[value=zh-TW]').prop('selected', true);
                        break;
                case 'en' :
                        $('#mutiLang option[value=en]').prop('selected', true);
                        break;
                default:
                        $('#mutiLang option[value=zh-TW]').prop('selected', true);
                        break;
            }
            //alert(lang);





        });
    </script>
    <script >
        function next(evt,t) { 
        var e = evt || window.event; 
        　if (evt.keyCode == 9 || evt.keyCode == 13) {  
        　　document.forms[0].elements[t].focus(); 
        　　return false; 
        　} 
        } 
    </script>
</body>
<script >
    var canlogin = '1';
    function checkexpire() { 
        var email = document.getElementById('email').value;
        var BASE_URL = "{{ url(config('backpack.base.route_prefix', '')) }}";

        //
        $.ajax({
            type: "POST",
            async: false,
            url: BASE_URL +"/checkexpire",
            data: {
                "_token": "{{csrf_token()}}",
                email:email,
            },
            
            success: function(data) {
                console.log('checkexpire 123');
                if(data.data) {
                    (async function getFormValues () {
                        var optinon = "";
                        var first = "";
                        const {value: formValues} = await swal({
                            title: '請修改密碼',
                            html:
                                '<input id="password" type="password" class="swal2-input" placeholder="請輸入原密碼">'+
                                '<input id="newpassword" type="password" class="swal2-input" placeholder="請輸入新密碼">'+
                                '<input id="newpasswordconfirm" type="password" class="swal2-input" placeholder="請確認新密碼">',
                            focusConfirm: false,
                            focusConfirm: false,
                            allowOutsideClick: false,
                            showCancelButton: true,
                            cancelButtonText:  '取消',
                            confirmButtonText: '確定',
                            preConfirm: () => {
                                return [
                                    document.getElementById('password').value,
                                    document.getElementById('newpassword').value,
                                    document.getElementById('newpasswordconfirm').value
                                ]
                            }
                        })
                        if (formValues) {
                            var password = document.getElementById('password').value;
                            var newpassword = document.getElementById('newpassword').value;
                            var newpasswordconfirm = document.getElementById('newpasswordconfirm').value;
                            var regex = new RegExp('^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#%&]).{8,15}');
                            if( newpassword != newpasswordconfirm) {
                                swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
                                return false;
                            }

                            if(!regex.test(newpassword)) {
                                swal("{{trans('common.warning')}}", "密碼請包含大小寫英文字母特殊符號數字 長度超過8碼", "warning");
                                return false;
                            }

                            $.post(BASE_URL +"/userupdatepassword", {
                                "_token": "{{csrf_token()}}",
                                email:email,
                                password: password,
                                newpassword: newpassword,
                                newpasswordconfirm: newpasswordconfirm
                            }, function(data){
                                if(data.success) {
                                    swal("完成", "", "success");
                                } else {
                                    swal("{{trans('common.warning')}}", data.message, "warning");
                                }
                                return false;
                            });

                        }
                    })()
                    return false;
                } else {
                    console.log('return true');
                    canlogin = "2";
                    return false;
                }
                return false;
            }
        });
        //
    }
    $('#myForm').submit(function() {
        console.log(canlogin);
        if(canlogin == "1") {
            return false;
        } else {
            return true;
        }
        

    });
</script>
</html>
