@extends('layout.layout')
@section('header')
<section class="content-header">
    <h1>
        警衛室車輛進出管理
        <small></small>
    </h1>
</section>

@endsection
@section('before_scripts')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="配送服務系統">
<!-- Meta Keyword -->
<meta name="keywords" content="DSS">
<meta name="author" content="">
<!-- CSS Global Compulsory -->
<!-- CSS Implementing Plugins -->

<!-- CSS Customization -->
<link rel="stylesheet" href="https://dsstms.standard-info.com/fordriver/assets/css/main.css">


@endsection

@section('content')

<div class="py-2 px-3">
    <div class="security-box">
        <div class="row">
          <div class="col-lg-4">
            <h3>可離場車輛</h3>
            <div class="border-box orange">
              <ul class="list-unstyled between-list" id="cango-box">
                @foreach($viewData['cango'] as $row2)
                    <li class="w-100" id="{{$row2->car_no}}">
                      <h2 class="inpage-title mb-0">{{$row2->car_no}}{{$row2->discharged_status}}</h2>
                      <button type="button" class="best-btn py-1 px-4" id="{{$row2->car_no}}-btn">放行</button>
                    </li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="d-flex justify-content-between">
              <h3>場內車輛</h3>
              <div class="d-flex">
                <div class="legend can-go">
                  <div class="color-box"></div>
                  <h4>可離場</h4>
                </div>
                <div class="legend">
                  <div class="color-box"></div>
                  <h4>不可離場</h4>
                </div>
              </div>
            </div>
            <div class="border-box">
              <div class="row" id="inside-box">

                @foreach($viewData['insidecar'] as $row)
                    <div class="col-lg-3" id="inside-{{$row->car_no}}-btn">
                        <button type="button" class="car-btn">{{ $row->car_no}}</button>
                    </div>
                @endforeach

                {{-- <div class="col-lg-3">
                  <button type="button" class="car-btn can-go">KNB0857</button>
                </div>
                <div class="col-lg-3">
                  <button type="button" class="car-btn">KNB0857</button>
                </div>
                <div class="col-lg-3">
                  <button type="button" class="car-btn">KNB0857</button>
                </div>
                <div class="col-lg-3">
                  <button type="button" class="car-btn">KNB0857</button>
                </div>
                <div class="col-lg-3">
                  <button type="button" class="car-btn can-go">KNB0857</button>
                </div> --}}

              </div>
            </div>
          </div>
        </div>
      </div>
</div>



@endsection

@section('after_scripts')
<script>
    console.log('{{$viewData["checkWh"]}}');
    // 使用者可看見的倉庫
    const checkWh = ('{{$viewData["checkWh"]}}').split(',');
    console.log(checkWh);
    /* websocket */
    securityInitWebsocket();
    function securityInitWebsocket () {
        let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/groupId_DsvdevTmsCheckCarNoByWebsocket';
        let ws = new WebSocket(wsURL); // 建立連線
        ws.onopen = function(e) {
            // websocketonopen(e);
            // console.log('ws 連線成功~~');
        };
        ws.error = function(error) {
            // websocketonerror(error);
            // console.error('ws 連線失敗', error);
        };
        ws.onmessage = function(e) {
            securityWebsocketonmessage(e);
        };
        ws.onclose = function(e) {
            setTimeout(function() {
                securityInitWebsocket();
            }, 1000);
            if (e.wasClean) {
                websocketclose(e);
            } else {
                console.log(e.code);
            }
        };
    }
    // function websocketonopen (e) {
    //     console.log('ws 連線成功~~');
    // }
    // function websocketonerror (error) {
    //     console.error('ws 連線失敗', error);
    // }
    function securityWebsocketonmessage (e) {
        // 後端通知前端，前端取得資料
        // console.log(e);
        let _data = e.data;
        console.log('ws 取得資料', _data);
        if (_data.indexOf('DsvTmsCheckCarNoByWebsocket') === -1) {
          let dataObj = JSON.parse('[' + _data.split('=').join('":"').split(', ').join('","').split('{').join('{"').split('}').join('"}') + ']');
          console.log(dataObj);
          let canSee = false;
          checkWh.forEach(item => {
            if (item.indexOf(dataObj[0].dc_id) !== -1) {
              canSee = true
            }
          });
          if (canSee) {
            updateSecurityData(dataObj[0]);
          }
        }
    }
    function securityWebsocketsend (data) {
        // 前端丟資料
        console.log('send data', data);
    }
    function securityWebsocketclose () {
        console.log('ws 關閉連線');
    }
    // 更新車輛
    function updateSecurityData (data) {
      switch (data.can_go) {
        case 'Y':
          document.getElementById(`inside-${data.car_no}-btn`).remove();
          let newLi = document.createElement('li');
          newLi.classList.add = 'w-100';
          newLi.id = data.car_no;
          newLi.innerHTML = `
            <h2 class="inpage-title mb-0">${data.car_no}${data.discharged_status}</h2>
            <button type="button" class="best-btn py-1 px-4" id="${data.car_no}-btn">放行</button>`;
          document.getElementById('cango-box').appendChild(newLi);
          setCanGoBtnListenerEvent(data.car_no);
          break;
        case 'N':
          let newDiv = document.createElement('div');
          newDiv.classList.add = 'col-lg-3';
          newDiv.id = `inside-${data.car_no}-btn`;
          newDiv.innerHTML = `<button type="button" class="car-btn">${data.car_no}</button>`;
          document.getElementById('inside-box').appendChild(newDiv);
          break;
      }
    }
    // 設定放行按鈕監聽
    function setCanGoBtnListenerEvent (carNo) {
      document.getElementById(carNo + '-btn').addEventListener('click', function (e) {
        swal({
              title: '車號' + carNo,
              text: "確定要放行嗎？",
              showCancelButton: true,
              confirmButtonColor: '#dc3545',
              cancelButtonColor: '#6c757d',
              confirmButtonText: "確定",
              cancelButtonText: "取消"
          }).then((result) => {
            if (result.value) {
            
              $.ajax({
                  type: "post", //  OR POST WHATEVER...
                  dataType : 'json', // 預期從server接收的資料型態
                  contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
                  url: BASE_URL + '/carleave',
                  data:JSON.stringify({
                      "car_no"             : carNo,
                      "_token"             : "{{csrf_token()}}",
                  }),
                  success: function(data) {
                    console.log(data);
                    document.getElementById(carNo).remove();
                  }
              });
            }
        });
      })
    }
</script>
<script>
  // 第一次載入畫面監聽放行按鈕
  @for ($i = 0; $i < count($viewData['cango']); $i++) {
    setCanGoBtnListenerEvent('{{$viewData["cango"][$i]->car_no}}');
  }
  @endfor
</script>
@endsection