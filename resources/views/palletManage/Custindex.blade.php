@extends('layout.layout')
@section('header')
<section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
    <h1>
        {{ trans('mod_pallet_manage.titleName') }}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('mod_pallet_manage.titleName') }}</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />
    <input type="hidden" value="{{$datafields2}}" id="datafields2" />
    <input type="hidden" value="{{$columns2}}" id="columns2" />
    <input type="hidden" value="{{$lang}}" id="lang" />
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="searchWindow2" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody2">
        <div id="searchContent2">

        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter2">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName2"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch2">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault2">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch2">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn2">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd2">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('before_scripts')
<script>
    var lang = "{{$lang}}";
    var languagename = "";
    var enabledheaderdetail = [];
    var tableName = 'mod_pallet_manage_by_cust';
    var fileName = 'palletManageCust';
    var btnGroupList = [];
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                if(focus=="1") {
                    $('#searchWindow').jqxWindow('open');
                }else{
                    $('#searchWindow2').jqxWindow('open');
                }
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                if(focus=="1"){
                    $('#gridOptModal').modal('show');
                }else{
                    $('#gridOptModal2').modal('show');
                }
               
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                filtercolumndata = [];
                var filtercolumn = [];

                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    for (var i = 0; i < filterGroups.length; i++) {
                        var filterGroup = filterGroups[i];
                        var filters = filterGroup.filter.getfilters();
                            for (var k = 0; k < filters.length; k++) {
                                if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    // new Date(unix_timestamp * 1000)
                                    filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                } else if(filters[k].condition == "contains" ) {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                } else {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                }
                                // filtercolumndata.push(filtercolumn);
                        }
                    }
                } else {
                    filtercolumn[m] = ['id', `in`, ids];
                }
                filtercolumn.push(['type_cd', `=`, "cust"]);
                var sortby = [];
                var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
                if(sortinformation.sortcolumn != null ) {
                    var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                    var obj = {};
                    obj["sortField"] = sortinformation.sortcolumn;
                    obj["sortValue"] = sortvalue;
                    sortby.push(obj);
                    // sortby.push([ sortinformation.sortcolumn,  tpye]);
                }
                $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                    'table': 'mod_pallet_manage_by_cust',
                    'fileName':'棧板管理' ,
                    'baseCondition': filtercolumn,
                    'sort': sortby,
                    'header': enabledheader,
                }, function(data){

                    if(data.message == "success") {
                        window.open(data.donwloadLink);
                    }

                });
            }
        },
        @can("PALLETMANAGECUST-CREATE")
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: btnAdd,
            btnFunc: function() {
                location.href =  BASE_URL + '/' + lang + '/' + fileName + '/create';
            }
        },
        @endcan
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row" id="mainForm">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li name="prodDetail" class="active">
                <a onclick="tab_1()" href="#tab_1" data-toggle="tab" aria-expanded="false">總覽</a>
            </li>
            <li name="prodGift">
                <a onclick="tab_2()" href="#tab_2" data-toggle="tab" aria-expanded="false">明細</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="griddiv">
                        <div id="jqxGrid"></div>
                        <div id="jqxGrid_2"></div>
                    </div>
                    <input type="button" style="display:none" id="updategridsign" >
                    <input type="button" style="display:none" id="updategriderror" >
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid2">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid2">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>

<script>
    $(document).ready(function() {
        // 在這撰寫javascript程式碼
        $("[href='#tab_1']").click();   
    });
    function tab_1() {
        focus = "1";
        $("#btnAdd").hide();
        $("#btnDelete").show();
        $("#btnTakedown").hide();
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid_2').style.display = 'none';
        // 
    }
    function tab_2() {

        focus = "2";
        $("#btnAdd").show();
        $('#jqxGrid_2').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_2"></div>')


        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'block';
        ///


        var sourcedetail = {
            datatype: "json",
            datafields: JSON.parse($("#datafields2").val()),
            root: "Rows",
            pagenum: 0,
            beforeprocessing: function(data) {
                sourcedetail.totalrecords = data[0].TotalRows;
                if (data[0].StatusCount.length > 0) {
                    data[0].StatusCount.push({
                        'count': data[0].TotalRows,
                        'status': 'ALL',
                        'statustext': 'ALL'
                    });
                }
            },
            filter: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'sort');
            },
            cache: false,
            pagesize: 50,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage_by_cust_detail_veiw/false') }}"+"?basecon=type_cd;EQUAL;cust;"
        }

        var dataAdapterdetail = new $.jqx.dataAdapter(sourcedetail, {
            async: false,
            loadError: function(xhr, status, error) {
                alert('Error loading "' + sourcedetail.url + '" : ' + error);
            },
            loadComplete: function() {

            }
        });

        var winHeight2 = $(window).height() - 350;
        $("#jqxGrid_2").jqxGrid({
            width: '100%',
            height: winHeight2,
            source: dataAdapterdetail,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 2000],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
                loadState2();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: JSON.parse($("#columns2").val())
        });


        ///
        var mainids = new Array();
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ischeck = "N";
        var ownerName = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                mainids.push(row.cust_name);
                ischeck = "Y";
            }
        }
        var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
        if ( ischeck == "N") {
            if(filterGroups.length > 0 ) {
                var allrows = $('#jqxGrid').jqxGrid('getrows');
                for(var i = 0; i < allrows.length; i++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', i);
                    mainids.push(row.cust_name);
                }
                sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage_by_cust_detail_veiw/false') }}"+"?basecon=type_cd;EQUAL;cust; )*(cust_name;IN;"+JSON.stringify(mainids); 
            } else {
                sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage_by_cust_detail_veiw/false') }}"; 
            }

        } else {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage_by_cust_detail_veiw/false') }}"+"?basecon=type_cd;EQUAL;cust; )*(cust_name;IN;"+JSON.stringify(mainids); 
        }
        $("#jqxGrid_2").jqxGrid('updatebounddata');


        function loadState2() {
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: 'mod_pallet_manage_by_cust_detail_veiw',
                    lang: 'zh-TW'
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid_2" ).jqxGrid('loadstate', response);
                    }

                    var listSource2 = [];
                    state2 = $("#jqxGrid_2").jqxGrid('getstate');
                    console.log(state2);
                    $('#jqxLoader').jqxLoader('close');

                    $.get(BASE_URL + "/searchList/get/" + "mod_pallet_manage_by_cust_detail_veiw", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName2']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName2']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" +"mod_pallet_manage_by_cust_detail_veiw/"+ $("select[name='selSearchName2']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent2").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state2);
                                        initSearchWindow2(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow2').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 200,
                                        minWidth: 200,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow2').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd2']").on("click", function() {
                                        var seasrchTpl = getSearchTpl2(state2);
                                        $("#searchContent2").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove2']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj2[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent2[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn2']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField2[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent2[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp2[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter2(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state2.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource2.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheaderdetail.push(headerdata);
                            }
                        }
                    });
                    console.log(listSource2);
                    $("#jqxlistbox2").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource2,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });
        }
        function initSearchWindow2(searchTpl) {
            $("#searchContent2").append(searchTpl);
        }
        $("#saveGrid2").on("click", function() {
            var items = $("#jqxlistbox2").jqxListBox('getItems');
            var gridname = "jqxGrid_2";
            var savekey = "mod_pallet_manage_by_cust_detail_veiw";
            // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
            enabledheaderdetail    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                }
                if (item.checked) {
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheaderdetail.push(headerdata);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'mod_pallet_manage_by_cust_detail_veiw',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        // alert("save successful");
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid2").on("click", function() { 
            savekey = "mod_pallet_manage_by_cust_detail_veiw";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });

        $("#jqxGrid_2").on("rowdoubleclick", function(event) {
            // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick;
            // original event.
            var ev = args.originalEvent;
            if(typeof toedit !== "undefined" && toedit == "false") {
                return;
            }
            var datarow = $("#jqxGrid_2").jqxGrid('getrowdata', boundIndex);
            console.log(datarow);

            // status
            var editUrl = BASE_URL  + "/" +lang +"/"+'palletManageCustdetailedit'+"/" + datarow.id ;
            //location.href = editUrl;
            window.open(editUrl);
        })

    }
</script>
@endsection