@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_pallet_manage.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletManage') }}">{{ trans('mod_pallet_manage.titleName') }}</a></li>
		<li class="active">{{ trans('mod_pallet_manage.titleName') }}</li>
      </ol>
    </section>
    <div id="searchWindow" style="overflow-x:hidden; display:none">
        <div>{{ trans('common.search') }}</div>
        <div id="searchBody">
            <div id="searchContent">
    
            </div>
            <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
                <div class="col-xs-3">
                    <select class="form-control input-sm" name="selSearchName"></select>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
                </div>
                <div class="col-xs-3">
                    <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                    <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                    <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
                </div>
                <div class="col-xs-3" style="align: right">
                    <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                    <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Grid Option</h4>
                </div>
                <div class="modal-body">
                    <div id="jqxlistbox"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                    <button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
                    <button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.wh_name') }} <span style="color:red">*<span></label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="wh_no" name="wh_no" required="required">
                                                <input type="text" class="form-control" id="wh_name" name="wh_name" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="wh_name"
                                                        info1="{{Crypt::encrypt('mod_warehouse')}}" 
                                                        info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname")}}" 
                                                        info3="{{Crypt::encrypt("dc_id is not null ")}}"
                                                        info5="{{Crypt::encrypt('mod_warehouse')}}"
                                                        info4="cust_no=wh_no;cname=wh_name;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.pallet_type') }} <span style="color:red">*<span></label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="pallet_code" name="pallet_code" required="required">
                                                <input type="text" class="form-control" id="pallet_type" name="pallet_type" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="pallet_type"
                                                        info1="{{Crypt::encrypt('mod_pallet')}}" 
                                                        info2="{{Crypt::encrypt("pallet_name+pallet_no,pallet_name,pallet_no")}}" 
                                                        info3="{{Crypt::encrypt("")}}"
                                                        info5="{{Crypt::encrypt('mod_pallet')}}"
                                                        info4="pallet_no=pallet_code;pallet_name=pallet_type;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="pallet_num">{{ trans('mod_pallet_manage.pallet_num') }} <span style="color:red">*<span></label>
                                            <input type="number" min ="0" class="form-control" id="pallet_num" name="pallet_num">
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="type">增加/減少 <span style="color:red">*<span></label>
                                            <select class="form-control" id="type" name="type">
                                                <option value="increase">增加</option>
                                                <option value="decrease">減少</option>
                                            </select>
                                        </div>

                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.move_type') }} <span style="color:red">*<span></label>
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="move_type" name="move_type" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="move_type"
                                                        info1="{{Crypt::encrypt('bscode')}}" 
                                                        info2="{{Crypt::encrypt("cd+cd_descp,cd,cd_descp")}}" 
                                                        info3="{{Crypt::encrypt("cd_type='MOVETYPE'")}}"
                                                        info5="{{Crypt::encrypt('bscode')}}"
                                                        info4="cd_descp=move_type;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label>{{ trans('mod_pallet_manage.remark') }}</label>
                                                <textarea class="form-control" rows="3" id="remark" name="remark"></textarea>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_pallet_manage.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_pallet_manage.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_pallet_manage.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_pallet_manage.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" id="type_cd" name="type_cd" value="warehouse" class="form-control">
                                    <input type="hidden" id="type_name" name="type_name" value="倉庫" class="form-control">
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
        </form>
    </div>



</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
                
            },"wh_name");
        }
    });

    $('#myForm button[btnName="pallet_type"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('pallet_type', "棧板搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="pallet_type"]').on('click', function(){
        var check = $('#subBox input[name="pallet_type"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","pallet_type",callBackFunc=function(data){
                console.log(data);
                
            },"pallet_type");
        }
    });

    $('#myForm button[btnName="move_type"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('move_type', "異動類型搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="move_type"]').on('click', function(){
        var check = $('#subBox input[name="move_type"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","move_type",callBackFunc=function(data){
                console.log(data);
                
            },"move_type");
        }
    });


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog";

    $(function(){
        //var formOpt = {};





        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouse/showmanage/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletManageWarehouselog" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {
            
        }
        formOpt.afterInit = function(data) {
            $('#type_cd').val('warehouse');
            $('#type_name').val('倉庫');

            menuBtnFunc.disabled(['iDel', 'iCopy' ,'iAdd']);
            
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"wh_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"pallet_type",
                    "column_type":"string",
                },
                {
                    "column_filed":"pallet_num",
                    "column_type":"string",
                },
                {
                    "column_filed":"type",
                    "column_type":"string",
                },
                {
                    "column_filed":"move_type",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        formOpt.saveSuccessFunc = function (){


            location.href = BASE_URL + '/' + lang + '/' + 'palletManageWarehouse';
        };
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);



    })
</script> 
<script>
$(function(){

});
</script>
@endsection