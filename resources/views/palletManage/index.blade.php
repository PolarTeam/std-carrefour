@extends('layout.layout')
@section('header')
<section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
    <h1>
        {{ trans('mod_pallet_manage.titleName') }}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('mod_pallet_manage.titleName') }}</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />
    <input type="hidden" value="{{$datafields2}}" id="datafields2" />
    <input type="hidden" value="{{$columns2}}" id="columns2" />
    <input type="hidden" value="{{$lang}}" id="lang" />
</section>
<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="searchWindow2" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody2">
        <div id="searchContent2">

        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter2">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName2"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch2">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault2">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch2">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn2">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd2">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="searchWindow3" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody3">
        <div id="searchContent3">
        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter3">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName3"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch3">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault3">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch3">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn3">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd3">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('before_scripts')
<script>
    // 總覽總數量
    // 各倉庫對應數量
    // 交易明細
    var lang = "{{$lang}}";
    var languagename = "";
    var enabledheaderdetail = [];
    var tableName = 'mod_pallet_manage_total';
    var fileName = 'palletManageWarehouse';
    // var condition ='?basecon=from_wh_cd;NULL;';
    var btnGroupList = [];
    var btnGroup = [
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                if(focus=="1") {
                    $('#searchWindow').jqxWindow('open');
                }else if(focus=="2"){
                    $('#searchWindow2').jqxWindow('open');
                }else{
                    $('#searchWindow3').jqxWindow('open');
                }
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                if(focus=="1"){
                    $('#gridOptModal').modal('show');
                }else if(focus=="2"){
                    $('#gridOptModal2').modal('show');
                }else{
                    $('#gridOptModal3').modal('show');
                }
               
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                var url  =BASE_URL;
                url = url.replace("admin","");
                var pageNum = 1;
                var pageSize = 50;
                var anotherCondition = typeof sqlcondition !== 'undefined' ? sqlcondition : [];
                if(focus=="1"){
                    var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];

                    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        var pageNum = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                        var pageSize = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagesize;
                        for (var i = 0; i < filterGroups.length; i++) {
                            var filterGroup = filterGroups[i];
                            var filters = filterGroup.filter.getfilters();
                            for (var k = 0; k < filters.length; k++) {
                                if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    // new Date(unix_timestamp * 1000)
                                    filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                } else if(filters[k].condition == "contains" ) {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                } else {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                }
                                // filtercolumndata.push(filtercolumn);
                            }
                        }
                    } else {
                        filtercolumn[m] = ['id', `in`, ids];
                    }
                    filtercolumn.push(['type_cd', `=`, "warehouse"]);
                    var sortby = [];
                    var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                        // sortby.push([ sortinformation.sortcolumn,  tpye]);
                    }
                    $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                        'table'           : 'mod_pallet_manage_total',
                        'fileName'        : '棧板管理',
                        'baseCondition'   : filtercolumn,
                        'anotherCondition': anotherCondition,
                        'pageNum'         : pageNum,
                        'pageSize'        : pageSize,
                        'sort'            : sortby,
                        'header'          : enabledheader,
                    }, function(data){

                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                } else if(focus=="2") {
                    var filterGroups = $('#jqxGrid_2').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];

                    var rows = $("#jqxGrid_2").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid_2").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        var pageNum = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                        var pageSize = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagesize;
                        for (var i = 0; i < filterGroups.length; i++) {
                            var filterGroup = filterGroups[i];
                            var filters = filterGroup.filter.getfilters();
                            for (var k = 0; k < filters.length; k++) {
                                if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    // new Date(unix_timestamp * 1000)
                                    filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                } else if(filters[k].condition == "contains" ) {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                } else {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                }
                                // filtercolumndata.push(filtercolumn);
                            }
                        }
                    } else {
                        filtercolumn[m] = ['id', `in`, ids];
                    }
                    filtercolumn.push(['type_cd', `=`, "warehouse"]);
                    var sortby = [];
                    var sortinformation = $('#jqxGrid_2').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                        // sortby.push([ sortinformation.sortcolumn,  tpye]);
                    }
                    $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                        'table'           : 'mod_pallet_manage',
                        'fileName'        : '棧板管理',
                        'baseCondition'   : filtercolumn,
                        'anotherCondition': anotherCondition,
                        'pageNum'         : pageNum,
                        'pageSize'        : pageSize,
                        'sort'            : sortby,
                        'header'          : enabledheaderdetail,
                    }, function(data){

                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                } else if(focus=="3") {
                    var filterGroups = $('#jqxGrid_3').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];

                    var rows = $("#jqxGrid_3").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid_3").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        var pageNum = $('#jqxGrid_3').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                        var pageSize = $('#jqxGrid_3').jqxGrid('getdatainformation').paginginformation.pagesize;
                        for (var i = 0; i < filterGroups.length; i++) {
                            var filterGroup = filterGroups[i];
                            var filters = filterGroup.filter.getfilters();
                            for (var k = 0; k < filters.length; k++) {
                                if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    // new Date(unix_timestamp * 1000)
                                    filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                    var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                    filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                } else if(filters[k].condition == "contains" ) {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                } else {
                                    filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                }
                                // filtercolumndata.push(filtercolumn);
                            }
                        }
                    } else {
                        filtercolumn[m] = ['id', `in`, ids];
                    }
                    filtercolumn.push(['type_cd', `=`, "warehouse"]);
                    var sortby = [];
                    var sortinformation = $('#jqxGrid_3').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                        // sortby.push([ sortinformation.sortcolumn,  tpye]);
                    }
                    $.post(BASE_URL + '/' + lang + '/' + fileName + '/excel/export', {
                        'table'           : 'pallet_total_by_wh_view',
                        'fileName'        : '棧板管理',
                        'baseCondition'   : filtercolumn,
                        'anotherCondition': anotherCondition,
                        'pageNum'         : pageNum,
                        'pageSize'        : pageSize,
                        'sort'            : sortby,
                        'header'          : enabledheaderdetail,
                    }, function(data){

                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                }

            }
        },
        @can("PALLETMANAGEWAREHOUSE-MANUAL")
        {
            btnId: "btnEdit",
            btnIcon: "fa fa-edit",
            btnText: '手動調整',
            btnFunc: function() {
                var editUrl = BASE_URL  + "/" +lang +"/palletManageWarehouseeditdetail";
                location.href = editUrl;
            }
        },
        @endcan
        @can("PALLETMANAGEWAREHOUSE-WHTOWH")
        {
            btnId: "btnfromwhinsert",
            btnIcon: "fa fa-edit",
            btnText: '新增',
            btnFunc: function() {
                var editUrl = BASE_URL  + "/" +lang +"/palletManageWarehouseeditwh";
                location.href = editUrl;
            }
        },
        @endcan
    ];

    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row" id="mainForm">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li name="prodDetail" class="active">
                <a onclick="tab_1()" href="#tab_1" data-toggle="tab" aria-expanded="false">總覽</a>
            </li>
            <li name="prodGift">
                <a onclick="tab_2()" href="#tab_2" data-toggle="tab" aria-expanded="false">明細(本倉)</a>
            </li>
            <li name="whtowh">
                <a onclick="tab_3()" href="#tab_3" data-toggle="tab" aria-expanded="false">明細(倉對倉)</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="griddiv">
                        <div id="jqxGrid"></div>
                        <div id="jqxGrid_2"></div>
                        <div id="jqxGrid_3"></div>
                    </div>
                    <input type="button" style="display:none" id="updategridsign" >
                    <input type="button" style="display:none" id="updategriderror" >
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Grid Option</h4>
			</div>
			<div class="modal-body">
				<div id="jqxlistbox"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
				<button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
				<button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid2">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid2">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal3">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox3"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid3">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid3">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>

<script>
    $(document).ready(function() {
        // 在這撰寫javascript程式碼
        $("[href='#tab_1']").click();   
    });
    function tab_1() {
        focus = "1";
        $("#btnAdd").show();
        $("#btnDelete").show();
        $("#btnTakedown").hide();
        $("#btnEdit").show();
        $("#btnAdd").show();
        $("#btnfromwhinsert").hide();
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid_3').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'none';
        // 
    }
    function tab_2() {
        focus = "2";
        $("#btnEdit").hide();
        $("#btnAdd").hide();
        $("#btnfromwhinsert").hide();
        $('#jqxGrid_2').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_2"></div>')


        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_3').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'block';
        ///
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ischeck = "N";
        var whNos = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                whNos.push(row.owner_by);
                ischeck = "Y";
            }
        }

        var sourcedetail = {
            datatype: "json",
            datafields: JSON.parse($("#datafields2").val()),
            root: "Rows",
            pagenum: 0,
            beforeprocessing: function(data) {
                sourcedetail.totalrecords = data[0].TotalRows;
                if (data[0].StatusCount.length > 0) {
                    data[0].StatusCount.push({
                        'count': data[0].TotalRows,
                        'status': 'ALL',
                        'statustext': 'ALL'
                    });
                }
            },
            filter: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_2").jqxGrid('updatebounddata', 'sort');
            },
            cache: false,
            pagesize: 50,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage/false') }}"+"?basecon=type_cd;EQUAL;warehouse;"
        }

        if ( ischeck == "N") {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage/false') }}" + "?basecon=type_cd;EQUAL;warehouse;";
        } else {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage/false') }}" + "?basecon=type_cd;EQUAL;warehouse;)*("+"wh_no;IN;"+JSON.stringify(whNos);
        }

        var dataAdapterdetail = new $.jqx.dataAdapter(sourcedetail, {
            async: false,
            loadError: function(xhr, status, error) {
                alert('Error loading "' + sourcedetail.url + '" : ' + error);
            },
            loadComplete: function() {

            }
        });

        var winHeight2 = $(window).height() - 350;
        $("#jqxGrid_2").jqxGrid({
            width: '100%',
            height: winHeight2,
            source: dataAdapterdetail,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 2000],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
                loadState2();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: JSON.parse($("#columns2").val())
        });
        $("#jqxGrid_2").jqxGrid('updatebounddata');


        function loadState2() {
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: 'mod_pallet_manage',
                    lang: 'zh-TW'
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid_2" ).jqxGrid('loadstate', response);
                    }

                    var listSource2 = [];
                    state2 = $("#jqxGrid_2").jqxGrid('getstate');
                    console.log(state2);
                    $('#jqxLoader').jqxLoader('close');

                    $.get(BASE_URL + "/searchList/get/" + "mod_pallet_manage", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName2']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName2']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" +"mod_pallet_manage/"+ $("select[name='selSearchName2']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent2").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state2);
                                        initSearchWindow2(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow2').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 200,
                                        minWidth: 200,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow2').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd2']").on("click", function() {
                                        var seasrchTpl = getSearchTpl2(state2);
                                        $("#searchContent2").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove2']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj2[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp2[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent2[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn2']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField2[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent2[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp2[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter2(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state2.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource2.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheaderdetail.push(headerdata);
                            }
                        }
                    });
                    console.log(listSource2);
                    $("#jqxlistbox2").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource2,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });
        }
        function initSearchWindow2(searchTpl) {
            $("#searchContent2").append(searchTpl);
        }
        $("#saveGrid2").on("click", function() {
            var items = $("#jqxlistbox2").jqxListBox('getItems');
            var gridname = "jqxGrid_2";
            var savekey = "mod_pallet_manage";
            // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
            enabledheaderdetail    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                }
                if (item.checked) {
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheaderdetail.push(headerdata);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'mod_pallet_manage',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("save successful");
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid2").on("click", function() { 
            savekey = "mod_pallet_manage";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal2').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });


        $("#jqxGrid_2").on("rowdoubleclick", function(event) {
            // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick;
            // original event.
            var ev = args.originalEvent;
            if(typeof toedit !== "undefined" && toedit == "false") {
                return;
            }
            var datarow = $("#jqxGrid_2").jqxGrid('getrowdata', boundIndex);
            console.log(datarow);

            // status
            var editUrl = BASE_URL  + "/" +lang +"/"+'palletManageWarehousedetailedit'+"/" + datarow.id ;
            //location.href = editUrl;
            window.open(editUrl);
        })
        function getSearchTpl2(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if (item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for (i in fields) {
            fieldStr += '<option value="' + fields[i].value + '">' + fields[i].label + '</option>';
        }




        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField2[]">' + fieldStr + '</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp2[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="CONTAINSLIKE">包含(多值)</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NOT_EQUAL">不等於</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                <option value="LESS_THAN">小於</option>\
                                <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                <option value="GREATER_THAN">大於</option>\
                                <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent2[]">\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove2">-</button>\
                            </div>\
                        </div>';
            return searchTpl;
        }

        var addfilter2 = function(datafield, filterval, filtercondition) {
            $("#jqxGrid_2").jqxGrid('clearfilters');
            var old_filed = "";
            for (i in datafield) {
                var filtergroup = new $.jqx.filter();
                if (old_filed != datafield[i]) {
                    old_filed = datafield[i];

                    var filter_or_operator = 1;
                    //var filtervalue = filterval;
                    var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                    if (datafield[i] == "updated_at" || datafield[i] == "created_at" || datafield[i] == "confirm_dt" || datafield[i] == "finish_date" || datafield[i] == "etd") {
                        filter = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                        filtergroup.addfilter(filter_or_operator, filter);
                    } else {
                        filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                        filtergroup.addfilter(filter_or_operator, filter);
                    }

                    // add the filters.
                    $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);
                } else {
                    var filter3 = filtergroup.createfilter('datefilter', filterval[i - 1], filtercondition[i - 1]);
                    filtergroup.addfilter(0, filter3);
                    $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);

                    var filter2 = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(0, filter2);
                    $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);
                }
            }
            // apply the filters.
            $("#jqxGrid_2").jqxGrid('applyfilters');
        }
        $("button[name='setDefault2']").on("click", function() {
            if(focus=="1"){
                savekey ="mod_dss_dlv";
            }else{
                savekey = "mod_dss_dlvdetail";
            }
            $.ajax({
                url: BASE_URL + "/setSearchDefault/" + savekey + "/" + $("select[name='selSearchName2']").val(),
                dataType: "JSON",
                method: "PUT",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew13')}}", "", "success");
                    } else {
                        swal("{{trans('common.msgnew5')}}", "", "error");
                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            });
        });
        $("button[name='delSearch2']").on("click", function() {
            $.ajax({
                url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName2']").val(),
                dataType: "JSON",
                method: "DELETE",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew4')}}", "", "success");
                        $("select[name='selSearchName2'] option:selected").remove();
                        $("input[name='searchName2']").val("");
                    } else {
                        if (typeof data.errorMsg !== "undefined") {
                            swal("{{trans('common.msgnew5')}}", data.errorMsg, "error");
                        } else {
                            swal("{{trans('common.msgnew5')}}", "", "error");
                        }

                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            });
        });

        $("button[name='saveSearch2']").on("click", function() {
            if(focus=="1"){
                savekey ="mod_dss_dlv";
            }else{
                savekey = "mod_dss_dlvdetail";
            }
            var htmlData = $("#searchContent2").html();
            var postData = {
                "key": savekey,
                "data": htmlData,
                "title": $("input[name='searchName2']").val(),
                "id": ($("select[name='selSearchName2'] option:selected").text() == $("input[name='searchName2']").val()) ? $("select[name='selSearchName2']").val() : null
            };
            $.ajax({
                url: BASE_URL + "/{{$lang}}" + "/saveSearchLayout",
                data: postData,
                dataType: "JSON",
                method: "POST",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew13')}}", "", "success");
                        $("select[name='selSearchName2'] option").removeAttr("selected");
                        $("select[name='selSearchName2']").append("<option value='" + result.id + "' selected>" + $("input[name='searchName2']").val() + "</option>")
                    } else {
                        swal("{{trans('common.msgnew5')}}", "", "error");
                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew12')}}", "", "error");
                }
            });
        });

    }
    function tab_3() {
        focus = "3";
        $("#btnEdit").hide();
        $("#btnAdd").hide();
        $("#btnfromwhinsert").show();
        $('#jqxGrid_3').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_3"></div>')


        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'none';
        document.getElementById('jqxGrid_3').style.display = 'block';
        ///
        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ischeck = "N";
        var whNos = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                whNos.push(row.owner_by);
                ischeck = "Y";
            }
        }

        var sourcedetail = {
            datatype: "json",
            datafields: JSON.parse($("#datafields2").val()),
            root: "Rows",
            pagenum: 0,
            beforeprocessing: function(data) {
                sourcedetail.totalrecords = data[0].TotalRows;
                if (data[0].StatusCount.length > 0) {
                    data[0].StatusCount.push({
                        'count': data[0].TotalRows,
                        'status': 'ALL',
                        'statustext': 'ALL'
                    });
                }
            },
            filter: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_3").jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $("#jqxGrid_3").jqxGrid('updatebounddata', 'sort');
            },
            cache: false,
            pagesize: 50,
            url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/pallet_total_by_wh_view/false') }}"+"?basecon=type_cd;EQUAL;warehouse;"
        }

        if ( ischeck == "N") {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/pallet_total_by_wh_view/false') }}" + "?basecon=type_cd;EQUAL;warehouse;";
        } else {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/pallet_total_by_wh_view/false') }}" + "?basecon=type_cd;EQUAL;warehouse;)*("+"wh_no;IN;"+JSON.stringify(whNos);
        }

        var dataAdapterdetail = new $.jqx.dataAdapter(sourcedetail, {
            async: false,
            loadError: function(xhr, status, error) {
                alert('Error loading "' + sourcedetail.url + '" : ' + error);
            },
            loadComplete: function() {

            }
        });

        var winHeight3 = $(window).height() - 350;
        $("#jqxGrid_3").jqxGrid({
            width: '100%',
            height: winHeight3,
            source: dataAdapterdetail,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 3000],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_3").jqxGrid('height', winHeight3 + 'px');
                loadState3();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: JSON.parse($("#columns2").val())
        });
        $("#jqxGrid_3").jqxGrid('updatebounddata');


        function loadState3() {
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: 'mod_pallet_manage',
                    lang: 'zh-TW'
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid_3" ).jqxGrid('loadstate', response);
                    }

                    var listSource3 = [];
                    state3 = $("#jqxGrid_3").jqxGrid('getstate');
                    console.log(state3);
                    $('#jqxLoader').jqxLoader('close');

                    $.get(BASE_URL + "/searchList/get/" + "mod_pallet_manage", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName3']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName3']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" +"mod_pallet_manage/"+ $("select[name='selSearchName3']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent3").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state3);
                                        initSearchWindow3(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow3').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 300,
                                        minWidth: 300,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow3').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd3']").on("click", function() {
                                        var seasrchTpl = getSearchTpl3(state3);
                                        $("#searchContent3").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove3']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField3[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj3[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp3[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent3[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn3']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField3[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent3[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp3[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter3(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state3.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource3.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheaderdetail.push(headerdata);
                            }
                        }
                    });
                    console.log(listSource3);
                    $("#jqxlistbox3").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource3,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });
        }
        function initSearchWindow3(searchTpl) {
            $("#searchContent3").append(searchTpl);
        }
        $("#saveGrid3").on("click", function() {
            var items = $("#jqxlistbox3").jqxListBox('getItems');
            var gridname = "jqxGrid_3";
            var savekey = "mod_pallet_manage";
            console.log('saveGrid3 log');
            // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
            enabledheaderdetail    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                }
                if (item.checked) {
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheaderdetail.push(headerdata);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'mod_pallet_manage',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("save successful");
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal3').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid3").on("click", function() { 
            savekey = "mod_pallet_manage";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal').modal('hide');
                        $('#gridOptModal3').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });


        $("#jqxGrid_3").on("rowdoubleclick", function(event) {
            // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick;
            // original event.
            var ev = args.originalEvent;
            if(typeof toedit !== "undefined" && toedit == "false") {
                return;
            }
            var datarow = $("#jqxGrid_3").jqxGrid('getrowdata', boundIndex);
            console.log(datarow);

            // status
            var editUrl = BASE_URL  + "/" +lang +"/"+'palletManageWarehousetotaledit'+"/" + datarow.id ;
            //location.href = editUrl;
            window.open(editUrl);
        })
        function getSearchTpl3(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if (item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for (i in fields) {
            fieldStr += '<option value="' + fields[i].value + '">' + fields[i].label + '</option>';
        }




        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField3[]">' + fieldStr + '</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp3[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="CONTAINSLIKE">包含(多值)</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NOT_EQUAL">不等於</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                <option value="LESS_THAN">小於</option>\
                                <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                <option value="GREATER_THAN">大於</option>\
                                <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent3[]">\
                            </div>\
                            <div class="col-xs-3">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove3">-</button>\
                            </div>\
                        </div>';
            return searchTpl;
        }

        var addfilter3 = function(datafield, filterval, filtercondition) {
            $("#jqxGrid_3").jqxGrid('clearfilters');
            var old_filed = "";
            for (i in datafield) {
                var filtergroup = new $.jqx.filter();
                if (old_filed != datafield[i]) {
                    old_filed = datafield[i];

                    var filter_or_operator = 1;
                    //var filtervalue = filterval;
                    var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                    if (datafield[i] == "updated_at" || datafield[i] == "created_at" || datafield[i] == "confirm_dt" || datafield[i] == "finish_date" || datafield[i] == "etd") {
                        filter = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                        filtergroup.addfilter(filter_or_operator, filter);
                    } else {
                        filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                        filtergroup.addfilter(filter_or_operator, filter);
                    }

                    // add the filters.
                    $("#jqxGrid_3").jqxGrid('addfilter', datafield[i], filtergroup);
                } else {
                    var filter3 = filtergroup.createfilter('datefilter', filterval[i - 1], filtercondition[i - 1]);
                    filtergroup.addfilter(0, filter3);
                    $("#jqxGrid_3").jqxGrid('addfilter', datafield[i], filtergroup);

                    var filter3 = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(0, filter3);
                    $("#jqxGrid_3").jqxGrid('addfilter', datafield[i], filtergroup);
                }
            }
            // apply the filters.
            $("#jqxGrid_3").jqxGrid('applyfilters');
        }
        $("button[name='setDefault3']").on("click", function() {
            if(focus=="1"){
                savekey ="mod_dss_dlv";
            }else{
                savekey = "mod_dss_dlvdetail";
            }
            $.ajax({
                url: BASE_URL + "/setSearchDefault/" + savekey + "/" + $("select[name='selSearchName3']").val(),
                dataType: "JSON",
                method: "PUT",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew13')}}", "", "success");
                    } else {
                        swal("{{trans('common.msgnew5')}}", "", "error");
                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            });
        });
        $("button[name='delSearch3']").on("click", function() {
            $.ajax({
                url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName3']").val(),
                dataType: "JSON",
                method: "DELETE",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew4')}}", "", "success");
                        $("select[name='selSearchName3'] option:selected").remove();
                        $("input[name='searchName3']").val("");
                    } else {
                        if (typeof data.errorMsg !== "undefined") {
                            swal("{{trans('common.msgnew5')}}", data.errorMsg, "error");
                        } else {
                            swal("{{trans('common.msgnew5')}}", "", "error");
                        }

                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            });
        });

        $("button[name='saveSearch3']").on("click", function() {
            if(focus=="1"){
                savekey ="mod_dss_dlv";
            }else{
                savekey = "mod_dss_dlvdetail";
            }
            var htmlData = $("#searchContent3").html();
            var postData = {
                "key": savekey,
                "data": htmlData,
                "title": $("input[name='searchName3']").val(),
                "id": ($("select[name='selSearchName3'] option:selected").text() == $("input[name='searchName3']").val()) ? $("select[name='selSearchName3']").val() : null
            };
            $.ajax({
                url: BASE_URL + "/{{$lang}}" + "/saveSearchLayout",
                data: postData,
                dataType: "JSON",
                method: "POST",
                success: function(result) {
                    if (result.msg == "success") {
                        swal("{{trans('common.msgnew13')}}", "", "success");
                        $("select[name='selSearchName3'] option").removeAttr("selected");
                        $("select[name='selSearchName3']").append("<option value='" + result.id + "' selected>" + $("input[name='searchName3']").val() + "</option>")
                    } else {
                        swal("{{trans('common.msgnew5')}}", "", "error");
                    }
                },
                error: function() {
                    swal("{{trans('common.msgnew13')}}", "", "error");
                }
            });
        });

    }
</script>
@endsection