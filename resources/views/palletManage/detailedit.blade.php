@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_pallet_manage.titleName') }}<small></small>
      </h1>
      <input type="hidden" value="{{$datafields}}" id="datafields" />
      <input type="hidden" value="{{$columns}}" id="columns" />
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/palletManage') }}">{{ trans('mod_pallet_manage.titleName') }}</a></li>
		<li class="active">{{ trans('mod_pallet_manage.titleName') }}</li>
      </ol>
    </section>
    <div id="searchWindow" style="overflow-x:hidden; display:none">
        <div>{{ trans('common.search') }}</div>
        <div id="searchBody">
            <div id="searchContent">
    
            </div>
            <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
                <div class="col-xs-3">
                    <select class="form-control input-sm" name="selSearchName"></select>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
                </div>
                <div class="col-xs-3">
                    <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                    <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                    <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
                </div>
                <div class="col-xs-3" style="align: right">
                    <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                    <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Grid Option</h4>
                </div>
                <div class="modal-body">
                    <div id="jqxlistbox"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                    <button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
                    <button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">
                                        @if(isset($id))
                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.wh_name') }}</label>
                                            <input type="text" class="form-control" id="wh_name" name="wh_name" required="required" readonly="true" disabled="disabled">
                                        </div>
                                        @else
                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.wh_name') }}</label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="wh_no" name="wh_no" required="required">
                                                <input type="text" class="form-control" id="wh_name" name="wh_name" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="wh_name"
                                                        info1="{{Crypt::encrypt('mod_warehouse')}}" 
                                                        info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname")}}" 
                                                        info3="{{Crypt::encrypt("")}}"
                                                        info5="{{Crypt::encrypt('mod_warehouse')}}"
                                                        info4="cust_no=wh_no;cname=wh_name;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="contact">{{ trans('mod_pallet_manage.pallet_type') }}</label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="pallet_code" name="pallet_code" required="required">
                                                <input type="text" class="form-control" id="pallet_type" name="pallet_type" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="pallet_type"
                                                        info1="{{Crypt::encrypt('mod_pallet')}}" 
                                                        info2="{{Crypt::encrypt("pallet_name+pallet_no,pallet_name,pallet_no")}}" 
                                                        info3="{{Crypt::encrypt("")}}"
                                                        info5="{{Crypt::encrypt('mod_pallet')}}"
                                                        info4="pallet_no=pallet_code;pallet_name=pallet_type;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                        @if(isset($id))
                                        <div class="form-group col-md-3">
                                            <label for="pallet_num">{{ trans('mod_pallet_manage.pallet_num') }}</label>
                                            <input type="number" min ="0" class="form-control" id="pallet_num" name="pallet_num">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="pallet_type">{{ trans('mod_pallet_manage.pallet_type') }}</label>
                                            <input type="text" min ="0" class="form-control" id="pallet_type" name="pallet_type" readonly="true" disabled="disabled">
                                        </div>
                                        @endif
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_pallet_manage.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_pallet_manage.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_pallet_manage.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_pallet_manage.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" id="type_cd" name="type_cd" value="warehouse" class="form-control">
                                    <input type="hidden" id="type_name" name="type_name" value="倉庫" class="form-control">
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
        </form>
    </div>



</div>

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <div class="tab-content">
                <!-- /.tab-pane -->
                <form method="get" action="" onsubmit="return false"  accept-charset="UTF-8" id="tempform" enctype="multipart/form-data">
                    <div class="tab-pane active" id="tab_2">
                        <div id="jqxGrid"></div>
                    </div>
                </form>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
                
            },"wh_name");
        }
    });

    $('#myForm button[btnName="pallet_type"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('pallet_type', "棧板搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="pallet_type"]').on('click', function(){
        var check = $('#subBox input[name="pallet_type"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","pallet_type",callBackFunc=function(data){
                console.log(data);
                
            },"pallet_type");
        }
    });


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail";

    $(function(){
        //var formOpt = {};



        var btnGroup = [
            @if(isset($id))
            {
                btnId: "btnSearchWindow",
                btnIcon: "fa fa-search",
                btnText: "{{ trans('common.search') }}",
                btnFunc: function () {
                    $('#searchWindow').jqxWindow('open');
                }
            },
            {
                btnId: "btnExportExcel",
                btnIcon: "fa fa-cloud-download",
                btnText: "{{ trans('common.exportExcel') }}",
                btnFunc: function () {
                    var url  =BASE_URL;
                    url = url.replace("admin","");
                    var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    // ----------
                    var url  =BASE_URL;
                    url = url.replace("admin","");
                    var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];
                    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        if($('#jqxGrid').jqxGrid('getrows').length == 0 ) {
                            swal('目前沒有資料','','warning');
                            return;
                        }
                        for (var i = 0; i < $('#jqxGrid').jqxGrid('getrows').length; i++) {
                            var row = $("#jqxGrid").jqxGrid('getrowdata', i);
                            if(typeof row != "undefined" && row.id!='') {
                                ids.push(row.id);
                            }
                        }
                    } 
                    filtercolumn[0] = ['id', `in`, ids];
                    var sortby = [];
                    var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                    }
                    $.post(BASE_URL +"/"+lang+ '/palletManageWarehouse/excel/export', {
                        'baseCondition': filtercolumn,
                        'sort'         : sortby,
                        'pageNum'      : 1,
                        'pageSize'     : 99999,
                        'header'       : enabledheader,
                        'table'        : 'mod_pallet_manage_log',
                        'fileName'     : 'mod_pallet_manage_log',
                    }, function(data){
                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                    // ----------
                }
            },
            {
                btnId: "btnOpenGridOpt",
                btnIcon: "fa fa-table",
                btnText: "{{ trans('common.gridOption') }}",
                btnFunc: function () {
                    $('#gridOptModal').modal('show');
                }
            },
            @endif
        ];

        initBtn(btnGroup);


        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/palletWarehousedetail" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {
            
        }
        formOpt.afterInit = function(data) {
            $('#type_cd').val('warehouse');
            $('#type_name').val('倉庫');


            menuBtnFunc.disabled(['iDel', 'iCopy' ,'iAdd']);
            // console.log(data);
            //jqxGrid srart
            // if(( data.data.updated_by == "{{Auth::user()->id}}" ) && data.data.type == "materiatransfer" ) {

            // } else {
            //     $("#btnRedirect").hide();
            // }

            var h = 600;

            var winHeigt = $(window).height() - h;
            
            var source = {
                datatype: "json",
                datafields: JSON.parse($("#datafields").val()),
                root: "Rows",
                pagenum: 0,
                filter: function() {
                    // update the grid and send a request to the server.
                    $("#jqxGrid").jqxGrid('updatebounddata', 'filter');
                },
                sort: function() {
                    // update the grid and send a request to the server.
                    $("#jqxGrid").jqxGrid('updatebounddata', 'sort');
                },
                cache: false,
                pagesize: 50,
                url: "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_pallet_manage_log/false') }}" + "?basecon=wh_no;EQUAL;" + data.data.wh_no+")*(type_cd;EQUAL;warehouse;"+")*(pallet_code;EQUAL;"+data.data.pallet_code+";",
            }

            var dataAdapter = new $.jqx.dataAdapter(source, {
                async: false,
                loadError: function(xhr, status, error) {
                    alert('Error loading "' + source.url + '" : ' + error);
                },
                loadComplete: function() {
                }
            });

            var columnsdata = JSON.parse($("#columns").val());


            console.log( JSON.parse($("#columns").val()));

            $("#jqxGrid").jqxGrid({
                width: '100%',
                height: winHeigt,
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                showfilterrow: true,
                pageable: true,
                virtualmode: true,
                autoshowfiltericon: true,
                columnsreorder: true,
                columnsresize: true,
                columnsautoresize: true,
                selectionmode:"checkbox",
                clipboard: true,
                enablebrowserselection: true,
                pagesizeoptions: [50, 100, 500, 2000],
                rendergridrows: function(params) {
                    //alert("rendergridrows");
                    return params.data;
                },
                ready: function() {
                    //$('#' + gridId).jqxGrid('autoresizecolumns');
                    loadState();
                },
                updatefilterconditions: function(type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
                columns: columnsdata
            });
            $("#jqxGrid").on('bindingcomplete', function(event) {
                console.log('bindingcomplete');
                // var winHeight = $(window).height() - 350 ;
                $("#jqxGrid").jqxGrid('height', winHeigt + 'px');
            });
            //jqxGrid end
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"wh_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"pallet_type",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        function loadState() {
            
            var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
            $.ajax({
                type: "GET", //  OR POST WHATEVER...
                url: loadURL,
                data: {
                    key: "mod_pallet_manage_whdetail",
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response != "") {
                        response = JSON.parse(response);
                        $("#jqxGrid").jqxGrid('loadstate', response);
                    }

                    var listSource = [];

                    state = $("#jqxGrid").jqxGrid('getstate');
                    console.log(state);
                    $('#jqxLoader').jqxLoader('close');
                    $.get(BASE_URL + "/searchList/get/" + "mod_pallet_manage_whdetail", {}, function(data) {
                        if (data.msg == "success") {
                            var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                            for (i in data.data) {
                                if (data.data[i]["layout_default"] == "Y") {
                                    opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                    $("input[name='searchName']").val(data.data[i]["title"]);
                                } else {
                                    opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                                }
                            }

                            $("select[name='selSearchName']").html(opt);

                            $.get(BASE_URL + "/searchHtml/get/" + $("select[name='selSearchName']").val(), {}, function(data) {
                                if (data.msg == "success") {
                                    if (data.data != null) {
                                        $("#searchContent").html(data.data["data"]);
                                    } else {
                                        var seasrchTpl = getSearchTpl(state);
                                        // initSearchWindow(seasrchTpl);
                                    }

                                    var offset = $(".content-wrapper").offset();
                                    $('#searchWindow').jqxWindow({
                                        position: {
                                            x: offset.left + 50,
                                            y: offset.top + 50
                                        },
                                        showCollapseButton: true,
                                        maxHeight: 400,
                                        maxWidth: 700,
                                        minHeight: 200,
                                        minWidth: 200,
                                        height: 300,
                                        width: 700,
                                        autoOpen: false,
                                        initContent: function() {
                                            $('#searchWindow').jqxWindow('focus');
                                        }
                                    });


                                    $("button[name='winSearchAdd']").on("click", function() {
                                        var seasrchTpl = getSearchTpl(state);
                                        $("#searchContent").append(searchTpl);
                                    });

                                    $(document).on("click", "button[name='winBtnRemove']", function() {
                                        $(this).parents(".row").remove();
                                    });

                                    $(document).on("change", "select[name='winField[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                        var val = $(this).val();
                                        var info = searchObj(val, fieldObj[0]);
                                        var str = [{
                                                val: 'CONTAINS',
                                                label: '包含'
                                            },
                                            {
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var num = [{
                                                val: 'EQUAL',
                                                label: '等於'
                                            },
                                            {
                                                val: 'NOT_EQUAL',
                                                label: '不等於'
                                            },
                                            {
                                                val: 'LESS_THAN',
                                                label: '小於'
                                            },
                                            {
                                                val: 'LESS_THAN_OR_EQUAL',
                                                label: '小於等於'
                                            },
                                            {
                                                val: 'GREATER_THAN',
                                                label: '大於'
                                            },
                                            {
                                                val: 'GREATER_THAN_OR_EQUAL',
                                                label: '大於等於'
                                            },
                                            {
                                                val: 'NULL',
                                                label: 'NULL'
                                            },
                                            {
                                                val: 'NOT_NULL',
                                                label: 'NOT NULL'
                                            },
                                        ];

                                        var opt = "";
                                        if (info.type == "string") {
                                            for (i in str) {
                                                opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                            }
                                        } else {
                                            for (i in num) {
                                                opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                            }
                                        }
                                        $($(this).parent().siblings()[0]).find("select").html(opt);
                                    });

                                    $(document).on("change", "select[name='winOp[]']", function() {
                                        $('option:selected', this).siblings().removeAttr('selected');
                                        $('option:selected', this).attr('selected', 'selected');
                                    });

                                    $(document).on("change", "input[name='winContent[]']", function() {
                                        $(this).attr('value', $(this).val());
                                    });

                                    $("button[name='winSearchBtn']").on("click", function() {
                                        var winField = [];
                                        var winContent = [];
                                        var winOp = [];
                                        $("select[name='winField[]']").each(function() {
                                            winField.push($(this).val());
                                        });
                                        $("input[name='winContent[]']").each(function() {
                                            winContent.push($(this).val());
                                        });
                                        $("select[name='winOp[]']").each(function() {
                                            winOp.push($(this).val());
                                        });

                                        addfilter(winField, winContent, winOp);
                                    });
                                }
                            });

                        }
                    });

                    $.each(state.columns, function(i, item) {
                        if (item.text != "" && item.text != "undefined") {
                            listSource.push({
                                label: item.text,
                                value: i,
                                checked: !item.hidden
                            });
                            if (!item.hidden) {
                                var headerdata = {
                                    'filed_text': item.text,
                                    'filed_name': i,
                                    'show': item.hidden
                                };
                                enabledheader.push(headerdata);
                            }
                        }
                    });

                    $("#jqxlistbox").jqxListBox({
                        allowDrop: true,
                        allowDrag: true,
                        source: listSource,
                        width: "99%",
                        height: 500,
                        checkboxes: true,
                        filterable: true,
                        searchMode: 'contains'
                    });
                }
            });

        }

        $("#saveGrid").on("click", function() {
            var items = $("#jqxlistbox").jqxListBox('getItems');
            var gridname = "jqxGrid";
            var savekey = "mod_pallet_manage_whdetail";
            items = $("#jqxlistbox").jqxListBox('getItems');
            gridname = 'jqxGrid';
            savekey ="mod_pallet_manage_whdetail";
            var key = 0;
            enabledheader    = [];
            $.each(items, function(i, item) {
                var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
                if (thisIndex != item.index) {
                    //console.log(item.value+":"+thisIndex+"="+item.index);
                    $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
                };
                key++;
                if (item.checked) {
                    var headerdata = {
                        'filed_text': item.label,
                        'filed_name': item.value,
                        'show': item.hidden
                    };
                    enabledheader.push(headerdata);
                    $("#" + gridname).jqxGrid('showcolumn', item.value);
                } else {
                    $("#" + gridname).jqxGrid('hidecolumn', item.value);
                }
            })

            $("#" + gridname).jqxGrid('endupdate');
            state = $("#" + gridname).jqxGrid('getstate');

            var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
            var stateToSave = JSON.stringify(state);

            $.ajax({
                type: "POST",
                url: saveUrl,
                data: {
                    data: stateToSave,
                    table_name: 'mod_pallet_manage',
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("save successful");
                        $('#gridOptModal').modal('hide');
                    } else {
                        alert("save failded");
                    }

                }
            });
        });

        $("#clearGrid").on("click", function() { 
            savekey ="mod_pallet_manage_whdetail";
            $.ajax({
                type: "POST",
                url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
                data: {
                    key: savekey,
                    lang: $("#lang").val()
                },
                success: function(response) {
                    if (response == "true") {
                        alert("clear successful");
                        location.reload();
                        $('#gridOptModal').modal('hide');
                    } else {
                        //alert("save failded");
                    }

                }
            });
        });

        function getSearchTpl(state) {
            var fields = [];
            $.each(state.columns, function(i, item) {
                if (item.hidden == false && item.text != "") {
                    var a = {
                        label: item.text,
                        value: i
                    }
                    fields.push(a);
                }
            });

            var fieldStr = "";
            for (i in fields) {
                fieldStr += '<option value="' + fields[i].value + '">' + fields[i].label + '</option>';
            }

            searchTpl = '<div class="row">\
                                <div class="col-xs-3">\
                                    <select class="form-control input-sm" name="winField[]">' + fieldStr + '</select>\
                                </div>\
                                <div class="col-xs-3">\
                                    <select class="form-control input-sm" name="winOp[]">\
                                    <option value="CONTAINS">包含</option>\
                                    <option value="CONTAINSLIKE">包含(多值)</option>\
                                    <option value="EQUAL">等於</option>\
                                    <option value="NOT_EQUAL">不等於</option>\
                                    <option value="NULL">NULL</option>\
                                    <option value="NOT_NULL">NOT NULL</option>\
                                    <option value="LESS_THAN">小於</option>\
                                    <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                    <option value="GREATER_THAN">大於</option>\
                                    <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                    </select>\
                                </div>\
                                <div class="col-xs-4">\
                                    <input type="text" class="form-control input-sm" name="winContent[]">\
                                </div>\
                                <div class="col-xs-2">\
                                    <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove">-</button>\
                                </div>\
                            </div>';
            return searchTpl;
        }

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection