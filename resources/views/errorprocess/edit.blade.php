@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_error_report.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_error_report') }}">{{ trans('mod_error_report.titleName') }}</a></li>
		<li class="active">{{ trans('mod_error_report.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                              <div class="row">

                                <div class="form-group col-md-3">
                                    <label for="sys_dlv_no">{{ trans('mod_error_report.sys_dlv_no') }}</label>
                                    <input type="text" class="form-control" id="sys_dlv_no"  name="sys_dlv_no"  readonly="true" disabled="disabled">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="ord_no">{{ trans('mod_error_report.ord_no') }}</label>
                                    <input type="text" class="form-control" id="ord_no"  name="ord_no"  readonly="true" disabled="disabled">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="error_name">{{ trans('mod_error_report.error_name') }}</label>
                                    <input type="text" class="form-control" id="error_name"  name="error_name"  readonly="true" disabled="disabled">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="owner_name">{{ trans('mod_error_report.owner_name') }}</label>
                                    <input type="text" class="form-control" id="owner_name"  name="owner_name"  readonly="true" disabled="disabled">
                                </div>
                              </div>

                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="status">{{ trans('mod_error_report.status_descp') }}</label>
                                    <select class="form-control " id="status" name="status">
                                        <option value="UNTREATED">未處理</option>
                                        <option value="PROCESS">處理中</option>
                                        <option value="FINISHED">已處理</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cust_name">{{ trans('mod_error_report.cust_name') }}</label>
                                    <input type="text" class="form-control" id="cust_name"  name="cust_name"  readonly="true" disabled="disabled">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="mission_status">{{ trans('mod_error_report.mission_status_desc') }}</label>
                                    <select class="form-control " id="mission_status" name="mission_status">
                                        <option value="GOBACK">任務回倉</option>
                                        <option value="CANCEL">任務取消結案</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="error_remark">{{ trans('mod_error_report.error_remark') }}</label>
                                    <textarea class="form-control" id='error_remark' rows="3" name="error_remark" readonly="true" disabled="disabled"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="process_remark">{{ trans('mod_error_report.process_remark') }}</label>
                                    <textarea class="form-control" id='process_remark' rows="3" name="process_remark"></textarea>
                                </div>
                            </div>



                                <div class="row">
                                  <div class="form-group col-md-3">
                                      <label for="created_by">{{ trans('mod_error_report.created_by') }}</label>
                                      <input type="text" class="form-control" id="created_by" name="created_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="created_at">{{ trans('mod_error_report.created_at') }}</label>
                                      <input type="text" class="form-control" id="created_at" name="created_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_by">{{ trans('mod_error_report.updated_by') }}</label>
                                      <input type="text" class="form-control" id="updated_by" name="updated_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_at">{{ trans('mod_error_report.updated_at') }}</label>
                                      <input type="text" class="form-control" id="updated_at" name="updated_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                              </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_8" data-toggle="tab" aria-expanded="false">圖片資訊</a></li>
                <li class=""><a href="#tab_12" data-toggle="tab" aria-expanded="false">異常推播紀錄</a></li>
            </ul>
            <div class="tab-content">
    
                <div class="tab-pane active" id="tab_8">
                    <div class="box box-primary" id="subBox8" style="display:none">
                        <form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="guid">圖片</label>
                                                <input type="file" name="guid" id="guid"> 
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="status">敘述</label>
                                                <select class="form-control" id="type_no" name="type_no">
                                                    <option value="error">異常回報</option>
                                                    <option value="normal">一般回報</option>
                                                    <option value="upstairs">上樓</option>
                                                    <option value="downpoint">下點</option>
                                                    <option value="pickdlv">提配單</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                        <button type="button" class="btn btn-sm btn-primary" id="Save9">{{ trans('common.save') }}</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="Cancel9">{{ trans('common.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <div id="imgGrid2"></div>
                </div>
                <div class="tab-pane active" id="tab_12">
                    <div class="box box-primary" id="subBox8" style="display:none">
                        <form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="row">
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                        <button type="button" class="btn btn-sm btn-primary" id="Save10">{{ trans('common.save') }}</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="Cancel10">{{ trans('common.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <div id="fcmGrid"></div>
                </div>

            
            </div>
            <!-- /.tab-content -->
        </div>
    </div> 
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId       = "";
    var ordNo        = "";
    var lang         = "en";
    var cust_no      = "";
    var editData     = null;
    var editObj      = null;
    var fieldData    = null;
    var fieldObj     = null;
    var initformdata = new Array();
    var canStore     = '{{$canStore}}';
    var canEdit      = '{{$canEdit}}';
    var canDelete    = '{{$canDelete}}';
    var remark       = '';
    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif
    @if(isset($ordNo))
        ordNo = "{{$ordNo}}";
    @endif
    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess";

    $(function(){
        var imagerenderer = function (row, datafield, value) {
            if(value != "") {
                console.log(BASE_URL.replace('/admin',''));
                return '<img style="margin-left: 15px;" height="60" width="50" src="'+"{{env('PRINTER_URL')}}"+ value + '"/>';

            }
            // if(value != "") {
            //     console.log(BASE_URL.replace('/admin',''));
            //     return '<img style="margin-left: 15px;" height="60" width="50" src="'+BASE_URL.replace('/admin','')+'/storage/uploadimg/' + value.replace('public/', '/zh-TW') + '"/>';
            // }
            return "";
        }
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/errorprocess" ;
        
        formOpt.initFieldCustomFunc = function (){

            // console.log('test step');
            // $('#mod_error_report').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            console.log(data);
            initformdata = data;
            if(data.data.process_remark != null ) {
                remark = data.data.process_remark;
            }
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
            ];

            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            remark = $('#process_remark').val();
            // var sel = document.getElementById("is_enabled");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#is_enabled_desc').val(text);

            // var sel = document.getElementById("announce_type");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#announce_type_desc').val(text);
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        // 1.在異常處理主畫面加上司機電話號碼的欄位
        // 2.在異常處理明細上方加按鈕「推播通知」，點擊之後跳出提示訊息，可以讓客服輸入內容，下方按鈕"確定，取消"
        var btnGroup = [
            {
                btnId: "btnurltoprocess",
                btnIcon: "fa fa-clone",
                btnText: "推播通知",
                btnFunc: function () {
                    var desecription = initformdata.data.owner_name + "-" + initformdata.data.ord_no +"-"+initformdata.data.cust_name;
                    (async function getFormValues () {
                        const { value: text } = await swal({
                            title: '推播訊息',
                            html:
                            '<span>'+desecription+'<span>'+
                                '<textarea id="swal-input2" class="swal2-input" placeholder="請輸入內容" maxlength="255" style="height:200px;">'+remark+"</textarea>",
                            focusConfirm: false,
                            focusConfirm: false,
                            showCancelButton: true,
                            scrollbarPadding:true,
                            allowOutsideClick: false,
                            cancelButtonText:  '取消',
                            confirmButtonText: '確定',
                            preConfirm: () => {
                                return [
                                    document.getElementById('swal-input2').value
                                ]
                            }
                        })
                        if (text) {
                            var content = document.getElementById('swal-input2').value;
                            $.post(BASE_URL + '/' + lang + '/' + 'errorprocess' + '/sendMessage/', {
                                'id'     : mainId,
                                'beforecontent' : desecription,
                                'content': content
                            }, function(data) {
                                if (data.success) {
                                    $('#fcmGrid').jqxGrid('updatebounddata');
                                    swal('操作成功', "", "success");
                                } else {
                                    swal('操作失敗', '', "error");
                                }
                            });
                            // swal.fire(text)
                        }
                    })()
                }
            },
            //
            {
                btnId: "btnExportExcel",
                btnIcon: "fa fa-cloud-download",
                btnText: 'Excel匯出',
                btnFunc: function () {
                    $.post(BASE_URL + '/' + lang + '/' + 'errorprocessfcm' + '/excel/export', {
                        'mainId'  : mainId,
                        'fileName': "{{date('Y-m-d H:i:s')}}"+'-異常推播紀錄',
                    }, function(data){
                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                }
            },
            //
        ];
        initBtn(btnGroup);

        var col = [
            [
                {name: "id", type: "number"},
                {name: "guid", type: "string"},
                {name: "type_no", type: "string"},
                {name: "descp", type: "string"},
                {name: "created_at", type: "string"}
            ],
            [
                {text: "id", datafield: "id", width: 100, hidden: true },
                {text: "圖片", datafield: "guid", width: 150, cellsrenderer: imagerenderer},
                { 
                    text: '路徑', 
                    width: 100,
                    datafield: 'imgurl', 
                    columntype: 'button', 
                    cellsrenderer: function (row, column, value) {
                    return "點我打開圖片";
                    // return "按鈕";
                }, buttonclick: function (row,event) {
                    var rowdata = $("#imgGrid2").jqxGrid('getrowdata', row);
                    var url = "{{env('PRINTER_URL')}}"+ rowdata.guid.replace('public/', '');
                    window.open(url);
                }},
                {text: "敘述", datafield: "descp", width: 300},
                {text: "上傳時間", datafield: "created_at", width: 300}
            ]
        ];
            
        var opt = {};
        opt.gridId = "imgGrid2";
        opt.fieldData = col;
        opt.formId = "subForm8";
        opt.saveId = "Save9";
        opt.cancelId = "Cancel9";
        opt.showBoxId = "subBox8";
        opt.height = 300; 
        opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/errorprocess/imgget') }}" + '/' + ordNo ;
        opt.defaultKey = {
            'ref_no': ordNo
        };
        opt.commonBtn = false;
        opt.showtoolbar = false;

        opt.beforeSave = function (row) {
        }
        opt.afterSave = function (data) {
            $('#imgGrid2').jqxGrid('updatebounddata');
        }
        opt.beforeCancel = function() {
        }

        genDetailGrid(opt);

        //
        var col = [
            [
                {name: "id", type: "number"},
                {name: "created_by", type: "string"},
                {name: "created_at", type: "datetime"},
                {name: "content", type: "string"},
            ],
            [
                {text: "id", datafield: "id", width: 100, hidden: true },
                {text: "推播人姓名/(回覆)姓名", datafield: "created_by", width: 300},
                {text: "推播時間", datafield: "created_at", width: 300},
                {text: "內容", datafield: "content", width: 300},
            ]
        ];
            
        var opt = {};
        opt.gridId = "fcmGrid";
        opt.fieldData = col;
        opt.formId = "subForm9";
        opt.saveId = "Save10";
        opt.cancelId = "Cancel10";
        opt.showBoxId = "subBox9";
        opt.height = 300; 
        opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/errorprocess/fcmget') }}" + '/' + mainId ;
        opt.defaultKey = {
            'sys_ord_no': mainId
        };
        opt.commonBtn = false;
        opt.showtoolbar = false;

        opt.beforeSave = function (row) {
        }
        opt.afterSave = function (data) {
           
        }
        opt.beforeCancel = function() {
        }

        genDetailGrid(opt);
        //

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection