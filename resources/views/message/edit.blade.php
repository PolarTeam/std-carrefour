@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        訊息夾<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/message') }}">訊息夾</a></li>
		<li class="active">訊息夾</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="title">標題</label>
                                            <input type="text" class="form-control" id="title" name="title" disabled="disabled" readonly="true">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="content">訊息內容</label>
                                            <textarea class="form-control" id='content' rows="3" name="content" disabled="disabled" readonly="true"></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="replay_message">回復訊息</label>
                                            <textarea class="form-control" id='replay_message' rows="3" name="replay_message" disabled="disabled" readonly="true"></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
    
    
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
                
            },"wh_name");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message/" + mainId;
        formOpt.replayUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message/";
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/message" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#owner_cd').select2();
            $('#parked_models').select2();
            $('#container').select2();

        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            $("#iAdd").hide();
            $("#iEdit").hide();
            $("#iSave").hide();
            $("#iCancel").hide();
            $("#iDel").hide();
            console.log(data.data);
            $.get( formOpt.replayUrl+data.data.replay_id , function( data ) {
                console.log('replay_data');
            });
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            return false;
            // var requiredColumn = [
            //     {
            //         "column_filed":"pallet_no",
            //         "column_type":"string",
            //     },
            //     {
            //         "column_filed":"pallet_name",
            //         "column_type":"string",
            //     },
            // ];
            // var cansafe = beforesave(requiredColumn) ;
            // if(!cansafe) {
            //     swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
            //     return false;
            // }
            // return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // if(typeof formOpt.afterInit === "function") {
    //     console.log(data.data);
    //     $.get( formOpt.replayUrl+data.data.replay_id , function( data ) {
    //         console.log('replay_data');
    //     });
    // }

});
</script>
@endsection