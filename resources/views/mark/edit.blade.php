@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_mark.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/markProfile') }}">{{ trans('mod_mark.titleName') }}</a></li>
		<li class="active">{{ trans('mod_mark.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 

        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('mod_mark.cust_no') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cust_name">{{ trans('mod_mark.cust_name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cust_name" name="cust_name">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="wh_name">{{ trans('mod_mark.wh_name') }} <span style="color:red">*<span></label>
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="wh_no" name="wh_no" required="required">
                                            <input type="text" class="form-control" id="wh_name" name="wh_name" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="wh_name"
                                                    info1="{{Crypt::encrypt('mod_warehouse')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname")}}" 
                                                    info3="{{Crypt::encrypt("")}}"
                                                    info5="{{Crypt::encrypt('mod_warehouse')}}"
                                                    info4="cust_no=wh_no;cname=wh_name;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('mod_mark.type') }}</label>
                                        <input type="hidden" class="form-control" id="type_desc" name="type_desc" required="required">
                                        <select class="form-control" id="type" name="type" no-clear="Y" >
                                            @foreach($type as $row)
                                                <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                                            @endforeach	
                                        </select>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>{{ trans('mod_mark.owner_name') }}</label>
                                        <input type="hidden" class="form-control" id="owner_name" name="owner_name" required="required">
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" id="owner_cd" name="owner_cd">
                                            @foreach($owner_cd as $row)
                                                <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                            @endforeach	
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label>{{ trans('mod_mark.parked_models_name') }}</label>
                                        <input type="hidden" class="form-control" id="parked_models_name" name="parked_models_name" required="required">
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" id="parked_models" name="parked_models">
                                            @foreach($parked_models as $row)
                                                <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                                            @endforeach	
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label>{{ trans('mod_mark.container_name') }}</label>
                                        <input type="hidden" class="form-control" id="container_name" name="container_name" required="required">
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" id="container" name="container">
                                            @foreach($container as $row)
                                                <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                                            @endforeach	
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status">啟用</label>
                                        <input type="hidden" class="form-control" id="is_enabled_desc" name="is_enabled_desc" required="required">
                                        <select class="form-control" id="is_enabled" name="is_enabled" no-clear="Y" >
                                            <option value="N">否</option>
                                            <option value="Y">是</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="content">{{ trans('mod_mark.remark') }}</label>
                                        <textarea class="form-control" id='remark' rows="3" name="remark"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                    </div>
                                </div>


                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="wh_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="wh_name"]').on('click', function(){
        var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","wh_name",callBackFunc=function(data){
                console.log(data);
                
            },"wh_name");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/markProfile" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#owner_cd').select2();
            $('#parked_models').select2();
            $('#container').select2();

        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var sel = document.getElementById("is_enabled");
            var text= sel.options[sel.selectedIndex].text;
            $('#is_enabled_desc').val(text);

            var sel = document.getElementById("type");
            var text= sel.options[sel.selectedIndex].text;
            $('#type_desc').val(text);

            var requiredColumn = [
                {
                    "column_filed":"cust_no",
                    "column_type":"string",
                },
                {
                    "column_filed":"cust_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"wh_no",
                    "column_type":"string",
                },
                {
                    "column_filed":"wh_name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection