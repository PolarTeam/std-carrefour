@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('sys_area.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/sysArea') }}">{{ trans('sys_area.titleName') }}</a></li>
		<li class="active">{{ trans('sys_area.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="cntry_cd">{{ trans('sys_area.cntry_cd') }}</label>
                                        <input type="text" class="form-control" id="cntry_cd" name="cntry_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cntry_nm">{{ trans('sys_area.cntry_nm') }}</label>
                                        <input type="text" class="form-control" id="cntry_nm" name="cntry_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                {{-- <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="state_cd">{{ trans('sys_area.state_cd') }}</label>
                                        <input type="text" class="form-control" id="state_cd" name="state_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="state_nm">{{ trans('sys_area.state_nm') }}</label>
                                        <input type="text" class="form-control" id="state_nm" name="state_nm" placeholder="Enter No.">
                                    </div>
                                </div>        --}}
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="city_cd">{{ trans('sys_area.city_cd') }}</label>
                                        <input type="text" class="form-control" id="city_cd" name="city_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city_nm">{{ trans('sys_area.city_nm') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm" placeholder="Enter No.">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="dist_cd">城市代碼</label>
                                        <input type="text" class="form-control" id="dist_cd" name="dist_cd" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="dist_nm">{{ trans('sys_area.dist_nm') }}</label>
                                        <input type="text" class="form-control" id="dist_nm" name="dist_nm" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="zip_f">{{ trans('sys_area.zip_f') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="zip_f" name="zip_f" placeholder="Enter No" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zip_e">{{ trans('sys_area.zip_e') }}</label>
                                        <input type="text" class="form-control" id="zip_e" name="zip_e" placeholder="Enter No.">
                                    </div>                                   
                                </div>
                                <div class="row">
                                    {{-- <div class="form-group col-md-6">
                                        <label for="station_cd">區域代碼</label>
                                        <input type="text" class="form-control" id="station_cd" name="station_cd" placeholder="Enter No" >
                                    </div> --}}
                                    <div class="form-group col-md-6">
                                        <label for="station_nm">區域名稱 <span style="color:red">*<span></label>
                                        <select class="form-control" id="station_nm" name="station_nm">
                                            <option value="北">北</option>
                                            <option value="中">中</option>
                                            <option value="南">南</option>
                                            <option value="東">東</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sys_area.remark') }}</label>
                                            <textarea class="form-control" rows="3" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>          
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('sys_area.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('sys_area.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('sys_area.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('sys_area.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="owner_cd"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('owner_cd', "製造商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="owner_cd"]').on('click', function(){
        var check = $('#subBox input[name="owner_cd"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","owner_cd",callBackFunc=function(data){
                console.log(data);
                
            },"owner_cd");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/sysArea" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#sysArea').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            // if($('#display_name').val() == ""){
            //     document.getElementById("display_name").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // }

            // if($('#name').val() == ""){
            //     document.getElementById("name").style.backgroundColor = "FCE8E6";
            //     iserror = true;
            // }
            
            var requiredColumn = [
                {
                    "column_filed":"city_nm",
                    "column_type":"string",
                },
                {
                    "column_filed":"zip_f",
                    "column_type":"string",
                },
                {
                    "column_filed":"station_nm",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection