@extends('layout.layout')
@section('header')
<section class="content-header d-flex justify-content-between">
    <h1>
        Dashboard - 數據
        <small></small>
    </h1>
    <div>
        <input type="date" name="" id="searchDataByDay" onchange="dateChanged()" value="{{date('Y-m-d')}}">
    </div>
</section>

@endsection
@section('before_scripts')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="配送服務系統">
<!-- Meta Keyword -->
<meta name="keywords" content="DSS">
<meta name="author" content="">
<!-- CSS Global Compulsory -->
<!-- CSS Implementing Plugins -->

@endsection

@section('content')

<div class="py-2 px-3">
    <div class="row">
      <div class="col-lg-6">
          <div>
            <div class="d-flex justify-content-between">
                <button type="button" class="blue-sm-btn" id="selectWhBtn">篩選倉庫</button>
                <ul class="list-unstyled page-list" id="wh-page-list">
                    <li><button type="button" class="active" id="wh-page-1">1</button></li>
                    {{-- <li><button type="button" id="wh-page-2">2</button></li> --}}
                </ul>
            </div>
            <table>
              <tbody class="list-unstyled order-list" id="wh-content">
                @foreach($viewData['warehouse'] as $warehouserow)
                <tr>
                  <td class="order-list-title">{{$warehouserow->cname}}</td>
                  <td>
                    <div class="order-box">
                      <div class="order-item">
                        <span class="order-lable">訂單量</span>
                        <span class="order-num">{{$warehouserow->total_count}}</span>
                      </div>
                      <div class="order-item">
                        <span class="order-lable">異常數</span>
                        <span class="order-num error">{{$warehouserow->error_count}}</span>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="order-box">
                      <div class="order-item">
                        <span class="order-lable">訂單完成率</span>
                        <span class="order-num finish">{{$warehouserow->finish_parsent}}%</span>
                      </div>
                      <div class="order-item">
                        <span class="order-lable">未完成率</span>
                        <span class="order-num">{{$warehouserow->dlv_parsent}}%</span>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach


              </tbody>  
            </tbody>  
              </tbody>  
            </table>
          </div>
      </div>
      <div class="col-lg-6">
        <div>
          <div class="d-flex justify-content-between">
              <button type="button" class="blue-sm-btn" id="selectOwnerBtn">篩選貨主</button>
              <ul class="list-unstyled page-list" id="owner-page-list">
                  <li><button type="button" class="active" id="owner-page-1">1</button></li>
                  {{-- <li><button type="button" id="owner-page-2">2</button></li> --}}
              </ul>
          </div>
          <table>
            <tbody class="list-unstyled order-list" id="owner-content">
              @foreach($viewData['owner'] as $ownerrow)
              <tr>
                <td class="order-list-title">{{$ownerrow->cust_name}}</td>
                <td>
                  <div class="order-box">
                    <div class="order-item">
                      <span class="order-lable">訂單量</span>
                      <span class="order-num">{{$ownerrow->total_count}}</span>
                    </div>
                    <div class="order-item">
                      <span class="order-lable">訂單完成率</span>
                      <span class="order-num finish">{{$ownerrow->finish_parsent }}%</span>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="order-box">
                    <div class="order-item">
                      <span class="order-lable">異常數</span>
                      <span class="order-num error">{{$ownerrow->error_count}}</span>
                    </div>
                    <div class="order-item">
                      <span class="order-lable">北</span>
                      <span class="order-num mr-2">10</span>
                      <span class="order-lable">中</span>
                      <span class="order-num mr-2">10</span>
                      <span class="order-lable">南</span>
                      <span class="order-num mr-2">10</span>
                      <span class="order-lable">東</span>
                      <span class="order-num mr-2">10</span>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<style>
  .d-flex {
    display: flex;
  }
  .justify-content-between {
    justify-content: space-between;
  }
  .justify-content-end {
    justify-content: flex-end;
  }
  .align-items-end {
    align-items: flex-end;
  }
  .align-items-center {
    align-items: center;
  }
  .w-100 {
    width: 100%;
  }
  .blue-sm-btn {
    font-size: 14px;
    color: #fff;
    background: #19294A;
    padding: 4px 10px;
    border-radius: 8px;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    margin-bottom: 8px;
  }
  .page-list {
    display: flex;
  }
  .page-list button {
    background: transparent;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    font-size: 16px;
    color: #999999;
    margin-right: 16px;
  }
  .page-list .active {
    color: #f47920;
    font-weight: bold;
  }
  .order-list {
    margin-bottom: 20px;
  }
  .order-list-title {
    font-size: 16px;
    white-space: nowrap;
    margin-right: 12px;
  }
  .order-box {
    display: flex;
    align-items: center;
    background-color: #fff;
    border-radius: 8px;
    padding: 4px 0;
    margin: 0 8px 8px 0;
    flex: 1;
  }
  .order-box:first-of-type {
    margin-right: 8px;
  }
  .order-item {
    display: flex;
    align-items: center;
    padding: 4px 12px;
    border-right: 1px solid #D2D6DE;
  }
  .order-item:last-of-type {
    border-right: 0px solid;
  }
  .order-lable {
    font-size: 18px;
    margin-right: 4px;
    white-space: nowrap;
  }
  .order-num {
    font-size: 30px;
    font-weight: bold;
    white-space: nowrap;
    margin-left: auto;
  }
  .order-num.error {
    color: #E80C0C;
  }
  .order-num.finish {
    color: #11A9ED;
  }
  .sub-title {
    font-size: 20px;
    color: #444444;
    margin-bottom: 10px;
  }
  .list-detail {
    background-color: #fff;
    border-radius: 0 8px 8px 0;
    padding: 16px;
    margin-bottom: 8px;
    font-size: 16px;
    flex: 1;
  }
  .list-detail .list-detail-title {
    font-size: 20px;
    color: #000;
    margin-bottom: 8px;
  }
  .error-content {
    font-size: 16px;
    color: #535353;
  }
  .error-content-date {
    font-size: 16px;
    color: #999DA8;
  }
  .list-status {
    width: 16px;
    height: auto;
    margin-bottom: 8px;
    border-radius: 8px 0 0 8px;
  }
  .list-status.un-do {
    background: #E73000;
  }
  .list-status.doing {
    background: #ffcd56;
  }
  .list-status.finish {
    background: #3bb300;
  }

  .orange-btn {
    color: #F47920;
    border: 1px solid #F47920;
    border-radius: 12px;
    background: #fff;
    font-size: 16px;
    padding: 12px 24px;
    margin: 0 1px 1px 3px;
  }
  .swal2-content .title {
      font-size: 20px;
      color: #333333;
      font-weight: 400;
  }
  .swal2-content .sub-title {
      font-size: 16px;
      color: #999999;
      font-weight: 400;
  }
  .swal2-content .sub-title .num {
      color: #3079ae;
  }
  .swal2-content label {
      font-size: 14px;
      color: #666666;
      font-weight: 400;
  }
  .swal2-content input {
      font-size: 14px;
      border: 1px solid #efeff1;
      padding: 12px;
      color: #333333;
      border-radius: 4px;
      width: 100%;
  }
  .swal2-content .autocomplet {
      width: 100%;
  }
  .swal2-content .autocomplet .search-btn {
      border: 1px solid #efeff1;
      color: #333333;
      border-radius: 4px;
      width: 44px;
      height: 44px;
      background: #fff;
      font-size: 14px;
      display: inline-block;
  }
  .swal2-content .autocomplet input {
      width: calc(100% - 49px);
      display: inline-block;
  }
  .swal2-content .select {
      position: relative;
  }
  .swal2-content .select select {
      font-size: 14px;
      border: 1px solid #efeff1;
      padding: 12px;
      color: transparent;
      border-radius: 4px;
      width: 100%;
      background: transparent;
      position: relative;
      z-index: 3;
  }
  .swal2-content .select::before {
      content: "\f107";
      font-family: "FontAwesome";
      font-weight:400;
      display: block;
      position: absolute;
      top: 50%;
      right: 20px;
      color: #c5c8ce;
      font-size: 20px;
      transform: translateY(-50%);
      z-index: 2;
  }
  .swal2-content .select p {
      position: absolute;
      top: 50%;
      left: 0;
      transform: translateY(-50%);
      color: #333333;
      font-size: 14px;
      z-index: 1;
      border: 1px solid #efeff1;
      padding: 12px;
      border-radius: 4px;
      background: #fff;
      height: 100%;
      width: 100%;
  }
  .swal2-content .border-bottom {
    padding-bottom: 16px;
    margin-bottom: 16px;
    border-bottom: 1px solid #D2D6DE;
  }
  .swal2-content .can-remove-list {
    text-align: start;
  }
  .swal2-content .can-remove-list li {
    color: #444444;
    font-size: 16px;
    margin-bottom: 24px;
  }
  .swal2-content .can-remove-list button {
    background: transparent;
    border: 0px solid transparent;
    outline: 0px solid transparent;
    color: #EA2020;
    font-size: 16px;
    display: inline-block;
    margin-right: 16px;
  }
  .mb-3 {
    margin-bottom: 16px;
  }
  .mr-2 {
    margin-right: 8px;
  }
</style>

@endsection

@section('after_scripts')
<script>
  // 換頁輪播
  let whAllData = JSON.parse(('{{$viewData["warehouse"]}}').split('&quot;').join('"'));
  let whData = JSON.parse(JSON.stringify(whAllData));
  let allowner = JSON.parse(('{{$viewData["allowner"]}}').split('&quot;').join('"'));
  let allwh = JSON.parse(('{{$viewData["allwh"]}}').split('&quot;').join('"'));
  allwh.forEach(wh =>{
    wh.dlv_parsent = 100
    wh.error_count = 0
    wh.finish_parsent = 0
    wh.total_count = 0
  })
  allowner.forEach(owner =>{
    owner.errorNumC = 0
    owner.errorNumE = 0
    owner.errorNumN = 0
    owner.errorNumS = 0
    owner.error_count = 0
    owner.finish_parsent =0
    owner.total_count = 0
  })
  let ownerAllData = [];
  let whInPageNums = 1; // 倉庫頁碼
  let ownerInPageNums = 1; // 貨主頁碼
  let pageSize = 10;  // 一頁幾筆
  let initowner = "";
  let isfirst = "Y";
  var lastentrydata = "";
  function dateChanged() {
      var input = document.getElementById("searchDataByDay");
      console.log("Selected date: " + input.value);
      $.ajax({
          type: "post", //  OR POST WHATEVER...
          dataType : 'json', // 預期從server接收的資料型態
          contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
          url: BASE_URL + '/zh-TW/dashboardTextbydate',
          data:JSON.stringify({
              "date" : input.value,
              "wharray" : whData,
              "owenerarray" : ownerData,
          }),
          success: function(data) {
              console.log(data);


              var datacount = 0;
              for (i = 0; i < data.data.allowner.length; i++) {
                datacount += data.data.allowner[i].total_count;
              }
              if( datacount == 0 && lastentrydata != input.value) {
                alert(`${input.value} 目前尚無資料，請選擇其他日期`);
                lastentrydata = input.value;
                return
              }

              ownerAllData = [];
              for (i = 0; i < data.data.owner.length; i++) {
                //
                ownerAllData.push({
                      errorNumN     : data.data.owner[i].errorNumN,
                      errorNumC     : data.data.owner[i].errorNumC,
                      errorNumS     : data.data.owner[i].errorNumS,
                      errorNumE     : data.data.owner[i].errorNumE,
                      cust_no       : data.data.owner[i].cust_no,
                      cust_name     : data.data.owner[i].cust_name,
                      total_count   : data.data.owner[i].total_count,
                      finish_parsent: data.data.owner[i].finish_parsent,
                      error_count   : data.data.owner[i].error_count
                  })
                //
              }

              whAllData = data.data.warehouse || [];
              whData = JSON.parse(JSON.stringify(whAllData));
              ownerData = JSON.parse(JSON.stringify(ownerAllData));
              setPageBtnEventListener('wh', whData, pageSize);
              endWhCarouselEvent();
              starWhCarouselEvent();
              chengePageEvent('wh', whData, whInPageNums, pageSize);

              setPageBtnEventListener('owner', ownerData, pageSize);
              endOwnerCarouselEvent();
              starOwnerCarouselEvent();
              chengePageEvent('owner', ownerData, ownerInPageNums, pageSize);
          }
      });
  }


  // 抓 ownerAllData 資料
  @for ($i = 0; $i < count($viewData["owner"]); $i++) {
    ownerAllData.push({
      errorNumN: '{{$viewData["owner"][$i]->errorNumN}}',
      errorNumC: '{{$viewData["owner"][$i]->errorNumC}}',
      errorNumS: '{{$viewData["owner"][$i]->errorNumS}}',
      errorNumE: '{{$viewData["owner"][$i]->errorNumE}}',
      cust_name: '{{$viewData["owner"][$i]->cust_name}}',
      cust_no: '{{$viewData["owner"][$i]->cust_no}}',
      total_count: '{{$viewData["owner"][$i]->total_count}}',
      finish_parsent: '{{$viewData["owner"][$i]->finish_parsent}}',
      error_count: '{{$viewData["owner"][$i]->error_count}}'
    })
  }
  @endfor
  var ownerArray = [];
  var whArray = [];
  // console.log(ownerAllData);
  let ownerData = JSON.parse(JSON.stringify(ownerAllData));
  // 換頁
  function chengePageEvent(type, data, pageNums, pageSize) {
      let el = document.getElementById(`${type}-content`);
      let content = document.getElementById(`${type}-content`).outerHTML;
      let contentList = data.filter((item, index)=> {
        return (index > (pageSize * (pageNums - 1) - 1) && index < (pageNums * pageSize));
      });
      let contentHtml = '';
      switch (type) {
        case 'wh':
          whArray = [];
          contentList.forEach((item, index) => {
            whArray.push(item.cust_no);
            contentHtml += `
              <tr>
                <td class="order-list-title">${item.cname}</td>
                <td>
                  <div class="order-box">
                    <div class="order-item" id="${type}-order-num-${index}">
                      <span class="order-lable">訂單量</span>
                      <span class="order-num">${item.total_count}</span>
                    </div>
                    <div class="order-item" id="${type}-order-error-${index}">
                      <span class="order-lable">異常數</span>
                      <span class="order-num error">${item.error_count}</span>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="order-box">
                    <div class="order-item" id="${type}-order-finish-${index}">
                      <span class="order-lable">訂單完成率</span>
                      <span class="order-num finish">${item.finish_parsent}%</span>
                    </div>
                    <div class="order-item" id="${type}-order-un-${index}">
                      <span class="order-lable">未完成率</span>
                      <span class="order-num">${item.dlv_parsent}%</span>
                    </div>
                  </div>
                </td>
              </tr>`;
          });
          break;
          case 'owner':
          ownerArray = [];
          contentList.forEach((item, index) => {
            ownerArray.push(item.cust_no);
            contentHtml += `
            <tr>
              <td class="order-list-title">${item.cust_name}(${item.cust_no})</td>
              <td>
                <div class="order-box">
                  <div class="order-item" id="${type}-order-num-${index}">
                    <span class="order-lable">訂單量</span>
                    <span class="order-num">${item.total_count}</span>
                  </div>
                  <div class="order-item" id="${type}-order-finish-${index}">
                    <span class="order-lable">訂單完成率</span>
                    <span class="order-num finish">${item.finish_parsent}%</span>
                  </div>
                </div>
              </td>
              <td>
                <div class="order-box">
                  <div class="order-item" id="${type}-order-error-${index}">
                    <span class="order-lable">異常數</span>
                    <span class="order-num error">${item.error_count}</span>
                  </div>
                  <div class="order-item" id="${type}-order-where-${index}">
                    <span class="order-lable">北</span>
                    <span class="order-num mr-2">${item.errorNumN}</span>
                    <span class="order-lable">中</span>
                    <span class="order-num mr-2">${item.errorNumC}</span>
                    <span class="order-lable">南</span>
                    <span class="order-num mr-2">${item.errorNumS}</span>
                    <span class="order-lable">東</span>
                    <span class="order-num mr-2">${item.errorNumE}</span>
                  </div>
                </div>
              </td>
            </tr>`;
          });
          break;
      };
      // console.log(contentHtml);
      el.innerHTML = contentHtml;
      // 寬度調整
      switch (type) {
        case 'wh':
          let whOrderNumArr = [];
          let whOrderErrorArr = [];
          let whOrderFinishArr = [];
          let whOrderUnArr = [];
          contentList.forEach((item, index) => {
            whOrderNumArr.push(document.getElementById(`${type}-order-num-${index}`).clientWidth);
            whOrderErrorArr.push(document.getElementById(`${type}-order-error-${index}`).clientWidth);
            whOrderFinishArr.push(document.getElementById(`${type}-order-finish-${index}`).clientWidth);
            whOrderUnArr.push(document.getElementById(`${type}-order-un-${index}`).clientWidth);
          })
          let whMaxWidthArr = [Math.max(...whOrderNumArr), Math.max(...whOrderErrorArr), Math.max(...whOrderFinishArr), Math.max(...whOrderUnArr)];
          contentList.forEach((item, index) => {
            document.getElementById(`${type}-order-num-${index}`).style.width = whMaxWidthArr[0] + 'px';
            document.getElementById(`${type}-order-error-${index}`).style.width = whMaxWidthArr[1] + 'px';
            document.getElementById(`${type}-order-finish-${index}`).style.width = whMaxWidthArr[2] + 'px';
            document.getElementById(`${type}-order-un-${index}`).style.width = whMaxWidthArr[3] + 'px';
          })
          break;
        case 'owner':
          let ownerOrderNumArr = [];
          let ownerOrderErrorArr = [];
          let ownerOrderFinishArr = [];
          let ownerOrderWhereArr = [];
          contentList.forEach((item, index) => {
            ownerOrderNumArr.push(document.getElementById(`${type}-order-num-${index}`).clientWidth);
            ownerOrderErrorArr.push(document.getElementById(`${type}-order-error-${index}`).clientWidth);
            ownerOrderFinishArr.push(document.getElementById(`${type}-order-finish-${index}`).clientWidth);
            ownerOrderWhereArr.push(document.getElementById(`${type}-order-where-${index}`).clientWidth);
          })
          let ownerMaxWidthArr = [Math.max(...ownerOrderNumArr), Math.max(...ownerOrderErrorArr), Math.max(...ownerOrderFinishArr), Math.max(...ownerOrderWhereArr)];
          contentList.forEach((item, index) => {
            document.getElementById(`${type}-order-num-${index}`).style.width = ownerMaxWidthArr[0] + 'px';
            document.getElementById(`${type}-order-error-${index}`).style.width = ownerMaxWidthArr[1] + 'px';
            document.getElementById(`${type}-order-finish-${index}`).style.width = ownerMaxWidthArr[2] + 'px';
            document.getElementById(`${type}-order-where-${index}`).style.width = ownerMaxWidthArr[3] + 'px';
          })
          break;
      };
      // 頁數 focuse
      let totalCont = Math.ceil(data.length / pageSize);
      switch (type) {
        case 'wh':
          whInPageNums = pageNums;
          break;
        case 'owner':
          ownerInPageNums = pageNums;
          break;
      }
      for (let i = 0; i < totalCont; i++) {
        document.getElementById(`${type}-page-${i + 1}`).classList.remove('active');
      };
      if (!document.getElementById(`${type}-page-${pageNums}`)) {
        return
      }
      document.getElementById(`${type}-page-${pageNums}`).classList.add('active');
  };
  // 點擊頁碼換頁
  function setPageBtnEventListener(type, data, pageSize) {
    let totalCont = Math.ceil(data.length / pageSize);
    document.getElementById(`${type}-page-list`).innerHTML = '';
    // console.log(totalCont);
    for (let i = 0; i < totalCont; i++) {
      let newli = document.createElement('li');
      newli.innerHTML = `<button type="button" class="active" id="${type}-page-${i + 1}">${i + 1}</button>`
      document.getElementById(`${type}-page-list`).appendChild(newli);
      // console.log(`${type}-page-${i + 1}`);
      document.getElementById(`${type}-page-${i + 1}`).addEventListener('click', function() {
        chengePageEvent(type, data, i + 1, pageSize);
        switch (type) {
          case 'wh':
            endWhCarouselEvent();
            starWhCarouselEvent();
            break;
          case 'owner':
            endOwnerCarouselEvent();
            starOwnerCarouselEvent();
            break;
        }
      });
    };
  }
  // 輪播換頁
  function carouselEvent (type, data) {
    let totalCont = Math.ceil(data.length / pageSize);
    switch (type) {
      case 'wh':
        whInPageNums += 1;
        if (whInPageNums > totalCont) {
          whInPageNums = 1;
        };
        chengePageEvent(type, whData, whInPageNums, pageSize);
        break;
      case 'owner':
        ownerInPageNums += 1;
        if (ownerInPageNums > totalCont) {
          ownerInPageNums = 1;
        };
        chengePageEvent(type, ownerData, ownerInPageNums, pageSize);
        break;
    }
  }
  var refreshtime    = parseInt("{{$viewData['refreshtime']}}");
  // 倉庫輪播換頁
  let whChengePageTimeout = refreshtime;
  let whCarousel;
  function starWhCarouselEvent () {
    whCarousel = setInterval(() => {
      carouselEvent('wh', whData);
    }, whChengePageTimeout);
  }
  function endWhCarouselEvent () {
    clearInterval(whCarousel);
  }
  // 貨主輪播換頁
  let ownerChengePageTimeout = refreshtime;
  let ownerCarousel;
  function starOwnerCarouselEvent () {
    ownerCarousel = setInterval(() => {
      carouselEvent('owner', ownerData);
    }, ownerChengePageTimeout);
  }
  function endOwnerCarouselEvent () {
    clearInterval(ownerCarousel);
  }
  //
  setPageBtnEventListener('wh', whData, pageSize);
  chengePageEvent('wh', whData, whInPageNums, pageSize);
  starWhCarouselEvent();
  setPageBtnEventListener('owner', ownerData, pageSize);
  chengePageEvent('owner', ownerData, ownerInPageNums, pageSize);
  starOwnerCarouselEvent();
  // 篩選倉庫
  let selectWhBtn = document.getElementById('selectWhBtn');
  selectWhBtn.addEventListener('click', function() {
  let whList = '';
  let whoption = '';
  whData.forEach((item, index) => {
    whList += `
    <li id="li-wh-${index}">
      <button type='button' id="wh-${index}" data-key="${item.cname}"><i class='fa fa-times' aria-hidden='true'></i></button>
      <span>${item.cname}</span>
    </li>`
  });
  allwh.forEach((item, index) => {
    whoption += ` <option value='${item.cust_no}'>${item.cname}</option>`;
  });
  let ordWhHtml = 
    "<div class='col-md-12' style='text-align: left';><label class='title'>篩選倉庫</label></div>\
    <div class='col-md-12'>\
      <div class='row d-flex align-items-end border-bottom'>\
        <div class='col-md-8' style='text-align: left';><label>倉庫</label>\
            <div class='form-group'>\
                <input type='hidden' name='check_wh_name' id = 'check_wh_name' class='form-control'>\
                <select class='form-control select2' id='check_wh' name='check_wh' multiple='multiple'>\
                    "+whoption+
                "</select>\
            </div>\
        </div>\
        <div class='col-md-6 d-flex align-items-end'>\
          <button type='button' class='orange-btn' id='whAddBottom'>新增</button>\
          <button type='button' class='orange-btn' id='whClearAllBottom'>全部取消</button>\
        </div>\
      </div>\
    </div>\
    <div class='col-md-12'>\
      <ul class='list-unstyled can-remove-list' id='can-remove-list'>" + whList + "</ul>\
    </div>"
    ;
    swal({
      title            : '',
      icon             : 'info',
      html             : ordWhHtml,
      width            : '40%',
      showCloseButton  : true,
      showCancelButton : false,
      focusConfirm     : false,
      showConfirmButton: false,
      allowOutsideClick: false,
      closeOnClickOutside: false,
      onOpen: function () {
          $('#check_wh').select2();
      },
      }).then((result) => {
        console.log('confirm do status');
        dateChanged();
    });
    // 監聽刪除按鈕
    whData.forEach((item, index) => {
      setRemoveBtnEvent(item, index);
    });
   //  監聽新增按鈕
   document.getElementById('whAddBottom').addEventListener('click', function() {
      let selectArr = $("#check_wh").val() || [];
      whforsql = $("#check_wh").val();
      let index = whData.length -1
      selectArr.forEach((item) => {
          let hasIndex = whData.findIndex((itm) => item === itm.cname);
          let allDataIndex = allwh.findIndex((itm) => item === itm.cname);
          if (hasIndex === -1) {
              whData.push(allwh[allDataIndex]);
              index++
              let content = document.createElement('li')
              content.innerHTML = `
              <li id="li-wh-${index}">
                <button type='button' id="wh-${index}" data-key="${item}"><i class='fa fa-times' aria-hidden='true'></i></button>
                <span>${item}</span>
              </li>`
              document.getElementById(`can-remove-list`).appendChild(content)
              setRemoveBtnEvent(allwh[allDataIndex], index);
          }
      });
      setPageBtnEventListener('wh', whData, pageSize);
      endWhCarouselEvent();
      starWhCarouselEvent();
      chengePageEvent('wh', whData, whInPageNums, pageSize);
      $("#check_wh").val('');
      $("#check_wh").trigger('change');
      dateChanged();
    })
    // 監聽全部取消按鈕
    document.getElementById('whClearAllBottom').addEventListener('click', function() {
        var canRemoveList = document.getElementById('can-remove-list').children;
        let canRemoveListLength = canRemoveList.length;
        for (let i = 0; i < canRemoveListLength; i++) {
          document.getElementById(`li-wh-${i}`).style.display = 'none';

          let removeIndex = i
          whData.splice(0, 1);
          setPageBtnEventListener('wh', whData, pageSize);
          endWhCarouselEvent();
          starWhCarouselEvent();
          chengePageEvent('wh', whData, whInPageNums, pageSize);
        }
        dateChanged();
    })
    // 監聽刪除按鈕 function
    function setRemoveBtnEvent(item, index) {
      document.getElementById(`wh-${index}`).addEventListener('click', function() {
          document.getElementById(`li-wh-${index}`).style.display = 'none';
          let removeIndex = whData.findIndex((itm) => item.cname === itm.cname);
          whData.splice(removeIndex, 1);
          // console.log(whData);
          setPageBtnEventListener('wh', whData, pageSize);
          endWhCarouselEvent();
          starWhCarouselEvent();
          chengePageEvent('wh', whData, whInPageNums, pageSize);
      })
    }
  });
  // 篩選貨主
  let selectOwnerBtn = document.getElementById('selectOwnerBtn');
  selectOwnerBtn.addEventListener('click', function() {
  let ownerList = '';
  let owneroption = '';
  ownerData.forEach((item, index) => {
    ownerList += `
    <li id="li-owner-${index}">
      <button type='button' id="owner-${index}" data-key="${item.cust_name}"><i class='fa fa-times' aria-hidden='true'></i></button>
      <span>${item.cust_name}(${item.cust_no})</span>
    </li>`;
  });
  allowner.forEach((item, index) => {
    owneroption += ` <option value='${item.cust_name}'>${item.cust_name}(${item.cust_no})</option>`;
  });
  let ordOwnerHtml = 
    "<div class='col-md-12' style='text-align: left';><label class='title'>篩選貨主</label></div>\
    <div class='col-md-12'>\
      <div class='row d-flex align-items-end border-bottom'>\
        <div class='col-md-8' style='text-align: left';><label>貨主</label>\
            <div class='form-group'>\
                <input type='hidden' name='check_owner_name' id = 'check_owner_name' class='form-control'>\
                <select class='form-control select2' id='check_owner' name='check_owner' multiple='multiple'>\
                    "+owneroption+
                    "</select>\
            </div>\
        </div>\
        <div class='col-md-4 d-flex align-items-flex-end'>\
          <button type='button' class='orange-btn' id='ownerAddBottom'>新增</button>\
          <button type='button' class='orange-btn' id='ownerClearAllBottom'>全部取消</button>\
        </div>\
      </div>\
    </div>\
    <div class='col-md-12'>\
      <ul class='list-unstyled can-remove-list' id='can-remove-list'>" + ownerList + "</ul>\
    </div>"
    ;
    swal({
      title            : '',
      icon             : 'info',
      html             : ordOwnerHtml,
      width            : '40%',
      showCloseButton  : true,
      showCancelButton : false,
      focusConfirm     : false,
      showConfirmButton: false,
      allowOutsideClick: false,
      closeOnClickOutside: false,
      onOpen: function () {
          $('#check_owner').select2();
      },
      }).then((result) => {
        console.log('confirm do status');
        dateChanged();
    });
    // 監聽刪除按鈕
    ownerData.forEach((item, index) => {
      setRemoveBtnEvent(item, index);
    });
    //  監聽新增按鈕
    document.getElementById('ownerAddBottom').addEventListener('click', function() {
      // console.log($("#check_owner").val());
      let selectArr = $("#check_owner").val() || [];
      ownerforsql = $("#check_owner").val();
      let index = ownerData.length -1
      selectArr.forEach((item) => {
          let hasIndex = ownerData.findIndex((itm) => item === itm.cust_name);
          let allDataIndex = allowner.findIndex((itm) => item === itm.cust_name);
          if (hasIndex === -1) {
              ownerData.push(allowner[allDataIndex]);
              index++
              ownerArray.push(item);
              let content = document.createElement('li')
              content.innerHTML = `
              <li id="li-owner-${index}">
                <button type='button' id="owner-${index}" data-key="${item}"><i class='fa fa-times' aria-hidden='true'></i></button>
                <span>${item}(${allowner[allDataIndex].owner_cd})</span>
              </li>`
              document.getElementById(`can-remove-list`).appendChild(content)
              setRemoveBtnEvent(allowner[allDataIndex], index);
          }

      });
      setPageBtnEventListener('owner', ownerData, pageSize);
      endOwnerCarouselEvent();
      starOwnerCarouselEvent();
      chengePageEvent('owner', ownerData, ownerInPageNums, pageSize);
      $("#check_owner").val('');
      $("#check_owner").trigger('change');
      dateChanged();
    })
    // 監聽刪除按鈕 function
    function setRemoveBtnEvent(item, index) {
      document.getElementById(`owner-${index}`).addEventListener('click', function() {
          document.getElementById(`li-owner-${index}`).style.display = 'none';
          let removeIndex = ownerData.findIndex((itm) => item.cust_name === itm.cust_name);
          ownerData.splice(removeIndex, 1);
          // console.log(ownerData);
          setPageBtnEventListener('owner', ownerData, pageSize);
          endWhCarouselEvent();
          starWhCarouselEvent();
          chengePageEvent('owner', ownerData, ownerInPageNums, pageSize);
      })
    }
    // 監聽全部取消按鈕
    document.getElementById('ownerClearAllBottom').addEventListener('click', function() {

        var canRemoveList = document.getElementById('can-remove-list').children;
        let canRemoveListLength = canRemoveList.length;
        for (let i = 0; i < canRemoveListLength; i++) {
          document.getElementById(`li-owner-${i}`).style.display = 'none';

          let removeIndex = i
          ownerData.splice(removeIndex, canRemoveListLength);
          setPageBtnEventListener('owner', ownerData, pageSize);
          endWhCarouselEvent();
          starWhCarouselEvent();
          chengePageEvent('owner', ownerData, ownerInPageNums, pageSize);
        }
        dateChanged();
    })

  });
  var count = 0;
  dashboardWebsocket();
  function dashboardWebsocket () {
      let wsURL = 'wss://dss-ws.target-ai.com:9599/websocket/getConnect/groupId_Dsvdevupdatedatsbboard';
      let ws = new WebSocket(wsURL); // 建立連線
      ws.onopen = function(e) {
          
          // websocketonopen(e);
          // console.log('ws 連線成功~~');
      };
      ws.error = function(error) {
          // websocketonerror(error);
          // console.error('ws 連線失敗', error);
      };
      ws.onmessage = function(e) {
          if(count >0 ) {
              dateChanged();
          }
          console.log('dashboardWebsocket');
          count ++;
      };
      ws.onclose = function(e) {

      };
  }
  function websocketsend (data) {
      // 前端丟資料
      console.log('send datanick', data);
  }
  function websocketclose () {
      console.log('ws 關閉連線nick');
  }
</script>
@endsection