@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('permissions.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/permission') }}">{{ trans('permissions.titleName') }}</a></li>
		<li class="active">{{ trans('permissions.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form permission="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('permissions.name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="display_name">{{ trans('permissions.display_name') }}</label>
                                        <input type="text" class="form-control" id="display_name" name="display_name" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="type">{{ trans('permissions.type') }}</label>
                                        <select type="text" class="form-control" id="type" name="type">
                                            <option value="web">WEB</option>
                                            <option value="app">APP</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="sorted">{{ trans('permissions.sorted') }}</label>
                                        <input type="number" min="0" class="form-control" id="sorted" name="sorted" required="required">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="color">{{ trans('permissions.color') }}</label>
                                        <input type="color" class="form-control" id="color" name="color" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="level">{{ trans('permissions.level') }}</label>
                                        <input type="number" class="form-control" id="level" name="level" required="required">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                
                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/permission" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#permission').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            if($('#display_name').val() == ""){
                document.getElementById("display_name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if($('#name').val() == ""){
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            var requiredColumn = [
                {
                    "column_filed":"display_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection