@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_mark_setting.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/marksetting') }}">{{ trans('mod_mark_setting.titleName') }}</a></li>
		<li class="active">{{ trans('mod_mark_setting.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="warehouse_name">{{ trans('mod_mark_setting.warehouse_name') }} <span style="color:red">*<span></label>
                                            <div class="input-group input-group-sm">
                                                <input type="hidden" class="form-control" id="warehouse_id" name="warehouse_id" required="required">
                                                <input type="text" class="form-control" id="warehouse_name" name="warehouse_name" required="required">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-flat lookup" btnname="warehouse_name"
                                                        info1="{{Crypt::encrypt('mod_warehouse')}}" 
                                                        info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname")}}" 
                                                        info3="{{Crypt::encrypt(" dc_id is not null ")}}"
                                                        info5="{{Crypt::encrypt('mod_warehouse')}}"
                                                        info4="cust_no=warehouse_id;cname=warehouse_name;" triggerfunc="" selectionmode="singlerow">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="status">{{ trans('mod_mark_setting.car_type_desc') }} <span style="color:red">*<span></label>
                                            <input type="hidden" name="car_type_desc" id = "car_type_desc" class="form-control">
                                            <select class="form-control select2" id="car_type" name="car_type">
                                                <option value="大車">大車</option>
                                                <option value="小車">小車</option>
                                                <option value="貨櫃">貨櫃</option>
                                            </select>
                                        </div>
                                        {{-- <div class="form-group col-md-3">
                                            <label for="car_type_desc">{{ trans('mod_mark_setting.car_type_desc') }}</label>
                                            <input type="text" class="form-control" id="car_type_desc" name="car_type_desc">
                                        </div> --}}
                                        <div class="form-group col-md-3">
                                            <label for="action">{{ trans('mod_mark_setting.action') }} <span style="color:red">*<span></label>
                                            <select class="form-control select2" id="action" name="action">
                                                <option value="人工櫃">人工櫃</option>
                                                <option value="棧板櫃">棧板櫃</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="con_type">{{ trans('mod_mark_setting.con_type') }}</label>
                                            <select class="form-control select2" id="con_type" name="con_type">
                                                <option value="20尺">20尺</option>
                                                <option value="40尺">40尺</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-md-3">
                                            <label for="box_num">{{ trans('mod_mark_setting.box_num') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="box_num" name="box_num">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="box_num_end">{{ trans('mod_mark_setting.box_num_end') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="box_num_end" name="box_num_end">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="items_num">{{ trans('mod_mark_setting.items_num') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="items_num" name="items_num">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="items_num_end">{{ trans('mod_mark_setting.items_num_end') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="items_num_end" name="items_num_end">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="port_type">{{ trans('mod_mark_setting.port_type') }} <span style="color:red">*<span></label>
                                            <select class="form-control select2" id="port_type" name="port_type">
                                                <option value="進貨">進貨</option>
                                                <option value="出貨">出貨</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="process_time">{{ trans('mod_mark_setting.process_time') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="process_time" name="process_time">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="trans_time">{{ trans('mod_mark_setting.trans_time') }} <span style="color:red">*<span></label>
                                            <input type="text" class="form-control" id="trans_time" name="trans_time">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="start_time">{{ trans('mod_mark_setting.start_time') }}</label>
                                            <input type="text" class="form-control" id="start_time" name="start_time">
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                            <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                            <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                            <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                        </div>
                                    </div>
    
    
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
    
                                </div>
                            </form>
                        </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="warehouse_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('warehouse_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="warehouse_name"]').on('click', function(){
        var check = $('#subBox input[name="warehouse_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","warehouse_name",callBackFunc=function(data){
                console.log(data);
                
            },"warehouse_name");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/marksetting" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');

        };
        formOpt.addFunc = function() {
            $("#port_type").val('進貨');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                {
                    "column_filed":"warehouse_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"action",
                    "column_type":"string",
                },
                {
                    "column_filed":"car_type",
                    "column_type":"string",
                },
                {
                    "column_filed":"box_num",
                    "column_type":"string",
                },
                {
                    "column_filed":"box_num_end",
                    "column_type":"string",
                },
                {
                    "column_filed":"items_num",
                    "column_type":"string",
                },
                {
                    "column_filed":"items_num_end",
                    "column_type":"string",
                },
                {
                    "column_filed":"process_time",
                    "column_type":"string",
                },
                {
                    "column_filed":"trans_time",
                    "column_type":"string",
                },
                {
                    "column_filed":"port_type",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            $('#car_type_desc').val($("#car_type option:selected").text());
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection