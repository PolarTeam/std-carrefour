@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_driver_checkin.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_driver_checkin') }}">{{ trans('mod_driver_checkin.titleName') }}</a></li>
		<li class="active">{{ trans('mod_driver_checkin.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="announce_type">{{ trans('mod_driver_checkin.job_type_desc') }}</label>
                                        <input type="hidden" class="form-control" id="job_type_desc" name="job_type_desc" >
                                        <select class="form-control" id="job_type" name="job_type" disabled="disabled" readonly="true">
                                            <option value="出貨">出貨</option>
                                            <option value="進貨">進貨</option>
                                            <option value="取空櫃">取空櫃</option>
                                            <option value="換櫃">換櫃</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="cust_ord_no">{{ trans('mod_driver_checkin.cust_ord_no') }}</label>
                                        <input type="text" class="form-control" id="cust_ord_no"  name="cust_ord_no" disabled="disabled" readonly="true">
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                        <label for="car_no">{{ trans('mod_driver_checkin.car_no') }}</label>
                                        <input type="text" class="form-control" id="car_no"  name="car_no" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="car_type_desc">{{ trans('mod_driver_checkin.car_type_desc') }}</label>
                                        <input type="text" class="form-control" id="car_type_desc"  name="car_type_desc" disabled="disabled" readonly="true">
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('mod_driver_checkin.phone') }}</label>
                                        <input type="text" class="form-control" id="phone"  name="phone" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="driver_name">{{ trans('mod_driver_checkin.driver_name') }}</label>
                                        <input type="text" class="form-control" id="driver_name"  name="driver_name" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="con_type_desc">{{ trans('mod_driver_checkin.con_type_desc') }}</label>
                                        <input type="text" class="form-control" id="con_type_desc"  name="con_type_desc" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="con_id">{{ trans('mod_driver_checkin.con_id') }}</label>
                                        <input type="text" class="form-control" id="con_id"  name="con_id" disabled="disabled" readonly="true">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="unloading_type_desc">{{ trans('mod_driver_checkin.unloading_type_desc') }}</label>
                                        <input type="text" class="form-control" id="unloading_type_desc"  name="unloading_type_desc" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="mark_no">{{ trans('mod_driver_checkin.mark_no') }}</label>
                                        {{-- <input type="text" class="form-control" id="mark_no"  name="mark_no"> --}}
                                        
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="mark_no" name="mark_no" required="required">
                                            <input type="text" class="form-control" id="mark_name" name="mark_name" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="mark_no"
                                                    info1="{{Crypt::encrypt('mod_mark')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cust_name,cust_no,cust_name")}}" 
                                                    info3="{{Crypt::encrypt("")}}"
                                                    info5="{{Crypt::encrypt('mod_mark')}}"
                                                    info4="cust_no=mark_no;cust_name=mark_name;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="warehouse">{{ trans('mod_driver_checkin.warehouse') }}</label>
                                        <input type="text" class="form-control" id="warehouse"  name="warehouse" disabled="disabled" readonly="true">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="status_desc">{{ trans('mod_driver_checkin.status_desc') }}</label>
                                        <input type="text" class="form-control" id="status_desc"  name="status_desc" disabled="disabled" readonly="true">
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <label for="entry_time">{{ trans('mod_driver_checkin.entry_time') }}</label>
                                        <input type="text" class="form-control" id="entry_time"  name="entry_time" disabled="disabled" readonly="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="leave_time">{{ trans('mod_driver_checkin.leave_time') }}</label>
                                        <input type="text" class="form-control" id="leave_time"  name="leave_time" disabled="disabled" readonly="true">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="remark">{{ trans('mod_driver_checkin.remark') }}</label>
                                        <input type="text" class="form-control" id="remark"  name="remark" disabled="disabled" readonly="true">
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-3">
                                      <label for="created_by">{{ trans('mod_driver_checkin.created_by') }}</label>
                                      <input type="text" class="form-control" id="created_by" name="created_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="created_at">{{ trans('mod_driver_checkin.created_at') }}</label>
                                      <input type="text" class="form-control" id="created_at" name="created_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_by">{{ trans('mod_driver_checkin.updated_by') }}</label>
                                      <input type="text" class="form-control" id="updated_by" name="updated_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_at">{{ trans('mod_driver_checkin.updated_at') }}</label>
                                      <input type="text" class="form-control" id="updated_at" name="updated_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                              </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_8" data-toggle="tab" aria-expanded="false">圖片資訊</a></li>
                {{-- <li class=""><a href="#tab_12" data-toggle="tab" aria-expanded="false">棧板資訊</a></li> --}}
            </ul>
            <div class="tab-content">
    
                <div class="tab-pane active" id="tab_8">
                    <div class="box box-primary" id="subBox8" style="display:none">
                        <form method="POST" accept-charset="UTF-8" id="subForm8" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="guid">圖片</label>
                                                <input type="file" name="guid" id="guid"> 
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="status">敘述</label>
                                                <select class="form-control" id="type_no" name="type_no">
                                                    <option value="error">異常回報</option>
                                                    <option value="normal">一般回報</option>
                                                    <option value="upstairs">上樓</option>
                                                    <option value="downpoint">下點</option>
                                                    <option value="pickdlv">提配單</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                        <button type="button" class="btn btn-sm btn-primary" id="Save9">{{ trans('common.save') }}</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="Cancel9">{{ trans('common.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <div id="imgGrid2"></div>
                </div>

                
            </div>
            <!-- /.tab-content -->
        </div>
    </div> 
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    
    $('#myForm button[btnName="mark_no"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('mark_no', "碼頭搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="mark_no"]').on('click', function(){
        var check = $('#subBox input[name="mark_no"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","mark_no",callBackFunc=function(data){
                console.log(data);
                
            },"mark_no");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin";

    $(function(){
        $("#imgGrid2").on("rowdoubleclick", function(event){
            var selectedrowindex = $("#imgGrid2").jqxGrid('getselectedrowindex');
            var griddata =  $("#imgGrid2").jqxGrid('getrowdata', selectedrowindex);
            var boundindex =  griddata.boundindex;
            console.log(boundindex);
            var url = BASE_URL.replace('/admin','')+'/storage/uploadimg/' + griddata.guid.replace('public/', '');
            window.open(url);
        })
        var imagerenderer = function (row, datafield, value) {
            if(value != "") {
                console.log(BASE_URL.replace('/admin',''));
                return '<img style="margin-left: 15px;" height="60" width="50" src="'+"{{env('PRINTER_URL')}}"+ value + '"/>';
            }
            return "";
        }
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/checkin" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#mod_driver_checkin').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            $("#iAdd").hide();
            $("#iDel").hide();

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            

            var requiredColumn = [
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


        var col = [
                [
                    {name: "id", type: "number"},
                    {name: "guid", type: "string"},
                    {name: "type_no", type: "string"},
                    {name: "descp", type: "string"},
                    {name: "created_at", type: "string"}
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true },
                    {text: "圖片", datafield: "guid", width: 150, cellsrenderer: imagerenderer},
                    { 
                        text: '路徑', 
                        width: 100,
                        datafield: 'imgurl', 
                        columntype: 'button', 
                        cellsrenderer: function (row, column, value) {
                        return "點我打開圖片";
                        // return "按鈕";
                    }, buttonclick: function (row,event) {
                        var rowdata = $("#imgGrid2").jqxGrid('getrowdata', row);
                        var url = "{{env('PRINTER_URL')}}"+ rowdata.guid.replace('public/', '');
                        window.open(url);
                    }},
                    {text: "敘述", datafield: "descp", width: 300},
                    {text: "上傳時間", datafield: "created_at", width: 300}
                ]
            ];
            
            var opt = {};
            opt.gridId = "imgGrid2";
            opt.fieldData = col;
            opt.formId = "subForm8";
            opt.saveId = "Save9";
            opt.cancelId = "Cancel9";
            opt.showBoxId = "subBox8";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/checkin/imgget') }}" + '/' + mainId ;
            opt.delUrl  =   "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/checkin') }}" + "/imgdelete/";
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/checkin') }}" + "/imgstore";
            opt.defaultKey = {
                'ref_no': mainId
            };
            opt.commonBtn = false;
            opt.showtoolbar = false;
            @can('ImgControl')
            @endcan

            opt.beforeSave = function (row) {
            }
            opt.afterSave = function (data) {
                $('#imgGrid2').jqxGrid('updatebounddata');
            }
            opt.beforeCancel = function() {
            }

            genDetailGrid(opt);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection