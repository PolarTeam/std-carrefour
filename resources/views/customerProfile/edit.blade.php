@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('sys_customers.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/project') }}">{{trans('sys_customers.titleName')}}</a></li>
		<li class="active">{{trans('sys_customers.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('sys_customers.cust_no') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('sys_customers.cname') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>    
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sys_customers.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">是</option>
                                            <option value="B">否</option>
                                        </select>
                                    </div>         
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('sys_customers.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>                   
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sys_customers.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax">{{ trans('sys_customers.fax') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div> 
                                    <div class="form-group col-md-3">
                                        <label for="need_detail">{{ trans('sys_customers.need_detail') }}</label>
                                        <select class="form-control" id="need_detail" name="need_detail">
                                            <option value="Y">是</option>
                                            <option value="N">否</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>{{ trans('sys_customers.wh_name') }}  <span style="color:red">*<span></label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" id="wh_no" name="wh_no">
                                            @foreach($wh_no as $row)
                                                <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="zip">{{ trans('sys_customers.zip') }}</label>
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="zip" name="zip" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="zip"
                                                    info1="{{Crypt::encrypt('sys_area')}}" 
                                                    info2="{{Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm")}}" 
                                                    info3="{{Crypt::encrypt('')}}"
                                                    info5="{{Crypt::encrypt('modVendor')}}"
                                                    info4="dist_cd=zip;city_nm=city_nm;dist_nm=area_nm;dist_nm=custer_info;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="address">{{ trans('sys_customers.address') }}</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sys_customers.remark') }}</label>
                                            <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <div class="row"> 
                                    <input type="hidden" id="created_by" name="created_by" class="form-control">
                                    <input type="hidden" id="updated_by" name="updated_by" class="form-control">
                                    <input type="hidden" id="created_at" name="created_at" class="form-control">
                                    <input type="hidden" id="updated_at" name="updated_at" class="form-control">
                                    <input type="hidden" id="type" name="type" value="OTHER"  class="form-control">
                                    <input type="hidden" id="zip_code" name="zip_code" class="form-control">
                                    <input type="hidden" id="en_address" name="en_address" class="form-control">
                                    <input type="hidden" id="website" name="website" class="form-control">
                                    <input type="hidden" id="industry" name="industry" class="form-control">
                                    <input type="hidden" id="fax" name="fax" class="form-control">
                                    <input type="hidden" id="identity" name="identity" class="form-control">
                                    <input type="hidden" id="city_no" name="city_no" class="form-control">
                                    <input type="hidden" id="area_id" name="area_id" class="form-control">

                                    <input type="hidden" id="g_key" name="g_key" class="form-control">
                                    <input type="hidden" id="c_key" name="c_key" class="form-control">
                                    <input type="hidden" id="s_key" name="s_key" class="form-control">
                                    <input type="hidden" id="d_key" name="d_key" class="form-control">
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>

                            </div>
                        </form>
                    </div>
                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile";

    $(function(){

        $('#myForm button[btnName="zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="zip"]').on('click', function(){
            var check = $('#subBox input[name="zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","zip",callBackFunc=function(data){
                    console.log(data);
                    
                },"zip");
            }
        });

        // $('#myForm button[btnName="wh_name"]').on('click', function(){
        //     $('#lookupModal').modal('show');
        //     initLookup('wh_name', "倉庫查詢", callBackFunc=function(data){
        //         console.log(data);
        //     });
        // });

        // $('#myForm input[name="wh_name"]').on('click', function(){
        //     var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
        //     if(check == false) {
        //         initAutocomplete("myForm","wh_name",callBackFunc=function(data){
        //             console.log(data);
                    
        //         },"wh_name");
        //     }
        // });


        $('#subBox button[btnName="fee_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('fee_name', "代收款建檔", callBackFunc=function(data){
            });
        });

        $('#subBox button[btnName="goods_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goods_no', "料號建檔", callBackFunc=function(data){
            });
        });


        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/customerProfile" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#cust_type').select2();
            $('#wh_no').select2();
            
        };
        formOpt.addFunc = function() {
            $('#cust_type').val();
            $('#cust_type').trigger('change');

            $('#wh_no').val();
            $('#wh_no').trigger('change');
            $('#status').val('A');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            $('#type').val('OTHER');
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            $('#type').val('OTHER');
            var requiredColumn = [
                {
                    "column_filed":"wh_no",
                    "column_type":"select2",
                    "ranked" : 0
                },
                {
                    "column_filed":"cname",
                    "column_type":"string",
                },
                {
                    "column_filed":"cust_no",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

        //
        @if(isset($id))



        @endif
        //


    })
</script> 
<script>
$(function(){
});
</script>
@endsection