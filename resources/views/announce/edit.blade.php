@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_announce.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_announce') }}">{{ trans('mod_announce.titleName') }}</a></li>
		<li class="active">{{ trans('mod_announce.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                              <div class="row">
                                  <div class="form-group col-md-3">
                                    <label for="announce_type">{{ trans('mod_announce.announce_type_desc') }} <span style="color:red">*<span></label>
                                    <input type="hidden" class="form-control" id="announce_type_desc" name="announce_type_desc">
                                    <select class="form-control select2" id="announce_type" name="announce_type" multiple="multiple">
                                        @foreach($announce_type as $row)
                                        <option value="{{$row->name}}">{{$row->display_name}}</option>
                                        @endforeach
                                    </select>
                                  </div>

                                  <div class="form-group col-md-3">
                                    <label for="announce_date">{{ trans('mod_announce.announce_date') }} <span style="color:red">*<span></label>
                                    <input type="date" class="form-control" id="announce_date"  name="announce_date">
                                  </div>
                                
                                  <div class="form-group col-md-3">
                                      <label for="announce_start_date">{{ trans('mod_announce.announce_start_date') }} <span style="color:red">*<span></label>
                                      <input type="date" class="form-control" id="announce_start_date"  name="announce_start_date">
                                  </div>

                                  <div class="form-group col-md-3">
                                      <label for="announce_end_date">{{ trans('mod_announce.announce_end_date') }} <span style="color:red">*<span></label>
                                      <input type="date" class="form-control" id="announce_end_date"  name="announce_end_date">
                                  </div>

                              </div>
                              <div class="row">

                                <div class="form-group col-md-3">
                                    <label for="announce_subject">{{ trans('mod_announce.announce_subject') }} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="announce_subject"  name="announce_subject">
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="hidden" class="form-control" id="wh_name" name="wh_name">
                                    <label for="contact">{{ trans('mod_announce.wh_name') }} <span style="color:red">*<span></label>
                                    <select class="form-control select2" id="wh_no" name="wh_no" multiple="multiple">
                                        @foreach($wh_no as $row)
                                            <option value="{{$row->cust_no}}">{{$row->cname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="announce_detail">{{ trans('mod_announce.announce_detail') }} <span style="color:red">*<span></label>
                                    <input type="text" class="form-control" id="announce_detail"  name="announce_detail">
                                  </div>

                              </div>

                              <div class="row">

                                <div class="form-group col-md-3">
                                    <label for="is_enabled">{{ trans('mod_announce.is_enabled_desc') }}</label>
                                    <input type="hidden" class="form-control" id="is_enabled_desc" name="is_enabled_desc">
                                    <select class="form-control" id="is_enabled" name="is_enabled">
                                        <option value="Y">是</option>
                                        <option value="N">否</option>
                                    </select>
                                  </div>
                            </div>


                              <div class="row">
                              </div>

                                <div class="row">
                                  <div class="form-group col-md-3">
                                      <label for="created_by">{{ trans('mod_announce.created_by') }}</label>
                                      <input type="text" class="form-control" id="created_by" name="created_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="created_at">{{ trans('mod_announce.created_at') }}</label>
                                      <input type="text" class="form-control" id="created_at" name="created_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_by">{{ trans('mod_announce.updated_by') }}</label>
                                      <input type="text" class="form-control" id="updated_by" name="updated_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_at">{{ trans('mod_announce.updated_at') }}</label>
                                      <input type="text" class="form-control" id="updated_at" name="updated_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                              </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    
    // $('#myForm button[btnName="wh_name"]').on('click', function(){
    //     $('#lookupModal').modal('show');
    //     initLookup('wh_name', "倉庫搜尋", callBackFunc=function(data){
    //         console.log(data);
    //     });
    // });

    // $('#myForm input[name="wh_name"]').on('click', function(){
    //     var check = $('#subBox input[name="wh_name"]').data('ui-autocomplete') != undefined;
    //     if(check == false) {
    //         initAutocomplete("myForm","wh_name",callBackFunc=function(data){
    //             console.log(data);
                
    //         },"wh_name");
    //     }
    // });


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/announceProfile" ;
        
        formOpt.initFieldCustomFunc = function (){
            $('#announce_type').select2();
            $('#wh_no').select2();
            // console.log('test step');
            // $('#mod_announce').select2();
            
        };
        formOpt.addFunc = function() {
            $("#is_enabled").val('N');

            $('#announce_type').val();
            $('#announce_type').trigger('change');

            $('#wh_no').val();
            $('#wh_no').trigger('change');
        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            
            var announceStartDate = new Date($("#announce_start_date").val());
            announceStartDate.setHours(0, 0, 0, 0);  // 設定時間部分為 0

            var today = new Date();
            today.setHours(0, 0, 0, 0);  // 設定時間部分為 0

            if(mainId =="") {
                if (announceStartDate < today) {
                    swal("{{trans('common.warning')}}", "公告起始日期不可小於今天", "warning");
                    return false;
                }
            }



            var requiredColumn = [
                {
                    "ranked":0,
                    "column_filed":"announce_type",
                    "column_type":"select2",
                },
                {
                    "column_filed":"wh_no",
                    "column_type":"select2",
                    "ranked" : 1
                },
                {
                    "column_filed":"announce_date",
                    "column_type":"string",
                },
                {
                    "column_filed":"announce_start_date",
                    "column_type":"string",
                },
                {
                    "column_filed":"announce_end_date",
                    "column_type":"string",
                },
                {
                    "column_filed":"announce_subject",
                    "column_type":"string",
                },
                {
                    "column_filed":"announce_detail",
                    "column_type":"string",
                },
            ];

            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            var sel = document.getElementById("is_enabled");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#is_enabled_desc').val(text);

            // var sel = document.getElementById("announce_type");
            // var text= sel.options[sel.selectedIndex].text;
            // $('#announce_type_desc').val(text);
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection