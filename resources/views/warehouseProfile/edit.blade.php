@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('mod_warehouse.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/project') }}">{{trans('mod_warehouse.titleName')}}</a></li>
		<li class="active">{{trans('mod_warehouse.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('mod_warehouse.cust_no') }}  <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('mod_warehouse.cname') }}  <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>         
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('mod_warehouse.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact">
                                    </div>                         
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('mod_warehouse.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" name="status_desc" id="status_desc" class="form-control">
                                        <label for="status">{{ trans('mod_warehouse.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">是</option>
                                            <option value="B">否</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="contact">緯度  <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="lat" name="lat">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="contact">經度  <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="lng" name="lng">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="contact">郵遞區號  <span style="color:red">*<span></label>
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" id="zip" name="zip" required="required">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="zip"
                                                    info1="{{Crypt::encrypt('sys_area')}}" 
                                                    info2="{{Crypt::encrypt("dist_cd+city_nm+dist_nm,city_nm,dist_cd,dist_nm")}}" 
                                                    info3="{{Crypt::encrypt("")}}"
                                                    info5="{{Crypt::encrypt('sys_area')}}"
                                                    info4="area_nm=city_nm;dist_cd=zip;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <label for="address">{{ trans('mod_warehouse.address') }}  <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="gps_range">{{ trans('mod_warehouse.gps_range') }}  <span style="color:red">*<span></label>
                                        <input type="number" min="0" class="form-control" id="gps_range" name="gps_range">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="leave_time">{{ trans('mod_warehouse.leave_time') }}  <span style="color:red">*<span></label>
                                        <input type="number" min="0" class="form-control" id="leave_time" name="leave_time">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="hidden" name="leave_decision_desc" id="leave_decision_desc" class="form-control">
                                        <label for="status">{{ trans('mod_warehouse.leave_decision_desc') }}</label>
                                        <select class="form-control" id="leave_decision" name="leave_decision">
                                            <option value="guard">警衛點選</option>
                                            <option value="warehouse">倉管對點</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="dc_id">{{ trans('mod_warehouse.dc_id') }}</label>
                                        <select class="form-control" id="dc_id" name="dc_id">
                                            <option value="A0">台北倉</option>
                                            <option value="B0">觀音一倉</option>
                                            <option value="C0">台中倉</option>
                                            <option value="D0">高雄倉</option>
                                            <option value="E0">特品倉</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="address">{{ trans('mod_warehouse.contactinfo') }}</label>
                                        <input type="text" class="form-control" id="contactinfo" name="contactinfo" placeholder="Enter Address">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('mod_warehouse.remark') }}</label>
                                            <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('mod_warehouse.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('mod_warehouse.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('mod_warehouse.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" readonly ="true">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('mod_warehouse.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" readonly ="true">
                                    </div>
                                </div>
                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>


<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_6" data-toggle="tab" aria-expanded="false">明細</a></li>
                <li class=""><a href="#tab_7" data-toggle="tab" aria-expanded="false">不提供預約</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_6">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">明細</h3>
                        </div>
                        <form method="POST" accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <input type="hidden" name="dayofweek_desc" id="dayofweek_desc" class="form-control" grid="true" required="required">
                                        <label for="dayofweek">{{ trans('mod_warehouse_detail.dayofweek_desc') }}</label>
                                        <select class="form-control" id="dayofweek" name="dayofweek">
                                            <option value="Monday">星期一</option>
                                            <option value="Tuesday">星期二</option>
                                            <option value="Wednesday">星期三</option>
                                            <option value="Thursday">星期四</option>
                                            <option value="Friday">星期五</option>
                                            <option value="Saturday">星期六</option>
                                            <option value="Sunday">星期日</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="start_at">{{ trans('mod_warehouse_detail.start_at') }}</label>
                                        <input type="text" class="form-control input-sm" id="start_at" name="start_at" grid="true" required="required">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="end_at">{{ trans('mod_warehouse_detail.end_at') }}</label>
                                        <input type="text" class="form-control input-sm" id="end_at" name="end_at" grid="true" required="required">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                            </div>
                        </form>
                    </div>
    
                    <div id="jqxGrid"></div>
                </div>

                <div class="tab-pane" id="tab_7">
                    <div class="box box-primary" id="subBox2" style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">不提供預約</h3>
                        </div>
                        <form method="POST" accept-charset="UTF-8" id="subForm2" enctype="multipart/form-data">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="dayofweek">年</label>
                                        <select class="form-control" id="years" name="years">
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="month">月</label>
                                        <select class="form-control" id="month" name="month">
                                            @for ($i=1; $i<=12;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="month">日</label>
                                        <select class="form-control select2" style="width: 100%; text-align:center" id="days" name="days" multiple="multiple">
                                            @for ($i=1; $i<=31;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <!-- /.box-body -->
    
                            <div class="box-footer">
                                <input type="hidden" class="form-control input-sm" name="id" grid="true">
                                <button type="button" class="btn btn-sm btn-primary" id="Save2">{{ trans('common.save') }}</button>
                                <button type="button" class="btn btn-sm btn-danger" id="Cancel2">{{ trans('common.cancel') }}</button>
                            </div>
                        </form>
                    </div>
    
                    <div id="jqxGrid_2"></div>
                </div>


            </div>
            <!-- /.tab-content -->
        </div>
    </div> 
</div>


@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile/batchDelete";

    $(function(){

        $('#myForm button[btnName="zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
                $("#city_nm").val(data.city_nm+data.dist_nm);
            });
        });

        $('#myForm input[name="zip"]').on('click', function(){
            var check = $('#subBox input[name="zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","zip",callBackFunc=function(data){
                    console.log(data);
                    $("#city_nm").val(data.city_nm+data.dist_nm);
                },"zip");
            }
        });

        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/warehouseProfile" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#cust_type').select2();
            $('#days').select2();
            
        };
        formOpt.addFunc = function() {
            $('#cust_type').val();
            $('#cust_type').trigger('change');
            $('#status').val('A');
            $('#leave_decision').val('guard');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            $('#type').val('OTHER');
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        
        formOpt.beforesaveFunc = function() {
            var sel = document.getElementById("status");
            var text= sel.options[sel.selectedIndex].text;
            $('#status_desc').val(text);
            

            var sel = document.getElementById("leave_decision");
            var text= sel.options[sel.selectedIndex].text;
            $('#leave_decision_desc').val(text);
            
            var iserror = false ;
            var requiredColumn = [
                {
                    "column_filed":"lat",
                    "column_type":"string",
                },
                {
                    "column_filed":"lng",
                    "column_type":"string",
                },
                {
                    "column_filed":"gps_range",
                    "column_type":"string",
                },
                {
                    "column_filed":"address",
                    "column_type":"string",
                },
                {
                    "column_filed":"leave_time",
                    "column_type":"string",
                },
                {
                    "column_filed":"zip",
                    "column_type":"string",
                },
                {
                    "column_filed":"cust_no",
                    "column_type":"string",
                },
                {
                    "column_filed":"cname",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


        if(mainId != "") {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "cust_no", type: "string"},
                    {name: "dayofweek_desc", type: "string"},
                    {name: "dayofweek", type: "string"},
                    {name: "start_at", type: "string"},
                    {name: "end_at", type: "string"},
                    {name: "warehouse_id", type: "string"},
                ],
                [
                    {text: "{{ trans('mod_warehouse_detail.id') }}", datafield: "id", width: 100, hidden: true},
                    {text: "{{ trans('mod_warehouse_detail.warehouse_id') }}", datafield: "warehouse_id", width: 100, hidden: true},
                    {text: "{{ trans('mod_warehouse_detail.dayofweek_desc') }}", datafield: "dayofweek_desc", width: 150, nullable: false},
                    {text: "{{ trans('mod_warehouse_detail.start_at') }}", datafield: "start_at", width: 150, nullable: false},
                    {text: "{{ trans('mod_warehouse_detail.end_at') }}", datafield: "end_at", width: 150, nullable: false},
                ]
            ];
                
            var opt = {};
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/warehousedetail/get') }}" + '/' + mainId;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousedetail') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousedetail') }}" + "/update";
            opt.delUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousedetail') }}" + "/delete/";
            opt.commonBtn = true;
            opt.showtoolbar = true;
            opt.defaultKey = {
                'warehouse_id': mainId
            };
            opt.beforeSave = function (row) {
                if(row.dayofweek == "" || row.dayofweek == null) {
                    swal("警告", "星期幾不可為空", "warning");
                    return false;
                }
                
                if(row.start_at == "" || row.start_at == null) {
                    swal("警告", "開始結束時間不可為空", "warning");
                    return false;
                }

                if(row.end_at == "" || row.end_at == null) {
                    swal("警告", "開始結束時間不可為空", "warning");
                    return false;
                }
                var sel = document.getElementById("dayofweek");
                var text= sel.options[sel.selectedIndex].text;
                $('#dayofweek_desc').val(text);

                return true;
            }

            opt.afterSave = function (data) {
                if(data.success == false) {
                    swal("警告", data.message, "warning");
                }
            }

            opt.beforeCancel = function() {
                
            }

            genDetailGrid(opt);


            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "years", type: "string"},
                    {name: "month", type: "string"},
                    {name: "days", type: "string"},

                    {name: "warehouse_id", type: "string"},
                ],
                [
                    {text: "{{ trans('mod_warehouse_notwork.id') }}", datafield: "id", width: 100, hidden: true},
                    {text: "年", datafield: "years", width: 100, nullable: false},
                    {text: "月", datafield: "month", width: 150, nullable: false},
                    {text: "日", datafield: "days", width: 150, nullable: false},
                ]
            ];
                
            var opt = {};
            opt.gridId = "jqxGrid_2";
            opt.fieldData = col;
            opt.formId = "subForm2";
            opt.saveId = "Save2";
            opt.cancelId = "Cancel2";
            opt.showBoxId = "subBox2";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW').'/warehousenotwork/get') }}" + '/' + mainId;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousenotwork') }}" + "/store";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousenotwork') }}" + "/update";
            opt.delUrl = "{{ url(config('backpack.base.route_prefix', '/zh-TW') . '/warehousenotwork') }}" + "/delete/";
            opt.commonBtn = true;
            opt.showtoolbar = true;
            opt.defaultKey = {
                'warehouse_id': mainId
            };
            opt.beforeSave = function (row) {
                row.days = $("#days").val();
                if(row.years == "" || row.years == null) {
                    swal("警告", "年不可為空", "warning");
                    return false;
                }
                
                if(row.month == "" || row.month == null) {
                    swal("警告", "月不可為空", "warning");
                    return false;
                }

                if(row.days == "" || row.days == null) {
                    swal("警告", "日不可為空", "warning");
                    return false;
                }

                return true;
            }
            opt.confirmData = function (postdata) {
               postdata.set('days', $("#days").val().join(','));
               return postdata;
            }
            opt.select2data = function (column,value) {
                if(column == "days") {
                    var a = value.split(',');
                    // $('#'+v['field_name']).select2();
                    // $('#'+v['field_name']).val(a);
                    // $('#'+v['field_name']).trigger('change');
                    
                    $('#subForm2 [name="'+column+'"]').val(a);
                    $('#subForm2 [name="'+column+'"]').trigger('change');
                }
            }
            opt.afterSave = function (data) {
                if(data.success == false) {
                    swal("警告", data.message, "warning");
                }
            }

            opt.beforeCancel = function() {
                
            }

            genDetailGrid(opt);

        }


    })
</script> 
<script>
$(function(){
});
</script>
@endsection