@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
         {{ trans('mod_dss_dlv.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/dlvProfile') }}"> {{ trans('mod_dss_dlv.titleName') }}</a></li>
		<li class="active"> {{ trans('mod_dss_dlv.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('modOrder.basicInfo') }}</a></li>
                    <li class=""><a href="#tab_7" data-toggle="tab" aria-expanded="false">提貨配送資料</a></li>
                    <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">材體積重量</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="dlv_no">{{ trans('mod_dss_dlv_detail.dlv_no') }}</label>
                                        <input type="text" class="form-control" id="dlv_no" name="dlv_no" >
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="dlv_type_desc">配送型態</label>
                                        <input type="text" class="form-control" id="dlv_type_desc" name="dlv_type_desc" >
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="sys_dlv_no">{{ trans('mod_dss_dlv_detail.sys_dlv_no') }}</label>
                                        <input type="text" class="form-control" id="sys_dlv_no" name="sys_dlv_no" >
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="dlv_no">{{ trans('mod_dss_dlv_detail.dlv_no') }}</label>
                                        <input type="number" class="form-control" id="dlv_no" name="dlv_no" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ord_no">{{ trans('mod_dss_dlv_detail.ord_no') }}</label>
                                        <input type="text" class="form-control" id="ord_no" name="ord_no" >
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="hidden" class="form-control" id="order_remark" name="order_remark" >
                                        <input type="hidden" class="form-control" id="dsv_remark" name="dsv_remark" >
                                        <label for="merge_remark">備註</label>
                                        <input type="text" class="form-control" id="merge_remark" name="merge_remark" readonly="true" disabled="disabled" >
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="created_by">{{ trans('mod_dss_dlv_detail.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="created_at">{{ trans('mod_dss_dlv_detail.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_by">{{ trans('mod_dss_dlv_detail.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="updated_at">{{ trans('mod_dss_dlv_detail.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at">
                                    </div>
                                </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                    <div id="tab_7" class="tab-pane">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary" id="statusDiv">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">{{ trans('modOrder.pickupInfo') }}</h3>
                                            <div class="box-tools">
                                                <button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class='box-body no-padding'>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label for="owner_name">{{ trans('mod_dss_dlv_detail.owner_name') }}</label>
                                                    <input type="text" class="form-control" id="pick_owner_name" name="pick_owner_name" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_name">{{ trans('mod_dss_dlv_detail.cust_name') }}</label>
                                                    <input type="text" class="form-control" id="pick_cust_name" name="pick_cust_name" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_zip">{{ trans('mod_dss_dlv_detail.cust_zip') }}</label>
                                                    <input type="text" class="form-control" id="pick_cust_zip" name="pick_cust_zip" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_address">{{ trans('mod_dss_dlv_detail.cust_address') }}</label>
                                                    <input type="text" class="form-control" id="pick_cust_address" name="pick_cust_address" >
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary" id="statusDiv">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">{{ trans('modOrder.deliveryInfo') }}</h3>
                                            <div class="box-tools">
                                                <button id="testbtn1" type="button" class="btn btn-box-tool" data-widget="collapse">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class='box-body no-padding'>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label for="owner_name">{{ trans('mod_dss_dlv_detail.owner_name') }}</label>
                                                    <input type="text" class="form-control" id="dlv_owner_name" name="dlv_owner_name" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_name">{{ trans('mod_dss_dlv_detail.cust_name') }}</label>
                                                    <input type="text" class="form-control" id="dlv_cust_name" name="dlv_cust_name" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_zip">{{ trans('mod_dss_dlv_detail.cust_zip') }}</label>
                                                    <input type="text" class="form-control" id="dlv_cust_zip" name="dlv_cust_zip" >
                                                </div>
                                            
                                                <div class="form-group col-md-3">
                                                    <label for="cust_address">{{ trans('mod_dss_dlv_detail.cust_address') }}</label>
                                                    <input type="text" class="form-control" id="dlv_cust_address" name="dlv_cust_address" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="tab_5" class="tab-pane">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="amt">{{ trans('mod_dss_dlv.total_cbm') }}</label>
                                    <input type="text" class="form-control" id="total_cbm" name="total_cbm" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="total_gw">{{ trans('mod_dss_dlv.total_gw') }}</label>
                                    <input type="text" class="form-control" id="total_gw" name="total_gw" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="total_gw">{{ trans('mod_dss_dlv.total_gw') }}</label>
                                    <input type="text" class="form-control" id="total_gw" name="total_gw" >
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    

    $('#myForm button[btnName="cust_no"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('cust_no', "車商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="cust_no"]').on('click', function(){
        var check = $('#subBox input[name="cust_no"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","cust_no",callBackFunc=function(data){
                console.log(data);
            },"cust_no");
        }
    });

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfile" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#dlvProfile').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            if($('#display_name').val() == ""){
                document.getElementById("display_name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if($('#name').val() == ""){
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            var requiredColumn = [

            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){

    // $.get("{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/dlvProfilePICKDLV/" + mainId , function( data ) {
    //     console.log(data);

    // });
});
</script>
@endsection