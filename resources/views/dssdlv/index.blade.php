@extends('layout.layout')
@section('header')
<style>
	*,
	*:before,
	*:after {
	  box-sizing: border-box;
	}
	
	.form-container {
	  padding: 1rem;
	  margin: 2rem auto;
	  background-color: #DDDDDD;
	  border: 1px solid #DDDDDD;
	  width: 50%;
	}
	
	/* HTML5 Boilerplate accessible hidden styles */
	.promoted-input-checkbox {
	  border: 0;
	  clip: rect(0 0 0 0);
	  height: 1px;
	  margin: -1px;
	  overflow: hidden;
	  padding: 0;
	  position: absolute;
	  width: 1px;
	}
	
	.promoted-checkbox input:checked + label > svg {
	  height: 24px;
	  -webkit-animation: draw-checkbox ease-in-out 0.2s forwards;
			  animation: draw-checkbox ease-in-out 0.2s forwards;
	}
	.promoted-checkbox label:active::after {
	  background-color: #DDDDDD;
	}
	.promoted-checkbox label {
	  color: #000000;
	  line-height: 40px;
	  cursor: pointer;
	  position: relative;
	}
	.promoted-checkbox label:after {
	  content: "";
	  height: 34px;
	  width: 34px;
	  margin-right: 1rem;
	  float: left;
	  /* background-color: #DDDDDD; */
	  border: 2px solid #DDDDDD;
	  border-radius: 3px;
	  -webkit-transition: 0.15s all ease-out;
	  transition: 0.15s all ease-out;
	}
	.promoted-checkbox svg {
	  stroke: #000000;
	  stroke-width: 2px;
	  height: 0;
	  width: 40px;
	  position: absolute;
	  left: -4px;
	  top: 5px;
	  stroke-dasharray: 33;
	}
	
	@-webkit-keyframes draw-checkbox {
	  0% {
		stroke-dashoffset: 33;
	  }
	  100% {
		stroke-dashoffset: 0;
	  }
	}
	
	@keyframes draw-checkbox {
	  0% {
		stroke-dashoffset: 33;
	  }
	  100% {
		stroke-dashoffset: 0;
	  }
	}
	.sortable {
		margin: 2px;
		padding: 5px;
	}
	.sortable-container{;
		float: left;
	}
	.sortable-container span {
		font-weight: bold;
	}
	.sortable div {
		border-radius: 5px;
		padding: 5px;
		background-color: white;
		color: black;
		cursor: pointer;
		border: 1px solid transparent;
		}
		.sortable div:hover {
		border: 1px solid #356AA0;
	}   
	.sortable-item{
			border: 1px solid #356AA0;
			/* border-radius: 5px; */
			padding: 3px;
			background-color: white;
			color: black;
	}
	.sortable-item:hover {
		background-color: #356AA0;
		color: white;
	}
	.test-item {
		height:32px;
	}
	.test-item:hover {
		background-color: #356AA0;
		height:32px;
		color: white;
	}
	.menu li{
		display:inline;
		padding-right:10px;
		width:5%;
		font-size: 15px;
	}
	.header li{
		display:inline;
		font-size: 15px;
	}
	.sortdiv{ 
		width:14%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivW{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:10%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWC{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:10.1%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWR{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:20.5%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivWA{ 
		/* background-color: #e8e8e8; */
		/* border: 1px solid #356AA0; */
		width:20%; display:inline-block !important; display:inline;
		vertical-align: top;
	}
	.sortdivsys{ 
		width:10%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivzip{ 
		width:10%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivaddr{ 
		width:20%; display:inline-block !important; display:inline;padding-right:10px;
		vertical-align: top;
	}
	.sortdivHsys{
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:10.25%; display:inline-block !important; display:inline; 
	}
	.sortdivHzip{
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:10.25%; display:inline-block !important; display:inline; 
	}
	.sortdivHaddr{ 
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:20.25%; display:inline-block !important; display:inline; 
	}
	.sortdivH{ 
		/* border: 1px solid; */
		background-color: #e8e8e8;
		padding-top:10px;
		width:14.25%; display:inline-block !important; display:inline; 
	}
	.ui-progressbar {
		position: relative;
	}
	.progress-label {
		position: absolute;
		left: 50%;
		top: 4px;
		font-weight: bold;
		text-shadow: 1px 1px 0 #fff;
	}
	.lmask {
		position: absolute;
		height: 100%;
		width: 100%;
		background-color: #000;
		bottom: 0;
		left: 0;
		right: 0;
		top: 0;
		z-index: 9999999;
		opacity: 0.4;
	}
	.swal2-container {
		z-index: 10000;
	}
	.lmask.fixed {
		position: fixed;
	}
	.lmask:before {
		content: '';
		background-color: rgba(0, 0, 0, 0);
		border: 5px solid rgba(0, 183, 229, 0.9);
		opacity: .9;
		border-right: 5px solid rgba(0, 0, 0, 0);
		border-left: 5px solid rgba(0, 0, 0, 0);
		border-radius: 50px;
		box-shadow: 0 0 35px #2187e7;
		width: 50px;
		height: 50px;
		-moz-animation: spinPulse 1s infinite ease-in-out;
		-webkit-animation: spinPulse 1s infinite linear;
		margin: -25px 0 0 -25px;
		position: absolute;
		top: 50%;
		left: 50%;
	}
	.lmask:after {
		content: '';
		background-color: rgba(0, 0, 0, 0);
		border: 5px solid rgba(0, 183, 229, 0.9);
		opacity: .9;
		border-left: 5px solid rgba(0, 0, 0, 0);
		border-right: 5px solid rgba(0, 0, 0, 0);
		border-radius: 50px;
		box-shadow: 0 0 15px #2187e7;
		width: 30px;
		height: 30px;
		-moz-animation: spinoffPulse 1s infinite linear;
		-webkit-animation: spinoffPulse 1s infinite linear;
		margin: -15px 0 0 -15px;
		position: absolute;
		top: 50%;
		left: 50%;
	}

	@-moz-keyframes spinPulse {
	0% {
	-moz-transform: rotate(160deg);
	opacity: 0;
	box-shadow: 0 0 1px #2187e7;
	}
	50% {
	-moz-transform: rotate(145deg);
	opacity: 1;
	}
	100% {
	-moz-transform: rotate(-320deg);
	opacity: 0;
	}
	}
	@-moz-keyframes spinoffPulse {
	0% {
	-moz-transform: rotate(0deg);
	}
	100% {
	-moz-transform: rotate(360deg);
	}
	}
	@-webkit-keyframes spinPulse {
	0% {
	-webkit-transform: rotate(160deg);
	opacity: 0;
	box-shadow: 0 0 1px #2187e7;
	}
	50% {
	-webkit-transform: rotate(145deg);
	opacity: 1;
	}
	100% {
	-webkit-transform: rotate(-320deg);
	opacity: 0;
	}
	}
	@-webkit-keyframes spinoffPulse {
	0% {
	-webkit-transform: rotate(0deg);
	}
	100% {
	-webkit-transform: rotate(360deg);
	}
	}
	
</style>
<section class="content-header">
    <h1>
        {{ trans('mod_dss_dlv.titleName') }}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">{{ trans('mod_dss_dlv.titleName') }}</li>
    </ol>
    <input type="hidden" value="{{$datafields}}" id="datafields" />
    <input type="hidden" value="{{$columns}}" id="columns" />

    <input type="hidden" value="{{$datafields2}}" id="datafields2" />
    <input type="hidden" value="{{$columns2}}" id="columns2" />
    <input type="hidden" value="{{$lang}}" id="lang" />
    <form method="POST" accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" id="excelFile" name="excelFile" style="display:none;"/>
        <button type="submit" class="btn btn-primary" id="btnImport" style="display:none;">{{ trans('excel.btnImport') }}</button>
    </form>
</section>
<div class='lmask' id="cssLoading" style="display:none"></div>
<div id="searchWindow" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody">
        <div id="searchContent">

        </div>
        <div class="row" style="border-top: 1px solid #dedede; margin-top: 10px; padding-top: 10px;" id="searchFooter">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>

</div>

<div id="searchWindow2" style="overflow-x:hidden; display:none">
    <div>{{ trans('common.search') }}</div>
    <div id="searchBody2">
        <div id="searchContent2">

        </div>
        <div class="row" style="border-top: 1px solid #0e0c0c; margin-top: 10px; padding-top: 10px;" id="searchFooter2">
            <div class="col-xs-3">
                <select class="form-control input-sm" name="selSearchName2"></select>
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control input-sm" placeholder="{{ trans('common.msgnew') }}" name="searchName2">
            </div>
            <div class="col-xs-3">
                <button class="btn btn-sm btn-success" name="saveSearch2">{{ trans('common.save') }}</button>
                <button class="btn btn-sm btn-warning" name="setDefault2">{{ trans('common.default') }}</button>
                <button class="btn btn-sm btn-danger" name="delSearch2">{{ trans('common.delete') }}</button>
            </div>
            <div class="col-xs-3" style="align: right">
                <button class="btn btn-sm btn-primary" name="winSearchBtn2">{{ trans('common.search') }}</button>
                <button class="btn btn-sm btn-primary" name="winSearchAdd2">{{ trans('common.addNewRow') }}</button>
            </div>
        </div>
    </div>

</div>
@endsection
@section('before_scripts')
<script>
    var btnGroupList = [];
    var enabledheader = [];
    var enabledheaderdetail = [];
    var lang = "{{$lang}}";
	var languagename = "";
    var tableName = 'mod_dss_dlv';
    var fileName = 'dlvProfile';
    var btnGroup = [
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
                
                var url  =BASE_URL;
                url = url.replace("admin","");
                var pageNum = 1;
                var pageSize = 50;
                var anotherCondition = typeof sqlcondition !== 'undefined' ? sqlcondition : [];
                var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                if(focus=="1"){
                    filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    // ----------
                    var url  =BASE_URL;
                    url = url.replace("admin","");
                    var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];
                    var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                    if($('#jqxGrid').jqxGrid('getrows').length == 0) {
                        swal("當前畫面沒有資料", "", "warning");
                        return;
                    }
                    var ids = new Array();
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        var pageNum = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                        var pageSize = $('#jqxGrid').jqxGrid('getdatainformation').paginginformation.pagesize;
                        for (var i = 0; i < filterGroups.length; i++) {
                            var filterGroup = filterGroups[i];
                            var filters = filterGroup.filter.getfilters();
                                for (var k = 0; k < filters.length; k++) {
                                    if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                        var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                        // new Date(unix_timestamp * 1000)
                                        filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                    } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                        var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                        filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                    } else if(filters[k].condition == "contains" ) {
                                        filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                    } else {
                                        filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                    }
                            }
                        }
                    } else {
                        filtercolumn[m] = ['id', `in`, ids];
                    }
                    var sortby = [];
                    var sortinformation = $('#jqxGrid').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                    }
                    $.post(BASE_URL +"/{{$lang}}"+ '/dlvProfile/excel/export', {
                        'baseCondition'   : filtercolumn,
                        'anotherCondition': anotherCondition,
                        'pageNum'         : pageNum,
                        'pageSize'        : pageSize,
                        'sort'            : sortby,
                        'header'          : enabledheader,
                        'table'           : 'mod_dss_dlv',
                        'fileName'        : 'dlvProfile',
                    }, function(data){
                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                    // ----------
                }else{
                    // ----------
                    var url  =BASE_URL;
                    url = url.replace("admin","");
                    filterGroups = $('#jqxGrid_2').jqxGrid('getfilterinformation');
                    filtercolumndata = [];
                    var filtercolumn = [];
                    var rows = $("#jqxGrid_2").jqxGrid('selectedrowindexes');
                    var ids = new Array();
                    if($('#jqxGrid_2').jqxGrid('getrows').length == 0) {
                        swal("當前畫面沒有資料", "", "warning");
                        return;
                    }
                    for (var m = 0; m < rows.length; m++) {
                        var row = $("#jqxGrid_2").jqxGrid('getrowdata', rows[m]);
                        if(typeof row != "undefined") {
                            ids.push(row.id);
                        }
                    }
                    if(ids.length == 0) {
                        var pageNum = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagenum+1;
                        var pageSize = $('#jqxGrid_2').jqxGrid('getdatainformation').paginginformation.pagesize;
                        for (var i = 0; i < filterGroups.length; i++) {
                            var filterGroup = filterGroups[i];
                            var filters = filterGroup.filter.getfilters();
                                for (var k = 0; k < filters.length; k++) {
                                    if(filters[k].condition == "GREATER_THAN_OR_EQUAL") {
                                        var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                        // new Date(unix_timestamp * 1000)
                                        filtercolumn[k] = [filterGroup.filtercolumn, '>=',  date+ " 00:00:00"];
                                    } else if(filters[k].condition == "LESS_THAN_OR_EQUAL") {
                                        var date = new Date(Date.parse(filters[k].value)).toLocaleDateString("zh-TW");
                                        filtercolumn[k] = [filterGroup.filtercolumn, '<=',  date + " 23:59:59"];
                                    } else if(filters[k].condition == "contains" ) {
                                        filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                    } else {
                                        filtercolumn[k] = [filterGroup.filtercolumn, ` LIKE `, `%`+filters[k].value+`%`];
                                    }
                                    // filtercolumndata.push(filtercolumn);
                            }
                        }
                    } else {
                        filtercolumn[m] = ['id', `in`, ids];
                    }
                    var sortby = [];
                    var sortinformation = $('#jqxGrid_2').jqxGrid('getsortinformation');
                    if(sortinformation.sortcolumn != null ) {
                        var sortvalue = sortinformation.sortdirection.ascending == true ? "ASC" : "DESC";
                        var obj = {};
                        obj["sortField"] = sortinformation.sortcolumn;
                        obj["sortValue"] = sortvalue;
                        sortby.push(obj);
                    }
                    $.post(BASE_URL + "/{{$lang}}"+ '/dlvProfile/excel/export', {
                        'baseCondition'   : filtercolumn,
                        'anotherCondition': anotherCondition,
                        'pageNum'         : pageNum,
                        'pageSize'        : pageSize,
                        'sort'            : sortby,
                        'header'          : enabledheaderdetail,
                        'table'           : 'mod_dss_dlv_detail',
                        'fileName'        : 'dlvProfiledetail',
                    }, function(data){
                        if(data.message == "success") {
                            window.open(data.donwloadLink);
                        }

                    });
                    // ----------
                }

            }
        },
        {
            btnId: "btnSearchWindow",
            btnIcon: "fa fa-search",
            btnText: "{{ trans('common.search') }}",
            btnFunc: function () {
                if(focus=="1"){
                    $('#searchWindow').jqxWindow('open');
                }else{
                    $('#searchWindow2').jqxWindow('open');
                }
            }
        },
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                if(focus=="1"){
                    $('#gridOptModal').modal('show');
                }else{
                    $('#gridOptModal2').modal('show');
                }
               
            }
        },
        @can('DSSDATAIMPORT')
        {
            btnId: "btnImportdatadss",
            btnIcon: "fa fa-table",
            btnText: "資料導入",
            btnFunc: function () {
                $("#cssLoading").show();
                $.post(BASE_URL + '/zh-TW/dlvProfile/revertdata',  function(data){
                    if(data.success ) {
                        $("#cssLoading").hide();
                        swal("完成", "", "success");
                        $("#jqxGrid").jqxGrid('clearselection');
                        $("#jqxGrid").jqxGrid('updatebounddata');
                    }
                    else{
                        $("#cssLoading").hide();
                        swal("操作失敗", data.message , "error");
                    }
                });
            }
        },
        @endcan
        // {
        //     btnId: "btnImportdatadss",
        //     btnIcon: "fa fa-table",
        //     btnText: "資料導入by車商",
        //     btnFunc: function () {
        //         $.post(BASE_URL + '/zh-TW/dlvProfile/revertdatabycar',  function(data){
        //             if(data.success ) {
        //                 swal("完成", "", "success");
        //                 $("#jqxGrid").jqxGrid('clearselection');
        //                 $("#jqxGrid").jqxGrid('updatebounddata');
        //             }
        //             else{
        //                 swal("操作失敗", data.message , "error");
        //             }
        //         });
        //     }
        // },
        // {
        //     btnId: "btnrevertdatabytranfer",
        //     btnIcon: "fa fa-table",
        //     btnText: "資料導入by台中至高雄轉運",
        //     btnFunc: function () {
        //         $.post(BASE_URL + '/zh-TW/dlvProfile/revertdatabytranfer',  function(data){
        //             if(data.success ) {
        //                 swal("完成", "", "success");
        //                 $("#jqxGrid").jqxGrid('clearselection');
        //                 $("#jqxGrid").jqxGrid('updatebounddata');
        //             }
        //             else{
        //                 swal("操作失敗", data.message , "error");
        //             }
        //         });
        //     }
        // },
    ];
    $.each(btnGroup, function(i, item) {
        var btnHtml = ' <a class="btn btn-app" id="{btnId}"><i class="{btnIcon}"></i> {btnText}</a>';
        btnHtml = btnHtml.replace("{btnId}",item.btnId);
        btnHtml = btnHtml.replace("{btnIcon}",item.btnIcon);
        btnHtml = btnHtml.replace("{btnText}",item.btnText);
        $("#btnArea").append(btnHtml);
        $("#"+item.btnId).on("click",function(){
            item.btnFunc();
        });
        
    });
</script>
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core.js?v={{Config::get('app.version')}}"></script>
@endsection

@section('content')
<div id="jqxLoader">
</div>
<div class="row" id="mainForm">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li name="prodDetail" class="active">
                <a onclick="tab_1()" href="#tab_1" data-toggle="tab" aria-expanded="false">車次總覽</a>
            </li>
            <li name="prodGift">
                <a onclick="tab_2()" href="#tab_2" data-toggle="tab" aria-expanded="false">車次明細</a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="button-group">
                    <div class="row" id="btnArea"></div>
                    <div id="griddiv">
                        <div id="jqxGrid"></div>
                        <div id="jqxGrid_2"></div>
                    </div>
                    <input type="button" style="display:none" id="updategridsign">
                    <input type="button" style="display:none" id="updategriderror">
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="gridOptModal2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Grid Option</h4>
            </div>
            <div class="modal-body">
                <div id="jqxlistbox2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" id="saveGrid2">{{ trans('common.saveChange') }}</button>
                <button type="button" class="btn btn-danger" id="clearGrid2">{{ trans('common.clearGrid') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/core') }}/grid-core-after.js?v={{Config::get('app.version')}}"></script>
<script>
    var focus ="1";
    var savekey = "mod_dss_dlv";
    var theme = 'bootstrap';
    var toedit = "false";
    function openPicture(path) {
        document.getElementById('light-box').style.display = 'flex';
        document.getElementById('light-box-lg-img').src = BASE_URL + "/storage/img/" + path.replace('public/img/', '');
        // window.open(BASE_URL + "/show/" + path , '簽收照片', config='height=500,width=600');
    }
    var columnsdata = JSON.parse($("#columns").val());

    $.grep(columnsdata, function(e){
        if(e.datafield == "image") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var image = dataAdapter.records[row].image;
                var btn = "";

                if(image != undefined ) {
                    btn = `<button class="btn btn-link" onclick="openPicture('{image}')">{{trans('common.msgnew66')}}</button>`;
                    btn = btn.replace("{image}", image);
                }
                
                return btn;
            };

            e.cellsrenderer = cellsrenderer;
        }
    });
    console.log(JSON.parse($("#datafields").val()));
    var fieldData = null;
    var fieldObj = null;
    var fieldObj2 = null;
    var winHeight2 = $(window).height() - 350;

    var sourcedetail = {
        datatype: "json",
        datafields: JSON.parse($("#datafields2").val()),
        root: "Rows",
        pagenum: 0,
        beforeprocessing: function(data) {
            sourcedetail.totalrecords = data[0].TotalRows;
            if (data[0].StatusCount.length > 0) {
                data[0].StatusCount.push({
                    'count': data[0].TotalRows,
                    'status': 'ALL',
                    'statustext': 'ALL'
                });
            }
        },
        filter: function() {
            // update the grid and send a request to the server.
            $("#jqxGrid_2").jqxGrid('updatebounddata', 'filter');
        },
        sort: function() {
            // update the grid and send a request to the server.
            $("#jqxGrid_2").jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 50,
        url: "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_dss_dlv_detail/false') }}"
    }

    var dataAdapterdetail = new $.jqx.dataAdapter(sourcedetail, {
        async: false,
        loadError: function(xhr, status, error) {
            alert('Error loading "' + sourcedetail.url + '" : ' + error);
        },
        loadComplete: function() {
        }
    });


    var h = 350;

    if (gridOpt.enabledStatus == false) {
        h = 350;
    }
    var winHeigt = $(window).height() - h;
    if (typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }
    console.log( JSON.parse($("#columns").val()));


    var columnsdetail = JSON.parse($("#columns2").val());
    $.grep(columnsdetail, function(e){
        if(e.datafield == "owner_name") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var rowdetail = $("#jqxGrid_2").jqxGrid('getrowdata', row);
                var consumables = rowdetail.consumables;
                var barcode = rowdetail.barcode;
                var owner_name = rowdetail.owner_name;
                var btn = "";
                if(consumables == "Y" ) {
                    btn = `<button class="btn btn-link" onclick="openHistory('${barcode}')">{{trans('common.msgnew65')}}</button>`;
                } else {
                    btn = defaulthtml;
                }
                
                return btn;
            };
            e.cellsrenderer = cellsrenderer;
        }

        if(e.datafield == "storage_name") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var rowdetail = $("#jqxGrid_2").jqxGrid('getrowdata', row);
                var consumables = rowdetail.consumables;
                var barcode = rowdetail.barcode;
                if(consumables == "Y" ) {
                    btn = `<button class="btn btn-link" onclick="openstorage('${barcode}')">{{trans('common.msgnew65')}}</button>`;
                } else {
                    btn = defaulthtml;
                }
                return btn;
            };
            e.cellsrenderer = cellsrenderer;
        }

        if(e.datafield == "mystatus") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var rowdetail = $("#jqxGrid_2").jqxGrid('getrowdata', row);
                var consumables = rowdetail.consumables;
                var num = rowdetail.num;
                var barcode = rowdetail.barcode;
                var part_no = rowdetail.part_no;
                var project = rowdetail.project;
                var use_project = rowdetail.use_project;
                var mystatus = rowdetail.mystatus;
                if(consumables == "Y" ) {
                    btn = `<button class="btn btn-link" onclick="openstatus('${barcode}','${part_no}', '${project}','${use_project}')">{{trans('common.msgnew65')}}</button>`;
                } else {
                    btn = defaulthtml;
                }
                return btn;
            };
            e.cellsrenderer = cellsrenderer;
        }
        if(e.datafield == "other_remark") {
            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var rowdetail = $("#jqxGrid_2").jqxGrid('getrowdata', row);
                var barcode = rowdetail.barcode;
                var otherremark = rowdetail.other_remark;
                if(otherremark != null ) {
                    btn = `<button class="btn btn-link" onclick="showremark('${barcode}')">查看備註</button>`;
                } else {
                    btn = `<button class="btn btn-link" onclick="showremark('${barcode}')">查看備註無內容</button>`;
                }
                return btn;
            };
            e.cellsrenderer = cellsrenderer;
        }
    });


    $("#jqxGrid_2").on('bindingcomplete', function(event) {
        $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
    });

    function loadState() {
        
        var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: {
                key: "dlvProfile",
                lang: $("#lang").val()
            },
            success: function(response) {
                if (response != "") {
                    response = JSON.parse(response);
                    $("#" + gridOpt.gridId).jqxGrid('loadstate', response);
                }

                var listSource = [];

                state = $("#" + gridOpt.gridId).jqxGrid('getstate');
                console.log(state);
                $('#jqxLoader').jqxLoader('close');
                $.get(BASE_URL + "/searchList/get/" + "dlvProfile", {}, function(data) {
                    if (data.msg == "success") {
                         var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                        for (i in data.data) {
                            if (data.data[i]["layout_default"] == "Y") {
                                opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                $("input[name='searchName']").val(data.data[i]["title"]);
                            } else {
                                opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                            }
                        }

                        $("select[name='selSearchName']").html(opt);

                        $.get(BASE_URL + "/searchHtml/get/" + $("select[name='selSearchName']").val(), {}, function(data) {
                            if (data.msg == "success") {
                                if (data.data != null) {
                                    $("#searchContent").html(data.data["data"]);
                                } else {
                                    var seasrchTpl = getSearchTpl(state);
                                    initSearchWindow(seasrchTpl);
                                }

                                var offset = $(".content-wrapper").offset();
                                $('#searchWindow').jqxWindow({
                                    position: {
                                        x: offset.left + 50,
                                        y: offset.top + 50
                                    },
                                    showCollapseButton: true,
                                    maxHeight: 400,
                                    maxWidth: 700,
                                    minHeight: 200,
                                    minWidth: 200,
                                    height: 300,
                                    width: 700,
                                    autoOpen: false,
                                    initContent: function() {
                                        $('#searchWindow').jqxWindow('focus');
                                    }
                                });


                                $("button[name='winSearchAdd']").on("click", function() {
                                    var seasrchTpl = getSearchTpl(state);
                                    $("#searchContent").append(searchTpl);
                                });

                                $(document).on("click", "button[name='winBtnRemove']", function() {
                                    $(this).parents(".row").remove();
                                });

                                $(document).on("change", "select[name='winField[]']", function() {
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                    var val = $(this).val();
                                    var info = searchObj(val, fieldObj[0]);
                                    var str = [{
                                            val: 'CONTAINS',
                                            label: '包含'
                                        },
                                        {
                                            val: 'EQUAL',
                                            label: '等於'
                                        },
                                        {
                                            val: 'NOT_EQUAL',
                                            label: '不等於'
                                        },
                                        {
                                            val: 'NULL',
                                            label: 'NULL'
                                        },
                                        {
                                            val: 'NOT_NULL',
                                            label: 'NOT NULL'
                                        },
                                    ];

                                    var num = [{
                                            val: 'EQUAL',
                                            label: '等於'
                                        },
                                        {
                                            val: 'NOT_EQUAL',
                                            label: '不等於'
                                        },
                                        {
                                            val: 'LESS_THAN',
                                            label: '小於'
                                        },
                                        {
                                            val: 'LESS_THAN_OR_EQUAL',
                                            label: '小於等於'
                                        },
                                        {
                                            val: 'GREATER_THAN',
                                            label: '大於'
                                        },
                                        {
                                            val: 'GREATER_THAN_OR_EQUAL',
                                            label: '大於等於'
                                        },
                                        {
                                            val: 'NULL',
                                            label: 'NULL'
                                        },
                                        {
                                            val: 'NOT_NULL',
                                            label: 'NOT NULL'
                                        },
                                    ];

                                    var opt = "";
                                    if (info.type == "string") {
                                        for (i in str) {
                                            opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                        }
                                    } else {
                                        for (i in num) {
                                            opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                        }
                                    }
                                    $($(this).parent().siblings()[0]).find("select").html(opt);
                                });

                                $(document).on("change", "select[name='winOp[]']", function() {
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                });

                                $(document).on("change", "input[name='winContent[]']", function() {
                                    $(this).attr('value', $(this).val());
                                });

                                $("button[name='winSearchBtn']").on("click", function() {
                                    var winField = [];
                                    var winContent = [];
                                    var winOp = [];
                                    $("select[name='winField[]']").each(function() {
                                        winField.push($(this).val());
                                    });
                                    $("input[name='winContent[]']").each(function() {
                                        winContent.push($(this).val());
                                    });
                                    $("select[name='winOp[]']").each(function() {
                                        winOp.push($(this).val());
                                    });

                                    addfilter(winField, winContent, winOp);
                                });
                            }
                        });

                    }
                });

                $.each(state.columns, function(i, item) {
                    if (item.text != "" && item.text != "undefined") {
                        listSource.push({
                            label: item.text,
                            value: i,
                            checked: !item.hidden
                        });
                        if (!item.hidden) {
                            var headerdata = {
                                'filed_text': item.text,
                                'filed_name': i,
                                'show': item.hidden
                            };
                            enabledheader.push(headerdata);
                        }
                    }
                });

                $("#jqxlistbox").jqxListBox({
                    allowDrop: true,
                    allowDrag: true,
                    source: listSource,
                    width: "99%",
                    height: 500,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });


    }
    function loadState2() {
        var loadURL = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/getLayoutJson') }}";
        $.ajax({
            type: "GET", //  OR POST WHATEVER...
            url: loadURL,
            data: {
                key: "mod_dss_dlvdetail",
                lang: $("#lang").val()
            },
            success: function(response) {
                if (response != "") {
                    response = JSON.parse(response);
                    $("#jqxGrid_2" ).jqxGrid('loadstate', response);
                }

                var listSource2 = [];
                state2 = $("#jqxGrid_2").jqxGrid('getstate');
                console.log(state2);
                $('#jqxLoader').jqxLoader('close');

                $.get(BASE_URL + "/searchList/get/" + "mod_dss_dlvdetail", {}, function(data) {
                    if (data.msg == "success") {
                         var opt = "<option value=''>{{ trans('common.msg1') }}</option>";

                        for (i in data.data) {
                            if (data.data[i]["layout_default"] == "Y") {
                                opt += "<option value='" + data.data[i]["id"] + "' selected>" + data.data[i]["title"] + "</option>";
                                $("input[name='searchName2']").val(data.data[i]["title"]);
                            } else {
                                opt += "<option value='" + data.data[i]["id"] + "'>" + data.data[i]["title"] + "</option>";
                            }
                        }

                        $("select[name='selSearchName2']").html(opt);

                        $.get(BASE_URL + "/searchHtml/get/" +"mod_dss_dlvdetail/"+ $("select[name='selSearchName2']").val(), {}, function(data) {
                            if (data.msg == "success") {
                                if (data.data != null) {
                                    $("#searchContent2").html(data.data["data"]);
                                } else {
                                    var seasrchTpl = getSearchTpl2(state2);
                                    initSearchWindow2(seasrchTpl);
                                }

                                var offset = $(".content-wrapper").offset();
                                $('#searchWindow2').jqxWindow({
                                    position: {
                                        x: offset.left + 50,
                                        y: offset.top + 50
                                    },
                                    showCollapseButton: true,
                                    maxHeight: 400,
                                    maxWidth: 700,
                                    minHeight: 200,
                                    minWidth: 200,
                                    height: 300,
                                    width: 700,
                                    autoOpen: false,
                                    initContent: function() {
                                        $('#searchWindow2').jqxWindow('focus');
                                    }
                                });


                                $("button[name='winSearchAdd2']").on("click", function() {
                                    var seasrchTpl = getSearchTpl2(state2);
                                    $("#searchContent2").append(searchTpl);
                                });

                                $(document).on("click", "button[name='winBtnRemove2']", function() {
                                    $(this).parents(".row").remove();
                                });

                                $(document).on("change", "select[name='winField2[]']", function() {
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                    var val = $(this).val();
                                    var info = searchObj(val, fieldObj2[0]);
                                    var str = [{
                                            val: 'CONTAINS',
                                            label: '包含'
                                        },
                                        {
                                            val: 'EQUAL',
                                            label: '等於'
                                        },
                                        {
                                            val: 'NOT_EQUAL',
                                            label: '不等於'
                                        },
                                        {
                                            val: 'NULL',
                                            label: 'NULL'
                                        },
                                        {
                                            val: 'NOT_NULL',
                                            label: 'NOT NULL'
                                        },
                                    ];

                                    var num = [{
                                            val: 'EQUAL',
                                            label: '等於'
                                        },
                                        {
                                            val: 'NOT_EQUAL',
                                            label: '不等於'
                                        },
                                        {
                                            val: 'LESS_THAN',
                                            label: '小於'
                                        },
                                        {
                                            val: 'LESS_THAN_OR_EQUAL',
                                            label: '小於等於'
                                        },
                                        {
                                            val: 'GREATER_THAN',
                                            label: '大於'
                                        },
                                        {
                                            val: 'GREATER_THAN_OR_EQUAL',
                                            label: '大於等於'
                                        },
                                        {
                                            val: 'NULL',
                                            label: 'NULL'
                                        },
                                        {
                                            val: 'NOT_NULL',
                                            label: 'NOT NULL'
                                        },
                                    ];

                                    var opt = "";
                                    if (info.type == "string") {
                                        for (i in str) {
                                            opt += '<option value="' + str[i].val + '">' + str[i].label + '</option>';
                                        }
                                    } else {
                                        for (i in num) {
                                            opt += '<option value="' + num[i].val + '">' + num[i].label + '</option>';
                                        }
                                    }
                                    $($(this).parent().siblings()[0]).find("select").html(opt);
                                });

                                $(document).on("change", "select[name='winOp2[]']", function() {
                                    $('option:selected', this).siblings().removeAttr('selected');
                                    $('option:selected', this).attr('selected', 'selected');
                                });

                                $(document).on("change", "input[name='winContent2[]']", function() {
                                    $(this).attr('value', $(this).val());
                                });

                                $("button[name='winSearchBtn2']").on("click", function() {
                                    var winField = [];
                                    var winContent = [];
                                    var winOp = [];
                                    $("select[name='winField2[]']").each(function() {
                                        winField.push($(this).val());
                                    });
                                    $("input[name='winContent2[]']").each(function() {
                                        winContent.push($(this).val());
                                    });
                                    $("select[name='winOp2[]']").each(function() {
                                        winOp.push($(this).val());
                                    });

                                    addfilter2(winField, winContent, winOp);
                                });
                            }
                        });

                    }
                });

                $.each(state2.columns, function(i, item) {
                    if (item.text != "" && item.text != "undefined") {
                        listSource2.push({
                            label: item.text,
                            value: i,
                            checked: !item.hidden
                        });
                        if (!item.hidden) {
                            var headerdata = {
                                'filed_text': item.text,
                                'filed_name': i,
                                'show': item.hidden
                            };
                            enabledheaderdetail.push(headerdata);
                        }
                    }
                });
                console.log(listSource2);
                $("#jqxlistbox2").jqxListBox({
                    allowDrop: true,
                    allowDrag: true,
                    source: listSource2,
                    width: "99%",
                    height: 500,
                    checkboxes: true,
                    filterable: true,
                    searchMode: 'contains'
                });
            }
        });
    }

    $("#saveGrid2").on("click", function() {
        var items = $("#jqxlistbox").jqxListBox('getItems');
        var gridname = "jqxGrid";
        var savekey = "mod_dss_dlv";
        if(focus=="1"){
            items = $("#jqxlistbox").jqxListBox('getItems');
            gridname = 'jqxGrid';
            savekey ="mod_dss_dlv";
        }else{
            items = $("#jqxlistbox2").jqxListBox('getItems');
            gridname = "jqxGrid_2";
            savekey = "mod_dss_dlvdetail";
        }
        // $("#" + gridOpt.gridId).jqxGrid('beginupdate');
        enabledheaderdetail    = [];
        $.each(items, function(i, item) {
            var thisIndex = $('#' + gridname).jqxGrid('getcolumnindex', item.value) - 1;
            if (thisIndex != item.index) {
                //console.log(item.value+":"+thisIndex+"="+item.index);
                $('#' + gridname).jqxGrid('setcolumnindex', item.value, (item.index +1));
            }
            if (item.checked) {
                $("#" + gridname).jqxGrid('showcolumn', item.value);
                var headerdata = {
                    'filed_text': item.label,
                    'filed_name': item.value,
                    'show': item.hidden
                };
                enabledheaderdetail.push(headerdata);
            } else {
                $("#" + gridname).jqxGrid('hidecolumn', item.value);
            }
        })

        $("#" + gridname).jqxGrid('endupdate');
        state = $("#" + gridname).jqxGrid('getstate');

        var saveUrl = "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/saveLayoutJson') }}";
        var stateToSave = JSON.stringify(state);

        $.ajax({
            type: "POST",
            url: saveUrl,
            data: {
                data: stateToSave,
                table_name: tableName,
                key: savekey,
                lang: $("#lang").val()
            },
            success: function(response) {
                if (response == "true") {
                    alert("save successful");
                    $('#gridOptModal').modal('hide');
                    $('#gridOptModal2').modal('hide');
                } else {
                    alert("save failded");
                }

            }
        });
    });

    $("#clearGrid2").on("click", function() { 
        if(focus=="1"){
            savekey ="mod_dss_dlv";
        }else{
            savekey = "mod_dss_dlvdetail";
        }
        $.ajax({
            type: "POST",
            url: "{{ url(config('backpack.base.api_route_prefix', ''). '/admin/baseApi/clearLayout') }}",
            data: {
                key: savekey,
                lang: $("#lang").val()
            },
            success: function(response) {
                if (response == "true") {
                    alert("clear successful");
                    location.reload();
                    $('#gridOptModal').modal('hide');
                    $('#gridOptModal2').modal('hide');
                } else {
                    //alert("save failded");
                }

            }
        });
    });
    // function myFunctionchange() {
    //     var x = document.getElementById("swal-input1").value;
    //     // console.log(x);
    //     $("#swal-input2").text(x);
    //     // document.getElementById("demo").innerHTML = "You selected: " + x;
    // }

    $("select[name='selSearchName2']").on("change", function() {
        var text = $('option:selected', this).text();
        if ($(this).val() == '') {
            text = "";
        }
        $("input[name='searchName2']").val(text);

        $.get(BASE_URL + "/searchHtml/get/" + $(this).val(), {}, function(data) {
            //console.log(data.data["data"]);
            if(focus=="1"){
                $("#searchContent").html(data.data["data"]);
            }else{
                $("#searchContent2").html(data.data["data"]);
            }
            
        });
    })

    function initSearchWindow2(searchTpl) {
        $("#searchContent2").append(searchTpl);
    }

    function getSearchTpl2(state) {
        var fields = [];
        $.each(state.columns, function(i, item) {
            if (item.hidden == false && item.text != "") {
                var a = {
                    label: item.text,
                    value: i
                }
                fields.push(a);
            }
        });

        var fieldStr = "";
        for (i in fields) {
            fieldStr += '<option value="' + fields[i].value + '">' + fields[i].label + '</option>';
        }




        searchTpl = '<div class="row">\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winField2[]">' + fieldStr + '</select>\
                            </div>\
                            <div class="col-xs-3">\
                                <select class="form-control input-sm" name="winOp2[]">\
                                <option value="CONTAINS">包含</option>\
                                <option value="CONTAINSLIKE">包含(多值)</option>\
                                <option value="EQUAL">等於</option>\
                                <option value="NOT_EQUAL">不等於</option>\
                                <option value="NULL">NULL</option>\
                                <option value="NOT_NULL">NOT NULL</option>\
                                <option value="LESS_THAN">小於</option>\
                                <option value="LESS_THAN_OR_EQUAL">小於等於</option>\
                                <option value="GREATER_THAN">大於</option>\
                                <option value="GREATER_THAN_OR_EQUAL">大於等於</option>\
                                </select>\
                            </div>\
                            <div class="col-xs-4">\
                                <input type="text" class="form-control input-sm" name="winContent2[]">\
                            </div>\
                            <div class="col-xs-2">\
                                <button class="btn btn-sm btn-info btn-danger" name="winBtnRemove2">-</button>\
                            </div>\
                        </div>';
        return searchTpl;
    }
    $("#jqxGrid_2").on("rowdoubleclick", function(event) {
        // var row = $("#jqxGrid").jqxGrid('getrowdata', rows['event.args.rowindex']);
        var args = event.args;
        // row's bound index.
        var boundIndex = args.rowindex;
        // row's visible index.
        var visibleIndex = args.visibleindex;
        // right click.
        var rightclick = args.rightclick;
        // original event.
        var ev = args.originalEvent;

        var datarow = $("#jqxGrid_2").jqxGrid('getrowdata', boundIndex);
        console.log(datarow);

        var editUrl = gridOpt.editUrl.replace("{id}", datarow.id);
        //location.href = editUrl;
        window.open(editUrl);
    })


    var addfilter2 = function(datafield, filterval, filtercondition) {
        $("#jqxGrid_2").jqxGrid('clearfilters');
        var old_filed = "";
        for (i in datafield) {
            var filtergroup = new $.jqx.filter();
            if (old_filed != datafield[i]) {
                old_filed = datafield[i];

                var filter_or_operator = 1;
                //var filtervalue = filterval;
                var filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                if (datafield[i] == "updated_at" || datafield[i] == "created_at" || datafield[i] == "confirm_dt" || datafield[i] == "finish_date" || datafield[i] == "etd") {
                    filter = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(filter_or_operator, filter);
                } else {
                    filter = filtergroup.createfilter('stringfilter', filterval[i], filtercondition[i]);
                    filtergroup.addfilter(filter_or_operator, filter);
                }

                // add the filters.
                $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);
            } else {
                var filter3 = filtergroup.createfilter('datefilter', filterval[i - 1], filtercondition[i - 1]);
                filtergroup.addfilter(0, filter3);
                $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);

                var filter2 = filtergroup.createfilter('datefilter', filterval[i], filtercondition[i]);
                filtergroup.addfilter(0, filter2);
                $("#jqxGrid_2").jqxGrid('addfilter', datafield[i], filtergroup);
            }
        }
        // apply the filters.
        $("#jqxGrid_2").jqxGrid('applyfilters');
    }


    function searchObj(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    $("button[name='setDefault2']").on("click", function() {
        if(focus=="1"){
            savekey ="mod_dss_dlv";
        }else{
            savekey = "mod_dss_dlvdetail";
        }
        $.ajax({
            url: BASE_URL + "/setSearchDefault/" + savekey + "/" + $("select[name='selSearchName2']").val(),
            dataType: "JSON",
            method: "PUT",
            success: function(result) {
                if (result.msg == "success") {
                    swal("{{trans('common.msgnew13')}}", "", "success");
                } else {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            },
            error: function() {
                swal("{{trans('common.msgnew5')}}", "", "error");
            }
        });
    });

    $("button[name='delSearch2']").on("click", function() {
        $.ajax({
            url: BASE_URL + "/delSearchLayout/" + $("select[name='selSearchName2']").val(),
            dataType: "JSON",
            method: "DELETE",
            success: function(result) {
                if (result.msg == "success") {
                    swal("{{trans('common.msgnew4')}}", "", "success");
                    $("select[name='selSearchName2'] option:selected").remove();
                    $("input[name='searchName2']").val("");
                } else {
                    if (typeof data.errorMsg !== "undefined") {
                        swal("{{trans('common.msgnew5')}}", data.errorMsg, "error");
                    } else {
                        swal("{{trans('common.msgnew5')}}", "", "error");
                    }

                }
            },
            error: function() {
                swal("{{trans('common.msgnew5')}}", "", "error");
            }
        });
    });

    $("button[name='saveSearch2']").on("click", function() {
        if(focus=="1"){
            savekey ="mod_dss_dlv";
        }else{
            savekey = "mod_dss_dlvdetail";
        }
        var htmlData = $("#searchContent2").html();
        var postData = {
            "key": savekey,
            "data": htmlData,
            "title": $("input[name='searchName2']").val(),
            "id": ($("select[name='selSearchName2'] option:selected").text() == $("input[name='searchName2']").val()) ? $("select[name='selSearchName2']").val() : null
        };
        $.ajax({
            url: BASE_URL + "/{{$lang}}" + "/saveSearchLayout",
            data: postData,
            dataType: "JSON",
            method: "POST",
            success: function(result) {
                if (result.msg == "success") {
                    swal("{{trans('common.msgnew13')}}", "", "success");
                    $("select[name='selSearchName2'] option").removeAttr("selected");
                    $("select[name='selSearchName2']").append("<option value='" + result.id + "' selected>" + $("input[name='searchName2']").val() + "</option>")
                } else {
                    swal("{{trans('common.msgnew5')}}", "", "error");
                }
            },
            error: function() {
                swal("{{trans('common.msgnew12')}}", "", "error");
            }
        });
    });
</script>
<script>
    var picsignsysno = 0;
    var picerrsysno = 0;
    var sysnonow = 0;
</script>

<script>
$(document).ready(function() {
    // 在這撰寫javascript程式碼
        $("[href='#tab_1']").click();   
    });

    function tab_1() {
        focus = "1";
        $("#btnAdd").show();
        $("#btnDelete").show();
        $("#btnTakedown").hide();
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid_2').style.display = 'none';
        // 
    }
    function tab_2() {

        focus = "2";

        $('#jqxGrid_2').jqxGrid('destroy');
        $('#griddiv').append('<div id="jqxGrid_2"></div>')

        $("#btnAdd").hide();
        $("#btnDelete").hide();
        $("#btnTakedown").show();
        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid_2').style.display = 'block';
        ///
        $("#jqxGrid_2").jqxGrid({
            width: '100%',
            height: winHeight2,
            source: dataAdapterdetail,
            sortable: true,
            filterable: true,
            altrows: true,
            showfilterrow: true,
            pageable: true,
            virtualmode: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            columnsresize: true,
            columnsautoresize: true,
            clipboard: true,
            selectionmode: 'checkbox',
            keyboardnavigation: false,
            enablebrowserselection: true,
            pagesizeoptions: [50, 100, 500, 2000],
            rendergridrows: function(params) {
                //alert("rendergridrows");
                return params.data;
            },
            ready: function() {
                $("#jqxGrid_2").jqxGrid('height', winHeight2 + 'px');
                loadState2();
            },
            updatefilterconditions: function(type, defaultconditions) {
                var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                switch (type) {
                    case 'stringfilter':
                        return stringcomparisonoperators;
                    case 'numericfilter':
                        return numericcomparisonoperators;
                    case 'datefilter':
                        return datecomparisonoperators;
                    case 'booleanfilter':
                        return booleancomparisonoperators;
                }
            },
            columns: columnsdetail
        });
        ///

        var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
        var ischeck = "N";
        var mainids = new Array();
        for (var m = 0; m < rows.length; m++) {
            var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
            if(typeof row != "undefined") {
                mainids.push(row.sys_dlv_no);
                ischeck = "Y";
            }
        }
        var filterGroups = $('#jqxGrid').jqxGrid('getfilterinformation');
        if ( ischeck == "N") {
            if(filterGroups.length > 0 ) {
                var allrows = $('#jqxGrid').jqxGrid('getrows');
                for(var i = 0; i < allrows.length; i++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', i);
                    mainids.push(row.sys_dlv_no);
                }
                sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_dss_dlv_detail/false') }}"+"?basecon=sys_dlv_no;IN;"+JSON.stringify(mainids); 
            } else {
                sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_dss_dlv_detail/false') }}"+"?basecon=id;EQUAL;0"; 
            }

        } else {
            sourcedetail.url = "{{ url(config('backpack.base.api_route_prefix', '').'/admin/baseApi/getGridJson/mod_dss_dlv_detail/false') }}"+"?basecon=sys_dlv_no;IN;"+JSON.stringify(mainids); 
        }
        $("#jqxGrid_2").jqxGrid('updatebounddata');
    }
</script>
@endsection