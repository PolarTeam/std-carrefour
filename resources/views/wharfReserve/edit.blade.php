@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('mod_mark_reserve.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mod_mark_reserve') }}">{{ trans('mod_mark_reserve.titleName') }}</a></li>
		<li class="active">{{ trans('mod_mark_reserve.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <h3 class="sub-title">預約資訊</h3>
                                <div class="row">

                                    <div class="form-group col-md-2">
                                        <label for="mark_no">{{ trans('mod_mark_reserve.dc_name') }} <span style="color:red">*<span></label>
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="wh_no" name="wh_no" >
                                            <input type="hidden" class="form-control" id="dc_id" name="dc_id" >
                                            <input type="text" class="form-control" id="dc_name" name="dc_name" >
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="dc_name"
                                                    info1="{{Crypt::encrypt('mod_warehouse_for_look')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,dc_id")}}" 
                                                    {{-- info3="{{Crypt::encrypt(" dc_id is null and cust_no in('".str_replace(',', '\',\'', Auth::user()->check_wh)."')")}}" --}}
                                                    info3="{{Crypt::encrypt(" dc_id is null ")}}"
                                                    info5="{{Crypt::encrypt('mod_warehouse')}}"
                                                    info4="cust_no=dc_id;cname=dc_name;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="reserve_date">{{ trans('mod_mark_reserve.reserve_date') }} <span style="color:red">*<span></label>
                                        <input type="date" class="form-control" id="reserve_date" name="reserve_date" >
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="reserve_h">{{ trans('mod_mark_reserve.reserve_h') }} <span style="color:red">*<span></label>
                                        <select class="form-control" id="reserve_h" name="reserve_h">
                                            @for($i=9; $i<=23;$i++)
                                                @if ($i < 10)
                                                <option value="0{{$i}}">0{{$i}}</option>
                                                @else
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endif
                                            @endfor
                                        </select>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="reserve_m">{{ trans('mod_mark_reserve.reserve_m') }} <span style="color:red">*<span></label>
                                        <select class="form-control" id="reserve_m" name="reserve_m">
                                            <option value="00">0</option>
                                            <option value="15">15</option>
                                            <option value="30">30</option>
                                            <option value="45">45</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-md-2">
                                        <label for="owner_name">{{ trans('mod_mark_reserve.owner_name') }} <span style="color:red">*<span></label>
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="owner_no" name="owner_no" >
                                            <input type="text" class="form-control" id="owner_name" name="owner_name" >
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="owner_name"
                                                    info1="{{Crypt::encrypt('sys_customers')}}" 
                                                    info2="{{Crypt::encrypt("cust_no+cname,cust_no,cname,receive_mail")}}" 
                                                    @if(isset($condition))
                                                        info3="{{Crypt::encrypt("status='A' AND type='OTHER' and cust_no in(".$condition.')')}}"
                                                    @else
                                                        info3="{{Crypt::encrypt("status='A' AND type='OTHER' AND g_key='".Auth::user()->g_key."' AND c_key='".Auth::user()->c_key."' ")}}"
                                                    @endif
                                                    info5="{{Crypt::encrypt('sys_customers')}}"
                                                    info4="cust_no=owner_no;cname=owner_name;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="status">{{ trans('mod_mark_reserve.car_type_descp') }} <span style="color:red">*<span></label>
                                        <input type="hidden" name="car_type_descp" id = "car_type_descp" class="form-control">
                                        <select class="form-control select2" id="car_type" name="car_type">
                                            <option value=""></option>
                                            <option value="3.5T">3.5T</option>
                                            <option value="6.8T">6.8T</option>
                                            <option value="11T">11T</option>
                                            <option value="17T">17T</option>
                                            <option value="貨櫃">貨櫃</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row border-bottom-box">
                                    <div class="form-group col-md-2">
                                        <label for="cust_import_no">{{ trans('mod_mark_reserve.cust_import_no') }} <span style="color:red" id="cust_import_no_red" >*<span></label>
                                        <input type="text" class="form-control" id="cust_import_no"  name="cust_import_no" >
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="process_time">{{ trans('mod_mark_reserve.process_time') }}</label>
                                        <input type="text" class="form-control" id="process_time"  name="process_time">
                                    </div>

                                    <div class="form-group col-md-3 mb-0">
                                        <button type="button" class="orange-btn" id="toggleReserveFullBtn">
                                            碼頭預約狀態
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>


                                <div class="reserve-full-box" id="reserveFullBox">
                                    <div>
                                        <table>
                                            <thead>
                                                <tr id="reservetime">
                                                    <td class="full-title">時間</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id="reservecount">
                                                    <td class="full-title">可約量/總約量</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <h3 class="sub-title">貨車資訊</h3>
                                <div class="row">

                                    <div class="form-group col-md-2">
                                        <label for="work_descp">{{ trans('mod_mark_reserve.truck_cmp_name') }} <span style="color:red">*<span></label>
                                        <input type="text" class="form-control" id="truck_cmp_name" name="truck_cmp_name" >
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="car_no">{{ trans('mod_mark_reserve.car_no') }}</label>
                                        <input type="text" class="form-control" id="car_no"  name="car_no" >
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="driver_name">{{ trans('mod_mark_reserve.driver_name') }}</label>
                                        <input type="text" class="form-control" id="driver_name"  name="driver_name" >
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="driver_phone">{{ trans('mod_mark_reserve.driver_phone') }}</label>
                                        <input type="text" class="form-control" id="driver_phone"  name="driver_phone" >
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="status">{{ trans('mod_mark_reserve.con_type_desc') }}</label>
                                        <input type="hidden" name="con_type_desc" id = "con_type_desc" class="form-control">
                                        <select class="form-control select2" id="con_type" name="con_type">
                                            <option value=""></option>
                                            @foreach($con_type as $row)
                                            <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="con_no">{{ trans('mod_mark_reserve.con_no') }}</label>
                                        <input type="text" class="form-control" id="con_no"  name="con_no" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label for="work_descp">{{ trans('mod_mark_reserve.work_descp') }} <span style="color:red" id="work_descp_red">*<span></label>
                                        <select class="form-control select2" id="work_type" name="work_type" >
                                            <option value=""></option>
                                            <option value="進貨">進貨</option>
                                            <option value="拉空櫃">拉空櫃</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="status">{{ trans('mod_mark_reserve.unloading_descp') }}</label>
                                        <input type="hidden" name="unloading_descp" id = "unloading_descp" class="form-control">
                                        <select class="form-control select2" id="unloading_type" name="unloading_type">
                                            <option value=""></option>
                                            @foreach($unloading_type as $row)
                                            <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="box_num">{{ trans('mod_mark_reserve.box_num') }} <span style="color:red" id="box_num_red">*<span></label>
                                        <input type="number" class="form-control" id="box_num" name="box_num" >
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="items_num">{{ trans('mod_mark_reserve.items_num') }} <span style="color:red" id="items_num_red">*<span></label>
                                        <input type="number" class="form-control" id="items_num" name="items_num" >
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col-md-3">
                                      <label for="created_by">{{ trans('mod_mark_reserve.created_by') }}</label>
                                      <input type="text" class="form-control" id="created_by" name="created_by" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="created_at">{{ trans('mod_mark_reserve.created_at') }}</label>
                                      <input type="text" class="form-control" id="created_at" name="created_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_by">{{ trans('mod_mark_reserve.updated_by') }}</label>
                                      <input type="text" class="form-control" id="updated_by" name="updated_by" switch="off"  disabled="disabled" readonly="true">
                                  </div>
                                  <div class="form-group col-md-3">
                                      <label for="updated_at">{{ trans('mod_mark_reserve.updated_at') }}</label>
                                      <input type="text" class="form-control" id="updated_at" name="updated_at" switch="off" disabled="disabled" readonly="true">
                                  </div>
                              </div>
                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>

<style>
    .orange-btn {
        color: #fff;
        background: #F47920;
        border-radius: 8px;
        border: 0px solid transparent;
        outline: 0px solid transparent;
        font-size: 24px;
        padding: 8px 18px;
    }
    .orange-btn:hover, .orange-btn:active, .orange-btn:focus {
        border: 0px solid transparent;
        outline: 0px solid transparent;
    }
    .orange-btn i {
        display: inline-block;
        margin-left: 18px;
        transform: rotate(0deg);
        transition: all 0.2s;
    }
    .orange-btn.open i {
        transform: rotate(180deg);
        transition: all 0.2s;
    }
    .border-bottom-box {
        border-bottom: 1px solid #D2D6DE;
    }
    .reserve-full-box {
        overflow: hidden;
        height: auto;
        border-bottom: 1px solid #D2D6DE;
        padding: 28px 15px;
        margin: 0 -15px 16px;
        transition: all 0.2s;
    }
    .reserve-full-box.hidden {
        height: 0;
        margin-bottom: 0;
        transition: all 0.2s;
    }
    .full-title {
        font-size: 18px;
        padding-right: 16px;
        padding-bottom: 4px;
        color: #888;
        text-align: end;
    }
    .time-item {
        font-size: 18px;
        min-width: 85px;
        padding-bottom: 4px;
        text-align: center;
    }
    .can-reserve {
        font-size: 24px;
        color: #000;
    }
    .time-item.full {
        color: #888!important;
    }
    .reserve-full-box tbody .time-item {
        padding-bottom: 32px;
        color: #888;
    }
    .reserve-full-box tbody .full-title {
        padding-bottom: 20px;
    }
    .sub-title {
        font-size: 20px;
        margin-bottom: 14px;
    }
</style>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;
    var canStore  = '{{$canStore}}';
    var canEdit   = '{{$canEdit}}';
    var canDelete = '{{$canDelete}}';

    $('#work_type').change(function() {
        console.log('work_type change');
        var workvalue = $('#work_type').val();
        if( workvalue  == "拉空櫃") {
            $("#cust_import_no_red").hide();
            $("#work_descp_red").hide();
            $("#box_num_red").hide();
            $("#items_num_red").hide();
        } else {
            $("#cust_import_no_red").show();
            $("#work_descp_red").show();
            $("#box_num_red").show();
            $("#items_num_red").show();
        }
    });
    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    $('#myForm button[btnName="dc_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('dc_name', "倉庫搜尋", callBackFunc=function(data){
            console.log(data);
            notwokday();
            var dc_id =$('#dc_id').val();
            $.post(BASE_URL+'/getownerlookup', {'dc_id': dc_id}, function(data){
                if(data.msg=='success'){
                    $('button[btnName="owner_name"]').attr('info3', data.val);
                }
            })

        });
    });

    $('#myForm input[name="dc_name"]').on('click', function(){
        var check = $('#subBox input[name="dc_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","dc_name",callBackFunc=function(data){
                console.log(data);
                notwokday();
                var dc_id =$('#dc_id').val();
                $.post(BASE_URL+'/getownerlookup', {'dc_id': dc_id}, function(data){
                    if(data.msg=='success'){
                        $('button[btnName="owner_name"]').attr('info3', data.val);
                    }
                })

            },"dc_name");
        }
    });


    $("#reserve_date").on("change", function() {
        console.log('change');
        notwokday();
        reservefull();
    });
    $("#car_type").on("change", function() {
        console.log('car_type change');
        reservefull();
    });
    function reservefull() {
        var reservetimestr = "\n<td class='full-title'>時間</td>\n";
        var reservecountstr = "\n<td class='full-title'>可約量/總約量</td>\n";
        
        // <td class="time-item">15:15</td>
        // <td class="time-item"><span class="can-reserve">2</span> / 8</td>
        $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: BASE_URL + '/get/reservedata',
            data:JSON.stringify({ 
                "car_type": $("#car_type").val(),
                "dc_id": $("#dc_id").val(),
                "reserve_date": $("#reserve_date").val(),
            }),
            success: function(data) {
                console.log('reservedata');
                console.log(data);
                var time =  '';
                for (let i = 0; i < data.length; i++) {
                    
                    time = data[i].full_date;
                    time = time.replace($("#reserve_date").val(), '');
                    time = time.replace(":00", '');
                    totalcount = data[i].totalcount;
                    reservecount = data[i].reservecount;
                    reservetimestr = reservetimestr+"<td class='time-item'>"+time+"</td>";
                    reservecountstr = reservecountstr+"<td class='time-item'><span class='can-reserve'>"+reservecount+"</span> / "+totalcount+"</td>";
                }
                reservetimestr = reservetimestr+"\n";
                reservecountstr = reservecountstr+"\n";
                console.log(reservetimestr)
                console.log(reservecountstr);
                $("#reservetime").html(reservetimestr);
                $("#reservecount").html(reservecountstr);
            
            }
        });

    }
    function notwokday() {
        var newoption = "\n";
        if ( $("#dc_id").val() == "" || $("#reserve_date").val() == "") {
            console.log("empty request")
            return;
        } 
        $.ajax({
            type: "post", //  OR POST WHATEVER...
            dataType : 'json', // 預期從server接收的資料型態
            contentType : 'application/json; charset=utf-8', // 要送到server的資料型態
            url: BASE_URL + '/get/notwokday',
            data:JSON.stringify({ 
                "dc_id" : $("#dc_id").val(),
                "date" : $("#reserve_date").val(),
            }),
            success: function(data) {
                console.log("notwokday");
                console.log(data);
                /*
                2023-06-08 By Tony 
                api取得值為空object表示mod_warehouse_notwork有符合的資料 
                api取得值為空list表示mod_warehouse_detail無符合的資料
                以上條件跳出相對應訊息dialog
                */
                if (!Array.isArray(data)) {
                    swal("警告", "預約日期未營業", "warning");
                    $("#reserve_h").html(newoption);  
                    return false;
                }
                if (data.length == 0) {
                    swal("警告", "無時段可預約", "warning");
                    $("#reserve_h").html(newoption);  
                    return false;
				}
                var start = parseInt(data[0].start_at);
                var end   = parseInt(data[0].end_at);
                var hour  = 9;
                for (let i = start; i < end; i++) {
                    if(i<10) {
                        hour = '0'+i;
                    } else {
                        hour = i;
                    }
                    newoption = newoption+"<option value='"+hour+"'>"+hour+"</option>\n";
                }
                newoption = newoption+"\n";
                $("#reserve_h").html(newoption);  
            
            }
        });
        reservefull();
    }

    $('#myForm button[btnName="con_type_desc"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('con_type_desc', "貨櫃類型搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="con_type_desc"]').on('click', function(){
        var check = $('#subBox input[name="con_type_desc"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","con_type_desc",callBackFunc=function(data){
                console.log(data);
                
            },"con_type_desc");
        }
    });


    

    $('#myForm button[btnName="owner_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('owner_name', "貨主搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="owner_name"]').on('click', function(){
        var check = $('#subBox input[name="owner_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","owner_name",callBackFunc=function(data){
                console.log(data);
                
            },"owner_name");
        }
    });

    $('#myForm button[btnName="unloading_descp"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('unloading_descp', "下貨搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="unloading_descp"]').on('click', function(){
        var check = $('#subBox input[name="unloading_descp"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","unloading_descp",callBackFunc=function(data){
                console.log(data);
                
            },"unloading_descp");
        }
    });

    $('#myForm button[btnName="work_descp"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('work_descp', "作業搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="work_descp"]').on('click', function(){
        var check = $('#subBox input[name="work_descp"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","work_descp",callBackFunc=function(data){
                console.log(data);
                
            },"work_descp");
        }
    });
    

    // $('#myForm button[btnName="car_type_descp"]').on('click', function(){
    //     $('#lookupModal').modal('show');
    //     initLookup('car_type_descp', "車型搜尋", callBackFunc=function(data){
    //         console.log(data);
    //     });
    // });

    // $('#myForm input[name="car_type_descp"]').on('click', function(){
    //     var check = $('#subBox input[name="car_type_descp"]').data('ui-autocomplete') != undefined;
    //     if(check == false) {
    //         initAutocomplete("myForm","car_type_descp",callBackFunc=function(data){
    //             console.log(data);
                
    //         },"car_type_descp");
    //     }
    // });

    $('#myForm button[btnName="truck_cmp_name"]').on('click', function(){
        $('#lookupModal').modal('show');
        initLookup('truck_cmp_name', "車商搜尋", callBackFunc=function(data){
            console.log(data);
        });
    });

    $('#myForm input[name="truck_cmp_name"]').on('click', function(){
        var check = $('#subBox input[name="truck_cmp_name"]').data('ui-autocomplete') != undefined;
        if(check == false) {
            initAutocomplete("myForm","truck_cmp_name",callBackFunc=function(data){
                console.log(data);
                
            },"truck_cmp_name");
        }
    });
    


    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/wharfReserve" ;
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#work_type').select2();
            $('#con_type').select2();
            $('#car_type').select2();
            $('#unloading_type').select2();
        };
        formOpt.addFunc = function() {
            $('#work_type').val();
            $('#work_type').trigger('change');

            $('#con_type').val();
            $('#con_type').trigger('change');

            $('#car_type').val();
            $('#car_type').trigger('change');

            $('#unloading_type').val();
            $('#unloading_type').trigger('change');
        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            $("#iAdd").hide();
            $("#iDel").hide();
            reservefull();
            if(canStore == "Y") {
                menuBtnFunc.enabled(['iAdd']);
            } else {
                menuBtnFunc.disabled(['iAdd']);
            }
            if(canEdit == "Y") {
                menuBtnFunc.enabled(['iEdit']);
            } else {
                menuBtnFunc.disabled(['iEdit']);
            }
            if(canDelete == "Y") {
                menuBtnFunc.enabled(['iDel']);
            } else {
                menuBtnFunc.disabled(['iDel']);
            }
        }
        formOpt.beforesaveFunc = function() {
            result = true;
            if($("#con_type").val() != '' && $("#con_type").val() != null) {
                var doublecheck = [
                    {
                        "column_filed":"con_no",
                        "column_type":"string",
                    },
                ];
                var result = beforesave(doublecheck) ;
            }
            var iserror = false ;
            var requiredColumn = [
                {
                    "column_filed":"cust_import_no",
                    "column_type":"string",
                },
                {
                    "column_filed":"dc_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"items_num",
                    "column_type":"string",
                },
                {
                    "column_filed":"box_num",
                    "column_type":"string",
                },
                {
                    "column_filed":"reserve_date",
                    "column_type":"string",
                },
                {
                    "column_filed":"owner_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"reserve_h",
                    "column_type":"string",
                },
                {
                    "column_filed":"reserve_m",
                    "column_type":"string",
                },
                {
                    "column_filed":"truck_cmp_name",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;

            if( $("#work_type").val() == "" ) {
                document.getElementsByClassName('select2-selection select2-selection')[2].style.backgroundColor = "FCE8E6";
                cansafe = false;
            }
            if( $("#car_type").val() == "" ) {
                document.getElementsByClassName('select2-selection select2-selection')[0].style.backgroundColor = "FCE8E6";
                cansafe = false;
            }
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            if(!result) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            
            $("#work_descp").val($('#work_type').val());
            $("#con_type_desc").val( $('#con_type').select2('data')[0].text);
            $("#car_type_desc").val( $('#car_type').select2('data')[0].text);
            $("#unloading_descp").val( $('#unloading_type').select2('data')[0].text);
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
<script>
    // 顯示額滿時段按鈕
    document.getElementById('toggleReserveFullBtn').addEventListener('click', function (e) {
        e.stopPropagation();
        reservefull();
    })
</script>
@endsection