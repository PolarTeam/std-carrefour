@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('company.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/project') }}">{{trans('company.titleName')}}</a></li>
		<li class="active">{{trans('company.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">{{ trans('sysCustomers.baseInfo') }}</a></li>                    
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cust_no">{{ trans('sysCustomers.custNo') }}</label>
                                        <input type="text" class="form-control" id="cust_no" name="cust_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cname">{{ trans('sysCustomers.cname') }}</label>
                                        <input type="text" class="form-control" id="cname" name="cname">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="ename">{{ trans('sysCustomers.ename') }}</label>
                                        <input type="text" class="form-control" id="ename" name="ename">
                                    </div>      
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('sysCustomers.email') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="eamil" class="form-control" id="email" name="email" >
                                        </div>
                                    </div>                              
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="cmp_abbr">{{ trans('sysCustomers.cmpAbbr') }}</label>
                                        <input type="text" class="form-control" id="cmp_abbr" name="cmp_abbr">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="contact">{{ trans('sysCustomers.contact') }}</label>
                                        <input type="text" class="form-control" id="contact" name="contact" =>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('sysCustomers.status') }}</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="A">{{ trans('sysCustomers.STATUS_A') }}</option>
                                            <option value="B">{{ trans('sysCustomers.STATUS_B') }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="identity">{{ trans('sysCustomers.identity') }}</label>
                                        <select class="form-control" id="identity" name="identity" @if(isset($id)) switch="off" @endif >
                                            <option value="G">集團</option>
                                            <option value="C">公司</option>
                                            <option value="S">站別</option>
                                            <option value="D">部門</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-3" id="group" style="display:none">
                                        <label for="g_key">集團</label>
                                        <select class="form-control" id="g_key" name="g_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="cmp" style="display:none">
                                        <label for="c_key">公司</label>
                                        <select class="form-control" id="c_key" name="c_key"></select>
                                    </div>
                                    <div class="form-group col-md-3" id="stn" style="display:none">
                                        <label for="d_key">站別</label>
                                        <select class="form-control" id="d_key" name="d_key"></select>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="phone">{{ trans('sysCustomers.phone') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control" id="phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="fax">{{ trans('sysCustomers.fax') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-fax"></i>
                                            </div>
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="zip">{{ trans('sysCustomers.zip') }}</label>
                                        <input type="text" class="form-control" id="zip" name="zip">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city_nm">{{ trans('sysCustomers.cityNm') }}</label>
                                        <input type="text" class="form-control" id="city_nm" name="city_nm">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="area_nm">{{ trans('sysCustomers.areaNm') }}</label>
                                        <input type="hidden" class="form-control" id="area_id" name="area_id">
                                        <input type="text" class="form-control" id="area_nm" name="area_nm">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="address">{{ trans('sysCustomers.address') }}</label>
                                        <input type="text" class="form-control" id="address" name="address">
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ trans('sysCustomers.remark') }}</label>
                                            <textarea class="form-control" rows="3" name="remark"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <input type="hidden" id="created_by" name="created_by" class="form-control">
                                <input type="hidden" id="updated_by" name="updated_by" class="form-control">
                                <input type="hidden" id="created_at" name="created_at" class="form-control">
                                <input type="hidden" id="updated_at" name="updated_at" class="form-control">
                                <input type="hidden" id="type" name="type" value="SELF"  class="form-control">
                                <input type="hidden" id="zip_code" name="zip_code" class="form-control">
                                <input type="hidden" id="en_address" name="en_address" class="form-control">
                                <input type="hidden" id="website" name="website" class="form-control">
                                <input type="hidden" id="industry" name="industry" class="form-control">
                                <input type="hidden" id="fax" name="fax" class="form-control">
                                <input type="hidden" id="identity" name="identity" class="form-control">
                                <input type="hidden" id="city_no" name="city_no" class="form-control">
                                <input type="hidden" id="area_id" name="area_id" class="form-control">

                                <input type="hidden" id="g_key" name="g_key" class="form-control">
                                <input type="hidden" id="c_key" name="c_key" class="form-control">
                                <input type="hidden" id="s_key" name="s_key" class="form-control">
                                <input type="hidden" id="d_key" name="d_key" class="form-control">
                                                                
                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control">
                                @endif

                            </div>
                        </form>
                    </div>
                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </form>
    </div> 
</div>



@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile/batchDelete";

    $(function(){

        $('#myForm button[btnName="zip"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('zip', "郵地區號查詢", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="zip"]').on('click', function(){
            var check = $('#subBox input[name="zip"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","zip",callBackFunc=function(data){
                    console.log(data);
                    
                },"zip");
            }
        });


        $('#subBox button[btnName="fee_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('fee_name', "代收款建檔", callBackFunc=function(data){
            });
        });

        $('#subBox button[btnName="goods_no"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('goods_no', "料號建檔", callBackFunc=function(data){
            });
        });


        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/companyProfile" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#cust_type').select2();

            
        };
        formOpt.addFunc = function() {
            $('#cust_type').val();
            $('#cust_type').trigger('change');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function(data) {
            $('#type').val('OTHER');
        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            var requiredColumn = [
                // {
                //     "column_filed":"project_name",
                //     "column_type":"string",
                // },
                // {
                //     "column_filed":"manage_by",
                //     "column_type":"string",
                // },
                // {
                //     "column_filed":"manage_name",
                //     "column_type":"string",
                // },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
});
</script>
@endsection