@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('mail_format.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/mailFormat') }}">{{trans('mail_format.mail_format')}}</a></li>
		<li class="active">{{trans('mail_format.mail_format')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="title">{{trans('mail_format.title')}}</label>
                                        <input type="text" class="form-control" id="title" name="title" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="type">{{trans('mail_format.type_descp')}}</label>
                                        <div class="input-group input-group-sm">
                                            <input type="hidden" class="form-control" id="type" name="type">
                                            <input type="text" class="form-control" id="type_descp" name="type_descp">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-flat lookup" btnname="type_descp"
                                                    info1="{{Crypt::encrypt('bscode')}}" 
                                                    info2="{{Crypt::encrypt('cd+cd_descp,cd,cd_descp')}}" 
                                                    info3="{{Crypt::encrypt('cd_type = "MF" ')}}"
                                                    info5="{{Crypt::encrypt('bscodeKind')}}"
                                                    info4="cd=type;cd_descp=type_descp;" triggerfunc="" selectionmode="singlerow">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="content">{{trans('mail_format.content')}}</label>
                                        <textarea class="form-control iEditor" id="content" name="content"></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_by') }}</label>
                                        <input type="text" class="form-control" id="created_by" name="created_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.created_at') }}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_by') }}</label>
                                        <input type="text" class="form-control" id="updated_by" name="updated_by" disabled="disabled" readonly="true" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.updated_at') }}</label>
                                        <input type="text" class="form-control" id="updated_at" name="updated_at" disabled="disabled" readonly="true" required="required">
                                    </div>
                                </div>

                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="created_by_name" class="form-control">
                                        <input type="hidden" name="created_at" class="form-control">
                                        <input type="hidden" name="updated_at"  class="form-control">
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>


@endsection
@include('layout.template.lookup')

@section('after_scripts')

<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>

<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
        
    @endif
    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat";

    $(function(){


        $('#myForm button[btnName="type_descp"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('type_descp', "郵件搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="type_descp"]').on('click', function(){
            var check = $('#subBox input[name="type_descp"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","type_descp",callBackFunc=function(data){
                    console.log(data);
                    
                },"type_descp");
            }
        });

        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/mailFormat" ;

        formOpt.addFunc = function() {
            CKEDITOR.instances['content'].setReadOnly(false);
            CKEDITOR.instances['content'].setData("");
        }
        formOpt.copyFunc = function() {
            setTimeout(function(){
                CKEDITOR.instances['content'].setReadOnly(false);
            }, 500);
        }
        formOpt.editFunc = function (){
            setTimeout(function(){
                CKEDITOR.instances['content'].setReadOnly(false);
            }, 500);
        };

        formOpt.initFieldCustomFunc = function (){
            $('#myForm'+' textarea[name="content"]').ckeditor({
                "extraPlugins" : "oembed,widget"
            });
            @if(isset($id))
                setTimeout(function(){
                    CKEDITOR.instances['content'].setReadOnly(true);
                }, 500);
            @endif

            
        }

        formOpt.saveSuccessFunc = function(data) {
            CKEDITOR.instances['content'].setReadOnly(true);
        }

        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            var requiredColumn = [
                {
                    "column_filed":"title",
                    "column_type":"string",
                },
                {
                    "column_filed":"type_descp",
                    "column_type":"string",
                }
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }
            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);



    })
</script> 
<script>
$(function(){
    // formOpt.initFieldCustomFunc();
    // $.get( formOpt.fieldsUrl , function( data ) {
    //     if(typeof formOpt.afterInit === "function") {
    //         console.log(data.data);
    //         // formOpt.afterInit();
    //         // data.data.forEach(element => {
    //         //     console.log(element);
    //         // });
    //     }
    // });
});
</script>
@endsection